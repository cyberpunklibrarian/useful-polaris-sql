/*
This query is for the Find Tool. It locates bibs with tags with specific data. From there you can easily
create a record set and use the native bulk bib change to update tags. I hope this helps somebody!

Contributed by: Heather Arnold - Municipal Library Consortium
*/

SELECT
    BibliographicRecordID

FROM
    BibliographicTags bt (nolock)

INNER JOIN
    BibliographicSubfields bs (nolock)
ON
    bt.bibliographictagid = bs.bibliographictagid
WHERE
    Tagnumber = xxx
AND
    Subfield = 'v'
AND DATA LIKE '%data%'