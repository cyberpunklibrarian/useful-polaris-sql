/* This query checks the items in an item record set to see if they're set to
Display in PAC. Additionally it checks the item's bib record to see if it's
set to Display in PAC. It returns item record modification information as well
as bib record modification info. */


SELECT
    cir.ItemRecordID AS [Item Record ID],
    cir.DisplayInPAC AS [Item Display in PAC],
    br.DisplayInPAC AS [Bib Display in PAC],
    ist.Description AS [Item Circ Status],
    br.BibliographicRecordID AS [Bib Record ID],
    br.BrowseTitle AS [Browse Title],
    ird.ModificationDate AS [Last Modified Date],
    pu.Name AS [Last Item Modifer],
    br.ModificationDate as [Bib Last Modified],
    pubib.Name as [Last Bib Modifier]

FROM
    Polaris.Polaris.CircItemRecords cir WITH (NOLOCK)

INNER JOIN
    Polaris.Polaris.BibliographicRecords br WITH (NOLOCK) ON br.BibliographicRecordID = cir.AssociatedBibRecordID
INNER JOIN
    Polaris.Polaris.ItemStatuses ist WITH (NOLOCK) ON ist.ItemStatusID = cir.ItemStatusID
INNER JOIN
    Polaris.Polaris.ItemRecordDetails ird WITH (NOLOCK) ON ird.ItemRecordID = cir.ItemRecordID
INNER JOIN
    Polaris.Polaris.PolarisUsers pu ON pu.PolarisUserID = ird.ModifierID
INNER JOIN
    Polaris.Polaris.PolarisUsers pubib on pubib.PolarisUserID = br.ModifierID

WHERE
    cir.ItemRecordID IN (
        SELECT
            ItemRecordID
        FROM
            Polaris.Polaris.ItemRecordSets WITH (NOLOCK)
        WHERE
            RecordSetID =   -- PUT YOUR RECORD SET ID NUMBER HERE
    )