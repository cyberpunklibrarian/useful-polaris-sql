/* This query will pull information about Collections actually in use at given branches and provide an
item count for those collections. */

SELECT
    DISTINCT cir.AssignedCollectionID AS [Collection ID],
    c.Name AS [Collection],
    c.Abbreviation AS [Abbreviation],
    o.Name AS [Library/Branch],
    COUNT(cir.ItemRecordID) AS [Item Count]

FROM
    Polaris.Polaris.CircItemRecords cir WITH (NOLOCK)

LEFT JOIN
    Polaris.Polaris.Collections c WITH (NOLOCK)
    ON (c.CollectionID = cir.AssignedCollectionID)
INNER JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK)
    ON (o.OrganizationID = cir.AssignedBranchID)

-- Uncomment and add OrganizationIDs to limit to given branches.
--WHERE cir.AssignedBranchID IN (1,2,3,4,5,6,7,8,9,10)

GROUP BY
    cir.AssignedCollectionID,
    c.Name,
    c.Abbreviation,
    o.Name
ORDER BY
    o.Name,
    c.Name