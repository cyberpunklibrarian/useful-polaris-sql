/*
Submitted by Gabrielle Gosselin at Richland Library (Columbia, SC)

The goal: find items where staff denied transit at CKI but didn't update the assigned branch to accurately reflect where the item physically was.
The report the query will go in is intended for staff to run, sort by CKI Location column, find the ones listed for their branch, and go look on 
the shelves for them and if found, fix the item record.
*/

SELECT
    cir.Barcode,
    br.BrowseTitle,
    ird.CallNumber,
    o.DisplayName AS [CKI Location],
    ab.DisplayName AS [Assigned Location]
FROM
    ItemRecordHistory irh (nolock)

JOIN
    CircItemRecords cir (nolock) on irh.ItemRecordID = cir.ItemRecordID
JOIN
    ItemRecordDetails ird (nolock) on irh.ItemRecordID = ird.ItemRecordID
JOIN
    BibliographicRecords br (nolock) on cir.AssociatedBibRecordID = br.BibliographicRecordID
JOIN
    Organizations o (nolock) on irh.OrganizationID = o.OrganizationID
join
    Organizations ab (nolock) on cir.AssignedBranchID = ab.OrganizationID

WHERE
    irh.ActionTakenID = 11
AND
    irh.OrganizationID <> irh.AssignedBranchID
AND
    irh.OrganizationID <> cir.AssignedBranchID
AND
    cir.ItemStatusID = 1
AND
    irh.TransactionDate = cir.ItemStatusDate
AND
    cir.ItemStatusDate = cir.CheckInDate
AND
    cir.Barcode not like 'econtent%'
AND
    irh.OrganizationID not in (19,22,17,20)
AND
    irh.ItemRecordID not in (
        SELECT DISTINCT
            cir.ItemRecordID 
        FROM
            ItemRecordHistory irh (nolock)
        JOIN
            CircItemRecords cir (nolock) on irh.ItemRecordID = cir.ItemRecordID
        WHERE
            irh.ActionTakenID = 27
        AND
            irh.OrganizationID = irh.AssignedBranchID
        AND
            irh.OrganizationID <> cir.AssignedBranchID
        AND
            cir.ItemStatusID = 1
        AND
            irh.TransactionDate > cir.ItemStatusDate
        AND
            cir.ItemStatusDate = cir.CheckInDate
        AND
            cir.Barcode not like 'econtent%'
    )