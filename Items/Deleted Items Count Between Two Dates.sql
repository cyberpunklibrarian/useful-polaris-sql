/* This is just a quick query that'll get you a count of items deleted between two given dates. */

SELECT
    o.Name AS [Library],
    COUNT(DISTINCT th.TransactionID) AS [Total]
FROM
    PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK)

LEFT OUTER JOIN
    PolarisTransactions.Polaris.TransactionDetails td WITH (NOLOCK) ON th.TransactionID = td.TransactionID
INNER JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK) on th.OrganizationID = o.OrganizationID

WHERE
    th.TransactionTypeID = 3007
AND
    td.TransactionSubTypeID = 38
AND
    th.TranClientDate BETWEEN '2020-01-01 00:00:00.000' and '2020-12-30 23:59:59.999'

GROUP BY
    o.Name
ORDER BY
    o.Name