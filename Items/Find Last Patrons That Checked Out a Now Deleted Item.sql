/* This query finds the last patrons to use an item that was deleted from the system. This is useful when you
find yourself looking at an item that may remain on a patron's account as an active replacement charge. */

SELECT
    pa.PatronID,
    p.Barcode,
    pr.PatronFullName,
    padir.Barcode,
    padir.BrowseTitle 

FROM
    Polaris.Polaris.PatronAccount pa WITH (NOLOCK)

JOIN
    Polaris.Polaris.PatronAcctDeletedItemRecords padir WITH (NOLOCK) ON pa.ItemRecordID = padir.ItemRecordID
JOIN
    Polaris.Polaris.PatronRegistration pr WITH (NOLOCK) ON pa.PatronID = pr.PatronID
JOIN
    Polaris.Polaris.Patrons p WITH (NOLOCK) ON p.PatronID = pr.PatronID

WHERE
    padir.Barcode IN ('36035108294152') -- Put your item barcode here

--AND pa.FeeReasonCodeID = -1 -- FeeReasonCodeID -1 is a replacement cost, can include this line optionally

ORDER BY
    pa.TxnDate DESC