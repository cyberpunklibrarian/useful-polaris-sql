/* This query pulls the complete item history for a given item record ID. The data is pulled from the
ItemRecordHistory table, so it may not be what you want for a full historical record as that table
is often set to clear information after a given amount of time. */

SELECT
    irh.ItemRecordHistoryID,
    irh.ItemRecordID,
    br.BrowseTitle,
    irh.TransactionDate,
    irha.ActionTakenDesc,
    irh.PatronID,
    pr.PatronFullName,
    o.Name
FROM
    Polaris.Polaris.ItemRecordHistory irh WITH (NOLOCK)
JOIN
    Polaris.Polaris.ItemRecords ir WITH (NOLOCK) ON ir.ItemRecordID = irh.ItemRecordID
JOIN
    Polaris.Polaris.BibliographicRecords br WITH (NOLOCK) ON br.BibliographicRecordID = ir.AssociatedBibRecordID
LEFT JOIN
    Polaris.Polaris.PatronRegistration pr WITH (NOLOCK) ON pr.PatronID = irh.PatronID
JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK) ON o.OrganizationID = irh.OrganizationID
JOIN
    Polaris.Polaris.ItemRecordHistoryActions irha WITH (NOLOCK) ON irha.ActionTakenID = irh.ActionTakenID
WHERE
    irh.ItemRecordID = 663 -- Place your ItemRecordID here
ORDER BY
    TransactionDate DESC