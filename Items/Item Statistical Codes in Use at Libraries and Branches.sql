/* This simple query pulls a list of item statistical codes in use at given libraries or throughout 
the library system. */


SELECT
    sc.OrganizationID AS [Organization ID],
    o.Name AS [Library/Branch],
    sc.StatisticalCodeID AS [Statistical Code ID],
    sc.Description AS [Stat Code Description]

FROM
    Polaris.Polaris.StatisticalCodes sc WITH (NOLOCK)

INNER JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK)
    ON (o.OrganizationID = sc.OrganizationID)

-- Uncomment the line below and add Organization IDs to limit by branch
--WHERE sc.OrganizationID IN (1,2,3,4,5)

ORDER BY
    o.Name,
    sc.[Description]
