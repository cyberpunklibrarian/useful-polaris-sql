/* If you've integrated your Polaris system with an eContent vendor, then patrons will be able to check out
eContent items via the PAC. We needed to get a circ count for items borrowed on the PAC and this was the
result. */

-- Happy little variables
DECLARE @StartDate DATETIME;
DECLARE @EndDate DATETIME;

-- Trying to be better about putting variables at the top so they're easy to find and set
SET @StartDate = '2022-05-01 00:00:00.000';
SET @EndDate = '2022-05-31 23:59:59.999';

SELECT
    ven.VendorName AS [Vendor],
    vendor.numValue AS [Vendor ID],
    COUNT(th.TransactionID) AS [PAC Circ Count]

FROM
    Polaris.Polaris.TransactionHeaders th WITH (NOLOCK)

INNER JOIN
    PolarisTransactions.Polaris.TransactionDetails paccheck WITH (NOLOCK) ON
    (th.TransactionID = paccheck.TransactionID AND paccheck.TransactionSubTypeID = 145 AND paccheck.numValue = 42)
INNER JOIN
    PolarisTransactions.Polaris.TransactionDetails vendor ON th.TransactionID = vendor.TransactionID AND vendor.TransactionSubTypeID = 300
INNER JOIN
    Polaris.Polaris.VendorAccounts va WITH (NOLOCK) ON va.VendorAccountID = vendor.numValue
INNER JOIN
    Polaris.Polaris.Vendors ven WITH (NOLOCK) ON ven.VendorID = va.VendorID

WHERE
    th.TranClientDate BETWEEN @StartDate AND @EndDate

GROUP BY
    ven.VendorName,
    vendor.numValue

ORDER BY
    [PAC Circ Count] DESC