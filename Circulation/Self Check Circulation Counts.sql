/* If you need to get self-check circ stats, but you don't know the WorkstationID of your self-check
machines, you can use this query to pull circ totals for each branch by specifically looking for the
TransactionSubTypeCodes that indicate self-check activity. */

SELECT
    o.Name AS [Branch],
    COUNT(th.TransactionID) AS [Self Checkouts]

FROM
    PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK)

INNER JOIN -- Specifically looking for the Checkout Type
    PolarisTransactions.Polaris.TransactionDetails td WITH (NOLOCK)
    ON (th.TransactionID = td.TransactionID AND td.TransactionSubTypeID = 145)
INNER JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK)
    ON (o.OrganizationID = th.OrganizationID)

WHERE
    th.TransactionTypeID = 6001
AND -- These apply to self-check activity
    td.numValue IN (12,22,24)
AND
    th.OrganizationID IN (
        SELECT OrganizationID
        FROM Polaris.Polaris.Organizations WITH (NOLOCK)
        -- The line below can be customized to include or exclude certain branches.
        --WHERE Name LIKE 'RCLS%'
    )
AND -- Adjust your dates accodingly
    th.TranClientDate BETWEEN '2022-12-01 00:00:00.000' AND '2022-12-31 23:59:59.999'

GROUP BY
    o.Name
ORDER BY
    o.Name