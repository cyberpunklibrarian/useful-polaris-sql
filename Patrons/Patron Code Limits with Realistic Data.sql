/* This query pulls the Patron Loan Limit table and replicate the layout you see in Polaris SA. However, the
output is limited to only the Patron Codes the library actually uses. */ 

SELECT
    o.Name AS "Organization",
    pc.Description AS "Patron Code",
    pll.MinFine AS "1st Level Fine Limit",
    pll.MaxFine AS "2nd Level Fine Limit",
    pll.TotalItems AS "Total Item Limit",
    pll.TotalOverDue AS "Total Overdue",
    pll.TotalHolds AS "Total Holds",
    pll.TotalILL AS "Total ILL",
    pll.TotalReserveItems AS "Total Reserve Items"

FROM
    Polaris.Polaris.PatronLoanLimits pll WITH (NOLOCK)

JOIN
    Polaris.Polaris.PatronCodes pc WITH (NOLOCK) ON pc.PatronCodeID = pll.PatronCodeID
JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK) ON o.OrganizationID = pll.OrganizationID
WHERE
    pll.OrganizationID IN (1,2,3,4,5) -- CHANGE AS NEEDED. VALUES SHOULD MATCH THROUGHOUT THE QUERY.

-- LIMIT TO PATRON CODES IN USE AT THAT LIBRARY
AND pll.PatronCodeID IN (
    SELECT DISTINCT(PatronCodeID)
    FROM Polaris.Polaris.Patrons WITH (NOLOCK)
    WHERE OrganizationID IN (1,2,3,4,5) -- CHANGE AS NEEDED. VALUES SHOULD MATCH THROUGHOUT THE QUERY.
)

ORDER BY
    o.Name,
    pc.[Description]