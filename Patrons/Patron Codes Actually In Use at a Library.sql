/* This query simply pulls a list of patron codes actually in use at a library
or branch along with a count of the number of patrons in those codes. It's not
all that unusual that a system may have a lot of codes but only a few are in
use at a given branch and, while adjusting the system, you only want to act
upon those codes actually in use. */

SELECT
    COUNT(p.PatronID) AS [Patron Count],
    p.PatronCodeID AS [Patron Code ID],
    pc.Description AS [Patron Code],
    o.Name AS [Library]

FROM
    Polaris.Polaris.Patrons p WITH (NOLOCK)
INNER JOIN
    Polaris.Polaris.PatronCodes pc WITH (NOLOCK) ON p.PatronCodeID = pc.PatronCodeID
INNER JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK) ON o.OrganizationID = p.OrganizationID

WHERE
    p.OrganizationID IN ()

GROUP BY
    p.PatronCodeID,
    pc.[Description],
    o.Name

ORDER BY
    [Patron Count] DESC