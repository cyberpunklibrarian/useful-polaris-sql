/* This query was written to prove a point. No matter how much you hide your item record and patron record
history, Polaris always retains check out histories in the PolarisTransactions database. This is a basic
query to pull all items checked out by a patron during a given period of time. */

SELECT
    pr.PatronFullName,
    br.BrowseTitle

FROM
    PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK)

INNER JOIN
    PolarisTransactions.Polaris.TransactionDetails td1 WITH (NOLOCK) ON td1.TransactionID = th.TransactionID
INNER JOIN
    PolarisTransactions.Polaris.TransactionDetails td2 WITH (NOLOCK) ON td2.TransactionID = th.TransactionID
INNER JOIN --SubTypeID 38 = The unique item record ID
    Polaris.Polaris.CircItemRecords cir WITH (NOLOCK) ON cir.ItemRecordID = td1.numValue AND td1.TransactionSubTypeID = 38
INNER JOIN --SubTypeID 6 = The unique patron record ID
    Polaris.Polaris.PatronRegistration pr ON pr.PatronID = td2.numValue AND td2.TransactionSubTypeID = 6
INNER JOIN
    Polaris.Polaris.BibliographicRecords br WITH (NOLOCK) ON cir.AssociatedBibRecordID = br.BibliographicRecordID

WHERE
    th.TransactionTypeID = 6001 --code for item checkout
AND
    td2.numValue = 000000 --put a patron ID here
AND -- Adjust dates as needed
    th.TranClientDate BETWEEN '2021-01-01 00:00:00.000' AND '2021-12-31 23:59:59.999'