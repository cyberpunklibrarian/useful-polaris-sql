/* Get a list of patrons with non-blocking notes. Pulls the patron ID, barcode, name, note text, and last updated date */

SELECT
    pn.PatronID AS "Patron ID",
    p.Barcode AS "Barcode",
    pr.PatronFullName AS "Name",
    pn.NonBlockingStatusNotes as "Notes",
    pn.NonBlockingStatusNoteDate AS "Last Noted"

FROM
    Polaris.Polaris.PatronNotes pn WITH (NOLOCK)

INNER JOIN
    Polaris.Polaris.Patrons p WITH (NOLOCK) ON p.PatronID = pn.PatronID
INNER JOIN
    Polaris.Polaris.PatronRegistration pr WITH (NOLOCK) ON pr.PatronID = pn.PatronID

WHERE
    DATALENGTH(pn.NonBlockingStatusNotes) > 2
    
ORDER BY
    pn.NonBlockingStatusNoteDate DESC