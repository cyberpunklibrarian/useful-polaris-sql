/* This query can be used as a basis for dealing with data in the PatronReadingHistory table. For
instance, it could be modified to search for the reading history of a specific patron, sets of patrons,
or to look for titles, etc. The original use of this file was to provide data as a CSV for import into
another ILS as a library migrated away from Polaris. */

SELECT
    prh.PatronID AS "Patron ID",
    p.Barcode AS "Patron Barcode",
    prh.ItemRecordID AS "Item Record ID",
    ir.Barcode AS "Item Barcode",
    br.BibliographicRecordID AS "Bib Record ID",
    br.BrowseTitle AS "Browse Title",
    br.BrowseAuthor AS "Browse Author",
    prh.CheckOutDate AS "Check Out Date",
    prh.LoaningOrgID AS "Loaning Org Code",
    o.Name AS "Loaning Library"

FROM
    Polaris.Polaris.PatronReadingHistory prh WITH (NOLOCK)

JOIN
    Polaris.Polaris.ItemRecords ir WITH (NOLOCK) ON ir.ItemRecordID = prh.ItemRecordID
JOIN
    Polaris.Polaris.BibliographicRecords br WITH (NOLOCK) ON br.BibliographicRecordID = ir.AssociatedBibRecordID
JOIN
    Polaris.Polaris.Patrons p WITH (NOLOCK) ON p.PatronID = prh.PatronID
JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK) ON prh.LoaningOrgID = o.OrganizationID

WHERE
    prh.ItemRecordID IS NOT NULL

ORDER BY
    prh.CheckOutDate DESC,
    prh.PatronID