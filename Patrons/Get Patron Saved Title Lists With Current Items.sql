/* This query pulls all of a patron's Saved Title lists from their PowerPAC account
and displays the current titles contained in each list. */

-- Drop your PatronID below
DECLARE @PatronID INT = 42244

SELECT
    DISTINCT br.BrowseTitle AS [Title],
    irrs.Name AS [Saved Title List]

FROM
    Polaris.Polaris.IRRecordStores irrs WITH (NOLOCK)

INNER JOIN
    Polaris.Polaris.IRRecords irr WITH (NOLOCK)
    ON (irr.RecordStoreID = irrs.RecordStoreID)
INNER JOIN
    Polaris.Polaris.IRRecordIndex irri WITH (NOLOCK)
    ON (irr.RecordStoreID = irri.RecordStoreID)
INNER JOIN
    Polaris.Polaris.IRCacheEntries irce WITH (NOLOCK)
    ON (irce.CacheEntryID = irr.CacheEntryID)
LEFT JOIN
    Polaris.Polaris.BibliographicRecords br WITH (NOLOCK)
    on (irce.LocalControlNumber = br.BibliographicRecordID)

WHERE
    irrs.AccountID = @PatronID
AND
    irri.AttributeTypeID = 2
ORDER BY
    irrs.Name,
    br.BrowseTitle