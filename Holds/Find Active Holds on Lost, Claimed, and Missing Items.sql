/* This query will find active holds on Lost, Claimed, and Missing items.

Contributed by: Derek Brown - Rochester Hills Public Library
*/


-- Youth Reports - Holds on Lost/Claimedx2/Missing
Delete from
    polaris.polaris.ItemRecordSets
WHERE
    RecordSetID in (1234) -- Put your Item RecordSetID here. This will clear previous records out of the record set.

Select
    distinct CIR.ItemRecordID into #tempdb -- Import data into temp before moving to recordset
from
    polaris.polaris.SysHoldRequests as SHR with (nolock)
join 
    polaris.polaris.CircItemRecords as CIR with (nolock) on SHR.BibliographicRecordID = CIR.AssociatedBibRecordID
join
    polaris.polaris.BibliographicRecords as BR with (nolock) on CIR.AssociatedBibRecordID = BR.BibliographicRecordID
where
    CIR.ItemStatusID in (7,8,9,10) -- Lost, Claimed Returned, Claimed Never Had, Missing
and
    SHR.SysHoldStatusID = 3 -- Hold Request Active
and CIR.AssignedCollectionID in () -- Limit to a given collection if needed

insert into polaris.polaris.ItemRecordSets (RecordSetID, ItemRecordID)
select
    1234, -- This should be the same RecordSetID as above
    itemrecordid
from
    #tempdb -- Insert into RecordSet

drop table #tempdb