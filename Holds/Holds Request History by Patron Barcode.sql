/* This generates a list of all the holds a patron has placed and what happened with them. 
NOTE: This query can take several minutes to run. It likely will not slow down the server
but once you execute the query, give it some time to finish. */

SELECT
    w.ComputerName,
    th.TranClientDate AS [Activity Date],
    th.TransactionTypeID,
    tt.TransactionTypeDescription AS [Activity Type],
    th.TransactionID,
    holdid.numValue AS [Hold ID],
    cir.Barcode AS [Item Barcode],
    br.BrowseTitle AS [Item Title],
    br2.BrowseTitle AS [Hold Title],
    br2.BibliographicRecordID AS [Hold Title ID]

FROM
    PolarisTransactions.Polaris.TransactionHeaders AS [th] WITH (NOLOCK)

INNER JOIN
    PolarisTransactions.Polaris.TransactionTypes AS [tt] WITH (NOLOCK) ON th.TransactionTypeID = tt.TransactionTypeID
INNER JOIN
    PolarisTransactions.Polaris.TransactionDetails AS [td] WITH (NOLOCK) ON th.TransactionID = td.TransactionID AND td.TransactionSubTypeID = '6'
LEFT OUTER JOIN
    PolarisTransactions.Polaris.TransactionDetails AS [td1] WITH (NOLOCK) ON th.TransactionID = td1.TransactionID AND td1.TransactionSubTypeID = '38'
LEFT OUTER JOIN
    Polaris.Polaris.CircItemRecords AS [cir] WITH (NOLOCK) ON td1.numValue = cir.ItemRecordID
LEFT OUTER JOIN
    Polaris.Polaris.BibliographicRecords AS [br] WITH (NOLOCK) ON cir.AssociatedBibRecordID = br.BibliographicRecordID
INNER JOIN
    Polaris.Polaris.Workstations AS [w] WITH (NOLOCK) ON th.WorkstationID = w.WorkstationID
LEFT OUTER JOIN
    PolarisTransactions.Polaris.TransactionDetails AS [holdid] WITH (NOLOCK) ON th.TransactionID = holdid.TransactionID AND holdid.TransactionSubTypeID ='233'
LEFT OUTER JOIN
    PolarisTransactions.Polaris.TransactionDetails AS [title] WITH (NOLOCK) ON th.TransactionID = title.TransactionID AND title.TransactionSubTypeID = '36'
LEFT OUTER JOIN
    Polaris.Polaris.BibliographicRecords AS [br2] WITH (NOLOCK) ON title.numValue = br2.BibliographicRecordID

WHERE
    td.numValue = (
        SELECT p.PatronID
        FROM Polaris.Polaris.Patrons AS [p] WITH (NOLOCK)
        WHERE p.Barcode =' ') -- Insert patron barcode here

AND
    tt.TransactionTypeDescription LIKE '%hold%'

ORDER BY
    th.TranClientDate