/* This is a quick and dirty query that pulls a list of branches that have hold pickup
exclusions assigned to them. */

SELECT
    o.Name AS [Library/Branch],
    e.Name AS [Excluded Branch]

FROM
    Polaris.Polaris.HoldsPickupBranchesExclude hpbe WITH (NOLOCK)

INNER JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK)
    ON (o.OrganizationID = hpbe.OrganizationID)
INNER JOIN
    Polaris.Polaris.Organizations e WITH (NOLOCK)
    ON (e.OrganizationID = hpbe.ExcludedBranchID)

WHERE
    hpbe.Exclude = 1

ORDER BY
    o.Name,
    e.Name
