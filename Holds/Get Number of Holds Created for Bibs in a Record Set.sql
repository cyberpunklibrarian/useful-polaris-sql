/* If you need to get some holds statistics for bibliographic records in a bib record set, this query should
help you out. You can adjust the TransactionTypeID to look for other holds/circ actviity as well. */


SELECT
    COUNT(th.TransactionID)
FROM
    PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK)
INNER JOIN
    PolarisTransactions.Polaris.TransactionDetails bib WITH (NOLOCK)
    ON (th.TransactionID = bib.TransactionID AND bib.TransactionSubTypeID = 36)
WHERE -- Adjust your dates as needed
    th.TranClientDate BETWEEN '2022-07-15 00:00:00.000' AND '2022-10-31 23:59:59.999'
AND -- 6005 = Hold Created, but this can be changed to suit your needs
    th.TransactionTypeID = 6005
AND
    bib.numValue IN (
        SELECT
            BibliographicRecordID
        FROM
            Polaris.Polaris.BibRecordSets WITH (NOLOCK)
        WHERE -- Drop in your bibliographic record set ID number below.
            RecordSetID = 3323)