# Useful Polaris SQL

## Useful Polaris SQL Website

Are you interested in these queries but not sure how to use git and/or GitLab? Would you rather have a more searchable website where you look for queries, view results, and copy what you need? Because that's something you can do!

If you're not into faffing around with git, try the [Innovative Users Group Forum - Polaris Clearinghouse Repository](https://forum.innovativeusers.org/c/clearinghouse-repository/polaris-repository/22)!

## Sharing SQL

I'm not a SQL expert.

But I use it everyday.

I've spoken with lots of Polaris ILS Administrators who wind up in their jobs because they're the "librarian who knows computers." At one library conference, that was literally the reason given to me by a young librarian for her promotion. The previous ILS Admin retired and, since she was the one that knew the most about computers, it was all "Congratulations on your promotion!"

Polaris uses Microsoft SQL Server on the back end, and its database architecture is standardised across installations. That makes it easy, and worthwhile, to share SQL code between librarians; especially those who might be new to Polaris and/or new to SQL. 

So that's what this is. I'll be adding queries here and there as I write them and revise them. I'll make sure the queries are safe, especially for the new folks. Many of them are just questions - a SELECT statement looking for data in return. There will be some queries that update the database, but those will be clearly marked. Queries that write information to the database should be taken *very* seriously and tried first on a testing/training server to make sure they work as intended before you *ever* let that code go near a production server.

Questions? Well, I'll do my best. As I said, I'm not an expert.

cyberpunklibrarian@protonmail.com

## Other Polaris SQL related collections

* Central Library Consortium (Ohio) - [BitBucket](https://bitbucket.org/clcdpc/sql/src/master/)
* Central Library Consortium (Ohio) - [Report Definitions](https://reportdefs.clcohio.org/)