# Supportal SQL Archive

This is a collection of Polaris SQL related posts, questions, and answers from the Polaris Supportal. Currently, it's a collection of separate files in Markdown and HTML format, one for each post. It needs some work to clean it up and it's not very searchable unless you clone/download this repo and search through it via your operating system's built in search features.

The mostly manual process for creating the initial archive is as follows:

* Searched for SQL queries
* Opened each post and checked to see if there were actually useful queries
* Clipped the page to [Joplin](https://joplinapp.org/)
* Exported the individual clips from Joplin to Markdown files

~Dan (Cyberpunk Librarian)