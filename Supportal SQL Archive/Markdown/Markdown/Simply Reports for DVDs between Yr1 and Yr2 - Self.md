Simply Reports for DVDs between Yr1 and Yr2 - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Simply Reports for DVDs between Yr1 and Yr2

0

46 views

Hello,

I was able to do a SQL search to get what i need, but I"m wondering if staff could do this themselves through Simply Reports?

Is there a way in SR to find:

DVDs with a Publication Date between 1950 and 1979.

These all use the MARCPubDateTwo field, but I'm not sure how I'd access that through SR, if anyone knows??

Thank you!

Tom

asked 3 years ago  by <img width="18" height="18" src="../../_resources/a3a87d9240fe43deaaa234397b64445e.jpg"/>  tbaxter@guelphpl.ca

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 3 years ago

<a id="upVoteAns_302"></a>[](#)

0

<a id="downVoteAns_302"></a>[](#)

Hi Tom.

I don't think that there is a filter in Simply Reports that will let you narrow by MarcPubDateTwo. A custom report may be your best bet if you would like your staff to be able to run it themselves.

One thing to note about MarcPubDateTwo is that the actual type of date in this field may vary, depending on coding in MARC 008/06. PubDateTwo is often the date that a film was originally released, but it may be the date the DVD was copyrighted. This may be more prevalent in DVDs cataloged in the early days of RDA, but older records may have copyright dates as well.

The MARCPublicationStatus field (char) may be helpful with this. If the value is 't' the date is likely to be a copyright date. If it is 'p' it could be the film's original release date.

Hope this helps,

JT

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a3a87d9240fe43deaaa234397b64445e.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_302"></a>[Accept as answer](#)

<a id="commentAns_302"></a>[Add a Comment](#)

<a id="upVoteAns_301"></a>[](#)

0

<a id="downVoteAns_301"></a>[](#)

Select br.BrowseTitle, br.MarcPubDateTwo
from Polaris.BibliographicRecords br with (nolock)
where PrimaryMARCTOMID = '33'
AND MarcPubDateTwo Between '1950' and '1979'

If I run a SR report using your options above, it only returns about 5-6 items, as this report pulls the publication year from that PublicationYear column.  But, the actual publication date is entered into the MarcPubDateTwo column in SQL.

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a3a87d9240fe43deaaa234397b64445e.jpg"/>  tbaxter@guelphpl.ca

- <a id="acceptAnswer_301"></a>[Accept as answer](#)

I see.  Sorry.  With any luck someone will show up who knows how to get SR to do what you need it to.

— jjack@aclib.us 3 years ago

Not a problem! Appreciate the help !Thank [you!](mailto:you@1)

— tbaxter@guelphpl.ca 3 years ago

<a id="commentAns_301"></a>[Add a Comment](#)

<a id="upVoteAns_300"></a>[](#)

0

<a id="downVoteAns_300"></a>[](#)

I tested this with a Bib List Report, using "Primary type of material" under "Bibliographic record general filters" and "MARC publication year" under "Bib date filters," and the results seemed accurate.  
The SQL it used to generate the report was 

select br.BrowseTitle,br.PublicationYear
from polaris.Polaris.BibliographicRecords br with (nolock)
where br.RecordOwnerID in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21)
and br.PrimaryMARCTOMID in (33)
and br.PublicationYear between 1950 and 1975

Is that about what you had?

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a3a87d9240fe43deaaa234397b64445e.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_300"></a>[Accept as answer](#)

<a id="commentAns_300"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Simply Reports Discrepancies](https://iii.rightanswers.com/portal/controller/view/discussion/960?)
- [Simply reports patron services](https://iii.rightanswers.com/portal/controller/view/discussion/355?)
- [Simply Reports - formatting when comments fields are included.](https://iii.rightanswers.com/portal/controller/view/discussion/128?)
- [Do reporting by subject headings?](https://iii.rightanswers.com/portal/controller/view/discussion/1059?)

### Related Solutions

- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [Run SimplyReports Scheduled Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930277327637)
- [Question about published SimplyReports reports](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989986518)
- [Question about a saved SimplyReports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930457380859)
- [Simply Reports session is locked](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930921070501)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)