Is there a report/way to find search terms people have used on the PAC? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=14)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [PAC](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=14)

# Is there a report/way to find search terms people have used on the PAC?

0

62 views

I'm looking to find out if I can run a report to find search terms people have used over a period of time. We're launching a new website and I've been hosting user tests. I found out in these tests that many patrons were going to the Library site and searching the catalog expecting to be also searching the Library website (we use a drop down search they need to change to website to search). I haven't found a solution to this UX issue, but I thought it could be something I monitor through a report. If I run a report and see terms that more likely a search intended for our website, it'd give me good data. But I haven't found a way to find "most searched words" or whatnot. 

asked 7 months ago  by <img width="18" height="18" src="../../_resources/e1dacfaa08d94e4ab630c2499b7fc9e5.jpg"/>  dreynolds@cityofcamas.us

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 7 months ago

<a id="upVoteAns_1062"></a>[](#)

0

<a id="downVoteAns_1062"></a>[](#)

In addition to the SQL, you'll also need to double check and make sure you have the various "Search..." Transaction logging elements enabled in Polaris System Adminitration.

answered 7 months ago  by <img width="18" height="18" src="../../_resources/e1dacfaa08d94e4ab630c2499b7fc9e5.jpg"/>  wosborn@clcohio.org

- <a id="acceptAnswer_1062"></a>[Accept as answer](#)

<a id="commentAns_1062"></a>[Add a Comment](#)

<a id="upVoteAns_1061"></a>[](#)

0

<a id="downVoteAns_1061"></a>[](#)

Someone posted this on the forums about 8 years ago.  Sorry, I don't know who to credit.

select COUNT(*) as 'Number of times searched', transactionstring as 'Search word'
from transactionheaders th with (nolock)
inner join transactiondetails td with (nolock) on (th.transactionid = td.transactionid)
inner join transactiondetailstrings tds with (nolock) on (td.numvalue = tds.transactionstringid)
where transactiontypeid in (1006,1007,1008,1009,1010,1011,1012,1013)
and transactionsubtypeid = 23
and tranclientdate between @From and @Through + ' 11:59:59 PM'
group by transactionstring
having COUNT(*) >25
order by COUNT(*) desc

You can change the second-to-last line if you want to change the threshold for inclusion on the report.

Incidentally, the top of ours is \[blank\] then various bib recordset numbers (like the on-order DVDs).

answered 7 months ago  by <img width="18" height="18" src="../../_resources/e1dacfaa08d94e4ab630c2499b7fc9e5.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_1061"></a>[Accept as answer](#)

<a id="commentAns_1061"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [PAC Widget Code](https://iii.rightanswers.com/portal/controller/view/discussion/319?)
- [Quick search on PAC is turning up deleted items, bibs with no items, and other things that should be suppressed. Is there a setting I'm overlooking?](https://iii.rightanswers.com/portal/controller/view/discussion/218?)
- [Can we change which formats appear first in PAC search results?](https://iii.rightanswers.com/portal/controller/view/discussion/193?)
- [\[How to fix\] 5.5+ PAC no longer returns to previous position in search results after placing hold](https://iii.rightanswers.com/portal/controller/view/discussion/161?)
- [Can a Limit By ignore articles like 'the' or 'an' using CQL?](https://iii.rightanswers.com/portal/controller/view/discussion/234?)

### Related Solutions

- [Search widget on library's website causes search term in the pac to be doubled.](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210211110701827)
- [Relevancy Ranking FAQ](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160503115746789)
- [How to determine the number of PAC searches within a specified time period using the find tool](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930699688701)
- [How is the popularity ranking of bibliographic records in the PAC determined?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930417865607)
- [Online Registration Defaults](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930782646108)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)