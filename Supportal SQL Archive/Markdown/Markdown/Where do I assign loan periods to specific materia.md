[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Where do I assign loan periods to specific material types? Ex. DVDs, New Books, CDs, etc.

0

116 views

Where do I find/change loan periods for specific materials? Ex. DVD, CD New Books, etc.

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_199abaf1c76642d38535818af12ae1cc.jpg"/> rhalterman@coralville.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 4 years ago

<a id="upVoteAns_798"></a>[](#)

0

<a id="downVoteAns_798"></a>[](#)

Answer Accepted

Technically you'd need to change loan period code settings rather than material type settings (though you may have your system setup to mirror the material/fine/loan code names).

The settings are for each branch/patron/loan period code. You can find them in system administration, Policy tables, Loan periods table.

Unless you only have on or two tiny changes to make, the updates are easier to perform via SQL and you may want to ask your site manager (support) for assistance.

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_199abaf1c76642d38535818af12ae1cc.jpg"/> wosborn@clcohio.org

<a id="commentAns_798"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Deleting Material Types using SQL](https://iii.rightanswers.com/portal/controller/view/discussion/323?)
- [Our library is extending our loan periods. Advice? Checklist?](https://iii.rightanswers.com/portal/controller/view/discussion/590?)
- [Do you use fine codes for items not related to a material type?](https://iii.rightanswers.com/portal/controller/view/discussion/359?)
- [In a consortium of 8 libraries, Is it possible to limit either a collection or material type by both the patron's home library and their hold pickup to branches within that library?](https://iii.rightanswers.com/portal/controller/view/discussion/262?)
- ["New Titles" on Website using API](https://iii.rightanswers.com/portal/controller/view/discussion/106?)

### Related Solutions

- [Receiving "Conversion failed when converting the varchar value ' V' to data type int." error when setting Special Loan Period for a specific branch](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170717102645744)
- [Limit PowerPAC results to print books only](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930693914524)
- [Checklist: Adding a New Material Type](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181212124422933)
- [Adding a Loan Period](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930906442286)
- [Patron Codes should have prevented checkouts based on Patron/Material Type Loan Limit Blocks table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930949436345)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)