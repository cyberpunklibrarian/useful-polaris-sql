[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [General Announcements](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=55)

# Job Posting - Library Application Support Manager Salt Lake County Libraries

0

31 views

**JOB SUMMARY**

Manages, plans, installs, configures and maintains software applications for the division.

&nbsp;

**MINIMUM QUALIFICATIONS**

&nbsp;

Bachelor's degree in Computer Science, Information Services, or another related field and four (4) years of related experience; of which one (1) year must have been supervisory; OR an equivalent combination of related education and experience. Education may not be substituted for the required supervisory experience.

&nbsp;

Preferred:

A Master of Library Science degree from an ALA accredited university

Working knowledge of SQL

Experience working in a Windows environment; Advanced knowledge of Microsoft Office Products;

Experience with Polaris integrated library system.

Experience with library software administration

&nbsp;

**ESSENTIAL FUNCTIONS**

*The following duties and responsibilities are intended to be representative of the work performed by the incumbent(s) in this position and are not all-inclusive. The omission of specific duties and responsibilities will not preclude it from the position.*

&nbsp;

*Applicants must be prepared to demonstrate the ability to perform the essential functions of the job with or without a reasonable accommodation.*

&nbsp;

- Oversees and administers library's ILS and other applications as assigned
- Plans, setup and configures all applications, modules and associated sub-systems; plans and executes all daily operations and regular maintenance of the applications
- Supervises staff which included hiring, promoting, orienting, training, assigning and reviewing work performance, annual work performance appraisal, and discipline.
- Provides support to internal teams, management, and vendors in identifying application needs and system requirements.
- Resolves complex issues with the library's core applications; this may include troubleshooting, collaboration with internal teams and project management
- Oversees integration and operation of application interfaces associated with the Integrated Library System for such services as self-checkout, automated sorting systems, online payments, PC management, online room reservations, and others.
- Develops documentation and procedures for support and/or training of new applications and technology products as required.
- Helps develop the annual technology budget, participates in the writing and evaluation of RFP and bids as directed
- Plans, designs, and participates in the implementation of complex IT systems.
- Collaborates with Web and IS team members to solve complex procedural, operational and technical problems.
- Articulates technical information and provides it to management and internal teams.
- Assists in continuity and disaster mitigation planning
- Researches and develops standards, proposals, and processes to prompt efficient use and increase productivity of applications in use by the library.

LocationWEST JORDAN LIBRARY - IT-1374 DivisionIS - Automation2500000201

Salary$69,594.00 to $104,391.00

Date OpenedMar 23, 2021 01:13 pm

Date ClosedApr 11, 2021 11:59 pm

Important Information

Environment  
Founded in 1939, Salt Lake County Library offers 17 neighborhood library branches with two new locations under construction, two reading rooms, services within two adult detention centers and a large event center. In 2019, the County Library connected with over 3.1 million individuals who visited one or more of our branches and/or the Library's Viridian Event Center, and over 4.1 million people visited our website. The County Library has just over 2 million diverse materials available for checkout and the materials were checked out just under 13 million times; of these, 191,399 were checked out by individuals receiving library services in Salt Lake County adult detention facilities. The community's interest in the collection, resources, programs, and social connectedness that the County Library provides is evidenced by the more than 37,000 applications for a new County Library card in 2019. **We offer a full range of health benefits as well as cash wellness incentives, a very robust retirement benefit and great work/life balance! For more information, please click here** [Salt Lake County Benefits](https://slco.org/human-resources/benefits/)Position TypeFull-Time

&nbsp;

Apply at - https://recruiting.adp.com/srccar/public/RTI.home?c=1210501&d=External&r=5000701748506&_fromPublish=true#/

&nbsp;

asked 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_34f7276a497349cea215c28aa4db310f.jpg"/> sglenn@slcolibrary.org

[polaris](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=7#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Job Posting - Librarian II Technical Services - Duluth Public Library](https://iii.rightanswers.com/portal/controller/view/discussion/1169?)
- [Job Posting - Branch Coordinator II - Mesa Public Library](https://iii.rightanswers.com/portal/controller/view/discussion/1056?)
- [Job Posting: Saskatchewan Information and Library Services Consortium - Integrated Library System Administrator (telecommute, 1 year term position)](https://iii.rightanswers.com/portal/controller/view/discussion/152?)
- [Sys Admin Job Posting-closes 3/6/17](https://iii.rightanswers.com/portal/controller/view/discussion/38?)
- [Job: Head of Materials Management (Darien, CT)](https://iii.rightanswers.com/portal/controller/view/discussion/1035?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Need to Add A Branch?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930881917383)
- [Library contact information on eReceipt](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930457677951)
- [Label Manager settings not saving](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930916508464)
- [How do I reset the year-to-date circulation counts?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930831368342)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)