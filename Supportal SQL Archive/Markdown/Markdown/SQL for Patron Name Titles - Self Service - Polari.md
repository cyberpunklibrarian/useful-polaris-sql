SQL for Patron Name Titles - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL for Patron Name Titles

0

26 views

Is there a SQL for finding patron accounts that have a Patron Name **Title** selected? TIA!

asked 1 year ago  by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg)  sbills@lpld.lib.in.us

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 1 year ago

<a id="upVoteAns_985"></a>[](#)

0

<a id="downVoteAns_985"></a>[](#)

Thank you both, so much!

answered 1 year ago  by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg)  sbills@lpld.lib.in.us

- <a id="acceptAnswer_985"></a>[Accept as answer](#)

<a id="commentAns_985"></a>[Add a Comment](#)

<a id="upVoteAns_983"></a>[](#)

0

<a id="downVoteAns_983"></a>[](#)

SELECT PatronID as RecordID

from PatronRegistration WITH (NOLOCK)

WHERE NameTitle IS NOT NULL

answered 1 year ago  by <img width="18" height="18" src="../../_resources/773c6fa9455f401f989a86bde78b987f.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_983"></a>[Accept as answer](#)

<a id="commentAns_983"></a>[Add a Comment](#)

<a id="upVoteAns_982"></a>[](#)

0

<a id="downVoteAns_982"></a>[](#)

I'm far from knowing much about SQL but I do have a statement for that one. I use it in the Find Tool to find particular titles. 

SELECT  PatronID as RecordID from PatronRegistration WITH (NOLOCK) WHERE NameTitle = 'Mrs.'

Replace the Mrs. for whichever title you want to find.

Someone else may have a better method.

One thing I really like about LEAP is the ability to remove that particular field from patron registration.

answered 1 year ago  by <img width="18" height="18" src="../../_resources/773c6fa9455f401f989a86bde78b987f.jpg"/>  csmiley@leegov.com

- <a id="acceptAnswer_982"></a>[Accept as answer](#)

<a id="commentAns_982"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)
- [Patrons with system block](https://iii.rightanswers.com/portal/controller/view/discussion/338?)
- [SQL for patron checkouts at a specific location](https://iii.rightanswers.com/portal/controller/view/discussion/269?)
- [Does anyone have an SQL for patrons whose notifications settings are listed as "none"?](https://iii.rightanswers.com/portal/controller/view/discussion/621?)
- [SQL for patron circ at a particular workstation](https://iii.rightanswers.com/portal/controller/view/discussion/918?)

### Related Solutions

- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [How do I find a deleted patron in SQL?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930727145951)
- [Error: GetPatronPreCirc() failed](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170922112726384)
- [Search patron reading history within the staff client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930708077324)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)