Hello, Does anyone have a Sql search query to check the 245 field  - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Hello, Does anyone have a Sql search query to check the 245 field

0

65 views

Sql search query to check the 245 field with 14,12, or 13 indicator? I want to do some clean-up on some records that would be hidden with the wrong indicators.

Thank you in advanced.

asked 2 years ago  by ![treel@pauldingcountylibrary.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_treel@pauldingcountylibrary.org20191218161844.jpg)  treel@pauldingcountylibrary.org

<a id="comment"></a>[Add a Comment](#)

## 5 Replies   last reply 2 years ago

<a id="upVoteAns_600"></a>[](#)

0

<a id="downVoteAns_600"></a>[](#)

Answer Accepted

Will this work? It's based on a SQL that I use to check on the second indiactors of 856 fields. It may not be the most elegant soution but on a trial run it seemed to work in our catalog.

SELECT DISTINCT bibliographicTAGS.bibliographicRECORDID AS RECORDID
  FROM bibliographicTAGS (nolock), bibliographicSUBFIELDS (nolock)
 WHERE bibliographicTAGS.bibliographicTAGID=bibliographicSUBFIELDS.bibliographicTAGID
  AND bibliographicTAGS.TAGNUMBER=245
  AND bibliographicTAGS.INDICATORONE=1
INTERSECT
SELECT DISTINCT bibliographicTAGS.bibliographicRECORDID AS RECORDID
  FROM bibliographicTAGS (nolock), bibliographicSUBFIELDS (nolock)
 WHERE bibliographicTAGS.bibliographicTAGID=bibliographicSUBFIELDS.bibliographicTAGID
  AND bibliographicTAGS.TAGNUMBER=245
  AND bibliographicTAGS.INDICATORTWO=3

answered 2 years ago  by <img width="18" height="18" src="../../_resources/a6aac6ad23a7474eaefdca57ee0f4007.jpg"/>  amihelich@wccls.org

<a id="commentAns_600"></a>[Add a Comment](#)

<a id="upVoteAns_602"></a>[](#)

0

<a id="downVoteAns_602"></a>[](#)

Will try both!! I know how to use SQL just not create a query. So thank you so soo much!!!

To bad its Friday.. (kinda). I'll have to play with this on Monday.

answered 2 years ago  by ![treel@pauldingcountylibrary.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_treel@pauldingcountylibrary.org20191218161844.jpg)  treel@pauldingcountylibrary.org

- <a id="acceptAnswer_602"></a>[Accept as answer](#)

<a id="commentAns_602"></a>[Add a Comment](#)

<a id="upVoteAns_601"></a>[](#)

0

<a id="downVoteAns_601"></a>[](#)

Hi.

Here's a query that I think will give you what you want.  It looks for BrowseTitles beginning with a string where the filing indicator in the 245 is not correct.

If you want to run it in the Find Tool, just remove the second line (",br.browsetitle").

You will need to adjust the last two lines for different leading articles and nonfiling characters. (Don't forget to look for foreign language articles such as El, La, Le, L', etc.)

Hope this helps.

JT

SELECT br.BibliographicRecordID
, br.BrowseTitle
FROM Polaris.Polaris.BibliographicRecords br WITH (NOLOCK)
JOIN Polaris.Polaris.BibliographicTags bt (NOLOCK)
ON (br.BibliographicRecordID = bt.BibliographicRecordID and bt.TagNumber = 245)
WHERE
br.recordstatusid = 1
AND bt.IndicatorTwo <> 2
AND br.BrowseTitle LIKE 'a %'

answered 2 years ago  by <img width="18" height="18" src="../../_resources/a6aac6ad23a7474eaefdca57ee0f4007.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_601"></a>[Accept as answer](#)

Wow, we're really on the ball this afternoon. 3 replies and 2 queries in 10 minutes. Go us! ![laughing](../../_resources/8ef25eb9003241daadfe5d1f343ae75d.gif)

JT

— jwhitfield@aclib.us 2 years ago

<a id="commentAns_601"></a>[Add a Comment](#)

<a id="upVoteAns_599"></a>[](#)

0

<a id="downVoteAns_599"></a>[](#)

Wrong indicator in 245. I have recently come across some records that shouldn't have a 245 field with a 14 indicator.

or 245 that shoud have a 14 indicator.

Especially while doing original cataloging was done.

answered 2 years ago  by ![treel@pauldingcountylibrary.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_treel@pauldingcountylibrary.org20191218161844.jpg)  treel@pauldingcountylibrary.org

- <a id="acceptAnswer_599"></a>[Accept as answer](#)

<a id="commentAns_599"></a>[Add a Comment](#)

<a id="upVoteAns_598"></a>[](#)

0

<a id="downVoteAns_598"></a>[](#)

Check it for what?

answered 2 years ago  by <img width="18" height="18" src="../../_resources/a6aac6ad23a7474eaefdca57ee0f4007.jpg"/>  sgrant@somd.lib.md.us

- <a id="acceptAnswer_598"></a>[Accept as answer](#)

<a id="commentAns_598"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for items withdrawn by a particular staff member](https://iii.rightanswers.com/portal/controller/view/discussion/809?)
- [alternate title/author fields](https://iii.rightanswers.com/portal/controller/view/discussion/304?)
- [Inputting and creating links to 880 fields](https://iii.rightanswers.com/portal/controller/view/discussion/545?)
- [Formatting date information in volume field to match requests](https://iii.rightanswers.com/portal/controller/view/discussion/36?)
- [How do you enter/search for titles with special characters?](https://iii.rightanswers.com/portal/controller/view/discussion/320?)

### Related Solutions

- [What does the 3M Novelist Select Job do?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930932903822)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [What MARC fields are searched in "Any field" keyword searches?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930441999288)
- [How can I locate bibs that were generated during a Terminated import without using SQL Server Management Studio?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930570115337)
- [Differences in Find Tool search results between Normal and Power searching](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930709525268)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)