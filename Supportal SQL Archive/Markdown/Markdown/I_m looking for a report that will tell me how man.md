I'm looking for a report that will tell me how many bib records a staff member has created and/or modified in a month. - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# I'm looking for a report that will tell me how many bib records a staff member has created and/or modified in a month.

0

34 views

Each month I have to report how many items have been catalogued.  I have this broken down by each cataloger, however I count the bib records instead of the items due to multiple copies.  Is there a report that I can run for each staff member that can give me the break down of each stat code and the quantity they catalogued that month?

asked 6 months ago  by <img width="18" height="18" src="../../_resources/53befe12f63b43ebb66607d9b0827aca.jpg"/>  cedwards@lincolncounty.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 6 months ago

<a id="upVoteAns_1068"></a>[](#)

0

<a id="downVoteAns_1068"></a>[](#)

I'm not sure if there's an existing report that will give you that info, but this sql should work:

`declare @startDate datetime = '1/1/2021' -- starts at beginning of this day, 00:00:00`
`declare @endDate datetime = '1/31/2021' -- goes through the end of this day, 23:59:59`

`-------------------------------------------------------`
`set @endDate = dateadd(second, 86399, @endDate)`

`select pu.Name [User]`
`,sc.Description [StatCode]`
`,count(distinct br.BibliographicRecordID) [NumTitles]`
`,count(distinct cir.ItemRecordID) [NumItems]`
`from Polaris.Polaris.BibliographicRecords br`
`join Polaris.Polaris.PolarisUsers pu`
`on pu.PolarisUserID = br.CreatorID`
`join Polaris.Polaris.CircItemRecords cir`
`on cir.AssociatedBibRecordID = br.BibliographicRecordID`
`join Polaris.Polaris.StatisticalCodes sc`
`on sc.StatisticalCodeID = cir.StatisticalCodeID and sc.OrganizationID = cir.AssignedBranchID`
`where br.CreationDate between @startDate and @endDate`
`group by pu.Name, sc.Description`
`order by pu.Name, sc.Description`

answered 6 months ago  by <img width="18" height="18" src="../../_resources/53befe12f63b43ebb66607d9b0827aca.jpg"/>  mfields@clcohio.org

- <a id="acceptAnswer_1068"></a>[Accept as answer](#)

<a id="commentAns_1068"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [I'm looking for a SQL report that will tell how long items are in held status.](https://iii.rightanswers.com/portal/controller/view/discussion/495?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [I need to pull OCLC numbers from records with a deleted status to batch update our OCLC holdings. I'm able to get a report with: Bib record ID, Title, and Bib record status, but the column for MARC OCLC number is blank. Why doesn't it show up?](https://iii.rightanswers.com/portal/controller/view/discussion/829?)
- [Creating a dropdown menu for a parameter so that it works in Polaris client reports](https://iii.rightanswers.com/portal/controller/view/discussion/769?)

### Related Solutions

- [Move reporting services subscriptions to another user](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930313777401)
- [Errors accessing reports in the client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930369939404)
- [Can a report created in SimplyReports be made available to staff without access to SimplyReports?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930241316369)
- [Accessing Report Builder to create a custom report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930999383964)
- [Statistical Summary Report Bin Record Totals](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930459959777)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)