[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Automatic update of patron/guardian?

0

53 views

Hi All,

We use one of our User Defined Fields to house the name(s) of the parent/guardian of patrons who are under 18.  Does anyone have a policy/procedure to remove that informaiton when a patron turns 18?

Thanks!

\- Robin Dye

asked 6 years ago by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG) rdye@krl.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 6 years ago

<a id="upVoteAns_364"></a>[](#)

0

<a id="downVoteAns_364"></a>[](#)

Hi Gabrielle,

We'd love to have that query.  Thanks for offering!

Robin

[rdye@krl.org](mailto:rdye@krl.org)

answered 6 years ago by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG) rdye@krl.org

- <a id="acceptAnswer_364"></a>[Accept as answer](#)

<a id="commentAns_364"></a>[Add a Comment](#)

<a id="upVoteAns_361"></a>[](#)

0

<a id="downVoteAns_361"></a>[](#)

Hi Robin,

We do the same thing with one of our UDFs but we don't have a library policy on removing the information when the patron "ages out".  I do have an automated SQL job that ages our patrons through our statistical age groups (a different UDF field) that you could modify to automatically wipe the parent/guardian field if that what you decide to do.  Just send me an email if you're interested in the query!

Gabrielle

answered 6 years ago by ![ggosselin@richlandlibrary.com](https://iii.rightanswers.com/portal/app/images/custom/avatar_ggosselin@richlandlibrary.com20170530111420.JPG) ggosselin@richlandlibrary.com

- <a id="acceptAnswer_361"></a>[Accept as answer](#)

<a id="commentAns_361"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Automatically Check In Items](https://iii.rightanswers.com/portal/controller/view/discussion/1064?)
- [Interlibrary Loan and Automatic Renewals](https://iii.rightanswers.com/portal/controller/view/discussion/485?)
- [Questions about automatic waiving of lost fees](https://iii.rightanswers.com/portal/controller/view/discussion/826?)
- [Does anyone know how to batch update the patron code field?](https://iii.rightanswers.com/portal/controller/view/discussion/886?)
- [Patron expiration date update](https://iii.rightanswers.com/portal/controller/view/discussion/727?)

### Related Solutions

- [Items not auto-renewed for specific patron](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930760925871)
- [Set all current patron expiration dates to the future to remove the expired patron block](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930981683916)
- [Auto-Renew basics and FAQ](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930279224783)
- [Missing item never made the automatic transition to Withdrawn even though it met the date criteria](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=211207090135203)
- [Replacement fines were not automatically waived after overdue paid](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930589018439)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)