[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Does anyone have a SQL query to get circulation statistics for material with the subject heading Historical Fiction?

0

62 views

I have a librarian who would like the lifetime circulation statistics for material with a subject heading of Historical Fiction. 

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_0db2822cfcd54e05b633eddd8943e1c5.jpg"/> scole@slcolibrary.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_1224"></a>[](#)

0

<a id="downVoteAns_1224"></a>[](#)

You're awesome! Thank you so much![smile](../_resources/smiley-smile_6e612f894ac84b73a01bec7f01af3141.gif)

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_0db2822cfcd54e05b633eddd8943e1c5.jpg"/> scole@slcolibrary.org

- <a id="acceptAnswer_1224"></a>[Accept as answer](#)

You're welcome!  If it doesn't work for you, or if you need it tweaked, please let me know and I'll see what I can do.

— jjack@aclib.us 2 years ago

<a id="commentAns_1224"></a>[Add a Comment](#)

<a id="upVoteAns_1223"></a>[](#)

0

<a id="downVoteAns_1223"></a>[](#)

Sorry, I posted that at the end of the day while talking to someone, forgetting the definition of multitasking as "doing several things badly at the same time."

This should work, but currently it just pulls *everything* which shows in the PAC (not limiting by format/audience/stat code/etc.):

SELECT DISTINCT bsi.BibliographicRecordID, br.BrowseCallNo, br.BrowseTitle, br.BrowseAuthor, br.LifeTimeCircCount  
FROM BibSubjectIndices bsi with (NOLOCK)  
JOIN MainSubjectHeadings msh with (NOLOCK)  
ON bsi.MainSubjectHeadingID = msh.MainSubjectHeadingID  
JOIN BibliographicRecords br with (NOLOCK)  
ON bsi.BibliographicRecordID = br.BibliographicRecordID  
WHERE BrowseSubject LIKE '%Historical Fiction%'  
AND br.DisplayInPAC = '1'  
ORDER BY br.BrowseCallNo, br.BrowseTitle, br.BrowseAuthor

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_0db2822cfcd54e05b633eddd8943e1c5.jpg"/> jjack@aclib.us

- <a id="acceptAnswer_1223"></a>[Accept as answer](#)

<a id="commentAns_1223"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Deleting Material Types using SQL](https://iii.rightanswers.com/portal/controller/view/discussion/323?)
- [Query Syntax for a Linked Server](https://iii.rightanswers.com/portal/controller/view/discussion/491?)
- [Does anyone have a SQL to find out hold information on integrated e-content?](https://iii.rightanswers.com/portal/controller/view/discussion/684?)
- [Do you use fine codes for items not related to a material type?](https://iii.rightanswers.com/portal/controller/view/discussion/359?)
- [Welcome emails for new patrons. Does anyone do this?](https://iii.rightanswers.com/portal/controller/view/discussion/307?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [SQL query for a count of items by material type](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930506564972)
- [What does the 3M Novelist Select Job do?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930932903822)
- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)
- [Export from old Supportal of forum posts with SQL queries](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161214213310321)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)