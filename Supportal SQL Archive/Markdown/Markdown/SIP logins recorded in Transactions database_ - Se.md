[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SIP logins recorded in Transactions database?

0

58 views

Hi, I've been asked to look into how to see a count of unique customer we have had in a given timeframe.

I can pull users with physical checkouts or holds placed pretty easily from the Transactions table, but I can't seem to find anywhere in that table where SIP checks are recorded. This would capture Overdrive logins (& some other database activity), branch PC logins and photocopier use. I know this activty resets the customer's "Last Activity Date" in the patron record, but is there a way to access it for a specified time period. (last active would make this really easy if we were looking at the last 365 days as opposed to the fiscal year)

asked 10 months ago by <img width="18" height="18" src="../_resources/default-avatar_2b6796838cbe4ff1a9c46008e683c141.jpg"/> dustin.booher@fairfaxcounty.gov

[sip](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=63#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults) [transactions](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=17#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 11 days ago

<a id="upVoteAns_1453"></a>[](#)

0

<a id="downVoteAns_1453"></a>[](#)

This is something we would like to be able to do as well. Did you ever figure it out? Thanks! 

answered 11 days ago by <img width="18" height="18" src="../_resources/default-avatar_2b6796838cbe4ff1a9c46008e683c141.jpg"/> jplpolaris@coj.net

- <a id="acceptAnswer_1453"></a>[Accept as answer](#)

<a id="commentAns_1453"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [CollectionHQ Transaction and Holds Data Extracts](https://iii.rightanswers.com/portal/controller/view/discussion/358?)
- [Anyone have an SQL code to create a monthly report of all waived transactions by all staff members? Or know how to generate it in Simply Reports](https://iii.rightanswers.com/portal/controller/view/discussion/1193?)
- [Last modifier of patron record](https://iii.rightanswers.com/portal/controller/view/discussion/1048?)
- [SQL for changes to a patron record](https://iii.rightanswers.com/portal/controller/view/discussion/840?)
- [SQL query to export specific record set: Polaris 4.1R2](https://iii.rightanswers.com/portal/controller/view/discussion/93?)

### Related Solutions

- [Patrons are unable to login to Envisionware](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930708745356)
- [How can I find all the transaction types and subtypes used in Polaris?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930580777417)
- [Slowness with SQL related transactions](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930842076787)
- [Leap Circulations in Transactions Database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930587079848)
- [Checkouts by Anonymous OPAC Workstation](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930335320406)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)