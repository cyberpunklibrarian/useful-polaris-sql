Anyone have a sql script that will total fines for a year and then break them down by patron code? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Anyone have a sql script that will total fines for a year and then break them down by patron code?

0

47 views

I'm looking for a sql script that will tally how many fines were accumulated in a year and then break them down by patron code. 

For example: 

Patron Code:

Child   1,153

Adult 12,456

Staff  959.00

etc, 

Thanks. 

Lori 

asked 1 year ago  by <img width="18" height="18" src="../../_resources/f76d7e806b4947e78050d6a25273c30e.jpg"/>  lori@esrl.org

[fines](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=130#t=commResults) [patron code](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=330#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 1 year ago

<a id="upVoteAns_835"></a>[](#)

0

<a id="downVoteAns_835"></a>[](#)

I'm looking for Fines charged (overdue item) within a year broken out by patron code.  The fines and fees in the canned report would be fine but I need it broken up by patron code. 

answered 1 year ago  by <img width="18" height="18" src="../../_resources/f76d7e806b4947e78050d6a25273c30e.jpg"/>  lori@esrl.org

- <a id="acceptAnswer_835"></a>[Accept as answer](#)

<a id="commentAns_835"></a>[Add a Comment](#)

<a id="upVoteAns_834"></a>[](#)

0

<a id="downVoteAns_834"></a>[](#)

See if either of these is what you are looking for:

--Fines Charged
SELECT PC.Description, SUM(PA.TxnAmount)
FROM PatronAccount PA (NOLOCK)
JOIN Patrons P (NOLOCK)
ON P.PatronID = PA.PatronID
JOIN PatronCodes PC (NOLOCK)
ON PC.PatronCodeID = P.PatronCodeID
WHERE PA.TxnCodeID = 1
AND PA.FeeReasonCodeID = 0
AND PA.TxnDate BETWEEN '2019-01-01' AND '2020-01-01'
GROUP BY PC.Description
ORDER BY PC.Description

--Fines Collected
SELECT PC.Description, SUM(PA.TxnAmount)
FROM PatronAccount PA (NOLOCK)
JOIN Patrons P (NOLOCK)
ON P.PatronID = PA.PatronID
JOIN PatronCodes PC (NOLOCK)
ON PC.PatronCodeID = P.PatronCodeID
WHERE PA.TxnCodeID = 2
AND PA.FeeReasonCodeID = 0
AND PA.TxnDate BETWEEN '2019-01-01' AND '2020-01-01'
GROUP BY PC.Description
ORDER BY PC.Description

Rex

answered 1 year ago  by <img width="18" height="18" src="../../_resources/f76d7e806b4947e78050d6a25273c30e.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_834"></a>[Accept as answer](#)

This is what I needed.  Thanks Rex!

— lori@esrl.org 1 year ago

<a id="commentAns_834"></a>[Add a Comment](#)

<a id="upVoteAns_833"></a>[](#)

0

<a id="downVoteAns_833"></a>[](#)

Are you looking for Total Overdue Fines or other charges as well and are you looking for Fines Charged or Collected?

Rex Helwig

answered 1 year ago  by <img width="18" height="18" src="../../_resources/f76d7e806b4947e78050d6a25273c30e.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_833"></a>[Accept as answer](#)

<a id="commentAns_833"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [SQL - looking for a report on how many fines we have for a specific collection code and how many were waived.](https://iii.rightanswers.com/portal/controller/view/discussion/673?)
- [SQL for Overdue fines](https://iii.rightanswers.com/portal/controller/view/discussion/543?)
- [Does anyone have a sql script for a monthly in-house check-in count?](https://iii.rightanswers.com/portal/controller/view/discussion/615?)

### Related Solutions

- [Make it so Staff are not charged fines](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930461837850)
- [Patron / Material Type Loan Limit Blocks policy table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930651711850)
- [Find patrons with a certain amount of existing fines/fees](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181016133058447)
- [Fee reason display order in Leap](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930612692592)
- [Collection Agency SQL Job Not Running](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930837487784)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)