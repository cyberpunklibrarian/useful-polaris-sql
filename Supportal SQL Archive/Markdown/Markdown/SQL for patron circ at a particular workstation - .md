SQL for patron circ at a particular workstation - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL for patron circ at a particular workstation

0

24 views

Does anybody have a SQL to find out the number of unique patron barcodes that checked out items from a particular workstation during a specific time period? We're trying to find out how many different patrons used curbside pickup during October through December. Thanks!

asked 8 months ago  by <img width="18" height="18" src="../../_resources/735ee8b5b389459ebc2cfcee1d6183bb.jpg"/>  musack@bcls.lib.nj.us

[circulation](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=185#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 8 months ago

<a id="upVoteAns_1050"></a>[](#)

0

<a id="downVoteAns_1050"></a>[](#)

Thank you so much!! I really appreciate your help, and will certainly let you know if I need anything else. I'm so glad there are SQL smart people on this forum :)

answered 8 months ago  by <img width="18" height="18" src="../../_resources/735ee8b5b389459ebc2cfcee1d6183bb.jpg"/>  musack@bcls.lib.nj.us

- <a id="acceptAnswer_1050"></a>[Accept as answer](#)

<a id="commentAns_1050"></a>[Add a Comment](#)

<a id="upVoteAns_1049"></a>[](#)

0

<a id="downVoteAns_1049"></a>[](#)

Hi-

This query should get you a list of unique patrons. Just make sure to chage the WorkstationID in the last line:

`Select distinct TD.numValue`
`From PolarisTransactions.Polaris.TransactionHeaders TH (nolock)`
`JOIN PolarisTransactions.Polaris.TransactionDetails TD (nolock)`
`ON TH.TransactionID = TD.TransactionID`
`AND TD.TransactionSubTypeID = 6`
`WHERE TH.TransactionTypeID = 6001`
`AND TH.TranClientDate between '10/1/2020' and '1/1/2021'`
`AND TH.WorkstationID IN (99)`

It's using the transaction log, so it may take a bit of time depending on the size of your system.

Let me know if you run into any issues or need anything else.

answered 8 months ago  by <img width="18" height="18" src="../../_resources/735ee8b5b389459ebc2cfcee1d6183bb.jpg"/>  mhammermeister@pinnaclelibraries.org

- <a id="acceptAnswer_1049"></a>[Accept as answer](#)

<a id="commentAns_1049"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)
- [SQL for Patron Name Titles](https://iii.rightanswers.com/portal/controller/view/discussion/836?)
- [Patrons with system block](https://iii.rightanswers.com/portal/controller/view/discussion/338?)
- [SQL for patron checkouts at a specific location](https://iii.rightanswers.com/portal/controller/view/discussion/269?)
- [Does anyone have an SQL for patrons whose notifications settings are listed as "none"?](https://iii.rightanswers.com/portal/controller/view/discussion/621?)

### Related Solutions

- [Patron Code for Computer / Internet Use](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930652657234)
- [How do I schedule the Year End Circ Count Rollover job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930582726474)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [How do I find a deleted patron in SQL?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930727145951)
- [Error: GetPatronPreCirc() failed](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170922112726384)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)