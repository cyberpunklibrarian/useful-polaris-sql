[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SimplyReports - Relationship designator makes sorting inventory more difficult for volunteers

0

49 views

Our catalog is mixed with AACR2 and RDA -- when I run inventory reports in SimplyReports, the "$e author" is being included in the sort of MARC authors and makes our lists wonky. This has been confusing our elderly volunteers who expect them to be in order as they are on the shelf. Is there a way to work around this? Or am I looking at having to add the $e to all of our authors?

asked 4 years ago by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg) sbills@lpld.lib.in.us

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 4 years ago

<a id="upVoteAns_749"></a>[](#)

0

<a id="downVoteAns_749"></a>[](#)

Your last suggestions best suits my skills. ;-) Thanks, JT!

answered 4 years ago by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg) sbills@lpld.lib.in.us

- <a id="acceptAnswer_749"></a>[Accept as answer](#)

<a id="commentAns_749"></a>[Add a Comment](#)

<a id="upVoteAns_748"></a>[](#)

0

<a id="downVoteAns_748"></a>[](#)

I don't think that there is a way in Simply Reports, since both versions (MARC and Sort) include multiple subfields in the 100/110 field.

It would be possible to create a custom report that just uses subfield a of these tags for the author but it requires some SQL and Report Builder knowledge, as well as the permissions to load the report onto your report server.

Another suggestion would be to use Find and Replace in Excel to remove the relator words and then re-sort the results before printing. You may need to remove the space before the word to make sure things sort out the way you want.

JT

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_a13f9e92e8544bf8a09caa97b3edc29d.jpg"/> jwhitfield@aclib.us

- <a id="acceptAnswer_748"></a>[Accept as answer](#)

<a id="commentAns_748"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Well, there's no "inventory" option for subject, and I don't see a lot of activity on the forums, but here goes: anybody using any sort of "live" wand/scanner for inventory? We're tired of CircTrak--saving barcodes/tweaking files/uploading.](https://iii.rightanswers.com/portal/controller/view/discussion/1089?)
- [Where can I find the SimplyReports User Guide?](https://iii.rightanswers.com/portal/controller/view/discussion/142?)
- [Report for patrons with items out. If possible, sort by location?](https://iii.rightanswers.com/portal/controller/view/discussion/777?)
- [SimplyReports or SQL to get circ count by a UDF?](https://iii.rightanswers.com/portal/controller/view/discussion/88?)
- [SimplyReports or SQL to get circ count by a UDF?](https://iii.rightanswers.com/portal/controller/view/discussion/82?)

### Related Solutions

- [Polaris Inventory Manager Guide 4.1R2](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160504101532134)
- [Cannot move Vital collection from one community (or collection) to another](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180405162957862)
- [IUG 2024: A Day in the Life of Product Design](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240418200428427)
- [IUG 2019: Designing for Usability](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=190521111412255)
- [Polaris Inventory Manager Installation 4.1R2](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160504101956110)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)