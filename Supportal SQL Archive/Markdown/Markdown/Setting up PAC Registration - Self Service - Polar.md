[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [PAC](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=14)

# Setting up PAC Registration

0

109 views

I'm looking into setting up PAC registration and have thought of a few questions.

1) Did you create a specific Patron Code to set as the default? i.e. Online Registration

2) Is there a way to only allow zip codes in our service area to register so we're not getting people from all over the country? If not, how did you handle these?

3) What expiration/address term did you give? We usually give 3 years, would we give like 3 months instead? Then those who haven't picked up are purged? Or how often do you purge?

&nbsp;

Any other specifics about how you set yours up would be so helpful! Bonus points for screen shots of your settings in Patron Acess Options  :-)

Thank you!

asked 2 years ago by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg) sbills@lpld.lib.in.us

[pac registration](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=488#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_1245"></a>[](#)

2

<a id="downVoteAns_1245"></a>[](#)

At San Diego PL we use the patron Code "Online Registration" and it expires after one month. Patrons are advised that they need to visit the library within that month to obtain a permanent library card. Online Registration accounts have no checkout privileges and are limited to two hold requests. 

On a weekly basis I run the following SQL search for Online Registration accounts that have been expired for more than seven days and then purge them:

SELECT pr.PatronID  
FROM PatronRegistration pr (nolock)  
JOIN Patrons p (nolock)  
ON p.PatronID = pr.PatronID  
WHERE p.PatronCodeID =14  
AND pr.ExpirationDate < DATEADD(dd, -7, GETDATE())  
AND p.Barcode LIKE 'PACREG%'

From time to time staff will issue a library card but forget to update the patron code to one that has borrowing privileges (Adult, Juvenile, etc.). The last line in the query above helps ensure that this staff error does not result in a library card being deleted.

Hope that helps!

Phil

answered 2 years ago by ![pgunderson@sandiego.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_pgunderson@sandiego.gov20210505173747.jpg) pgunderson@sandiego.gov

- <a id="acceptAnswer_1245"></a>[Accept as answer](#)

<a id="commentAns_1245"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Customizing PAC self-registration?](https://iii.rightanswers.com/portal/controller/view/discussion/1140?)
- [Where to set limit for WorldCat request through PAC?](https://iii.rightanswers.com/portal/controller/view/discussion/1135?)
- [Quick search on PAC is turning up deleted items, bibs with no items, and other things that should be suppressed. Is there a setting I'm overlooking?](https://iii.rightanswers.com/portal/controller/view/discussion/218?)
- [Does anyone know how to set up automatic "welcome emails" when someone registers for a new library card via the PAC?](https://iii.rightanswers.com/portal/controller/view/discussion/1157?)
- [How do I set the checkbox on the Donation page for Donor information to default to be checked?](https://iii.rightanswers.com/portal/controller/view/discussion/678?)

### Related Solutions

- [Branches for online registration](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930919922328)
- [New patrons identified as a duplicate / new patrons not able to register on the PAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930681961607)
- [Removing notification options in the PAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930952432121)
- [Restrict Patron Information Updates on the PAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930535427887)
- [Defining what UDF fields display in a Patron Self-Registration from the PowerPAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930353339884)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)