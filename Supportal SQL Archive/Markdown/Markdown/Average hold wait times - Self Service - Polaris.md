Average hold wait times - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Average hold wait times

0

63 views

I'm trying to get an average of time from when a hold is created to when it becomes held. By collection or material type. I know it involves transaction headers, transaction types 6005 and 6006 but the execution escapes me. 

We changed loan periods for some of our items and I want to see the effect this has on hold wait times. So if I could add dates the hold was placed between, that would be handy to compare one month of holds to another. 

I'm hoping to get results that look like the following:

For holds created in September 2019,

Material Type |  Average days on hold (created to held)

DVD                  |   21

Book                 |   17

Book-on-CD     |   10

Thanks!

asked 1 year ago  by <img width="18" height="18" src="../../_resources/a6b0eb1482444b28973f517a74e316ed.jpg"/>  crosenthal@pwcgov.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_990"></a>[](#)

0

<a id="downVoteAns_990"></a>[](#)

this is great Jack. Thank you! How would I tweak it to show material type instead of collection?

answered 1 year ago  by <img width="18" height="18" src="../../_resources/a6b0eb1482444b28973f517a74e316ed.jpg"/>  crosenthal@pwcgov.org

- <a id="acceptAnswer_990"></a>[Accept as answer](#)

I have no idea, sorry.  I've tried poking around in those tables and it takes several minutes every time I run a query.  I've tried various things, including checking out something that was on hold, and I'm just not seeing how it's done.  Perhaps someone else can help.

— jjack@aclib.us 1 year ago

After digging into this further, here's what I've found:

Material Type is not logged to the database on hold creation (TransactionTypeID 6005) or hold held (TransactionTypeID 6006).

It's also not logged on "hold satisfied" (transaction 6039).

It is logged on check out (TransactionTypeID 6001).

If it's possible to tie material type to the length of time something is held, it would probably have to be done only for items which were then checked out (excluding everything which either wasn't picked up or is still being held), and even then it wouldn't be easy or quick.

\[edit to add: and it also might not be possible, as--again--it's not logged directly on the "hold satisfied" transaction type, and the "check out" transaction might be considered something completely separate, with no clear and unambiguous way to link the two.\]

— jjack@aclib.us 1 year ago

<a id="commentAns_990"></a>[Add a Comment](#)

<a id="upVoteAns_989"></a>[](#)

0

<a id="downVoteAns_989"></a>[](#)

If I've understood you right, you want things grouped by the month a hold was placed, not the month the hold was held.  If I've got that right, then this should work for you:

SELECT td.numValue AS 'HoldRequestID', MAX(th.TranClientDate) AS 'Held', col.Name 
INTO #tmpHeldDate
FROM PolarisTransactions..TransactionHeaders th (NOLOCK)
JOIN PolarisTransactions..TransactionDetails td (NOLOCK)
ON (td.TransactionID = th.TransactionID) AND (td.TransactionSubTypeID = '233') --HoldRequestID
JOIN PolarisTransactions..TransactionDetails tdColl (NOLOCK)
ON (tdColl.TransactionID = th.TransactionID) AND (tdColl.TransactionSubTypeID = '61') --CollectionID
JOIN Polaris.Polaris.Collections col with (NOLOCK)
ON tdColl.numValue = CollectionID
WHERE th.TransactionTypeID = '6006' --hold held
AND col.CollectionID IN (@Collection)
GROUP BY td.numValue, col.Name

SELECT td.numValue AS 'HoldRequestID', th.TranClientDate AS 'Created'
INTO #tmpCreatedDate
FROM PolarisTransactions..TransactionHeaders th (NOLOCK)
JOIN PolarisTransactions..TransactionDetails td (NOLOCK)
ON (td.TransactionID = th.TransactionID) AND (td.TransactionSubTypeID = '233') --HoldRequestID
WHERE th.TransactionTypeID = '6005' --hold created
AND th.TranClientDate BETWEEN @From AND @Through + ' 11:59:59 pm'

SELECT thd.Name AS 'Description', AVG(datediff(day,tfd.Created,thd.Held)) AS 'Days held'
FROM #tmpHeldDate thd with (NOLOCK)
JOIN #tmpCreatedDate tfd with (NOLOCK)
ON thd.HoldRequestID = tfd.HoldRequestID
GROUP BY thd.Name
ORDER BY thd.Name

DROP TABLE #tmpHeldDate
DROP TABLE #tmpCreatedDate

You could also change that second-to-last bit like so, if you wanted to see how many items were held for how long:

SELECT thd.Name AS 'Description', datediff(day,tfd.Created,thd.Held) AS 'Days held', count(datediff(day,tfd.Created,thd.Held)) AS '# held'
FROM #tmpHeldDate thd with (NOLOCK)
JOIN #tmpCreatedDate tfd with (NOLOCK)
ON thd.HoldRequestID = tfd.HoldRequestID
GROUP BY thd.Name, datediff(day,tfd.Created,thd.Held)
ORDER BY thd.Name, datediff(day,tfd.Created,thd.Held)

Unfortunately I'm not seeing any current transactions marking when a hold was suspended or unsuspended, just an obsolete one about hold reactivation.  There may be a way to get at that info; if so, I'd be happy to hear about it.

answered 1 year ago  by <img width="18" height="18" src="../../_resources/a6b0eb1482444b28973f517a74e316ed.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_989"></a>[Accept as answer](#)

<a id="commentAns_989"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Average time things are lost before coming back?](https://iii.rightanswers.com/portal/controller/view/discussion/436?)
- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [Circ by material type and time frame](https://iii.rightanswers.com/portal/controller/view/discussion/85?)
- [I'm looking for a SQL report that will tell how long items are in held status.](https://iii.rightanswers.com/portal/controller/view/discussion/495?)
- [CollectionHQ Transaction and Holds Data Extracts](https://iii.rightanswers.com/portal/controller/view/discussion/358?)

### Related Solutions

- [Can we suspend the hold option for specific titles?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930255045537)
- [Adjust the time when text message and e-mail notices are sent](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930915214612)
- [Hold Slip](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161021093203518)
- [Hold Pick-Up Slip - Tag](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161005084041146)
- [Combined In-Transit and Hold Slip](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161005093304601)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)