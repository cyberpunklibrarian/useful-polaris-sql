[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Third Party](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=56)

# CCL list for collections

0

50 views

Hi all,

We are currently creating an app with Capira. They've asked us to supply them with CCLs for our collections. Does anyone know where to find this list? Or is this the same as the abbreviations? 

Thank you!

asked 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_84d3e0913d4e4cbea08100e8430eb840.jpg"/> joy.meza@brentwoodtn.gov

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 3 years ago

<a id="upVoteAns_1093"></a>[](#)

0

<a id="downVoteAns_1093"></a>[](#)

It sounds like they want what you have listed in your policy table called PAC Limit By Display (again found in SA, usually defined at the branch level, I think). 

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_84d3e0913d4e4cbea08100e8430eb840.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_1093"></a>[Accept as answer](#)

<a id="commentAns_1093"></a>[Add a Comment](#)

<a id="upVoteAns_1092"></a>[](#)

0

<a id="downVoteAns_1092"></a>[](#)

Hi Trevor,

Here's the response I received when I gave the collection IDs. Have any insight?

*These are not valid CCLs. We will need the full commands for each that are programmed into your ILS, they usually appear like*

&nbsp;

*TOM=&lt;value&gt;*

*MAT=&lt;value&gt;*

*COL=&lt;value&gt;*

*AB=&lt;value&gt;*

&nbsp;

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_84d3e0913d4e4cbea08100e8430eb840.jpg"/> joy.meza@brentwoodtn.gov

- <a id="acceptAnswer_1092"></a>[Accept as answer](#)

<a id="commentAns_1092"></a>[Add a Comment](#)

<a id="upVoteAns_1091"></a>[](#)

0

<a id="downVoteAns_1091"></a>[](#)

Thanks, Trevor. I'll try that and hopefully, that's all they need.

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_84d3e0913d4e4cbea08100e8430eb840.jpg"/> joy.meza@brentwoodtn.gov

- <a id="acceptAnswer_1091"></a>[Accept as answer](#)

<a id="commentAns_1091"></a>[Add a Comment](#)

<a id="upVoteAns_1090"></a>[](#)

0

<a id="downVoteAns_1090"></a>[](#)

If you're looking for the CollectionID, you can find that in SQL in the Collections table.  Without SQL, you can look at your list of collections in System Administration.  Expand the first column (which is normally so small you can't see the ID) and you'll see the number associated with each collection.

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_84d3e0913d4e4cbea08100e8430eb840.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_1090"></a>[Accept as answer](#)

<a id="commentAns_1090"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Does anyone have a list or can add to the list of companies that offer e-Card and/or electronic renewal and identification verification services that integrate with Polaris API?](https://iii.rightanswers.com/portal/controller/view/discussion/1109?)
- [PayPal Confirmation Email Problem](https://iii.rightanswers.com/portal/controller/view/discussion/1335?)
- [KitKeeper Authentication -- Ask Plymouth Rocket to develop for Polaris API](https://iii.rightanswers.com/portal/controller/view/discussion/451?)
- [Patron Address Verification Services](https://iii.rightanswers.com/portal/controller/view/discussion/1265?)
- [Is anyone using the "24-Hour Library, Envisionware's unique self-service library branch?" If you are could, you please share any information you have about it, the good, the bad and the ugly.](https://iii.rightanswers.com/portal/controller/view/discussion/448?)

### Related Solutions

- [Working with Vega Bookmark Lists for Librarians](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=221221133943630)
- [Power Search Access Points](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170606100444820)
- [Power Search Wildcards](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170607114526743)
- [Collection workform "branches" checkboxes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930545936496)
- [Collection Agency and Minimum Balance](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181231110228981)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)