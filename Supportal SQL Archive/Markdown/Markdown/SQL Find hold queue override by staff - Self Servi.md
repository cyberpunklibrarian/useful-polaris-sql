[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL Find hold queue override by staff

0

43 views

I need to create a report showing when staff have overridden the block that an item is being held for another patron to either checkout or extend the loan period on an item.

asked 3 months ago by <img width="18" height="18" src="../_resources/default-avatar_8882606826cb4f7b8184242ecf6fd4b1.jpg"/> Chloe.Kirk@fortbend.lib.tx.us

[holds queue](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=545#t=commResults) [override](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=546#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 3 months ago

<a id="upVoteAns_1423"></a>[](#)

0

<a id="downVoteAns_1423"></a>[](#)

This is a bit clunky but it should return a list of times when staff checked items out to patrons without satisfying a hold. When I checked this on our system, I found it mostly returned either items being checked out to another household member or items held for staff that they chose to check out under a different card for a program.

```markup
declare @date as datetime='2024-04-29' 
select itemrecordhistory.transactiondate,itemrecordhistory.itemrecordid from itemrecordhistory with (nolock)
left join transactiondetails with (nolock) on itemrecordhistory.itemrecordid=transactiondetails.numvalue
left join transactionheaders with (nolock) on transactiondetails.transactionid=transactionheaders.transactionid
where actiontakenid=13 --action taken "checked out"
and transactiondetails.transactionsubtypeid=38 --grabs the itemrecordid from the transactiondetails
and itemrecordhistory.transactiondate > @date --limits the itemrecordhistory to your range
and transactionheaders.transactiondate > @date --limits the transactionheaders to your range
and olditemstatusid=4 --was held
and newitemstatusid=2 --now is out
and transactiontypeid=6001 --transaction type "check out"
and abs(datediff(minute,itemrecordhistory.transactiondate,transactionheaders.transactiondate)) < 1 --transaction and item history entry are within 1 minute of each other
and itemrecordid not in (select itemrecordhistory.itemrecordid from itemrecordhistory with (nolock)
left join transactiondetails with (nolock) on itemrecordhistory.itemrecordid=transactiondetails.numvalue
left join transactionheaders with (nolock) on transactiondetails.transactionid=transactionheaders.transactionid
where actiontakenid=13 and transactiondetails.transactionsubtypeid=38 and itemrecordhistory.transactiondate > @date and transactionheaders.transactiondate > @date
and olditemstatusid=4 and newitemstatusid=2 and transactiontypeid=6039
and abs(datediff(minute,itemrecordhistory.transactiondate,transactionheaders.transactiondate)) < 1) --no 6039 transaction (hold satisfied)
```

Because you wouldn't be able to renew an item that was held for someone else, I tried to come up with a way to find times when staff renewed an item that *could have* filled a hold for the second part of your question. But this is necessarily an estimate - in our system, for instance, some new books are only available for local pickup, so while it may look like staff are overriding a block to renew items on that bib, in actuality they wouldn't be eligible to fill that hold anyway. Also, it only looks at hold requests that are currently active.

```markup
declare @date as datetime='2024-04-20' 
select * from itemrecordhistory with (nolock)
left join circitemrecords with (nolock) on itemrecordhistory.itemrecordid=circitemrecords.itemrecordid
where transactiondate > @date
and ActionTakenID=28 --renewal - shouldn't include patron-initiated renewals, which wouldn't be allowed anyway if the item fills a hold
and circitemrecords.associatedbibrecordid in (select bibliographicrecordid from sysholdrequests with (nolock) where activationdate > @date)
```

I'm curious to see if anyone else has any suggestions as well!

answered 3 months ago by <img width="18" height="18" src="../_resources/default-avatar_8882606826cb4f7b8184242ecf6fd4b1.jpg"/> ebrondermajor@onlib.org

- <a id="acceptAnswer_1423"></a>[Accept as answer](#)

<a id="commentAns_1423"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Patron Code "Staff "override](https://iii.rightanswers.com/portal/controller/view/discussion/704?)
- [Are cancelled holds considered holds in holds item count?](https://iii.rightanswers.com/portal/controller/view/discussion/414?)
- [SQL for holds information](https://iii.rightanswers.com/portal/controller/view/discussion/1154?)
- [Is there a way to set a longer held period for one patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/983?)
- [Is there a way to set staff placed holds for patrons to default to patron branch instead of branch location when hold is placed?](https://iii.rightanswers.com/portal/controller/view/discussion/799?)

### Related Solutions

- [Why did a patron's request move down in the hold queue?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930362222815)
- [Can we suspend the hold option for specific titles?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930255045537)
- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)
- [Restrict ability to move patrons in the Holds Queue](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930592096942)
- [Hold request on serial not supplied](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930916146469)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)