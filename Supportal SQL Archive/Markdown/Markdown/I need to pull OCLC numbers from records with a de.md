I need to pull OCLC numbers from records with a deleted status to batch update our OCLC holdings. I'm able to get a report with: Bib record ID, Title, and Bib record status, but the column for MARC OCLC number is blank. Why doesn't it show up?  - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# I need to pull OCLC numbers from records with a deleted status to batch update our OCLC holdings. I'm able to get a report with: Bib record ID, Title, and Bib record status, but the column for MARC OCLC number is blank. Why doesn't it show up?

0

35 views

I can't successfully pull the MARC OCLC number into a report. The columns selected for output are: MARC OCLC number; MARC bibliographic record ID; Bib record status name; MARC title. You can see in the attached report that the OCLC number column is blank. 

asked 1 year ago  by <img width="18" height="18" src="../../_resources/732fc2abbc1e4802af1977483a337eb9.jpg"/>  pswaidner@indypl.org

- [BibLists_270449.xlsx](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/829?fileName=829-BibLists_270449.xlsx "BibLists_270449.xlsx")

[cataloging](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=4#t=commResults) [oclc cataloging](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=194#t=commResults) [oclc number](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=258#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_977"></a>[](#)

0

<a id="downVoteAns_977"></a>[](#)

I created a report in Reporting Services using the following SQL query that might help you track down your OCLC number.

IF OBJECT_ID('tempdb..#TEMP', 'U') IS NOT NULL
DROP TABLE #TEMP
CREATE TABLE #TEMP
(Data nvarchar(4000))
INSERT INTO #TEMP
SELECT bs.Data
FROM BibliographicRecords b WITH (NOLOCK)
JOIN BibliographicTags bt WITH (NOLOCK)
ON bt.BibliographicRecordID = b.BibliographicRecordID
JOIN BibliographicSubfields bs WITH (NOLOCK)
ON bs.BibliographicTagID = bt.BibliographicTagID
WHERE b.RecordStatusID = 4
AND bt.TagNumber = 35
AND bs.Data LIKE '%OC%'
AND b.RecordStatusDate BETWEEN (SELECT DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) - 1, 0))
and (SELECT DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0))
SELECT DISTINCT Data FROM #TEMP ORDER BY Data
DROP TABLE #TEMP

Rex Helwig
Finger Lakes Library System

answered 1 year ago  by <img width="18" height="18" src="../../_resources/732fc2abbc1e4802af1977483a337eb9.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_977"></a>[Accept as answer](#)

<a id="commentAns_977"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [I'm looking for a report that will tell me how many bib records a staff member has created and/or modified in a month.](https://iii.rightanswers.com/portal/controller/view/discussion/942?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [I'm looking for a SQL report that will tell how long items are in held status.](https://iii.rightanswers.com/portal/controller/view/discussion/495?)

### Related Solutions

- [Export OCLC Control Numbers from Polaris to update WorldCat Holdings using Simply Reports – Record Set Method](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181128101912576)
- [Export OCLC Control Numbers from Polaris to update WorldCat Holdings using Simply Reports – Added Record Query Method](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181128103351248)
- [Statistical Summary Report Bin Record Totals](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930459959777)
- [Where to find Patron Circ Count in Patron record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930283871188)
- [Determining the number of registered patrons](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930339388822)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)