Lost Item Report - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Lost Item Report

0

55 views

Hello, 

We would like to run a report to see the number of items lost due to patrons not returning material in the last 2 years. 

Does anyone have a SQL report that would assist in this?  

Thank you in advance. 

Teresa R. 

Email: treel@mypccl.org

asked 7 months ago  by <img width="18" height="18" src="../../_resources/dfe6b9cf7f214d8ab6c6540073004d61.jpg"/>  treel@mypccl.org

Thank you so much for your help!!

— treel@mypccl.org 7 months ago

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 7 months ago

<a id="upVoteAns_1060"></a>[](#)

0

<a id="downVoteAns_1060"></a>[](#)

Answer Accepted

I just remembered that some systems might have things set up to automatically withdraw and/or delete and/or purge items which stay in "Lost" status a certain amount of time.  It sounds like your system might not be set up to do that, but if it is, it might be underreporting a bit, depending on how long the system waits before doing any of that.

That said, this should work for the count of items lost between two dates, assuming you wanted to include items lost on the second date (12/31/20) (If you leave the time out when specifying a date, it assumes you mean the first millisecond of that date, hence specifying just before midnight on 12/31/20):

SELECT ItemRecordID
FROM CircItemRecords with (NOLOCK)
WHERE ItemStatusID IN (7)
AND ItemStatusDate BETWEEN '1/1/2019' AND '12/31/2020 11:59:59 PM'

Once you get into wanting a sum cost, you're moving out of what the client and/or Leap can do, but it's possible in Report Builder:

SELECT count(cir.ItemRecordID) AS 'Items lost', sum(ird.Price) AS 'Sum Price'
FROM CircItemRecords cir with (NOLOCK)
JOIN ItemRecordDetails ird with (NOLOCK)
ON cir.ItemRecordID = ird.ItemRecordID
WHERE cir.ItemStatusID IN (7)
AND cir.ItemStatusDate BETWEEN '1/1/2019' AND '12/31/2020 11:59:59 PM'

I should note: this does assume that only your physical items go into Lost status.  Our OverDrive items *don't* go into Lost status, but I don't know how customizable the OverDrive integration is, so I suppose it's possible that for other systems they might.  I have no idea how Polaris deals with titles from other providers (Axis360, 3M, etc.); they may also generate items in the system which could do strange things.

If you wanted to explicitly exclude those items, you could limit by cir.MaterialTypeID, cir.AssignedCollectionID, etc. to either include only the things you want or exclude the things you don't want.

answered 7 months ago  by <img width="18" height="18" src="../../_resources/dfe6b9cf7f214d8ab6c6540073004d61.jpg"/>  jjack@aclib.us

<a id="commentAns_1060"></a>[Add a Comment](#)

<a id="upVoteAns_1059"></a>[](#)

0

<a id="downVoteAns_1059"></a>[](#)

Did you want a separate report, something with figures broken up by collection/materialtype, etc., or just a raw total?  If you just want the raw ttoal, this should work (make sure to set the index to "Item record" or the results will be garbage, then just check "Count only" in the client and/or Leap):

SELECT ItemRecordID
FROM CircItemRecords with (NOLOCK)
WHERE ItemStatusID IN (7)
AND ItemStatusDate > '2/23/2019'

Caveats: 

This assumes that "Lost" is ItemStatusID 7 in your system, as it is in ours.  You should be able to verify this by spot-checking a few, if you run it without the "count only" option set.

If this doesn't work for you or isn't what you're looking for, please let me know.

answered 7 months ago  by <img width="18" height="18" src="../../_resources/dfe6b9cf7f214d8ab6c6540073004d61.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_1059"></a>[Accept as answer](#)

Hello, 

This worked for that specific date. How do I modify it to contain multiple years? Say that I want a count of lost items from 1/1/2019- 12/31/2020?

On another note, my Director would like a report that tells us the number of materials that their status was changed to Lost between1/1/2019-12/31/20 & would include the loss in cost of those materials.  Is this possible? Thank you

— treel@mypccl.org 7 months ago

<a id="commentAns_1059"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Report of Lost and Paid Items](https://iii.rightanswers.com/portal/controller/view/discussion/331?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Adding number of "live" items to a report](https://iii.rightanswers.com/portal/controller/view/discussion/666?)
- [Searching for Items that have not circ'd in 2 years ex. DVD's](https://iii.rightanswers.com/portal/controller/view/discussion/1053?)

### Related Solutions

- [Item Circulation Statistics Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930404036798)
- [Item Circulation by Statistical Code](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930592909894)
- [Claimed item does not appear in the Claimed Items report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930283717537)
- [Errors accessing reports in the client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930369939404)
- [Custom report parameters not allowing multiple selections in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930424447003)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)