[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# How to Find Definitions of ActionTakenID Numbers

0

65 views

Originally posted by dhaskell@scottsdaleaz.gov on Tuesday, April 11th 18:33:03 EDT 2017  
<br/>I would like to find out what the numbers mean in the column ActionTakenID in the table Polaris.Polaris.ItemRecordHistory. Is there a lookup table or map for this type of information? The Polaris Database Repository only goes as far a table headings. Thank you. Dan, Scottsdale Public Library

asked 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_cf9843dd29aa45f9bf8fb90719adb15a.jpg"/> support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 7 years ago

<a id="upVoteAns_78"></a>[](#)

1

<a id="downVoteAns_78"></a>[](#)

Do you have SQL access?  This should work:

SELECT \* FROM Polaris.Polaris.ItemRecordHistoryActions with (NOLOCK) 

If that won't work for you, or if you can't get them all easily dumped into a spreadsheet, let me know and I can attach the results.  As far as I can see, there's no way to customize them, so they should be the same across library systems.  
<br/>John / Alachua County Library District 

answered 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_cf9843dd29aa45f9bf8fb90719adb15a.jpg"/> jjack@aclib.us

- <a id="acceptAnswer_78"></a>[Accept as answer](#)

<a id="commentAns_78"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for circulation numbers](https://iii.rightanswers.com/portal/controller/view/discussion/94?)
- [Adding number of "live" items to a report](https://iii.rightanswers.com/portal/controller/view/discussion/666?)
- [Do reporting by subject headings?](https://iii.rightanswers.com/portal/controller/view/discussion/1059?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [I need to pull OCLC numbers from records with a deleted status to batch update our OCLC holdings. I'm able to get a report with: Bib record ID, Title, and Bib record status, but the column for MARC OCLC number is blank. Why doesn't it show up?](https://iii.rightanswers.com/portal/controller/view/discussion/829?)

### Related Solutions

- [How to read SIP logs: Field Definitions](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170310144452308)
- [What do the terms in the Financial Transaction Summary by Fee Reason report mean?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930334637492)
- [Long Distance Telephony](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930882128740)
- [SAN number for EDI ordering](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930858154155)
- [Maximum item call number length](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930857542877)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)