Finding a item's [DELETED] title when searching patron fines - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Finding a item's \[DELETED\] title when searching patron fines

0

72 views

Does anyone know if there is a way, either through SQL or SimplyReports, to show the an item's title for overdue/lost fines when the item has been deleted? The title will show up on the patron's account with the prefix \[deleted\] in Polaris, but the field is blank when running a list through SimplyReports.

asked 1 year ago  by <img width="18" height="18" src="../../_resources/fbded88c50ae40bdbf572da60d33daee.jpg"/>  josh.barnes@portneuflibrary.org

[patron account](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=85#t=commResults) [simply reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=11#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_854"></a>[](#)

1

<a id="downVoteAns_854"></a>[](#)

Hi Josh.

There is a table named PattronAcctDeletedIemRecords that contains the ItemRecordID, Barcode, (bib) BrowseTitle, (bib) BrowseAuthor and the item's MaterialType. The (bib) information will stay even if both the item and the bibliographic record have gone away.

You should be able to join the PatronAccount table to this table on the ItemRecordID. I'm not sure if there's any way to pull the information in Simply Reports.

This table may also be very helpful if you receive an item that has been completely deleted, but may have an outstanding bill that you want to waive (because the item is in good condition) as long as the item still has a barcode number.

Here's a Find Tool query for this:

SELECT
PatronID
FROM Polaris.Polaris.PatronAccount pa WITH (NOLOCK)
JOIN Polaris.Polaris.PatronAcctDeletedItemRecords pad (NOLOCK)
ON (pa.ItemRecordID = pad.ItemRecordID)
WHERE
pad.Barcode like '111222333' --Replace this with the desired barcode number.

Hope this helps.

JT

answered 1 year ago  by <img width="18" height="18" src="../../_resources/fbded88c50ae40bdbf572da60d33daee.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_854"></a>[Accept as answer](#)

<a id="commentAns_854"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL to Find all bibs with same title.](https://iii.rightanswers.com/portal/controller/view/discussion/675?)
- [Circulation count of deleted items from a collection](https://iii.rightanswers.com/portal/controller/view/discussion/92?)
- [Need help with query to find checked out titles with holds](https://iii.rightanswers.com/portal/controller/view/discussion/1063?)
- [Anyone have a sql script that will total fines for a year and then break them down by patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/688?)
- [Report to find Items added by Material Type in a certain year?](https://iii.rightanswers.com/portal/controller/view/discussion/430?)

### Related Solutions

- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Deleting item records with fines](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930356108467)
- [If retain deleted records is set to Yes will the records be visible during searches?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930352546757)
- [Power Search Access Points](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170606100444820)
- [How to find Patrons in a Secured state](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=201105115918390)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)