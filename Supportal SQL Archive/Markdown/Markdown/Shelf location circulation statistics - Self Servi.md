Shelf location circulation statistics - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Shelf location circulation statistics

0

103 views

One of our librarians wants circulation statistics by shelf location. I pulled together this script, but now she would also like the number of items each shelf location has, and I need help with that. (Yes, we could just assign them a collection instead of a shelf location, but it would soon be lots more collections.) Has anyone done this? Many thanks.

select  torg.name as TransactionBranchName, sl.description as ShelfLocation,count(distinct th.transactionid) as Total  
from PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK)
left outer join PolarisTransactions.Polaris.TransactionDetails td (nolock)
on (th.TransactionID = td.TransactionID)  
inner join Polaris.Polaris.Organizations torg (nolock)
on (th.OrganizationID = torg.OrganizationID)
inner join Polaris.Polaris.ShelfLocations sl (nolock)
on (td.numvalue = sl.ShelfLocationID)  
where th.TransactionTypeID = 6001
and td.TransactionSubTypeID = 296
and th.TranClientDate between dateadd(mm,-1,getdate()) and getdate()
and th.OrganizationID = 15
and (sl.description in  
    (Select description from shelflocations where organizationid = 15))
Group by  torg.name,sl.description
Order by  torg.name,sl.description

asked 2 years ago  by <img width="18" height="18" src="../../_resources/99e9d991b0394bae81f1ba66fb07ce44.jpg"/>  mborg@gmilcs.org

<a id="comment"></a>[Add a Comment](#)

## 6 Replies   last reply 2 years ago

<a id="upVoteAns_510"></a>[](#)

0

<a id="downVoteAns_510"></a>[](#)

Rex,

The problem is in my original script. One of the shelf locations is called "Coming Soon" and two other libraries are using that shelf location. The library that is orgID 15 has 850 items, but the report returns 5777 items. What is even more odd is that the shelflocationID for 15 is different from the other libraries; the count seems to be reporting based on the description not the shelflocationID and orgID. As long as the shelf location description is unique, the report is beautiful.

Marilyn

answered 2 years ago  by <img width="18" height="18" src="../../_resources/99e9d991b0394bae81f1ba66fb07ce44.jpg"/>  mborg@gmilcs.org

- <a id="acceptAnswer_510"></a>[Accept as answer](#)

<a id="commentAns_510"></a>[Add a Comment](#)

<a id="upVoteAns_509"></a>[](#)

0

<a id="downVoteAns_509"></a>[](#)

Marilyn,

I am not seeing the cross-from other branch assigned items.  However, I do see where the piece I added didn't take into account items marked for deletion so I needed to add one more line to the query to account for that:

and cir.RecordStatusID <> 4

The query below now accounts for items marked for deletion and does not count them.

declare @OrgID int set @OrgID = 15
select  torg.name as TransactionBranchName, sl.description as ShelfLocation,count(distinct th.transactionid) as Total, count(distinct cir.itemrecordid) as \[Count\]
from PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK)
left outer join PolarisTransactions.Polaris.TransactionDetails td (nolock)
on (th.TransactionID = td.TransactionID) 
inner join Polaris.Polaris.Organizations torg (nolock)
on (th.OrganizationID = torg.OrganizationID)
inner join Polaris.Polaris.ShelfLocations sl (nolock)
on (td.numvalue = sl.ShelfLocationID)
join CircItemRecords cir
on (cir.ShelfLocationID = sl.ShelfLocationID) 
where th.TransactionTypeID = 6001
and td.TransactionSubTypeID = 296
and th.TranClientDate between dateadd(mm,-1,getdate()) and getdate()
and th.OrganizationID = @OrgID
and cir.AssignedBranchID = @OrgID
and cir.RecordStatusID <> 4
and (sl.description in 
    (Select description from shelflocations where organizationid = @OrgID))
Group by  torg.name,sl.description
Order by  torg.name,sl.description

answered 2 years ago  by <img width="18" height="18" src="../../_resources/99e9d991b0394bae81f1ba66fb07ce44.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_509"></a>[Accept as answer](#)

<a id="commentAns_509"></a>[Add a Comment](#)

<a id="upVoteAns_508"></a>[](#)

0

<a id="downVoteAns_508"></a>[](#)

JT, what I'm trying to achieve is circulation statistics for shelf locations along with the item count for each shelf location. Part of what I'm running into is the way shelf locations are coded in the database. I thought I had accounted for that, but apparently not. For example, if the branch I want the statistic for shares the same shelflocationID as another, my script (and consequently Rex's addition) pulls up the count for both branches.

I'm trying to get statistics for shelf locations because the library wants to know how successful it is to pull books from various collections and item stat codes onto a display.

Does this help you help me? Many thanks!

Marilyn

answered 2 years ago  by <img width="18" height="18" src="../../_resources/99e9d991b0394bae81f1ba66fb07ce44.jpg"/>  mborg@gmilcs.org

- <a id="acceptAnswer_508"></a>[Accept as answer](#)

<a id="commentAns_508"></a>[Add a Comment](#)

<a id="upVoteAns_507"></a>[](#)

0

<a id="downVoteAns_507"></a>[](#)

Hi Marilyn.

Do you need circulation stats or just a count of how many items are assigned to each shelf location? If you just need a count, do you need it to be broken down by organization, collection and/or material type?

JT

answered 2 years ago  by <img width="18" height="18" src="../../_resources/99e9d991b0394bae81f1ba66fb07ce44.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_507"></a>[Accept as answer](#)

<a id="commentAns_507"></a>[Add a Comment](#)

<a id="upVoteAns_505"></a>[](#)

0

<a id="downVoteAns_505"></a>[](#)

Does this work for you?

declare @OrgID int set @OrgID = 15
select  torg.name as TransactionBranchName, sl.description as ShelfLocation,count(distinct th.transactionid) as Total, count(distinct cir.itemrecordid) as \[Count\]
from PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK)
left outer join PolarisTransactions.Polaris.TransactionDetails td (nolock)
on (th.TransactionID = td.TransactionID) 
inner join Polaris.Polaris.Organizations torg (nolock)
on (th.OrganizationID = torg.OrganizationID)
inner join Polaris.Polaris.ShelfLocations sl (nolock)
on (td.numvalue = sl.ShelfLocationID)
join CircItemRecords cir
on (cir.ShelfLocationID = sl.ShelfLocationID) 
where th.TransactionTypeID = 6001
and td.TransactionSubTypeID = 296
and th.TranClientDate between dateadd(mm,-1,getdate()) and getdate()
and th.OrganizationID = @OrgID
and cir.AssignedBranchID = @OrgID
and (sl.description in 
    (Select description from shelflocations where organizationid = @OrgID))
Group by  torg.name,sl.description
Order by  torg.name,sl.description

Rex Helwig
Finger Lakes Library System
[rhelwig@flls.org](mailto:rhelwig@flls.org)
607-319-5615

answered 2 years ago  by <img width="18" height="18" src="../../_resources/99e9d991b0394bae81f1ba66fb07ce44.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_505"></a>[Accept as answer](#)

<a id="commentAns_505"></a>[Add a Comment](#)

<a id="upVoteAns_506"></a>[](#)

0

<a id="downVoteAns_506"></a>[](#)

Rex, this is very close--exactly what I want except for three shelf locations showing very strange item counts. I'm trying to figure out if it is the report or the library.

Thanks so much!

Marilyn

answered 2 years ago  by <img width="18" height="18" src="../../_resources/99e9d991b0394bae81f1ba66fb07ce44.jpg"/>  mborg@gmilcs.org

- <a id="acceptAnswer_506"></a>[Accept as answer](#)

<a id="commentAns_506"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Circulation by Item Statistical Code & limited by assigned collection?](https://iii.rightanswers.com/portal/controller/view/discussion/801?)
- [Circulation without Renewals](https://iii.rightanswers.com/portal/controller/view/discussion/87?)
- [SQL or report for circ stats by Patron Statistical Class?](https://iii.rightanswers.com/portal/controller/view/discussion/488?)
- [SQL for circulation numbers](https://iii.rightanswers.com/portal/controller/view/discussion/94?)
- [Circulation report by patrons age](https://iii.rightanswers.com/portal/controller/view/discussion/97?)

### Related Solutions

- [Add Shelf Locations - System Level](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930254225543)
- [Error when saving Item Templates](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930278097820)
- [What to check after setting up a new code in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170228154215576)
- [Circulation statistics and deleted records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930736904760)
- [Shelf-Ready Ordering and Enriched EDI](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930673779758)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)