[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Polaris Database Backups

0

146 views

We are currently planning to upgrade to Polaris 6.1 in the near future and as part of this process, we are looking at different solutions for database backups going forward after the upgrade. We have a SQL Server data copy job planned that will allow us to restore from the previous night, however this will not account for incremental updates throughout the day to cover those transactions that took place after the nightly backup. I'm just gathering informaiton to see what other Polaris users are doing.

What solutions is your library using for database backups?

Any informaiton is helpful. Thanks!

asked 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_a6322bceb7d545e9a4e29899a07d77d6.jpg"/> mwagers@daytonmetrolibrary.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 5 years ago

<a id="upVoteAns_483"></a>[](#)

0

<a id="downVoteAns_483"></a>[](#)

We use the Azure managed backup service that is built into SQL 2014+. https://docs.microsoft.com/en-us/sql/relational-databases/backup-restore/sql-server-managed-backup-to-microsoft-azure?view=sql-server-2017

It backs up transaction logs throughout the day automatically based on system activity. We keep the transaction logs for 3 days. (Note your database needs to be in Full recovery mode or you won't have transaction logs to back up)

We agument this with a full backup to another Azure region (we keep this for 30 days) each night and then also a local on-prem backup each night (we just keep this for one day). We also replicate our database guest VM between a couple of HyperV host machines.

For our system that has about 350GB between our Polaris and PolarisTranactions database, this costs us around $5k in Azure storage fees annually.

&nbsp;

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_a6322bceb7d545e9a4e29899a07d77d6.jpg"/> wosborn@clcohio.org

- <a id="acceptAnswer_483"></a>[Accept as answer](#)

<a id="commentAns_483"></a>[Add a Comment](#)

<a id="upVoteAns_470"></a>[](#)

0

<a id="downVoteAns_470"></a>[](#)

We do a backup overnight, and then do our upgrades in the morning, before branches open and any transactions occur (and after most jobs finish).  So far we haven't noticed any issues with this approach.  We have custom tables, as well, which we script to files just in case they get deleted (hasn't been a problem yet).

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_a6322bceb7d545e9a4e29899a07d77d6.jpg"/> jtenter@sasklibraries.ca

- <a id="acceptAnswer_470"></a>[Accept as answer](#)

<a id="commentAns_470"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Advice on creating our own table in Polaris database](https://iii.rightanswers.com/portal/controller/view/discussion/1300?)
- [Setting up the SMTP in Polaris SA](https://iii.rightanswers.com/portal/controller/view/discussion/115?)
- [Has anyone who houses their own servers come up with a way fo have multifuction authenication for Polaris?](https://iii.rightanswers.com/portal/controller/view/discussion/981?)
- [Anyone suddenly receiving this pop up error "Authentication Failure: Polaris workstation not enabled" on a PC that is used everyday??](https://iii.rightanswers.com/portal/controller/view/discussion/978?)
- [Is anyone having issues (pop-up windows not showing, drop downs not working) with Polaris client after the Windows 10 Update 1803?](https://iii.rightanswers.com/portal/controller/view/discussion/258?)

### Related Solutions

- [Pre-upgrade backups of customizations](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930565480894)
- [How do I set up a new Z39.50 connection?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930981743344)
- [Instructions for backups (SQL jobs and reports, language strings, PAC customizations) in preparation for an upgrade](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930913067166)
- [Database changes from 5.2.199 to 5.5.283](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170807151518546)
- [Polaris Database Repository (Version 5.5)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=171013130008229)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)