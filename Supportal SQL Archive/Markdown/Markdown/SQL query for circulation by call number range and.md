[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL query for circulation by call number range and date range

0

195 views

Hello all!

&nbsp;

Does anyone have a SQL query that will return non-fiction circulation for a specified call number range during a specified date range?

&nbsp;

Thank you!

Michael Gregory

Technical Services Manager

Campbell County Public Library

asked 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_7c890b6984db4a03bf3d7e119e96726a.jpg"/> mgregory@cc-pl.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 6 years ago

<a id="upVoteAns_152"></a>[](#)

0

<a id="downVoteAns_152"></a>[](#)

Hello!

Normally I do use SimplyReports, but I was trying to run circulation for a previous year, and SimplyReports wouldn't go back that far.  One of our Librarians needed it for comparative information she is supposed to supply with some documentation she's working on.

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_7c890b6984db4a03bf3d7e119e96726a.jpg"/> mgregory@cc-pl.org

- <a id="acceptAnswer_152"></a>[Accept as answer](#)

<a id="commentAns_152"></a>[Add a Comment](#)

<a id="upVoteAns_149"></a>[](#)

0

<a id="downVoteAns_149"></a>[](#)

I don't but to you have access to Simply Reports? Seems like you could get that information from that product?

&nbsp;

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_7c890b6984db4a03bf3d7e119e96726a.jpg"/> sgrant@somd.lib.md.us

- <a id="acceptAnswer_149"></a>[Accept as answer](#)

<a id="commentAns_149"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for circulation by call number prefix and date range](https://iii.rightanswers.com/portal/controller/view/discussion/913?)
- [Reset due dates - SQL help](https://iii.rightanswers.com/portal/controller/view/discussion/1324?)
- [I need help creating a SQL for circulation data that includes a lot of details.](https://iii.rightanswers.com/portal/controller/view/discussion/550?)
- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)
- [SQL for circ of items in record set, to include checkout info](https://iii.rightanswers.com/portal/controller/view/discussion/1122?)

### Related Solutions

- [You Saved Message - What is the date range for YTD savings?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930616286891)
- [How do I reset the year-to-date circulation counts?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930831368342)
- [Bulk change item due dates](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930588598835)
- [Query to find unfillable holds due to volume data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930242167096)
- [Find all permission groups that have the Secure patron record permissions](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930759206464)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)