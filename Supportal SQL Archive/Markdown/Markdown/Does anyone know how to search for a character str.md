Does anyone know how to search for a character string in a bib record, specifically in 650 fields?  Is there an SQL search where I could plug the string of characters in and find records that have that specific string? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Does anyone know how to search for a character string in a bib record, specifically in 650 fields? Is there an SQL search where I could plug the string of characters in and find records that have that specific string?

0

56 views

Thank you for your help with this.

Glenda Audrain

Gaudrain@co.washington.ar.us

asked 1 year ago  by <img width="18" height="18" src="../../_resources/93f61d3611104810af13037416a90fb1.jpg"/>  gaudrain@co.washington.ar.us

[bibs](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=312#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 1 year ago

<a id="upVoteAns_837"></a>[](#)

0

<a id="downVoteAns_837"></a>[](#)

Answer Accepted

This should work:

SELECT DISTINCT bt.BibliographicRecordID
FROM BibliographicTags bt WITH (NOLOCK)
JOIN BibliographicRecords br with (NOLOCK)
ON br.BibliographicRecordID = bt.BibliographicRecordID
JOIN BibliographicSubfields bsf (NOLOCK)
ON bsf.BibliographicTagID = bt.BibliographicTagID
WHERE bt.TagNumber = 650
AND DisplayInPAC = 1
AND bsf.Data LIKE '%*SearchTerm*%'

\[Edit to add: you can remove the "And DisplayInPAC = 1" if you also want to pull up bibs which have been suppressed from display.\]

answered 1 year ago  by <img width="18" height="18" src="../../_resources/93f61d3611104810af13037416a90fb1.jpg"/>  jjack@aclib.us

<a id="commentAns_837"></a>[Add a Comment](#)

<a id="upVoteAns_839"></a>[](#)

0

<a id="downVoteAns_839"></a>[](#)

Thanks for your help!  I really appreiate you Susan and John.  John's solution was exactly what I needed and Susan's is about the same.  You saved me some time.

Thanks again,

Glenda Audrain

answered 1 year ago  by <img width="18" height="18" src="../../_resources/93f61d3611104810af13037416a90fb1.jpg"/>  gaudrain@co.washington.ar.us

- <a id="acceptAnswer_839"></a>[Accept as answer](#)

<a id="commentAns_839"></a>[Add a Comment](#)

<a id="upVoteAns_838"></a>[](#)

0

<a id="downVoteAns_838"></a>[](#)

We have this search, "Specific subfield search" but you have to put in the tag. So put the tag, the subfield and between the %%" put the string.

 For example, here's how I find bib records with an 035, with "DLC" in the subfield a:

and btsv.TagNumber = 035

and btsv.Subfield = 'a'

and btsv.Data like '%DLC%'

The full search is below:

SELECT distinct btsv.BibliographicRecordID FROM Polaris.BibliographicTagsAndSubfields_View btsv

where btsv.BibliographicRecordID IN (select br.BibliographicRecordID from Polaris.BibliographicRecords br (nolock) where br.RecordStatusID = 1)

**and btsv.TagNumber =**

**and btsv.Subfield = ''**

**and btsv.Data like '%%'**

--and btsv.IndicatorOne = 0

--and btsv.IndicatorTwo = 0

answered 1 year ago  by <img width="18" height="18" src="../../_resources/93f61d3611104810af13037416a90fb1.jpg"/>  sgrant@somd.lib.md.us

- <a id="acceptAnswer_838"></a>[Accept as answer](#)

<a id="commentAns_838"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [How do you enter/search for titles with special characters?](https://iii.rightanswers.com/portal/controller/view/discussion/320?)
- [Can you change a tag in a bib record in Leap?](https://iii.rightanswers.com/portal/controller/view/discussion/1055?)
- [Use Item Record Call Numbers to Bulk Update Bib MARC Tag](https://iii.rightanswers.com/portal/controller/view/discussion/235?)
- [Does anyone work with Baker & Taylor as a vendor, and now receive FTP files for MARC records?](https://iii.rightanswers.com/portal/controller/view/discussion/999?)
- [How should I be entering call number info in the 092 to make the subfields correctly correspond with item record call number fields?](https://iii.rightanswers.com/portal/controller/view/discussion/313?)

### Related Solutions

- [1XX, 6XX, 7XX or 8XX tags not linked to Authority Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930898871020)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [How to link multiple Authority Records to a Bibliographic Record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930318385316)
- [Why when adding a 650 tag to a bib does the tag come up as invalid.](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181228145044629)
- [If retain deleted records is set to Yes will the records be visible during searches?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930352546757)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)