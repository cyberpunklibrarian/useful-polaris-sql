[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# How to store date that report was last run?

0

37 views

Hi -

&nbsp;

I'd like to have a report parameter that is the datetime when the report was last run.  That way, when staff run the report, they know it will be retrieving only data that was added, changed, whatever, since the last time they ran it.  For example, a report of holds now ready for pickup where the patron needs to be called.  We want only the ones that became ready for pickup since the last time the report was run, to avoid calling twice.  I'll be using Report Builder to create the report.

Thanks in advance!

Bill

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_38b76aa2291c4590b704d918a7f87042.jpg"/> wtaylor@washcolibrary.org

[paramet](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=458#t=commResults) [report builder](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=151#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_1177"></a>[](#)

0

<a id="downVoteAns_1177"></a>[](#)

Have you considered setting it up to run on a schedule (daily, weekly, etc.) and only including the titles which fall within that schedule?   You can set up reports to have a default value which pulls only the last X days'/weeks'/months' worth etc. on the subscription page.

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_38b76aa2291c4590b704d918a7f87042.jpg"/> jjack@aclib.us

- <a id="acceptAnswer_1177"></a>[Accept as answer](#)

<a id="commentAns_1177"></a>[Add a Comment](#)

<a id="upVoteAns_1176"></a>[](#)

0

<a id="downVoteAns_1176"></a>[](#)

I can see issues with this practice, such as when reports fail or someone runs the report and does not act on the data. Regardless, the way to see report execution history is to build a query based on the ReportServer view \[ExecutionLog3\].

answered 2 years ago by ![eric.young@phoenix.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_eric.young@phoenix.gov20191029113427.jpg) eric.young@phoenix.gov

- <a id="acceptAnswer_1176"></a>[Accept as answer](#)

<a id="commentAns_1176"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Is it possible to save a record set when running a saved report in Simply Reports?](https://iii.rightanswers.com/portal/controller/view/discussion/979?)
- [Renewals reports](https://iii.rightanswers.com/portal/controller/view/discussion/83?)
- [Simply Reports Discrepancies](https://iii.rightanswers.com/portal/controller/view/discussion/960?)

### Related Solutions

- [Can I create a report in SimplyReports that can be run with a specific date range?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930371790305)
- [Run SimplyReports Scheduled Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930277327637)
- [Where are UMS reports stored?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930596768478)
- [Reports and Notices not running](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930295540365)
- [Errors accessing reports in the client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930369939404)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)