[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for items returned late by collection

0

50 views

We're thinking about extending the loan period for some of our collections from 7 days to 21 days, so I'm writing up a recommendation.

I'm looking for a way to see what number of items in a given collection, in a given time frame, were returned overdue.

I'm hoping the results could provide me the following info;

Fiction, calendar year 2019: 2,000 items returned, 500 were overdue  
DVDs, calendar year 2019: 1,000  items returned, 600 were overdue

&nbsp;

Also, I was wondering if could do a similar thing with overdue fines paid per collection:

Fiction, calendar year 2019: $1,000 in overdue fines collected  
DVDs, calendar year 2019: $2,000 in overdue fines collected

&nbsp; Thanks!

&nbsp;

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_5baaa3084c3e4d1292da77e64968a422.jpg"/> crosenthal@pwcgov.org

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [Circulation count of deleted items from a collection](https://iii.rightanswers.com/portal/controller/view/discussion/92?)
- [Circulation by Item Statistical Code & limited by assigned collection?](https://iii.rightanswers.com/portal/controller/view/discussion/801?)
- [SQL for Item records from PO/FY/Branch](https://iii.rightanswers.com/portal/controller/view/discussion/326?)

### Related Solutions

- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Creating a new collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930912772907)
- [EDI items coming in with the wrong collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930695455841)
- [System Administration Overview--Parameters, Profiles, Tables, Collections and SQL Jobs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160908152552966)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)