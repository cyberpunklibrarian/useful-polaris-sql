[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# How to delete custom reports from toolbar made in SR

0

55 views

I created custom reports in SR and published to toolbar. I found that I made a mistake, so I wanted to redo report. I deleted from SR and started fresh. The old one will not disappear from the toolbar. I've hit the refresh button multiple times. What am I missing?

asked 4 years ago by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg) sbills@lpld.lib.in.us

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 4 years ago

<a id="upVoteAns_953"></a>[](#)

1

<a id="downVoteAns_953"></a>[](#)

Answer Accepted

Hi.

I'm not sure how easy it is to delete a report that was published from Simply Reports. If you have access to the web interface of SQL Reporting Services (the URL would be something like [http://\[yourservername\]/Reports/browse/](http://aclpro/Reports/browse/)) you can navigate to the Custom Folder and delete the report from there. (If you are asked for a password, your Simply Reports password will probably work.)

Hope this helps.

JT

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_62fa0f61663b46dfab4200bcbd5356b7.jpg"/> jwhitfield@aclib.us

As always, JT, you're the best! Thank you!!  
<br/>

— sbills@lpld.lib.in.us 4 years ago

<a id="commentAns_953"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Simply Reports Discrepancies](https://iii.rightanswers.com/portal/controller/view/discussion/960?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Simply reports patron services](https://iii.rightanswers.com/portal/controller/view/discussion/355?)

### Related Solutions

- [Question about a saved SimplyReports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930457380859)
- [Question about published SimplyReports reports](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989986518)
- [Custom report parameters not allowing multiple selections in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930424447003)
- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [Run SimplyReports Scheduled Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930277327637)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)