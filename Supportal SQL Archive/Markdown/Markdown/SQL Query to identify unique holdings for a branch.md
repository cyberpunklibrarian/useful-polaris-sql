[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# SQL Query to identify unique holdings for a branch

0

103 views

I'm trying to identify titles where copies are held only by 1 particular branch.

I can come up with how to identify what titles the branch holds (which I don't need SQL for, of course), but not those that are held by that branch and no others,

&nbsp;

TIA

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_a2d7392bb33f4a98af8cf259512adc77.jpg"/> nbennyhoff@stchlibrary.org

[sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults) [unique titles by branch](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=339#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 4 years ago

<a id="upVoteAns_904"></a>[](#)

0

<a id="downVoteAns_904"></a>[](#)

Answer Accepted

That exact search didn't work in my system, but it helped me build one that did:

(CN == "\*") AND (AB = 10) NOT (AB <> 10)

Thanks very much. This helped immensely and will definitely help in the future.

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_a2d7392bb33f4a98af8cf259512adc77.jpg"/> nbennyhoff@stchlibrary.org

<a id="commentAns_904"></a>[Add a Comment](#)

<a id="upVoteAns_903"></a>[](#)

0

<a id="downVoteAns_903"></a>[](#)

I've used a power search to find titles owned by only one branch and no others. I'm not sure if this will work for you?

CN="#\*" AND (AB={42}) NOT (AB<>{42})

(with the AB number being whichever branch you're interested in)

\-Amy

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_a2d7392bb33f4a98af8cf259512adc77.jpg"/> amihelich@wccls.org

- <a id="acceptAnswer_903"></a>[Accept as answer](#)

<a id="commentAns_903"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Bib record modifiers](https://iii.rightanswers.com/portal/controller/view/discussion/1380?)
- [SQL for items withdrawn by a particular staff member](https://iii.rightanswers.com/portal/controller/view/discussion/809?)
- [Is the an SQL statement that finds items in processing that have holds?](https://iii.rightanswers.com/portal/controller/view/discussion/476?)
- [changing branch with bulk change](https://iii.rightanswers.com/portal/controller/view/discussion/35?)
- [Polaris OCLC Holdings](https://iii.rightanswers.com/portal/controller/view/discussion/410?)

### Related Solutions

- [Export OCLC Control Numbers from Polaris to update WorldCat Holdings using Simply Reports – Added Record Query Method](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181128103351248)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Collection workform "branches" checkboxes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930545936496)
- [Add Shelf Locations - System Level](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930254225543)
- [Find System Generated Authority Headings (Find Tool SQL)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=221207162751470)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)