[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# adding info to bibs via SQL script?

0

94 views

Originally posted by kara.steiniger@smgov.net on Thursday, March 2nd 17:42:17 EST 2017  
<br/>Hello, We have a somewhat time-consuming method of ordering added copies via EDI which we’d love to simplify if possible. The issue arises when the ISBN of the incoming item doesn’t match the existing bib record, though we want the new copy to be added to the existing bib. This happens a lot with children’s books that have different bindings. Often, the existing ISBN is no longer available. Our Acquisitions staff manually add the incoming copy ISBN to our existing record, so that when on-order item records are loaded, they’ll attach to the existing bib. Does anyone have a less-manual way of dealing with a similar situation? I was wondering if it’s possible to update records in SQL – adding the ISBN in the last position (below other 020s), if we had a file of bib control IDs and associated ISBNs.

Thanks!

\--Kara

asked 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_25c7e83035504f33b740926b12965d5a.jpg"/> support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 7 years ago

<a id="upVoteAns_91"></a>[](#)

0

<a id="downVoteAns_91"></a>[](#)

Hello:

&nbsp;

I agree with using Mardc Edit.  You can generate a report\\SQL query with the information that you need and dump it into a CSV file.  Edit the information, and then dump it into Polaris using MARC Edit. 

We have done it here though I was the one building the reports\\SQL query not doing the editing.

Vincent Kruggel

&nbsp;

answered 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_25c7e83035504f33b740926b12965d5a.jpg"/> vkruggel@orl.bc.ca

- <a id="acceptAnswer_91"></a>[Accept as answer](#)

<a id="commentAns_91"></a>[Add a Comment](#)

<a id="upVoteAns_89"></a>[](#)

0

<a id="downVoteAns_89"></a>[](#)

As Vincent mentioned, this is not a SQL task for the faint of heart, you're talking about some major work here.

Rather than manipulating the MARC record in Polaris, you could use the fantasic/free MARCEdit tool http://marcedit.reeset.net/ to manipulate the incoming Acq record to add the ISBNs from your exisiting records to create the match point for the import process. MARCEdit includes the capablity to create a batch process and the ablility to merge MARC files using a variety of match points.

Here is my pseudo for how the process might work:

1.  Export the MARC records you want to order from Polaris
2.  Create a "basic" empty MARC record in MARCEdit using your order data
    1.  You seemed to indicate that you already have a file that has the exisiting Polaris Bib ID and the "new" ISBN
    2.  The basic MARC record should really only need the 001 and the 020
3.  Use MARCEdit to merge the two files on the 001 fields (the Polaris bib id)
4.  The new MARC file will now be the exported Polaris record with the new ISBN
5.  Import the new file to create your order records in Polaris
    1.  You'll need to match on the 020, but also tell Polaris to retain the 020 so your new ISBN will be included (this might include extra 020s that you don't want)
6.  Process your order as usual

None of the process above has been tested, but MARCEdit is a very powerful tool and its likely that you'll be able to use it in someway to help streamline your workflow.

Good luck!

answered 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_25c7e83035504f33b740926b12965d5a.jpg"/> wosborn@clcohio.org

- <a id="acceptAnswer_89"></a>[Accept as answer](#)

One option that would make this easier would be if Polaris itself could match and de-dup on the 001 (Polaris bib ID). If Polaris could do that you'd be able to eliminate a step. We have an enhancement to be able to match on the 001, it is ticket #480302 for us. You could ask your site manager to be added to the same request.

— wosborn@clcohio.org 7 years ago

<a id="commentAns_89"></a>[Add a Comment](#)

<a id="upVoteAns_86"></a>[](#)

0

<a id="downVoteAns_86"></a>[](#)

I am not saying it is not possible, but the Polaris.BibliographicRecords table has three triggers on it which will need to be handled also.  Polaris recommends against doing this with SQL.  Too many land mines.

&nbsp;

Work though it on your training database, but the time may not be worth the effort.

&nbsp;

Vincent Kruggel

Systems Analyst

Okanagan Regional Library

vkruggel@orl.bc.ca

answered 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_25c7e83035504f33b740926b12965d5a.jpg"/> vkruggel@orl.bc.ca

- <a id="acceptAnswer_86"></a>[Accept as answer](#)

<a id="commentAns_86"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Has anyone automated adding users/workstation to Polaris either through SQL or with the API?](https://iii.rightanswers.com/portal/controller/view/discussion/710?)
- [Does anyone delete PatronID info from the transaction database, so that if you're served with a subpeona, and can come back empty handed?](https://iii.rightanswers.com/portal/controller/view/discussion/1100?)
- [Is it possible to add multiple days to the Dates Closed table without adding them individually?](https://iii.rightanswers.com/portal/controller/view/discussion/761?)
- [Is there any harm in deleting old item/bib record templates?](https://iii.rightanswers.com/portal/controller/view/discussion/591?)
- [SQL for waiving certain fines](https://iii.rightanswers.com/portal/controller/view/discussion/192?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Vital start and stop scripts](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=151113130835651)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Adding a new UDF to patron registration](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930740605846)
- [How can I locate bibs that were generated during a Terminated import without using SQL Server Management Studio?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930570115337)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)