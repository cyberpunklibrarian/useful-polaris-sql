Adding number of "live" items to a report - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Adding number of "live" items to a report

0

28 views

I have a report with a multitude of columns (pasted below)

I'd like to add a another column with number of "live" copies systemwide

so whatever the correct the addition would be to get a Count of items with cir.ItemStatusID in (1,2,4,5,6,13,15)

select  oi.abbreviation as itembranchabbr,col.abbreviation as collectionabbr,ird.callnumber,br.browseauthor,br.browsetitle,ist.description as itemstatusdescr,cir.lifetimecirccount as itemlifetimecirccount,pyic.ytdcirccount as prevyearcirccount,cir.ytdcirccount as itemytdcirccount,cir.lastcheckoutrenewdate,cir.barcode as itembarcode,br.publicationyear,cir.firstavailabledate as firstavailabledate  from polaris.circitemrecords cir with (nolock) inner join polaris.itemrecorddetails ird with (nolock) on (cir.itemrecordid = ird.itemrecordid)  left join polaris.collections col with (nolock) on (cir.assignedcollectionid = col.collectionid)  inner join polaris.itemstatuses ist with (nolock) on (cir.itemstatusid = ist.itemstatusid)  inner join polaris.bibliographicrecords br with (nolock) on (cir.associatedbibrecordid = br.bibliographicrecordid)  inner join polaris.organizations oi with (nolock) on (cir.assignedbranchid = oi.organizationid)  left join polaris.prevyearitemscirc pyic with (nolock) on (cir.itemrecordid = pyic.itemrecordid) where cir.assignedbranchid in (@OrganizationList)   and cir.assignedcollectionid in (12)  and cir.itemstatusid in (1) order by polaris.dbo.erms_normalizegcn(0,ird.callnumber,40) asc

asked 1 year ago  by <img width="18" height="18" src="../../_resources/d183762366814ed08a392052479bb4f0.jpg"/>  crosenthal@pwcgov.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_815"></a>[](#)

0

<a id="downVoteAns_815"></a>[](#)

There are a couple of ways to accomplish this.

One way is to join to the view RWRITER_BibDerivedDataView (on BibliographicRecordID) and then subtract the NumberLostItems, NumberMissingItems, NumberClaimRetItems and NumberWithdrawnItems from the NumberOfItems in this view. This does not, however account for "bad" items in other statuses.

Another way is to join to a sub-select statement that gets a count of the number of items in the desired statuses. This may give a more accurate count of live copies.

The query below has both methods, so you can compare the results. One method may be quicker than the other, but I'm not sure. If one option looks better to you, you can remove the join to the view and the line with the calculation from the select statement--or remove the join to the sub-select "ic" (and the sub-select, of course) and the first element of the select statement as desired.

I had to change "polaris." to "Polaris.Polaris" to test the query. It should work fine with the additional Polaris, but you can delete that if it causes a problem.

Hope this helps,

JT

select ic.LiveItems as LiveCopies,
(rw.NumberofItems - rw.NumberClaimRetItems - rw.NumberLostItems - rw.NumberMissingItems - rw.NumberWithdrawnItems) as LiveCopiesRW,

oi.abbreviation as itembranchabbr,
col.abbreviation as collectionabbr,
ird.callnumber,br.browseauthor,
br.browsetitle,ist.description as itemstatusdescr,
cir.lifetimecirccount as itemlifetimecirccount,
pyic.ytdcirccount as prevyearcirccount,
cir.ytdcirccount as itemytdcirccount,
cir.lastcheckoutrenewdate,
cir.barcode as itembarcode,
br.publicationyear,
cir.firstavailabledate as firstavailabledate

from Polaris.Polaris.circitemrecords cir with (nolock)
inner join Polaris.Polaris.itemrecorddetails ird with (nolock) on (cir.itemrecordid = ird.itemrecordid)
left join Polaris.Polaris.collections col with (nolock) on (cir.assignedcollectionid = col.collectionid)
inner join Polaris.Polaris.itemstatuses ist with (nolock) on (cir.itemstatusid = ist.itemstatusid)
inner join Polaris.Polaris.bibliographicrecords br with (nolock) on (cir.associatedbibrecordid = br.bibliographicrecordid)
inner join Polaris.Polaris.organizations oi with (nolock) on (cir.assignedbranchid = oi.organizationid)
left join Polaris.Polaris.prevyearitemscirc pyic with (nolock) on (cir.itemrecordid = pyic.itemrecordid)
 
join polaris.polaris.RWRITER_BibDerivedDataView rw (nolock) on (br.BibliographicRecordID = rw.BibliographicRecordID)

join (select br.BibliographicRecordID, count(ir.ItemRecordID) as LiveItems from Polaris.Polaris.BibliographicRecords br with (nolock)
join polaris.polaris.itemrecords ir (nolock) on (br.BibliographicRecordID = ir.AssociatedBibRecordID and ir.ItemStatusID in (1,2,4,5,6,13,15))
group by br.BibliographicRecordID) ic
on (br.BibliographicRecordID = ic.BibliographicRecordID)

where cir.assignedbranchid in (@OrganizationList)
and cir.assignedcollectionid in (12)
and cir.itemstatusid in (1)

order by Polaris.dbo.erms_normalizegcn(0,ird.callnumber,40) asc

answered 1 year ago  by <img width="18" height="18" src="../../_resources/d183762366814ed08a392052479bb4f0.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_815"></a>[Accept as answer](#)

<a id="commentAns_815"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [SQL for circulation numbers](https://iii.rightanswers.com/portal/controller/view/discussion/94?)
- [Report to find Items added by Material Type in a certain year?](https://iii.rightanswers.com/portal/controller/view/discussion/430?)
- [How to Find Definitions of ActionTakenID Numbers](https://iii.rightanswers.com/portal/controller/view/discussion/96?)
- [Do reporting by subject headings?](https://iii.rightanswers.com/portal/controller/view/discussion/1059?)

### Related Solutions

- [Increasing number of rows in SimplyReports added to record set](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930422767289)
- [Export OCLC Control Numbers from Polaris to update WorldCat Holdings using Simply Reports – Added Record Query Method](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181128103351248)
- [Long Distance Telephony](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930882128740)
- [SAN number for EDI ordering](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930858154155)
- [Maximum item call number length](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930857542877)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)