[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL for total holds

0

97 views

I'm looking to find a list of patrons who have more than a certain number of active holds. I looked in Simply Reports and couldn't find a way to get a list of patrons with "greater than or equal to X" holds. Can anyone provide a query for that? Thanks :)

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_0798a43d23aa4a6b84d29ecb5fc4f262.jpg"/> musack@bcls.lib.nj.us

[holds](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=96#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_1397"></a>[](#)

0

<a id="downVoteAns_1397"></a>[](#)

Answer Accepted

I think the following SQL below will get you where you want to be... I recommend you visit the IUG forums https://forum.innovativeusers.org/ as there is a bunch of resources there for custom SQL. You may also want to look at the SysHoldStatuses table to make sure the desired statuses are included in your "Active" query. I was not sure if you meant "Active" as in the hold is in Active status or if you meant that the hold could be filled, like Pending, Active, etc...

&nbsp;

SELECT  
PatronID,  
COUNT(\*) as HoldsCount  
FROM Polaris.SysHoldRequests shr WITH(NOLOCK)  
WHERE SysHoldStatusID = 3 --Active  
GROUP BY PatronID  
HAVING  
COUNT(\*) >= 16; -- Replace X with the minimum number of holds

answered 1 year ago by ![eric.young@phoenix.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_eric.young@phoenix.gov20191029113427.jpg) eric.young@phoenix.gov

<a id="commentAns_1397"></a>[Add a Comment](#)

<a id="upVoteAns_1398"></a>[](#)

0

<a id="downVoteAns_1398"></a>[](#)

Thank you!!

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_0798a43d23aa4a6b84d29ecb5fc4f262.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_1398"></a>[Accept as answer](#)

<a id="commentAns_1398"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for holds information](https://iii.rightanswers.com/portal/controller/view/discussion/1154?)
- [SQL Find hold queue override by staff](https://iii.rightanswers.com/portal/controller/view/discussion/1388?)
- [Is there an SQL to change item level holds to bib level holds?](https://iii.rightanswers.com/portal/controller/view/discussion/407?)
- [I need a SQL for locating hold requests where the hold has been denied by a library](https://iii.rightanswers.com/portal/controller/view/discussion/843?)
- [Does anyone have an SQL for a request ratio?](https://iii.rightanswers.com/portal/controller/view/discussion/394?)

### Related Solutions

- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)
- [Hold Processing SQL jobs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930920183263)
- [Query to find unfillable holds due to volume data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930242167096)
- [Unable to delete two holds, error message about ILL$Messages appears](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930410469399)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)