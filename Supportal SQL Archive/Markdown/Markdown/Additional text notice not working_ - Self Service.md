Additional text notice not working? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Additional text notice not working?

0

64 views

We just upgraded to 6.3. We've noticed lately that patrons do not receive text messages when they have 'additional text' checked in their notificaiton preferences. They will receive the phone or email notice, but not the additional text. If they have text as the only notification it works and the text goes out successfully. 

I'm not sure this happened since the upgrade, or was happening before that and we just didn't notice.

Has anyone else noticed this? I don't see it in known issues unless I'm missing something.

Thanks.

Cecilia Smiley

asked 1 year ago  by <img width="18" height="18" src="../../_resources/68a7055f3d12426a80662290f8f0f5b3.jpg"/>  csmiley@leegov.com

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 1 year ago

<a id="upVoteAns_797"></a>[](#)

0

<a id="downVoteAns_797"></a>[](#)

You can use this SQL to locate patrons who should have an additional text message from 12/01/2019 to today:

select nh.* from polaris.polaris.PatronRegistration pr
join polaris.polaris.Patrons p on
p.PatronID = pr.PatronID
join Results.Polaris.NotificationHistory nh on
nh.PatronId = pr.PatronID
where pr.EnableSMS = 1 and TxtPhoneNumber = 1 and Phone1CarrierID = 23 and PhoneVoice1 is not null and pr.DeliveryOptionID != 8 and nh.DeliveryOptionId = 8 and nh.NoticeDate > '2019-12-01'

If you're hosted, use this SQL search from a patron find tool:

select distinct(pr.patronid) from polaris.polaris.PatronRegistration pr
join polaris.polaris.Patrons p on
p.PatronID = pr.PatronID
join Results.Polaris.NotificationHistory nh on
nh.PatronId = pr.PatronID
where pr.EnableSMS = 1 and TxtPhoneNumber = 1 and Phone1CarrierID = 23 and PhoneVoice1 is not null and pr.DeliveryOptionID != 8 and nh.DeliveryOptionId = 8 and nh.NoticeDate > '2019-12-01'

In my system, the patrons are still getting text messages, we have 26 million annual circ and in the last 30 days, we've had about 30 patrons who fell into this situation of getting an additional text message, so you'd need to adjust expectations based on your volume.

I also confirmed as you can see in the attached screenshot that additional txt do NOT show up in the patron status. In the screenshot, you see the patron is set to email w/additional txt, you can see the email in the notice history, but not the text, yet our text delivery log shows that we did send them a text. So if you're only using that to determine if the patron got the text message, it can be misleading.

answered 1 year ago  by <img width="18" height="18" src="../../_resources/68a7055f3d12426a80662290f8f0f5b3.jpg"/>  wosborn@clcohio.org

- <a id="acceptAnswer_797"></a>[Accept as answer](#)

- [email-additional-txt.png](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/656?fileName=656-797_email-additional-txt.png "email-additional-txt.png")

<a id="commentAns_797"></a>[Add a Comment](#)

<a id="upVoteAns_794"></a>[](#)

0

<a id="downVoteAns_794"></a>[](#)

Wes,

Yes - it's all set up correctly. It has been since Polaris first offered texting. In fact this worked fine until recently. We've tested thoroughly with phones set up with at least two different providers (Sprint and Verizon). There is no indication of a text going out when it should (and used to) according to the patron record and according to personal experience. 

This only happens when texting is set up as **additional** notice. The other method (email or phone) works, a call or email is sent, but no text. If the record is set up to text as primary/only notification option it works fine.

It's just puzzling and we're not sure if it just started with our upgrade to 6.3 or at some point before that.

I don't think it's a huge issue as I don't see a lot of patrons with that set up, i.e. email/phone notices with an additional text. It seems a bit much, but that is just my own opinion after trying that set up. One notice is enough for me but we just find it odd and wondered if anyone else has the same experience.

Thanks for the suggestions.

Also - I did go back to known issues for 6.2 since we went straight from 6.1 (where it worked) to 6.3 (where it doesn't appear to work) and didn't see anything in either document.

Thanks, Cecilia

answered 1 year ago  by <img width="18" height="18" src="../../_resources/68a7055f3d12426a80662290f8f0f5b3.jpg"/>  csmiley@leegov.com

- <a id="acceptAnswer_794"></a>[Accept as answer](#)

<a id="commentAns_794"></a>[Add a Comment](#)

<a id="upVoteAns_793"></a>[](#)

0

<a id="downVoteAns_793"></a>[](#)

Also, a quick note on looking at known issues, make sure you **search back through all versions of known issues**. The 6.3 known issues only include issues that were fixed or reported in 6.3, if the problem was discovered in 5.6 but still exisits in 6.3, it won't be listed on the 6.3 known issues page.

answered 1 year ago  by <img width="18" height="18" src="../../_resources/68a7055f3d12426a80662290f8f0f5b3.jpg"/>  wosborn@clcohio.org

- <a id="acceptAnswer_793"></a>[Accept as answer](#)

<a id="commentAns_793"></a>[Add a Comment](#)

<a id="upVoteAns_792"></a>[](#)

0

<a id="downVoteAns_792"></a>[](#)

Is this a report from patron's or are you just saying you don't see the additional text listed?

Here are the notes we have in our internal documentation about additional texts:

- Patrons can also receive text messages**in addition**to their primary notification method.
- In addition to being enabled on the patron account,**the notice type must also be enabled**to support the Additional TXT feature. See the[Notice Types section above](https://discourse.clcohio.org/t/polaris-notices-overview/3368#heading--notice-types)for examples of the different notice types.
- Additional text messages don’t have a transaction history entry or a notification history entry in Polaris, so it’s NOT possible to tell that the patron received the additional text message in Polaris, or when the message was sent.

We've been on 6.3 for a couple of months and haven't had any reports of issues with additional text, but sometimes it can take awhile for us to hear about something like this.

answered 1 year ago  by <img width="18" height="18" src="../../_resources/68a7055f3d12426a80662290f8f0f5b3.jpg"/>  wosborn@clcohio.org

- <a id="acceptAnswer_792"></a>[Accept as answer](#)

<a id="commentAns_792"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [T-Mobile patrons not receiving text notifications](https://iii.rightanswers.com/portal/controller/view/discussion/982?)
- [What did you run into when implementing Email receipts and Text notices?](https://iii.rightanswers.com/portal/controller/view/discussion/553?)
- [Additional gender options](https://iii.rightanswers.com/portal/controller/view/discussion/614?)
- [Notices](https://iii.rightanswers.com/portal/controller/view/discussion/59?)
- [Remove the notification option of fax?](https://iii.rightanswers.com/portal/controller/view/discussion/694?)

### Related Solutions

- [Setting up phone notification with additional TXT messages](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930542970896)
- [Long Distance Phone Numbers and Text Messages](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930379236869)
- [Remove held-till date on text notices for one branch's holds](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930458066960)
- [How should phone numbers be formatted in patron registration?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930411345829)
- [Would like a way to gather all patrons with no notification option selected](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930285364780)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)