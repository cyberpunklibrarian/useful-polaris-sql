[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Update item record data using item control number as a match point?

0

30 views

We want to populate statistical categories for **existing** item records with vendor supplied data, but aren't sure if there's a way to match the returned item strings without using a barcode, which only allows for saving provisionally or not at all. Is it possible to match on the item control number so the data populates correctly? 

Pam

asked 2 months ago by <img width="18" height="18" src="../_resources/default-avatar_c080abce332c432ebaa886bf7ab9f163.jpg"/> pswaidner@indypl.org

<a id="comment"></a>[Add a Comment](#)

## 5 Replies   last reply 2 months ago

<a id="upVoteAns_1441"></a>[](#)

0

<a id="downVoteAns_1441"></a>[](#)

No problem, glad I could be of some help!

answered 2 months ago by <img width="18" height="18" src="../_resources/default-avatar_c080abce332c432ebaa886bf7ab9f163.jpg"/> jtenter@sasklibraries.ca

- <a id="acceptAnswer_1441"></a>[Accept as answer](#)

<a id="commentAns_1441"></a>[Add a Comment](#)

<a id="upVoteAns_1440"></a>[](#)

0

<a id="downVoteAns_1440"></a>[](#)

Yes, that was the initial idea, but you're right that there's no corresponding item id map. I will reach out to our site administrator for further guidance on this. Thank you for helping me think it through!

Pam

answered 2 months ago by <img width="18" height="18" src="../_resources/default-avatar_c080abce332c432ebaa886bf7ab9f163.jpg"/> pswaidner@indypl.org

- <a id="acceptAnswer_1440"></a>[Accept as answer](#)

<a id="commentAns_1440"></a>[Add a Comment](#)

<a id="upVoteAns_1439"></a>[](#)

0

<a id="downVoteAns_1439"></a>[](#)

Hi Pam,

So then are you thinking the Item Record ID would be in one of the 949 bib tag subfields, to match the data, or how would the Item ID be be used to find and update the Statistical Code (possibly manually?)

I don't think the 949 has a subfield for the Item Record ID, at least not from what I've seen.  The help articles don't seem to include specific details, though, so you may want to confirm with your site manager if that was the approach you were thinking of.

\-Jason

answered 2 months ago by <img width="18" height="18" src="../_resources/default-avatar_c080abce332c432ebaa886bf7ab9f163.jpg"/> jtenter@sasklibraries.ca

- <a id="acceptAnswer_1439"></a>[Accept as answer](#)

<a id="commentAns_1439"></a>[Add a Comment](#)

<a id="upVoteAns_1437"></a>[](#)

0

<a id="downVoteAns_1437"></a>[](#)

Hi Jason,

The idea is that we'd send our bib and item data to the vendor, and they'd embed the statistical code in the 949 tag. I'm wondering if it's possible to update the code when records are reimported by matching on the item record id. I don't think SQL would play in.

Pam

answered 2 months ago by <img width="18" height="18" src="../_resources/default-avatar_c080abce332c432ebaa886bf7ab9f163.jpg"/> pswaidner@indypl.org

- <a id="acceptAnswer_1437"></a>[Accept as answer](#)

<a id="commentAns_1437"></a>[Add a Comment](#)

<a id="upVoteAns_1436"></a>[](#)

0

<a id="downVoteAns_1436"></a>[](#)

Hi Pam,

I'd like to help if I can, but I want to make sure I understand the question.

Am I correct that you have received a list of data about items from a vendor, and you'd like to update the Statistical Code item field using those details using SQL, but you're wondering if you can use the item control number to match the data.  Is that right, or am I misunderstanding?

\-Jason

answered 2 months ago by <img width="18" height="18" src="../_resources/default-avatar_c080abce332c432ebaa886bf7ab9f163.jpg"/> jtenter@sasklibraries.ca

- <a id="acceptAnswer_1436"></a>[Accept as answer](#)

<a id="commentAns_1436"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Use Item Record Call Numbers to Bulk Update Bib MARC Tag](https://iii.rightanswers.com/portal/controller/view/discussion/235?)
- [How should I be entering call number info in the 092 to make the subfields correctly correspond with item record call number fields?](https://iii.rightanswers.com/portal/controller/view/discussion/313?)
- [Deleting item records with holds](https://iii.rightanswers.com/portal/controller/view/discussion/32?)
- [e-content with no item records](https://iii.rightanswers.com/portal/controller/view/discussion/251?)
- [SQL for items with information in "Name of piece"](https://iii.rightanswers.com/portal/controller/view/discussion/1345?)

### Related Solutions

- [Export OCLC Control Numbers from Polaris to update WorldCat Holdings using Simply Reports – Record Set Method](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181128101912576)
- [Do the home and owning branch need to match for floating item records?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930466429189)
- [Maximum item call number length](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930857542877)
- [Export OCLC Control Numbers from Polaris to update WorldCat Holdings using Simply Reports – Added Record Query Method](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181128103351248)
- [Using MARCEdit to remove authority records containing corrupted diacritics from the weekly update files](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200228111744955)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)