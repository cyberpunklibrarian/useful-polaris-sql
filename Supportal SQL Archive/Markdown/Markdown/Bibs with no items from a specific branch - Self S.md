[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Bibs with no items from a specific branch

0

52 views

Is there a way to find all bib records that do NOT have any items from a particular branch? We're currently changing some of our T items to YA, starting at our main branch (we'll call it HQ) and are changing the bibs as we change items. While a lot of the bibs will be changed by going through all the items at HQ, there are some bibs that only have items from other branches. So, what I need to do is find the bibs that only have items from other branches, not HQ. Does anyone have an SQL for this?

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_6a3caee194d7411eaaf4fc8bdcab47df.jpg"/> musack@bcls.lib.nj.us

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_1266"></a>[](#)

0

<a id="downVoteAns_1266"></a>[](#)

Thank you!! I'll give that a try :) I really appreciate your help

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_6a3caee194d7411eaaf4fc8bdcab47df.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_1266"></a>[Accept as answer](#)

You're welcome!  Let me know if you need that tweaked at all and I'll see what I can do.

— jjack@aclib.us 2 years ago

Worked like a charm! Exactly what I was looking for ... thanks!!  
<br/>

— musack@bcls.lib.nj.us 2 years ago

<a id="commentAns_1266"></a>[Add a Comment](#)

<a id="upVoteAns_1265"></a>[](#)

0

<a id="downVoteAns_1265"></a>[](#)

There's probably a more efficient way to do this, but this should work.  Replace @Collection with the collection ID(s) you're interested in, and @Branch with the ID for the branch you want none of the items to be at.

This will *only* turn up bibs with at least one item which shows in the PAC.

SELECT DISTINCT cir.AssociatedBibRecordID  
FROM CircItemRecords cir with (NOLOCK)  
WHERE cir.AssignedCollectionID IN (@Collection)  
AND cir.DisplayInPAC = 1  
AND cir.AssociatedBibRecordID NOT IN (  
SELECT AssociatedBibRecordID  
FROM CircItemRecords cir with (NOLOCK)  
WHERE AssignedBranchID IN (@Branch)  
)

So for us, bibs with items in Early Readers, but none of them at HQ, would be:

SELECT DISTINCT cir.AssociatedBibRecordID  
FROM CircItemRecords cir with (NOLOCK)  
WHERE cir.AssignedCollectionID IN (12)  
AND cir.DisplayInPAC = 1  
AND cir.AssociatedBibRecordID NOT IN (  
SELECT AssociatedBibRecordID  
FROM CircItemRecords cir with (NOLOCK)  
WHERE AssignedBranchID IN (3)  
)

If you need to list more than one collection, you can just separate the collection IDs with commas.

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_6a3caee194d7411eaaf4fc8bdcab47df.jpg"/> jjack@aclib.us

- <a id="acceptAnswer_1265"></a>[Accept as answer](#)

<a id="commentAns_1265"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Workflow to prevent new items floating when branches are using the "load balancing" floating property](https://iii.rightanswers.com/portal/controller/view/discussion/126?)
- [Use Item Record Call Numbers to Bulk Update Bib MARC Tag](https://iii.rightanswers.com/portal/controller/view/discussion/235?)
- [changing branch with bulk change](https://iii.rightanswers.com/portal/controller/view/discussion/35?)
- [Bib record modifiers](https://iii.rightanswers.com/portal/controller/view/discussion/1380?)
- [SQL for Bibs lacking local call #](https://iii.rightanswers.com/portal/controller/view/discussion/605?)

### Related Solutions

- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Bib Call Number is not automatically copying to Item Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930769128110)
- [Do the home and owning branch need to match for floating item records?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930466429189)
- [Finding unlinked bibs or authority records for database cleanup](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930305889326)
- [Collection workform "branches" checkboxes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930545936496)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)