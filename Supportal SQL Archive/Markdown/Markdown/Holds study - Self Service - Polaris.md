[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Holds study

0

140 views

Good morning Polariiiverse

&nbsp;

We'd like to do a study for how long it takes holds to be filled. Has anyone else done this, and if so do you have any tips and /or "I wish we'd thought of this" that you'd be willing to share?

Also any SQL?

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_6449f4db26ce4654b4d13bf8eb9f61e8.jpg"/> aal-shabibi@champaign.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 4 years ago

<a id="upVoteAns_714"></a>[](#)

0

<a id="downVoteAns_714"></a>[](#)

Hi Marilyn, yes we'd looked at that but staff were not convinced it was accurate because it does include on orders.

Hope all is well with you.

&nbsp;

Amy

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_6449f4db26ce4654b4d13bf8eb9f61e8.jpg"/> aal-shabibi@champaign.org

- <a id="acceptAnswer_714"></a>[Accept as answer](#)

<a id="commentAns_714"></a>[Add a Comment](#)

<a id="upVoteAns_713"></a>[](#)

0

<a id="downVoteAns_713"></a>[](#)

Hi Amy,

There is a toolbar report called Request Time to Fill. I have not looked at the query behind it, but I think it still includes requests for titles that are on-order. That doesn't work for us, especially as pre-publication orders skew the results. It may be a good starting point, however.

Marilyn

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_6449f4db26ce4654b4d13bf8eb9f61e8.jpg"/> mborg@gmilcs.org

- <a id="acceptAnswer_713"></a>[Accept as answer](#)

<a id="commentAns_713"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Are cancelled holds considered holds in holds item count?](https://iii.rightanswers.com/portal/controller/view/discussion/414?)
- [Combined Hold Slips](https://iii.rightanswers.com/portal/controller/view/discussion/68?)
- [SQL for holds information](https://iii.rightanswers.com/portal/controller/view/discussion/1154?)
- [Any way or workaround to display an alias on Hold Pickup Slips for open hold shelves?](https://iii.rightanswers.com/portal/controller/view/discussion/1068?)
- [Does anyone know how to put a patron's contact preference on their holds slip?](https://iii.rightanswers.com/portal/controller/view/discussion/652?)

### Related Solutions

- [Holds Not Trapping at Self-Check](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930897357365)
- [Request did not appear on Pending list despite "In" item](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930397510485)
- [How does Polaris determine requests to be duplicates?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930617910193)
- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)
- [Restrict ability to move patrons in the Holds Queue](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930592096942)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)