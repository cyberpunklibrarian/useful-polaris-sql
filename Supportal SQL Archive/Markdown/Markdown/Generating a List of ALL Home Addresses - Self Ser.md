Generating a List of ALL Home Addresses - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Generating a List of ALL Home Addresses

0

20 views

I would like to generate a list of **all** existing patron home addresses for specific patron types, even if the patron has a different address type listed for their Notices Address in their record.

So far, I have come up with the following SQL query:

select p.Barcode,pr.PatronFullName,addr.StreetOne,addr.StreetTwo,pos.City,pos.State,pos.PostalCode
from Polaris.PatronRegistration pr with (nolock)
inner join Polaris.Patrons p with (nolock)
on (pr.PatronID = p.PatronID)
left join Polaris.PatronAddresses pa with (nolock)
on (pr.PatronID = pa.PatronID and pa.AddressTypeID = 2 )
left join Polaris.Addresses addr with (nolock)
on (pa.AddressID = addr.AddressID)
left join Polaris.PostalCodes pos with (nolock)
on (addr.PostalCodeID = pos.PostalCodeID)
where p.PatronCodeID in (7,17,18,20,25,27,28,32,34,42,47)
AND pa.AddressLabelID = 1
order by StreetOne

The problem that I am finding, though, is that this will only pull home addresses if the patron has chosen "Home" for their Notice Address in their patron record. In other words, if a patron shows up on the results list when I run this the first time, and I then go into their record and update the Notice Address from "Home" to "Alternate" if they have a second address listed, the patron will be removed from the search results when I run this query a second time, **even though the Home Address still exists in the patron record.**

Is there a way to get a list of **all** home addresses for these patron types, even if the patron records are set to "Alternative" in the Notice Address field?

Thanks for any assistance in figuring this out!

Andrew Teeple

Lake County (IN) Public Library

asked 1 month ago  by <img width="18" height="18" src="../../_resources/56f4d15cd9a44613843068112aea63f2.jpg"/>  ateeple@lcplin.org

[patron address](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=172#t=commResults) [patron registration](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=238#t=commResults) [sql patron](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=387#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 month ago

<a id="upVoteAns_1145"></a>[](#)

0

<a id="downVoteAns_1145"></a>[](#)

Answer Accepted

Hi Andrew,

I was looking over your query to see how you were pulling the info you needed, and I noticed that you have a join on 'and pa.AddressTypeID = 2' which, if I read the address type list correctly, is literally joining on 'address type = notice' so I think that before you get to the where statement, the join is filtering out all addresses except the one marked 'notice'. I cut out just that bit, and your query seems to work fine, including listing multiple addresses for a patron if more than one is marked as a home address.

`select p.Barcode,pr.PatronFullName,addr.StreetOne,addr.StreetTwo,pos.City,pos.State,pos.PostalCode`
`from Polaris.PatronRegistration pr with (nolock)`
`inner join Polaris.Patrons p with (nolock)`
`on (pr.PatronID = p.PatronID)`
`left join Polaris.PatronAddresses pa with (nolock)`
`on (pr.PatronID = pa.PatronID and pa.AddressTypeID = 2 ) <== I cut the highlighted bit right there`
`left join Polaris.Addresses addr with (nolock)`
`on (pa.AddressID = addr.AddressID)`
`left join Polaris.PostalCodes pos with (nolock)`
`on (addr.PostalCodeID = pos.PostalCodeID)`
`where p.PatronCodeID in (7,17,18,20,25,27,28,32,34,42,47)`
`AND pa.AddressLabelID = 1`
`order by StreetOne`

I hope that helps you get the address information you need.

--Daniel

answered 1 month ago  by <img width="18" height="18" src="../../_resources/56f4d15cd9a44613843068112aea63f2.jpg"/>  ddunphy@aclib.us

Thanks, Daniel! Cutting that part out, then adding a "DISTINCT" qualifier in the SELECT line, seems to have done the trick (I added the DISTINCT since my results were showing up with duplicates for many patrons). Thanks for your help on this!

— ateeple@lcplin.org 1 month ago

<a id="commentAns_1145"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Inserting corrected addresses in Patron registration](https://iii.rightanswers.com/portal/controller/view/discussion/364?)
- [ORS Selection list](https://iii.rightanswers.com/portal/controller/view/discussion/72?)
- [Advice On Use Of Address Check](https://iii.rightanswers.com/portal/controller/view/discussion/899?)
- [Anyone having a question mark symbol randomly occupying the Street Address Line 2 on a patron's registration?](https://iii.rightanswers.com/portal/controller/view/discussion/613?)
- [List all items with a Special Item Check In Note?](https://iii.rightanswers.com/portal/controller/view/discussion/548?)

### Related Solutions

- [Outreach picklist generation](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930564049678)
- [Unable to delete Outreach route](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180820155708472)
- [Changing Address Check Term](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930723806393)
- [Locating patrons using a specific address type](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180628133452068)
- [Wrong default postal code](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180316120042002)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)