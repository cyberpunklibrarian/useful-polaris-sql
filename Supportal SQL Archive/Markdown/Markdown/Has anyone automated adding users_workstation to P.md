[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Has anyone automated adding users/workstation to Polaris either through SQL or with the API?

1

126 views

We're looking at automating user/workstation creation and permission changes. We have a lot of users who come and go and it becomes time consuming to constantly make changes. Has anyone found a way to do this?

&nbsp;

Thanks!

Spencer Pottebaum

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_9bd3de21d64943d4aaa548d653c52ba0.jpg"/> spencerp@wccls.org

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 4 years ago

<a id="upVoteAns_900"></a>[](#)

0

<a id="downVoteAns_900"></a>[](#)

Now that I've had a chance to catch my breathe from the last few weeks, here are some details about our user creation script.

- It's set up as a Python virtual environment, where we can give different commands (add and delete user, mainly)
- It uses a few different Python packages, including pymssql to connect to Polaris.  It's been a while since I've thought through what each package does, but I've attached the requirements file in case it helps point you in the right direction for something.
- It connects to our support portal (Freshworks), and grabs all new user requests from tickets (based on ticket metadata)
- When a user is created, the script first adds them to our Active Directory, then to Polaris
- The ticket needs to include a user to copy permissions from:
    - When adding to Polaris, the script first confirms the user to copy from exists, and that the new user does not (against the PolarisUsers table).  
    - The new user is added to the users table, and we use the "Polaris.SA_Permission_MergeUserGroups" SProc to copy the permissions from the existing user.
- One challenge we've had is that this process is great for new users, but for returning users it's not effective.  We suspend but don't delete users when they leave, so their footprint remains in the system, which means they generate an error in the script.  I've added functionality to allow this, but got sidetracked before it went live - the below steps seemed to work well on testing, though:
    - Instead of throwing an error if the new username already exists, the script checks whether they're suspended (UsersPPPP table, attrID = 659), and allows updates in these cases.  The script doesn't try to create a profile on the users table when this occurs, instead it removes the suspended flag.
    - Any existing permissions are removed before adding new permissions.  I had to run a trace to identify how Polaris does this, and used that to build a SProc.  The SProc uses a CURSOR to add permission (from the GroupUsers table) to a table valued parameter, which uses the SA_Permission_UpdateSettings SProc to remove them from the user.  I'm reluctant to post my script because we haven't used it ourselves enough to be fully confident in it, but I've given enough detail to build from if you want to incorprate something similar.
- Passwords are created using a randomizing function.  Once the user is created, the new login and password are printed as a note on the support ticket.
- I also included some IF statements to clean-up spaces and weird characters that staff include with names in the ticket - it wasn't a big concern, but it gave me the opportunity to refresh on Python.

&nbsp;

I think that covers things at a high level.  Hope this helps!

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_9bd3de21d64943d4aaa548d653c52ba0.jpg"/> jtenter@sasklibraries.ca

- <a id="acceptAnswer_900"></a>[Accept as answer](#)

- [requirements.txt](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/710?fileName=710-900_requirements.txt "requirements.txt")

<a id="commentAns_900"></a>[Add a Comment](#)

<a id="upVoteAns_869"></a>[](#)

0

<a id="downVoteAns_869"></a>[](#)

My team (before I joined) developed a Python script to do this work.  We have a support portal (FreshWorks), and the script checks for user creation tickets, then creates the Active Directory profile and Polaris account.  It also handles account removals.

I'd be happy to provide some info on how it's constructed, in case that helps, but it would take some good Python skills to set something similar up.

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_9bd3de21d64943d4aaa548d653c52ba0.jpg"/> jtenter@sasklibraries.ca

- <a id="acceptAnswer_869"></a>[Accept as answer](#)

Yeah I'd definitely be interested in hearing what you guys setup, I haven't programmed in Python in a few years but I'm familiar with it.

— spencerp@wccls.org 4 years ago

<a id="commentAns_869"></a>[Add a Comment](#)

<a id="upVoteAns_867"></a>[](#)

0

<a id="downVoteAns_867"></a>[](#)

I have glazed over the SQL options years back but stopped because of available time. I too would be interested to hear what others have come up with on this topic. 

Thanks!

Eric Young

answered 4 years ago by ![eric.young@phoenix.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_eric.young@phoenix.gov20191029113427.jpg) eric.young@phoenix.gov

- <a id="acceptAnswer_867"></a>[Accept as answer](#)

<a id="commentAns_867"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [adding info to bibs via SQL script?](https://iii.rightanswers.com/portal/controller/view/discussion/107?)
- [Automating holds function based on dimension of item?](https://iii.rightanswers.com/portal/controller/view/discussion/352?)
- [Query Syntax for a Linked Server](https://iii.rightanswers.com/portal/controller/view/discussion/491?)
- ["New Titles" on Website using API](https://iii.rightanswers.com/portal/controller/view/discussion/106?)
- [Has anyone automated the process of billing Phone customers](https://iii.rightanswers.com/portal/controller/view/discussion/1263?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Automated Acquisitions Guide.pdf](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160506100947648)
- [Automated Acquisitions Guide 6.6](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200930145441557)
- [Automated Acquisitions Guide 6.7](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210104100922863)
- [Automating Polaris Client Deployment (Polaris 5.5)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170725153908464)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)