Number of Renewals for Currently Checked Out Items - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Number of Renewals for Currently Checked Out Items

0

146 views

We're in search of an SQL that would give a list of items that are currently checked out and includes the number of times the item has been renewed.  Our renewal limit is two so we're searching for items that exceed this number.  We do not use auto renewal.  If the SQL could also include the patronID or barcode that would be great. 

Thanks in advance,

Anne Krulik

Burlington County Library System

asked 3 years ago  by <img width="18" height="18" src="../../_resources/b2d906d285604e22bd7dc85abf476843.jpg"/>  akrulik@bcls.lib.nj.us

[renewals](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=80#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 5 Replies   last reply 3 years ago

<a id="upVoteAns_205"></a>[](#)

0

<a id="downVoteAns_205"></a>[](#)

Answer Accepted

Oh, sorry, I didn't know you wanted it to work in the client.  I'm not aware of any way to tell the client what columns you want shown in the results.

To get this to work in the client, you'd have to go into the search for item records (not bib records) and then switch to the SQL search mode and use this:
SELECT ItemRecordID
FROM ItemCheckouts with (NOLOCK)
WHERE Renewals > 2

The Circulation screen (with the red circular arrow) on each item should show both the number of renewals taken and the current borrower.

answered 3 years ago  by <img width="18" height="18" src="../../_resources/b2d906d285604e22bd7dc85abf476843.jpg"/>  jjack@aclib.us

<a id="commentAns_205"></a>[Add a Comment](#)

<a id="upVoteAns_208"></a>[](#)

0

<a id="downVoteAns_208"></a>[](#)

I wanted to let you know that the first SQL you sent worked too.  We are able to sort that list by PatronID which is a big help.

SELECT ItemRecordID, Renewals, PatronID
FROM ItemCheckouts with (NOLOCK)
WHERE Renewals > 2

Thanks again.

answered 3 years ago  by <img width="18" height="18" src="../../_resources/b2d906d285604e22bd7dc85abf476843.jpg"/>  akrulik@bcls.lib.nj.us

- <a id="acceptAnswer_208"></a>[Accept as answer](#)

Great!  I'm glad I could help.

— jjack@aclib.us 3 years ago

<a id="commentAns_208"></a>[Add a Comment](#)

<a id="upVoteAns_206"></a>[](#)

0

<a id="downVoteAns_206"></a>[](#)

It worked!  I can change the greater than number which is a big help, too.

SELECT ItemRecordID
FROM ItemCheckouts with (NOLOCK)
WHERE Renewals > 2

Thanks again!!

answered 3 years ago  by <img width="18" height="18" src="../../_resources/b2d906d285604e22bd7dc85abf476843.jpg"/>  akrulik@bcls.lib.nj.us

- <a id="acceptAnswer_206"></a>[Accept as answer](#)

<a id="commentAns_206"></a>[Add a Comment](#)

<a id="upVoteAns_204"></a>[](#)

0

<a id="downVoteAns_204"></a>[](#)

Thanks for the quick response.  Is this SQL supposed to work in the F12 find tool in the staff client?  If not, I'll need to have one of my co-workers run it on the server.  The fact that it has the PatronID will work.  

This is message I got when I attempted to run it in F12 find tool.

![](../../_resources/c4f2ea5ec6ba4893b3048ebd92eb4105.png)

answered 3 years ago  by <img width="18" height="18" src="../../_resources/b2d906d285604e22bd7dc85abf476843.jpg"/>  akrulik@bcls.lib.nj.us

- <a id="acceptAnswer_204"></a>[Accept as answer](#)

<a id="commentAns_204"></a>[Add a Comment](#)

<a id="upVoteAns_203"></a>[](#)

0

<a id="downVoteAns_203"></a>[](#)

This should work, unless you want the item's barcode and/or title on the report too:

SELECT ItemRecordID, Renewals, PatronID
FROM ItemCheckouts with (NOLOCK)
WHERE Renewals > 2

If this isn't what you're looking for, please let me know.

answered 3 years ago  by <img width="18" height="18" src="../../_resources/b2d906d285604e22bd7dc85abf476843.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_203"></a>[Accept as answer](#)

<a id="commentAns_203"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [I need a report of the total number of items checked out by user ID where the total number out is greater than 35](https://iii.rightanswers.com/portal/controller/view/discussion/961?)
- [Interlibrary Loan and Automatic Renewals](https://iii.rightanswers.com/portal/controller/view/discussion/485?)
- [We are rolling out hotspots for our patrons. We have created the records for the Hotspots. How can we restrict checkouts... we want no renewals... and you have to wait 48 hours prior to checking out the device again. Does anyone have any advice ?](https://iii.rightanswers.com/portal/controller/view/discussion/596?)
- [Automatically Check In Items](https://iii.rightanswers.com/portal/controller/view/discussion/1064?)
- [Will an item renew if we have to override a checkout?](https://iii.rightanswers.com/portal/controller/view/discussion/987?)

### Related Solutions

- [Items not auto-renewed for specific patron](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930760925871)
- [Changing renewal limits system wide](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930343101412)
- [Auto-renew and patron initiated blocking conditions](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930723290342)
- [Renewal numbers differ in PAC from client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930678864373)
- [Leap receipts are consistently long regardless the number of items being checked out](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930992303843)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)