[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# SQL Search for high Circ items

0

64 views

I know that there has to be a really simple way to do this but I can't seem to figure it out or piece it together.

I need to search for all items that have a lifetime circ total of greater than 25 in the past 3 years

Thanks

asked 4 months ago by <img width="18" height="18" src="../_resources/default-avatar_a560d445f6a94cbbb829363ebf468ece.jpg"/> amber@wpl.lib.in.us

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 4 months ago

<a id="upVoteAns_1421"></a>[](#)

0

<a id="downVoteAns_1421"></a>[](#)

Answer Accepted

Here's a item find tool SQL search that gives you all items that have a first available date in the past three years that have a lifetime circ of 25 times or more:

&nbsp;

select  cir.ItemRecordID  from Polaris.CircItemRecords cir with (nolock)  
inner join Polaris.ItemRecordDetails ird with (nolock)  
on (cir.ItemRecordID = ird.ItemRecordID)   
where cir.LifetimeCircCount >= 25   
and cir.FirstAvailableDate between dateadd(yy,-3,getdate()) and getdate() 

answered 4 months ago by <img width="18" height="18" src="../_resources/default-avatar_a560d445f6a94cbbb829363ebf468ece.jpg"/> crosenthal@pwcgov.org

<a id="commentAns_1421"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for items with information in "Name of piece"](https://iii.rightanswers.com/portal/controller/view/discussion/1345?)
- [SQL for items withdrawn by a particular staff member](https://iii.rightanswers.com/portal/controller/view/discussion/809?)
- [Is the an SQL statement that finds items in processing that have holds?](https://iii.rightanswers.com/portal/controller/view/discussion/476?)
- [Is anyone doing batch searching to the OCLC WorldCat Metadata API?](https://iii.rightanswers.com/portal/controller/view/discussion/1252?)
- [How do you enter/search for titles with special characters?](https://iii.rightanswers.com/portal/controller/view/discussion/320?)

### Related Solutions

- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Bulk Change Circulation Statuses](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930606607662)
- [How do I schedule the Year End Circ Count Rollover job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930582726474)
- [What holdings information is sent when my Polaris database is a remote target?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930755803285)
- [How do I find item counts by material type?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930417498777)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)