SQL question - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=20)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# SQL question

0

291 views

I'm looking for a way to find every patron that has a PatronNameTitle assigned. I'm not seeing that in simply reports or find tool. Thanks!

asked 2 years ago  by <img width="18" height="18" src="../../_resources/390876f6062045bbb50aa8774ec2b7e0.jpg"/>  bsierra@davenportlibrary.com

[sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_561"></a>[](#)

0

<a id="downVoteAns_561"></a>[](#)

Answer Accepted

This should work in the client:

SELECT PatronID 
FROM PatronRegistration with (NOLOCK)
WHERE NameTitle IS NOT NULL
I don't remember ever seeing SimplyReports offer a NOT NULL option, though maybe it has one somewhere that I've either forgotten or not yet found.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/390876f6062045bbb50aa8774ec2b7e0.jpg"/>  jjack@aclib.us

Thanks! Looks like it worked. 

— bsierra@davenportlibrary.com 2 years ago

<a id="commentAns_561"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Windows Vista Question](https://iii.rightanswers.com/portal/controller/view/discussion/109?)
- [SQL for waiving certain fines](https://iii.rightanswers.com/portal/controller/view/discussion/192?)
- [SQL for Range of Closed Dates](https://iii.rightanswers.com/portal/controller/view/discussion/716?)
- [Deleting Material Types using SQL](https://iii.rightanswers.com/portal/controller/view/discussion/323?)
- [Creating Jobs in SQL Agent for an SSIS package](https://iii.rightanswers.com/portal/controller/view/discussion/403?)

### Related Solutions

- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)
- [Leap FAQs and PDF.pdf](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161108160057843)
- [How can SQL jobs be accessed?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930442864574)
- [Saved Searches emails are not being received](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930389681864)
- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)