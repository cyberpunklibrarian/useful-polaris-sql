SQL query to find a specific tag within a bib record set - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# SQL query to find a specific tag within a bib record set

0

107 views

Hello, SQL Masters,

I'm in need of a query to locate a specific tag within some genealogy bibs in a record set.  Thanks in advance to all of you brilliant SQL masters!

asked 2 years ago  by <img width="18" height="18" src="../../_resources/7ab6280640d3401fa8e68c183226348c.jpg"/>  louanne@stpl.us

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 2 years ago

<a id="upVoteAns_557"></a>[](#)

0

<a id="downVoteAns_557"></a>[](#)

Answer Accepted

Replace the ??? with your Tag number and your RecordSetID

SELECT DISTINCT BibliographicRecordID as recordid
FROM Polaris.BibliographicTags with (nolock)
WHERE EffectiveTagNumber = ???
and BibliographicRecordID in (Select BibliographicRecordID from BibRecordSets where RecordSetID = ???)

Rex Helwig
Finger Lakes Library System

answered 2 years ago  by <img width="18" height="18" src="../../_resources/7ab6280640d3401fa8e68c183226348c.jpg"/>  rhelwig@flls.org

<a id="commentAns_557"></a>[Add a Comment](#)

<a id="upVoteAns_559"></a>[](#)

0

<a id="downVoteAns_559"></a>[](#)

Thanks, Rex! Worked like a charm!

answered 2 years ago  by <img width="18" height="18" src="../../_resources/7ab6280640d3401fa8e68c183226348c.jpg"/>  louanne@stpl.us

- <a id="acceptAnswer_559"></a>[Accept as answer](#)

<a id="commentAns_559"></a>[Add a Comment](#)

<a id="upVoteAns_558"></a>[](#)

0

<a id="downVoteAns_558"></a>[](#)

Oh cool. I didn't know you could search in a specific record set. I'm taking this sql search for use too!

Susan Grant

answered 2 years ago  by <img width="18" height="18" src="../../_resources/7ab6280640d3401fa8e68c183226348c.jpg"/>  sgrant@somd.lib.md.us

- <a id="acceptAnswer_558"></a>[Accept as answer](#)

<a id="commentAns_558"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Can you change a tag in a bib record in Leap?](https://iii.rightanswers.com/portal/controller/view/discussion/1055?)
- [Use Item Record Call Numbers to Bulk Update Bib MARC Tag](https://iii.rightanswers.com/portal/controller/view/discussion/235?)
- [SQL for Bibs lacking local call #](https://iii.rightanswers.com/portal/controller/view/discussion/605?)
- [SQL for items withdrawn by a particular staff member](https://iii.rightanswers.com/portal/controller/view/discussion/809?)
- [Finding new and replaced bibs in Polaris between certain dates](https://iii.rightanswers.com/portal/controller/view/discussion/871?)

### Related Solutions

- [Bibliographic and Authority Record tag order](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930557664312)
- [Selecting a bibliographic style when printing a bibliographic record set](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930306605978)
- [MARC record display in PAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930394778428)
- [How can I locate bibs that were generated during a Terminated import without using SQL Server Management Studio?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930570115337)
- [1XX, 6XX, 7XX or 8XX tags not linked to Authority Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930898871020)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)