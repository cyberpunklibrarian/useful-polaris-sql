Items in system 2+ years with 2+ copies - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Items in system 2+ years with 2+ copies

0

63 views

Originally posted by rdye@krl.org on Wednesday, January 11th 18:12:01 EST 2017
Hi all. I'm looking for a report of all items at a branch that have been available for two or more years and have 2 or more copies in our system (not just the branch). Any help would be appreciated!

asked 4 years ago  by <img width="18" height="18" src="../../_resources/fbb98542332549a9b31d47dd4d7ebe5e.jpg"/>  support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 5 Replies   last reply 4 years ago

<a id="upVoteAns_65"></a>[](#)

0

<a id="downVoteAns_65"></a>[](#)

RE: Quick SQL query

Response by ggosselin@richlandlibrary.com on January 30th, 2017 at 9:37 am

It stripped the characters again!
Be sure to put parentheses around all of the nolocks and around the distinct cir.ItemRecordIDs. Also there should be a greater than symbol in the having so that it is only showing you results that have a count of item records more than 1

answered 4 years ago  by <img width="18" height="18" src="../../_resources/fbb98542332549a9b31d47dd4d7ebe5e.jpg"/>  support1@iii.com

- <a id="acceptAnswer_65"></a>[Accept as answer](#)

<a id="commentAns_65"></a>[Add a Comment](#)

<a id="upVoteAns_64"></a>[](#)

0

<a id="downVoteAns_64"></a>[](#)

RE: Quick SQL query

Response by ggosselin@richlandlibrary.com on January 30th, 2017 at 9:35 am

It looks like all of my parentheses were removed when I posted, and the greater than symbol in the having clause. Weird!
I also should have set the RecordStatusID = 1 instead of 4. 1 is Final and 4 is Deleted.
To get only those items with a specific assigned branch, add in the line:
and cir.assignedbranchID = 3
and edit the 3 to be the ID of the branch you want to run the query for.
To get the lifetime circ count in the results you'll need to join to the bibliographicrecords table and add LifetimeCircCount to the select part of the statement as well as the group by.
So, to give this another shot posting the full query hopefully it won't strip any characters this time!:
select cir.AssociatedBibRecordID, count distinct cir.ItemRecordID AS \[NumOfCopies\],br.LifetimeCircCount
from CircItemRecords cir nolock
join ItemRecordDetails ird nolock
on cir.ItemRecordID = ird.ItemRecordID
join BibliographicRecords br nolock
on cir.AssociatedBibRecordID = br.BibliographicRecordID
where cir.RecordStatusID = 1
and ird.CreationDate between '1899-01-01' and '2015-01-01'
and cir.AssignedBranchID = 3
group by cir.AssociatedBibRecordID, br.LifetimeCircCount
having count distinct cir.ItemRecordID 1
order by count distinct cir.ItemRecordID desc

answered 4 years ago  by <img width="18" height="18" src="../../_resources/fbb98542332549a9b31d47dd4d7ebe5e.jpg"/>  support1@iii.com

- <a id="acceptAnswer_64"></a>[Accept as answer](#)

<a id="commentAns_64"></a>[Add a Comment](#)

<a id="upVoteAns_63"></a>[](#)

0

<a id="downVoteAns_63"></a>[](#)

RE: Quick SQL query

Response by rdye@krl.org on January 26th, 2017 at 5:47 pm

Thanks so much! Sorry for the delay but I didn't get an email - thought I had subscribed but hadn't.
I got this to work after some tweaks - we must have different syntax rules. But now I realize I need to add a couple of things.
\- How to get items assigned to a certain branch
\- add the Lifetime circ count (I thought I'd done this correctly, but it changes the results drastically).
Sincerely,
Still Learning

answered 4 years ago  by <img width="18" height="18" src="../../_resources/fbb98542332549a9b31d47dd4d7ebe5e.jpg"/>  support1@iii.com

- <a id="acceptAnswer_63"></a>[Accept as answer](#)

<a id="commentAns_63"></a>[Add a Comment](#)

<a id="upVoteAns_62"></a>[](#)

0

<a id="downVoteAns_62"></a>[](#)

Quick SQL query

Response by ggosselin@richlandlibrary.com on January 12th, 2017 at 3:34 pm

Here is a quick SQL query I came up with. it doesn't have a relative date filter on it, so you'd need to modify the createdate line to actually be two years from the date you're running it:
select cir.AssociatedBibRecordID, count distinct cir.ItemRecordID AS \[NumOfCopies\]
from CircItemRecords cir nolock
join ItemRecordDetails ird nolock
on cir.ItemRecordID = ird.ItemRecordID
where cir.RecordStatusID = 4
and ird.CreationDate between '1899-01-01' and '2015-01-01'
group by cir.AssociatedBibRecordID
having count distinct cir.ItemRecordID 1
order by count distinct cir.ItemRecordID desc

answered 4 years ago  by <img width="18" height="18" src="../../_resources/fbb98542332549a9b31d47dd4d7ebe5e.jpg"/>  support1@iii.com

- <a id="acceptAnswer_62"></a>[Accept as answer](#)

<a id="commentAns_62"></a>[Add a Comment](#)

<a id="upVoteAns_61"></a>[](#)

0

<a id="downVoteAns_61"></a>[](#)

SQL input getting mangled

Response by wosborn@clcohio.org on February 10th, 2017 at 11:54 am
In addition to SQL input getting mangled in forum posts, it is also happening if you try to include SQL in a ticket update. Even if you attach the SQL as a text file on a ticket, it will STILL get mangled.
I've opened a ticket/enhancement request on this: #520943 in case someone else wants to also enter a ticket in an attempt to "bump" this issue up.

answered 4 years ago  by <img width="18" height="18" src="../../_resources/fbb98542332549a9b31d47dd4d7ebe5e.jpg"/>  support1@iii.com

- <a id="acceptAnswer_61"></a>[Accept as answer](#)

<a id="commentAns_61"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Searching for Items that have not circ'd in 2 years ex. DVD's](https://iii.rightanswers.com/portal/controller/view/discussion/1053?)
- [SQL top bib checkouts between 2 dates](https://iii.rightanswers.com/portal/controller/view/discussion/650?)
- [Report to find Items added by Material Type in a certain year?](https://iii.rightanswers.com/portal/controller/view/discussion/430?)
- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)
- [Fiscal year expenditures report](https://iii.rightanswers.com/portal/controller/view/discussion/98?)

### Related Solutions

- [Why would an item show as having been checked-in on a 3M SelfCheck?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930416753992)
- [4.0 Patron Services - Borrow by Mail (Part 2)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160908155214216)
- [Hold Pick-Up Slip (Vertical 2)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161025092049109)
- [Bibliographic 690 Local Tag Indicator 2 Value](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930567733510)
- [marcgt value for 380$2](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181011195525699)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)