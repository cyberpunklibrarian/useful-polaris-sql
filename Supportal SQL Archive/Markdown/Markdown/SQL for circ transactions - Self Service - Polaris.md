SQL for circ transactions - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for circ transactions

0

94 views

Does anyone have SQL for finding checkouts and renewals that will display the patron ID and item barcode?

I can get it to display the checkouts, but when I try to add the renewal it will do that, but haven't been able to retrieve the item barcode.

Any help would be appreciated!

Thanks,

Sarah

asked 9 months ago  by <img width="18" height="18" src="../../_resources/8cbd9393a5f94bb7a4c0d90204f21b81.jpg"/>  shouk@faylib.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 9 months ago

<a id="upVoteAns_1036"></a>[](#)

0

<a id="downVoteAns_1036"></a>[](#)

Answer Accepted

The following seems to work for pulling circ data from the ItemRecordHistory table:

SELECT DISTINCT(irh.ItemRecordHistoryID), irh.TransactionDate, irh.PatronID, cir.Barcode AS 'Item Barcode', irhs.ActionTakenDesc
FROM Polaris.ItemRecordHistory irh (nolock)
JOIN Polaris.PatronAddresses pa (nolock)
ON irh.PatronID = pa.PatronID
JOIN CircItemRecords cir (nolock)
ON cir.ItemRecordID = irh.ItemRecordID
JOIN Polaris.ItemRecordHistoryActions irhs (nolock)
ON irh.ActionTakenID = irhs.ActionTakenID
WHERE irh.ActionTakenID IN (13,28,44,45,73,74,75,76,77,78,79,80,81,82,89,90,91,93)
AND irh.TransactionDate BETWEEN '10/01/2020' AND '12/29/2020'
ORDER BY irh.TransactionDate ASC

Two caveats: I'm fairly new to SQL and Polaris so I offer no guarantees that this is right :-) Also, I believe the ItemRecordHistory table only goes as far back as you set it to in Polaris admin. I'm guessing a more accurate script would draw from the Transaction tables.

Phil

answered 9 months ago  by ![pgunderson@sandiego.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_pgunderson@sandiego.gov20210505173747.jpg)  pgunderson@sandiego.gov

<a id="commentAns_1036"></a>[Add a Comment](#)

<a id="upVoteAns_1039"></a>[](#)

0

<a id="downVoteAns_1039"></a>[](#)

Hi Phil,

Thanks so much for your query, it has definitely helped! 

Sarah

answered 9 months ago  by <img width="18" height="18" src="../../_resources/8cbd9393a5f94bb7a4c0d90204f21b81.jpg"/>  shouk@faylib.org

- <a id="acceptAnswer_1039"></a>[Accept as answer](#)

<a id="commentAns_1039"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)
- [SQL help for for circ info on 'new' stuff](https://iii.rightanswers.com/portal/controller/view/discussion/698?)
- [SimplyReports or SQL to get circ count by a UDF?](https://iii.rightanswers.com/portal/controller/view/discussion/82?)
- [SimplyReports or SQL to get circ count by a UDF?](https://iii.rightanswers.com/portal/controller/view/discussion/88?)
- [SQL or report for circ stats by Patron Statistical Class?](https://iii.rightanswers.com/portal/controller/view/discussion/488?)

### Related Solutions

- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [How can I find all the transaction types and subtypes used in Polaris?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930580777417)
- [How do I schedule the Year End Circ Count Rollover job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930582726474)
- [Slowness with SQL related transactions](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930842076787)
- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)