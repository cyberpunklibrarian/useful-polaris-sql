[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# SQL for item records with $0.01 price?

0

78 views

Thanks in advance to the SQL masters! ![cool](../_resources/smiley-cool_cbc30c4307c94bc6a9cf9eadd9123935.gif)

asked 5 years ago by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg) sbills@lpld.lib.in.us

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 5 years ago

<a id="upVoteAns_555"></a>[](#)

0

<a id="downVoteAns_555"></a>[](#)

Answer Accepted

Here you go.  You can plop this in the Find Tool.

&nbsp;

&nbsp;

SELECT ItemRecordID  
FROM POlaris.Polaris.ItemRecordDetails AS \[ird\] WITH (NOLOCK)  
WHERE ird.Price = '0.01'

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_a1734eaeb2ed4f9d89d93def4558ce80.jpg"/> trevor.diamond@mainlib.org

Thank you, Trevor!

— sbills@lpld.lib.in.us 5 years ago

<a id="commentAns_555"></a>[Add a Comment](#)

<a id="upVoteAns_556"></a>[](#)

0

<a id="downVoteAns_556"></a>[](#)

Cool! Will add this sql search to our Tech Services wiki!

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_a1734eaeb2ed4f9d89d93def4558ce80.jpg"/> sgrant@somd.lib.md.us

- <a id="acceptAnswer_556"></a>[Accept as answer](#)

<a id="commentAns_556"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for items with information in "Name of piece"](https://iii.rightanswers.com/portal/controller/view/discussion/1345?)
- [Deleting item records with holds](https://iii.rightanswers.com/portal/controller/view/discussion/32?)
- [Bib record modifiers](https://iii.rightanswers.com/portal/controller/view/discussion/1380?)
- [Delete a collection, stat code, or Shelf code from drop down list on item records](https://iii.rightanswers.com/portal/controller/view/discussion/402?)
- [Update item record data using item control number as a match point?](https://iii.rightanswers.com/portal/controller/view/discussion/1396?)

### Related Solutions

- [Default pricing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930543557097)
- [Collection value by branch](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930252689136)
- [Item record is missing item history](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930266377481)
- [How can I locate bibs that were generated during a Terminated import without using SQL Server Management Studio?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930570115337)
- [Import Profiles and Item Record Creation](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930457734892)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)