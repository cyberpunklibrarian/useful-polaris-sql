[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Requests that cant be filled

0

130 views

Good afternoon PolarIIIverse

&nbsp;

How does your library keep track of & resolve unfillable requests where the system no longers owns an item on that can fill the particular request? Which department handles this and would you mind sharing your workflow?

&nbsp;

Thanks

&nbsp;

Amy

&nbsp;

&nbsp;

&nbsp;

&nbsp;

asked 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_6084ed1c319248f1b584bbf7a15fa0a6.jpg"/> aal-shabibi@champaign.org

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 5 years ago

<a id="upVoteAns_392"></a>[](#)

0

<a id="downVoteAns_392"></a>[](#)

We compare two excel spreadsheets to find duplicate values.

In order to create the spreadsheet, we start with a bib record set of bibs with holds that can't be filled because the items are missing, lost, etc. We make this bib record set using an SQL query in the Find Tool, but you could also make it by creating an item record set of all items with certain circ. statuses (lost, missing unavailable, mending). You then turn the item record set into a bib record set and put the bibs with holds and only one item into a new bib record set. 

You run the bib record set through SimplyReports to make a spreadsheet of BIB record IDs of all the bib with unfillable holds.

Then you use SR to make an excel spreadsheet of all the active holds in the system.  This spreadsheet contains the bib record ids.

Then you use the method in the attached insturctions to find the bib record ids which appear both in the spreadsheet of bibs with unfillable holds and the spreadsheet of all the holds. 

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_6084ed1c319248f1b584bbf7a15fa0a6.jpg"/> lori@esrl.org

- <a id="acceptAnswer_392"></a>[Accept as answer](#)

- [Compiling the two excel sheets for deduping.docx](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/308?fileName=308-392_Compiling+the+two+excel+sheets+for+deduping.docx "Compiling the two excel sheets for deduping.docx")

<a id="commentAns_392"></a>[Add a Comment](#)

<a id="upVoteAns_329"></a>[](#)

0

<a id="downVoteAns_329"></a>[](#)

I've found this discussion interesting.  We too pull the Holds Purchase Alert report and are working on procedures for handling the unfillable requests.  Has anyone found a way to pull this report using SimplyReports?  We would like to be able to work with the report in excel.  I know we could probably save the report as a CSV file and manipulate it, but SimplyReports is much easier.

Thanks in advance,

Anne Krulik

Burlington County Library System

&nbsp;

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_6084ed1c319248f1b584bbf7a15fa0a6.jpg"/> akrulik@bcls.lib.nj.us

- <a id="acceptAnswer_329"></a>[Accept as answer](#)

<a id="commentAns_329"></a>[Add a Comment](#)

<a id="upVoteAns_326"></a>[](#)

0

<a id="downVoteAns_326"></a>[](#)

Thanks Katherine!

&nbsp;

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_6084ed1c319248f1b584bbf7a15fa0a6.jpg"/> aal-shabibi@champaign.org

- <a id="acceptAnswer_326"></a>[Accept as answer](#)

<a id="commentAns_326"></a>[Add a Comment](#)

<a id="upVoteAns_323"></a>[](#)

0

<a id="downVoteAns_323"></a>[](#)

Hi, Amy,

&nbsp;

I'm the manager of acquisitions and collections at my library and I manage purchasing for patron requests.  Here's how I do it!

I run the canned report "Holds Purchase Alert" (under Circulation --> Holds) at least every other week, sometimes weekly.  This report allows you to put in your ratio of holds to copies (we use 6:1) and then lets you know where to buy more copies.  At the bottom of this report it inclues ALL items with holds and 0 available copies.  This lets me address those issues (some are in repair, some need to be moved to a different bib, some need to be reordered) at the same time.

You can also schedule reports to run automatically and email to you if you'd rather do it on a set schedule.

Hope this is helpful!

&nbsp;

Katherine Manion

Assistant Director

Urbandale Public Library

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_6084ed1c319248f1b584bbf7a15fa0a6.jpg"/> kmanion@urbandale.org

- <a id="acceptAnswer_323"></a>[Accept as answer](#)

<a id="commentAns_323"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Bookclub requests](https://iii.rightanswers.com/portal/controller/view/discussion/720?)
- [Does anyone know how to edit the text in the warning about items not filling holds?](https://iii.rightanswers.com/portal/controller/view/discussion/376?)
- [Does anyone have an SQL for a request ratio?](https://iii.rightanswers.com/portal/controller/view/discussion/394?)
- [Purchase or Inter-Library Loan Request Form/Database?](https://iii.rightanswers.com/portal/controller/view/discussion/856?)
- [SQL for holds information](https://iii.rightanswers.com/portal/controller/view/discussion/1154?)

### Related Solutions

- [Should non-circulating items be Pending for holds?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180409103151965)
- [Trapping Preferences - What are they and how do they work?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930956439738)
- [How to use the Polaris ILL Module without OCLC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930581900307)
- [Can we suspend the hold option for specific titles?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930255045537)
- [Patron getting moved to the bottom of the holds queue](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930273974922)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)