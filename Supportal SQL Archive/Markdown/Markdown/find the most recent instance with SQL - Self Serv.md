find the most recent instance with SQL - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=20)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# find the most recent instance with SQL

0

57 views

I'm trying to build a report that tracks our in-process items and how long it takes them to move through tech services. I'm specifically looking to track how long it takes to move from ActionTakenID's (1,2,31) to (21) to (8).  
The query I've put together is almost there, except when the item record contains more than one instance of the ActionTakenID 8 (modified via item bulkchange).  I need the report to show the most recent instance and it currenty shows the first instance.
I thought maybe the MAX function was the way, but I'm having trouble figuring out how to use it correctly. 

Any ideas and help would be greatly appriciated!
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
select vir.ItemRecordID as 'ItemID'
    , vir.barcode
    , vir.Title
    , vir.Collection
    , vir.CallNumber
    , vir.MaterialType
    , convert(char (20),h1.TransactionDate,101) as 'OrderDate'
    , convert(char (20),h2.TransactionDate,101) as 'ReceivedDate'
    , datediff(day,h1.transactiondate,h2.transactiondate) as 'ord-rcv'
    , max(convert(char (20),h3.TransactionDate,101)) as 'Bulkchange'
    , datediff(day,h2.transactiondate,h3.transactiondate) as 'rcv-blkchg'
from polaris.polaris.itemrecordhistory h1
inner join polaris.polaris.itemrecordhistory h2 (nolock) on h1.itemrecordid = h2.itemrecordid
inner join polaris.polaris.itemrecordhistory h3 (nolock) on h1.itemrecordid = h3.itemrecordid
left outer join polaris.polaris.viewitemrecords vir (nolock) on h1.itemrecordid = vir.ItemRecordID
left outer join polaris.polaris.CircItemRecords cir (nolock) on vir.ItemRecordID=cir.ItemRecordID
where h1.ActionTakenID in (1,2,31)
and h2.ActionTakenID = 21
and h3.actiontakenid = 8
and h1.transactiondate < h2.TransactionDate
and h2.transactiondate < h3.TransactionDate
and vir.RecordStatus = 'final'
and vir.ItemStatusID in (15)  
and cir.StatisticalCodeID in (13,14,15,16,17,18,19,20)  
group by vir.ItemRecordID
    , vir.barcode
    , vir.Title
    , vir.Collection
    , vir.CallNumber
    , vir.MaterialType
    , convert(char (20),h1.TransactionDate,101)
    , datediff(day,h1.transactiondate,h2.transactiondate)
    , convert(char (20),h2.TransactionDate,101)
    , convert(char (20),h3.TransactionDate,101)
    , datediff(day,h2.transactiondate,h3.transactiondate)
order by \[Bulkchange\]

asked 2 years ago  by <img width="18" height="18" src="../../_resources/9016a19694cb42aa83505c24b344f4fd.jpg"/>  seddings@tscpl.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_566"></a>[](#)

0

<a id="downVoteAns_566"></a>[](#)

Answer Accepted

that did the trick, thank you JT!

answered 2 years ago  by <img width="18" height="18" src="../../_resources/9016a19694cb42aa83505c24b344f4fd.jpg"/>  seddings@tscpl.org

<a id="commentAns_566"></a>[Add a Comment](#)

<a id="upVoteAns_565"></a>[](#)

0

<a id="downVoteAns_565"></a>[](#)

Hi.

I tried your query on our database and got similar weirdness with the Bulkchange date. I think that the query does not like a convert and an aggregate function on the same field.

To get around this, I tried creating a subquery with the dates not converted and then selecting and converting the dates in the main select statement.

Below is the reworked query.  Since your database (and your acquisitions procedures) do not match ours exactly, I had to tweak things a bit to make them work in my database.  The query posted does not include those tweaks, with one exception:  I changed the max for Bulkchange date to min. This is because the items I was reviewing had been circulating for a while and had a later bulk change (which messed up the results). This query \_should\_ return the first Bulkchange date after the Receiveddate. To figure out what will work best for you, try running it with min and then with max.

Hope this helps.

JT

select
t1.ItemID
, t1.Barcode
, t1.Collection
, t1.CallNumber
, t1.MaterialType
, convert(char (20),t1.OrderDate,101) as 'OrderDate'
, convert(char (20),t1.ReceivedDate,101) as 'ReceivedDate'
, datediff(day,t1.OrderDate,t1.ReceivedDate) as 'ord-rcv'
, convert(char (20),t1.Bulkchange,101) as 'Bulkchange'
, datediff(day,t1.ReceivedDate,t1.Bulkchange) as 'rcv-blkvhg'
from
(select vir.ItemRecordID as 'ItemID'
    , vir.barcode
    , vir.Title
    , vir.Collection
    , vir.CallNumber
    , vir.MaterialType
    , h1.TransactionDate as 'OrderDate'
    , h2.TransactionDate as 'ReceivedDate'
    , min(h3.TransactionDate) as 'Bulkchange'
from polaris.polaris.itemrecordhistory h1
inner join polaris.polaris.itemrecordhistory h2 (nolock) on h1.itemrecordid = h2.itemrecordid
inner join polaris.polaris.itemrecordhistory h3 (nolock) on h1.itemrecordid = h3.itemrecordid
left outer join polaris.polaris.viewitemrecords vir (nolock) on h1.itemrecordid = vir.ItemRecordID
left outer join polaris.polaris.CircItemRecords cir (nolock) on vir.ItemRecordID=cir.ItemRecordID
where h1.ActionTakenID in (1,2,31)
and h2.ActionTakenID = 21
and h3.actiontakenid = 8
and h1.transactiondate < h2.TransactionDate
and h2.transactiondate < h3.TransactionDate
and vir.RecordStatus = 'final'
and vir.ItemStatusID in (15) 
and cir.StatisticalCodeID in (13,14,15,16,17,18,19,20) 
group by vir.ItemRecordID
    , vir.barcode
    , vir.Title
    , vir.Collection
    , vir.CallNumber
    , vir.MaterialType
    , h1.TransactionDate
    , h2.TransactionDate
 ) t1
order by t1.Bulkchange

answered 2 years ago  by <img width="18" height="18" src="../../_resources/9016a19694cb42aa83505c24b344f4fd.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_565"></a>[Accept as answer](#)

<a id="commentAns_565"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Where do I go to restart the Polaris SIP services? Anyone else running into issues with the Vendor Hoopla and their recent update?](https://iii.rightanswers.com/portal/controller/view/discussion/642?)
- [SQL for waiving certain fines](https://iii.rightanswers.com/portal/controller/view/discussion/192?)
- [Deleting Material Types using SQL](https://iii.rightanswers.com/portal/controller/view/discussion/323?)
- [SQL for Range of Closed Dates](https://iii.rightanswers.com/portal/controller/view/discussion/716?)
- [Creating Jobs in SQL Agent for an SSIS package](https://iii.rightanswers.com/portal/controller/view/discussion/403?)

### Related Solutions

- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)
- [How can SQL jobs be accessed?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930442864574)
- [Saved Searches emails are not being received](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930389681864)
- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [SQL mail for Job alerts](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930770825097)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)