a SQL that compares items in two collections - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# a SQL that compares items in two collections

0

29 views

We keep separate collections for our Bookmobiles and Main.  I need a query that can show me the titles in the main collection that are not in the companion bookmobile collection.  Is that possible?

asked 1 year ago  by <img width="18" height="18" src="../../_resources/1ed0879b4b9248fdbf664ceeff219a9e.jpg"/>  seddings@tscpl.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_998"></a>[](#)

0

<a id="downVoteAns_998"></a>[](#)

Answer Accepted

Hi.

Here's a Find Tool query that should do what you want. It can be adapted into a query for a report by adding a join to the BibliographicRecords table so you can get titles, authors and other data as needed.

This query is used in the Title Find Tool, even though it is querying item record information.

It looks for bib records with an item in a particular collection and excludes those bib records with an item in the other collection. (It also excludes deleted and Withdrawn items from each part of the query).

Hope this helps.

JT

SELECT DISTINCT ir.AssociatedBibRecordID
FROM Polaris.Polaris.ItemRecords ir WITH (NOLOCK)
WHERE ir.RecordStatusID = 1
AND ir.ItemStatusID <>11
AND ir.AssignedCollectionID = 10
AND ir.AssociatedBibRecordID NOT IN
(SELECT DISTINCT ir.AssociatedBibRecordID
FROM Polaris.Polaris.ItemRecords ir WITH (NOLOCK)
WHERE ir.RecordStatusID = 1
AND ir.ItemStatusID <>11
AND ir.AssignedCollectionID = 20)

answered 1 year ago  by <img width="18" height="18" src="../../_resources/1ed0879b4b9248fdbf664ceeff219a9e.jpg"/>  jwhitfield@aclib.us

Hi JT,

Using the Bib find tool makes sense and that was something I stumbled over trying to figure it out!

Thank you,

Shannon

— seddings@tscpl.org 1 year ago

<a id="commentAns_998"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Delete a collection, stat code, or Shelf code from drop down list on item records](https://iii.rightanswers.com/portal/controller/view/discussion/402?)
- [SQL for items withdrawn by a particular staff member](https://iii.rightanswers.com/portal/controller/view/discussion/809?)
- [Is the an SQL statement that finds items in processing that have holds?](https://iii.rightanswers.com/portal/controller/view/discussion/476?)
- [How do you purge withdrawn items](https://iii.rightanswers.com/portal/controller/view/discussion/186?)
- [Deleting item records with holds](https://iii.rightanswers.com/portal/controller/view/discussion/32?)

### Related Solutions

- [Creating a new collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930912772907)
- [How do I specify collections to display in the Collection field of an item record?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930457552095)
- [Item branch bulk change error](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930329171579)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Collection workform "branches" checkboxes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930545936496)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)