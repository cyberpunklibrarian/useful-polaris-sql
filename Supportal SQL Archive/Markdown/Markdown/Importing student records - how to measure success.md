Importing student records - how to measure success? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Importing student records - how to measure success?

0

133 views

Hi all,

We're going to do a pilot project to create library accounts for all students in one school district using their student ID number which will be a different format than our regular patron barcodes.  We'd like to know how many students use our databases.  We can get statistics on #views and #sessions for each database, but is there a way to know how many of those hits are from these student cards?

How else are you measuring success of student account projects?

Thanks for any information or advice you can offer.

\- Robin

asked 3 years ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

[database use](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=133#t=commResults) [student cards](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=134#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 3 years ago

<a id="upVoteAns_335"></a>[](#)

0

<a id="downVoteAns_335"></a>[](#)

Answer Accepted

Hi Robin,

If you use a separate patron code for the students and use e-sources for your databases, I can share a SQL script I devised that breaks down database logins by patron code. It may be a little crude compared to what JT and Trevor write, but it seems to work for us. You can change to your dates and OrgIDs.

Marilyn

select  torg.name as TransactionBranchName,tds.TransactionString as EsourceName, pcode.description as PatronCode, count(distinct th.transactionid) as Total
from PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK)
left outer join PolarisTransactions.Polaris.TransactionDetails td (nolock)
on (th.TransactionID = td.TransactionID)
left outer join PolarisTransactions.Polaris.TransactionDetails td2 (nolock)
on (th.TransactionID = td2.TransactionID)
inner join Polaris.Polaris.PatronCodes pcode (nolock)
on (td2.numvalue = pcode.PatronCodeID)
inner join Polaris.Polaris.Organizations torg (nolock)
on (th.OrganizationID = torg.OrganizationID)  
inner join PolarisTransactions.Polaris.TransactionDetailStrings tds with (nolock)
on (td.numvalue = tds.TransactionStringID)  
where th.TransactionTypeID = 2201
and td.TransactionSubTypeID = 241
and td2.transactionsubtypeid = 7
and th.TranClientDate between '1/1/2017' and '12/01/2017 23:59:59'
and th.OrganizationID in (15)  
Group by  torg.name,tds.TransactionString, pcode.description
Order by  torg.name,tds.TransactionString, pcode.description

answered 3 years ago  by <img width="18" height="18" src="../../_resources/02cc6a836d3242eb9cdf5acb2c3600c4.jpg"/>  mborg@gmilcs.org

<a id="commentAns_335"></a>[Add a Comment](#)

<a id="upVoteAns_336"></a>[](#)

0

<a id="downVoteAns_336"></a>[](#)

Thanks Eric and Marilyn! We'll start exploring this right away.

\- Robin

answered 3 years ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

- <a id="acceptAnswer_336"></a>[Accept as answer](#)

<a id="commentAns_336"></a>[Add a Comment](#)

<a id="upVoteAns_332"></a>[](#)

0

<a id="downVoteAns_332"></a>[](#)

Hey Robin,

We use Patron Codes to split out our students cards.

Hope this helps!

\-\- Eric

answered 3 years ago  by ![eric.young@phoenix.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_eric.young@phoenix.gov20191029113427.jpg)  eric.young@phoenix.gov

- <a id="acceptAnswer_332"></a>[Accept as answer](#)

<a id="commentAns_332"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Best practices for importing student records](https://iii.rightanswers.com/portal/controller/view/discussion/725?)
- [Looking for information on experiences with student IDs as library cards](https://iii.rightanswers.com/portal/controller/view/discussion/814?)
- [Student cards - allow checkout of items?](https://iii.rightanswers.com/portal/controller/view/discussion/248?)
- [Looking to gather Libraries' experience with Student Cards](https://iii.rightanswers.com/portal/controller/view/discussion/444?)
- [Looking for Info on Student Library Cards](https://iii.rightanswers.com/portal/controller/view/discussion/607?)

### Related Solutions

- [Importing Student Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930247180454)
- [Patron Record Import TRN Format - Polaris 6.5](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200513113659590)
- [Anatomy of a TRN file](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180822133010554)
- [Imported Patrons with Password of 4321](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930727358148)
- [Is it possible to import a list of barcodes to update the inventory date?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930316910362)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)