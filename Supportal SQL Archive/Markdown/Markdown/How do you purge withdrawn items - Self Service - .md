[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# How do you purge withdrawn items

0

234 views

I want to purge old withdrawn bibs and item records, but some bibs still have active item records. What is the best and easiest way to clean these records up?

asked 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_4cb3c6efbc104094ab729f27556f85d7.jpg"/> dfeatherston@pcpl21.org

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 6 years ago

<a id="upVoteAns_158"></a>[](#)

1

<a id="downVoteAns_158"></a>[](#)

I use a multi-step process. This may be more complicated than you want, but it may be helpful.

Once a month I identify all item records that were put in Withdrawn status over 90 days ago (Find Tool SQL Query). (This allows items from paid bills that were returned after payment to be re-activated, and patrons can get a refund if they return the item within 90 days.)

I check to see if there are any bibs with holds but no good items (Find Tool SQL query). If all items are withdrawn, I notify the appropriate selector and set those items aside for the next month.

I then delete the remaining items. Any items with This Item Only holds come up as errors. I resolve the issue as needed before deleting them.

Bibs that have no more final items can be put in a Weeded record set. Like Trevor, I delete items in batches and combine the bibs into one weeded record set.

I then pull OCLC numbers from the bib records via a couple of custom reports and batch-unset holdings via the Connexion client. Any bibs with no OCLC number are unset manually as needed. Any errors due to OCLC numbers in our records that do not match holdings in OCLC (holdings are not set on that OCLC number) are checked and holdings are unset manually.

(We did a reclamation project a couple of years ago and due to a misunderstanding of how the process works, we did not update all of the OCLC numbers in our records. >sigh<)

Once holdings are unset, I delete the bib records.

We retain deleted items and bib records for about one year, and then use a purge to delete them. Keeping the deleted bibs and items preserves "handles" for some statistical data that is not kept in the transactions database.

Again, this may be more complicated than whayt you want to do, but some of the steps were added over time as I realized that things were being overlooked. :-)  If you are interested in the SQL or the reports, please let me know and I can send them to you.

JT

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_4cb3c6efbc104094ab729f27556f85d7.jpg"/> jwhitfield@aclib.us

- <a id="acceptAnswer_158"></a>[Accept as answer](#)

<a id="commentAns_158"></a>[Add a Comment](#)

<a id="upVoteAns_143"></a>[](#)

1

<a id="downVoteAns_143"></a>[](#)

In the past, I've put all the withdrawn items in a record set and then deleted them.  I usually only deleted ~1250 at a time so the deletion job doesn't get interrupted and the software has time to process the bibs (which you can select to delete) and any subsequently affected authority records (which you can select to delete as well).

&nbsp;

If you system retains deleted items, you can schedule a purge through Utilities > Cataloging Processing > Purge Cataloging Records.

&nbsp;

If there are unbreakable links, you will have to manually attend to those before you can delete a record (cancel holds, resolve lost/checkout issues, child/parent item issues).

&nbsp;

HTH

&nbsp;

Trevor D  
\--  
Trevor Diamond  
Systems/UX Librarian  
MAIN (Morris Automated Information Network), Morristown NJ  
www.mainlib.org

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_4cb3c6efbc104094ab729f27556f85d7.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_143"></a>[Accept as answer](#)

<a id="commentAns_143"></a>[Add a Comment](#)

<a id="upVoteAns_159"></a>[](#)

0

<a id="downVoteAns_159"></a>[](#)

JT--your process looks a lot like ours!

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_4cb3c6efbc104094ab729f27556f85d7.jpg"/> sgrant@somd.lib.md.us

- <a id="acceptAnswer_159"></a>[Accept as answer](#)

<a id="commentAns_159"></a>[Add a Comment](#)

<a id="upVoteAns_144"></a>[](#)

0

<a id="downVoteAns_144"></a>[](#)

Well, if they have active item records, you literally can't delete the bib records. How are you defining "active"? We only delete/purge item records (then the bib records) once an item record has been marked as Withdrawn by branch staff. We wait 60 days (sometimes they change their minds!) then delete. After about 18 months, we purge.  
<br/>If by "active" you mean, as Trevor above mentioned, holds, you will need to clean those up one at a time. Other issues (such as inappropriate circulation status (lost, checked out) are resolved if you mark those items as Withdrawn first.

&nbsp;

Our workflow (includes OCLC deletion part) is attached.

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_4cb3c6efbc104094ab729f27556f85d7.jpg"/> sgrant@somd.lib.md.us

- <a id="acceptAnswer_144"></a>[Accept as answer](#)

- [Deleting items marked as withdrawn final extranet version.docx](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/186?fileName=186-144_Deleting+items+marked+as+withdrawn+final+extranet+version.docx "Deleting items marked as withdrawn final extranet version.docx")

<a id="commentAns_144"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for items withdrawn by a particular staff member](https://iii.rightanswers.com/portal/controller/view/discussion/809?)
- [Deleting item records with holds](https://iii.rightanswers.com/portal/controller/view/discussion/32?)
- [e-content with no item records](https://iii.rightanswers.com/portal/controller/view/discussion/251?)
- [Update item record data using item control number as a match point?](https://iii.rightanswers.com/portal/controller/view/discussion/1396?)
- [SQL for items with information in "Name of piece"](https://iii.rightanswers.com/portal/controller/view/discussion/1345?)

### Related Solutions

- [Deleting and Purging Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930748692772)
- [How do I remove item records with the status of Deleted?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930423208345)
- [Cataloging Purge Process failed with TrappingItemRecordID error](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930492779037)
- [Cataloging Purge Processing job failed](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930969081140)
- [Item record is missing item history](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930266377481)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)