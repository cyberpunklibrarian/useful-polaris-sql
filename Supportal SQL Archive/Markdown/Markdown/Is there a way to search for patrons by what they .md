[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Is there a way to search for patrons by what they have their notification setting as? i.e. is there a way to find any patrons that were accidentally set as fax instead of text?

0

78 views

We had a patron that was supposed to be receiving text notices but was instead marked as fax causing items to sit in limbo for 3 months. Is there a way to search to make sure that no other patrons are entered this way?

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_264dd5b02c8b4189ba5e78d8a3ea8d50.jpg"/> jory.kunzman@aberdeen.sd.us

[fax](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=495#t=commResults) [notices](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=351#t=commResults) [notification setting](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=302#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_1257"></a>[](#)

0

<a id="downVoteAns_1257"></a>[](#)

Hi, Jory:

&nbsp;

Try running the SQL query -- it should identify anyone set for fax notification:

\-----

SELECT DISTINCT P.Barcode  
FROM Patrons P WITH (NOLOCK)  
JOIN PatronRegistration PR WITH (NOLOCK)  
ON P.PatronID = PR.PatronID  
WHERE PR.DeliveryOptionID LIKE '6'

\-----

Hope this helps!

&nbsp;

Andrew Teeple

Lake County (IN) Public Library

ateeple@lcplin.org

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_264dd5b02c8b4189ba5e78d8a3ea8d50.jpg"/> ateeple@lcplin.org

- <a id="acceptAnswer_1257"></a>[Accept as answer](#)

In case you don't have direct access to SQL, you could also run this in the patron status / registration find tool by making the following small tweak:

SELECT DISTINCT P.PatronID  
FROM Patrons P WITH (NOLOCK)  
JOIN PatronRegistration PR WITH (NOLOCK)  
ON P.PatronID = PR.PatronID  
WHERE PR.DeliveryOptionID LIKE '6'

— wosborn@clcohio.org 2 years ago

<a id="commentAns_1257"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [T-Mobile patrons not receiving text notifications](https://iii.rightanswers.com/portal/controller/view/discussion/982?)
- [Is there a way to set staff placed holds for patrons to default to patron branch instead of branch location when hold is placed?](https://iii.rightanswers.com/portal/controller/view/discussion/799?)
- [Is there a way to set a longer held period for one patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/983?)
- [Does anyone have an SQL for patrons whose notifications settings are listed as "none"?](https://iii.rightanswers.com/portal/controller/view/discussion/621?)
- [Is there a way to undo a patron merge?](https://iii.rightanswers.com/portal/controller/view/discussion/813?)

### Related Solutions

- [Would like a way to gather all patrons with no notification option selected](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930285364780)
- [Setting up phone notification with additional TXT messages](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930542970896)
- [Long Distance Phone Numbers and Text Messages](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930379236869)
- [Patron purge and record set](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930655028324)
- [How should phone numbers be formatted in patron registration?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930411345829)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)