[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Patron Credits

0

144 views

We would like to be able to credit patron accounts with a waive (a prize for our reading program). However, it looks like if I apply a credit to patron accounts, I have to select a payment method and this will show up as money we should have taken in on our deposit. Does anyone know of a way to credit a patron account without it showing on the deposit? Since we're trying to load the waives for future use there's really no money changing hands here.

&nbsp;

Thanks,

Stephanie Van Atta  
Avondale Public Library  
svanatta@avondale.org

asked 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_d96c9f92254c4c00a8f14c91c5e0b29c.jpg"/> svanatta@avondale.org

[patron account](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=85#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 6 years ago

<a id="upVoteAns_278"></a>[](#)

0

<a id="downVoteAns_278"></a>[](#)

Answer Accepted

Other than maybe calling things out with notes on the deposit and then the ensuing credit, I don't believe it'll work the way you're describing. Polaris assumes that, if you have a credit, then money was taken/deposited somewhere. I wrestled with this a bit during our Food for Fines thing where we tally up food donated above and beyond a patron's fines and then apply that extra money to kids' accounts throughout the ILS. In the end, I wound up writing some code to handle the account list, the fines waived, and the notes added. It takes a bit to run, but it seemed like the best solution that didn't involve paying Polaris for some custom SQL work or something.

&nbsp;

Good luck!

answered 6 years ago by ![danielmesser@mcldaz.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_danielmesser@mcldaz.org20180523162210.jpg) danielmesser@mcldaz.org

<a id="commentAns_278"></a>[Add a Comment](#)

<a id="upVoteAns_292"></a>[](#)

0

<a id="downVoteAns_292"></a>[](#)

Just spitballing here but could you turn on Voucher in the Policy Tables>Payment Methods and use that as the payment method.  It should be easy to identify and exclude in the Daily Cash Drawer report or whichever report your using.

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_d96c9f92254c4c00a8f14c91c5e0b29c.jpg"/> rhelwig@flls.org

- <a id="acceptAnswer_292"></a>[Accept as answer](#)

<a id="commentAns_292"></a>[Add a Comment](#)

<a id="upVoteAns_283"></a>[](#)

0

<a id="downVoteAns_283"></a>[](#)

Actually, the thing I wrote *isn't* SQL. It's a script written in AutoHotkey that basically automates the process. It goes through a list of barcodes and fines and waives with notes, just as if a human was doing it. That way we kinda keep the digital paper trail real and there's an account associated with the waives and all of that stuff that, I suppose, keeps an auditor happy somewhere. If you're interested, I can send you the script. It's pretty bargain basement, but it's worked for us for three or four years now.

&nbsp;

Take care!

answered 6 years ago by ![danielmesser@mcldaz.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_danielmesser@mcldaz.org20180523162210.jpg) danielmesser@mcldaz.org

- <a id="acceptAnswer_283"></a>[Accept as answer](#)

<a id="commentAns_283"></a>[Add a Comment](#)

<a id="upVoteAns_282"></a>[](#)

0

<a id="downVoteAns_282"></a>[](#)

Hi Daniel,

Thanks for the reply. Unfortunately since we're hosted I can't upload any custom SQL. However, I can add notes to accounts. 

&nbsp;

Thanks,

Stephanie

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_d96c9f92254c4c00a8f14c91c5e0b29c.jpg"/> svanatta@avondale.org

- <a id="acceptAnswer_282"></a>[Accept as answer](#)

<a id="commentAns_282"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Staying on top of Patron Credits](https://iii.rightanswers.com/portal/controller/view/discussion/1369?)
- [Applying credit in Leap](https://iii.rightanswers.com/portal/controller/view/discussion/534?)
- [Do you add a percentage based fee to online credit card transactions?](https://iii.rightanswers.com/portal/controller/view/discussion/420?)
- [Patron expiration date update](https://iii.rightanswers.com/portal/controller/view/discussion/727?)
- [Patron Identification removal](https://iii.rightanswers.com/portal/controller/view/discussion/180?)

### Related Solutions

- [Applying credits to patron accounts](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930528887412)
- [Allow patrons to have credit on their accounts](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930252861980)
- [Staff cannot select "Credit Card" as a payment method](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930280452661)
- [Self-Check credit card transaction do not appear in the Polaris Credit Card Manager](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930951221225)
- [Credit card vendor options](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180612111730348)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)