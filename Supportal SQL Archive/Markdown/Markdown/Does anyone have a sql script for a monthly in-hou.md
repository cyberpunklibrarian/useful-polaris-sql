Does anyone have a sql script for a monthly in-house check-in count? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Does anyone have a sql script for a monthly in-house check-in count?

0

53 views

I'm looking to report on In-House check-in totals by a monthly range and by select branch IDs. Does anyone have a script you're willing to share?

Thanks,

Marian McCollum

Jackson County Library Services

asked 1 year ago  by <img width="18" height="18" src="../../_resources/72859acfbbc3452c920a9cd3c295712b.jpg"/>  marian.mccollum@lsslibraries.com

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_741"></a>[](#)

0

<a id="downVoteAns_741"></a>[](#)

Thank you JT. With minor modifications this is perfect! You made my day, thanks again!

Marian

answered 1 year ago  by <img width="18" height="18" src="../../_resources/72859acfbbc3452c920a9cd3c295712b.jpg"/>  marian.mccollum@lsslibraries.com

- <a id="acceptAnswer_741"></a>[Accept as answer](#)

<a id="commentAns_741"></a>[Add a Comment](#)

<a id="upVoteAns_739"></a>[](#)

0

<a id="downVoteAns_739"></a>[](#)

Hi Marian.

Here's a "quick and dirty" query that may do what you want. The TransactionDate lines are set to start with the first desired date and to be smaller than the day after the last desired date (that way you get everything on the last day). If you use a date in the middle of the month, you will only get partial results for that month. (Seems obvious, but I'm sure it's something I would do and then wonder why July's numbers were so small. ![laughing](../../_resources/d0be35e018614ea892be92e465a6750f.gif))

Please note that "today's" item record history is in a separate table, so you would want to run this after the last day you want to consider.

Hope this helps, or at least is a good starting point.

JT

SELECT
org.Name AS Branch
,DATEPART(MM,irh.TransactionDate) AS \[Month\]
,DATEPART(YYYY,irh.TransactionDate) AS \[Year\]
,COUNT(irh.ItemRecordHistoryID) as InHouseCount

FROM Polaris.Polaris.ItemRecordHistory irh WITH (NOLOCK)
JOIN Polaris.Polaris.Organizations org (NOLOCK)
ON (irh.OrganizationID = org.OrganizationID)

WHERE
irh.ActionTakenID = 36 --Checked in via In House
AND irh.OrganizationID in (3,4,5,6,7)
AND irh.TransactionDate >= '07/01/2019'
AND irh.TransactionDate < '11/01/2029' --The day after the last date to count

GROUP BY
org.Name
,DATEPART(MM,irh.TransactionDate)
,DATEPART(YYYY,irh.TransactionDate)

ORDER BY
DATEPART(MM,irh.TransactionDate)
,DATEPART(YYYY,irh.TransactionDate)
,org.Name

answered 1 year ago  by <img width="18" height="18" src="../../_resources/72859acfbbc3452c920a9cd3c295712b.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_739"></a>[Accept as answer](#)

<a id="commentAns_739"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL top bib checkouts between 2 dates](https://iii.rightanswers.com/portal/controller/view/discussion/650?)
- [SimplyReports or SQL to get circ count by a UDF?](https://iii.rightanswers.com/portal/controller/view/discussion/82?)
- [SimplyReports or SQL to get circ count by a UDF?](https://iii.rightanswers.com/portal/controller/view/discussion/88?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [Anyone have a sql script that will total fines for a year and then break them down by patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/688?)

### Related Solutions

- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [Why is in-house check in not counting as circulation?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930877939075)
- [Are in-house check-ins included in canned circulation reports?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930566498391)
- [Setting the default view for the Check In workform](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930805135071)
- [SQL query for a count of items by material type](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930506564972)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)