Average number days items are in transit? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Average number days items are in transit?

0

35 views

I'm hopeful that someone has already created a report that returns the average number of days items are in In-Transit/Transferred status between two dates.

asked 7 months ago  by <img width="18" height="18" src="../../_resources/2cd2cce6757d449787d5a89acef1538a.jpg"/>  rhelwig@flls.org

[days in transit](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=406#t=commResults) [reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=32#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 5 months ago

<a id="upVoteAns_1058"></a>[](#)

0

<a id="downVoteAns_1058"></a>[](#)

Answer Accepted

Hi Rex--

Below are the scripts we use. We pull out individual instances and share these in Excel, letting staff crunch the data as they wish (mean, median, by material type, collection, etc.). And we separate out holds from non-holds. But I think that if you're just looking for an average, you can union the two queries then take the average of the Days column.

We place limits on both the time an item went in transit as well as when it exited to weed out most instances of, say, an item getting shelved when it should have been shipped out and not found until two years later.

HTH.

Paul

For holds:

select cir.barcode as "Item Barcode", h1.transactiondate as "Sent in Transit",
o1.name as "Sent From",
h2.transactiondate as "Received",
o2.name as "Received By",
m.Description as "Material Type",
c.Name as "Collection",
datediff(day,h1.transactiondate,h2.transactiondate) as "Days"
from polaris.polaris.itemrecordhistory h1
inner join polaris.polaris.itemrecordhistory h2
on h1.itemrecordid = h2.itemrecordid
and h2.transactiondate = (select min(h3.transactiondate)
from polaris.polaris.itemrecordhistory h3 (nolock)
where h3.itemrecordid = h1.itemrecordid
and h3.transactiondate > h1.transactiondate
and h3.olditemstatusid = 5 and h3.NewItemStatusID = 4)
inner join polaris.polaris.organizations o1 (nolock)
on h1.OrganizationID = o1.organizationid
inner join polaris.polaris.organizations o2 (nolock)
on h2.organizationid = o2.OrganizationID
left outer join polaris.polaris.circitemrecords cir (nolock)
on h1.itemrecordid = cir.ItemRecordID
left outer join polaris.polaris.materialtypes m (nolock)
on cir.materialtypeid = m.MaterialTypeID
left outer join polaris.polaris.collections c with (nolock)
on cir.AssignedCollectionID = c.CollectionID
where h1.olditemstatusid <> 5 and h1.newitemstatusid = 5
and h1.transactiondate between '2020-12-01' and '2021-02-01' -- date item goes in transit
and h2.transactiondate between '2021-01-01' and '2021-02-01' -- date item arrives at pickup location
and h2.olditemstatusid = 5 and h2.NewItemStatusID = 4

For non-holds:

select cir.barcode as "Item Barcode", h1.transactiondate as "Sent in Transit",
o1.name as "Sent From",
h2.transactiondate as "Received",
o2.name as "Received By",
m.Description as "Material Type",
c.name as "Collection",
datediff(day,h1.transactiondate,h2.transactiondate) as "Days"
from polaris.polaris.itemrecordhistory h1 (nolock)
inner join polaris.polaris.itemrecordhistory h2 (nolock)
on h1.itemrecordid = h2.itemrecordid
and h2.transactiondate = (select min(h3.transactiondate)
from polaris.polaris.itemrecordhistory h3 (nolock)
where h3.itemrecordid = h2.ItemRecordID
and h3.olditemstatusid = 6 and h3.newitemstatusid <> 6
and h3.transactiondate > h1.TransactionDate)
left outer join polaris.polaris.circitemrecords cir (nolock)
on h1.itemrecordid = cir.ItemRecordID
left outer join polaris.polaris.materialtypes m (nolock)
on cir.materialtypeid = m.MaterialTypeID
inner join polaris.polaris.Organizations o1 (nolock)
on h1.organizationid = o1.organizationid
inner join polaris.polaris.organizations o2 (nolock)
on h2.organizationid = o2.OrganizationID
inner join polaris.polaris.collections c with (nolock)
on cir.AssignedCollectionID = c.collectionid
where h1.olditemstatusid <> 6 and h1.newitemstatusid = 6
and h2.olditemstatusid = 6 and h2.newitemstatusid <> 6
and h2.newitemstatusid <> 2 -- exclude cases where item goes from in transit to checked out
and h2.AssignedBranchID = h2.organizationid
and h1.transactiondate between '2020-12-01' and '2021-02-01' -- date item goes in transit
and h2.transactiondate between '2021-01-01' and '2021-02-01' -- date item arrives at (possibly new) assigned branch

answered 7 months ago  by <img width="18" height="18" src="../../_resources/2cd2cce6757d449787d5a89acef1538a.jpg"/>  pkeith@chipublib.org

Thank you Paul.  This worked perfectly.

Rex

— rhelwig@flls.org 7 months ago

<a id="commentAns_1058"></a>[Add a Comment](#)

<a id="upVoteAns_1087"></a>[](#)

0

<a id="downVoteAns_1087"></a>[](#)

Paul - Just wanted to say a huge "Thank you!" for doing this and posting it.  I needed exactly this SQL and you saved me a lot of time.

Bill

answered 5 months ago  by <img width="18" height="18" src="../../_resources/2cd2cce6757d449787d5a89acef1538a.jpg"/>  wtaylor@washcolibrary.org

- <a id="acceptAnswer_1087"></a>[Accept as answer](#)

<a id="commentAns_1087"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Adding number of "live" items to a report](https://iii.rightanswers.com/portal/controller/view/discussion/666?)
- [Average hold wait times](https://iii.rightanswers.com/portal/controller/view/discussion/845?)
- [Looking for a IN-Transit query to find past counts](https://iii.rightanswers.com/portal/controller/view/discussion/84?)
- [Average time things are lost before coming back?](https://iii.rightanswers.com/portal/controller/view/discussion/436?)
- [SQL for circulation numbers](https://iii.rightanswers.com/portal/controller/view/discussion/94?)

### Related Solutions

- [Maximum item call number length](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930857542877)
- [In Transit Slip](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161025142953609)
- [In-Transit for Hold Slip.pdf](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161103082802699)
- [Bib Call Number is not automatically copying to Item Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930769128110)
- [Copying bib call number to item record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930712730749)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)