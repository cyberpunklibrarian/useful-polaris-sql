SUM of ItemYTDCircCount and TotalItemLIfetimeCircCount from CircItemRecords generating numbers that have no basis in reality - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SUM of ItemYTDCircCount and TotalItemLIfetimeCircCount from CircItemRecords generating numbers that have no basis in reality

0

103 views

Hello:

Using the following query I should be able to sum the counts that I need for the all of the items associated with a bib record.  It should work, but in stead I get very large numbers for both.  There is a Lifetime count in the Bibliographic records table but I want the YeartoDate also which means that I need to go to the CircItems table anyway.

I have been doing surgery on this table during this morning so if something seems off then it is.  I have gone down so many rabbit holes I can't see the sky anymore. 

Please someone hand me a shovel!

SELECT BR.BrowseTitle,
       BR.BrowseAuthor,
       CIR.AssociatedBibRecordID as BibRecordID,
       MT.Description,
       COL.Abbreviation,
       SUM (isnull(cir.LifetimeCircCount,0)) as TotalItemLifeCircCount,
       SUM (isnull(cir.YTDCircCount,0)) as TotalItemYTDCircCount,
       COUNT (distinct shr.SysHoldRequestID) as Requests,
       COUNT (distinct cir.itemrecordid) as LinkedItems,
       ((COUNT (distinct shr.SysHoldRequestID))/ (COUNT (distinct cir.itemrecordid))) as Ratio
FROM Polaris.Polaris.SysHoldRequests shr (NOLOCK)
    INNER JOIN Polaris.Polaris.BibliographicRecords BR (NOLOCK)
        ON BR.BibliographicRecordID = shr.BibliographicRecordID
    Left JOIN  Polaris.Polaris.CircItemRecords CIR (NOLOCK)
        ON CIR.AssociatedBibRecordID = SHR.BibliographicRecordID
    INNER JOIN Polaris.Polaris.BibliographicTags bt (NOLOCK)
        ON BT.BibliographicRecordID = BR.BibliographicRecordID and BT.TagNumber = 245
    INNER JOIN Polaris.Polaris.MaterialTypes MT (NOLOCK)
        ON MT.MaterialTypeID = CIR.MaterialTypeID
    INNER JOIN Polaris.Polaris.Collections COL (NOLOCK)
        ON COL.CollectionID = CIR.AssignedCollectionID
    LEFT JOIN Polaris.Polaris.BibliographicSubfields bs (NOLOCK)
        ON BS.BibliographicTagID = BT.BibliographicTagID and bs.Subfield = 'h'
WHERE
    SysHoldStatusID in (1,3,4)
    and BR.PrimaryMARCTOMID not in (6,28,41)
    and CIR.itemstatusid not in (7,16,8,9,10,14,11)
    and cir.RecordStatusID = 1
GROUP by COL.abbreviation, BR.BrowseTitle, BR.BrowseAuthor, CIR.AssociatedBibrecordid, MT.Description
HAVING ((COUNT (distinct SHR.SysHoldRequestID))/ (COUNT (distinct CIR.itemrecordid))) >= 5
Order by ((COUNT (distinct SHR.SysHoldRequestID))/ (COUNT (distinct CIR.itemrecordid))),
col.Abbreviation

asked 3 years ago  by <img width="18" height="18" src="../../_resources/77697ccae0574fb9ae66e4c92ca0a0f6.jpg"/>  vkruggel@orl.bc.ca

[reporting](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=82#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 3 years ago

<a id="upVoteAns_215"></a>[](#)

0

<a id="downVoteAns_215"></a>[](#)

Answer Accepted

I still don't know why it is happening, but when I pick apart the query on my training server, it turns out, for example, that the LifetimeCircCount is ACTUALLY LifetimeCircCount*NumberofRequests. I countered by simply dividing the appropriate numbers, like so:

(SUM(cir.LifetimeCircCount) / (COUNT(DISTINCT shr.SysHoldRequestID))) as TotalItemLifeCircCount,
(SUM(cir.YTDCircCount) / (COUNT(DISTINCT shr.SysHoldRequestID))) as TotalItemYTDCircCount,

What is the point of the BiblbiographicTags/Subfields linking?  I couldn't see as it was doing anything, so I removed it while I was playing around with your code.

And I don't know if this is affecting your query, but my item counts don't seem to be matching up (items are being left out of the total items, but I'm not sure why, since they seem like they would meet the criteria).  Somehow, a collection is occasionally excluded, leaving the total items lacking. In my system, there's a bit of chaos, so I would see a single bib home to items in 3-4 collections.  Running your query (modified to reduce the lifetimecirc and ytd as I noted above) I get

BrowseTitle BrowseAuthor BibRecordID Description Abbreviation TotalItemLifeCircCount TotalItemYTDCircCount Requests LinkedItems Ratio
A bad moms Christmas \[videorecording (DVD widescreen)\]. NULL 1099140 DVD Audiovisual 43 30 56 6 9
A bad moms Christmas \[videorecording (DVD widescreen)\]. NULL 1099140 DVD Videos & DVDs 57 35 56 7 8

But I broke up the query to put everything in a temp table first and get 

BrowseTitle BrowseAuthor BibRecordID Material Type Collection TotalItemLifeCircCount TotalItemYTDCircCount Requests LinkedItems Ratio
A bad moms Christmas \[videorecording (DVD widescreen)\]. NULL 1099140 DVD Audiovisual 43 30 56 6 9
A bad moms Christmas \[videorecording (DVD widescreen)\]. NULL 1099140 DVD New Audiovisual 72 53 56 12 4
A bad moms Christmas \[videorecording (DVD widescreen)\]. NULL 1099140 DVD Videos & DVDs 57 35 56 7 8

Here's the query I ended up using to get the "best" results on my system.

SELECT BR.BrowseTitle, BR.BrowseAuthor, br.BibliographicRecordID as \[BibRecordID\], MT.Description, COL.Abbreviation, cir.ItemRecordID, cir.LifetimeCircCount, cir.YTDCircCount
INTO ##ReducingStats
FROM Polaris.Polaris.SysHoldRequests AS \[shr\] WITH (NOLOCK)
INNER JOIN Polaris.Polaris.BibliographicRecords AS \[br\] WITH (NOLOCK)
ON br.BibliographicRecordID = shr.BibliographicRecordID
INNER JOIN Polaris.Polaris.CircItemRecords AS \[cir\] WITH (NOLOCK)
ON CIR.AssociatedBibRecordID = SHR.BibliographicRecordID
INNER JOIN Polaris.Polaris.MaterialTypes AS \[mt\] WITH (NOLOCK)
ON mt.MaterialTypeID = cir.MaterialTypeID
LEFT OUTER JOIN Polaris.Polaris.Collections AS \[col\] WITH (NOLOCK)
ON cir.AssignedCollectionID = col.CollectionID
--on active, inactive, or pending holds
WHERE SysHoldStatusID IN (1,3,4)
--excluding certain bibs based on TOM
AND br.PrimaryMARCTOMID not in (6,28,41)
--excluding lost, claimed, missing, withdrawn, and unavailable items
AND cir.itemstatusid NOT IN (7,16,8,9,10,14,11)
--counting only FINAL records
AND cir.RecordStatusID = '1'
GROUP by br.BrowseTitle, br.BrowseAuthor, br.BibliographicRecordID, mt.Description, col.Abbreviation, cir.ItemRecordID, cir.LifetimeCircCount, cir.YTDCircCount

SELECT rs.BrowseTitle, rs.BrowseAuthor, rs.BibRecordID, rs.Description AS \[Material Type\], rs.Abbreviation AS \[Collection\],
(SUM(rs.LifetimeCircCount) / (COUNT(DISTINCT shr.SysHoldRequestID))) as TotalItemLifeCircCount,
(SUM(rs.YTDCircCount) / (COUNT(DISTINCT shr.SysHoldRequestID))) as TotalItemYTDCircCount,
COUNT(DISTINCT(shr.SysHoldRequestID)) as Requests,
COUNT(DISTINCT(rs.ItemRecordID)) as LinkedItems,
((COUNT (DISTINCT(shr.SysHoldRequestID)))/ (COUNT (DISTINCT(rs.ItemRecordID)))) AS \[Ratio\]
FROM Polaris.Polaris.SysHoldRequests AS \[shr\] WITH (NOLOCK)
INNER JOIN ##ReducingStats AS \[rs\] WITH (NOLOCK)
ON shr.BibliographicRecordID = rs.BibRecordID
--on active, inactive, or pending holds
WHERE SysHoldStatusID IN (1,3,4)
GROUP by rs.BrowseTitle, rs.BrowseAuthor, rs.BibRecordID, rs.Description, rs.Abbreviation
Order by rs.BrowseAuthor, rs.BrowseTitle

HTH

Trevor D

answered 3 years ago  by <img width="18" height="18" src="../../_resources/77697ccae0574fb9ae66e4c92ca0a0f6.jpg"/>  trevor.diamond@mainlib.org

<a id="commentAns_215"></a>[Add a Comment](#)

<a id="upVoteAns_218"></a>[](#)

0

<a id="downVoteAns_218"></a>[](#)

This is my final result for now...

This query is part of a larger project for collection refresh.  The Titles selected by the 5 to 1 ratio of holds to items.  When it ghets to that point it means that an order needs to be placed.  Now I am pushing in the circulation data to add more context to the the linked items and the holds.  There are more sections to be bolted on but this is giving me the start that I need. 

Thanks for everything.

SELECT BR.BrowseTitle,
       BR.BrowseAuthor,
       br.BibliographicRecordID as \[BibRecordID\],
       MT.Description,
       COL.Abbreviation,
       cir.ItemRecordID,
       cir.LifetimeCircCount,
       cir.YTDCircCount
INTO ##ReducingStats
FROM Polaris.Polaris.SysHoldRequests AS \[shr\] WITH (NOLOCK)
    INNER JOIN Polaris.Polaris.BibliographicRecords AS \[br\] WITH (NOLOCK)
        ON br.BibliographicRecordID = shr.BibliographicRecordID
    INNER JOIN Polaris.Polaris.CircItemRecords AS \[cir\] WITH (NOLOCK)
        ON CIR.AssociatedBibRecordID = SHR.BibliographicRecordID
    INNER JOIN Polaris.Polaris.MaterialTypes AS \[mt\] WITH (NOLOCK)
        ON mt.MaterialTypeID = cir.MaterialTypeID
    INNER JOIN Polaris.Polaris.BibliographicTags bt (NOLOCK)
        ON BT.BibliographicRecordID = BR.BibliographicRecordID and BT.TagNumber = 245
    LEFT OUTER JOIN Polaris.Polaris.Collections AS \[col\] WITH (NOLOCK)
        ON cir.AssignedCollectionID = col.CollectionID
    LEFT JOIN Polaris.Polaris.BibliographicSubfields bs (NOLOCK)
         ON BS.BibliographicTagID = BT.BibliographicTagID and bs.Subfield = 'h'
        --on active, inactive, or pending holds
WHERE SysHoldStatusID IN (1,3,4)
    --excluding certain bibs based on TOM
    AND br.PrimaryMARCTOMID not in (6,28,41)
    --excluding lost, claimed, missing, withdrawn, and unavailable items
    AND cir.itemstatusid NOT IN (7,16,8,9,10,14,11)
    --counting only FINAL records
    AND cir.RecordStatusID = '1'
    GROUP by br.BrowseTitle, br.BrowseAuthor, br.BibliographicRecordID, mt.Description, col.Abbreviation, cir.ItemRecordID, cir.LifetimeCircCount, cir.YTDCircCount
SELECT rs.BrowseTitle,
       rs.BrowseAuthor,
       rs.BibRecordID,
       rs.Description AS \[Material Type\],
       rs.Abbreviation AS \[Collection\],
       (SUM(rs.LifetimeCircCount) / (COUNT(DISTINCT shr.SysHoldRequestID))) as TotalItemLifeCircCount,
       (SUM(rs.YTDCircCount) / (COUNT(DISTINCT shr.SysHoldRequestID))) as TotalItemYTDCircCount,
       COUNT(DISTINCT(shr.SysHoldRequestID)) as Requests,
       COUNT(DISTINCT(rs.ItemRecordID)) as LinkedItems,
       ((COUNT (DISTINCT(shr.SysHoldRequestID)))/ (COUNT (DISTINCT(rs.ItemRecordID)))) AS \[Ratio\]
FROM Polaris.Polaris.SysHoldRequests AS \[shr\] WITH (NOLOCK)
    INNER JOIN ##ReducingStats AS \[rs\] WITH (NOLOCK)
        ON shr.BibliographicRecordID = rs.BibRecordID
        --on active, inactive, or pending holds
WHERE SysHoldStatusID IN (1,3,4)
GROUP by rs.BrowseTitle,
         rs.BrowseAuthor,
         rs.BibRecordID,
         rs.Description,
         rs.Abbreviation
HAVING ((COUNT (distinct SHR.SysHoldRequestID))/ (COUNT (distinct rs.itemrecordid))) >= 5
--Takes Bibs with a ration of requests to Items of Five or higher
Order by ((COUNT (distinct SHR.SysHoldRequestID))/ (COUNT (distinct rs.itemrecordid))),
      rs.Abbreviation
DROP TABLE ##ReducingStats

answered 3 years ago  by <img width="18" height="18" src="../../_resources/77697ccae0574fb9ae66e4c92ca0a0f6.jpg"/>  vkruggel@orl.bc.ca

- <a id="acceptAnswer_218"></a>[Accept as answer](#)

<a id="commentAns_218"></a>[Add a Comment](#)

<a id="upVoteAns_217"></a>[](#)

0

<a id="downVoteAns_217"></a>[](#)

That worked for me.

I have kept my tags, but changed the SUM statement.  To answer a question you had, some of the numbers will not add up quite correctly at least not yet.  If you have an item in two different collections it counts them seperatly in this query so far.  I have to still put them together.

I have not looked at the entire query yet, but you are better at this then I am.

Thanks again.

answered 3 years ago  by <img width="18" height="18" src="../../_resources/77697ccae0574fb9ae66e4c92ca0a0f6.jpg"/>  vkruggel@orl.bc.ca

- <a id="acceptAnswer_217"></a>[Accept as answer](#)

<a id="commentAns_217"></a>[Add a Comment](#)

<a id="upVoteAns_216"></a>[](#)

0

<a id="downVoteAns_216"></a>[](#)

Thanks for the input.  I will test and see how it goes.

The tag was placed as a way to eliminate no printed items.  It was working before I started to expand the query as this query was initially part of a Purchasing report that I have been expanding.

Again Thanks for the response.  It is appreciated.

answered 3 years ago  by <img width="18" height="18" src="../../_resources/77697ccae0574fb9ae66e4c92ca0a0f6.jpg"/>  vkruggel@orl.bc.ca

- <a id="acceptAnswer_216"></a>[Accept as answer](#)

<a id="commentAns_216"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for circulation numbers](https://iii.rightanswers.com/portal/controller/view/discussion/94?)
- [Adding number of "live" items to a report](https://iii.rightanswers.com/portal/controller/view/discussion/666?)
- [How to Find Definitions of ActionTakenID Numbers](https://iii.rightanswers.com/portal/controller/view/discussion/96?)
- [Do reporting by subject headings?](https://iii.rightanswers.com/portal/controller/view/discussion/1059?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)

### Related Solutions

- [Patron did not receive a billing notice](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930935904805)
- [Hiding a Vital Site from User View](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160121190744683)
- [Long Distance Telephony](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930882128740)
- [SAN number for EDI ordering](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930858154155)
- [Maximum item call number length](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930857542877)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)