[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Easily converting item holds to bib holds?

0

110 views

Does anyone have a quick and easy way to convert item holds to bib holds? I'm sure there's a better way than going through individually, canceling the item hold, replacing it as a bib, and scooching the patron back up to the right spot in the queue... Can anyone help a noob out? Thanks in advance!!

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_2227611dcfa443f594a2da10d3b13d20.jpg"/> elinacre@altoona-iowa.com

<a id="comment"></a>[Add a Comment](#)

## 9 Replies   last reply 1 year ago

<a id="upVoteAns_1296"></a>[](#)

1

<a id="downVoteAns_1296"></a>[](#)

Answer Accepted

I haven't found a super-fast way, but the method below at least avoids having to cancel and move the patron in line:

Copy the Bib Control number of the title. Open the hold request to be converted and go to ACTIONS > Transfer. Set the search to Control number > Exact, paste it in and search. Opening the record will bring up the "Transfer Hold Request" popup.

Click Transfer and then Continue when you get the warning about Duplicate Hold Requests.

The hold should convert from an item-level to a title-level hold and the patron will maintain their original place in the queue.

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_2227611dcfa443f594a2da10d3b13d20.jpg"/> tvineberg@sandiego.gov

<a id="commentAns_1296"></a>[Add a Comment](#)

<a id="upVoteAns_1305"></a>[](#)

1

<a id="downVoteAns_1305"></a>[](#)

Good point, Emily - weird that the Polaris client doesn't preserve the queue order but Leap does.

Regarding the latter question, below is a SQL query that seemed to work when I tried it (copy into Leap > Find Tool > Hold Request > SQL Search; or Polaris client > Circulation > Hold Requests > Search Mode: SQL).

It should pick up item-level Hold Requests whose Bib Record has more than one item attached. I set it to look at only "Active" holds older than 3 days, but those parameters could be adjusted:

SELECT sh.SysHoldRequestID  
FROM SysHoldRequests sh (nolock)  
JOIN BibliographicRecords br (nolock)  
ON sh.BibliographicRecordID = br.BibliographicRecordID  
JOIN RWRITER_BibDerivedDataView ddv (nolock)  
ON (br.BibliographicRecordID = ddv.BibliographicRecordID)  
WHERE ddv.NumberofItems > 1  
AND sh.ItemLevelHold = 1  
AND sh.SysHoldStatusID = 3  
AND sh.CreationDate < DATEADD(day,-3,GETDATE())

Or, to start with Bib Records instead of Hold Requests (Leap > Find Tool > Bibliographic Records > SQL Search; in Polaris client > Cataloging > Bibliographic Records > Search Mode: SQL):

SELECT DISTINCT br.BibliographicRecordID  
FROM BibliographicRecords br (nolock)  
JOIN SysHoldRequests sh (nolock)  
ON br.BibliographicRecordID = sh.BibliographicRecordID  
JOIN RWRITER_BibDerivedDataView ddv (nolock)  
ON (br.BibliographicRecordID = ddv.BibliographicRecordID)  
WHERE ddv.NumberofItems > 1  
AND sh.ItemLevelHold = 1  
AND sh.SysHoldStatusID = 3  
AND sh.CreationDate < DATEADD(day,-3,GETDATE())

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_2227611dcfa443f594a2da10d3b13d20.jpg"/> tvineberg@sandiego.gov

- <a id="acceptAnswer_1305"></a>[Accept as answer](#)

<a id="commentAns_1305"></a>[Add a Comment](#)

<a id="upVoteAns_1306"></a>[](#)

0

<a id="downVoteAns_1306"></a>[](#)

THOMAS. You are a god! I am SO excited about this.

Man, what other processes could be wildly easier if I could just figure out SQL?! Ha!

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_2227611dcfa443f594a2da10d3b13d20.jpg"/> elinacre@altoona-iowa.com

- <a id="acceptAnswer_1306"></a>[Accept as answer](#)

<a id="commentAns_1306"></a>[Add a Comment](#)

<a id="upVoteAns_1304"></a>[](#)

0

<a id="downVoteAns_1304"></a>[](#)

You're a hero!!! Thanks so much, Thomas!

Erin--I did find the "Transfer" option under Tools on the hold request screen in Polaris, but I think you have to look up the bib record separately to get the control number, and it doesn't keep their place in queue so you have to bump them back up to their spot in line. Still, easier than what I was doing before (and the patron doesn't have a "cancelled" hold on their account to question staff about).

Now, to quickly be able to figure out which item holds this actually matters for (as in, titles we have more than one copy of)...![tongue-out](../_resources/smiley-tongue-out_7848e18de5814fb99d4aef5465073ee3.gif)

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_2227611dcfa443f594a2da10d3b13d20.jpg"/> elinacre@altoona-iowa.com

- <a id="acceptAnswer_1304"></a>[Accept as answer](#)

<a id="commentAns_1304"></a>[Add a Comment](#)

<a id="upVoteAns_1303"></a>[](#)

0

<a id="downVoteAns_1303"></a>[](#)

One other note - if you happen to have several item holds for a single title, you can convert them simultaneously from the Holds Queue.

In the All Hold Requests tab, check the boxes next to any active ones that need to be transferred and click Transfer, then search the appropriate control number and open it (this also works if you need to move a group of holds to another bib record entirely).

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_2227611dcfa443f594a2da10d3b13d20.jpg"/> tvineberg@sandiego.gov

- <a id="acceptAnswer_1303"></a>[Accept as answer](#)

- [\__Hold Queue Transfer.PNG](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/1251?fileName=1251-1303___Hold+Queue+Transfer.PNG "__Hold Queue Transfer.PNG")

<a id="commentAns_1303"></a>[Add a Comment](#)

<a id="upVoteAns_1302"></a>[](#)

0

<a id="downVoteAns_1302"></a>[](#)

This makes things so much easier!! I was canceling the item hold, placing the bib hold, and then re-ordering the queue. Thank you!

&nbsp;

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_2227611dcfa443f594a2da10d3b13d20.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_1302"></a>[Accept as answer](#)

<a id="commentAns_1302"></a>[Add a Comment](#)

<a id="upVoteAns_1301"></a>[](#)

0

<a id="downVoteAns_1301"></a>[](#)

Ah, I was looking in the staff client, not LEAP. Thanks, Thomas!

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_2227611dcfa443f594a2da10d3b13d20.jpg"/> eshield@nols.org

- <a id="acceptAnswer_1301"></a>[Accept as answer](#)

<a id="commentAns_1301"></a>[Add a Comment](#)

<a id="upVoteAns_1300"></a>[](#)

0

<a id="downVoteAns_1300"></a>[](#)

@eshield It's actually on the Hold Request page, which will open if you click on the hold either from the patron record holds tab, or the holds queue.  See screenshot.

This option will be grayed out if the item is "Pending" and not "Active" (i.e., the item is on a picklist already rather than checked out, missing, etc.)

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_2227611dcfa443f594a2da10d3b13d20.jpg"/> tvineberg@sandiego.gov

- <a id="acceptAnswer_1300"></a>[Accept as answer](#)

- [\__Hold Request.PNG](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/1251?fileName=1251-1300___Hold+Request.PNG "__Hold Request.PNG")

<a id="commentAns_1300"></a>[Add a Comment](#)

<a id="upVoteAns_1298"></a>[](#)

0

<a id="downVoteAns_1298"></a>[](#)

I've wanted something like this for a long time so I was excited that someone had some kind of workaround, but ... I don't see an "actions" option or a "transfer" option? Is that supposed to be on the item record? Thanks for additional info.

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_2227611dcfa443f594a2da10d3b13d20.jpg"/> eshield@nols.org

- <a id="acceptAnswer_1298"></a>[Accept as answer](#)

<a id="commentAns_1298"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Is there an SQL to change item level holds to bib level holds?](https://iii.rightanswers.com/portal/controller/view/discussion/407?)
- [Are cancelled holds considered holds in holds item count?](https://iii.rightanswers.com/portal/controller/view/discussion/414?)
- [Checking in holds for specific branches](https://iii.rightanswers.com/portal/controller/view/discussion/1121?)
- [Does anyone know how to edit the text in the warning about items not filling holds?](https://iii.rightanswers.com/portal/controller/view/discussion/376?)
- [Scanning an item record to determine destination (especially for holds) without clicking further?](https://iii.rightanswers.com/portal/controller/view/discussion/1309?)

### Related Solutions

- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Why can't I link to the holds queue from an item record?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989583652)
- [Request did not appear on Pending list despite "In" item](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930397510485)
- [Item Held Longer Than Expected](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930398757933)
- [How to create bulk holds from a record set](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161128141614846)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)