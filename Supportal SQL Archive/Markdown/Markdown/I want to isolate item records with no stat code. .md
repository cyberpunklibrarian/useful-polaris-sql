[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# I want to isolate item records with no stat code. Does anyone have a query to find records where stat code is blank/none?

0

91 views

I can run a SQL query by Statistical Code if the item record has a stat code. I have tried NULL and NONE in place of the code.  NULL and NONE return no results or an error message.  I know we have item records with None as a statistical code because I can see them on a circulation by item statistical class code report.

&nbsp;

Thanks for any suggestions.

Rebecca Pool

Deer Park Public Library

asked 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_964b7b9fcb604ae7903609c2364b2af2.jpg"/> rpool@deerparktx.org

[sql queries item statistical codes](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=171#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 5 years ago

<a id="upVoteAns_421"></a>[](#)

0

<a id="downVoteAns_421"></a>[](#)

Answer Accepted

SELECT CIR.ItemRecordID as RecordID  
from polaris.polaris.CircItemRecords cir (nolock)  
where cir.StatisticalCodeID is null

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_964b7b9fcb604ae7903609c2364b2af2.jpg"/> aal-shabibi@champaign.org

Thank you!  This worked and I was able to adapt my query to more info.  

— rpool@deerparktx.org 5 years ago

YAY!

— aal-shabibi@champaign.org 5 years ago

<a id="commentAns_421"></a>[Add a Comment](#)

<a id="upVoteAns_422"></a>[](#)

0

<a id="downVoteAns_422"></a>[](#)

Do you have access to SimplyReports?  You can go into Item List Reports and check 'stat code' and then check the 'not present' box.  This way you can use other limiters such as location, collection, etc.

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_964b7b9fcb604ae7903609c2364b2af2.jpg"/> akrulik@bcls.lib.nj.us

- <a id="acceptAnswer_422"></a>[Accept as answer](#)

We have not purchased SimplyReports because it was out of our price range when it came out.  I can generally get what we need with a SQL query.  

I may price it and budget for it this year. 

— rpool@deerparktx.org 5 years ago

<a id="commentAns_422"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [Circulation by Item Statistical Code & limited by assigned collection?](https://iii.rightanswers.com/portal/controller/view/discussion/801?)
- [SQL query to export specific record set: Polaris 4.1R2](https://iii.rightanswers.com/portal/controller/view/discussion/93?)
- [SQL for Item records from PO/FY/Branch](https://iii.rightanswers.com/portal/controller/view/discussion/326?)
- [Does anyone have an SQL report for a specific tag in Authority records?](https://iii.rightanswers.com/portal/controller/view/discussion/625?)

### Related Solutions

- [Item statistical class code changes automatically](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181025152019128)
- [What to check after setting up a new code in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170228154215576)
- [Online Registration Defaults](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930782646108)
- [Item record is missing item history](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930266377481)
- [How does "Use template values instead of these (if available)" work?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170524120618032)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)