[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Acquisitions](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=10)

# SQL for low circ items with stats for all items associated with that title

0

55 views

Leap does not have the handy Preview for a bib record that includes all items linked to that bib and their circulation stats as does the client.  Our Librarians get a monthly report of items at their branch that haven't circ'd recently.  They then look up that title and take a look at the preview to find out how many other items of that title are in the system and how they are circ'ing.  We're getting ready to move all front line staff off the client and use Leap exclusively, so a report that shows all that information would be great!  

asked 1 year ago by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG) rdye@krl.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_1328"></a>[](#)

0

<a id="downVoteAns_1328"></a>[](#)

Answer Accepted

&nbsp;Such good news!  You know how it is to get staff to move to something new, so the more that Leap can provide, the better.  Thanks again Wes!

answered 1 year ago by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG) rdye@krl.org

<a id="commentAns_1328"></a>[Add a Comment](#)

<a id="upVoteAns_1327"></a>[](#)

0

<a id="downVoteAns_1327"></a>[](#)

I believe a Leap version of the Preview screen is set to ship with 7.4 in March https://portal.productboard.com/iii/6-innovative-product-status-board-new/tabs/23-polaris

&nbsp;

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_19adfa1ffc36406da0ed34020255d9b0.jpg"/> wosborn@clcohio.org

- <a id="acceptAnswer_1327"></a>[Accept as answer](#)

<a id="commentAns_1327"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Barcode Missing in Item Record](https://iii.rightanswers.com/portal/controller/view/discussion/216?)
- [Is there a way in the Acquistions module to receive and barcode multipart items or combo packages that will need to be split and attached to two sepaerate bibliographic records but paid and received as one item?](https://iii.rightanswers.com/portal/controller/view/discussion/175?)
- [Can anyone explain to me when/how an line item goes from On-Order to Pending Claim?](https://iii.rightanswers.com/portal/controller/view/discussion/1342?)
- [Can I invoice a POLI with no linked final bib record?](https://iii.rightanswers.com/portal/controller/view/discussion/412?)
- [Missing received PO](https://iii.rightanswers.com/portal/controller/view/discussion/212?)

### Related Solutions

- [Purchase order line items are using the incorrect ISBN](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930639446953)
- [Undo a canceled POLI (Purchase Order Line Item)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930937885320)
- [EDI PO Line item using wrong item template from $h](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930472812561)
- [EDI PO Line item w/no matching item templates](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930492618840)
- [Item records created when PO is released have incorrect data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930962885848)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)