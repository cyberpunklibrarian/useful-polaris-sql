SQL for item circ stats in record set between two dates - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for item circ stats in record set between two dates

0

53 views

Hi all,

I've been mucking about in SQL server this afternoon and can't get this quite right.

I have an item record set. I'd like to craft a report that spits out the title, collection code, and most importantly, the number of times each item was checked out/renewed during a specific date range (6/1/19 - 9/30/19).

Does anyone have a SQL query that would fit the bill?

Thanks in advance!

asked 5 months ago  by <img width="18" height="18" src="../../_resources/59be1dde328d44e7a52e0b65bbe08ce8.jpg"/>  josh.rouan@baldwinlib.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 5 months ago

<a id="upVoteAns_1089"></a>[](#)

0

<a id="downVoteAns_1089"></a>[](#)

Answer Accepted

Thanks very much! That query will not return results for 2019 for me so I am sticking with 2020 numbers which it will return. I tried to join from the TransactionHeaders table but couldn't get it to work.

Thanks again -

answered 5 months ago  by <img width="18" height="18" src="../../_resources/59be1dde328d44e7a52e0b65bbe08ce8.jpg"/>  josh.rouan@baldwinlib.org

Hi again Josh,

I had just *assumed* that the ItemRecordHistory table at your library would also include three years so you wouldn't need to look at the TransactionTables. Here is the TransactionTables version.

     1\. Once again, replace 99999 with your RecordSetID of choice.

     2\. This version should peer back as far as you like, but it is *slow*. The IRH version I posted gave me results on my sample record set before I could click start on my timer. This version takes a minute fifteen on the same record set. 

SELECT
     MIN(br.BrowseTitle) AS Title,
     cir.ItemRecordID,
     MIN(cir.AssignedCollectionID) AS CollectionCode,
     COUNT(td.TransactionID) AS CircCount
FROM
     PolarisTransactions.Polaris.TransactionHeaders AS th
     INNER JOIN PolarisTransactions.Polaris.TransactionDetails AS td
         ON td.TransactionID = th.TransactionID
     INNER JOIN Polaris.ItemRecordSets AS irs
         ON irs.ItemRecordID = td.NumValue
     INNER JOIN Polaris.CircItemRecords AS cir
         ON cir.ItemRecordID = irs.ItemRecordID
     INNER JOIN Polaris.BibliographicRecords AS br
         ON br.BibliographicRecordID = cir.AssociatedBibRecordID
WHERE
    th.TranClientDate >= '6/1/2019'
    AND th.TranClientDate < '10/1/2019'
    AND th.TransactionTypeID = '6001'
    AND td.TransactionSubTypeID = 38
    AND irs.RecordSetID = 99999   --Change this to the correct RecordSetID
GROUP BY cir.ItemRecordID

I hope this version gets you the data you need!

     Daniel

— ddunphy@aclib.us 5 months ago

This did the trick, thanks very much!

— josh.rouan@baldwinlib.org 5 months ago

<a id="commentAns_1089"></a>[Add a Comment](#)

<a id="upVoteAns_1088"></a>[](#)

0

<a id="downVoteAns_1088"></a>[](#)

Hi Josh,

Here is a query that might work for you.

     1\. Replace 99999 with your RecordSetID of choice.

     2\. Since you specified the number of times *each item* was checked out, it's grouped by ItemRecordID, but if you want the circ count by title, you can just group by BrowseTitle and comment out the ItemRecordID in the Select statement. I included the ID so you could tell items apart if your record set includes more than one with the same title.

     3\. Since you have the items in a record set and don't have to worry about lost information from deleted items, I used the ItemRecordHistory table, but I think it only goes back three years so if you wanted to look much further back than your specified dates you'd need to switch this to the transaction tables.

Here goes:

SELECT
     MIN(br.BrowseTitle) AS Title,
     cir.ItemRecordID,
     MIN(cir.AssignedCollectionID) AS CollectionCode,
     COUNT(irh.ItemRecordHistoryID) AS CircCount
FROM
     Polaris.ItemRecordSets AS irs
     INNER JOIN ItemRecordHistory AS irh
          ON irh.ItemRecordID = irs.ItemRecordID
          AND irs.RecordSetID = 99999  --Change this to the correct RecordSetID                                          
          AND irh.ActionTakenID IN (13,75,77,89,90,91,28,73,74,76,80,82,93)
          AND irh.TransactionDate >= '6/1/2019'
          AND irh.TransactionDate < '10/1/2019'
     INNER JOIN Polaris.CircItemRecords AS cir
          ON cir.ItemRecordID = irh.ItemRecordID
     INNER JOIN Polaris.BibliographicRecords AS br
          ON br.BibliographicRecordID = cir.AssociatedBibRecordID
GROUP BY cir.ItemRecordID

I hope this is at least somewhat helpful.

     Daniel

answered 5 months ago  by <img width="18" height="18" src="../../_resources/59be1dde328d44e7a52e0b65bbe08ce8.jpg"/>  ddunphy@aclib.us

- <a id="acceptAnswer_1088"></a>[Accept as answer](#)

<a id="commentAns_1088"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL or report for circ stats by Patron Statistical Class?](https://iii.rightanswers.com/portal/controller/view/discussion/488?)
- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)
- [SQL query to export specific record set: Polaris 4.1R2](https://iii.rightanswers.com/portal/controller/view/discussion/93?)
- [SQL for Item records from PO/FY/Branch](https://iii.rightanswers.com/portal/controller/view/discussion/326?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)

### Related Solutions

- [Bulk Change Circulation Statuses](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930606607662)
- [Item statistical class code changes automatically](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181025152019128)
- [Billed items be set to Lost and Lost Item Recovery table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930478370792)
- [Why can't I link to the holds queue from an item record?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989583652)
- [Do the home and owning branch need to match for floating item records?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930466429189)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)