Finding duplicate 035s, 050 etc. - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Finding duplicate 035s, 050 etc.

0

109 views

Hi!

While I know how to find duplicate ISBNs in the find tool, I need a query to find duplicate 050s or 035s

asked 2 years ago  by <img width="18" height="18" src="../../_resources/6741867dbcad45c782662549ef8b9bc2.jpg"/>  crosenthal@pwcgov.org

[duplicates](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=257#t=commResults) [oclc number](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=258#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_587"></a>[](#)

1

<a id="downVoteAns_587"></a>[](#)

Hi.

Here is a Find Tool Query that I think may work for you. It looks for bibs with duplicate 035 values using the BibliographicTag035Index table. You can modify this to look for some other 0xx fields (022, 024, 027, 028, 030, 035, 037, 050, 055, 060, 070, 080, 082, 086) by changing 035 to the value.

If you want to consider all 035 fields, you can remove the "WHERE bti.SystemControlNumber LIKE..." line. (To exclude OCLC numbers. change "LIKE" to "NOT LIKE".) This line needs to be modified or totally removed for other control number checks (you cannot comment it out with "--").

IMPORTANT NOTE: The index tables normalize the control numbers, so an 035 with "(OCoLC)12345" will match one with "(OCoLC)ocm12345".

Hope this helps.

JT

SELECT br.BibliographicRecordID from Polaris.Polaris.BibliographicRecords br WITH (NOLOCK)WHERE br.BibliographicRecordID IN
(
SELECT br.BibliographicRecordID from Polaris.Polaris.BibliographicRecords br WITH (NOLOCK)
JOIN Polaris.Polaris.BibliographicTag035Index bti (NOLOCK)
ON (br.BibliographicRecordID = bti.BibliographicRecordID)
WHERE bti.SystemControlNumber LIKE '(OCoLC)%'
GROUP BY br.BibliographicRecordID, bti.SystemControlNumber HAVING (COUNT(bti.BibliographicTagID) > 1)
)

answered 2 years ago  by <img width="18" height="18" src="../../_resources/6741867dbcad45c782662549ef8b9bc2.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_587"></a>[Accept as answer](#)

<a id="commentAns_587"></a>[Add a Comment](#)

<a id="upVoteAns_588"></a>[](#)

0

<a id="downVoteAns_588"></a>[](#)

Thanks JT! This works great!

Out of curiousity, how would I find seperate MARC records that have duplicate OCLC numbers, Kind of like items with duplicate barcodes but bibs with duplicate 035s

answered 2 years ago  by <img width="18" height="18" src="../../_resources/6741867dbcad45c782662549ef8b9bc2.jpg"/>  crosenthal@pwcgov.org

- <a id="acceptAnswer_588"></a>[Accept as answer](#)

<a id="commentAns_588"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Duplicate ISBN in a MARC](https://iii.rightanswers.com/portal/controller/view/discussion/819?)
- [Finding new and replaced bibs in Polaris between certain dates](https://iii.rightanswers.com/portal/controller/view/discussion/871?)
- [Is the an SQL statement that finds items in processing that have holds?](https://iii.rightanswers.com/portal/controller/view/discussion/476?)
- [SQL for Bibs lacking local call #](https://iii.rightanswers.com/portal/controller/view/discussion/605?)
- [Re-importing MARCIVE records after clean-up](https://iii.rightanswers.com/portal/controller/view/discussion/672?)

### Related Solutions

- [Does the de-duplication process de-dupe on all values in repeatable tags (example - ISBN), or just the first tag?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930402321885)
- [How can Authority Control records be merged?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930673534728)
- [Printing multiple spine labels for the same call number](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930444030856)
- [What is the difference between Material Type and Type of Material?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930618950203)
- [If retain deleted records is set to Yes will the records be visible during searches?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930352546757)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)