Lost-and-Paid - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Lost-and-Paid

0

121 views

Originally posted by cmaxey@websterpl.org on Wednesday, June 21st 15:18:30 EDT 2017
How are other libraries dealing with Lost-and-Paid items? We don't refund after 6 months, so I'd like to get rid of (delete from system) any items that are Lost-and-Paid over that length of time.

asked 4 years ago  by <img width="18" height="18" src="../../_resources/d8dea01824e74f64b53f0259e40b210a.jpg"/>  support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 4 years ago

<a id="upVoteAns_105"></a>[](#)

0

<a id="downVoteAns_105"></a>[](#)

Very helpful, thanks.  Now all we have to do is decide if auditors will require printed report before deletion occurs.

answered 4 years ago  by <img width="18" height="18" src="../../_resources/d8dea01824e74f64b53f0259e40b210a.jpg"/>  cmaxey@websterpl.org

- <a id="acceptAnswer_105"></a>[Accept as answer](#)

<a id="commentAns_105"></a>[Add a Comment](#)

<a id="upVoteAns_104"></a>[](#)

0

<a id="downVoteAns_104"></a>[](#)

We don't offer refunds at all for items that have already been paid for. We try to make that clear to the patron when they pay.  Every month, we delete the item records that have been lost-paid.  If an item was the last copy of that title, we suppress the Bib from the PAC and update OCLC.  Attached is a complete set of instruction for our process. 

Here's the SQL statement I run in the Item Record find tool:

`SELECT distinct cir.itemrecordid`

`from polaris.polaris.CircItemRecords cir with(nolock)`

`inner join polaris.polaris.BibliographicRecords bib with(nolock)`

`on cir.AssociatedBibRecordID = bib.BibliographicRecordID`

`WHERE ItemStatusID = 7`

`and ItemRecordID not in (select ItemRecordID AS RecordID from polaris.polaris.PatronLostItems)`

`and cir.ItemStatusDate < '20170831'`

Hope this is helpful. 

answered 4 years ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

- <a id="acceptAnswer_104"></a>[Accept as answer](#)

- [Delete Lost Paid.docx](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/77?fileName=77-104_Delete+Lost+Paid.docx "Delete Lost Paid.docx")

<a id="commentAns_104"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Related Solutions

- [Replacement fines were not automatically waived after overdue paid](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930589018439)
- [Which lost item recovery settings are used for partially paid replacement costs?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170523113239517)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)