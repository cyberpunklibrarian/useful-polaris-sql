Report or query to review average age of collection by collection code? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Report or query to review average age of collection by collection code?

0

24 views

Sorry in advance if this is right in front of me or has been answered before.

One of the libraries in this consortium has requested a report of the average age of their library's collection, by collection code.

Thank you in advance for any assistance or pointers you can provide.

Alison

Monarch Library System

asked 2 months ago  by <img width="18" height="18" src="../../_resources/b446823d025044fb8b49f880ee909d6f.jpg"/>  ahoffman@monarchlibraries.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 months ago

<a id="upVoteAns_1123"></a>[](#)

0

<a id="downVoteAns_1123"></a>[](#)

Answer Accepted

Here is the query inside our Custom Polaris report RDL which I believe was curtousy of another Polaris Library although I forget which one to credit.  Hopefully you can tailor it to fit your needs.  Let me know if you'd like the RDL file and I can email it to you.

USE Polaris
SELECT COL.Name AS 'Collection', DatePart(yyyy, getdate()) as 'CurrYear', AVG(BR.PublicationYear) AS 'AvgAge'
INTO #TEMP
FROM Polaris.Bibliographicrecords BR WITH (NOLOCK)
JOIN Polaris.CircItemRecords CIR WITH (NOLOCK)
ON CIR.AssociatedBibrecordID = BR.BibliographicRecordID
JOIN Polaris.Collections COL WITH (NOLOCK)
ON COL.CollectionID = CIR.AssignedCollectionID
JOIN Polaris.ItemRecordDetails IRD WITH (NOLOCK)
ON IRD.ItemRecordID = CIR.ItemRecordID
WHERE CIR.HomeBranchID IN (@GIS\_MS\_nOrganizationID)
AND BR.RecordStatusID <> 4
AND CIR.RecordStatusID <> 4
AND Col.Name NOT Like '%Periodical%'
GROUP BY COL.Name
Order By COL.Name
SELECT \[Collection\], AvgAge AS 'Average Age Year', CurrYear-AvgAge AS 'Average Age of Collection'
FROM #TEMP
ORDER BY \[Collection\], AvgAge, CurrYear-AvgAge
DROP Table #TEMP

Rex Helwig
Finger Lakes Library System
rhelwig@flls.org

answered 2 months ago  by <img width="18" height="18" src="../../_resources/b446823d025044fb8b49f880ee909d6f.jpg"/>  rhelwig@flls.org

Thanks so much! This will definitely help.

Alison

— ahoffman@monarchlibraries.org 2 months ago

<a id="commentAns_1123"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [SQL - looking for a report on how many fines we have for a specific collection code and how many were waived.](https://iii.rightanswers.com/portal/controller/view/discussion/673?)
- [Circulation report by patrons age](https://iii.rightanswers.com/portal/controller/view/discussion/97?)
- [Report/query for historical INNreach circ data?](https://iii.rightanswers.com/portal/controller/view/discussion/1052?)

### Related Solutions

- [Where are UMS reports stored?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930596768478)
- [Sending Collection Agency Reports Manually](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930959368316)
- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [Item Circulation by Statistical Code](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930592909894)
- [Question about Circulation by Postal Code report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930374734874)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)