Is there anyway to pull the SQL from the canned reports and notices? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Is there anyway to pull the SQL from the canned reports and notices?

0

240 views

I would like to take a peek at the SQL behind the canned reports?  Anyone know how to do this?  Also, our system is hosted so....I'm limited as to what I can see sometimes. 

asked 2 years ago  by <img width="18" height="18" src="../../_resources/6a314992e6c740ebb84bf64de44e3c87.jpg"/>  lori@esrl.org

[reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=32#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 2 years ago

<a id="upVoteAns_639"></a>[](#)

1

<a id="downVoteAns_639"></a>[](#)

Hi Lori--

We're also hosted, so the following should work for you.

In Reporting Services, find the report you're interested. In the
report's menu (Add to Favorites, Open, Edit in Report Builder, Subscribe, etc.), choose Edit in Report Builder.

In Report Builder, expand Datasets, and look for the dataset with the report's column names (or something close). Open the dataset's properties. On the main tab (Query), it'll tell you what stored procedure this dataset uses. Copy this down (it should begin "Polaris.Rpt_"), then close out of Properties, close out of Report Builder, and exit Reporting Services.

In SQL Server Management Studio, in Object Explorer, expand the Databases folder, expand the Polaris database, expand the Progarmmability folder, then expand the Stored Procedures folder. Find the stored procedure you copied down in the previous step. Hover your mouse over it to pull up the context menu. Select Script Stored Procedure as, then CREATE To, then New Query Window. You'll have the report's script in a new window to review.

You can also select Modify rather than "Select Script Stored Procedure" as from the context menu in SSMS, but this is a bit less safe as it's all too easy to overwrite the original stored procedure in this way.

HTH.

Paul
\_\_\_\_\_
Paul Keith
Chicago Public Library

answered 2 years ago  by <img width="18" height="18" src="../../_resources/6a314992e6c740ebb84bf64de44e3c87.jpg"/>  pkeith@chipublib.org

- <a id="acceptAnswer_639"></a>[Accept as answer](#)

Ugh. Report Builder won't open for me.  

— lori@esrl.org 2 years ago

Your site manager can get you access. Just open a ticket.

Paul

— pkeith@chipublib.org 2 years ago

When I expand the Stored Procedures it only opens up another tab called System Stored Proceedures.  Inside that file, all the listings begin with sys.sp.   There is nothing with Polaris.Rpt in there. 

— lori@esrl.org 2 years ago

Viewing stored procedures is permissions based. Likely, your profile doesn't currently have access. I'd ask your site manager check and, if you don't currently have access, grant it.

Paul

— pkeith@chipublib.org 2 years ago

<a id="commentAns_639"></a>[Add a Comment](#)

<a id="upVoteAns_643"></a>[](#)

0

<a id="downVoteAns_643"></a>[](#)

Even with access to SSMS, cloud services won't give me access to view SP's. I have to ask my Site Manager for the code. 

answered 2 years ago  by <img width="18" height="18" src="../../_resources/6a314992e6c740ebb84bf64de44e3c87.jpg"/>  jlmcconnel@cob.org

- <a id="acceptAnswer_643"></a>[Accept as answer](#)

<a id="commentAns_643"></a>[Add a Comment](#)

<a id="upVoteAns_640"></a>[](#)

0

<a id="downVoteAns_640"></a>[](#)

If you can download the RDL, you can open it in a program like Visual Studio (Or Report builder if you have access to that in SSRS).  The "guts" of the reports are mostly just pointing at a stored procedure, which you can then find vis SSMS.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/6a314992e6c740ebb84bf64de44e3c87.jpg"/>  trevor.diamond@mainlib.org

- <a id="acceptAnswer_640"></a>[Accept as answer](#)

<a id="commentAns_640"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Searching for Items that have not circ'd in 2 years ex. DVD's](https://iii.rightanswers.com/portal/controller/view/discussion/1053?)
- [Renewals reports](https://iii.rightanswers.com/portal/controller/view/discussion/83?)
- [Simply Reports Discrepancies](https://iii.rightanswers.com/portal/controller/view/discussion/960?)
- [Fiscal year expenditures report](https://iii.rightanswers.com/portal/controller/view/discussion/98?)

### Related Solutions

- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [Errors accessing reports in the client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930369939404)
- [Custom report parameters not allowing multiple selections in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930424447003)
- [Same report results](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930545379375)
- [Unable to Use Report Builder (in SQL Server Reporting Services)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930942443296)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)