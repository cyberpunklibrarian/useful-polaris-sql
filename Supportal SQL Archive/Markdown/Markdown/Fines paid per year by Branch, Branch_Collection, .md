[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Fines paid per year by Branch, Branch/Collection, and Branch/Patron Code

0

32 views

We're planning on going fine-free soon, and I need to provide some statistics. I think I'm able to use Simply Reports to find the total fines paid for overdue items per branch per year, but I'm looking for a little more detail. We're going to be eliminating overdue fines on children's materials first, then transitioning to eliminating all overdue fines. So I'm looking for the following SQL queries:

1.  Total overdue fines paid at each branch in 2017, 2018, and 2019
2.  Total overdue fines paid per branch for the same period, broken down by juvenile/teen vs. adult materials (would Collection be the easier way?)
3.  Total overdue fines paid per branch for the same period, broken down by Patron Code

Any help?

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_5a7f1d659fb545ae963681087f7b5ea6.jpg"/> musack@bcls.lib.nj.us

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_1174"></a>[](#)

0

<a id="downVoteAns_1174"></a>[](#)

I have the fines charged and paid per branch, but I cannot fine a way to use Simply Reports to find ONLY overdue fines (not replacement costs or other fees) paid by Patron Code or Collection/Stat. Any SQL help?

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_5a7f1d659fb545ae963681087f7b5ea6.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_1174"></a>[Accept as answer](#)

<a id="commentAns_1174"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Lost-and-Paid](https://iii.rightanswers.com/portal/controller/view/discussion/77?)
- [PayPal to pay fines in PAC](https://iii.rightanswers.com/portal/controller/view/discussion/530?)
- [Do you send Maximum Fines notices?](https://iii.rightanswers.com/portal/controller/view/discussion/232?)
- [Bulk Change Postal Code](https://iii.rightanswers.com/portal/controller/view/discussion/1353?)
- [Patron Code "Staff "override](https://iii.rightanswers.com/portal/controller/view/discussion/704?)

### Related Solutions

- [What to check after setting up a new code in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170228154215576)
- [Replacement fines were not automatically waived after overdue paid](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930589018439)
- [Paid lost item still displaying as unpaid](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930877186258)
- [Patron blocks not clearing after fees paid](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930955043925)
- [Collection Agency block does not clear even after the patron pays their fines](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930289283379)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)