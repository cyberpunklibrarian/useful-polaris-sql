SQL for Bibs w/no good items but have holds - including item and patron info - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for Bibs w/no good items but have holds - including item and patron info

0

77 views

Super SQL’ers - 

I’ve long dreamed of one report that lists Bib records which have no active items but have holds.  We’ve been getting at this with a couple of clunky reports but I would love one that had all the information our selectors need in one place.  I’ve managed a SQL report that has all the data, but it ends up being incredibly long because it lists the bib once for each ISBN and once for each patron with a hold. 

Here’s the query, and attached is a mock up of the report of my dreams.  **Fame and glory awaits!**

SELECT distinct BR.BibliographicRecordID as RecordID,

       br.BrowseTitle,

       ISBN.ISBNDisplayData,

       br.BrowseAuthor,

       br.BrowseCallNo,

       BR.MARCPubDateOne as \[PubDate\],

       cs.Abbreviation as \[Collection\],

       br.LifetimeCircCount as \[LifeCirc\],

       rw.NumberActiveHolds \[# Holds\],

       rw.NumberofItems as \[# Items\],

       rw.NumberLostItems as \[# Lost\],

       rw.NumberWithdrawnItems as \[# Discard\],

       rw.NumberClaimRetItems as \[Claim Ret\],

       rw.NumberMissingItems as \[Missing\],

       cir.ItemStatusID as \[ItemStatus\],

       cir.ItemStatusDate as \[StatusDate\],

       pr.PatronFirstLastName

FROM polaris.BibliographicRecords BR (NOLOCK)

LEFT OUTER JOIN polaris.RWRITER_BibDerivedDataView RW (NOLOCK)

 ON BR.BibliographicRecordID = RW.BibliographicRecordID

left join polaris.BibliographicISBNIndex ISBN

 on br.BibliographicRecordID = isbn.BibliographicRecordID

inner join polaris.CircItemRecords cir with (nolock)

 on cir.AssociatedBibRecordID = br.BibliographicRecordID

inner join polaris.Collections cs with (nolock)

 on cir.AssignedCollectionID = cs.CollectionID

inner join polaris.SysHoldRequests shr with (nolock)

 on br.BibliographicRecordID = shr.BibliographicRecordID

inner join polaris.PatronRegistration pr with (nolock)

 on pr.PatronID = shr.PatronID

WHERE RW.NumberofItems = (RW.NumberWithdrawnItems + RW.NumberLostItems + rw.NumberClaimRetItems + rw.NumberMissingItems)

 AND RW.NumberOfItems > 0

 AND RW.NumberActiveHolds > 0

 AND BR.RecordStatusID = 1

 order by cs.Abbreviation asc, BrowseTitle, isbn.ISBNDisplayData, cir.ItemStatusID, pr.PatronFirstLastName

asked 1 year ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

- [Dream report.xlsx](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/594?fileName=594-Dream+report.xlsx "Dream report.xlsx")

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 1 year ago

<a id="upVoteAns_737"></a>[](#)

0

<a id="downVoteAns_737"></a>[](#)

Thanks Jon and JT.  I'll give these approaches a try!

\- Robin

answered 1 year ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

- <a id="acceptAnswer_737"></a>[Accept as answer](#)

<a id="commentAns_737"></a>[Add a Comment](#)

<a id="upVoteAns_728"></a>[](#)

0

<a id="downVoteAns_728"></a>[](#)

I think the easiest way would be to break up the query into 4 pieces.  Then you could use subreports in SQL Server Reporting Services to pull up the ISBN, Item, and Hold sections.  If you can upload reports, you can try the attached.

For the subreports to work as-built, you'll have to create a folder in SSRS called Subreports in the same place as you upload the 'Bib holds with no items.rdl' file, then upload all three of the sub*.rdl files into the Subreports folder.

answered 1 year ago  by <img width="18" height="18" src="../../_resources/354a2b0693e44e96a2a1e126ae4adb48.jpg"/>  jstuckel@midcolumbialibraries.org

- <a id="acceptAnswer_728"></a>[Accept as answer](#)

- [Bibreport.zip](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/594?fileName=594-728_Bibreport.zip "Bibreport.zip")

<a id="commentAns_728"></a>[Add a Comment](#)

<a id="upVoteAns_725"></a>[](#)

0

<a id="downVoteAns_725"></a>[](#)

Hi Robin.

I have had limited success with grouping in reports, but it may be possible to get your query to output in the desired arrangement.

I don't know if it would help, but I have a query I adapted from a stored procedure that allows you to chose a ratio of holds to good items, or if you seletct 0 as good items to get only bibs with no good items and holds. It gives the number of items that recently went Lost or Missing and if there are any items On Order or what we call Available Soon (In processing). It avoids the multiple rows for each ISBN/UPC by only retrieving the largest BibliographicSubfieldID for subfield a of an 020/024 linked to that bib. This should be the number most recently added to the bib.

This report is used for selection, so we don't pull patron data. If you needed patron info, you shoud be able to replace the count of holds with patron info for each hold, and possibly group by other fields so that you have one row for the main info and a separate row for each patron. I can share the .rdl for this report if that would help.

Hope this is kind of helpful,

JT

--Query based on code from Polaris stored procedure Rpt_HoldPurchaseAlert
--with additions to limit by item assigned collection and include additional data

BEGIN
 SET NOCOUNT ON

 DECLARE @CollectionID Integer = 1
 DECLARE @nNumberOfItems Integer = 0
 DECLARE @nNumberOfHolds Integer = 1

 
 DECLARE @t TABLE
 (
  BibliographicRecordID int NOT NULL,
  CALLNO nvarchar(50) NULL,
  Author nvarchar(255) NULL,
  Title nvarchar(255) NULL,
  StandardNo varchar(100) NULL,
  MARCTypeOfMaterial varchar(80) NULL,
  HoldCount int NULL,
  ItemCount int NULL,
  OOASCount int NULL,
  LostMissRecCount int NULL
 )
 DECLARE @t1 TABLE
 ( BibliographicrecordID int NOT NULL,
  TagNumber int NULL,
  SubFieldID int NULL,
  StandardNumber nvarchar (100) NULL)
   
 DECLARE @t2 TABLE
 ( BibliographicrecordID int NOT NULL,
  StandardNumber nvarchar (100) NULL)

 --gets only locked requests
 INSERT INTO @t(BibliographicRecordID, HoldCount)
  SELECT distinct BibliographicRecordID, COUNT(SHR.SysHoldRequestID) as holds
 FROM Polaris.Polaris.SysHoldRequests SHR WITH (NOLOCK)
 WHERE SysHoldStatusID IN (1,3) AND BibliographicRecordID <> 0
 and BibliographicRecordID in
 (SELECT distinct it.AssociatedBibRecordID as Bibliographicrecordid from polaris.polaris.ItemRecords it with (nolock)
 where  it.assignedcollectionid in (@CollectionID))
 GROUP BY BibliographicRecordID

 \-\- only include final item records that are not withdrawn \[claim ret, lost, missing\]
 \-\- \[modified to count only holdable items\]
 UPDATE @t
 SET ItemCount =
  (SELECT COUNT(CIR.AssociatedBibRecordID)
   FROM Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)
   WHERE CIR.AssociatedBibRecordID = t.BibliographicRecordID AND
    CIR.ItemStatusID NOT IN (7,8,9,10,11) AND CIR.RecordStatusID = 1
    and CIR.Holdable= 1)
 FROM @t t, Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)

  UPDATE @t
 SET OOASCount =
  (SELECT COUNT(CIR.AssociatedBibRecordID)
   FROM Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)
   WHERE CIR.AssociatedBibRecordID = t.BibliographicRecordID AND
    CIR.ItemStatusID IN (13,15) AND CIR.RecordStatusID = 1
    and CIR.Holdable= 1)
 FROM @t t, Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)

  UPDATE @t
 SET LostMissRecCount =
  (SELECT COUNT(CIR.AssociatedBibRecordID)
   FROM Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)
   WHERE CIR.AssociatedBibRecordID = t.BibliographicRecordID AND
    CIR.ItemStatusID in (7,8,9,10) AND CIR.RecordStatusID = 1
    and CIR.ItemStatusDate >= dateadd(dd,-14,getdate()))
 FROM @t t, Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)

 IF (@nNumberOfItems > 0)
 BEGIN
  declare @fRatio float
  select @fRatio = @nNumberOfHolds / (@nNumberOfItems * 1.0)

  DELETE FROM @t
   WHERE ItemCount <> 0 
   AND HoldCount/(ItemCount * 1.0) < @fRatio
 END
 ELSE
 BEGIN
  DELETE FROM @t
   WHERE ItemCount <> 0 
   AND HoldCount/ItemCount < @nNumberOfHolds
 END

 IF (@nNumberOfItems = 0)
 BEGIN
  DELETE FROM @t
   WHERE ItemCount <> 0 
 END

 UPDATE @t
 SET CALLNO = BR.BrowseCallNo,
  Author = BR.BrowseAuthor,
  Title = BR.BrowseTitle,
  MARCTypeOfMaterial = MTOM.Description
 FROM @t t
 INNER JOIN Polaris.Polaris.BibliographicRecords BR WITH (NOLOCK)
  ON t.BibliographicRecordID = BR.BibliographicRecordID
 LEFT OUTER JOIN Polaris.Polaris.MARCTypeOfMaterial MTOM WITH (NOLOCK)
  ON BR.PrimaryMARCTOMID = MTOM.MARCTypeOfMaterialID 

 INSERT INTO @t1
 (BibliographicrecordID, StandardNumber, TagNumber, SubFieldID)
 (select bt.BibliographicRecordID, bs.data as StandardNumber, bt.TagNumber,
 bs.BibliographicSubfieldID as SubFieldID
 from polaris.polaris.BibliographicSubfields bs with (nolock)
 join polaris.polaris.bibliographictags bt (nolock) on (bs.BibliographicTagID = bt.BibliographicTagID)
 where bt.TagNumber in (020,024)
 and bs.Subfield like 'a'
 and bt.BibliographicRecordID in(Select bibliographicrecordid from @t))

 INSERT INTO @t2
 (Bibliographicrecordid, StandardNumber)
 (Select BibliographicrecordID, max(standardnumber) as StandardNumber from @t1
 group by BibliographicrecordID)

 UPDATE @t
 SET StandardNo = t2.StandardNumber
 FROM @t t
 join @t2 t2 on (t.BibliographicRecordID = t2.BibliographicrecordID)

 SELECT
  BibRecordID = LTrim(str(BibliographicRecordID)),
  CALLNO,
  Title,
  Author,
  StandardNo,
  MARCTypeOfMaterial,
  GoodItems = LTrim(str(ItemCount)),
  Holds = LTrim(str(HoldCount)),
  OnOrderAvailSoon = LTrim(str(OOASCount)),
  LostMissRecently = LTrim(str(LostMissReccount))
 FROM @t
 ORDER BY HoldCount DESC, ItemCount
END

answered 1 year ago  by <img width="18" height="18" src="../../_resources/354a2b0693e44e96a2a1e126ae4adb48.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_725"></a>[Accept as answer](#)

<a id="commentAns_725"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for bibs with holds and no owned items by branches](https://iii.rightanswers.com/portal/controller/view/discussion/821?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [SQL top bib checkouts between 2 dates](https://iii.rightanswers.com/portal/controller/view/discussion/650?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)

### Related Solutions

- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [Why can't I link to the holds queue from an item record?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989583652)
- [EDI PO Line item w/no matching item templates](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930492618840)
- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)