[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Report for number of items out

0

46 views

Does anyone have a report to show which patrons have excessive items checked out? We have a checkout limit of 35 items, but I just came across a patron who has over 70 items checked out, and I'm trying to find out if there are others like this. I'd also like to show how many holds, if possible. Any advice or SQL reports? Thanks!

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_872759f8f99f47fa86abf114da4a4c42.jpg"/> musack@bcls.lib.nj.us

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_1272"></a>[](#)

0

<a id="downVoteAns_1272"></a>[](#)

Answer Accepted

We use this but I'm not sure what the numbers are, We have a pretty high checkout limit so this sql is for OUR limit. 

&nbsp;

SELECT IC.PatronID FROM Polaris.Polaris.ItemCheckouts IC (NOLOCK) JOIN Polaris.Polaris.Patrons P (NOLOCK) ON P.PatronID = IC.PatronID JOIN Polaris.Polaris.PatronLoanLimits PLL (NOLOCK) ON P.PatronCodeID = PLL.PatronCodeID AND P.OrganizationID = PLL.OrganizationID GROUP BY IC.PatronID, PLL.TotalItems HAVING COUNT(IC.PatronID) > PLL.TotalItems

\-- INFORMATIONAL : Patrons who will be blocked at the self-check. Warning prompt for staff. May prevent login to certain digital resources due to exceeding checkout limit.

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_872759f8f99f47fa86abf114da4a4c42.jpg"/> sgrant@somd.lib.md.us

<a id="commentAns_1272"></a>[Add a Comment](#)

<a id="upVoteAns_1274"></a>[](#)

0

<a id="downVoteAns_1274"></a>[](#)

Thank you!! That worked :)

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_872759f8f99f47fa86abf114da4a4c42.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_1274"></a>[Accept as answer](#)

<a id="commentAns_1274"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Reports for Floating Items](https://iii.rightanswers.com/portal/controller/view/discussion/62?)
- [Does anyone have a damaged item template report you are willing to share?](https://iii.rightanswers.com/portal/controller/view/discussion/956?)
- [I need a report of the total number of items checked out by user ID where the total number out is greater than 35](https://iii.rightanswers.com/portal/controller/view/discussion/961?)
- [Floating Reports](https://iii.rightanswers.com/portal/controller/view/discussion/363?)
- [Report for patrons with items out. If possible, sort by location?](https://iii.rightanswers.com/portal/controller/view/discussion/779?)

### Related Solutions

- [Reports for On-the-Fly (OTF) items](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930655195362)
- [Why is in-house check in not counting as circulation?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930877939075)
- [Long Overdue Item report shows thousands of items](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930725698901)
- [Statistics for Holds and ILLs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930628713826)
- [Where are UMS reports stored?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930596768478)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)