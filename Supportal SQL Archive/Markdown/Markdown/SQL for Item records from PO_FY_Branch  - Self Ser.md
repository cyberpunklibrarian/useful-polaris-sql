SQL for Item records from PO/FY/Branch  - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for Item records from PO/FY/Branch

0

55 views

Hi all.

I need to be able to create record sets of items based upon PONumber, Fiscal year, and branch, so that we can track how successful discretionary ordering has been. I was wondering if anyone had a piece of SQL that would allow me to do so. I don’t have access to server level SQL so this will have to be run in the client.

TIA

Andrew

asked 3 years ago  by <img width="18" height="18" src="../../_resources/cb65425056094fc8999de4767e9b26f4.jpg"/>  andrew.wright1@dallaslibrary.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 3 years ago

<a id="upVoteAns_371"></a>[](#)

1

<a id="downVoteAns_371"></a>[](#)

Hi Andrew.

This Find Took Item query may work for you.

I included a line for FiscalYearID, but this may not be necessary, since new funds are created with each fiscal year and a FundID should be unique.

I also included a line to omit any items that have a fund segment that is in On Order status. This also ot be necessary, but I wanted to have it in case you wanted to limit by the order status. You can change "<>" to "=" or "IN (1,3,5,2)", or delete that line of the query as desired.

Hope this helps.

JT

PS: The Supportal did not like the truncation symbol I added to the "PONumber LIKE" line. If you need to truncate, you can add a percent sign before the closing single quote. You can also put the percent sign in other locations within the quotes if you, for example, wanted to find PO numbers that end with a string.

\-\-\-\-\-\-\-

SELECT liit.ItemRecordID
FROM Polaris.Polaris.POLines pl WITH (nolock)
JOIN Polaris.Polaris.PurchaseOrders po (nolock) ON (po.purchaseorderid = pl.purchaseorderid)
JOIN Polaris.Polaris.POLineItemSegments pls (nolock) ON (pl.polineitemid = pls.polineitemid)
JOIN Polaris.Polaris.POLineItemSegmentAmts plf ON (pls.polineitemsegmentid = plf.polineitemsegmentid)
JOIN Polaris.Polaris.LineItemSegmentToItemRecord liit (nolock) ON (pls.POLineItemSegmentID = liit.POLineItemSegmentID)
WHERE plf.FundID = 123
AND pl.StatusID <> 10
AND plf.FiscalYearID = 3
AND pls.DestinationOrgID = 5
AND po.PONumber LIKE 'YourPONumber'

\-\-\-\-\-\-\-

(Here are the StatusIDs and Statuses for PO Line Segments)

StatusID Status
1 Backordered
2 Canceled
3 Pending Claim
4 Claimed
5 Completed
6 Confirmed
7 Currently Received
8 Exceptional Condition
9 Never Published
10 On Order
11 Out of Print
12 Pending
13 Received
15 Return Requested
16 Returned
18 Partly Rec
19 Closed
20 Not Yet Published

answered 3 years ago  by <img width="18" height="18" src="../../_resources/cb65425056094fc8999de4767e9b26f4.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_371"></a>[Accept as answer](#)

<a id="commentAns_371"></a>[Add a Comment](#)

<a id="upVoteAns_377"></a>[](#)

0

<a id="downVoteAns_377"></a>[](#)

JT.

Thanks, I played around with this and it works great.

Andrew

answered 3 years ago  by <img width="18" height="18" src="../../_resources/cb65425056094fc8999de4767e9b26f4.jpg"/>  andrew.wright1@dallaslibrary.org

- <a id="acceptAnswer_377"></a>[Accept as answer](#)

<a id="commentAns_377"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [SQL for changes to a patron record](https://iii.rightanswers.com/portal/controller/view/discussion/840?)
- [SQL query to export specific record set: Polaris 4.1R2](https://iii.rightanswers.com/portal/controller/view/discussion/93?)
- [Last modifier of patron record](https://iii.rightanswers.com/portal/controller/view/discussion/1048?)
- [SQL for items returned late by collection](https://iii.rightanswers.com/portal/controller/view/discussion/718?)

### Related Solutions

- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [Item record is missing item history](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930266377481)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [SQL query for a count of items by material type](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930506564972)
- [Import Profiles and Item Record Creation](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930457734892)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)