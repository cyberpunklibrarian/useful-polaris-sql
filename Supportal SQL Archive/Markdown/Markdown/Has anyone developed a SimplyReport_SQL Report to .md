[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Has anyone developed a SimplyReport/SQL Report to answer PLA's Public Library Data Service circulation questions?

0

83 views

I'm trying to answer 25 - 30 of their questionaire.

asked 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_1c283de65a6d498ab58bb45d58cb60c8.jpg"/> rickels@hcplonline.org

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Simply Reports Discrepancies](https://iii.rightanswers.com/portal/controller/view/discussion/960?)
- [Simply reports patron services](https://iii.rightanswers.com/portal/controller/view/discussion/355?)
- [Circulation report by patrons age](https://iii.rightanswers.com/portal/controller/view/discussion/97?)

### Related Solutions

- [Same report results](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930545379375)
- [Reports returning a 503 error](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930713278993)
- [Circulation counts differ](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930770391656)
- [Move reporting services subscriptions to another user](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930313777401)
- [Question about Circulation by Postal Code report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930374734874)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)