[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for Bib Record Count (not e-materials) and number of bibs added per year

0

69 views

My brain power is low functioning today, so I'm going to ask what is probably a super easy question...

I'm trying to get a quote for on-going authority control and I need to give them a count of records in the catalog (but I want to exclude any of our e-material from OD, Hoopla,etc  because maintaining those records are not a priority) and an estimation of how many are added per year. Is there a sql query for this?

Thanks in advance!

asked 4 years ago by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg) sbills@lpld.lib.in.us

[sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 4 years ago

<a id="upVoteAns_873"></a>[](#)

0

<a id="downVoteAns_873"></a>[](#)

&nbsp;

Here's a Find Tool query to find bibs created in the previous year. (I modified an old saved query so most things are in lower case, but it should work OK.) It uses DATEADD to get a relative date. You can also enter a specific date such as '03/12/2019' (in single quotes) instead of the DATEADD string.

&nbsp;

It omits Electronic Resources, Ebook, Digital Collection, Eaudiobook, Streaming Music, Streaming Video and Emagazine from the results. If you want to count any of these, you will need to remove the corresponding number from the last line. If you have any electronic resource bibs that have a Type Of Material that isn't in the list, you will need to add that number.

&nbsp;

Hope this helps.

&nbsp;

JT

&nbsp;

&nbsp;

&nbsp;

Select br.bibliographicrecordid as id from bibliographicrecords br with (nolock)  
join polarisusers polu (nolock) on (br.creatorid = polu.polarisuserid)  
where br.creationdate >= dateadd(yy,-1,getdate())  
and br.recordstatusid = 1  
and br.primarymarctomid not in (6,36,38,41,48,49,50)

&nbsp;

Edit: The join to polarisusers is not really necessary, it is a "leftover" from the saved query. ![embarassed](../_resources/smiley-embarassed_ba0b8759aa084b69aaeae6406291de91.gif)  JT

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_224e42e4dcfb45d88a8a5bca591d59fb.jpg"/> jwhitfield@aclib.us

- <a id="acceptAnswer_873"></a>[Accept as answer](#)

<a id="commentAns_873"></a>[Add a Comment](#)

<a id="upVoteAns_871"></a>[](#)

0

<a id="downVoteAns_871"></a>[](#)

Sorry, I made the assumption your Overdrive eBibs had Item Records attached.

Try this:

1.  Open the Bib Find Tool
2.  Enter an asterisk in the For: field
3.  Check the count only box
4.  Change the Limit by: to Type of material and select \[aeb\] Eaudiobook, \[ebk\] Ebook & \[emg\] Emagazine and any others that you want to count
5.  Click on Search and subtract that from your total count.

Rex

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_224e42e4dcfb45d88a8a5bca591d59fb.jpg"/> rhelwig@flls.org

- <a id="acceptAnswer_871"></a>[Accept as answer](#)

Ah ha! Thanks, Rex! ![innocent](../_resources/smiley-innocent_c18bb025cce341428b3ea703f8f66477.gif)

— sbills@lpld.lib.in.us 4 years ago

<a id="commentAns_871"></a>[Add a Comment](#)

<a id="upVoteAns_870"></a>[](#)

0

<a id="downVoteAns_870"></a>[](#)

You should be able to get that number from the Bib Find Tool.

1.  Open Bib find Tool and enter asterisk in the For: and check the Count only box
2.  Click on the Collections Tab and uncheck the e-Collections and click on Search and record your number.
3.  If you have eMagazines in you collection like we do, as Bibs only then click on New Search
4.  Enter and asterisk in the For: and click on the Limit by: and select Type of material
5.  Scroll down and select eMagazine and check the Count only box and click on Search and subtract that number from the number you got in #2
6.  To find how many Bib were created in a given period click on New Searrch
7.  In the Search by: select Creation date and enter your From anf To dates, check Count only and uncheck any Collections you don't want in your count and click on Search.

Rex

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_224e42e4dcfb45d88a8a5bca591d59fb.jpg"/> rhelwig@flls.org

- <a id="acceptAnswer_870"></a>[Accept as answer](#)

I tried something similar but kept getting an error. We don't have an "e-collection" as our e items don't have item records. Maybe this needs to be reconsidered so I can more easily sort them out.. I used an asterik, selected count only, and chose all material types except those related to our e-materials. The error I got was: "Invalid search. Please verify the query and then resubmit the search. Source: "Polaris" \[0x80040202\] "Recursion depth exceeded maximum limit."  -- I have no idea what that means.

Creation date search -- DUH. Thank you!

— sbills@lpld.lib.in.us 4 years ago

<a id="commentAns_870"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL top bib checkouts between 2 dates](https://iii.rightanswers.com/portal/controller/view/discussion/650?)
- [I need to pull OCLC numbers from records with a deleted status to batch update our OCLC holdings. I'm able to get a report with: Bib record ID, Title, and Bib record status, but the column for MARC OCLC number is blank. Why doesn't it show up?](https://iii.rightanswers.com/portal/controller/view/discussion/829?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [SQL to Find all bibs with same title.](https://iii.rightanswers.com/portal/controller/view/discussion/675?)
- [SQL for bibs with holds and no owned items by branches](https://iii.rightanswers.com/portal/controller/view/discussion/821?)

### Related Solutions

- [Circulation statistics and deleted records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930736904760)
- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Bib Call Number is not automatically copying to Item Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930769128110)
- [Does an Enriched EDI order include the item or the bibliographic record call number?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930445590381)
- [Copying bib call number to item record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930712730749)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)