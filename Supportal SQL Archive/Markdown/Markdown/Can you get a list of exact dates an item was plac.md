Can you get a list of exact dates an item was placed on hold? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Can you get a list of exact dates an item was placed on hold?

0

27 views

I'm not looking to get information about patrons or anything. What I'd like to know is the specific dates that a single title was placed on hold. This way we could better monitor the success of our promotions. Ideally, what I'm looking for is something like: 

Barcode: 123456789

A hold was placed on: 1/10/2021, 8/2/2021, 8/2/2021, 8/3/2021
This would ideally go *backwards. *I don't want any other data just matching a barcode + the specific dates the item was placed on hold. It looks like I may be able to do that in Simply Reports, but it keeps saying that I need a new bib number. 

asked 1 month ago  by <img width="18" height="18" src="../../_resources/e2d75117e21a44059447de0aef07af47.jpg"/>  agoodman@darienlibrary.org

[simply reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=11#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 1 month ago

<a id="upVoteAns_1135"></a>[](#)

1

<a id="downVoteAns_1135"></a>[](#)

I haven't heard back on my question so I created 2 Queries.  See if either is what you're looking for.  Both look at the PolarisTransactions Databse.  Item Specific Holds are the only Hold Requests with a Barcode Recorded until they're trapped returns less results.

--Holds Request Placed on a specific Item Record by Barcode - This will only return Item Specific Hold Requests since they are the only ones with a Barcode entered at the time the request is created.

SELECT th.TranClientDate AS "Hold Creation Date"
FROM PolarisTransactions.Polaris.TransactionHeaders th (NOLOCK)
JOIN PolarisTransactions.Polaris.TransactionDetails td (NOLOCK)
ON td.TransactionID=th.TransactionID
JOIN Polaris.Polaris.CircItemRecords cir (NOLOCK)
on cir.ItemRecordID=td.numValue
WHERE cir.Barcode = 'BARCODE' --Enter your barcode here
AND th.TransactionTypeID=6005
AND td.TransactionSubTypeID=38
AND th.TranClientDate > '2020-01-01' --Enter you Beginning Date Here. If you want to search between two dates then replace with \[AND th.TranClientDate BETWEEN '2021-01-01' AND '2021-07-01'\]

--Holds Request placed as a First Available copy based on Item Barcode. This is looking at Hold Requests placed on the Items Associated BibliographicRecordID

SELECT th.TranClientDate AS "Hold Creation Date"
FROM PolarisTransactions.Polaris.TransactionHeaders th (NOLOCK)
JOIN PolarisTransactions.Polaris.TransactionDetails td (NOLOCK)
ON td.TransactionID=th.TransactionID
JOIN Polaris.Polaris.CircItemRecords cir (NOLOCK)
on cir.AssociatedBibRecordID=td.numValue
WHERE cir.Barcode = 'A20519409264' --Enter your barcode here
AND th.TransactionTypeID=6005
AND td.TransactionSubTypeID=36
AND th.TranClientDate > '2020-01-01'

Rex

answered 1 month ago  by <img width="18" height="18" src="../../_resources/e2d75117e21a44059447de0aef07af47.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_1135"></a>[Accept as answer](#)

<a id="commentAns_1135"></a>[Add a Comment](#)

<a id="upVoteAns_1138"></a>[](#)

0

<a id="downVoteAns_1138"></a>[](#)

We used the second query and it worked beautifully. Thank you so much again!

answered 1 month ago  by <img width="18" height="18" src="../../_resources/e2d75117e21a44059447de0aef07af47.jpg"/>  agoodman@darienlibrary.org

- <a id="acceptAnswer_1138"></a>[Accept as answer](#)

<a id="commentAns_1138"></a>[Add a Comment](#)

<a id="upVoteAns_1137"></a>[](#)

0

<a id="downVoteAns_1137"></a>[](#)

Rex, Thank you! I'm so sorry I couldn't reply earlier. Turns out the link in the notification messages don't work so I couldn't get back here until I got back to my work computer's bookmarks. 
I certainly appreciate this very much. I'll check it out and let you know. 

answered 1 month ago  by <img width="18" height="18" src="../../_resources/e2d75117e21a44059447de0aef07af47.jpg"/>  agoodman@darienlibrary.org

- <a id="acceptAnswer_1137"></a>[Accept as answer](#)

<a id="commentAns_1137"></a>[Add a Comment](#)

<a id="upVoteAns_1134"></a>[](#)

0

<a id="downVoteAns_1134"></a>[](#)

Quick question.  Are you looking for when the Hold Request is created for just the specific Item barcode or if a Hold Request is created for any item attached to the Assiciated Bib?

Rex

answered 1 month ago  by <img width="18" height="18" src="../../_resources/e2d75117e21a44059447de0aef07af47.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_1134"></a>[Accept as answer](#)

<a id="commentAns_1134"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Mass change Hold Pick Up date?](https://iii.rightanswers.com/portal/controller/view/discussion/95?)
- [Report for custom Hold Requests to Fill list](https://iii.rightanswers.com/portal/controller/view/discussion/91?)
- [SQL for bibs with holds and no owned items by branches](https://iii.rightanswers.com/portal/controller/view/discussion/821?)
- [SQL for Bibs w/no good items but have holds - including item and patron info](https://iii.rightanswers.com/portal/controller/view/discussion/594?)
- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)

### Related Solutions

- [Item Held Longer Than Expected](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930398757933)
- [Date an item is held until in the telephony voice message](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930375783756)
- [Request did not appear on Pending list despite "In" item](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930397510485)
- [Count of items owned on a date in the past](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930671934108)
- [What will happen to holds if I change the default Holds Expiration Date?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930309710990)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)