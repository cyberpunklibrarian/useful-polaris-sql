unique patrons - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# unique patrons

0

83 views

I need to determine unique library users, particularly the number of patrons who have checked out from a Collection during a certain time period.  Is there a canned report or a way in Simply Reports to do this?  I may just be missing something obvious.  If not, does anyone have a sql report for this?

asked 2 years ago  by <img width="18" height="18" src="../../_resources/df45ef91913d4f7f8ed7813033bd51d6.jpg"/>  esouth@flpl.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_693"></a>[](#)

0

<a id="downVoteAns_693"></a>[](#)

Answer Accepted

**Okay, I had to modify what I use a bit to add the collection, but this should pull out unique patrons who've checked out 1 or more items from the collections you choose. I borrowed this originally and modified it to my own needs, so if anyone has feedback for improvement I'd love to hear it!**

select DISTINCT td1.numValue as RecordID, p.Barcode, COUNT(td2.numValue) as ItemCheckoutTotal
from polaris.transactionheaders th with(nolock)
left join polaris.transactiondetails td1 with(nolock)
on th.Transactionid = td1.transactionid
left join polaris.polaris.Patrons p (nolock)
on p.PatronID = td1.numValue
left join polaris.transactiondetails td2 with(nolock)
on th.Transactionid = td2.transactionid
left join polaris.transactiondetails td3 with(nolock)
on th.Transactionid = td3.transactionid
where th.transactiontypeid=6001 --checkout
and td1.transactionsubtypeid=6 --patronID
and td2.TransactionSubTypeID=38 --itemrecordID
and td3.TransactionSubTypeID=61 --assignedcollectioncode
and th.TranClientDate between '07-1-2018' and '06-30-2019 23:59:59'
and td3.numValue IN (1,2,3,4,6) --enter the collection code(s)
group by td1.numValue, p.Barcode

**You can run this in the find tool**

select DISTINCT td1.numValue
from polaris.transactionheaders th with(nolock)
left join polaris.transactiondetails td1 with(nolock)
on th.Transactionid = td1.transactionid
left join polaris.polaris.Patrons p (nolock)
on p.PatronID = td1.numValue
left join polaris.transactiondetails td2 with(nolock)
on th.Transactionid = td2.transactionid
left join polaris.transactiondetails td3 with(nolock)
on th.Transactionid = td3.transactionid
where th.transactiontypeid=6001
and td1.transactionsubtypeid=6
and td2.TransactionSubTypeID=38
and td3.TransactionSubTypeID=61
and th.TranClientDate between '07-1-2018' and '06-30-2019 23:59:59'
and td3.numValue IN (1,2,3,4,6)

answered 2 years ago  by <img width="18" height="18" src="../../_resources/df45ef91913d4f7f8ed7813033bd51d6.jpg"/>  abrookins@cvlga.org

Perfect!  Thank you so much!

— esouth@flpl.org 2 years ago

<a id="commentAns_693"></a>[Add a Comment](#)

<a id="upVoteAns_689"></a>[](#)

0

<a id="downVoteAns_689"></a>[](#)

I don't have an answer but I'd be interested in this information, as well. 

answered 2 years ago  by <img width="18" height="18" src="../../_resources/df45ef91913d4f7f8ed7813033bd51d6.jpg"/>  marcia.friginette@wsplibrary.ca

- <a id="acceptAnswer_689"></a>[Accept as answer](#)

<a id="commentAns_689"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for changes to a patron record](https://iii.rightanswers.com/portal/controller/view/discussion/840?)
- [Last modifier of patron record](https://iii.rightanswers.com/portal/controller/view/discussion/1048?)
- [Simply reports patron services](https://iii.rightanswers.com/portal/controller/view/discussion/355?)
- [Circulation report by patrons age](https://iii.rightanswers.com/portal/controller/view/discussion/97?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)

### Related Solutions

- [Including Patron Emails for UMS Collection Agency Reports (Polaris versions earlier than 6.7)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210415123808587)
- [Customizing the Vital Home Page Per Site (versions prior to Vital 8.1)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170420071556346)
- [Where are UMS reports stored?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930596768478)
- [Retrieving deleted patron information](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930938135361)
- [Expired Patron Blocks Not Appearing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930808100621)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)