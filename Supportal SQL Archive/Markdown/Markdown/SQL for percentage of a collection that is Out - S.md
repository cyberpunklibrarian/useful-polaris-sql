SQL for percentage of a collection that is Out - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for percentage of a collection that is Out

0

62 views

We need a report showing the percentage of a collection that is checked out.  Include branch, collectionID, #items with circ status In, # items with circ status Out, and percentage Out. Thanks for any help you can offer!

asked 2 years ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

[collection](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=267#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_621"></a>[](#)

0

<a id="downVoteAns_621"></a>[](#)

Answer Accepted

Thanks so much Jack!  

answered 2 years ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

<a id="commentAns_621"></a>[Add a Comment](#)

<a id="upVoteAns_617"></a>[](#)

0

<a id="downVoteAns_617"></a>[](#)

You'll need to adjust a fair amount of this to fit your own system, and I was using Report Builder to calculate "% Out" with the expression *=IIF((Fields!Out.Value/(Fields!Out.Value+Fields!In.Value))\*100 > 0, (Fields!Out.Value/(Fields!Out.Value+Fields!In.Value))\*100,"0")*

That expression should report a "0" if there are *no *items in a particular collection checked in at a particular branch; otherwise you'll get an error due it trying to divide by zero.

If you're using Report Builder, you'll almost certainly want to use a matrix in the report output.  We have Collection as a Row group with Branch as Column group, with In, Out, and "% Out" as Values.  It might work better for you with the rows and columns swapped.

SELECT oi.Abbreviation as 'Branch', col.Name as 'Collection', ISNULL(sub2.NumIn,0) as 'In', ISNULL(sub1.Out,0) AS 'Out'
FROM Polaris.Polaris.Collections col (NOLOCK)
CROSS JOIN Organizations oi (NOLOCK)
FULL JOIN (
select oi2.Abbreviation, col2.Name, count(*) as 'Out'
FROM Polaris.Polaris.CircItemRecords cir2 with (nolock)
INNER join Polaris.Polaris.Collections col2 with (nolock)
on (cir2.AssignedCollectionID = col2.CollectionID)
INNER JOIN Polaris.Polaris.ItemStatuses ist2 (NOLOCK)
ON cir2.ItemStatusID = ist2.ItemStatusID
INNER join Polaris.Polaris.ItemRecordDetails ird2 with (nolock)
on (cir2.ItemRecordID = ird2.ItemRecordID)
INNER join Polaris.Polaris.Organizations oi2 with (nolock)
on (cir2.AssignedBranchID = oi2.OrganizationID)
where cir2.AssignedBranchID in (4,5,16,17,6,7,3,8,9,10,11,12,13,14)
and cir2.ItemStatusID in (2)
AND cir2.AssignedCollectionID NOT IN (10, 22, 30, 39, 40, 41)
GROUP BY col2.Name,oi2.Abbreviation
) sub1
ON (oi.Abbreviation = sub1.Abbreviation AND col.Name = sub1.Name)
FULL JOIN (
select oi3.Abbreviation, col3.Name, count(*) as 'NumIn'
FROM Polaris.Polaris.CircItemRecords cir3 with (nolock)
INNER join Polaris.Polaris.Collections col3 with (nolock)
on (cir3.AssignedCollectionID = col3.CollectionID)
INNER JOIN Polaris.Polaris.ItemStatuses ist3 (NOLOCK)
ON cir3.ItemStatusID = ist3.ItemStatusID
INNER join Polaris.Polaris.ItemRecordDetails ird3 with (nolock)
on (cir3.ItemRecordID = ird3.ItemRecordID)
INNER join Polaris.Polaris.Organizations oi3 with (nolock)
on (cir3.AssignedBranchID = oi3.OrganizationID)
where cir3.AssignedBranchID in (4,5,16,17,6,7,3,8,9,10,11,12,13,14)
and cir3.ItemStatusID in (1)
AND cir3.AssignedCollectionID NOT IN (10, 22, 30, 39, 40, 41)
GROUP BY col3.Name,oi3.Abbreviation
) sub2
ON (oi.Abbreviation = sub2.Abbreviation AND col.Name = sub2.Name)
where oi.OrganizationID in (4,5,16,17,6,7,3,8,9,10,11,12,13,14)
AND col.CollectionID NOT IN (10, 22, 30, 39, 40, 41)
ORDER BY oi.Abbreviation, col.Name

We're telling it explicitly which branches we are interested in and which collections we are not.  These are the lines which you'll need to adjust to fit your own system:

- where cir2.AssignedBranchID in (4,5,16,17,6,7,3,8,9,10,11,12,13,14) 
- AND cir2.AssignedCollectionID NOT IN (10, 22, 30, 39, 40, 41)
- where cir3.AssignedBranchID in (4,5,16,17,6,7,3,8,9,10,11,12,13,14) 
- AND cir3.AssignedCollectionID NOT IN (10, 22, 30, 39, 40, 41)
- where oi.OrganizationID in (4,5,16,17,6,7,3,8,9,10,11,12,13,14) 
- AND col.CollectionID NOT IN (10, 22, 30, 39, 40, 41)

If this isn't working for you, please let me know.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/3dc316679add4930bcbcbc4d514925b1.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_617"></a>[Accept as answer](#)

<a id="commentAns_617"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [SQL for items returned late by collection](https://iii.rightanswers.com/portal/controller/view/discussion/718?)
- [SQL - looking for a report on how many fines we have for a specific collection code and how many were waived.](https://iii.rightanswers.com/portal/controller/view/discussion/673?)
- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)

### Related Solutions

- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [Polaris Collection Agency job after upgrading to SQL2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930771502651)
- [Collection Agency SQL Job Not Running](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930837487784)
- [System Administration Overview--Parameters, Profiles, Tables, Collections and SQL Jobs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160908152552966)
- [Collection workform "branches" checkboxes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930545936496)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)