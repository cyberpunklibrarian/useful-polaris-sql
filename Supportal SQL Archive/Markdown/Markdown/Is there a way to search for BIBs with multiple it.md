Is there a way to search for BIBs with multiple items? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Is there a way to search for BIBs with multiple items?

0

72 views

Is there a way to search for BIBs with multiple items? (i.e. several copies of the same book)

asked 3 years ago  by <img width="18" height="18" src="../../_resources/3c2d1c0757f84956a79e781596b79bb3.jpg"/>  rhalterman@coralville.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 3 years ago

<a id="upVoteAns_167"></a>[](#)

0

<a id="downVoteAns_167"></a>[](#)

Thank you!

answered 3 years ago  by <img width="18" height="18" src="../../_resources/3c2d1c0757f84956a79e781596b79bb3.jpg"/>  rhalterman@coralville.org

- <a id="acceptAnswer_167"></a>[Accept as answer](#)

<a id="commentAns_167"></a>[Add a Comment](#)

<a id="upVoteAns_166"></a>[](#)

0

<a id="downVoteAns_166"></a>[](#)

Hi.

There is a view in Polaris called RWriter_BibDerivedDataView that includes information on the number of items linked to a bib record. To search in the Find Tool, you can do an SQL query like this:

SELECT BibliographicRecordID from RWriter_BibDerivedDataView with (nolock)

WHERE NumberofItems > 1

Depending on your library's holdings, you may get a large number of records. You can set the NumberoFItems to a larger number, or select the Count Only option if you just want to know how many.

Please note that this includes items of all statuses. The View also includes counts of Lost, Missing, Claimed and Withdrawn items, so it is possible to construct a query that will omit titles where there is only one "good" item.

To do this, use

SELECT BibliographicRecordID from RWriter_BibDerivedDataView with (nolock)

WHERE (NumberofItems - NumberLostItems - NumberMissingItems - NumberClaimRetItems - NumberWithdrawnItems) > 1

Hope this helps.

JT

answered 3 years ago  by <img width="18" height="18" src="../../_resources/3c2d1c0757f84956a79e781596b79bb3.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_166"></a>[Accept as answer](#)

<a id="commentAns_166"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Is there an SQL to change item level holds to bib level holds?](https://iii.rightanswers.com/portal/controller/view/discussion/407?)
- [Is there a way to set a longer held period for one patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/983?)
- [Way to allow staff to waive fines from Resolve Lost Item window but NOT from patron account?](https://iii.rightanswers.com/portal/controller/view/discussion/305?)
- [Is anyone that has multiple locations using Borrow by Mail?](https://iii.rightanswers.com/portal/controller/view/discussion/552?)
- [I want to send out as multiple unique domains (different for every library) through Office 365.](https://iii.rightanswers.com/portal/controller/view/discussion/171?)

### Related Solutions

- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [How to create bulk holds from a record set](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161128141614846)
- [Why can't I link to the holds queue from an item record?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989583652)
- [Linking holds on multiple bib records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930439017974)
- [Lifetime circulation counts differ on bib preview and item statistics](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930410221804)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)