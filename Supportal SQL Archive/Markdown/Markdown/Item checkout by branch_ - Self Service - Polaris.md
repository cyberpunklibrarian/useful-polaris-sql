Item checkout by branch? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Item checkout by branch?

0

125 views

Hello,

Is it possible to find out how many times an item has been checked out from a certain location?  Staff member wants to see if items are doing better on their shelves than other locations, or if it makes sense to send the item to a different branch.

Thanks!

Tom

asked 2 years ago  by <img width="18" height="18" src="../../_resources/4fb6e6b4a1184f9d9e8997280d25b423.jpg"/>  tbaxter@guelphpl.ca

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_620"></a>[](#)

0

<a id="downVoteAns_620"></a>[](#)

Answer Accepted

Hi Tom.

I'm not sure if this is exactly what you are looking for, but it generates a list of all items currently assigned to a branch and gives a count of checkouts and renewals at that branch and a separate count of checkouts/renewals at any other branch.

You will need to change the OrganizationID (2 occurrences) and BranchID (1 occurrence) and if you want to exclude renewals, delete the appropriate  ActionTakenIDs from the list. (There's a comment indicating which are for renewals.)

If you want to narrow the list, you can add conditions (like collection, statistical code, etc.) between WHERE and GROUP BY.

Hope this helps.

JT

SELECT
ir.Barcode
,ir.CallNumber
,ISNULL(ir.VolumeNumber, '') AS Volume
,br.BrowseTitle
,COUNT(CASE WHEN irh.OrganizationID = 7 THEN irh.ItemRecordHistoryID END) AS ThisBranch
,COUNT(CASE WHEN irh.OrganizationID <> 7 THEN irh.ItemRecordHistoryID END) AS OtherBranch

FROM Polaris.Polaris.Itemrecords ir WITH (NOLOCK)
JOIN Polaris.Polaris.BibliographicRecords br (NOLOCK)
ON (ir.AssociatedBibRecordID = br.BibliographicRecordID)
JOIN Polaris.Polaris.ItemRecordHistory irh (NOLOCK)
ON (ir.ItemRecordID = irh.ItemRecordID AND irh.ActionTakenID in (13,75,77,78,79,81,89,91,28,73,76,80,82,93)) --28 and after are renewals

WHERE ir.AssignedBranchID = 7

GROUP BY
ir.Barcode
,ir.CallNumber
,ir.VolumeNumber
,br.BrowseTitle

ORDER BY
ir.CallNumber
,ISNULL(ir.VolumeNumber, '')

answered 2 years ago  by <img width="18" height="18" src="../../_resources/4fb6e6b4a1184f9d9e8997280d25b423.jpg"/>  jwhitfield@aclib.us

Also, if you want to compare one branch with another, you can change the "<> 7" in the second COUNT statement to be "= 8" or IN (6,9,10) if you want to compare to a small group. JT

— jwhitfield@aclib.us 2 years ago

<a id="commentAns_620"></a>[Add a Comment](#)

<a id="upVoteAns_624"></a>[](#)

0

<a id="downVoteAns_624"></a>[](#)

This is perfect.

Thank you very much JT!

Tom

answered 2 years ago  by <img width="18" height="18" src="../../_resources/4fb6e6b4a1184f9d9e8997280d25b423.jpg"/>  tbaxter@guelphpl.ca

- <a id="acceptAnswer_624"></a>[Accept as answer](#)

<a id="commentAns_624"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for Item records from PO/FY/Branch](https://iii.rightanswers.com/portal/controller/view/discussion/326?)
- [SQL for bibs with holds and no owned items by branches](https://iii.rightanswers.com/portal/controller/view/discussion/821?)
- [Checkouts by Title in a collection at a branch by year](https://iii.rightanswers.com/portal/controller/view/discussion/932?)
- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [Report of Lost and Paid Items](https://iii.rightanswers.com/portal/controller/view/discussion/331?)

### Related Solutions

- [Change text on checkout receipts](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930905424507)
- [Block checkouts in SIP when item is out](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930597906901)
- [ExpressCheck: Held items can be checked out by the wrong patrons](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930574330183)
- [Patron / Material Type Loan Limit Blocks policy table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930651711850)
- [Polaris Expresscheck: Checkout Error: Item in Hand](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930924556710)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)