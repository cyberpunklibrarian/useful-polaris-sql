[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL to list foreign language films?

0

45 views

Hello hello! A coworker is trying to find all our foreign language DVDs. Apparently there's not a subject heading for that, so I'm trying to construct a search for NOT 'eng' in the 008 field. Is that a thing?! Thoughts?? Thanks in advance!

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_e9141b0f348b4714a8cbaa51873c3461.jpg"/> elinacre@altoona-iowa.com

[sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 2 years ago

<a id="upVoteAns_1240"></a>[](#)

0

<a id="downVoteAns_1240"></a>[](#)

Answer Accepted

Hi, 

My cataloging knowledge is pretty basic so please let me know if I've overlooked something, but I used your idea of excluding 'eng' in the 008 field to make this query. It seems to grab all of our DVDs that are in other languages. You didn't specify if you wanted bibs or items so I wrote it for bibliographic records, and I wasn't sure if you wanted to make a report or just use it in the Find Tool so I set it up to work either way.

SELECT br.BibliographicRecordID  
FROM Polaris.Polaris.BibliographicRecords AS br  
          INNER JOIN Polaris.Polaris.BibliographicTagsAndSubfields_View AS btas_v  
             ON btas_v.BibliographicRecordID = br.BibliographicRecordID  
WHERE br.PrimaryMARCTOMID = 33  
            AND btas_v.TagNumber = 8  
            AND SUBSTRING(btas_v.Data, 36,3) <> 'eng'

I hope this helps you get the data you need. 

&nbsp;    -Daniel

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_e9141b0f348b4714a8cbaa51873c3461.jpg"/> ddunphy@aclib.us

<a id="commentAns_1240"></a>[Add a Comment](#)

<a id="upVoteAns_1242"></a>[](#)

0

<a id="downVoteAns_1242"></a>[](#)

Y'all are amazing! Thank you both so much!!

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_e9141b0f348b4714a8cbaa51873c3461.jpg"/> elinacre@altoona-iowa.com

- <a id="acceptAnswer_1242"></a>[Accept as answer](#)

<a id="commentAns_1242"></a>[Add a Comment](#)

<a id="upVoteAns_1241"></a>[](#)

0

<a id="downVoteAns_1241"></a>[](#)

It's possible someone with more cataloging knowledge will know of a more precise way to accomplish it, but this should give you a list of bibs that don't have 'eng' somewhere in their 008:

```markup
select bt.BibliographicRecordID
from Polaris.Polaris.BibliographicTags bt
join Polaris.polaris.BibliographicSubfields bs
	on bs.BibliographicTagID = bt.BibliographicTagID
where bt.TagNumber = 008
	and bs.Data not like '%eng%'
```

&nbsp;

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_e9141b0f348b4714a8cbaa51873c3461.jpg"/> mfields@clcohio.org

- <a id="acceptAnswer_1241"></a>[Accept as answer](#)

<a id="commentAns_1241"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Report for custom Hold Requests to Fill list](https://iii.rightanswers.com/portal/controller/view/discussion/91?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)
- [SQL - 650 subject](https://iii.rightanswers.com/portal/controller/view/discussion/1228?)
- [Can you get a list of exact dates an item was placed on hold?](https://iii.rightanswers.com/portal/controller/view/discussion/1031?)
- [SQL for changes to a patron record](https://iii.rightanswers.com/portal/controller/view/discussion/840?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Foreign language articles not displaying correctly in PAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930580096138)
- [Vega Discover: Removing Stored Language Key from Browser](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=231218193533190)
- [IUG 2015: Z39.50: Searching and displaying foreign language material](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160418160638864)
- [Where can I find a list of the tables and columns used in the Polaris SQL databases?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170224120334527)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)