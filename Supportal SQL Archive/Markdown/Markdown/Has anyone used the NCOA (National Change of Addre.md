Has anyone used the NCOA (National Change of Address) Service available from Unique Management Services? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=56)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Third Party](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=56)

# Has anyone used the NCOA (National Change of Address) Service available from Unique Management Services?

0

56 views

Our library district is interested in using the National Change of Address (NCOA) service available with Unique Management Services to update our patron addresses with any new addresses on file with the United States Post Office.  If anyone has used this service, I would be interested in hearing about your results and any procedures implemented.

Thank you.

Brad Winner

asked 2 years ago  by <img width="18" height="18" src="../../_resources/38b6aceaef8b46de8bc2f5bc12cfcbae.jpg"/>  bwinner@stchlibrary.org

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 2 years ago

<a id="upVoteAns_682"></a>[](#)

1

<a id="downVoteAns_682"></a>[](#)

Just following up with a question.  Are your member libraries then going into each Patron Record and manually updating the address or are you importing the contents of the spreadsheet into the Polaris database and updating the addresses using SQL?

Rex Helwig
Finger Lakes Library System
rhelwig@flls.org

answered 2 years ago  by <img width="18" height="18" src="../../_resources/38b6aceaef8b46de8bc2f5bc12cfcbae.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_682"></a>[Accept as answer](#)

Hi Rex,

For now, our member libraries are going into each Patron Record and manually updating the address - some of them may not go into the record to add a 7 digit zip code; some may want to add a block for various reasons, etc.  Some committee mebers have been working on a procedures document and it's almost done.  We hadn't considered updating the addresses using SQL, wondering if inconsistencies in the patron record entry would be an issue.  Are you going to do an SQL replacement?  If so, would you please share your SQL script?  Thanks.  Sylvia 

— sylvial@wccls.org 2 years ago

Sylvia,

This is on my to-do list so we're not there yet.  Colleen Medling (cmedling@slcolibrary.org) at Salt Lake County Library Services did a presentation on this at one of the past PUG/IUG conferences.  She had a whole procedure on how to import it into the Polaris Database and update the addresses with SQL Updates.  I'm not sure if she is still there or not but here is the iformation she sent me.

From Colleen Medling 6/15/2016 to Rex Helwig:
It’s actually very straight forward.  Once you receive the data back from Unique you can run the attached script to change everyone’s addresses.If you have non-residents it is a bit more work.  First you’ll have to determine from the file receive from Unique who are out of area patrons and insert them into a different table.  We normally use the script to change their addresses as well then use the procedure below :

1. Gather email addresses and inform patrons their library card will expire.  They can purchase a library membership if they so desire (and we do have people that do this)
2. Use the Polaris client (find tool) to run the following query against the temp table
SELECT DISTINCT nrt.PatronID
FROM slco_nonResidentTemp nrt
INNER JOIN Patrons p ON nrt.PatronID = p.PatronID
   WHERE p.OrganizationID = 4
save the records into a record set.  We do this by branch since when you batch change patron data it will change everyone to one branch.
3. Bulk change :
a. Patron code to Non-resident
b. Expire date – 30 days
c. Add non-blocking note
4. Delete the record sets
5. Delete the temp tables

\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-

SQL Script Colleen provided in her email to me which will probably need some minor editing (replace "slco"):

USE \[Polaris\]
GO/\*\*\*\*\*\* Object:  StoredProcedure \[Polaris\].\[slco_UpdatePatronAddressesFromUnique\]    Script Date: 7/30/2015 2:53:32 PM ******/SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO/\* slco_UpdatePatronAddressesFromUnique.sql */
/\* Changes Patron addresses in Addresses table from a temporary table. */
/\* Created S.Glenn 7/30/2015*/
/\* Mod. S.Glenn 8/6/2015 source file name change*/
ALTER PROCEDURE \[Polaris\].\[slco_UpdatePatronAddressesFromUnique\]
AS
BEGIN
    SET NOCOUNT ON

/\* Drop temp tables */
IF OBJECT\_ID('Polaris.slco\_AddressTemp') IS NOT NULL
BEGIN
    DROP TABLE Polaris.slco_AddressTemp
END
IF OBJECT\_ID('Polaris.slco\_nonResidentTemp') IS NOT NULL
BEGIN
    DROP TABLE Polaris.slco_nonResidentTemp
END

/\*Create the temp table \*/
CREATE TABLE Polaris.slco_AddressTemp(
   \[PatronID\] \[int\] NOT NULL,
   \[StreetOne\] \[varchar\](https://iii.rightanswers.com/portal/controller/view/discussion/64) NULL,
   \[StreetTwo\] \[varchar\] (64) NULL,
   \[City\] \[varchar\] (32) NOT NULL,
   \[State\] \[varchar\] (32) NOT NULL,
   \[PostalCode\] \[varchar\] (12) NULL,
   \[ZipPlusFour\] \[varchar\] (4) NULL,
   \[AddressID\] \[int\] NULL,
   \[PostalCodeID\] \[int\] NULL)

/\*Bulk insert to temp table from a file\*/
    BULK INSERT Polaris.slco_AddressTemp
    FROM 'C:\\Test\\uniqueAddressChange.csv'
    WITH
    (
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\\n'
    )

/\* Send Out of County addresses to temp table for processing through record set*/
SELECT DISTINCT sat.PatronID,a.AddressID,pc.PostalCodeID,pr.PatronFullName,a.StreetOne,a.StreetTwo,a.ZipPlusFour
    INTO Polaris.slco_nonResidentTemp
        FROM (((((Polaris.slco_AddressTemp sat
        INNER JOIN Polaris.Patrons p ON sat.PatronID = p.PatronID)
        INNER JOIN Polaris.PatronRegistration pr ON pr.PatronID = p.PatronID)
        INNER JOIN Polaris.PatronAddresses pa ON p.PatronID = pa.PatronID)
        INNER JOIN Polaris.Addresses a ON pa.AddressID = a.AddressID)
        INNER JOIN Polaris.PostalCodes pc ON a.PostalCodeID = pc.PostalCodeID)
        WHERE
        sat.PostalCode NOT IN (SELECT zipcodesCounty FROM slco\_salt\_lake_ZipCodes)

/\* Update PostalCodeID in slco_AddressTemp */
UPDATE Polaris.slco_AddressTemp
    SET PostalCodeID = pc.PostalCodeID
    FROM slco_AddressTemp sat
        LEFT JOIN Polaris.PostalCodes pc
        ON sat.PostalCode = pc.PostalCode
    WHERE sat.City = pc.City

/\* Update AddressID in slco_AddressTemp */
UPDATE Polaris.slco_AddressTemp
    SET AddressID = pa.AddressID
    FROM slco_AddressTemp sat
        LEFT JOIN Polaris.PatronAddresses pa
        ON sat.PatronID = pa.PatronID

/\* Update all addresses from temp table */
UPDATE Polaris.Addresses
    SET StreetOne = sat.StreetOne,
        StreetTwo = sat.StreetTwo,
        ZipPlusFour = sat.ZipPlusFour,
        PostalCodeID = sat.PostalCodeID
    FROM slco_AddressTemp sat
    INNER JOIN Polaris.Addresses a
    ON sat.AddressID = a.AddressID

END

— rhelwig@flls.org 2 years ago

<a id="commentAns_682"></a>[Add a Comment](#)

<a id="upVoteAns_677"></a>[](#)

1

<a id="downVoteAns_677"></a>[](#)

Hi Brad,

We started using ghe NCOA service from Unique Management Service (UMS) last year and we were pleased.  We decided to extend the expiration period for our patron code that most patrons use (from 2 yrs to 4 yrs) and were concerned about keeping the mailing addresses current.  We contact UMS and they were great.  Our county required a contact and the UMS folks were totally helpful and understanding about the process.  Since it was the first time we used the NCOA service we had a lot of changes and our libraries developed a procedure for implementing the corrections after UMS sent us the corrections.  Last year we sent 268,321 patron addresses and the turnaround was within 3 days - faster than we expected.  We have 14 libraries in our consortium and sent a "master file" containing records for all of our libraries.  We included the Registered Branch for each entry so we could sort the file and send each library the file for their branch.  We're planning on sending a file this Sept/Oct and will also include the patron code and last activity date at the request of the circulation staff.

Please fee free to contact me at:  [sylvial@wccls.org](mailto:sylvial@wccls.org)  if you have further questions.  Best - Sylvia

answered 2 years ago  by <img width="18" height="18" src="../../_resources/38b6aceaef8b46de8bc2f5bc12cfcbae.jpg"/>  sylvial@wccls.org

- <a id="acceptAnswer_677"></a>[Accept as answer](#)

<a id="commentAns_677"></a>[Add a Comment](#)

<a id="upVoteAns_684"></a>[](#)

0

<a id="downVoteAns_684"></a>[](#)

We were trying to do this with quipu and the offline patron (AKA student import) file a few years ago. We developed a report that included the patron information in the proper offline file format and we would then export it to quipu, they developed something that would update the addresses, but keep the rest of the file format the same and then we tried to re-import it.

There were problems with the addresses on the re-import not getting assigned to the proper county. Part of that was fixed in 6.1 but then there were other issues introduced because of the Name Titles getting switched to database fields. I think these things go cleared up in 6.3, so we'll be trying this process again. Hopefully with better luck this time.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/38b6aceaef8b46de8bc2f5bc12cfcbae.jpg"/>  wosborn@clcohio.org

- <a id="acceptAnswer_684"></a>[Accept as answer](#)

<a id="commentAns_684"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Ebsco Discovery Service?](https://iii.rightanswers.com/portal/controller/view/discussion/280?)
- [Integrating EDS into the PAC](https://iii.rightanswers.com/portal/controller/view/discussion/841?)
- [Is anyone using the "24-Hour Library, Envisionware's unique self-service library branch?" If you are could, you please share any information you have about it, the good, the bad and the ugly.](https://iii.rightanswers.com/portal/controller/view/discussion/448?)
- [hoopla & SIP changes - erg](https://iii.rightanswers.com/portal/controller/view/discussion/651?)
- [Does your library use Kanopy?](https://iii.rightanswers.com/portal/controller/view/discussion/256?)

### Related Solutions

- [Changing Address Check Term](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930723806393)
- [What happens when we delete a postal code that's in use?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930308320846)
- [System Changes Made by Innovative Staff](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160125092907532)
- [3.6 Patron Services - First Available Copy Holds](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160908153810159)
- [Blocks for expired registration and address checks](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930502649491)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)