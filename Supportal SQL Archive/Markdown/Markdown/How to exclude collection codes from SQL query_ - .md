[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# How to exclude collection codes from SQL query?

0

64 views

Hello! The following query displays all the bib records in our teen collection that has more than one item attached to the record. However, the quantity still counts items that are in a different collection code that also points to the same bib. For example, we have a graphic novel in the teen collection and in our graphic novels collection. How do I exclude the collections that are not teens from the final quantity count?  
<br/>

```
`SELECT DISTINCT`

`br.BibliographicRecordID AS "Bib Record ID",`

`br.SortTitle AS "Title",`

`br.BrowseAuthor AS "Author",`

`mt.Description AS "Kind",`

`rw.NumberOfItems As "Qty"`

`FROM Polaris.Polaris.BibliographicRecords br WITH (NOLOCK)`

`JOIN Polaris.Polaris.CircItemRecords ci (nolock)`

`ON (ci.AssociatedBibRecordID = br.BibliographicRecordID)`

`JOIN Polaris.Polaris.RWRITER_BibDerivedDataView rw (NOLOCK)`

`ON (br.BibliographicRecordID = rw.BibliographicRecordID)`

`JOIN Polaris.Polaris.MaterialTypes mt (nolock)`

`ON (mt.MaterialTypeID = ci.MaterialTypeID)`

`WHERE ci.AssignedCollectionID IN (118,129,130,215,220,221,222,223,224,225,226,227) AND rw.NumberofItems >= 2`

`ORDER BY 5 ASC;`
```

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_644f5cd2e52d450f853bc08b650129da.jpg"/> agoodman@darienlibrary.org

[sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 2 years ago

<a id="upVoteAns_1183"></a>[](#)

0

<a id="downVoteAns_1183"></a>[](#)

Answer Accepted

I think this should give you what you're looking for

```
select br.BibliographicRecordID [Bib Record ID]
 ,br.SortTitle [Title]
 ,br.BrowseAuthor [Author]
 ,mt.Description [Kind]
 ,count(cir.ItemRecordID) [Qty]
from Polaris.Polaris.BibliographicRecords br
join polaris.polaris.CircItemRecords cir
 on cir.AssociatedBibRecordID = br.BibliographicRecordID
join polaris.polaris.MaterialTypes mt
 on mt.MaterialTypeID = cir.MaterialTypeID
where cir.AssignedCollectionID in (118,129,130,215,220,221,222,223,224,225,226,227)
group by br.BibliographicRecordID, br.SortTitle, br.BrowseAuthor, mt.Description
having count(cir.ItemRecordID) > 1
order by [Qty] asc
```

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_644f5cd2e52d450f853bc08b650129da.jpg"/> mfields@clcohio.org

<a id="commentAns_1183"></a>[Add a Comment](#)

<a id="upVoteAns_1185"></a>[](#)

0

<a id="downVoteAns_1185"></a>[](#)

It did work! 

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_644f5cd2e52d450f853bc08b650129da.jpg"/> agoodman@darienlibrary.org

- <a id="acceptAnswer_1185"></a>[Accept as answer](#)

<a id="commentAns_1185"></a>[Add a Comment](#)

<a id="upVoteAns_1184"></a>[](#)

0

<a id="downVoteAns_1184"></a>[](#)

Thank you! I'm trying to check it against our collection to see if I get the expected results before formally "Accept as answer" on here. 

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_644f5cd2e52d450f853bc08b650129da.jpg"/> agoodman@darienlibrary.org

- <a id="acceptAnswer_1184"></a>[Accept as answer](#)

<a id="commentAns_1184"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL - looking for a report on how many fines we have for a specific collection code and how many were waived.](https://iii.rightanswers.com/portal/controller/view/discussion/673?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [Circulation by Item Statistical Code & limited by assigned collection?](https://iii.rightanswers.com/portal/controller/view/discussion/801?)
- [Help with automatically exporting SQL query to CSV file](https://iii.rightanswers.com/portal/controller/view/discussion/1399?)
- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)

### Related Solutions

- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)
- [Collection workform "branches" checkboxes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930545936496)
- [Export from old Supportal of forum posts with SQL queries](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161214213310321)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)