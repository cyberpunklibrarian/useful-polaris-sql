[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# How can edit the content of notifications, not the text, but what is pulled to go into the notifications. Specifically I want to remove a section about how many renewals a patron has left.

0

45 views

So the fields are:

Date Due     Title     Format m    Call No.    From      **Renewals remaining**

**The renewals remainins is the column I want to remove.**

**Thanks!**

**Ron**

asked 10 months ago by ![tech@salinelibrary.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_tech@salinelibrary.org20211216111845.jpg) tech@salinelibrary.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 10 months ago

<a id="upVoteAns_1408"></a>[](#)

0

<a id="downVoteAns_1408"></a>[](#)

Hi Ron,

For email and print notififcations you can edit the notices directly in SSRS report builder, if you have access to that.  It's the SQL application used to create and manage reports printed from Leap, including the notices.  You may need to speak to your site manager about it if you don't have access today.  There is a bit of a learning curve with ReportBuilder, but if you run into a specific issue and post about it I bet someone can help with questions.  Definitely a useful app to use if you'd like to add custom reports, though!

There is a way to add custom versions of these notices by adding them to a custom reports path in SSRS (which your site manager can also likely speak to), although this didn't work for us.  We ended up simply overwriting some of the notices with custom versions, and replacing them after upgrades (since that resets those reports to a default state).  Still, likely worth trying the custom route.

We've had SMS and phone notices on a different service for a while, and I'm not sure if this is a factor for either of those...

\-Jason

answered 10 months ago by <img width="18" height="18" src="../_resources/default-avatar_ec48d996678f4513b44e1005477c70c8.jpg"/> jtenter@sasklibraries.ca

- <a id="acceptAnswer_1408"></a>[Accept as answer](#)

<a id="commentAns_1408"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [T-Mobile patrons not receiving text notifications](https://iii.rightanswers.com/portal/controller/view/discussion/982?)
- [Remove the notification option of fax?](https://iii.rightanswers.com/portal/controller/view/discussion/694?)
- [Have any libraries discontinued their Telephony service for renewal and notifications?](https://iii.rightanswers.com/portal/controller/view/discussion/1296?)
- [Is there a way to search for patrons by what they have their notification setting as? i.e. is there a way to find any patrons that were accidentally set as fax instead of text?](https://iii.rightanswers.com/portal/controller/view/discussion/1198?)
- [Does anyone have an SQL for patrons whose notifications settings are listed as "none"?](https://iii.rightanswers.com/portal/controller/view/discussion/621?)

### Related Solutions

- [How Do Almost Overdue Reminder Notices and Autorenew Interact with Library Closed Dates?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181218172047064)
- [Items not auto-renewed for specific patron](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930760925871)
- [Long Distance Phone Numbers and Text Messages](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930379236869)
- [Setting up phone notification with additional TXT messages](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930542970896)
- [Would like a way to gather all patrons with no notification option selected](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930285364780)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)