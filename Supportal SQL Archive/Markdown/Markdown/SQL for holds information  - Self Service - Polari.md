[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL for holds information

0

113 views

I’m trying to find some historical information on holds, in order to see if specific collections/items are checked out more at one location than another. And I’d like to run it by specific dates for when the hold was created (e.g. 1/1/21 – 12/31/21) Here’s what I’d like to see:

- Item assigned branch
- Hold pickup branch
- Hold creation date
- Item collection
- Item call #
- Item barcode
- Item title
- Item material type

I can run a report in Simply Reports, but that only shows the holds that are currently active and/or checked out. I'm looking for information on all past holds.

Any SQL help? Thanks!

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_a3179390a65846bf92da38831d2ded18.jpg"/> musack@bcls.lib.nj.us

[holds](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=96#t=commResults) [requests](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=181#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL Find hold queue override by staff](https://iii.rightanswers.com/portal/controller/view/discussion/1388?)
- [Is there an SQL to change item level holds to bib level holds?](https://iii.rightanswers.com/portal/controller/view/discussion/407?)
- [I need a SQL for locating hold requests where the hold has been denied by a library](https://iii.rightanswers.com/portal/controller/view/discussion/843?)
- [Does anyone have an SQL for a request ratio?](https://iii.rightanswers.com/portal/controller/view/discussion/394?)
- [SQL Query for how many first time borrowers are checking out holds?](https://iii.rightanswers.com/portal/controller/view/discussion/504?)

### Related Solutions

- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [Hold Processing SQL jobs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930920183263)
- [Query to find unfillable holds due to volume data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930242167096)
- [Unable to delete two holds, error message about ILL$Messages appears](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930410469399)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)