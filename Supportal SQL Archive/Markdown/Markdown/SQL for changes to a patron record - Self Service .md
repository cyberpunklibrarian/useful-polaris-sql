[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for changes to a patron record

0

109 views

Anyone have a query that will show all changes made to a patron record?

&nbsp;

asked 4 years ago by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG) rdye@krl.org

[sql patron](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=387#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Last modifier of patron record](https://iii.rightanswers.com/portal/controller/view/discussion/1048?)
- [SQL or Simply Report Active Patrons](https://iii.rightanswers.com/portal/controller/view/discussion/1164?)
- [SIP logins recorded in Transactions database?](https://iii.rightanswers.com/portal/controller/view/discussion/1357?)
- [SQL for Item records from PO/FY/Branch](https://iii.rightanswers.com/portal/controller/view/discussion/326?)
- [SQL query to export specific record set: Polaris 4.1R2](https://iii.rightanswers.com/portal/controller/view/discussion/93?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Patron registration numbers jumped in value](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989626569)
- [Find all permission groups that have the Secure patron record permissions](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930759206464)
- [Changing the Polaris password in SQL](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930597029785)
- [How do I change the text of the You Saved receipts?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170119085231340)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)