Is there an SQL to change item level holds to bib level holds? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Is there an SQL to change item level holds to bib level holds?

0

71 views

Is there an SQL to change item level holds to bib level holds?

asked 2 years ago  by <img width="18" height="18" src="../../_resources/5d4a2413bc6f4b28bde3b18dc3468db0.jpg"/>  rhalterman@coralville.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_466"></a>[](#)

0

<a id="downVoteAns_466"></a>[](#)

Hi Ruth--

This is what we use at Chicago Public Library:

update polaris.polaris.SysHoldRequests
set itembarcode = null, ItemLevelHold = 0, ItemLevelHoldItemRecordID = null, volumenumber = null
from polaris.polaris.SysHoldRequests
where SysHoldRequestID = \[put hold request id here\]

We've been using this for a while now but have never tested it systematically so use at your own risk.

Paul

Paul Keith

Technology Department

Chicago Public Library

answered 2 years ago  by <img width="18" height="18" src="../../_resources/5d4a2413bc6f4b28bde3b18dc3468db0.jpg"/>  pkeith@chipublib.org

- <a id="acceptAnswer_466"></a>[Accept as answer](#)

<a id="commentAns_466"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Are cancelled holds considered holds in holds item count?](https://iii.rightanswers.com/portal/controller/view/discussion/414?)
- [I need a SQL for locating hold requests where the hold has been denied by a library](https://iii.rightanswers.com/portal/controller/view/discussion/843?)
- [Does anyone know how to edit the text in the warning about items not filling holds?](https://iii.rightanswers.com/portal/controller/view/discussion/376?)
- [Does anyone have an SQL for a request ratio?](https://iii.rightanswers.com/portal/controller/view/discussion/394?)
- [Considering becoming a Fines Free library! Any advice from an ILS POV?](https://iii.rightanswers.com/portal/controller/view/discussion/465?)

### Related Solutions

- [How to create bulk holds from a record set](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161128141614846)
- [Request icon in PowerPAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930435193073)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)
- [Why can't I link to the holds queue from an item record?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989583652)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)