[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Checking in holds for specific branches

0

48 views

Hello,  
Hoping someone can get at this in a single report as a SQL on the find tool. I'm looking for a report that lists:  
1\. Holds on bibs with more than 1 item record that have a status of in-process.  
2\. We want to know the branch of each requestor in order so we can  
3\. Check-in materials so the copy slated for a branch is going to fulfill the request at that branch.

In other words, if we've got 12 copies of a book with 4 holds, we want the 1st copies checked in to match the branch associated with the holds. I can get at this but it's a pain in the neck to do manually and I don't think we've managed to get all the info into a single report.

Any SQL love out there? :)  
Thanks for any help you can provide!

Erin Shield

North Olympic Library System

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_640c434489364a60b6b6298abfe4942f.jpg"/> eshield@nols.org

[floating](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=12#t=commResults) [holds](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=96#t=commResults) [item record](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=268#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Is there a way to set staff placed holds for patrons to default to patron branch instead of branch location when hold is placed?](https://iii.rightanswers.com/portal/controller/view/discussion/799?)
- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)
- [SQL Query for how many first time borrowers are checking out holds?](https://iii.rightanswers.com/portal/controller/view/discussion/504?)
- [Can we stop patrons from placing holds on items they already have checked out or being Held?](https://iii.rightanswers.com/portal/controller/view/discussion/958?)
- [Is there a way to proactively notify patrons if the item thay have checked out has a Hold Request on it?](https://iii.rightanswers.com/portal/controller/view/discussion/1329?)

### Related Solutions

- [Why didn't an item trap according to the branch's "Prefer my..." preferences?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181121103836921)
- [Re-routing holds during brief branch closure](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930587478425)
- [Receiving "Conversion failed when converting the varchar value ' V' to data type int." error when setting Special Loan Period for a specific branch](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170717102645744)
- [INN-Reach: Items that are transferred back from the branches to Main are not auto-returning when checked in](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930314735486)
- [Can we suspend the hold option for specific titles?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930255045537)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)