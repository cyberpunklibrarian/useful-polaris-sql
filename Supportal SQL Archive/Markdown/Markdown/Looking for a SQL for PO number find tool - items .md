Looking for a SQL for PO number find tool - items not invoiced - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Looking for a SQL for PO number find tool - items not invoiced

0

57 views

I am currently clicking each and every PO to find items that have not yet been invoiced so we can follow up on those before we roll over. Is there a SQL I can run in PO Find Tool that will pull up POs that have POLIs not yet invoiced so I don't have to click each and every one?

Thanks in advance!

asked 2 years ago  by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg)  sbills@lpld.lib.in.us

[pos](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=177#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 1 year ago

<a id="upVoteAns_465"></a>[](#)

0

<a id="downVoteAns_465"></a>[](#)

Answer Accepted

Hi.

Here are a couple of queries. The first searches for purchase orders, and the second searches for PO lines (in the appropriate Find Tool).

(Purchase Orders Find Tool)

SELECT PurchaseOrderID from POLineItemSegments WITH (NOLOCK)

WHERE Invoiced = 0

AND StatusID = 13

(Purchase Order Line Items Find Tool)

SELECT POLineItemID from POLineItemSegments WITH (NOLOCK)

WHERE Invoiced = 0

AND StatusID = 13

The StatusID = 13 means the PO line has been received.

Here are the IDs and descriptions of the other statuses:

StatusID Status
1 Backordered
2 Canceled
3 Pending Claim
4 Claimed
5 Completed
6 Confirmed
7 Currently Received
8 Exceptional Condition
9 Never Published
10 On Order
11 Out of Print
12 Pending
13 Received
15 Return Requested
16 Returned
18 Partly Rec
19 Closed
20 Not Yet Published

You can change the ID to search for a different status,

or search for any that do not have a particular ID by using "<>" and the ID instead of "=",

or search for a set of IDs by using AND StatusID IN (13,18) ,

or exclude a set of IDs by using AND STATUSID NOT IN (2,4,16) .

Hope this helps,

JT

answered 2 years ago  by <img width="18" height="18" src="../../_resources/d514dc5cb07b4e4aa16c1b47adfabdc0.jpg"/>  jwhitfield@aclib.us

Thank you so much, JT!

— sbills@lpld.lib.in.us 2 years ago

<a id="commentAns_465"></a>[Add a Comment](#)

<a id="upVoteAns_965"></a>[](#)

0

<a id="downVoteAns_965"></a>[](#)

Here's a query with more output fields that I think will work for you. I looked for a view that would avoid lots of joins in the query, but I didn't find one that had most of what you wanted. >sigh<

This query includes a list price and a discount price, and a received date/time as well as a received date (the PO Line's status date). You can delete or comment out the ones you don't want using "--" at the beginning of the undesired line (two hyphens, no quotation marks).

Hope this helps,

JT

SELECT pols.POLineItemID
,po.PONumber
,br.BrowseTitle AS \[Title\]
,pol.ListPriceUnitBase AS ListPrice
,pol.DiscPriceUnitBase As DiscPrice
,fnd.Name AS Fund
,pol.StatusDate AS ReceivedDateTime
,FORMAT(pol.StatusDate, 'M/d/yyyy') AS ReceivedDate
FROM Polaris.Polaris.PolineItemSegments pols WITH (NOLOCK)
JOIN Polaris.Polaris.POLineItemSegmentAmts polsa (NOLOCK)
ON (polsa.POLineItemSegmentID = pols.POLineItemSegmentID)
JOIN Polaris.Polaris.Polines pol (NOLOCK)
ON (pols.POLineItemID = pol.POLineItemID)
JOIN Polaris.Polaris.PurchaseOrders po (NOLOCK)
ON (pols.PurchaseOrderID = po.PurchaseOrderID)
JOIN Polaris.Polaris.Funds fnd (NOLOCK)
ON (polsa.FundID = fnd.FundID)
JOIN Polaris.Polaris.BibliographicRecords br (NOLOCK)
ON (pol.BibliographicRecordID = br.BibliographicRecordID)
WHERE pols.Invoiced = 0
AND pols.StatusID = 13

answered 1 year ago  by <img width="18" height="18" src="../../_resources/d514dc5cb07b4e4aa16c1b47adfabdc0.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_965"></a>[Accept as answer](#)

FYI: This will not work in the Find Tool, but can be used as the basis of a custom report. JT

— jwhitfield@aclib.us 1 year ago

This worked perfectly! As always, thank you JT!!

— sbills@lpld.lib.in.us 1 year ago

<a id="commentAns_965"></a>[Add a Comment](#)

<a id="upVoteAns_964"></a>[](#)

0

<a id="downVoteAns_964"></a>[](#)

How can I adjust this SQL to get more output columns? I'm currently only getting column A (see attachment), and then I've been typing in the rest of the columns. Will the SQL server management studio give me all of this info so I don't have to look up each POLI ID and all the extra work? 

Thank you!

answered 1 year ago  by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg)  sbills@lpld.lib.in.us

- <a id="acceptAnswer_964"></a>[Accept as answer](#)

- [rcvd not invoiced.jpg](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/408?fileName=408-964_rcvd+not+invoiced.jpg "rcvd not invoiced.jpg")

<a id="commentAns_964"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for Item records from PO/FY/Branch](https://iii.rightanswers.com/portal/controller/view/discussion/326?)
- [I'm looking for a SQL report that will tell how long items are in held status.](https://iii.rightanswers.com/portal/controller/view/discussion/495?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [SQL for circulation numbers](https://iii.rightanswers.com/portal/controller/view/discussion/94?)
- [Adding number of "live" items to a report](https://iii.rightanswers.com/portal/controller/view/discussion/666?)

### Related Solutions

- [Credit an individual Invoice Line Item that has been paid for](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930299529481)
- [What is the limit on the number of PO lines?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930581649532)
- [POs with status of Partly Received, but all line items are Received](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930265371398)
- [How does an on order template match to a purchase order line item?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930395628685)
- [Default Price Overriding Price from 970 $p during Bulk Add to PO](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930605884624)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)