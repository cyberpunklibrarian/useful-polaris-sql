Point me to where I can find the Bibliographic Keyword rules table! I can't find it in Admin. - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Point me to where I can find the Bibliographic Keyword rules table! I can't find it in Admin.

0

71 views

It appears that the 586 tag isn't being searched in our catalog. I cannot find where in administration where you can even see your keyword rules (even though there is supposed to be one called: Bibliographic Keyword Rules).

Thanks.

asked 2 years ago  by <img width="18" height="18" src="../../_resources/0f24433f42104499b9ae8542a7ab8bf7.jpg"/>  sgrant@somd.lib.md.us

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_628"></a>[](#)

1

<a id="downVoteAns_628"></a>[](#)

Answer Accepted

This is the SQL I put together to read the table:

```
select bkr.TagNumber
 ,bkr.Subfield
 ,case when substring(BinaryFlags, 1, 1) = '1' then 'Yes' else 'No' end [Genre]
 ,case when substring(BinaryFlags, 2, 1) = '1' then 'Yes' else 'No' end [General Notes]
 ,case when substring(BinaryFlags, 3, 1) = '1' then 'Yes' else 'No' end [Publisher]
 ,case when substring(BinaryFlags, 4, 1) = '1' then 'Yes' else 'No' end [Series]
 ,case when substring(BinaryFlags, 5, 1) = '1' then 'Yes' else 'No' end [Subject]
 ,case when substring(BinaryFlags, 6, 1) = '1' then 'Yes' else 'No' end [Title]
 ,case when substring(BinaryFlags, 7, 1) = '1' then 'Yes' else 'No' end [Author]
from
( 
 select bkr.TagNumber
 ,bkr.Subfield
 ,bkr.TypeFlags
 ,right('0000000' + Polaris.dbo.DecimalToBinary(bkr.TypeFlags), 7) as BinaryFlags
 from polaris.polaris.BibliographicKeywordRules bkr 
) bkr
order by bkr.TagNumber, bkr.Subfield
```

answered 2 years ago  by <img width="18" height="18" src="../../_resources/0f24433f42104499b9ae8542a7ab8bf7.jpg"/>  mfields@clcohio.org

<a id="commentAns_628"></a>[Add a Comment](#)

<a id="upVoteAns_625"></a>[](#)

0

<a id="downVoteAns_625"></a>[](#)

I believe that you can only find it in SQL. Do you have SQL access to your Polaris instance?

answered 2 years ago  by <img width="18" height="18" src="../../_resources/0f24433f42104499b9ae8542a7ab8bf7.jpg"/>  wosborn@clcohio.org

- <a id="acceptAnswer_625"></a>[Accept as answer](#)

Darn it, I don't but the IT team does. I will discuss with them.

— sgrant@somd.lib.md.us 2 years ago

<a id="commentAns_625"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Problems with diacritics in bibliographic records from eContent](https://iii.rightanswers.com/portal/controller/view/discussion/29?)
- ["Tricks" for locating poor bibliographic records for merging? Cataloging cleanup question](https://iii.rightanswers.com/portal/controller/view/discussion/1005?)
- [Looking for another Polaris library that has added Spanish initial articles to the Initial Articles Table in SA.](https://iii.rightanswers.com/portal/controller/view/discussion/906?)
- [Finding new and replaced bibs in Polaris between certain dates](https://iii.rightanswers.com/portal/controller/view/discussion/871?)
- [For the libraries that pay for zmarc, what setting do you select for Perform Authority Control in the Bibliographic Record tab in your import profiles? And why?](https://iii.rightanswers.com/portal/controller/view/discussion/475?)

### Related Solutions

- [Database tables for Keyword and Browse indexing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930397363904)
- [Why can't I see the "Do Not Overlay" checkbox?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=171003145914463)
- [Error attempting to search an external authority database when saving bibliographic records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930667278253)
- [Deleting and Purging Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930748692772)
- [How to link multiple Authority Records to a Bibliographic Record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930318385316)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)