[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Information regarding library practices with retaining Patron ID

0

123 views

Hello,

&nbsp;

We are looking for some information, if possible. There is a process in Polaris that would strip the Patron ID from the transactions in the Transaction Database. We were under the impression that most libraries do not keep the patron ID information in the Transaction database. We are looking for the following information:

&nbsp;

1.  Does your library remove the Patron ID from the transactions?
2.  How long do you keep the information for, if you do remove the ID?

&nbsp;

Thank you,

Shannon Witte

ILS Co-Administrator – Librarian 2

Technology

The Ocean County Library

asked 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_0b78071a891c442bbecfac3871b86a65.jpg"/> switte@theoceancountylibrary.org

[patron id](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=16#t=commResults) [transactions](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=17#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 6 years ago

<a id="upVoteAns_362"></a>[](#)

0

<a id="downVoteAns_362"></a>[](#)

We don't purge the patronid from our transactions database.  As previously mentioned it can be suppressed from view outside the database (so even those using the client wouldn't be able to see it), and it is useful information for building reports and gathering statistics.  It all depends on the level of patron privacy you're aiming for.  There was a great presentation on this at IUG this year (Wes Osborn was a co-presenter).  If you can find the slides from that you'll get a great overview of the options for patron privacy.

answered 6 years ago by ![ggosselin@richlandlibrary.com](https://iii.rightanswers.com/portal/app/images/custom/avatar_ggosselin@richlandlibrary.com20170530111420.JPG) ggosselin@richlandlibrary.com

- <a id="acceptAnswer_362"></a>[Accept as answer](#)

<a id="commentAns_362"></a>[Add a Comment](#)

<a id="upVoteAns_359"></a>[](#)

0

<a id="downVoteAns_359"></a>[](#)

MAIN does not purge the PatronIDs from the transaction database.  We do not show it in the Item History, but the transaction DB is locked behind SQL access so this information, while useful for occasional queries, is not open for "public" perusal.

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_0b78071a891c442bbecfac3871b86a65.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_359"></a>[Accept as answer](#)

<a id="commentAns_359"></a>[Add a Comment](#)

<a id="upVoteAns_358"></a>[](#)

0

<a id="downVoteAns_358"></a>[](#)

Hi. I was going to post almost the same question Shannon asked. The functionality in Polaris that allows us to delete the patron ID from the transaction database is great. How long are libraries saving the data before purging the PatronID?

(I see that theree are 50 views for this question but no one is sharing their best practice.)

Many thanks.

Marilyn

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_0b78071a891c442bbecfac3871b86a65.jpg"/> mborg@gmilcs.org

- <a id="acceptAnswer_358"></a>[Accept as answer](#)

<a id="commentAns_358"></a>[Add a Comment](#)

<a id="upVoteAns_101"></a>[](#)

0

<a id="downVoteAns_101"></a>[](#)

There is a process built into Polaris since 4.??.  Search the polaris Help for "remove patron id from circ transactions".  This will explain the process and which transactions are affected.  I believe this is Branch or System-wide option.

Rex

answered 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_0b78071a891c442bbecfac3871b86a65.jpg"/> rhelwig@flls.org

- <a id="acceptAnswer_101"></a>[Accept as answer](#)

<a id="commentAns_101"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [In a consortium of 8 libraries, Is it possible to limit either a collection or material type by both the patron's home library and their hold pickup to branches within that library?](https://iii.rightanswers.com/portal/controller/view/discussion/262?)
- [PACREG Patrons - Virtual Patrons](https://iii.rightanswers.com/portal/controller/view/discussion/724?)
- [Does anyone have experience with EnvisionWare's 24-Hour Library?](https://iii.rightanswers.com/portal/controller/view/discussion/282?)
- [Does anyone have a SQL to find out hold information on integrated e-content?](https://iii.rightanswers.com/portal/controller/view/discussion/684?)
- [Our library is extending our loan periods. Advice? Checklist?](https://iii.rightanswers.com/portal/controller/view/discussion/590?)

### Related Solutions

- [Vega Documentation Help](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=221011141621840)
- [What Information Should I Use for Library of Congress?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930355289656)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [Retrieving deleted patron information](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930938135361)
- [Library contact information on eReceipt](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930457677951)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)