How to identify patrons with "none" in "Notice address" field - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# How to identify patrons with "none" in "Notice address" field

0

29 views

Hello, all,

I am trying to figure out how to identify patrons with "none" selected for the "Notice address" field.

It is probably right in front of me, but I cannot seem to see/find an option or where it is listed in the tables.

Thanks, much, as always, for any assistance you can provide.

Alison Hoffman

Monarch Library System

asked 1 month ago  by <img width="18" height="18" src="../../_resources/d208cac819284a8a808ba4a9fd536099.jpg"/>  ahoffman@monarchlibraries.org

[patron registration](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=238#t=commResults) [sql patron](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=387#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 month ago

<a id="upVoteAns_1148"></a>[](#)

0

<a id="downVoteAns_1148"></a>[](#)

Answer Accepted

This is definied in the PatronAddresses table.  You have to find patrons that 1) have an address and 2) don't have an address designated for notices. The SQL below works in the Find Tool.

SELECT DISTINCT(pa.PatronID)
FROM Polaris.Polaris.PatronAddresses AS \[pa\] WITH (NOLOCK)
WHERE pa.PatronID NOT IN (SELECT DISTINCT(pa.PatronID)
FROM Polaris.Polaris.PatronAddresses AS \[pa\] WITH (NOLOCK)
WHERE pa.AddressTypeID = '2')

answered 1 month ago  by <img width="18" height="18" src="../../_resources/d208cac819284a8a808ba4a9fd536099.jpg"/>  trevor.diamond@mainlib.org

Thank you for the help, Trevor! Very much appreciated. :-)

Alison

— ahoffman@monarchlibraries.org 1 month ago

<a id="commentAns_1148"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for changes to a patron record](https://iii.rightanswers.com/portal/controller/view/discussion/840?)
- [Last modifier of patron record](https://iii.rightanswers.com/portal/controller/view/discussion/1048?)
- [Simply reports patron services](https://iii.rightanswers.com/portal/controller/view/discussion/355?)
- [Circulation report by patrons age](https://iii.rightanswers.com/portal/controller/view/discussion/97?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)

### Related Solutions

- [New patrons identified as a duplicate / new patrons not able to register on the PAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930681961607)
- [Overdrive integration creating non-integrated items and no resource entity](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170503103227830)
- [Retrieving deleted patron information](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930938135361)
- [Configuring Vital to provide a URL in the dc:identifier tag of OAI Harvested Metadata](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170317045312407)
- [Expired Patron Blocks Not Appearing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930808100621)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)