SQL to find bibs with unprocessed 970 field - if other processed 970 fields also present? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=10)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Acquisitions](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=10)

# SQL to find bibs with unprocessed 970 field - if other processed 970 fields also present?

0

21 views

Hello,

Is there a report or SQL query available to locate bib records that have an unprocessed 970 field - when other, processed 970 fields are also present?

I was able to use the SQL below to find bibs if there is only a single 970 field, and that 970 field is unprocessed, but I am trying to find something that works when processed 970s are also present.

I only recently learned about the $9 utility - which is not currently enabled here - and that may be the only way to get the results I need - but I thought I would query this group in the meantime.

I work in a consortium, and three of the member libraries use the acquisitions module - sometimes there is an oops, and one of those libraries does not complete a PO process, leaving unprocessed 970 fields to be accidentally picked up by another acquisitions library that happens to order that same title at a later date. I was hoping to be able to proactively keep an eye out for that. (and if there is a Simply Reports or canned report option staring me in the face, oops, and I would appreciate the head's up on that as well :-)

In any case, thank you in advance for any suggestions or confirmation of what is doable.

Alison

Monarch Library System

SELECT distinct BR.BibliographicRecordID AS RecordID
FROM BibliographicRecords BR WITH (NOLOCK)
INNER JOIN Polaris.BibliographicTags BT WITH (NOLOCK)
    ON BT.BibliographicRecordID = BR.BibliographicRecordID
INNER JOIN Polaris.BibliographicSubfields BS WITH (NOLOCK)
    ON BS.BibliographicTagID = BT.BibliographicTagID
WHERE BR.RecordStatusID = 1
AND BT.TagNumber = 970
AND BS.Subfield = 'l'
AND BR.BibliographicRecordID NOT IN
(
SELECT DISTINCT(BR.BibliographicRecordID) AS recordID
FROM polaris.Bibliographicrecords BR
JOIN polaris.BibliographicTags BT (NOLOCK)
ON BT.BibliographicrecordID = BR.BibliographicrecordID
JOIN polaris.BibliographicSubfields BS (NOLOCK)
ON BS.BibliographictagID = BT.BibliographicTagID
WHERE TagNumber = 970
AND BS.Subfield LIKE '9')

asked 1 year ago  by <img width="18" height="18" src="../../_resources/a741bf02f2194c16abe9d4692fb6fab9.jpg"/>  ahoffman@monarchlibraries.org

[acquisitions](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=53#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_962"></a>[](#)

0

<a id="downVoteAns_962"></a>[](#)

Answer Accepted

Hi Alison.

Here is a tweak to your query that I think will work. (I needed to double-up the "Polaris." in the query to test it, but it is probably not necessary in the Find Tool.)

It uses an additional, left outer join to BibliographicSubfields with the added condition that the subfield be 9. In the where statement, it uses the IS NULL to find bib records that have at least one 970, but at least one 970 without a subfield 9.

I also changed the sub query to be IN. This combined with the main query should find bibs with a 970, subfield 9 and another 970 without a subfield 9. (My testing turned up a few bibs with inexplicably brief 970s that need to be deleted, so thanks for asking this question. ![laughing](../../_resources/af96b130806d4353a9de55062e1b2ecd.gif) )

Hope this helps,

JT

SELECT distinct BR.BibliographicRecordID AS RecordID
FROM Polaris.Polaris.BibliographicRecords BR WITH (NOLOCK)
INNER JOIN Polaris.Polaris.BibliographicTags BT WITH (NOLOCK)
    ON BT.BibliographicRecordID = BR.BibliographicRecordID
INNER JOIN Polaris.Polaris.BibliographicSubfields BS WITH (NOLOCK)
    ON BS.BibliographicTagID = BT.BibliographicTagID
LEFT OUTER JOIN Polaris.Polaris.BibliographicSubfields BS2 (NOLOCK)
 ON (BT.BibliographicTagID = BS2.BibliographicTagID and BS2.Subfield = '9')
WHERE BR.RecordStatusID = 1
AND BT.TagNumber = 970

AND BS2.BibliographicSubfieldID IS NULL
AND BR.BibliographicRecordID IN
(
SELECT DISTINCT(BR.BibliographicRecordID) AS recordID
FROM Polaris.Polaris.Bibliographicrecords BR
JOIN Polaris.Polaris.BibliographicTags BT (NOLOCK)
ON BT.BibliographicrecordID = BR.BibliographicrecordID
JOIN Polaris.Polaris.BibliographicSubfields BS (NOLOCK)
ON BS.BibliographictagID = BT.BibliographicTagID
WHERE BT.TagNumber = 970
AND BS.Subfield LIKE '9')

answered 1 year ago  by <img width="18" height="18" src="../../_resources/a741bf02f2194c16abe9d4692fb6fab9.jpg"/>  jwhitfield@aclib.us

One other thing, for the query to work in our Find Tool, I had to remove the line "AND BS.Subfield = '1'", so if you need to search for this specific subfield, you should be able to add it back to the query. JT

— jwhitfield@aclib.us 1 year ago

Thank you so much! This is just what I needed. I should have thought of adjusting the join - still learning as I go. :-)

— ahoffman@monarchlibraries.org 1 year ago

<a id="commentAns_962"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Can I invoice a POLI with no linked final bib record?](https://iii.rightanswers.com/portal/controller/view/discussion/412?)
- [Vendor Import Profiles](https://iii.rightanswers.com/portal/controller/view/discussion/188?)
- [Missing received PO](https://iii.rightanswers.com/portal/controller/view/discussion/212?)
- [Barcode Missing in Item Record](https://iii.rightanswers.com/portal/controller/view/discussion/216?)
- [Is there a way in the Acquistions module to receive and barcode multipart items or combo packages that will need to be split and attached to two sepaerate bibliographic records but paid and received as one item?](https://iii.rightanswers.com/portal/controller/view/discussion/175?)

### Related Solutions

- [EDI multiple prices for 970 field](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930521506399)
- [Default Price Overriding Price from 970 $p during Bulk Add to PO](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930605884624)
- [970 $f not being recognized when bulk adding to PO](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930524520097)
- [Information on POLI processing workslip doesn't look correct?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930940937326)
- [Does an Enriched EDI order include the item or the bibliographic record call number?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930445590381)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)