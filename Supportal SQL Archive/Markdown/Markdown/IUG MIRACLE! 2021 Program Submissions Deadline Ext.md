[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [General Announcements](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=55)

# IUG MIRACLE! 2021 Program Submissions Deadline Extended to 12/23

0

6 views

If you've been hoping for just a little extra time to submit your presentations for IUG2021 virtual conference, it's time to celebrate! The deadline has been extended to 12/23. Please use the following link to submit your proposal:  
<br/>https://www.xcdsystem.com/iug/abstract/index.cfm?ID=RzVkyNr  
<br/>Having trouble coming up with a program? Below are some ideas that attendees to IUG 2019 said they would like to see a program on in the future:

&nbsp;

- Practical sessions – on any part of the system! (cataloging, acquisition, circulation, Decision Center, etc.…)
- SQL – many members are still learning SQL – how are you using it in your library?
- Statistics – What statistics are available – how are you using them at your library?
- Create Lists! - there are a lot of us still trying to learn how to use create lists effectively
- "How my library does it" – are you doing something different – or unique at your library? Let everyone else know about it!
- Workflow – How are you using your ILS to help create workflow efficiencies
- Print templates!
- New and creative things at your library – have you gone fine free? How did you do it? Redesign your website? Implemented a new service?
- How-to sessions - there are always new attendees who are just learning to use the system, learning from someone who is already using the system is one of the best things about the IUG conference.

  
<br/>NEED HELP WITH YOUR PROGRAM IDEA?  
Have an idea, but not sure if you should submit it? Or would like to submit a program idea, but not sure what to present? Let us help you! Just contact Trevor Diamond or me, and we will be happy to answer all your questions!  
<br/>Thinking about reusing an old presentation?  That's great!  I can get a PDF copy of past submissions, if that will help you in re-entering for 2021. Just email me at trevor.diamond@mainlib.org and let me know 1)  your name and 2) the name of the presentation.  
<br/>Since this is a virtual conference, January-March will be spent recording presentations ahead of time.  Have no experience with recording online presentations?  No problem!  A member of the IUG 2021 Program Committee will be available to walk through the process with you to ensure success.  
<br/>Please contact us!  
Rhonda Glazier rglazier@uccs.edu  
Trevor Diamond   trevor.diamond@mainlib.org

asked 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_f6f499aa25d440f1883859cea22179a1.jpg"/> trevor.diamond@mainlib.org

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [IUG 2021 Program Submissions Due Soon!](https://iii.rightanswers.com/portal/controller/view/discussion/889?)
- [IUG 2021 Program Committee Wants You!](https://iii.rightanswers.com/portal/controller/view/discussion/842?)
- [Call for IUG2021 Program Submissions](https://iii.rightanswers.com/portal/controller/view/discussion/875?)
- [IUG 2022 Program Committee](https://iii.rightanswers.com/portal/controller/view/discussion/1062?)
- [DEADLINE TOMORROW: Polaris ILS IUG 2019 Scholarships](https://iii.rightanswers.com/portal/controller/view/discussion/383?)

### Related Solutions

- [Known Issues for Vega Products](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=230918105727553)
- [IUG 2021: ASAA: Advanced System Access and Administration in Sierra](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210413100359770)
- [IUG 2021: What Your Account Manager Can Do for You](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210413110252460)
- [IUG 2021 Quick Hit: Going Fines Free (Sierra)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210413102925993)
- [IUG 2021: Take the Leap in 2021](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210413105527760)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)