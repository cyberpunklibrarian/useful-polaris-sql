[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL for circ of items in record set, to include checkout info

0

100 views

I'm looking for a SQL that will give me circ stats on items in a record set, as well as checkout  and patron info. Specifically, I have a record set of items and, for those items, I need to know how many checkouts/renewals in a particular time period, and I also need to know the checkout branch and ZIP code of the patron who checked the item out. If I can't get the ZIP, I at least need the checkout location.

I found an SQL for circ stats for a record set, which was extremely helpful. Now I need to know where those checkouts occurred, as well as location information for the patrons who checked out/renewed the items.

Help??

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_e3b7421eb42741c49f07994ae3a779f0.jpg"/> musack@bcls.lib.nj.us

[circulation stats](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=412#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 2 years ago

<a id="upVoteAns_1204"></a>[](#)

0

<a id="downVoteAns_1204"></a>[](#)

Daniel,

That was EXACTLY what I needed ... thanks!!

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_e3b7421eb42741c49f07994ae3a779f0.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_1204"></a>[Accept as answer](#)

<a id="commentAns_1204"></a>[Add a Comment](#)

<a id="upVoteAns_1202"></a>[](#)

0

<a id="downVoteAns_1202"></a>[](#)

Daniel,

Thank you so much! I will give it a shot. I really appreciate your help :)

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_e3b7421eb42741c49f07994ae3a779f0.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_1202"></a>[Accept as answer](#)

<a id="commentAns_1202"></a>[Add a Comment](#)

<a id="upVoteAns_1201"></a>[](#)

0

<a id="downVoteAns_1201"></a>[](#)

Hi,

This query lists checkouts of items from one specified item record set as rows of Item ID, Bib Title, Checkout Date, Checkout Branch, and whichever Patron Zip Code is stored with the transaction. I don't know off-hand how Polaris decides which zip code to store if a patron has more than one listed address.

The item record set ID and the target dates are all included as specific values in the query that you can change to match your needs, but the query uses the transaction tables so it is *not fast* even for just one month timeframes. Larger date ranges might be quite slow or even time out.

SELECT  
   ir.ItemRecordID AS ItemID  
   , br.BrowseTitle AS Title  
   , th.TransactionDate AS CheckOutDate  
   , org.Name AS CheckoutBranch  
   , pc.PostalCode  
FROM  
   Polaris.Polaris.BibliographicRecords AS br  
   INNER JOIN Polaris.Polaris.ItemRecords AS ir  
      ON ir.AssociatedBibRecordID = br.BibliographicRecordID  
   INNER JOIN Polaris.Polaris.ItemRecordSets AS irs  
      ON irs.ItemRecordID = ir.ItemRecordID  
   INNER JOIN PolarisTransactions.Polaris.TransactionDetails AS td38  
      ON td38.numValue = irs.ItemRecordID  
   INNER JOIN PolarisTransactions.Polaris.TransactionDetails AS td302  
      ON td302.TransactionID = td38.TransactionID  
   INNER JOIN PolarisTransactions.Polaris.TransactionHeaders AS th  
      ON th.TransactionID = td38.TransactionID  
   INNER JOIN Polaris.Polaris.PostalCodes AS pc  
      ON pc.PostalCodeID = td302.numValue  
   INNER JOIN Polaris.Polaris.Organizations AS org  
      ON org.OrganizationID = th.OrganizationID  
WHERE  
   irs.RecordSetID = 103525 /\* <== Change to ID number of target record set \*/  
   AND th.TransactionDate >= '12/1/2021' /\* <== Change to first day of target date range \*/  
   AND th.TransactionDate < '1/1/2022' /\* <== Change to day after last day of target date range \*/  
   AND th.TransactionTypeID = 6001 /\* Checkouts \*/  
   AND td38.TransactionSubTypeID = 38 /\* ItemRecordID \*/  
   AND td302.TransactionSubTypeID = 302 /\* Patron PostalCodeID \*/

I hope that helps you get the data you need. Please do let me know if you have any questions about the query or its results.

&nbsp;    -Daniel

Edited to show branch names instead of ID numbers.

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_e3b7421eb42741c49f07994ae3a779f0.jpg"/> ddunphy@aclib.us

- <a id="acceptAnswer_1201"></a>[Accept as answer](#)

<a id="commentAns_1201"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL Query for how many first time borrowers are checking out holds?](https://iii.rightanswers.com/portal/controller/view/discussion/504?)
- [I need a SQL for phatom lost item count attached to a patron's record.](https://iii.rightanswers.com/portal/controller/view/discussion/839?)
- [Checking in holds for specific branches](https://iii.rightanswers.com/portal/controller/view/discussion/1121?)
- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)
- [We are rolling out hotspots for our patrons. We have created the records for the Hotspots. How can we restrict checkouts... we want no renewals... and you have to wait 48 hours prior to checking out the device again. Does anyone have any advice ?](https://iii.rightanswers.com/portal/controller/view/discussion/596?)

### Related Solutions

- [Change text on checkout receipts](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930905424507)
- [Billed items be set to Lost and Lost Item Recovery table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930478370792)
- [Do the home and owning branch need to match for floating item records?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930466429189)
- [Items didn't float upon check-in at receiving branch](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930277940804)
- [ExpressCheck: Held items can be checked out by the wrong patrons](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930574330183)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)