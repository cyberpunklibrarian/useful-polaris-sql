Report for patrons with items out. If possible, sort by location?   - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Report for patrons with items out. If possible, sort by location?

0

26 views

Hello, 

Looking for a SQL search to find patrons with items checked out.  If possible sort by location. ( How do I find the Organization ID? ) 

Or is there another report to run.  other than an overdue report. 

Big Thank You in Advance!!

Treel

treel@mypccl.org

asked 1 year ago  by <img width="18" height="18" src="../../_resources/849575e5c365443d8d87c36a35e1765d.jpg"/>  treel@mypccl.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_944"></a>[](#)

0

<a id="downVoteAns_944"></a>[](#)

Hopefully you are running this from the SQL Server Management Console.

select o.Abbreviation, ic.OrganizationID, ic.PatronID, Count(ic.PatronID) AS "ItemsOut"
from ItemCheckouts ic
join Organizations o
on o.OrganizationID = ic.OrganizationID
group by o.Abbreviation, ic.OrganizationID, ic.PatronID
order by ic.OrganizationID, ic.PatronID

Rex Helwig
Finger Lakes Library System

answered 1 year ago  by <img width="18" height="18" src="../../_resources/849575e5c365443d8d87c36a35e1765d.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_944"></a>[Accept as answer](#)

<a id="commentAns_944"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Reports for Floating Items](https://iii.rightanswers.com/portal/controller/view/discussion/62?)
- [Does anyone have a damaged item template report you are willing to share?](https://iii.rightanswers.com/portal/controller/view/discussion/956?)
- [Floating Reports](https://iii.rightanswers.com/portal/controller/view/discussion/363?)
- [How do you run your weeding reports?](https://iii.rightanswers.com/portal/controller/view/discussion/464?)
- [I need a report of the total number of items checked out by user ID where the total number out is greater than 35](https://iii.rightanswers.com/portal/controller/view/discussion/961?)

### Related Solutions

- [Where to find Patron Circ Count in Patron record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930283871188)
- [Sending Collection Agency Reports Manually](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930959368316)
- [Long Overdue Item report shows thousands of items](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930725698901)
- [Reports for On-the-Fly (OTF) items](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930655195362)
- [Statistics for Holds and ILLs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930628713826)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)