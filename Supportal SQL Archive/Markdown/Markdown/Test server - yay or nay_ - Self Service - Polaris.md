[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Test server - yay or nay?

0

85 views

RIP - our test server. Should we spend the money to set up a new virtual server or not have a test server at all? Opinions please! Thank you!

asked 2 years ago by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg) sbills@lpld.lib.in.us

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_1236"></a>[](#)

1

<a id="downVoteAns_1236"></a>[](#)

Answer Accepted

We use our test server every day. As a matter of data protection and accuracy, all SQL scripts and and test transactions/settings are developed, transacted, set in the test environment prior to ever touching production data. Industry standard is to use a test/staging environment that closely reflect that of your production environment.

  
Even "SELECT" SQL queries can impact system performance if they are bad queries and those rare occurrences should not be felt by our staff or public.

You could ask yourself "if you are not writing scripts or testing settings then why pay for a test environment?" and my reply would be "The cost for a test environment is the same as paying for insurance. You may not need it but when you do, it could save your career and public perception."

&nbsp;

Thanks for reading, best of luck!

&nbsp;

answered 2 years ago by ![eric.young@phoenix.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_eric.young@phoenix.gov20191029113427.jpg) eric.young@phoenix.gov

Agreed.

— trevor.diamond@mainlib.org 2 years ago

Thank you, both! I want to keep it, but need all the ammo I can get to justify cost. ;)

— sbills@lpld.lib.in.us 2 years ago

The biggest warning about system administration is that you will spend most of your days deep in the internals of live production systems. If you make a mistake, it could directly affect your library and it could have a direct impact on people you serve. The trite advice is “don’t mess up,” but this misses the great importance of the learning stage of your career. If you don’t mess up, you’ll never learn. The trick is finding a way to mess up without causing huge problems for your library or your career. This is where your test/staging environment becomes the solution to your problems and your core learning tool.

  
The more you rely on using the test/staging environment the more ammo you have to keep it. Custom report and SQL script dev, ILS settings and testing, as well as troubleshooting odd cases are the highlights of what the test/staging environment can solve. The details of the ammo needed are in the guts of your use of that resource. Hope this helps. Best of luck!

— eric.young@phoenix.gov 2 years ago

I will simply add one more "agreed" to all of the comments above. I use the test server almost every day and have utilized it (and been so thankful for it!) for everything mentioned above.

— ahoffman@monarchlibraries.org 2 years ago

<a id="commentAns_1236"></a>[Add a Comment](#)

<a id="upVoteAns_1249"></a>[](#)

1

<a id="downVoteAns_1249"></a>[](#)

It's great to test changes that you want to make, test upgrades, and use for training without worrying. I am learning Sys Admin so get on there a lot since I can't mess anything up. We had librarians from each branch do testing on upgrades using the test server. We train on the test server, "You can't mess anything up!"  
<br/>Very useful!

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_c62cca3fdb4d4d9093d92ae37ef4a2cb.jpg"/> sgrant@somd.lib.md.us

- <a id="acceptAnswer_1249"></a>[Accept as answer](#)

<a id="commentAns_1249"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Setting up a DNS ALIAS for Testing a server migration](https://iii.rightanswers.com/portal/controller/view/discussion/690?)
- [Consortium Circulation parameter in SA not working how I expect in test server](https://iii.rightanswers.com/portal/controller/view/discussion/709?)
- [Server Busy](https://iii.rightanswers.com/portal/controller/view/discussion/278?)
- [Query Syntax for a Linked Server](https://iii.rightanswers.com/portal/controller/view/discussion/491?)
- [Experiences with Windows Firewall on Polaris Server](https://iii.rightanswers.com/portal/controller/view/discussion/446?)

### Related Solutions

- [SSL certificate for Leap installation on a training server](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930259684761)
- [Where can I find the SIP test tool?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930326801962)
- [Unable to log off of a terminal server running on Microsoft Server 2012](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930958047582)
- [Can I test MobilePAC on Production before going "live" with it?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930568641803)
- [Polaris Telephony Server Specs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=230214140944563)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)