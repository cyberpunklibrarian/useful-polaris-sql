SQL for Patrons owing $50+ all in overdue fines - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for Patrons owing $50+ all in overdue fines

0

191 views

Hi all,

Does anyone have a query that will give me patrons who owe $50 or more but all from overdue fines, no replacement costs?

Thanks!

asked 3 years ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

<a id="comment"></a>[Add a Comment](#)

## 6 Replies   last reply 2 years ago

<a id="upVoteAns_178"></a>[](#)

0

<a id="downVoteAns_178"></a>[](#)

Answer Accepted

Thanks again John!  It works great.

\- Robin

answered 3 years ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

<a id="commentAns_178"></a>[Add a Comment](#)

<a id="upVoteAns_180"></a>[](#)

1

<a id="downVoteAns_180"></a>[](#)

I misunderstood the question here--I thought the request was for patrons who owe at least $50 in overdue fines, regardless of whatever other charges they have.  Robin was looking for a list of patrons who owe at least $50 in overdue fines and do not have charges for any other reason.
The SQL for that should be:

select ac.PatronID
from Polaris.PatronAccount ac with (nolock)
WHERE ac.TxnID in (
select distinct TxnID from Polaris.PatronAccount with (nolock)
where TxnCodeID in (1)
AND FeeReasonCodeID = 0
and OutstandingAmount > 0
)
AND ac.PatronID NOT in (
select distinct PatronID from Polaris.PatronAccount with (nolock)
where TxnCodeID in (1)
AND FeeReasonCodeID <> 0
and OutstandingAmount > 0
)
GROUP BY ac.PatronID
HAVING SUM(ac.OutstandingAmount) >= 50

answered 3 years ago  by <img width="18" height="18" src="../../_resources/5abe5ac681e245b98103f0eb69a461c9.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_180"></a>[Accept as answer](#)

<a id="commentAns_180"></a>[Add a Comment](#)

<a id="upVoteAns_605"></a>[](#)

0

<a id="downVoteAns_605"></a>[](#)

In theory this should work, but I can't verify it because we don't charge overdue fines.

select ac.PatronID, pat.Barcode 

from Polaris.PatronAccount ac with (nolock)
JOIN Patrons pat with (NOLOCK)
ON ac.PatronID = pat.PatronID
WHERE ac.TxnID in (
select distinct TxnID from Polaris.PatronAccount with (nolock)
where TxnCodeID in (1)
AND FeeReasonCodeID = 0
and OutstandingAmount > 0
)
AND ac.PatronID NOT in (
select distinct PatronID from Polaris.PatronAccount with (nolock)
where TxnCodeID in (1)
AND FeeReasonCodeID <> 0
and OutstandingAmount > 0
)
GROUP BY ac.PatronID, pat.Barcode
HAVING SUM(ac.OutstandingAmount) >= 50

answered 2 years ago  by <img width="18" height="18" src="../../_resources/5abe5ac681e245b98103f0eb69a461c9.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_605"></a>[Accept as answer](#)

I keep getting invalid object name "Patrons"

— lori@esrl.org 2 years ago

Hm.  You might try changing the third line to "*JOIN Polaris.Patrons pat with (NOLOCK)*" (adding the "Polaris." to the line).

Our system handles the join just fine, but it's possible that some systems require you to be more specific about where the table is.

— jjack@aclib.us 2 years ago

LOL. I had to add it twice.  Polaris.Polaris.PatronAccount.  Anyway, it worked.  Thank you so much!

— lori@esrl.org 2 years ago

<a id="commentAns_605"></a>[Add a Comment](#)

<a id="upVoteAns_603"></a>[](#)

0

<a id="downVoteAns_603"></a>[](#)

How can I add barcode to the output.  When I execute the query all I get is Patron ID.  I need the barcode as well. 

Thanks.

Lori

answered 2 years ago  by <img width="18" height="18" src="../../_resources/5abe5ac681e245b98103f0eb69a461c9.jpg"/>  lori@esrl.org

- <a id="acceptAnswer_603"></a>[Accept as answer](#)

<a id="commentAns_603"></a>[Add a Comment](#)

<a id="upVoteAns_179"></a>[](#)

0

<a id="downVoteAns_179"></a>[](#)

Glad to hear it!

answered 3 years ago  by <img width="18" height="18" src="../../_resources/5abe5ac681e245b98103f0eb69a461c9.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_179"></a>[Accept as answer](#)

<a id="commentAns_179"></a>[Add a Comment](#)

<a id="upVoteAns_177"></a>[](#)

0

<a id="downVoteAns_177"></a>[](#)

This should work, assuming the FeeReasonCodeIDs are set by Polaris and not individual library systems:

select ac.PatronID
from Polaris.PatronAccount ac with (nolock)
WHERE ac.TxnID in (
select distinct TxnID from Polaris.PatronAccount with (nolock)
where TxnCodeID in (1)
AND FeeReasonCodeID = 0
and OutstandingAmount > 0
)
GROUP BY ac.PatronID
HAVING SUM(ac.OutstandingAmount) >= 50

answered 3 years ago  by <img width="18" height="18" src="../../_resources/5abe5ac681e245b98103f0eb69a461c9.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_177"></a>[Accept as answer](#)

<a id="commentAns_177"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for Overdue fines](https://iii.rightanswers.com/portal/controller/view/discussion/543?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [Anyone have a sql script that will total fines for a year and then break them down by patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/688?)
- [SQL for changes to a patron record](https://iii.rightanswers.com/portal/controller/view/discussion/840?)
- [SQL - looking for a report on how many fines we have for a specific collection code and how many were waived.](https://iii.rightanswers.com/portal/controller/view/discussion/673?)

### Related Solutions

- [Why do fine notices say patrons are going to collections if they owe $50?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930377747307)
- [Maximum Overdue Fine Balance vs Fines Policy Table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=171116095355303)
- [Patron / Material Type Loan Limit Blocks policy table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930651711850)
- [Polaris SIP not returning detailed fine information](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930827108312)
- [Fee reason display order in Leap](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930612692592)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)