SQL query comparing separate transactions - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL query comparing separate transactions

0

48 views

Hi, all. I'm trying to come up with a SQL statement that essentially needs separate instances of the various transactions tables, and I can't seem to figure it out with my limited SQL skills. A little background :

Our director noticed that our recent circulation numbers have been almost as high as they were at this time last year, despite the fact that library usage has been down because of the pandemic. We've been quarantining returned items for a few days before checking them in, and he wondered if maybe some of the items in quarantine were auto-renewing between the time they were returned and the time they were checked in. No doubt this is happening, but I'm doubtful that it would contribute a huge amount to our circ stats. I wanted to see if I could get an actual number, though.

So, it seems to me that what I need to do is get a report of all the items that were checked in during the month of October which were also auto-renewed within four days before the checkin date (four days is our quarantine period). From my understanding of the database structure, this is going to involve the TransactionHeaders table (which will indicate the date of the transaction, and whether it was a checkin or a checkout) and the TransactionDetails table (which will specify whether a checkout is an auto-renewal and indicate the item involved). It's not difficult to get a list of all items that were checked in during a given timespan. It's also not difficult to get a list of all itmes that were auto-renewed during a given timespan. Where I'm running into a problem is managing to get both of those out of a single query which will also check whether the auto-renewal date for a given item is within four days before its checkin date.

Here's what I have :

SELECT th.TranClientDate as AutoRenewDate, th2.TranClientDate as CheckInDate, td3.numValue as ItemNumber
FROM PolarisTransactions.Polaris.TransactionHeaders th (nolock), PolarisTransactions.Polaris.TransactionHeaders th2 (nolock) on (th.TransactionID = th2.TransactionID)
INNER JOIN PolarisTransactions.Polaris.TransactionDetails td (nolock) on (th.TransactionID = td.TransactionID)
INNER JOIN PolarisTransactions.Polaris.TransactionDetails td2 (nolock) on (th2.TransactionID = td2.TransactionID)
INNER JOIN PolarisTransactions.Polaris.TransactionDetails td3 (nolock) on (th.TransactionID = td3.TransactionID)
INNER JOIN PolarisTransactions.Polaris.TransactionDetails td4 (nolock) on (th2.TransactionID = td4.TransactionID)
WHERE th.TransactionTypeID = 6001
AND th2.TransactionTypeID = 6002
AND td.TransactionSubTypeID = 145
AND td.numValue = 48
AND th2.TranClientDate > '10/01/2020'
AND th2.TranClientDate < '10/31/2020'
AND datediff(d, th.TranClientDate, th2.TranClientDate) < 5
AND td3.TransactionSubTypeID = 38
AND td4.TransactionSubTypeID = 38
AND td3.numValue = td4.numValue
AND th.OrganizationID = 19
ORDER BY AutoRenewDate

Unfortunately, it doesn't run, and I'm pretty sure it's because of the two instances of the TransactionHeaders table that aren't explicitly JOINed. The problem is that I feel like, for what I'm trying to do, they can't be joined, because I need to find separate transactions that won't have any common values in that table. If I do try to join them, the query will run, but it gives no results. Maybe I have something wrong in the WHERE statement, but I can't chake the feeling that the problem is arising earlier than that.

Sorry for the wall of text.  Does anyone have any ideas on how to fix this? I hope my explanation of what I'm looking for made any sense at all.

Thanks in advance.

-Tim Nix
Webster Groves Public Library

asked 10 months ago  by <img width="18" height="18" src="../../_resources/2dd50b9fe25a48e0a0aafde7b9df4ca6.jpg"/>  tnix@wgpl.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 10 months ago

<a id="upVoteAns_1027"></a>[](#)

0

<a id="downVoteAns_1027"></a>[](#)

Answer Accepted

Thanks to anyone who took a look at this, but I ended up figuring it out on my own.  Turns out, you can put whole queries into your FROM clause.  Here's what I ended up using, if anyone's curious.

SELECT CheckIns.ItemNumber, AutoRenews.AutoRenewDate, CheckIns.CheckInDate
FROM (
SELECT th.TranClientDate as AutoRenewDate, td2.numValue as ItemNumber
FROM PolarisTransactions.Polaris.TransactionHeaders th (nolock)
INNER JOIN PolarisTransactions.Polaris.TransactionDetails td (nolock) on (th.TransactionID = td.TransactionID)
INNER JOIN PolarisTransactions.Polaris.TransactionDetails td2 (nolock) on (th.TransactionID = td2.TransactionID)
WHERE th.TransactionTypeID = 6001
AND td.TransactionSubTypeID = 145
AND td.numValue = 48
AND td2.TransactionSubTypeID = 38
AND th.TranClientDate > '09/27/2020'
AND th.TranClientDate < '10/31/2020'
AND th.OrganizationID = 19 ) as AutoRenews
INNER JOIN (
SELECT th.TranClientDate as CheckInDate, td.numValue as ItemNumber
FROM PolarisTransactions.Polaris.TransactionHeaders th (nolock)
INNER JOIN PolarisTransactions.Polaris.TransactionDetails td (nolock) on (th.TransactionID = td.TransactionID)
WHERE th.TransactionTypeID = 6002
AND td.TransactionSubTypeID = 38
AND th.TranClientDate > '10/01/2020'
AND th.TranClientDate < '10/31/2020'
AND th.OrganizationID = 19 ) as CheckIns on (AutoRenews.ItemNumber = CheckIns.ItemNumber)
WHERE datediff(d, AutoRenews.AutoRenewDate, CheckIns.CheckInDate) >= 0
AND datediff(d, AutoRenews.AutoRenewDate, CheckIns.CheckInDate) < 5

answered 10 months ago  by <img width="18" height="18" src="../../_resources/2dd50b9fe25a48e0a0aafde7b9df4ca6.jpg"/>  tnix@wgpl.org

<a id="commentAns_1027"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [Is there still a SQL repository for the Polaris forum?](https://iii.rightanswers.com/portal/controller/view/discussion/706?)
- [SQL query to export specific record set: Polaris 4.1R2](https://iii.rightanswers.com/portal/controller/view/discussion/93?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [Need help with query to find checked out titles with holds](https://iii.rightanswers.com/portal/controller/view/discussion/1063?)

### Related Solutions

- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)
- [Export from old Supportal of forum posts with SQL queries](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161214213310321)
- [How to create a read only user for staff to use to query SQL Server Management Studio](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930482184973)
- [How was the data gathered in SimplyReports?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930857021045)
- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)