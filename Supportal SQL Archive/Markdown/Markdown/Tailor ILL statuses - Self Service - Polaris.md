[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Tailor ILL statuses

0

66 views

Patrons are confused when they see the ILL Status of Received in their accounts, and assume the item has been received at their pickup branch and is ready for them.  Is there any way to change that status to something like "Processing" or "Received - Processing"?   
<br/>

asked 1 year ago by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG) rdye@krl.org

[ill requests](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=501#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 1 year ago

<a id="upVoteAns_1283"></a>[](#)

0

<a id="downVoteAns_1283"></a>[](#)

Answer Accepted

The ILL Statuses table is not in Polaris Admin, is it?  Just in SQL?  

answered 1 year ago by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG) rdye@krl.org

I believe the only way to update it is via the SQL table.  I didn't see it in any of the Admin Settings.

Rex Helwig  
Finger Lakes Library System

— rhelwig@flls.org 1 year ago

<a id="commentAns_1283"></a>[Add a Comment](#)

<a id="upVoteAns_1284"></a>[](#)

0

<a id="downVoteAns_1284"></a>[](#)

Thanks Rex!  I'll definitely as my site manager before tweaking a table.

answered 1 year ago by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG) rdye@krl.org

- <a id="acceptAnswer_1284"></a>[Accept as answer](#)

<a id="commentAns_1284"></a>[Add a Comment](#)

<a id="upVoteAns_1282"></a>[](#)

0

<a id="downVoteAns_1282"></a>[](#)

I would think you could change the "description" of the 5 different "Received" statuses in the Polaris.ILLStatuses table.  I would check with your Site Manager first to be sure it wouldn't cause any unintended problems.

<ins>ILLStatusID   Description   InternalDescription</ins>  
1                  Inactive        Inactive  
3                  Active           Active  
10                Received       Recd-Held  
11                Received       Recd-Transferred  
12                Received       Recd-Satisfied  
13                Received       Recd-Used  
14                Received       Recd-Unused  
15                Returned       Returned  
16                Cancelled      Cancelled  
5                  Shipped        Shipped

Rex Helwig  
Finger Lakes Library System

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_046d2443f4b64993ae158d33b49704a6.jpg"/> rhelwig@flls.org

- <a id="acceptAnswer_1282"></a>[Accept as answer](#)

<a id="commentAns_1282"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [ILL in Polaris](https://iii.rightanswers.com/portal/controller/view/discussion/64?)
- [Polaris ILL, INN-Reach and Illiad](https://iii.rightanswers.com/portal/controller/view/discussion/691?)
- [Where can I find information about the ILL module?](https://iii.rightanswers.com/portal/controller/view/discussion/583?)
- [Is it possible to bundle items for shelf status and checkout?](https://iii.rightanswers.com/portal/controller/view/discussion/822?)
- [Meescan - Self-Checkout from Mobile Device](https://iii.rightanswers.com/portal/controller/view/discussion/1083?)

### Related Solutions

- [INN-Reach: Items that are transferred back from the branches to Main are not auto-returning when checked in](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930314735486)
- [Polaris not recognizing ILL item barcode](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930615916421)
- [How to use the Polaris ILL Module without OCLC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930581900307)
- [What colors are hold and ILL statuses in the PAC?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170330195105351)
- [Collection column in Request Manager](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930880565629)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)