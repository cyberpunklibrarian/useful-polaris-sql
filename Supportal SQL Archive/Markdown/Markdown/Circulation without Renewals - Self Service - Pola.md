Circulation without Renewals - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Circulation without Renewals

0

157 views

Originally posted by andrew.wright1@dallascityhall.com on Friday, January 6th 10:19:47 EST 2017
I am looking to find the top circulating titles by shelf location and everything I have tried is including renewals giving false results. Does anyone know a way to calculate the top circulating titles excluding renewals? I only have SQL access in the client. Thanks in advance Andrew Wright

asked 4 years ago  by <img width="18" height="18" src="../../_resources/4ca7bcedb19240e98b79eaf5d9d71454.jpg"/>  support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 4 years ago

<a id="upVoteAns_66"></a>[](#)

0

<a id="downVoteAns_66"></a>[](#)

Exclude renewals

Response by cgabehart@sno-isle.org on January 18th, 2017 at 11:10 am

The only way I know of to exclude renewals is to use a statement like this in your "where" clause:
and th.TransactionID not in ( Select h.transactionid
from PolarisTransactions.Polaris.TransactionHeaders h (nolock)
INNER JOIN PolarisTransactions.Polaris.TransactionDetails d (nolock)
on d.TransactionID = h.TransactionID and h.TransactionTypeID = 6001 and d.TransactionSubTypeID = 124 -- Renewal
where h.TranClientDate between '2016-01-01' and '2016-12-31 23:59:59')
I hope this helps!

answered 4 years ago  by <img width="18" height="18" src="../../_resources/4ca7bcedb19240e98b79eaf5d9d71454.jpg"/>  support1@iii.com

- <a id="acceptAnswer_66"></a>[Accept as answer](#)

<a id="commentAns_66"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Renewals reports](https://iii.rightanswers.com/portal/controller/view/discussion/83?)
- [SQL for circulation numbers](https://iii.rightanswers.com/portal/controller/view/discussion/94?)
- [Shelf location circulation statistics](https://iii.rightanswers.com/portal/controller/view/discussion/441?)
- [Simply Reports Discrepancies](https://iii.rightanswers.com/portal/controller/view/discussion/960?)
- [Circulation count of deleted items from a collection](https://iii.rightanswers.com/portal/controller/view/discussion/92?)

### Related Solutions

- [Items not auto-renewed for specific patron](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930760925871)
- [Changing renewal limits system wide](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930343101412)
- [Auto-Renew basics and FAQ](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930279224783)
- [IUG 2018: Automatic Renewals in Sierra - You Gotta Keep ‘Em Circulating](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180517121741599)
- [Renewal numbers differ in PAC from client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930678864373)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)