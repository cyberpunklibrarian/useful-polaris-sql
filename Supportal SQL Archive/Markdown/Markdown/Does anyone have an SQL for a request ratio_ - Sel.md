Does anyone have an SQL for a request ratio? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Does anyone have an SQL for a request ratio?

0

116 views

We are in search of an SQL for a request (holds) ratio.  Ideally it would give us the number of requests, number of copies system wide, the ratio of requests to copies, the bib control number, collection and title.  Any help would be appreciated.

Thanks,

Anne Krulik

Burlington County Library System

Westampton, New Jersey

asked 2 years ago  by <img width="18" height="18" src="../../_resources/ba5af9207cc2451bb4c189a7923388a9.jpg"/>  akrulik@bcls.lib.nj.us

[holds](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=96#t=commResults) [ratio](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=184#t=commResults) [requests](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=181#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 7 Replies   last reply 2 years ago

<a id="upVoteAns_448"></a>[](#)

1

<a id="downVoteAns_448"></a>[](#)

Thank you JT and Katherine for all the insight.  I appreciate the tip for "Not Grouped by Type of Material".

answered 2 years ago  by <img width="18" height="18" src="../../_resources/ba5af9207cc2451bb4c189a7923388a9.jpg"/>  akrulik@bcls.lib.nj.us

- <a id="acceptAnswer_448"></a>[Accept as answer](#)

<a id="commentAns_448"></a>[Add a Comment](#)

<a id="upVoteAns_447"></a>[](#)

0

<a id="downVoteAns_447"></a>[](#)

Maybe you've already looked at this, but the canned report "Holds Purchase Alert" will give you most of this data.  It does ask for a ratio to start (we look at anything with more that 5 holds to 1 copy), but you can enter 1 to 1 in the field and it will show you everything with holds.  I recommend running it as "Not Grouped by Type of Material" and it will sort from items with most to least holds.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/ba5af9207cc2451bb4c189a7923388a9.jpg"/>  kmanion@urbandale.org

- <a id="acceptAnswer_447"></a>[Accept as answer](#)

<a id="commentAns_447"></a>[Add a Comment](#)

<a id="upVoteAns_446"></a>[](#)

0

<a id="downVoteAns_446"></a>[](#)

Hi Anne.

Try this query. I think it will include what you need. The trickiest part of this is handling situations where one bib record has items with different collections. This query lists the most common collection. I think that this query will give a NULL value if there are an equal number of items assigned to two collections. Hopefully, this will be rare.

The query also includes bibs that have no good items but have a hold. The ratio of Holds per Item is actually the number of holds in these cases.

Hope this helps,

JT

---Begin Query Here ---

BEGIN
 SET NOCOUNT ON
 
 DECLARE @t TABLE
 (
  BibliographicRecordID int NOT NULL,
  CALLNO nvarchar(50) NULL,
  Author nvarchar(255) NULL,
  Title nvarchar(255) NULL,
  HoldCount int NULL,
  ItemCount int NULL
 )

 DECLARE @t2 TABLE
 (AssociatedBibRecordID int NOT NULL,
 AssignedCollectionID int NULL
 )

  DECLARE @nNumberOfItems int = 1
  DECLARE @nNumberOfHolds int = 4

 --gets only locked requests
 INSERT INTO @t(BibliographicRecordID, HoldCount)
  SELECT distinct BibliographicRecordID, COUNT(SHR.SysHoldRequestID) as holds
 FROM Polaris.Polaris.SysHoldRequests SHR WITH (NOLOCK)
 WHERE SysHoldStatusID IN (1,3) AND BibliographicRecordID <> 0
 and BibliographicRecordID in
 (SELECT distinct it.AssociatedBibRecordID as Bibliographicrecordid from polaris.polaris.ItemRecords it with (nolock)
 )
 GROUP BY BibliographicRecordID

 \-\- only include final item records that are not withdrawn \[claim ret, lost, missing\]
 \-\- \[modified to count only holdable items\]
 UPDATE @t
 SET ItemCount =
  (SELECT COUNT(CIR.AssociatedBibRecordID) AS ItemCount
   FROM Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)
   WHERE CIR.AssociatedBibRecordID = t.BibliographicRecordID AND
    CIR.ItemStatusID NOT IN (7,8,9,10,11) AND CIR.RecordStatusID = 1
    and CIR.Holdable= 1)
FROM @t t, Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)

 UPDATE @t
 SET CALLNO = BR.BrowseCallNo,
  Author = BR.BrowseAuthor,
  Title = BR.BrowseTitle
  FROM @t t
 INNER JOIN Polaris.Polaris.BibliographicRecords BR WITH (NOLOCK)
  ON t.BibliographicRecordID = BR.BibliographicRecordID

 IF (@nNumberOfItems > 0)
 BEGIN
  declare @fRatio float
  select @fRatio = @nNumberOfHolds / (@nNumberOfItems * 1.0)

  DELETE FROM @t
   WHERE ItemCount <> 0 
   AND HoldCount/(ItemCount * 1.0) < @fRatio
 END
 ELSE
 BEGIN
  DELETE FROM @t
   WHERE ItemCount <> 0 
   AND HoldCount/ItemCount < @nNumberOfHolds
 END

 INSERT INTO @t2 (AssociatedBibRecordID, AssignedCollectionID)
select AssociatedBibRecordID, AssignedCollectionID
from (select AssociatedBibRecordID, AssignedCollectionID, count(*) as cnt,
             row_number() over (partition by AssociatedBibRecordID order by count(*) desc) as seqnum
      from Polaris.Polaris.CircItemrecords
   WHERE AssociatedBibRecordID IN (SELECT BibliographicRecordID FROM @t)
      group by AssociatedBibRecordID, AssignedCollectionID
     ) CollMode
where seqnum = 1

 SELECT
  BibliographicRecordID,
  CALLNO,
  Title,
  Author,
  cols.Name as MostCommonColl,
  GoodItems = ItemCount,
  Holds = HoldCount,
  ISNULL((HoldCount / NULLIF(ItemCount, 0)), HoldCount)  AS HoldsPerItem
  FROM @t t

  LEFT JOIN @t2 t2 ON (t.BibliographicRecordID = t2.AssociatedBibRecordID)
  JOIN Polaris.Polaris.Collections Cols (NOLOCK)
  ON t2.AssignedCollectionID = cols.CollectionID

 ORDER BY HoldCount DESC, ItemCount
END

answered 2 years ago  by <img width="18" height="18" src="../../_resources/ba5af9207cc2451bb4c189a7923388a9.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_446"></a>[Accept as answer](#)

<a id="commentAns_446"></a>[Add a Comment](#)

<a id="upVoteAns_445"></a>[](#)

0

<a id="downVoteAns_445"></a>[](#)

Thanks.  Maybe there is a way we can shorten it since we're not interested in the status of the items, but simply the total number of holds on each bib record, the number copies and the ratio of holds per copy.  We could do without the collection if we got the bib number and title.  We could even compute the ratio in excel.

We do run the holds purchase alert and holds ratio reports in Reports and Notices.  Back in the day when we were on a different ILS we used to run an SQL for the holds ratio with the basic information I mentioned above.

Appreciate the input.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/ba5af9207cc2451bb4c189a7923388a9.jpg"/>  akrulik@bcls.lib.nj.us

- <a id="acceptAnswer_445"></a>[Accept as answer](#)

<a id="commentAns_445"></a>[Add a Comment](#)

<a id="upVoteAns_444"></a>[](#)

0

<a id="downVoteAns_444"></a>[](#)

Here is the SQL. As you can see by the comments at the top, it is a modification of the code in a stored procedure. I'm pretty sure that the original proceure uses temporary tables, but at the time I was working on this I was more comfortable with delcared table variables.

Hope this helps.

JT

----------Query begings below-----

--Query based on code from Polaris stored procedure Rpt_HoldPurchaseAlert
--with additions to limit by item assigned collection
--Added field for recently lost items.
BEGIN
 SET NOCOUNT ON
 
 DECLARE @t TABLE --\[CALLNO, SizzCount, OOASCount added\]
 (
  BibliographicRecordID int NOT NULL,
  CALLNO nvarchar(50) NULL,
  Author nvarchar(255) NULL,
  Title nvarchar(255) NULL,
  StandardNo varchar(100) NULL,
  MARCTypeOfMaterial varchar(80) NULL,
  HoldCount int NULL,
  ItemCount int NULL,
  SizzCount int NULL,
  OOASCount int NULL,
  LostMissRecCount int NULL
 )
 DECLARE @t1 TABLE
 ( BibliographicrecordID int NOT NULL,
  TagNumber int NULL,
  SubFieldID int NULL,
  StandardNumber nvarchar (100) NULL)
   
 DECLARE @t2 TABLE
 ( BibliographicrecordID int NOT NULL,
  StandardNumber nvarchar (100) NULL)

 --gets only locked requests
 INSERT INTO @t(BibliographicRecordID, HoldCount)
  SELECT distinct BibliographicRecordID, COUNT(SHR.SysHoldRequestID) as holds
 FROM Polaris.Polaris.SysHoldRequests SHR WITH (NOLOCK)
 WHERE SysHoldStatusID IN (1,3) AND BibliographicRecordID <> 0
 and BibliographicRecordID in
 (SELECT distinct it.AssociatedBibRecordID as Bibliographicrecordid from polaris.polaris.ItemRecords it with (nolock)
 where  it.assignedcollectionid in (@CollectionID))
 GROUP BY BibliographicRecordID

 \-\- only include final item records that are not withdrawn \[claim ret, lost, missing\]
 \-\- \[modified to count only holdable items\]
 UPDATE @t
 SET ItemCount =
  (SELECT COUNT(CIR.AssociatedBibRecordID)
   FROM Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)
   WHERE CIR.AssociatedBibRecordID = t.BibliographicRecordID AND
    CIR.ItemStatusID NOT IN (7,8,9,10,11) AND CIR.RecordStatusID = 1
    and CIR.Holdable= 1)
 FROM @t t, Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)

  UPDATE @t
 SET SizzCount =
  (SELECT COUNT(CIR.AssociatedBibRecordID)
   FROM Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)
   WHERE CIR.AssociatedBibRecordID = t.BibliographicRecordID AND
    CIR.ItemStatusID NOT IN (7,8,9,11) AND CIR.RecordStatusID = 1
    and CIR.MaterialTypeID in (23,25))
 FROM @t t, Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)

  UPDATE @t
 SET OOASCount =
  (SELECT COUNT(CIR.AssociatedBibRecordID)
   FROM Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)
   WHERE CIR.AssociatedBibRecordID = t.BibliographicRecordID AND
    CIR.ItemStatusID IN (13,15) AND CIR.RecordStatusID = 1
    and CIR.Holdable= 1)
 FROM @t t, Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)

  UPDATE @t
 SET LostMissRecCount =
  (SELECT COUNT(CIR.AssociatedBibRecordID)
   FROM Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)
   WHERE CIR.AssociatedBibRecordID = t.BibliographicRecordID AND
    CIR.ItemStatusID in (7,8,9,10) AND CIR.RecordStatusID = 1
    and CIR.ItemStatusDate >= dateadd(dd,-14,getdate()))
 FROM @t t, Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)

 IF (@nNumberOfItems > 0)
 BEGIN
  declare @fRatio float
  select @fRatio = @nNumberOfHolds / (@nNumberOfItems * 1.0)

  DELETE FROM @t
   WHERE ItemCount <> 0 
   AND HoldCount/(ItemCount * 1.0) < @fRatio
 END
 ELSE
 BEGIN
  DELETE FROM @t
   WHERE ItemCount <> 0 
   AND HoldCount/ItemCount < @nNumberOfHolds
 END

 UPDATE @t
 SET CALLNO = BR.BrowseCallNo,
  Author = BR.BrowseAuthor,
  Title = BR.BrowseTitle,
  MARCTypeOfMaterial = MTOM.Description
 FROM @t t
 INNER JOIN Polaris.Polaris.BibliographicRecords BR WITH (NOLOCK)
  ON t.BibliographicRecordID = BR.BibliographicRecordID
 LEFT OUTER JOIN Polaris.Polaris.MARCTypeOfMaterial MTOM WITH (NOLOCK)
  ON BR.PrimaryMARCTOMID = MTOM.MARCTypeOfMaterialID 

 INSERT INTO @t1
 (BibliographicrecordID, StandardNumber, TagNumber, SubFieldID)
 (select bt.BibliographicRecordID, bs.data as StandardNumber, bt.TagNumber,
 bs.BibliographicSubfieldID as SubFieldID
 from polaris.polaris.BibliographicSubfields bs with (nolock)
 join polaris.polaris.bibliographictags bt (nolock) on (bs.BibliographicTagID = bt.BibliographicTagID)
 where bt.TagNumber in (020,024)
 and bs.Subfield like 'a'
 and bt.BibliographicRecordID in(Select bibliographicrecordid from @t))

 INSERT INTO @t2
 (Bibliographicrecordid, StandardNumber)
 (Select BibliographicrecordID, max(standardnumber) as StandardNumber from @t1
 group by BibliographicrecordID)

 UPDATE @t
 SET StandardNo = t2.StandardNumber
 FROM @t t
 join @t2 t2 on (t.BibliographicRecordID = t2.BibliographicrecordID)

 SELECT
  BibRecordID = LTrim(str(BibliographicRecordID)),
  CALLNO,
  Title,
  Author,
  StandardNo,
  MARCTypeOfMaterial,
  GoodItems = LTrim(str(ItemCount)),
  Holds = LTrim(str(HoldCount)),
  Sizzlers = LTrim(str(SizzCount)),
  OnOrderAvailSoon = LTrim(str(OOASCount)),
  LostMissRecently = LTrim(str(LostMissReccount))
 FROM @t
 ORDER BY HoldCount DESC, ItemCount
END

answered 2 years ago  by <img width="18" height="18" src="../../_resources/ba5af9207cc2451bb4c189a7923388a9.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_444"></a>[Accept as answer](#)

<a id="commentAns_444"></a>[Add a Comment](#)

<a id="upVoteAns_443"></a>[](#)

0

<a id="downVoteAns_443"></a>[](#)

JT,

I'm going to need to go over this with one of my coworkers.  Could you post the SQL for the main dataset?  We could then look at both and see what works. 

Thanks, Anne

answered 2 years ago  by <img width="18" height="18" src="../../_resources/ba5af9207cc2451bb4c189a7923388a9.jpg"/>  akrulik@bcls.lib.nj.us

- <a id="acceptAnswer_443"></a>[Accept as answer](#)

<a id="commentAns_443"></a>[Add a Comment](#)

<a id="upVoteAns_442"></a>[](#)

0

<a id="downVoteAns_442"></a>[](#)

Hi Anne.

Attached is a report we use. It contains some information that you may not need, and may not have everything you want.

It includes input parameters for the number of "good" (not Lost, Missing, Claimed, Withdrawn) items, the number of holds per good item and the collection(s) to look at.

Output is:

Number of holds, number of good items, number of Sizzlers (non-holdable items in our "lucky day" collection), number of items In Processing (we call Available Soon), items that went to a bad status (Lost, Missing, Claimed) within the past 14 days, Call Number, Title, Author, a standard number (from the hold request, as I recall, but possibly from somewhere else) and the TOM.

It is an .rdl file, but to upload it I changed the extension to .txt. You can download it, change the extension and then load it in your report server to see if it works for you.

If the report does not work and you need the SQL for the main dataset, I can post it here.

Hope this helps,

JT

answered 2 years ago  by <img width="18" height="18" src="../../_resources/ba5af9207cc2451bb4c189a7923388a9.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_442"></a>[Accept as answer](#)

- [Holds Purchase Alert With Ordered and Recently LMC.txt](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/394?fileName=394-442_Holds+Purchase+Alert+With+Ordered+and+Recently+LMC.txt "Holds Purchase Alert With Ordered and Recently LMC.txt")

<a id="commentAns_442"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Does anyone have an SQL for patrons whose notifications settings are listed as "none"?](https://iii.rightanswers.com/portal/controller/view/discussion/621?)
- [I need a SQL for locating hold requests where the hold has been denied by a library](https://iii.rightanswers.com/portal/controller/view/discussion/843?)
- [Bookclub requests](https://iii.rightanswers.com/portal/controller/view/discussion/720?)
- [Requests that cant be filled](https://iii.rightanswers.com/portal/controller/view/discussion/308?)
- [Purchase or Inter-Library Loan Request Form/Database?](https://iii.rightanswers.com/portal/controller/view/discussion/856?)

### Related Solutions

- [Hold Processing SQL jobs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930920183263)
- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)
- [Dimensions for a patron photo](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930892170570)
- [How does Polaris determine requests to be duplicates?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930617910193)
- [Patron Status does not show a reminder that is in the NotificationHistory table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930782414146)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)