Items going from Lost to In in last month - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Items going from Lost to In in last month

0

58 views

Hi all,

Anyone have a SQL query to list items that have changed circ status form Lost to In in the last month?  

Thanks!!

Robin Dye, Kitsap Regional Library

asked 2 years ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

[circulation status](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=271#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_648"></a>[](#)

0

<a id="downVoteAns_648"></a>[](#)

Answer Accepted

Thanks JT!  You've done it again.

\- Robin  ![laughing](../../_resources/6320a86348ad4bb3840c162e569cfeff.gif)

answered 2 years ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

<a id="commentAns_648"></a>[Add a Comment](#)

<a id="upVoteAns_647"></a>[](#)

0

<a id="downVoteAns_647"></a>[](#)

Hi Robin.

I think this one may work for you. It uses ItemRecordHistory to look for checkins where the item was previously lost by the patron.

I also tried it to find items that went from Lost to In or Reshelving and got the same number of items. I included both options in the query. You may want to try it with action line commented out and the status lines "un-commmented" to make sure both work the same.

Hope this helps,

JT

SELECT DISTINCT
ir.ItemRecordID
,ir.Barcode
,ir.CallNumber
,br.BrowseTitle

FROM Polaris.Polaris.ItemRecords ir WITH (NOLOCK)
JOIN Polaris.Polaris.ItemRecordHistory irh (NOLOCK)
ON (ir.ItemRecordID = irh.ItemRecordID)
JOIN Polaris.Polaris.BibliographicRecords br (NOLOCK)
ON (ir.AssociatedBibRecordID = br.BibliographicRecordID)

WHERE

irh.ActionTakenID in (12) --Checked in, was lost by the patron
--irh.OldItemStatusID = 7
--AND irh.NewItemStatusID in (1, 17)
AND irh.TransactionDate >= DATEADD(MONTH,DATEDIFF(MONTH,0,GETDATE())-1,0)
AND irh.TransactionDate < DATEADD(MONTH,DATEDIFF(MONTH,0,GETDATE()),0)

--The date lines will get the entire previous month, no matter what day you run the query.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/c2b570ac7d324ad0b3fa6bc0fd6ff03c.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_647"></a>[Accept as answer](#)

I edited this to remove a typo. jtw

— jwhitfield@aclib.us 2 years ago

<a id="commentAns_647"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Report of Lost and Paid Items](https://iii.rightanswers.com/portal/controller/view/discussion/331?)
- [SQL for Lost and Paid Items](https://iii.rightanswers.com/portal/controller/view/discussion/662?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [Does anyone have a sql script for a monthly in-house check-in count?](https://iii.rightanswers.com/portal/controller/view/discussion/615?)
- [Average time things are lost before coming back?](https://iii.rightanswers.com/portal/controller/view/discussion/436?)

### Related Solutions

- [Items converting to Lost status](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930282614657)
- [Which lost item recovery settings are used for partially paid replacement costs?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170523113239517)
- [Default pricing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930543557097)
- [Items not moving to Lost status](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930564239028)
- [Item displaying as Lost/Accruing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930784833630)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)