[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# How to get a number for items returned on a specific date?

0

56 views

Hello!  
<br/>I've been asked to find out how many items were returned yesterday. I looked in SimplyReports > Items > Item count reports > Item Date Filters and tried with two options: 

- Item record history transaction date = 0 results
- Last check in date = got some results but based on the time (Item last check in date), I was only getting numbers for about 3 hours. 

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_bc7ad7fdf02447a78e31121f2e8ee09f.jpg"/> agoodman@darienlibrary.org

[simply reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=11#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 5 Replies   last reply 1 year ago

<a id="upVoteAns_1321"></a>[](#)

1

<a id="downVoteAns_1321"></a>[](#)

Answer Accepted

Here is an SQL query to count the check ins on a particular day at a particular branch. I’ve added a line to exclude check ins of digital items and a line to limit the count to items that were ‘Out’ or ‘Lost’ when checked in. In case you don’t have access to custom report building, I’ve written it to work in the Item Find Tool: just switch to SQL and check the ‘Count Only’ box.

&nbsp;

- If you want to count check ins across all branches, just delete the line for OrganizationID. Otherwise, change the ‘99’ to the ID number for the branch you want to count.
- If you want to count check ins from transfers between branches, just delete the line for OldItemStatusID.
- If you want to count digital check ins, just delete the line for AssignedCollectionID. Otherwise, change the ‘99’ in that line to the ID for your digital collection.

&nbsp;

SELECT irh.ItemRecordID

FROM Polaris.Polaris.ItemRecordHistory AS irh

&nbsp;        INNER JOIN Polaris.Polaris.ItemRecordHistoryActions AS irha

&nbsp;               ON irha.ActionTakenID = irh.ActionTakenID

&nbsp;        INNER JOIN Polaris.Polaris.CircItemRecords AS cir

&nbsp;               ON cir.ItemRecordID = irh.ItemRecordID

WHERE irh.TransactionDate >= '1/29/2023'

&nbsp;           AND irh.TransactionDate < '1/30/2023'

&nbsp;           AND irha.ActionTakenDesc LIKE '%Check%In%'

&nbsp;     

AND irh.OrganizationID \= 99       

&nbsp;           AND cir.AssignedCollectionID <> 99

&nbsp;           AND irh.OldItemStatusID IN (2,7)

&nbsp;

I hope this helps you get the data you need.

&nbsp;               --Daniel

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_bc7ad7fdf02447a78e31121f2e8ee09f.jpg"/> ddunphy@aclib.us

<a id="commentAns_1321"></a>[Add a Comment](#)

<a id="upVoteAns_1325"></a>[](#)

1

<a id="downVoteAns_1325"></a>[](#)

Hi Amanda,

In our library, it goes back three years, but I'm told there's a setting for it somewhere and that many libraries retain it for only 2 years. Either way, 2019 is out of reach for the Item Record History Tables. Here's a query using the transaction tables. That can be quite a bit slower and more taxing on your system resources (because the tables are so large). On the other hand, this still works in the Item Find Tool, and the check ins from only 1 day shouldn’t be a problem so I’d just be careful about using a wider date range.

Finally, I’d be remiss if I didn’t mention that, although the transaction tables will retain the check in records indefinitely, if you use this query in the Item Find Tool it will probably drop all item records that have been deleted so the further back you look the more check ins you may be missing. Since the query uses the item number stored in the transaction tables, if you run it in a custom SQL environment you should get an accurate count regardless of date, but if you run it in the Find Tool you may get a significant undercount. For reference, for an arbitrary date in 2018, I see a loss of nearly 30% of the actual check ins for Find Tool vs SSMS. I didn’t check any additional days so I don’t know if that’s representative.

Anyhow, here’s the transaction table version. Remember to change the ‘99’s to the branch ID you’re targeting/your digital collection ID respectively (or delete those lines).

`SELECT td38.numValue`

`FROM PolarisTransactions.Polaris.TransactionHeaders AS th`

&nbsp;    `         INNER JOIN PolarisTransactions.Polaris.TransactionDetails AS td38`

&nbsp;          `                     ON td38.TransactionID = th.TransactionID AND td38.TransactionSubTypeID = 38`

&nbsp;    `         INNER JOIN PolarisTransactions.Polaris.TransactionDetails AS td61`

&nbsp;          `                     ON td61.TransactionID = th.TransactionID AND td61.TransactionSubTypeID = 61`

`WHERE th.TranClientDate >= '1/29/2023'`

&nbsp;     `           AND th.TranClientDate < '1/30/2023'`

&nbsp;     `           AND th.TransactionTypeID = 6002`

&nbsp;     `           AND th.OrganizationID = 99`

&nbsp;     `           AND td61.numValue <>  99`

&nbsp;

I hope this helps you get the historical data you need,

&nbsp;           --Daniel

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_bc7ad7fdf02447a78e31121f2e8ee09f.jpg"/> ddunphy@aclib.us

- <a id="acceptAnswer_1325"></a>[Accept as answer](#)

<a id="commentAns_1325"></a>[Add a Comment](#)

<a id="upVoteAns_1326"></a>[](#)

0

<a id="downVoteAns_1326"></a>[](#)

Dear Daniel,   
<br/>Thank you for the information! I greatly appreciate your time, expertise, and generosity in answering my question. 

  
Best,   
<br/>Amanda

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_bc7ad7fdf02447a78e31121f2e8ee09f.jpg"/> agoodman@darienlibrary.org

- <a id="acceptAnswer_1326"></a>[Accept as answer](#)

<a id="commentAns_1326"></a>[Add a Comment](#)

<a id="upVoteAns_1324"></a>[](#)

0

<a id="downVoteAns_1324"></a>[](#)

Dear Daniel,   
<br/>You are a saint! I have SQL access in the custom side, but I know my colleague who didn't have access could run SQL and find answers to queries. We had the same permissions. I'm temporarily in charge of this. :-)   
 

Best,   
<br/>Amanda

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_bc7ad7fdf02447a78e31121f2e8ee09f.jpg"/> agoodman@darienlibrary.org

- <a id="acceptAnswer_1324"></a>[Accept as answer](#)

<a id="commentAns_1324"></a>[Add a Comment](#)

<a id="upVoteAns_1323"></a>[](#)

0

<a id="downVoteAns_1323"></a>[](#)

Dear Daniel,  
<br/>By any chance, do you know how far back I might expect this history to go? I tried to check for pre-pandemic of 12/1/2019 and receieved zero results. We're self-hosted so that might affect things.   
<br/> Best,   
<br/>Amanda

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_bc7ad7fdf02447a78e31121f2e8ee09f.jpg"/> agoodman@darienlibrary.org

- <a id="acceptAnswer_1323"></a>[Accept as answer](#)

<a id="commentAns_1323"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for items returned late by collection](https://iii.rightanswers.com/portal/controller/view/discussion/718?)
- [Adding number of "live" items to a report](https://iii.rightanswers.com/portal/controller/view/discussion/666?)
- [Does anyone know of a way to remove a library's telephone number on the return address portion of a printed overdue notice?](https://iii.rightanswers.com/portal/controller/view/discussion/1021?)
- [Can you get a list of exact dates an item was placed on hold?](https://iii.rightanswers.com/portal/controller/view/discussion/1031?)
- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)

### Related Solutions

- [Tip for checking the due date on returned overdue materials](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930463638326)
- [How to take a collection inventory using Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930994460225)
- [What is the difference between Creation Date and Acquisition Date?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930268866818)
- [What happens when old Lost items are returned?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180907110226360)
- [Maximum item call number length](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930857542877)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)