print a patron's reading history - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# print a patron's reading history

0

88 views

Is there a way to print a patrons reading history?

asked 11 months ago  by <img width="18" height="18" src="../../_resources/683e3cc01c1346a8b9b2c6e799cf61e3.jpg"/>  snorris@mljlibrary.org

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 4 months ago

<a id="upVoteAns_1004"></a>[](#)

2

<a id="downVoteAns_1004"></a>[](#)

If you have SQL read access, you can run the below which creates a relatively clean printout.

select prh.PatronID, prh.ItemRecordID, mtom.Description, br.BrowseTitle, br.BrowseAuthor, prh.CheckOutDate from PatronReadingHistory prh
inner join ItemRecordDetails ird on (prh.ItemRecordID=ird.ItemRecordID)
inner join CircItemRecords cir on (ird.ItemRecordID=cir.ItemRecordID)
inner join BibliographicRecords br on (cir.AssociatedBibRecordID=br.BibliographicRecordID)
left join MARCTypeOfMaterial mtom on (br.PrimaryMarcTOMID = mtom.MARCTypeOfMaterialID)
where prh.PatronID={patron ID here without braces}
order by prh.CheckOutDate

Kevin French

Systems Librarian

GMILCS, Inc.

31 Mount Saint Mary's Way

Hooksett, NH 03106-4400

answered 11 months ago  by <img width="18" height="18" src="../../_resources/683e3cc01c1346a8b9b2c6e799cf61e3.jpg"/>  kfrench@gmilcs.org

- <a id="acceptAnswer_1004"></a>[Accept as answer](#)

<a id="commentAns_1004"></a>[Add a Comment](#)

<a id="upVoteAns_1099"></a>[](#)

0

<a id="downVoteAns_1099"></a>[](#)

When I try to run this, SQL Server Management Studio tells me that 'PatronReadingHistory' is an invalid object name. Anyone know if it changed at some point?

answered 4 months ago  by <img width="18" height="18" src="../../_resources/683e3cc01c1346a8b9b2c6e799cf61e3.jpg"/>  erumsey@cityofirving.org

- <a id="acceptAnswer_1099"></a>[Accept as answer](#)

Hi,  I'm on version 6.6 and can still run the above.  It also looks the same as it has historically in the 6.6 Database Repository.  Maybe the issue is with line 6 where I should have commented out the message for putting in the patron ID so I've done so below.

select prh.PatronID, prh.ItemRecordID, mtom.Description, br.BrowseTitle, br.BrowseAuthor, prh.CheckOutDate from PatronReadingHistory prh
inner join ItemRecordDetails ird on (prh.ItemRecordID=ird.ItemRecordID)
inner join CircItemRecords cir on (ird.ItemRecordID=cir.ItemRecordID)
inner join BibliographicRecords br on (cir.AssociatedBibRecordID=br.BibliographicRecordID)
left join MARCTypeOfMaterial mtom on (br.PrimaryMarcTOMID = mtom.MARCTypeOfMaterialID)
where prh.PatronID=--patron ID here without braces
order by prh.CheckOutDate

— kfrench@gmilcs.org 4 months ago

Thank you so much!

— erumsey@cityofirving.org 3 months ago

I ended up adding some things to the SQL and it worked. I also recieved the PatronReadingHistory' invalid.

Try this.

use Polaris

select prh.PatronID, prh.ItemRecordID, mtom.Description, br.BrowseTitle, br.BrowseAuthor, prh.CheckOutDate from \[Polaris\].\[PatronReadingHistory\] prh
inner join \[Polaris\].\[ItemRecordDetails\] ird on (prh.ItemRecordID=ird.ItemRecordID)
inner join \[Polaris\].\[CircItemRecords\] cir on (ird.ItemRecordID=cir.ItemRecordID)
inner join \[Polaris\].\[BibliographicRecords\] br on (cir.AssociatedBibRecordID=br.BibliographicRecordID)
left join \[Polaris\].\[MARCTypeOfMaterial\] mtom on (br.PrimaryMarcTOMID = mtom.MARCTypeOfMaterialID)
where prh.PatronID=     --patron ID here without braces
order by prh.CheckOutDate

— brandon.williams@mesaaz.gov 1 month ago

<a id="commentAns_1099"></a>[Add a Comment](#)

<a id="upVoteAns_1005"></a>[](#)

0

<a id="downVoteAns_1005"></a>[](#)

Thank you Kevin!  Unfortunatly, we do not have SQL. 

answered 11 months ago  by <img width="18" height="18" src="../../_resources/683e3cc01c1346a8b9b2c6e799cf61e3.jpg"/>  snorris@mljlibrary.org

- <a id="acceptAnswer_1005"></a>[Accept as answer](#)

<a id="commentAns_1005"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [What causes patron's reading history to clear unexpectedly?](https://iii.rightanswers.com/portal/controller/view/discussion/249?)
- [Reading history settings](https://iii.rightanswers.com/portal/controller/view/discussion/520?)
- [Does anyone use EnvisionWare and/or Polaris to place money on library cards to pay for copy and print jobs?](https://iii.rightanswers.com/portal/controller/view/discussion/221?)
- [Problems with the Honeywell 1400g reading Brodart barcodes?](https://iii.rightanswers.com/portal/controller/view/discussion/929?)
- [Patron expiration date update](https://iii.rightanswers.com/portal/controller/view/discussion/727?)

### Related Solutions

- [Do ILLs appear in patron reading history?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930815959032)
- [Search patron reading history within the staff client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930708077324)
- [How does patron reading history work?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170412165935243)
- [Reader ratings for Outreach patrons is grayed out](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930509433339)
- [Patron Status does not show a reminder that is in the NotificationHistory table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930782414146)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)