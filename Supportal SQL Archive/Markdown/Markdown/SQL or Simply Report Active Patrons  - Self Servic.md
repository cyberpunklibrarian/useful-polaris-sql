[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL or Simply Report Active Patrons

0

138 views

Hello,

A board member has requested a report to find how many active patrons we've had yearly from 2017-to 2021. We have 16,522 registered patrons and they want to see how many of those are active. This will also trigger us to purge our patron accounts. Is there a SQL query or Simply report that can generate these numbers?

Thank you in advance!!!

TReel 

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_6c9cc859f2a94f06879ec17207122874.jpg"/> treel@mypccl.org

[patron account](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=85#t=commResults) [simply reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=11#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 2 years ago

<a id="upVoteAns_1233"></a>[](#)

1

<a id="downVoteAns_1233"></a>[](#)

I should start with a couple of caveats. :)

It seems to me that how you measure this depends on how you define an "active" patron. Do you count patrons who have attended library programs or asked reference questions but haven't borrowed any materials? I would think that data isn't usually accessible to Polaris so, even if you're tracking it somewhere, combining it with Polaris transaction data would be pretty cumbersome. But if you're willing to limit the definition of "active patrons" for each year to "patrons who have at least one checkout during that calendar year" then the query below will give you that number (although that number may be a somewhat imperfect standin for the "actual" number of active patrons). The query doesn't look at any other activity (checkins, holds placed, etc.) although you could just add other transaction types to that first WHERE statement at the cost of some additional runtime, and if you wanted a more expansive timeframe (like considering patrons active if they have a checkout in the past 2 years) that would take some rewriting. Finally, since you wanted to go back 5 years, I used the transaction tables so this does not run quickly.

SELECT  
   YEAR(th.TransactionDate)  
   , COUNT(DISTINCT td.numValue)  
FROM  
   PolarisTransactions.polaris.TransactionHeaders AS th  
   INNER JOIN PolarisTransactions.Polaris.TransactionDetails AS td  
      ON td.TransactionID = th.TransactionID  
WHERE  
   th.TransactionTypeID = 6001  
   AND th.TransactionDate >= '1/1/2017'  
   AND th.TransactionDate < '1/1/2022'  
   AND td.TransactionSubTypeID = 6  
GROUP BY YEAR(th.TransactionDate)

I hope that helps.

&nbsp;    -Daniel

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_6c9cc859f2a94f06879ec17207122874.jpg"/> ddunphy@aclib.us

- <a id="acceptAnswer_1233"></a>[Accept as answer](#)

Using the transaction tables also assumes the library had the appropriate transaction logging turned on during this period (Polaris Sys Admin setting). It is really the ONLY way you could attempt to get this information historically like this, but just wanting to put that out there in case someone ran this and didn't get data back and wasn't sure why.

This may or may NOT include eContent checkouts depending on if you have the integration turned on and properly working for your eContent provider(s).

— wosborn@clcohio.org 2 years ago

One other suggestion, you should use **TranClientDate** instead of TransactionDate. The reason is that typically **TranClientDate** has a SQL index and will be MUCH faster than TransactionDate which does NOT typically have an index.

Without an index, the query must do a table scan of ALL entries in the table versus being able to "seek" and selectively pull the data when using an index. So, using **TranClientDate** should speed up the query.

— wosborn@clcohio.org 2 years ago

<a id="commentAns_1233"></a>[Add a Comment](#)

<a id="upVoteAns_1237"></a>[](#)

0

<a id="downVoteAns_1237"></a>[](#)

Good morning!

&nbsp;

We are needing the same information for our Library Board.  I am new to SQL!  When we copy and paste the query above, and we change the dates to what we need them to be.  I get the following error message: The Search Could Not Be Processed By The Remote Database: 'PR'.  Could someone please tell me what I am doing wrong?

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_6c9cc859f2a94f06879ec17207122874.jpg"/> robin.walden@brentwoodtn.gov

- <a id="acceptAnswer_1237"></a>[Accept as answer](#)

It sounds like you are trying to run this SQL in the Find Tool. It was meant to be run in a SQL query window. I'd recommend that you reach out to support for additional help and other possible solutions for finding the information you're looking for.

— wosborn@clcohio.org 2 years ago

Hi Robin,

&nbsp;

My query was intended to run in a report created by Report Builder or in an environment such as SQL Server Management Studio (SSMS). It looks to me as if you might be attempting to execute this query in the Patron Find Tool (using SQL Search Mode). While the SQL Search in the Find Tool can be quite useful, it has a few limits compared to full SQL including:

&nbsp;

&nbsp;       1. It can only Select one column. So my query above won't run in the Find Tool because it attempts to return two columns in the Select statement.

&nbsp;       2. That column must be the ID for the type of record matching the Find Tool. So my query also violates this rule because it tries to return 'Year' and 'Count' where the Patron Find Tool only returns PatronID. As a caution, a single column that doesn't match the Find Tool won't actually cause an error--it will just give wildly inaccurate results as it attempts to intepret the results your query did return as the results the Find Tool expects to be returned. :)

&nbsp;

So when I try to run my query in the Find Tool, I get the same error message as you did: The Search Could Not Be Processed By The Remote Database: 'PR'

&nbsp;

Two possible solutions occur to me. One would be to check with your Library's Polaris site manager to see if you can just have Report Builder or SSMS added on your machine, get the credentials you need, and start designing and utilizing your own custom reports. But I recognize that may not be an immediately available option for you, and you might need the data in a hurry. The query below is edited to work in the Patron Find Tool. Just switch to SQL Search, check the box for 'Count Only', and paste this query. Even with this edited query, I found that I ran into local resource limits if I tried to retrieve a whole year's data so I used a 3 month interval which would allow you to build an 'Active Users by Quarter' table. Of course, the other downside is that for each quarter you'd need to change the dates, run this query again, and record the results manually in a spreadsheet or document. 

&nbsp;

SELECT DISTINCT td.numValue  
FROM  
PolarisTransactions.polaris.TransactionHeaders AS th  
INNER JOIN PolarisTransactions.Polaris.TransactionDetails AS td  
ON td.TransactionID = th.TransactionID  
WHERE  
th.TransactionTypeID = 6001  
AND th.TranClientDate >= '1/1/2022'  
AND th.TranClientDate < '4/1/2022'  
AND td.TransactionSubTypeID = 6

&nbsp;

I hope this helps you get the data you need, and welcome to the Polaris SQL community!

&nbsp;    -Daniel

— ddunphy@aclib.us 2 years ago

<a id="commentAns_1237"></a>[Add a Comment](#)

<a id="upVoteAns_1235"></a>[](#)

0

<a id="downVoteAns_1235"></a>[](#)

Thank you all for the suggestions. I am not SQL savvy and to be honest it (makes me want to hide under a blanket) Thank you for sharing your SQL codes with us that are just dipping our toes in the water, It is a tremendous help.

&nbsp;[wosborn@clcohio.org](mailto:wosborn@clcohio.org) you mentioned using **TranClientDate,** how would I go about doing this correctly? 

&nbsp;

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_6c9cc859f2a94f06879ec17207122874.jpg"/> treel@mypccl.org

- <a id="acceptAnswer_1235"></a>[Accept as answer](#)

Any place you see: th.TransactionDate in the script from ddunphy@aclib.us, replace it with th.TranClientDate

— wosborn@clcohio.org 2 years ago

<a id="commentAns_1235"></a>[Add a Comment](#)

<a id="upVoteAns_1234"></a>[](#)

0

<a id="downVoteAns_1234"></a>[](#)

How you define "active patrons" is the caveat for sure... In another approach you could coalesce the LastActivityDate, RegistrationDate, and EntryDate from the PatronRegistration and Patrons tables.

  
The LastActivityDate is updated when certain transactions occur (see Polaris docs for more details). However, if the patron has not done any of those activities, then LastActivityDate would be NULL, so in that case you would want to know if the patron was registered during that time, so RegistrationDate would be used. However, in the event where a record was imported to your system, say from a migration, the Registration Date would be NULL. So, the last date to coalesce would be Entry Date as all patron records should have an Entry Date value even if the record was imported.

  
SELECT CASE WHEN (COALESCE (LastActivityDate,RegistrationDate,EntryDate)) >= (cast(dateadd(year, -3, getdate()) as date))  
THEN 'Active'  
ELSE 'Inactive' END AS \[ActivityStatus\],  
COUNT(DISTINCT pr.PatronID)  
FROM  
Polaris.Polaris.PatronRegistration pr (nolock)  
JOIN Polaris.Polaris.Patrons p (nolock)  
ON pr.PatronID = p.PatronID  
GROUP BY CASE WHEN (COALESCE (LastActivityDate,RegistrationDate,EntryDate)) >= (cast(dateadd(year, -3, getdate()) as date))  
THEN 'Active'  
ELSE 'Inactive' END

There are many ways to slice and dice the date... I recommend defining "Active Patron" with board approval before drafting code, it will help you set the standard for what you should query.

Good luck!

&nbsp;

answered 2 years ago by ![eric.young@phoenix.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_eric.young@phoenix.gov20191029113427.jpg) eric.young@phoenix.gov

- <a id="acceptAnswer_1234"></a>[Accept as answer](#)

LastActivityDate gets overwritten each time one of the "activities" occurs though. So, if a patron were active in 2018 and then active later in 2020, using LastActivityDate they would only "count" as being active in 2020.

This is a much better method if you were to say run this at the end of each year for the previous year and then store the data somewhere else for future comparison, but if you haven't done that, then I think you'll need to attempt to comb the transaction tables - assuming you've turned on the appropriate logging there as well.

— wosborn@clcohio.org 2 years ago

So true, I just tossed the grouping in to replicate the visual... oversight, sorry.

I typically just use the coalesce to go back 'n years of activity. Anything older than say 3 years is determined "Inactive". SQL updated...

My script won't help you find activity in the past, just determine if the patron is currently "Active" based on set criteria.

— eric.young@phoenix.gov 2 years ago

<a id="commentAns_1234"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Simply reports patron services](https://iii.rightanswers.com/portal/controller/view/discussion/355?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [Simply Reports Discrepancies](https://iii.rightanswers.com/portal/controller/view/discussion/960?)

### Related Solutions

- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Patron List report in Simply Reports showing blank addresses for patrons with addresses](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930560488447)
- [Question about a saved SimplyReports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930457380859)
- [Question about published SimplyReports reports](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989986518)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)