[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch

0

51 views

Does anyone have a SQL query they would be willing to share which would return a list of patrons who are registered at a particular branch but whose recent transactions take place only at other branches? We share our Polaris database with another library system and we would like to see how many patrons of the other library system only use our branches, and vice versa. Since a query like this involves using the transaction tables, I am having a hard time building a query that will return the data I need.

&nbsp;

Thanks in advance!

asked 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_3ce6698ec04f4b5fb3ab1ff584ed9cb8.jpg"/> ateeple@lcplin.org

[checked out items sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=343#t=commResults) [patron registration](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=238#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults) [sql patron](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=387#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Identifying a count of patrons, by registered library, whose only activity over a period of time is OverDrive checkout through Polaris integration?](https://iii.rightanswers.com/portal/controller/view/discussion/1044?)
- [Fines paid per year by Branch, Branch/Collection, and Branch/Patron Code](https://iii.rightanswers.com/portal/controller/view/discussion/1079?)
- [Is there a way to set staff placed holds for patrons to default to patron branch instead of branch location when hold is placed?](https://iii.rightanswers.com/portal/controller/view/discussion/799?)
- [SQL for patron circ at a particular workstation](https://iii.rightanswers.com/portal/controller/view/discussion/918?)
- [Does anyone have a SQL report that will show renewal vs new cards for a particular patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/1377?)

### Related Solutions

- [Statistics for registered patrons](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930383515616)
- [New patrons identified as a duplicate / new patrons not able to register on the PAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930681961607)
- [Which line of the Loan Periods table is used?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170413110944913)
- [Error: The Postal Code is not valid. Please try another or see a staff member.](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170616164552265)
- [Cannot bulk change library assigned blocks](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930641484135)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)