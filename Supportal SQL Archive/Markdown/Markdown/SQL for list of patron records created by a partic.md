SQL for list of patron records created by a particular staff member - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL for list of patron records created by a particular staff member

0

70 views

I'd like to collect data about activity on patron accounts created by our outreach staff.  Is there a way to make a list of patron records created by a particular staff member?

asked 1 year ago  by <img width="18" height="18" src="../../_resources/939eaefb9fd44a0297f8ffa15c12e1d0.jpg"/>  slewis@nolalibrary.org

Is there a way to show this by the last staff member who modified the record? I am trying to see how many patron records a staff member either renewed or created for a contest we are having. I am seeing some issues with CreatorID since the staff who renews is not the creator of the original record.

— brandon.williams@mesaaz.gov 28 days ago

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_850"></a>[](#)

0

<a id="downVoteAns_850"></a>[](#)

Answer Accepted

This is fantastic.  Thank you so much.  How would you best limit by creation date?

answered 1 year ago  by <img width="18" height="18" src="../../_resources/939eaefb9fd44a0297f8ffa15c12e1d0.jpg"/>  slewis@nolalibrary.org

You'd have to join the PatronRegistration table so you could limit by Registration Date (but then PatronID would become an ambiguous column name, so you'd also have to change the top line).
SELECT p.PatronID
FROM Patrons p with (NOLOCK)
JOIN PolarisUsers pu with (NOLOCK)
ON pu.PolarisUserID = p.CreatorID
JOIN PatronRegistration pr with (NOLOCK)
ON pr.PatronID = p.PatronID
WHERE pu.Name = 'jjack'
AND pr.RegistrationDate > '1/1/2019'

*--or--*

SELECT p.PatronID
FROM Patrons p with (NOLOCK)
JOIN PolarisUsers pu with (NOLOCK)
ON pu.PolarisUserID = p.CreatorID
JOIN PatronRegistration pr with (NOLOCK)
ON pr.PatronID = p.PatronID
WHERE pu.Name = 'jjack'
AND pr.RegistrationDate BETWEEN '10/1/2018' AND '1/1/2019 11:59:59 PM'

\[You can change the dates as needed, of course; just don't lose the single quotes around them or the query won't work.\]

— jjack@aclib.us 1 year ago

<a id="commentAns_850"></a>[Add a Comment](#)

<a id="upVoteAns_849"></a>[](#)

0

<a id="downVoteAns_849"></a>[](#)

This should work: 

SELECT PatronID
FROM Patrons p with (NOLOCK)
JOIN PolarisUsers pu with (NOLOCK)
ON pu.PolarisUserID = p.CreatorID
WHERE pu.Name = 'jjack'
--^ change this pu.Name to match the Polaris user name.

There's more you could do--limiting by creation date, limiting to unexpired patrons, etc.; if you need any of that, please let me know.

answered 1 year ago  by <img width="18" height="18" src="../../_resources/939eaefb9fd44a0297f8ffa15c12e1d0.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_849"></a>[Accept as answer](#)

<a id="commentAns_849"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for patron circ at a particular workstation](https://iii.rightanswers.com/portal/controller/view/discussion/918?)
- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)
- [Does anyone have an SQL for patrons whose notifications settings are listed as "none"?](https://iii.rightanswers.com/portal/controller/view/discussion/621?)
- [Patron Code "Staff "override](https://iii.rightanswers.com/portal/controller/view/discussion/704?)
- [I need a SQL for phatom lost item count attached to a patron's record.](https://iii.rightanswers.com/portal/controller/view/discussion/839?)

### Related Solutions

- [Error: The Postal Code is not valid. Please try another or see a staff member.](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170616164552265)
- [Search patron reading history within the staff client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930708077324)
- [Change ExpressCheck Credentials](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180315161854260)
- [Find all permission groups that have the Secure patron record permissions](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930759206464)
- [Will Saved Title Lists be retained when two patrons are merged?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930896743928)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)