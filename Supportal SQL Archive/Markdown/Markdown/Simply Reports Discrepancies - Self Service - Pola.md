[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Simply Reports Discrepancies

0

117 views

I've been running a report of daily checkouts using an Item Count. I include the branch abbreviation and patron barcode and filter it using relative date = one day ago.

One of the managers uses this each day for tracking during the pandemic/shutdown and continues to run it.

I also do a monthly statistical report on checkouts, also broken out by branch.

She adds up the daily circ counts and compares to the monthly check out stats and they are off by a pretty wide margin. Over a period of six months the daily report adds up to 10,000 fewer ckos than the monthly stat report.

I tried leaving out patron barcode from the count, or including material type or any other item related setting but that report is always less than the stat report.

Does anyone know why? I know there will always be different results depending on the way the report is run but this seems fairly significant and I can't explain why.

Thanks.

&nbsp;

asked 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_b28ae700542442e6ac089f31037d507b.jpg"/> csmiley@leegov.com

[circulation stats](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=412#t=commResults) [simply reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=11#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 3 years ago

<a id="upVoteAns_1083"></a>[](#)

0

<a id="downVoteAns_1083"></a>[](#)

SPELL CHECKED version :)

I came across discrepancies as well after manually combining dailies and then doing a ***time range report*** <ins>using</ins> the same criteria. After comparing the reports (in Excel), I took the Date field column and made a copy beside it. I then changed the "date" to a "time" number in the copied column. I was able to see the transactions in seconds. In comparing reports, there were some transactions not showing up on the ***time range report.*** For those specific 

items, the checkout times were within smaller "seconds" to the next check out item's time. This was within the same patron id's transactions.

Example: Patron had 5 check out transactions right after another. Transaction 4 didn't show up on the **time range report.** It was done within 5 seconds from transaction 3. All the other transactions were done at least 15 seconds after each other and showed up on both reports.

&nbsp;

I wasn't sure if the system needs a certain amount of time between transactions before it registers, but if that was the case, why would the daily reports pick them up? Credit Card transactions do similar things if a dup is "assumed" and deletes out. After attaching an example of my findings on a ticket, I didn't get anywhere with it.  We use Savannah and found the same issues pulling dailies and a time range report there. I'm not sure if it boils down to software programming? Savannah pulls from Polaris.

Not sure if this helps, but if you have the time you should be able to recreate the above..

Best,

Sarah

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_b28ae700542442e6ac089f31037d507b.jpg"/> slavallee@cityofterrell.org

- <a id="acceptAnswer_1083"></a>[Accept as answer](#)

That's interesting. I have not checked down to the seconds and compare transactions. I'll check that and see if one is missing some of them. It doesn't make sense that it would do that but I suppose it's possible.

Thanks for the insight.

— csmiley@leegov.com 3 years ago

<a id="commentAns_1083"></a>[Add a Comment](#)

<a id="upVoteAns_1082"></a>[](#)

0

<a id="downVoteAns_1082"></a>[](#)

Here are the statements.

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_b28ae700542442e6ac089f31037d507b.jpg"/> csmiley@leegov.com

- <a id="acceptAnswer_1082"></a>[Accept as answer](#)

- [CircSQLExamples.docx](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/960?fileName=960-1082_CircSQLExamples.docx "CircSQLExamples.docx")

I think I see a few different problems here, but I'm not sure I have a clear/definitive answer, just some avenues to explore.  I'm sorry.

The first problem is that the top query, with patron barcodes, relies on the ItemCheckouts table but itemrecordIDs are moved out of that table as things get checked back in.  Luckily it's doing a left join on ItemCheckouts rather than an inner join, but depending on how the stats are getting counted you could still be missing checkouts simply because something was checked out for less than a day or because it was renewed then checked in later the same day (this last one could mean you're missing a lot of circs if your system auto-renews things for patrons).  I'm not sure if you're dumping the list of patron barcodes into Excel and using COUNT or COUNTA or if you're having SimplyReports aggregate them and report a count, but the patron barcode will be null if the item is no longer checked out so you might be missing circs simply due to a program counting the wrong field/column.

The second problem is that both queries are deriving a date using getdate(), which automatically appends the current time, which means that you can get different results depending on when you run the report. You mentioned this in one of the other comments, and you'd think in theory that it should all work out in the end, but I couldn't swear to it: if you tend to run the report at noon but run it in the afternoon one day, you'll be missing a chunk of the previous day's checkouts which won't get captured and summed up later, and if you usually run it in the afternoon but run it in the morning one day, you'll be double-counting some of those checkouts. That said, I don't know how careful people are to always run these at the same time and I haven't dug into it to see how much variance you can get with this sort of thing. If it's a report running on a schedule, it might be fine (though I think in general it would be more reliable to specify dates rather than using relative dates, or to use relative dates with some additional thing to strip times and/or specify exact times, but I don't think Simply Reports offers that as an option).

The third problem is that neither query takes care to exclude downloadable titles, yet Polaris can't be trusted for circ figures on OverDrive titles (or presumably on titles in 3M, Axis360, Hoopla, etc.), partly due to the fact that Polaris often doesn't sync data every 15 minutes as it intends to and partly due to the fact that patrons can read picture books and/or early readers in less than 15 minutes and partly due to the fact that people sometimes check something out out but decide it's not for them and return it early. That said, both queries are making that same mistake (not excluding ebooks/eAudio), so I think this is a problem in general but probably not the source of the discrepancy you've run into.

A fourth potential problem--maybe only in theory--is the inner joins. The top query, using patron barcodes, is doing an inner join between CircItemRecords and ItemRecordDetails. I only found 25 items (out of about 860k items) which were in one of those tables but not the other, so I'm not sure what sort of sync schedule they're on or what sort of efforts the system makes to keep the two tables in sync, but circs will only get counted on items which appear in both those tables.  I don't know how often this sort of thing happens and/or how prevalent it might be in other organizations, sorry.  It might not be an issue for you at all; for us, I'd say it's definitely not statistically significant.  :-)

I suspect that the main problem is joining through ItemCheckouts but I couldn't swear to it. When I poked around in our database I found early reader books which had been checked out in OverDrive and returned ten minutes later, DVDs and/or audiobooks which were returned after 2 or 3 hours, and a lot of items which were auto-renewed then returned later that same day.  All of these things have a chance of not getting counted in that top query.

— jjack@aclib.us 3 years ago

That first problem I have identified and figured it out - if an item is checked out then back in on the same date there is no patron listed with it. The same thing happens with auto-renew. I found a lot of those - where an item auto-renewed when the job ran during the early monring hours, but the patron returned the item that day so the patron barcode didn't record, but the transaction was still counted.

The second problem I'm not sure about either. Both reports are scheduled to run during off hours, very early in the morning after the auto-renew job runs. I'll have to check schedule time though and see if they are the exact same time, I know they are very close. That might make a difference but it seems that the monthly total should still reflect the daily reports added up.

The third issue you mention wouldn't apply because we don't integrate Overdrive or Hoopla. (We tried when it first came out, but was so problematic that we decided against it).

&nbsp;

I'm still leaning towards the difference between one report using transacting branch and the other report using assigned branch. The daily uses assigned branch and I'm not so sure that's going to count the same as where the transaction actually occurred. I'm still puzzled why the totals aren't a bit closer.

I have found statistics frequently pose this kind of dilemma - depending on how you run them results may vary. 

Thanks for all your insight, that's helpful.

— csmiley@leegov.com 3 years ago

Ah....  For some reason I thought the manager was seeing a 10k difference in *total* circs across the entire system (meaning that 10k circs were just not accounted for at all) but that's probably not it at all--they're just looking at circs at their own branch, aren't they?  If so, you're probably right, and that's almost certainly the problem.  I'm not clear on what makes Polaris reassign branches on an item, but there could easily be a 10k difference in circs over 6 months depending on whether you're counting circs by transacting branch or by assigned branch (I saw up to a 300-circ difference in a single day on one branch).

— jjack@aclib.us 3 years ago

<a id="commentAns_1082"></a>[Add a Comment](#)

<a id="upVoteAns_1081"></a>[](#)

0

<a id="downVoteAns_1081"></a>[](#)

What specific relative date filters are you using? Without knowing the full details, you may be missing out on renewals in a daily item count report, which are included in your statistical report. One way to check is to try running it for the period but choosing "renewals only" on a statistical report and seeing if that accounts for the difference during that time period.

answered 3 years ago by ![sthero@yrl.ab.ca](https://iii.rightanswers.com/portal/app/images/custom/avatar_sthero@yrl.ab.ca20190719103410.jpg) sthero@yrl.ab.ca

- <a id="acceptAnswer_1081"></a>[Accept as answer](#)

Each of the reports is using both checkouts and renewals so I know those are both included. The relative date on both is 1 day ago and the report run. I thought maybe it was the timing, but if that's the case we shouldn't see such a big difference between the daily numbers added up compared to a monthly statistic report.

— csmiley@leegov.com 3 years ago

<a id="commentAns_1081"></a>[Add a Comment](#)

<a id="upVoteAns_1080"></a>[](#)

0

<a id="downVoteAns_1080"></a>[](#)

It's hard to say without seeing the SQL for each.  Usually you can see it by running the report then viewing source (in Chrome or Firefox you'd right-click to then might have to confirm form re-submission; in Edge apparently you can't right-click there but it still honors CTRL+U to bring it up).  Once you have the page source you'd scroll down or CTRL+F for "SQLStatementHide."  The SQL which generated the report should be there in that tag's value, looking something like "select t.org" etc.

Are you able to pull those queries up?  If so, could you post them?

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_b28ae700542442e6ac089f31037d507b.jpg"/> jjack@aclib.us

- <a id="acceptAnswer_1080"></a>[Accept as answer](#)

Ah. I think I see part of the issue. One is running against the transacting branch, the other agains assigned branch. And I feel like I should have known that because that has come up before when there are discrepancies. I'll try and attach those statements I got from the page source so you can take a look.

But still - it seems like such a large discrepancy at the end of the month. Wouldn't the totals be closer? I know they'll never be exact, stats never are. And managers want to know clear cut answers to these things.

— csmiley@leegov.com 3 years ago

<a id="commentAns_1080"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Simply reports patron services](https://iii.rightanswers.com/portal/controller/view/discussion/355?)
- [Simply Reports - formatting when comments fields are included.](https://iii.rightanswers.com/portal/controller/view/discussion/128?)
- [SQL or Simply Report Active Patrons](https://iii.rightanswers.com/portal/controller/view/discussion/1164?)
- [Do reporting by subject headings?](https://iii.rightanswers.com/portal/controller/view/discussion/1059?)

### Related Solutions

- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [Run SimplyReports Scheduled Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930277327637)
- [Question about a saved SimplyReports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930457380859)
- [Question about published SimplyReports reports](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989986518)
- [Simply Reports Overview](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160908151835082)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)