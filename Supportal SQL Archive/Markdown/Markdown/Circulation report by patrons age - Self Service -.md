Circulation report by patrons age - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Circulation report by patrons age

0

85 views

Originally posted by janus.l.lindholm@ci.eugene.or.us on Wednesday, March 15th 20:47:53 EDT 2017
One of my staff is filling out a survey that wants to know the number of checkouts/renewals made by teens over a years time. I'm looking at March 2016 to March 2017 and ages 13 - 17. Any ideas? Note that teens do not have their own patron code, so I'm looking at Birth Date in patron registration. We are on Polaris 5.0 build 385.

asked 4 years ago  by <img width="18" height="18" src="../../_resources/4e9dc27b85164c109b5869d792254d0b.jpg"/>  support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 4 years ago

<a id="upVoteAns_93"></a>[](#)

0

<a id="downVoteAns_93"></a>[](#)

I take most of my information from the Transaction database as the Item Record History table only stores the last years worth of circulation.  All of the inforamtion is stored in it you just need to get familliar with how the inforamtion is stored.  It was unweildly at first, but I became familiar with it in a few weeks.

If you are interested I can break the tables down for you, and how you can get information of of them fast.

Let me know.

Vincent Kruggel

vkruggel@orl.bc.ca

answered 4 years ago  by <img width="18" height="18" src="../../_resources/4e9dc27b85164c109b5869d792254d0b.jpg"/>  vkruggel@orl.bc.ca

- <a id="acceptAnswer_93"></a>[Accept as answer](#)

<a id="commentAns_93"></a>[Add a Comment](#)

<a id="upVoteAns_77"></a>[](#)

0

<a id="downVoteAns_77"></a>[](#)

I put something like this together but had a lot of caveats (as far as I could tell, it underreported by .87 percent):

- The system is storing numbers for circulation in several different places, and none of those circulation numbers match exactly what we had on the statistical reports.  We could not figure out the reason for the difference.
- You might get better results with the Transaction tables, but those are treacherous and extremely slow to work with.
- We had to exclude ephemeral items because those can't be tied to a patron's age.
- It's impossible to calculate the number of circs of OverDrive/OneClick titles by patrons of a certain age group because that info isn't stored permanently in Polaris and is not available to us through the vendor.  For us that meant excluding AssignedBranchID 20, but you might have to exclude them some other way.
- You'll want to double check that you are including all the ActionTakenIDs you want, and none you don't.
- There is no sanity check for birthdays.  Some of the patrons in your system might have birthdays in 1900 (and one of ours listed a birthday in 1753.)  Such patrons would obviously not be included as teens, even if they actually are.

SELECT irha.ActionTakenDesc, count(irha.ActionTakenDesc) AS '#'
FROM ItemRecordHistory irh WITH (NOLOCK)
LEFT JOIN PatronRegistration pr WITH (NOLOCK)
ON irh.PatronID = pr.PatronID
JOIN CircItemRecords cir WITH (NOLOCK)
ON irh.ItemRecordID = cir.ItemRecordID
JOIN ItemRecordHistoryActions irha WITH (NOLOCK)
ON irh.ActionTakenID = irha.ActionTakenID
WHERE pr.Birthdate IS NOT NULL
AND ((CONVERT(int,CONVERT(char(8),irh.TransactionDate,112))-CONVERT(char(8),pr.Birthdate,112))/10000) >= @FromAge
AND ((CONVERT(int,CONVERT(char(8),irh.TransactionDate,112))-CONVERT(char(8),pr.Birthdate,112))/10000) <= @ThroughAge
AND irh.ActionTakenID IN (13,28,44,45,73,74,75,76,77,80,81,82,89,90,91,93)
--78,79, (Checked out - PAC / Mobile PAC)
AND cir.AssignedBranchID NOT IN (20)
AND irh.TransactionDate > @StartDate
AND irh.TransactionDate <= @LastIncludeddDate + ' 11:59:59 PM'
GROUP BY irha.ActionTakenDesc
ORDER BY irha.ActionTakenDesc

John / Alachua County Library District

answered 4 years ago  by <img width="18" height="18" src="../../_resources/4e9dc27b85164c109b5869d792254d0b.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_77"></a>[Accept as answer](#)

<a id="commentAns_77"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Simply Reports Discrepancies](https://iii.rightanswers.com/portal/controller/view/discussion/960?)
- [Simply reports patron services](https://iii.rightanswers.com/portal/controller/view/discussion/355?)
- [Searching for Items that have not circ'd in 2 years ex. DVD's](https://iii.rightanswers.com/portal/controller/view/discussion/1053?)

### Related Solutions

- [Question about Circulation by Postal Code report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930374734874)
- [Where to find Patron Circ Count in Patron record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930283871188)
- [Statistics for Holds and ILLs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930628713826)
- [Are in-house check-ins included in canned circulation reports?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930566498391)
- [Item Circulation Statistics Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930404036798)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)