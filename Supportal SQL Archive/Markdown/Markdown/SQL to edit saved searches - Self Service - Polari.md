[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL to edit saved searches

0

71 views

Hi All,

We have several saved searches that are failing because the patrons used quotes when building the search - for example  brsn="Fiction" - which will return the same results in the original search as  brsn=fiction, but when saved, the search will fail.  

I can find all the failed searches through SQL in SDIHeaders, but I don't know the SQL commands to edit/replace the search statement.

\- Robin

asked 3 years ago by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG) rdye@krl.org

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 3 years ago

<a id="upVoteAns_1030"></a>[](#)

0

<a id="downVoteAns_1030"></a>[](#)

Sorry to go sideways on this but it is the only post even remotely related.

Is there a way to trigger a saved search to run on command or change the calculated next run date?

I am troubleshooting an unusual non-delivery issue and it would be helpful for testing to have this capability.

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_6768ee0d24074de286180b7dbbf1ac5f.jpg"/> fbourne@sjlib.org

- <a id="acceptAnswer_1030"></a>[Accept as answer](#)

<a id="commentAns_1030"></a>[Add a Comment](#)

<a id="upVoteAns_1023"></a>[](#)

0

<a id="downVoteAns_1023"></a>[](#)

Thanks Rex!  (That does work but in the meantime I figured out the quotes weren't the real problem.)![smile](../_resources/smiley-smile_69cb0469933f40e8a516ab5890bf93ea.gif)

\- Robin 

answered 3 years ago by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG) rdye@krl.org

- <a id="acceptAnswer_1023"></a>[Accept as answer](#)

<a id="commentAns_1023"></a>[Add a Comment](#)

<a id="upVoteAns_1022"></a>[](#)

0

<a id="downVoteAns_1022"></a>[](#)

Let me try this again since I deleted my previous comment.

After checking our DB we don't have any brsn searches saved so you'll need to test this for yours.

&nbsp;

\--Search for brsn searches with "

&nbsp;

select \* from SDIHeader where SearchCriteria like '%brsn="%'

begin tran  
UPDATE SDIHeader  
SET SearchCriteria = REPLACE(SearchCriteria, '"', '')  
WHERE SDISearchID IN (select SDISearchID from SDIHeader where SearchCriteria like '%brsn=""%')

\-- rollback or commit

&nbsp;

Rex Helwig  
Finger Lakes Library System  
[rhelwig@flls.org](mailto:rhelwig@flls.org)

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_6768ee0d24074de286180b7dbbf1ac5f.jpg"/> rhelwig@flls.org

- <a id="acceptAnswer_1022"></a>[Accept as answer](#)

<a id="commentAns_1022"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for MARC tag search](https://iii.rightanswers.com/portal/controller/view/discussion/194?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [Searching for Items that have not circ'd in 2 years ex. DVD's](https://iii.rightanswers.com/portal/controller/view/discussion/1053?)
- [Is it possible to save a record set when running a saved report in Simply Reports?](https://iii.rightanswers.com/portal/controller/view/discussion/979?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)

### Related Solutions

- [Saved Searches emails are not being received](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930389681864)
- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Sort order for content carousels](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930381469391)
- [Links in saved search e-mail not working](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930268629338)
- [What does SDI stand for?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930683786942)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)