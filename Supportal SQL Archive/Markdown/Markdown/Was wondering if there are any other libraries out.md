[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [General Announcements](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=55)

# Was wondering if there are any other libraries out there that have gone from a turn key environment to a hosted environment and if so some of the benefits and drawbacks associated with the switch?

0

37 views

We currently use XenServer coupled with XenCenter to manage our server environment. We have one physical server with our DC, SQL & TS being virtualized. Thinking about making the switch over to a cloud hosted environment, but would like to get some feedback from other libraries that have made the switch and see what their comments are.

asked 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_7ae3db8d492a492c99b3dccf5b17af7c.jpg"/> fmiller@auglaizelibraries.org

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Job Posting - Library Application Support Manager Salt Lake County Libraries](https://iii.rightanswers.com/portal/controller/view/discussion/951?)
- [Academic libraries and corsortia with academic libraries: Idea Lab challenge](https://iii.rightanswers.com/portal/controller/view/discussion/373?)
- [Job Posting: Saskatchewan Information and Library Services Consortium - Integrated Library System Administrator (telecommute, 1 year term position)](https://iii.rightanswers.com/portal/controller/view/discussion/152?)
- [Job Posting - Branch Coordinator II - Mesa Public Library](https://iii.rightanswers.com/portal/controller/view/discussion/1056?)
- [Job Posting - Librarian II Technical Services - Duluth Public Library](https://iii.rightanswers.com/portal/controller/view/discussion/1169?)

### Related Solutions

- [Polaris Integration with EContent Guide](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170419085906750)
- [Converting non-integrated with resource entities to integrated](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930788012745)
- [How are multiple material types handled in the e-book integration?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930542419146)
- [What type of SSL certificate is needed for Cloud Integration?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930937483909)
- [3M integrated ebook import stuck in Running status](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930904762065)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)