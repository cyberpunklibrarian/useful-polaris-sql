[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Leap](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=13)

# Leap Offline Procedures

0

238 views

Is anyone using Leap Offline from 6.3? We are upgrading this weekend and have been trying out and testing new features in our testing environment. We would like to introduce Leap Offline but are stuck on the process. We've followed the steps from the What's New document, but seem to stop at Uploading Offline Transactions. After performing several transactions in the offline installation, we go back to the test site and upload the transactions. From there we don't see an option to see any reports like you get from the offline client.

Shouldn't there be a report of the transactions and any possible errors? I also kept track of the transactions and checked those patron records but don't see anything. Does this have to be in a production environment instead of testing?

I fee like maybe we're missing something very obvious and just aren't seeing it. Could anyone who is successfully using it share some advice?

&nbsp;

Thanks,

Cecilia Smiley

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_463f04a52b494a1d8cd9998a946597ca.jpg"/> csmiley@leegov.com

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 4 years ago

<a id="upVoteAns_760"></a>[](#)

1

<a id="downVoteAns_760"></a>[](#)

You have to run a SQL job called "Automatic Offline Circulation Upload Processing".  It was originally introduced to process offline files from the desktop client, but in addition to slurping up files, it runs the transactions.  Make sure the Parameter > Patron Services > Offline (Automated): Polaris user for automatic upload process is set to PolarisExec.

&nbsp;

If you don't have access to the SQL Job Activity Monitor, your site manage should be able to run the job for you. THis job has to be run whenever you want to process offline stuff into the real world. 

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_463f04a52b494a1d8cd9998a946597ca.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_760"></a>[Accept as answer](#)

It looks like you also have to set the "Offline (Automated): Enable automatic offline uploading" to Yes.

Rex

&nbsp;

— rhelwig@flls.org 4 years ago

<a id="commentAns_760"></a>[Add a Comment](#)

<a id="upVoteAns_812"></a>[](#)

0

<a id="downVoteAns_812"></a>[](#)

Innovative published a new article about Leap processing: [https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?ismodal=true&solutionid=190807164520143](https://iii.rightanswers.com/portal/controller/view/discussion/../../../app/portlets/results/viewsolution.jsp?ismodal=true&solutionid=190807164520143)

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_463f04a52b494a1d8cd9998a946597ca.jpg"/> wosborn@clcohio.org

- <a id="acceptAnswer_812"></a>[Accept as answer](#)

<a id="commentAns_812"></a>[Add a Comment](#)

<a id="upVoteAns_761"></a>[](#)

0

<a id="downVoteAns_761"></a>[](#)

Thanks for the quick reply Trevor. That explains everything. I'll talk to my site manager, but on the other hand we have a lot of stability now and very rarely use offline anyway. I hope that continues!

&nbsp;

Thanks, Cecilia

&nbsp;

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_463f04a52b494a1d8cd9998a946597ca.jpg"/> csmiley@leegov.com

- <a id="acceptAnswer_761"></a>[Accept as answer](#)

<a id="commentAns_761"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Are you using Leap Offline?](https://iii.rightanswers.com/portal/controller/view/discussion/612?)
- [Can you register patrons in leap offline? If not, are there plans to do this later?](https://iii.rightanswers.com/portal/controller/view/discussion/713?)
- [LEAP for Dummies or LEAP 101](https://iii.rightanswers.com/portal/controller/view/discussion/911?)
- [Leap and RFID](https://iii.rightanswers.com/portal/controller/view/discussion/427?)
- [Payments in Leap](https://iii.rightanswers.com/portal/controller/view/discussion/50?)

### Related Solutions

- [Safari cap on writable storage](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200612160131423)
- [Leap offline overview and FAQ](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=190807164520143)
- [Adding Leap permissions en mass](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930306603411)
- [Leap resource guide](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210217105146847)
- [What is required for Leap?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930646331533)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)