Need help with query to find checked out titles with holds - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Need help with query to find checked out titles with holds

0

26 views

I'm trying to find bibs that have holds and whose items have been checked out for 60 days or more.  How do I keep this from giving me false positives?  My query as it is now will give a bib record if any of the attached items have been checked out for 60 days, not if ALL of them have been.  Any help would be much appreciated.  Here's what I've got right now:

SELECT distinct br.BibliographicRecordID as RecordID, br.BrowseTitle, br.BrowseAuthor, cs.Abbreviation as \[Collection\], rw.NumberActiveHolds \[# Holds\], rw.NumberofItems as \[# Items\], cir.ItemStatusDate
FROM polaris.polaris.BibliographicRecords BR (NOLOCK)
LEFT OUTER JOIN polaris.polaris.RWRITER_BibDerivedDataView RW (NOLOCK)
ON BR.BibliographicRecordID = RW.BibliographicRecordID
inner join polaris.polaris.CircItemRecords cir with (nolock)
on cir.AssociatedBibRecordID = br.BibliographicRecordID
inner join polaris.polaris.Collections cs with (nolock)
on cir.AssignedCollectionID = cs.CollectionID
inner join polaris.polaris.SysHoldRequests shr with (nolock)
on br.BibliographicRecordID = shr.BibliographicRecordID
WHERE RW.NumberOfItems <= 3
AND RW.NumberActiveHolds > 0
AND BR.RecordStatusID = 1
AND cir.ItemStatusID = 2
AND cir.ItemStatusDate <dateadd(day, -60, getdate())
order by cs.Abbreviation asc, BrowseTitle

asked 4 days ago  by <img width="18" height="18" src="../../_resources/75385639f10b4581937182aacb14142b.jpg"/>  kmarshall@leegov.com

[holds](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=96#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 4 days ago

<a id="upVoteAns_1168"></a>[](#)

0

<a id="downVoteAns_1168"></a>[](#)

I looked at your query, and I think it's giving you false positives because your condition of out more than 60 days is in the WHERE statement being applied to each row, but, for your purposes, it should be in the HAVING statement being applied to each grouping. I added a GROUP BY and HAVING to your query (and made a bunch of the SELECT lines aggregate functions). I also removed the holds table since you're pulling the number of holds from the derived data view, and it was returning too many rows. Here is what I got:

SELECT
   br.BibliographicRecordID as RecordID
   , MAX(br.BrowseTitle) as Title
   , MAX(br.BrowseAuthor) as Author
   , MAX(cs.Abbreviation) as \[Collection\]
   , MAX(rw.NumberActiveHolds) \[# Holds\]
   , MAX(rw.NumberofItems) as \[# Items\]
   , MAX(cir.ItemStatusDate) as StatusDate
FROM polaris.polaris.BibliographicRecords BR 
   LEFT OUTER JOIN polaris.polaris.RWRITER_BibDerivedDataView RW 
   ON BR.BibliographicRecordID = RW.BibliographicRecordID
   INNER JOIN polaris.polaris.CircItemRecords cir  
   ON cir.AssociatedBibRecordID = br.BibliographicRecordID
   INNER JOIN polaris.polaris.Collections cs 
   ON cir.AssignedCollectionID = cs.CollectionID
WHERE RW.NumberOfItems <= 3
   AND RW.NumberActiveHolds > 0
   AND BR.RecordStatusID = 1
GROUP BY br.BibliographicRecordID
HAVING SUM(CASE WHEN cir.ItemStatusID = 2 THEN 1 ELSE 0 END) = MAX(RW.NumberofItems)
   AND SUM(CASE WHEN cir.ItemStatusDate <dateadd(DD, -60, getdate()) THEN 1 ELSE 0 END) =  MAX(RW.NumberofItems)
ORDER BY 4,2

Run against our DB, it doesn't produce too many results (items with holds can't be renewed, and items that are 'out' too long go from 'out' to 'lost'), but all of the bibs it turns up with more than 1 linked item have all items 'out' for more than 60 days so this might be what you're looking for.

Good luck!

     --Daniel

answered 4 days ago  by <img width="18" height="18" src="../../_resources/75385639f10b4581937182aacb14142b.jpg"/>  ddunphy@aclib.us

- <a id="acceptAnswer_1168"></a>[Accept as answer](#)

This is great, thank you!!

— kmarshall@leegov.com 1 day ago

<a id="commentAns_1168"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [I need to pull OCLC numbers from records with a deleted status to batch update our OCLC holdings. I'm able to get a report with: Bib record ID, Title, and Bib record status, but the column for MARC OCLC number is blank. Why doesn't it show up?](https://iii.rightanswers.com/portal/controller/view/discussion/829?)
- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [SQL to Find all bibs with same title.](https://iii.rightanswers.com/portal/controller/view/discussion/675?)
- [SQL help for for circ info on 'new' stuff](https://iii.rightanswers.com/portal/controller/view/discussion/698?)
- [Average hold wait times](https://iii.rightanswers.com/portal/controller/view/discussion/845?)

### Related Solutions

- [Query to find unfillable holds due to volume data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930242167096)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Linking holds on multiple bib records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930439017974)
- [Holds Not Trapping at Self-Check](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930897357365)
- [Can we suspend the hold option for specific titles?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930255045537)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)