SQL top bib checkouts between 2 dates - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL top bib checkouts between 2 dates

0

61 views

Hello-

I am trying to find the 'Top 10" checkouts for a specific location between 2 dates.  Since they are not asking for specific items, I am looking at bibs.  Renewal count should not be included.  I apologize if this has been asked but I searched the forums and couldn't find anything.

Thanks your time is appreciated!

Kimberly Hunter

Black Gold Libraries

asked 1 year ago  by <img width="18" height="18" src="../../_resources/94691e84f5964cd89a8974b02d8d122b.jpg"/>  khunter@blackgold.org

[bibs](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=312#t=commResults) [checkouts](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=313#t=commResults) [count](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=314#t=commResults) [patron](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=315#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_786"></a>[](#)

0

<a id="downVoteAns_786"></a>[](#)

Thanks so much!  You are awesome!

answered 1 year ago  by <img width="18" height="18" src="../../_resources/94691e84f5964cd89a8974b02d8d122b.jpg"/>  khunter@blackgold.org

- <a id="acceptAnswer_786"></a>[Accept as answer](#)

<a id="commentAns_786"></a>[Add a Comment](#)

<a id="upVoteAns_785"></a>[](#)

0

<a id="downVoteAns_785"></a>[](#)

Hi Kimberly.

Here is  a query based on one I created a while ago. it uses the ItemRecordHistory table to get checkouts. If you do ever want to include renewals, I will put a separate string that includes all types of renewals below the query. I used two TransactionDate conditions to get all transactions from after midnight on the first date to the last transaction before the last date.

There are conditions for collections and statistical codes. They can be removed if you don't want them.

One last thing: ItemRecordHistory does not count transactions on the day you run the query (ItemRecordHistoryDaily does this and that data is transferred to ItemRecordHistory overnight) so as long as the last date you use isn't tomorrow's date, you should be fine.

Hope this helps,

JT

SELECT
br.BibliographicRecordID
,br.BrowseCallNo
,br.BrowseAuthor
,br.BrowseTitle
,COUNT(irh.ItemRecordHistoryID) as Ckouts
FROM Polaris.Polaris.Bibliographicrecords br WITH (NOLOCK)
JOIN Polaris.Polaris.ItemRecords ir (NOLOCK)
ON (br.BibliographicRecordID = ir.AssociatedBibRecordID)
JOIN Polaris.Polaris.ItemRecordHistory irh (NOLOCK)
ON (ir.ItemRecordID = irh.ItemRecordID and irh.ActionTakenID in (13,75,77,81,89,91))

WHERE irh.TransactionDate >= '11/01/2019'
AND irh.TransactionDate < '12/01/2019'
AND irh.OrganizationID = 11
AND ir.AssignedCollectionID in (2,6)
AND ir.StatisticalCodeID in (1,2,3,4,5,6,7,8,9,11)

GROUP BY
br.BibliographicRecordID
,br.BrowseCallNo
,br.BrowseAuthor
,br.BrowseTitle

ORDER BY
COUNT(irh.ItemRecordHistoryID) DESC
,br.BrowseCallNo
,br.BrowseTitle
,br.BrowseAuthor

-----If you want to include renewals, use this string in the join to ItemRecordHistory:

ON (ir.ItemRecordID = irh.ItemRecordID and irh.ActionTakenID in (13,75,77,81,89,91,73,76,80,93))

answered 1 year ago  by <img width="18" height="18" src="../../_resources/94691e84f5964cd89a8974b02d8d122b.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_785"></a>[Accept as answer](#)

<a id="commentAns_785"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Checkout length by checkin date](https://iii.rightanswers.com/portal/controller/view/discussion/877?)
- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [SQL for bibs with holds and no owned items by branches](https://iii.rightanswers.com/portal/controller/view/discussion/821?)
- [SQL to Find all bibs with same title.](https://iii.rightanswers.com/portal/controller/view/discussion/675?)

### Related Solutions

- [You Saved Message - What is the date range for YTD savings?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930616286891)
- [Circulation statistics and deleted records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930736904760)
- [How are due dates calculated?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930778190992)
- [What is the patron age update step in the patron processing job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930331250831)
- [Which line of the Loan Periods table is used?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170413110944913)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)