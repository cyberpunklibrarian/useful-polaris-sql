[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Wondering if anyone's come up with a good way to manage items that are in a temp. location/shelf loc.? I want items to keep temp/shelf loc. after checkin & have an exp. date that will make them show up on pull list so they can be returned to perm. loc.

0

92 views

Hi, I'm new to Polaris and am wondering if anyone has come up with a good way to manage items on display in temporary locations or shelf locations?   What I'd like to accomplish is:

- Items on display should keep their temp. locations or shelf locations even after checkout & checkin
- Staff should be able to set an expiration date on the temp. location/shelf location within the item record so that when that date arrives, the items will show up on the next day's pull list and direct staff to scan items and return them to their permanent location.

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_c9ca9798e0904262bc1113c583b4f01e.jpg"/> cheryl.nessman@meadpl.org

[temporary location shelf location display](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=329#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 4 years ago

<a id="upVoteAns_827"></a>[](#)

0

<a id="downVoteAns_827"></a>[](#)

Hi Cheryl,

I'm not sure if there is a better way, but we do a couple of things. We have display shelf locations set up as well as a New arrivals shelf location and these act as temporary locations for us.

For display items, we add items to record sets along with a note (this may indicate when to pull the items and change the shelf location ex. Staff picks Jan) or we may just indicate this with the record set name ex. 2020/01 Read the book, watch the movie display. Since we do new displays each month this is an easy way to track things when searching by record set name (you can also search by free text note). 

We use bulk change to update the shelf locations and remove the notes so it is relatively fast. We also have SQL reports that run regularly to catch items that need to come off the New arrivals shelf. This is based on the first available date. These reports are directly saved to our CX (Customer Experience) drive for each branch and staff pull these lists as time permits, though it is weekly minimally or whenever our shelves get too full.

Once these items are pulled our CX staff update the shelf location at check-in which they can do using the Manage items option, though certain staff have bulk change permission and can do that instead depending on how many items they need to update.

&nbsp;

Hope that helps, but if you have any more questions you can email me at [mwilliams@coqlibrary.ca](mailto:mwilliams@coqlibrary.ca) :)

Best,

Melani

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_c9ca9798e0904262bc1113c583b4f01e.jpg"/> mwilliams@coqlibrary.ca

- <a id="acceptAnswer_827"></a>[Accept as answer](#)

<a id="commentAns_827"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Changing offensive LOC subject terms to local subject terms](https://iii.rightanswers.com/portal/controller/view/discussion/879?)
- [Delete a collection, stat code, or Shelf code from drop down list on item records](https://iii.rightanswers.com/portal/controller/view/discussion/402?)
- [How should I be entering call number info in the 092 to make the subfields correctly correspond with item record call number fields?](https://iii.rightanswers.com/portal/controller/view/discussion/313?)
- [How do you purge withdrawn items](https://iii.rightanswers.com/portal/controller/view/discussion/186?)
- [Deleting item records with holds](https://iii.rightanswers.com/portal/controller/view/discussion/32?)

### Related Solutions

- [What Information Should I Use for Library of Congress?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930355289656)
- [Where does the SimplyReports target audience filter pull from?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930728087303)
- [How to take a collection inventory using Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930994460225)
- [What is the difference between Creation Date and Acquisition Date?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930268866818)
- [No borrower listed in item record history](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930633020928)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)