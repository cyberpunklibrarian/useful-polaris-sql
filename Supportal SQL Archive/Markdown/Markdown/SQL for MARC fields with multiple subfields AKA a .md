[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# SQL for MARC fields with multiple subfields AKA a subfield string

0

100 views

Dear collective wisdom, does anyone know a SQL to find a MARC field string in bibs?

For example, say I want to search for ***650 \\7 $aColoring books$2lcgft***.

Right now, my SQLs find bibs that have two 650's, with coloring books in one 650 and lcgft in another. In other words:

*650 \\0 $aColoring books.*

*650 \\7 $aActivity books.$2lcgft*

&nbsp;

asked 2 years ago by ![marikok@wccls.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_marikok@wccls.org20240119162535.jpg) marikok@wccls.org

[bibliographic records](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=442#t=commResults) [marc](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=79#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_1195"></a>[](#)

0

<a id="downVoteAns_1195"></a>[](#)

Answer Accepted

The following will only return records where your two search terms are in the same MARC field:

select distinct bt.BibliographicRecordID from polaris.polaris.bibliographictags bt with (nolock)  
inner join polaris.polaris.bibliographicsubfields bs1 with (nolock)  
on bt.bibliographictagid = bs1.BibliographicTagID  
inner join polaris.polaris.bibliographicsubfields bs2 with (nolock)  
on bt.bibliographictagid = bs2.bibliographictagid  
where bs1.data like 'thrillers (fiction)%'  
and bs2.data like 'lcgft%'

If you want the subfields right next to each other in a specific order, that's a lot trickier. The following will find *most* instances of this, but miss a few since the subfieldids are not always sequential.

select distinct bt.BibliographicRecordID from polaris.polaris.bibliographictags bt with (nolock)  
inner join polaris.polaris.bibliographicsubfields bs1 with (nolock)  
on bt.bibliographictagid = bs1.BibliographicTagID  
inner join polaris.polaris.bibliographicsubfields bs2 with (nolock)  
on bt.bibliographictagid = bs2.bibliographictagid  
where bs1.data like 'thrillers (fiction)%'  
and bs2.data like 'lcgft%'  
and bs2.BibliographicSubfieldID - bs1.bibliographicsubfieldid = 1

HTH.

Paul  
\___\___\___\___\___\___  
Paul Keith  
Chicago Public Library

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_5f99c1073225466dad716467d922dbc4.jpg"/> pkeith@chipublib.org

Thanks a million, Paul! I just gave it a try and it seems to be doing the trick![cool](../_resources/smiley-cool_b394e8bcaa9b42878081abb0759fc216.gif)

— marikok@wccls.org 2 years ago

<a id="commentAns_1195"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Authority subfield mixup](https://iii.rightanswers.com/portal/controller/view/discussion/1253?)
- [How should I be entering call number info in the 092 to make the subfields correctly correspond with item record call number fields?](https://iii.rightanswers.com/portal/controller/view/discussion/313?)
- [alternate title/author fields](https://iii.rightanswers.com/portal/controller/view/discussion/304?)
- [Duplicate ISBN in a MARC](https://iii.rightanswers.com/portal/controller/view/discussion/819?)
- [Accession Numbers](https://iii.rightanswers.com/portal/controller/view/discussion/1029?)

### Related Solutions

- [Database tables for Keyword and Browse indexing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930397363904)
- [856 tag display in the PowerPAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930661861606)
- [Item Record Shelving Scheme updated even though 852 tag did not include a Shelving Scheme subfield](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170227150057532)
- [Remove subfield e from 100 tag for RDA records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930517311975)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)