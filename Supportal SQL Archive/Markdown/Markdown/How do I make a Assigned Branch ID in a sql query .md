[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# How do I make a Assigned Branch ID in a sql query be a name and not a ID number?

0

89 views

I'm trying to write query where the Assigned Branch ID is not a number but the item's assigned branch name.   For instance, assigned branch ID #3 is for our Central branch. How do I get the query row in the assigned branch column to say Central instead of 3?

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_44286e58d3d54f6f8afca0a5b05f0082.jpg"/> lori@esrl.org

[sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 4 years ago

<a id="upVoteAns_825"></a>[](#)

0

<a id="downVoteAns_825"></a>[](#)

Answer Accepted

Hi Lori (guessing your name by your email, so sorry if I messed it up).

What you need to do is join the item table (CircItemRecords or ItemRecordDetails) or the ItemRecords view (basically combines the two tables) to the Organizations table and select the name or abbreviation from that table.

For example:

SELECT  
ci.Barcode,  
org.Name

FROM Polaris.Polaris.CircItemRecords ci WITH (NOLOCK)

JOIN Polaris.Polaris.Organizations org (NOLOCK)

ON (ci.AssignedBranchID = org.OrganizationID)

WHERE...

Hope this helps,

JT

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_44286e58d3d54f6f8afca0a5b05f0082.jpg"/> jwhitfield@aclib.us

Thank you.  This is big help.

— lori@esrl.org 4 years ago

How would I also change MaterialType ID to Book, DVDd, etc.?

— lori@esrl.org 4 years ago

Hi Lori.

There is a table named MaterialTypes. You can...

JOIN Polaris.Polaris.MaterialTypes mt (NOLOCK)

ON (ci.MaterialTypeID = mt.MaterialTypeID)

In your select statement, you can select

,mt.Description AS MaterialType (or whatever you want the column to be named)

Hope this helps.

JT

— jwhitfield@aclib.us 4 years ago

<a id="commentAns_825"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [How to find my Institution ID?](https://iii.rightanswers.com/portal/controller/view/discussion/1060?)
- [I need to pull OCLC numbers from records with a deleted status to batch update our OCLC holdings. I'm able to get a report with: Bib record ID, Title, and Bib record status, but the column for MARC OCLC number is blank. Why doesn't it show up?](https://iii.rightanswers.com/portal/controller/view/discussion/829?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [SQL for circulation numbers](https://iii.rightanswers.com/portal/controller/view/discussion/94?)
- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Why is the EDI Agent job continuously running?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930649540631)
- [Collection workform "branches" checkboxes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930545936496)
- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)
- [Export from old Supportal of forum posts with SQL queries](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161214213310321)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)