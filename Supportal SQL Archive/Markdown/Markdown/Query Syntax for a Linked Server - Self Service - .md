[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Query Syntax for a Linked Server

0

15 views

Does anyone know the correct syntax for including a linked server in a query?

I set up a linked server and tested the connection which returned a successful response.

In the attached query, I get

"Msg 208, Level 16, State 1, Line 2  
Invalid object name 'snoislesrv\\ils-prod-c.windows.sno-isle.org.Polaris.Patrons'."

&nbsp;

Thank you.

Jon Lellelid

jlellelid@sno-isle.org

asked 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_1ad078fc5cf1464e9679b557fe3e5e0f.jpg"/> jlellelid@sno-isle.org

- [SnoIsle Query with Linked Server.txt](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/491?fileName=491-SnoIsle+Query+with+Linked+Server.txt "SnoIsle Query with Linked Server.txt")

[linked server](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=255#t=commResults) [polaris](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=7#t=commResults) [sql server](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=256#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Server Busy](https://iii.rightanswers.com/portal/controller/view/discussion/278?)
- [Setting up a DNS ALIAS for Testing a server migration](https://iii.rightanswers.com/portal/controller/view/discussion/690?)
- [Experiences with Windows Firewall on Polaris Server](https://iii.rightanswers.com/portal/controller/view/discussion/446?)
- [Consortium Circulation parameter in SA not working how I expect in test server](https://iii.rightanswers.com/portal/controller/view/discussion/709?)
- [SQL or Scoping for Items Withdrawn NOT linked to patron fee records](https://iii.rightanswers.com/portal/controller/view/discussion/525?)

### Related Solutions

- [How to create a read only user for staff to use to query SQL Server Management Studio](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930482184973)
- [Automatically open SQL Server query window](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930908117973)
- [Running a SQL query](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930978843960)
- [Customizations not working in my XML document](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930797469887)
- [Debugging begins when executing a SQL query](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930385453783)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)