SQL for recent titles added - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for recent titles added

0

73 views

I have some really old SQLs that are no longer working. I have zero SQL knowledge and could use your help! If you can fix this one or send me a better solution, I would greatly appreciate it! End goal is to have a list of all adult books and media that have been fully cataloged -- we do not keep magazines, paperbacks, ILLs, or youth materials in the list. I can easily weed these things out though at the end before turning it in. I just can't get an accurate report of the titles within the quarter. HELP! ![innocent](../../_resources/a8901ace2ef5407382ae5a71fcc0f508.gif)

SELECT

      BR.BrowseTitle,

      BR.BrowseAuthor

      BR.BrowseCallNo

FROM BibliographicRecords BR

JOIN

CircItemRecords CIR ON CIR.AssociatedBibRecordID = BR.BibliographicRecordID

JOIN

      ItemRecordDetails IRD ON IRD.ItemRecordID = CIR.ItemRecordID

WHERE IRD.CreationDate BETWEEN '07/01/2019' AND '09/30/2019' AND

      IRD.CallNumberPrefix NOT IN ( ‘J%’,‘YA%’)

asked 2 years ago  by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg)  sbills@lpld.lib.in.us

[sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_708"></a>[](#)

0

<a id="downVoteAns_708"></a>[](#)

Answer Accepted

Hi.

Here is a query that worked on my end. I needed to add the database and schema (Polaris.Polaris.) to the tables and I put the text strings into two separate NOT LIKE statements. You may or may not need the database/schema part, or yours may be named slightly different.

Hope this helps.

JT

SELECT
      BR.BrowseTitle,
      BR.BrowseAuthor,
      BR.BrowseCallNo

FROM Polaris.Polaris.BibliographicRecords BR

JOIN

Polaris.Polaris.CircItemRecords CIR ON CIR.AssociatedBibRecordID = BR.BibliographicRecordID

JOIN

      Polaris.Polaris.ItemRecordDetails IRD ON IRD.ItemRecordID = CIR.ItemRecordID

WHERE IRD.CreationDate BETWEEN '07/01/2019' AND '09/30/2019' AND
      IRD.CallNumberPrefix NOT LIKE 'J%' AND
   IRD.CallNumberPrefix NOT LIKE 'YA%'

answered 2 years ago  by <img width="18" height="18" src="../../_resources/085c189266c6437887a750076cead0e1.jpg"/>  jwhitfield@aclib.us

One additional thing: I find that using BETWEEN with dates can be a bit tricky. Since CreationDate is a datetime, if you use a date without a time after the AND you may not get the entire last day (you may only get the first millisecond of that day). You can either use something like

WHERE IRD.CreationDate >= '07/01/2019'
AND IRD.CreationDate < '10/01/2019'

or something like

WHERE IRD.CreationDate BETWEEN '07/01/2019' AND '09/30/2019 23:59:59.999'

Hope this is also helpful.

JT

— jwhitfield@aclib.us 2 years ago

Oh, JT. You're the best! Thank you!!

— sbills@lpld.lib.in.us 2 years ago

<a id="commentAns_708"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL to Find all bibs with same title.](https://iii.rightanswers.com/portal/controller/view/discussion/675?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [Need help with query to find checked out titles with holds](https://iii.rightanswers.com/portal/controller/view/discussion/1063?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [Adding number of "live" items to a report](https://iii.rightanswers.com/portal/controller/view/discussion/666?)

### Related Solutions

- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [New Titles dashboard in PAC should not include all titles that were recently purchased](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930574262908)
- [Adding a new UDF to patron registration](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930740605846)
- [SysHoldRequest table missing Title information](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930733866097)
- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)