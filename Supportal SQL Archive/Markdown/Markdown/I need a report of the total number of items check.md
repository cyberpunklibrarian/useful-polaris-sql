I need a report of the total number of items checked out by user ID where the total number out is greater than 35 - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# I need a report of the total number of items checked out by user ID where the total number out is greater than 35

0

28 views

I need a report of the total number of items checked out by user ID where the total number out is greater than 35.

I don't know if this query is correct. I got it from SimplyReports. Are the right tables being usered?

select p.Barcode as PatronBarcode,pc.Description as PatronCodeDescr,count(*) As NumberOfItems
from Polaris.CircItemRecords cir with (nolock)
inner join Polaris.ItemRecordDetails ird with (nolock) on (cir.ItemRecordID = ird.ItemRecordID)
left join Polaris.ItemCheckouts icko with (nolock) on (cir.ItemRecordID = icko.ItemRecordID)
left join Polaris.Patrons p with (nolock) on (icko.PatronID = p.PatronID)
left join Polaris.PatronCodes pc with (nolock) on (p.PatronCodeID = pc.PatronCodeID)
where cir.AssignedBranchID in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,27,28,29,30,31)
and cir.ItemStatusID in (2)
and p.PatronCodeID in (1,8,22,23,6,7,11,19,17,18,15,16,12)
Group by p.Barcode,pc.Description
order by count(*) desc

asked 5 months ago  by ![carl.ratz@phoenix.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_carl.ratz@phoenix.gov20170808200600.png)  carl.ratz@phoenix.gov

[items out by user](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=413#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 5 months ago

<a id="upVoteAns_1084"></a>[](#)

0

<a id="downVoteAns_1084"></a>[](#)

I didn't see anything wrong with it offhand, though I did wonder why it woulddo a left join on ItemCheckouts then specify an ItemStatusID of 2 rather than just doing an inner join on ItemCheckouts (ItemStatusID 2 is "Out," but the ItemCheckouts table should only list items which currently *are* Out).  There might be a good reason to do it the way the softwarechose to, but the results should be the same either way and it's not a slow query so it probably doesn't matter.

I would caution, though, that this query will really only be reliable for physical materials; Polaris' handling of downloadable content is not completely reliablefor various reasons (some of which are out of Polaris' hands).  If you needed OverDrive/Hoopla/3M etc. figures added in in a way you can trust, this query below would only be a starting point (after excluding downloadables, if you haven't already done that by excluding Branch #26).

If you wanted to limitthe results on this to only the patrons with more than 35 items out, you could add a "having" clause above the last line: 

select p.Barcode as PatronBarcode,pc.Description as PatronCodeDescr,count(*) As NumberOfItems
from Polaris.CircItemRecords cir with (nolock)
inner join Polaris.ItemRecordDetails ird with (nolock) on (cir.ItemRecordID = ird.ItemRecordID)
left join Polaris.ItemCheckouts icko with (nolock) on (cir.ItemRecordID = icko.ItemRecordID)
left join Polaris.Patrons p with (nolock) on (icko.PatronID = p.PatronID)
left join Polaris.PatronCodes pc with (nolock) on (p.PatronCodeID = pc.PatronCodeID)
where cir.AssignedBranchID in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,27,28,29,30,31)
and cir.ItemStatusID in (2)
and p.PatronCodeID in (1,8,22,23,6,7,11,19,17,18,15,16,12)
Group by p.Barcode,pc.Description
Having count(*) > 35
order by count(*) desc

answered 5 months ago  by <img width="18" height="18" src="../../_resources/d0a7637fd2184586b9c748d165c5be2d.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_1084"></a>[Accept as answer](#)

<a id="commentAns_1084"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Reports for Floating Items](https://iii.rightanswers.com/portal/controller/view/discussion/62?)
- [Does anyone have a damaged item template report you are willing to share?](https://iii.rightanswers.com/portal/controller/view/discussion/956?)
- [Floating Reports](https://iii.rightanswers.com/portal/controller/view/discussion/363?)
- [Report for patrons with items out. If possible, sort by location?](https://iii.rightanswers.com/portal/controller/view/discussion/779?)
- [How do you run your weeding reports?](https://iii.rightanswers.com/portal/controller/view/discussion/464?)

### Related Solutions

- [Why is in-house check in not counting as circulation?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930877939075)
- [Credit Card reports at Self-Check](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930668080115)
- [Long Overdue Item report shows thousands of items](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930725698901)
- [Reports for On-the-Fly (OTF) items](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930655195362)
- [Statistics for Holds and ILLs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930628713826)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)