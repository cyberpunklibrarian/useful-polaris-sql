[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# I need a SQL for phatom lost item count attached to a patron's record.

0

55 views

I need help creating a SQL for phantom lost item counts attached to patron records.  These counts came over with our migration and they have no item information attached to them, and they cannot be reset to zero.  I'm trying to delete these patron's but the system thinks there are unbreakable links to items, and yet there is no item information attached to the account. 

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_530c5559bd1940e9ab4a1b9cfd77d5da.jpg"/> lori@esrl.org

[lost item count](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=386#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)
- [Way to allow staff to waive fines from Resolve Lost Item window but NOT from patron account?](https://iii.rightanswers.com/portal/controller/view/discussion/305?)
- [SQL for circ of items in record set, to include checkout info](https://iii.rightanswers.com/portal/controller/view/discussion/1122?)
- [Identifying a count of patrons, by registered library, whose only activity over a period of time is OverDrive checkout through Polaris integration?](https://iii.rightanswers.com/portal/controller/view/discussion/1044?)
- [Moving items to lost without billing](https://iii.rightanswers.com/portal/controller/view/discussion/1384?)

### Related Solutions

- [Items converting to Lost status](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930282614657)
- [Which lost item recovery settings are used for partially paid replacement costs?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170523113239517)
- [Paid lost item still displaying as unpaid](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930877186258)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [Default pricing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930543557097)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)