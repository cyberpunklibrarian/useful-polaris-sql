[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Retaining existing bib control number in 6.3

0

183 views

How does this work vis-a-vis the existing record?

Prior to 6.3 if an incoming record replaced an existing one, the existing record was marked as deleted and links (items, holds, etc.)  to the old BibliographicRecordID were updated to reflect the new one. The deleted record could be undeleted if it was incorrectly overlaid and the items/holds returned to the old record.

With 6.3, the incoming record retains the existing record's ID. What happens to the exising record? Does it get marked deleted with a new control number/BibliographicRecordID, or does it go away permanently? Is there an option?

Thanks for any insights you can provide.

JT

asked 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_cc4173f127994bf7998642719a32ac15.jpg"/> jwhitfield@aclib.us

<a id="comment"></a>[Add a Comment](#)

## 5 Replies   last reply 4 years ago

<a id="upVoteAns_612"></a>[](#)

0

<a id="downVoteAns_612"></a>[](#)

Answer Accepted

Hi JT - 

&nbsp;

Is the specific situations where the control number is maintained during overlay, the shell of the existing record doesn't go away, and links to items holds etc are not moved, because the record is now being modified, not deleted.  

In effect, the new record becomes the old one, and nothing is marked/deleted.  Feel free to email me any questions.  

&nbsp;

Thanks, Kellie

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_cc4173f127994bf7998642719a32ac15.jpg"/> kellie.conner@iii.com

Thank you Kellie! We just upgraded to 6.3 as well and I had the same question. 

— esloan@cityofboise.org 5 years ago

<a id="commentAns_612"></a>[Add a Comment](#)

<a id="upVoteAns_767"></a>[](#)

0

<a id="downVoteAns_767"></a>[](#)

We moved in November to 6.3 and this was one of the first things I noticed.  I also noted there was a checkbox to retain the bib number or not, but I was not able to procede if I unchecked the box.  Should I have been able to?  I have just about all permissions possible so if anyone in our setup were to be able to do this, it would be me.  It isn't any big deal just kind of baffled as to the reasoning for having a checkbox if unchecking it effectively cancels the transaction.

Thanks,

Jane DeBellis

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_cc4173f127994bf7998642719a32ac15.jpg"/> janed@santarosa.fl.gov

- <a id="acceptAnswer_767"></a>[Accept as answer](#)

<a id="commentAns_767"></a>[Add a Comment](#)

<a id="upVoteAns_766"></a>[](#)

0

<a id="downVoteAns_766"></a>[](#)

We moved in November to 6.3 and this was one of the first things I noticed.  I also noted there was a checkbox to retain the bib number or not, but I was not able to procede if I unchecked the box.  Should I have been able to?  I have just about all permissions possible so if anyone in our setup were to be able to do this, it would be me.  It isn't any big deal just kind of baffled as to the reasoning for having a checkbox if unchecking it effectively cancels the transaction.

Thanks,

Jane DeBellis

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_cc4173f127994bf7998642719a32ac15.jpg"/> janed@santarosa.fl.gov

- <a id="acceptAnswer_766"></a>[Accept as answer](#)

Hi Jane.

There seem to be some circumstances when the user is presented with the check box to retain the exising control number, but others when the user is not. I have a feeling that the appearance of the box mostly happens when saving a "new" record that matches one or more existing records. I have been checking the box when I have been copying a record for a multi-DVD set but just tried this without checking the box and it seems that it retained the existing control number anyway.  Odd.

I also tried saving a new recordd one where there was only one matching DVD (I just copied that bib). In this case, the box was checked and I could not uncheck it. Once I saved the new bib as final, I did not get the check box either by checking for duplicates or by saving the record.

If you can uncheck the box with a single match (if the box appears), there may be some permission that is different from our setup.

This may be something to contact Poaris/III about. If you get any news you can share, please do.

JT

&nbsp;

— jwhitfield@aclib.us 4 years ago

<a id="commentAns_766"></a>[Add a Comment](#)

<a id="upVoteAns_711"></a>[](#)

0

<a id="downVoteAns_711"></a>[](#)

Hi Tamar.

I created a report that uses data from the import jobs tables to compare the new title with the old one. (The old browse title is stored in one of those tables.) If there is a mismatch, I pull bib data from our training server (refreshed weekly) and replace the data in our production server. Since the refresh day is Sunday, I have to check on import jobs before the end of the day Friday--I usually do it right after the import job finishes.

We set up a Z39.50 connection from our production server to our training server, so I can search for the bib control number with the Find Tool Databases option set to search the training server. (This avoids confusion that may be caused by having two staff client sessions open at the same time.)

I can share the .rdl file if desired, but the report formatting may not be as "perfect" as I would like. I can also post the SQL query.

There is a hiccup with 6.3 that you may not have run across yet: If a bib being imported matches on a locked (open) bib record, Polaris does not create a provisional record. The report indicates that a provisional record was created in the Import Statistics section, but no record is actually created.  At least that's been my experience. I plan to submit a ticket on this to make sure there is not a setting somewhere that is "wonky."

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_cc4173f127994bf7998642719a32ac15.jpg"/> jwhitfield@aclib.us

- <a id="acceptAnswer_711"></a>[Accept as answer](#)

Thanks for this response and your solution. I've forwarded it on to our database admin person since it's out of my scope. We'll have to see what we can do when incorrect overlays happen. I haven't yet encountered trying to overlay a record that's open, but it's just a question of when, since it happens periodically, especially with popular titles that staff are putting holds on directly from the on-order record, instead of from the PAC. Anyway, thanks again. I'll be in touch directly if we have more questions here!

— tmiller@denverlibrary.org 4 years ago

I have the same question about how to backtrack to reversse an incorrect overlay situation.  I don't have access to the server, and we don't use a training server so I'm wondering if anyone has come up with any other options?

— lherring@cityofallen.org 4 years ago

There are two clues we've found so far: the import report will list the title you've overlayed, and the ISBN of the title you overlayed will be retained in the purchase order line item. That will at least identify the title, but the record will be lost, as far as I know. So far, we've only overlayed a title that was so close in number that it was on the same book truck, which was an easy fix.

— tmiller@denverlibrary.org 4 years ago

Thanks Tamar, I suppose it is better to know about this now so we can have realistic expections about how we can manage an incorrect merge.

— lherring@cityofallen.org 4 years ago

This may be more trouble than it's worth, but you could set the import profile to retain the 035 field. If you had a record for "That red book" with an OCLC number of 123 and unintentionally replaced it with data for "This green book" with an OCLC number of 456, you would end up with a record having 2 OCLC numbers and could use them to figure out what was replaced. If the incorrectly-updated record did not have an OCLC number (an on-order record?) using the PO information would help.

As I said, this may be more trouble than it's worth if you end up with records that were correctly updated, but have 2 different OCLC numbers. It may be worth a small-scale test to see if it causes excessive issues.

Hope this helps,

JT

— jwhitfield@aclib.us 4 years ago

<a id="commentAns_711"></a>[Add a Comment](#)

<a id="upVoteAns_710"></a>[](#)

0

<a id="downVoteAns_710"></a>[](#)

In the event you overlay the wrong record in Polaris 6.3, how do you backtrack from that error since there is no longer an overlaid record in deleted status?

Thanks,  
Tamar

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_cc4173f127994bf7998642719a32ac15.jpg"/> tmiller@denverlibrary.org

- <a id="acceptAnswer_710"></a>[Accept as answer](#)

<a id="commentAns_710"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Did upgrading to 6.3 bring a change to the way OCLC numbers are displayed in the 035?](https://iii.rightanswers.com/portal/controller/view/discussion/557?)
- [Update item record data using item control number as a match point?](https://iii.rightanswers.com/portal/controller/view/discussion/1396?)
- [Use Item Record Call Numbers to Bulk Update Bib MARC Tag](https://iii.rightanswers.com/portal/controller/view/discussion/235?)
- [Authority Control Vendors](https://iii.rightanswers.com/portal/controller/view/discussion/739?)
- [Accession Numbers](https://iii.rightanswers.com/portal/controller/view/discussion/1029?)

### Related Solutions

- [Bib Call Number is not automatically copying to Item Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930769128110)
- [Deleting and Purging Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930748692772)
- [Export OCLC Control Numbers from Polaris to update WorldCat Holdings using Simply Reports – Record Set Method](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181128101912576)
- [Copying bib call number to item record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930712730749)
- [How does "Use template values instead of these (if available)" work?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170524120618032)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)