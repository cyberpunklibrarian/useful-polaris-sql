Holds Purchase Alert but add Publication Year - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Holds Purchase Alert but add Publication Year

0

48 views

 I have access to Report builder (not SSMS) and am trying to create a version of the canned Holds Purchase Alert report that also includes publication year of the items.

Any help would be much appreciated!

asked 1 year ago  by <img width="18" height="18" src="../../_resources/3e3747f562da419892f766f0527883a7.jpg"/>  crosenthal@pwcgov.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_740"></a>[](#)

0

<a id="downVoteAns_740"></a>[](#)

Answer Accepted

Hi.

Here is the query I created based on the stored procedure behind this report.

It includes a parameter for the assigned collection in the INSERT INTO statement. If you do not want to use this parameter, you should be ablel to delete the text from "and BibliographicRecordID in (SELECT...).

It also includes a count of items that went Lost or Missing recently to help you identify bibs that may be OK once the shelves are searched for Missing items, or a patron returns a Lost one for which they just received a bill.

The standard number is the ISBN or UPC number that was most recently added to the bib.

Hope this helps.

JT

--Query based on code from Polaris stored procedure Rpt_HoldPurchaseAlert
--with additions to limit by item assigned collection
--Added fieldsfor pubyear and recently lost/missing items.
BEGIN
 SET NOCOUNT ON
 DECLARE @nNumberOfHolds int
 DECLARE @nNumberOfItems int
 DECLARE @CollectionID int --\[added\]
 
 set @nNumberOfHolds = 4
 set @nNumberofItems = 1
 set @CollectionID = 3

 DECLARE @t TABLE --\[CALLNO, SizzCount, OOASCount added\]
 (
  BibliographicRecordID int NOT NULL,
  CALLNO nvarchar(50) NULL,
  Author nvarchar(255) NULL,
  Title nvarchar(255) NULL,
  PubYear int NULL,
  StandardNo varchar(50) NULL,
  MARCTypeOfMaterial varchar(80) NULL,
  HoldCount int NULL,
  ItemCount int NULL,
  LostMissRecCount int NULL
 )
 DECLARE @t1 TABLE
 ( BibliographicrecordID int NOT NULL,
  TagNumber int NULL,
  SubFieldID int NULL,
  StandardNumber nvarchar (50) NULL)
   
 DECLARE @t2 TABLE
 ( BibliographicrecordID int NOT NULL,
  StandardNumber nvarchar (50) NULL)

 --gets only locked requests
 INSERT INTO @t(BibliographicRecordID, HoldCount)
  SELECT distinct BibliographicRecordID
  , COUNT(SHR.SysHoldRequestID) as holds
 FROM Polaris.Polaris.SysHoldRequests SHR WITH (NOLOCK)
 WHERE SysHoldStatusID IN (1,3) AND BibliographicRecordID <> 0
 and BibliographicRecordID in
 (SELECT distinct it.AssociatedBibRecordID as Bibliographicrecordid from polaris.polaris.ItemRecords it with (nolock)
 where  it.assignedcollectionid in (@CollectionID))
 GROUP BY BibliographicRecordID

 \-\- only include final item records that are not withdrawn \[claim ret, lost, missing\]
 \-\- \[modified to count only holdable items\]
 UPDATE @t
 SET ItemCount =
  (SELECT COUNT(CIR.AssociatedBibRecordID)
   FROM Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)
   WHERE CIR.AssociatedBibRecordID = t.BibliographicRecordID AND
    CIR.ItemStatusID NOT IN (7,8,9,10,11) AND CIR.RecordStatusID = 1
    and CIR.Holdable= 1)
 FROM @t t, Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)

  UPDATE @t
 SET LostMIssRecCount =
  (SELECT COUNT(CIR.AssociatedBibRecordID)
   FROM Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)
   WHERE CIR.AssociatedBibRecordID = t.BibliographicRecordID AND
    CIR.ItemStatusID in (7,10) AND CIR.RecordStatusID = 1
    and CIR.ItemStatusDate >= dateadd(dd,-14,getdate()))
 FROM @t t, Polaris.Polaris.CircItemRecords CIR WITH (NOLOCK)
 
 IF (@nNumberOfItems > 0)
 BEGIN
  declare @fRatio float
  select @fRatio = @nNumberOfHolds / (@nNumberOfItems * 1.0)

  DELETE FROM @t
   WHERE (ItemCount <> 0  and Itemcount is not null)
   AND HoldCount/(ItemCount * 1.0) < @fRatio
 END
 ELSE
 BEGIN
  DELETE FROM @t
   WHERE (ItemCount <> 0    and Itemcount is not null)
   AND HoldCount/(ItemCount * 1.0) < @nNumberOfHolds
 END
 
 UPDATE @t
 SET CALLNO = BR.BrowseCallNo,
  Author = BR.BrowseAuthor,
  Title = BR.BrowseTitle,
  PubYear = BR.MARCPubDateOne,
  MARCTypeOfMaterial = MTOM.Description
 FROM @t t
 INNER JOIN Polaris.Polaris.BibliographicRecords BR WITH (NOLOCK)
  ON t.BibliographicRecordID = BR.BibliographicRecordID
 LEFT OUTER JOIN Polaris.Polaris.MARCTypeOfMaterial MTOM WITH (NOLOCK)
  ON BR.PrimaryMARCTOMID = MTOM.MARCTypeOfMaterialID 

 INSERT INTO @t1
 (BibliographicrecordID, StandardNumber, TagNumber, SubFieldID)
 (select bt.BibliographicRecordID, bs.data as StandardNumber, bt.TagNumber,
 bs.BibliographicSubfieldID as SubFieldID
 from polaris.polaris.BibliographicSubfields bs with (nolock)
 join polaris.polaris.bibliographictags bt (nolock) on (bs.BibliographicTagID = bt.BibliographicTagID)
 where bt.TagNumber in (020,024)
 and bs.Subfield like 'a'
 and bt.BibliographicRecordID in(Select bibliographicrecordid from @t))

 INSERT INTO @t2
 (Bibliographicrecordid, StandardNumber)
 (Select BibliographicrecordID, max(standardnumber) as StandardNumber from @t1
 group by BibliographicrecordID)

 UPDATE @t
 SET StandardNo = t2.StandardNumber
 FROM @t t
 join @t2 t2 on (t.BibliographicRecordID = t2.BibliographicrecordID)

 SELECT
  BibRecordID = LTrim(str(BibliographicRecordID)),
  CALLNO,
  Title,
  Author,
  PubYear,
  StandardNo,
  MARCTypeOfMaterial,
  GoodItems = LTrim(str(ItemCount)),
  Holds = LTrim(str(HoldCount)),

  LostMissRecently = LTrim(str(LostMissReccount))
 FROM @t
 ORDER BY HoldCount DESC, ItemCount
END

answered 1 year ago  by <img width="18" height="18" src="../../_resources/3e3747f562da419892f766f0527883a7.jpg"/>  jwhitfield@aclib.us

I just noticed that this query has some declared variables/parameters. For Report Builder purposes, you can remove these lines:

 DECLARE @nNumberOfHolds int
 DECLARE @nNumberOfItems int
 DECLARE @CollectionID int --\[added\]
 
 set @nNumberOfHolds = 4
 set @nNumberofItems = 1
 set @CollectionID = 3

You will then need to define them as report parameters in Report Builder. If you do not want them as parameters, you can replace @NumberOfHolds, @NumberOfItems and @CollectionID with actual values in the query,

Sorry I missed this when copying and pasting. JT

— jwhitfield@aclib.us 1 year ago

<a id="commentAns_740"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Fiscal year expenditures report](https://iii.rightanswers.com/portal/controller/view/discussion/98?)
- [Has anyone developed a SimplyReport/SQL Report to answer PLA's Public Library Data Service circulation questions?](https://iii.rightanswers.com/portal/controller/view/discussion/439?)
- [Items in system 2+ years with 2+ copies](https://iii.rightanswers.com/portal/controller/view/discussion/86?)
- [Average hold wait times](https://iii.rightanswers.com/portal/controller/view/discussion/845?)
- [Report to find Items added by Material Type in a certain year?](https://iii.rightanswers.com/portal/controller/view/discussion/430?)

### Related Solutions

- [Sort by year searching](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930814636875)
- [Default Price Overriding Price from 970 $p during Bulk Add to PO](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930605884624)
- [YTD circulation and Previous YTD circulation do not add up to Lifetime circulation](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930953785925)
- [How to reach the Sales Department at Innovative](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170825150344473)
- [Acquisitions Fiscal Year Rollover Instruction](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930945428671)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)