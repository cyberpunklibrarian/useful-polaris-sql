[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for an owning branch' s shelf location circs for 2021

0

69 views

Hi,

Does anybody have an SQL that will give me a branch's shelf location circs that will provide me with circs for only their branch in 2021? I have this query from a previous post, but it gives me the circ count for other library items that circ'd at their library with the same shelf location.  

&nbsp;

select torg.name as TransactionBranchName, sl.description as ShelfLocation,count(distinct th.transactionid) as Total  
from PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK)  
left outer join PolarisTransactions.Polaris.TransactionDetails td (nolock)  
on (th.TransactionID = td.TransactionID)  
inner join Polaris.Polaris.Organizations torg (nolock)  
on (th.OrganizationID = torg.OrganizationID)  
inner join Polaris.Polaris.ShelfLocations sl (nolock)  
on (td.numvalue = sl.ShelfLocationID)  
where th.TransactionTypeID = 6001  
and td.TransactionSubTypeID = 296  
and th.TranClientDate between '01/01/2021' and '12/31/2021'  
and th.OrganizationID = 16  
and (sl.description in  
(Select description from POLARIS.ShelfLocations where organizationid = 16))  
Group by torg.name,sl.description  
Order by torg.name,sl.description

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_286b705af25f4d7ba917c14a39d35db1.jpg"/> scole@slcolibrary.org

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 1 year ago

<a id="upVoteAns_1332"></a>[](#)

0

<a id="downVoteAns_1332"></a>[](#)

Answer Accepted

Hi,

&nbsp;

Although only the transacting branch is recorded in the TransactionHeaders table, the assigned branch is captured in the TransactionDetails table so I just used your SQL and added a second join to the TransactionDetails table (with the second join limited to subtype 125) to grab the Items Assigned Branch ID field. I tried to add comments to all of my edits for clarity. Here is what that gave me:

&nbsp;

`select torg.name as TransactionBranchName, sl.description as ShelfLocation,count(distinct th.transactionid) as Total`  
`from PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK)`  
`left outer join PolarisTransactions.Polaris.TransactionDetails td296 (nolock) --I changed your alias to account for a second join to TransactionDetails`  
`on (th.TransactionID = td296.TransactionID)`  
`inner join PolarisTransactions.Polaris.TransactionDetails td125 (nolock) --I added a second join to TransactionDetails to get the Items Assigned Brannch Field`  
`on (th.TransactionID = td125.TransactionID)`  
`inner join Polaris.Polaris.Organizations torg (nolock)`  
`on (th.OrganizationID = torg.OrganizationID)`  
`inner join Polaris.Polaris.ShelfLocations sl (nolock)`  
`on (td296.numvalue = sl.ShelfLocationID)`  
`where th.TransactionTypeID = 6001`  
`and td296.TransactionSubTypeID = 296`  
`and td125.TransactionSubTypeID = 125 --limits the second TransactionDetails join to the Items Assigned Branch subtype`  
`and th.TranClientDate between '01/01/2021' and '12/31/2021'`  
`and th.OrganizationID = 16`  
`and td125.numValue = 16 --limits transactions to items assigned to your target banch`  
`and (sl.description in`  
`(Select description from POLARIS.Polaris.ShelfLocations where organizationid = 16)) --I added the second Polaris so that I could test this locally`  
`Group by torg.name,sl.description`  
`Order by torg.name,sl.description`

We don't use shelf location for most things so I couldn't test it extensively, but it *appears to* *work* at least.

&nbsp;

I hope that helps you get the data you need.

&nbsp;    -Daniel

&nbsp;

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_286b705af25f4d7ba917c14a39d35db1.jpg"/> ddunphy@aclib.us

<a id="commentAns_1332"></a>[Add a Comment](#)

<a id="upVoteAns_1337"></a>[](#)

0

<a id="downVoteAns_1337"></a>[](#)

Thank you, Daniel. I appreciate your help!

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_286b705af25f4d7ba917c14a39d35db1.jpg"/> scole@slcolibrary.org

- <a id="acceptAnswer_1337"></a>[Accept as answer](#)

<a id="commentAns_1337"></a>[Add a Comment](#)

<a id="upVoteAns_1334"></a>[](#)

0

<a id="downVoteAns_1334"></a>[](#)

Yes, the checkouts recorded in the transaction tables will remain regardless of what subsequently happens to the items, even if the item records are deleted. There can be complications when, for instance, you want the titles of the items that circulated since the title is not recorded in the Transaction Tables so you have to join to the bib records table but can't use the item table as a midpoint because the item has been deleted. But, in the query you posted, you're just counting the transactions and don't need to link them to the item tables so you won't lose any transactions. As long as theren't any rows missing from your shelf location table, you'll get a full count of the checkouts that occurred during your reference time. 

&nbsp;

&nbsp;    -Daniel

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_286b705af25f4d7ba917c14a39d35db1.jpg"/> ddunphy@aclib.us

- <a id="acceptAnswer_1334"></a>[Accept as answer](#)

<a id="commentAns_1334"></a>[Add a Comment](#)

<a id="upVoteAns_1333"></a>[](#)

0

<a id="downVoteAns_1333"></a>[](#)

I think this works! Just to clarify the SUM PREV YEAR CIRC COUNT in Simply Reports (Items > Item Count Reports > Report Output Columns > Sum Prev Year Circ Count) only gives the totals of item records still attached to the bib record? So if the item records have been deleted, the circs will not be counted? But this query counts those deleted item records circs because we are using the transaction table?

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_286b705af25f4d7ba917c14a39d35db1.jpg"/> scole@slcolibrary.org

- <a id="acceptAnswer_1333"></a>[Accept as answer](#)

<a id="commentAns_1333"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for bibs with holds and no owned items by branches](https://iii.rightanswers.com/portal/controller/view/discussion/821?)
- [Shelf location circulation statistics](https://iii.rightanswers.com/portal/controller/view/discussion/441?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)
- [SQL for Item records from PO/FY/Branch](https://iii.rightanswers.com/portal/controller/view/discussion/326?)
- [SQL help for for circ info on 'new' stuff](https://iii.rightanswers.com/portal/controller/view/discussion/698?)

### Related Solutions

- [Add Shelf Locations - System Level](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930254225543)
- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [What to check after setting up a new code in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170228154215576)
- [Error when saving Item Templates](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930278097820)
- [A New Branch](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161228114330859)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)