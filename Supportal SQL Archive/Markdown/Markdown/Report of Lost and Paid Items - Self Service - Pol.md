Report of Lost and Paid Items - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Report of Lost and Paid Items

0

103 views

Is there a way to run a report in Simply Reports for all Lost Items that have been paid for. I know how to run a report for Lost items, but I can't figure out how to cross-reference that with paid.

asked 3 years ago  by <img width="18" height="18" src="../../_resources/a3fe72262b9e4ba0b51c26d201269cc3.jpg"/>  scolombo@wbrplibrary.us

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 3 years ago

<a id="upVoteAns_375"></a>[](#)

0

<a id="downVoteAns_375"></a>[](#)

I cannot help with Simply Reports options - though I would love to hear from someone else who has figured out how to do it there! :-)

However, I thought I could perhaps share my newbie attempts at SQL work-around queries to find that information. My libraries wanted to see payments for lost as well as damaged beyond repair items.

One of the attached reports is for items still in the database. The other is for items that have been purged. I added these reports to the custom report folder which is why the dates and library ID sections in the WHERE portion are as they are.

In terms of the 6+ months, I think you could substitute PA.TxnDate **<= dateadd(mm,-6,getdate())** for the PA.TxnDate BETWEEN @dtBeginDate AND @dtEndDate?

In any case, just passing these along in case they are somehow useful. (as I said, I'm still learning, so please excuse any oddness :-)

Alison

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a3fe72262b9e4ba0b51c26d201269cc3.jpg"/>  ahoffman@monarchlibraries.org

- <a id="acceptAnswer_375"></a>[Accept as answer](#)

- [Payments for Replacement Fees and Damaged with Related Item Information.txt](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/331?fileName=331-375_Payments+for+Replacement+Fees+and+Damaged+with+Related+Item+Information.txt "Payments for Replacement Fees and Damaged with Related Item Information.txt")
- [Payments for Replacement Fees with Related PURGED Item Information.txt](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/331?fileName=331-375_Payments+for+Replacement+Fees+with+Related+PURGED+Item+Information.txt "Payments for Replacement Fees with Related PURGED Item Information.txt")

This is very helpful, Thanks!

— scolombo@wbrplibrary.us 3 years ago

<a id="commentAns_375"></a>[Add a Comment](#)

<a id="upVoteAns_374"></a>[](#)

0

<a id="downVoteAns_374"></a>[](#)

Here is a query that may be helpful.

I also have a report that we have set to run weekly to give a list of all items linked to bills that were paid in the last week. The report is based on a tweak to a report created in Simply Reports. It is not limited to Lost items, and it is set to use relative dates, but I can share it if that would be helpful.

JT

SELECT DISTINCT
it.ItemRecordID
, it.Barcode
, br.BrowseTitle
, ac.TxnDate
FROM Polaris.Polaris.PatronAccount ac WITH (nolock)
INNER JOIN Polaris.Polaris.ItemRecords it (nolock)
ON (ac.ItemRecordID = it.ItemRecordID)
INNER JOIN Polaris.Polaris.BibliographicRecords br (nolock)
ON (it.AssociatedBibRecordID = br.BibliographicRecordID)
where TxnCodeID not in (1,4,8,9)
AND txnamount > 0  
AND it.ItemStatusID = 7 --Lost
AND ac.FeeReasonCodeID in (9,8,-1,7) 
AND ac.PaymentMethodID in (11,14,12,4,5,13,3,1,2,17,6,15) 
AND ac.TxnDate >= dateadd(dd,-7,getdate()) --or '01/01/2018'
AND ac.TxnDate < getdate() -- or '01/31/2018'

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a3fe72262b9e4ba0b51c26d201269cc3.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_374"></a>[Accept as answer](#)

Thanks!

— scolombo@wbrplibrary.us 3 years ago

<a id="commentAns_374"></a>[Add a Comment](#)

<a id="upVoteAns_373"></a>[](#)

0

<a id="downVoteAns_373"></a>[](#)

I would love something for this as well! I have an SQL query I got from someone once that I'd be happy to share here, but I'd also like to be able to add a timeframe to it and I haven't been able to figure it out. I need Lost items that were paid 6+ months prior to the report run date

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a3fe72262b9e4ba0b51c26d201269cc3.jpg"/>  abrookins@cvlga.org

- <a id="acceptAnswer_373"></a>[Accept as answer](#)

<a id="commentAns_373"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Adding number of "live" items to a report](https://iii.rightanswers.com/portal/controller/view/discussion/666?)
- [Searching for Items that have not circ'd in 2 years ex. DVD's](https://iii.rightanswers.com/portal/controller/view/discussion/1053?)
- [Renewals reports](https://iii.rightanswers.com/portal/controller/view/discussion/83?)

### Related Solutions

- [Item Circulation Statistics Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930404036798)
- [Item Circulation by Statistical Code](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930592909894)
- [Claimed item does not appear in the Claimed Items report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930283717537)
- [Errors accessing reports in the client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930369939404)
- [Custom report parameters not allowing multiple selections in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930424447003)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)