[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Data Driven Subscriptions

0

55 views

Originally posted by jlellelid@sno-isle.org on Friday, April 21st 10:01:27 EDT 2017  
<br/>Has anyone had experience setting up a data driven subscription? I have a number of frequently-run reports that do not contain data. I would prefer not to send out a blank report to the user only to have them respond "why is my report blank today" and/or" why are you sending me emails that do not contain any data." I find the data-driven instructions mystifying in relation to how can I apply it to Polaris reports and the users that receive them. Any help would be greatly appreciated. Thank you.

Jon Lellelid Sno-Isle Libraries jlellelid@sno-isle.org

asked 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_e4fda87ed58b4dfb9ea99193914f6c5a.jpg"/> support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 6 years ago

<a id="upVoteAns_187"></a>[](#)

0

<a id="downVoteAns_187"></a>[](#)

Well DDS might not quite take care of your needs (now that I've reread the post).  You can use DDS to define the various parts of a subscription, but it wouldn't be going so far as to run the report and not send it if it is blank.

\[thinking\]...

Unless you had a really long query in which you basically put the report in the WHERE clause so you could only limit to organizations which would return results.  That would be neat!  However, in my experience, a blank report is better than no report (how would the staff know the difference between not getting an email because there was no data and not getting an email because there was a glitch?).  if you still want to try and write a conditional send query, i'd be happy to be involved in the process.

&nbsp;

Things DDS could do for you: create one subscription which sends out individualized reports to all your organizations (1 subscription for 77 emails, for example). Allow you to dynamically determine dates (so you can have a report run on the first of the month using dates for the previous month), and another other parts of a subcription (email address, subject, body...so you could reference appropriate dates when sending an email).

&nbsp;

The first question in DDS is...do you have Enterprise edition of SQL Server (Reporting Services)?  This is required to utillize them.

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_e4fda87ed58b4dfb9ea99193914f6c5a.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_187"></a>[Accept as answer](#)

<a id="commentAns_187"></a>[Add a Comment](#)

<a id="upVoteAns_185"></a>[](#)

0

<a id="downVoteAns_185"></a>[](#)

No Trevor I have not. Vincent Kruggel in Alberta is also interested.

&nbsp;

Jon Lellelid

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_e4fda87ed58b4dfb9ea99193914f6c5a.jpg"/> jlellelid@sno-isle.org

- <a id="acceptAnswer_185"></a>[Accept as answer](#)

<a id="commentAns_185"></a>[Add a Comment](#)

<a id="upVoteAns_184"></a>[](#)

0

<a id="downVoteAns_184"></a>[](#)

Hey Jon-

&nbsp;

Did you ever get any help with this?

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_e4fda87ed58b4dfb9ea99193914f6c5a.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_184"></a>[Accept as answer](#)

<a id="commentAns_184"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [EZProxy - New York Times / Wall Street Journal](https://iii.rightanswers.com/portal/controller/view/discussion/1165?)
- [Advice on creating our own table in Polaris database](https://iii.rightanswers.com/portal/controller/view/discussion/1300?)
- [Welcome emails for new patrons. Does anyone do this?](https://iii.rightanswers.com/portal/controller/view/discussion/307?)
- [Does your library do security vulnerability scans on your network and the applications that use it?](https://iii.rightanswers.com/portal/controller/view/discussion/259?)
- [Migrating from Horizon to Polaris](https://iii.rightanswers.com/portal/controller/view/discussion/336?)

### Related Solutions

- [Serials Publication Pattern Templates](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160503130322211)
- [Training - 5.0 SP3 Subscription and Standing Order Functionality.pptx](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160506095843765)
- [Training - 5.0 SP3 Serials.pptx](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160505145156203)
- [Deleting Serial Issue Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930491104336)
- [Cannot enter an email address in Reporting Services Subscriptions](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930940586494)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)