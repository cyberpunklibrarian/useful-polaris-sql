[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# How to find my Institution ID?

\-1

56 views

Hi I am trying to find my home library's ID number. Could you share how you can find your home library codes, called "Institution ID" or "Code AO" at Polaris system please?  Thanks. 

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_ca1620bda3224c2cbf4d4f564156dfd2.jpg"/> wxie@orlandparklibrary.org

[home library codes](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=447#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_1167"></a>[](#)

0

<a id="downVoteAns_1167"></a>[](#)

If you are a single site library, you InsitutionID will be 3 (1 is the system, 2 is the library, 3 is the branch). If you are a multi-branch library or part of a consortium, then you can see your ID in SystemAdministration (I usually got to Exploerer > Branch and then right click to look at the properties of the relevant branch).

&nbsp;

If you have SQL access, the InstitutionID in SIP is the same as OrganizationID in the Organizations table.

&nbsp;

You can also see you ID by going to the PAC.  Open the catalog and make sure you have selected your branch.  The URL will contain a part that reads "ctx=74.0.0.1003" or something like that.  The first number after CTX is your InstitutionID. 

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_ca1620bda3224c2cbf4d4f564156dfd2.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_1167"></a>[Accept as answer](#)

That is amazing. I just posted it. Thanks!

— wxie@orlandparklibrary.org 2 years ago

<a id="commentAns_1167"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [I need to pull OCLC numbers from records with a deleted status to batch update our OCLC holdings. I'm able to get a report with: Bib record ID, Title, and Bib record status, but the column for MARC OCLC number is blank. Why doesn't it show up?](https://iii.rightanswers.com/portal/controller/view/discussion/829?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)
- [Circulation count of deleted items from a collection](https://iii.rightanswers.com/portal/controller/view/discussion/92?)
- [How to view holds history over time for entire collection?](https://iii.rightanswers.com/portal/controller/view/discussion/1337?)
- [Looking for a IN-Transit query to find past counts](https://iii.rightanswers.com/portal/controller/view/discussion/84?)

### Related Solutions

- [IUG 2014: Content Pro IRX - A Multi-Faceted Institutional Repository Solution](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160419091703190)
- [IUG 2013: Content Pro IRX: An Institutional Digital Repository Solution](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160420083049363)
- [Vital Batch Modification - Adding an Alt-ID](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160728154001429)
- [Tiff Image file did not create a Thumbnail in Vital](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160829104153255)
- [Why is the EDI Agent job continuously running?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930649540631)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)