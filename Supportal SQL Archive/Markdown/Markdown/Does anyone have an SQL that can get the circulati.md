[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Does anyone have an SQL that can get the circulation counts from either funds or invoice line items in a certain fiscal year that will count deleted items circulation?

0

43 views

I have an SQL that pulls the invoice line items and I can get transaction counts for everything except for withdrawn/deleted items. I am guessing that this is because they are no longer linked to the invoices. We are trying to get the information to calculate cost per circ based off  funds and statistical codes for 2023.  I tried the same approach with funds but got the same results. Is there another way to do this?  Results attached.

SELECT  
INVFY.FiscalYearName,  
InvLin.InvLineItemID,  
INVLinSeg.FundAlternativeName,  
INVLinSeg.SegmentQuantityRec,  
INVLinSeg.SegmentGrandTotalBase,  
InvLin.MARCBrowseTitle,  
INVLinSeg.SegmentDestinationOrganizationName,  
INVLinSegItm.ItemStatisticalCode,  
COUNT(DISTINCT th.TransactionID) AS TransactionCount  
from  
Polaris.RwriterInvoiceFiscalYears INVFY with (nolock)  
left outer join Polaris.RwriterInvHeaderView INVHead with (nolock) on (INVFY.InvoiceID = INVHead.InvoiceID)  
inner join Polaris.RwriterInvlineView InvLin with (NOLOCK) on (INVHead.InvoiceID = InvLin.InvoiceID and INVFY.FiscalYearID = InvLin.FiscalYearID)  
inner join Polaris.RwriterInvlineSegmentView INVLinSeg with (NOLOCK) on (InvLin.InvoiceID = INVLinSeg.InvoiceID and InvLin.InvLineItemID = INVLinSeg.InvLineItemID and INVFY.FiscalYearID = INVLinSeg.FiscalYearID)  
LEFT JOIN Polaris.RwriterLineItemToItemsView INVLinSegItm WITH (NOLOCK) ON INVLinSeg.InvLineItemSegmentID = INVLinSegItm.InvLineItemSegmentID  
LEFT JOIN PolarisTransactions.Polaris.TransactionDetails td WITH (NOLOCK) ON INVLinSegItm.ItemRecordID = td.numvalue  
LEFT JOIN PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK) ON th.TransactionID = td.TransactionID AND th.TransactionTypeID = 6001 AND th.TranClientDate BETWEEN '2023-01-01 00:00:00.000' AND '2023-12-31 23:59:59.999'  
where  
INVFY.FiscalYearID = 14  
AND InvLin.InvLineItemID = 782725  
GROUP BY  
INVFY.FiscalYearName,  
InvLin.InvLineItemID,  
INVLinSeg.FundAlternativeName,  
INVLinSeg.SegmentQuantityRec,  
INVLinSeg.SegmentGrandTotalBase,  
InvLin.MARCBrowseTitle,  
INVLinSeg.SegmentDestinationOrganizationName,  
INVLinSegItm.ItemStatisticalCode

&nbsp;

&nbsp;

&nbsp;

asked 2 months ago by <img width="18" height="18" src="../_resources/default-avatar_2d377718d7614b43810d650756e1bbe8.jpg"/> scole@slcolibrary.org

- [Screenshot 2024-06-19 150515.jpg](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/1397?fileName=1397-Screenshot+2024-06-19+150515.jpg "Screenshot 2024-06-19 150515.jpg")

[fiscalyear](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=548#t=commResults) [funds](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=509#t=commResults) [invoices](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=549#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 months ago

<a id="upVoteAns_1438"></a>[](#)

0

<a id="downVoteAns_1438"></a>[](#)

What about something like this? I think you're right that the invoices are no longer linked to deleted items, but funds and statistical codes are still in the circitemrecords table. Of course, this assumes that the item price is equivalent to what would have been listed on the invoice, and that the assigned branch is equivalent to the SegmentDestinationOrganizationName, etc etc...

```markup
select barcode,circitemrecords.RecordStatusID,fundingsource,price,SortTitle,organizations.name,statisticalcodes.Description,count(distinct th.transactionid) as transactioncount from circitemrecords with (nolock)
left join itemrecorddetails with (nolock) on circitemrecords.itemrecordid=itemrecorddetails.itemrecordid
left join bibliographicrecords with (nolock) on circitemrecords.AssociatedBibRecordID=bibliographicrecords.BibliographicRecordID
LEFT JOIN PolarisTransactions.Polaris.TransactionDetails td WITH (NOLOCK) ON circitemrecords.ItemRecordID = td.numvalue
LEFT JOIN PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK) ON th.TransactionID = td.TransactionID 
left join organizations with (nolock) on circitemrecords.assignedbranchid=organizations.OrganizationID
left join statisticalcodes with (nolock) on (circitemrecords.statisticalcodeid=statisticalcodes.statisticalcodeid and circitemrecords.assignedbranchid=statisticalcodes.organizationid)
where th.TransactionTypeID = 6001 AND th.TranClientDate BETWEEN '2023-01-01 00:00:00.000' AND '2023-12-31 23:59:59.999' and year(itemrecorddetails.AcquisitionDate)=2023
group by barcode,circitemrecords.RecordStatusID,fundingsource,price,SortTitle,organizations.name,statisticalcodes.Description
```

&nbsp;

```markup
select barcode,circitemrecords.RecordStatusID,fundingsource,price,SortTitle,organizations.name,statisticalcodes.Description,count(distinct th.transactionid) as transactioncount from circitemrecords with (nolock)
left join itemrecorddetails with (nolock) on circitemrecords.itemrecordid=itemrecorddetails.itemrecordid
left join bibliographicrecords with (nolock) on circitemrecords.AssociatedBibRecordID=bibliographicrecords.BibliographicRecordID
LEFT JOIN PolarisTransactions.Polaris.TransactionDetails td WITH (NOLOCK) ON circitemrecords.ItemRecordID = td.numvalue
LEFT JOIN PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK) ON th.TransactionID = td.TransactionID 
left join organizations with (nolock) on circitemrecords.assignedbranchid=organizations.OrganizationID
left join statisticalcodes with (nolock) on (circitemrecords.statisticalcodeid=statisticalcodes.statisticalcodeid and circitemrecords.assignedbranchid=statisticalcodes.organizationid)
where th.TransactionTypeID = 6001 AND th.TranClientDate BETWEEN '2023-01-01 00:00:00.000' AND '2023-12-31 23:59:59.999' and year(itemrecorddetails.AcquisitionDate)=2023
group by barcode,circitemrecords.RecordStatusID,fundingsource,price,SortTitle,organizations.name,statisticalcodes.Description
```

&nbsp;

answered 2 months ago by <img width="18" height="18" src="../_resources/default-avatar_2d377718d7614b43810d650756e1bbe8.jpg"/> ebrondermajor@onlib.org

- <a id="acceptAnswer_1438"></a>[Accept as answer](#)

<a id="commentAns_1438"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Circulation count of deleted items from a collection](https://iii.rightanswers.com/portal/controller/view/discussion/92?)
- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)
- [Searching for Items that have not circ'd in 2 years ex. DVD's](https://iii.rightanswers.com/portal/controller/view/discussion/1053?)
- [Does anyone have a sql script for a monthly in-house check-in count?](https://iii.rightanswers.com/portal/controller/view/discussion/615?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)

### Related Solutions

- [Credit an individual Invoice Line Item that has been paid for](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930299529481)
- [Circulation statistics and deleted records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930736904760)
- [Why do I see two of each fund in the drop downs?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200825194121527)
- [Unable to pay an invoice](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930645041913)
- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)