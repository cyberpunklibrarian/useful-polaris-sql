[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [General Announcements](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=55)

# New England IUG Agenda

0

13 views

Hi Polaris users,

New England IUG is less than a month away on Tuesday September 24th!

The event will be held at the Harry Bennett Branch of The Ferguson Library ([115 Vine Rd. Stamford, CT 06905](https://www.google.com/maps?hl=en&client=firefox-a&ie=UTF8&q=harry+bennett+115+Vine+Road+Stamford,+CT+06905&fb=1&gl=us&hq=harry+bennett&hnear=115+Vine+Rd,+Stamford,+CT+06905&cid=0,0,1298882548850842271&ll=41.105484,-73.542438&spn=0.010348,0.011201&z=15&iwloc=A&source=embed)) from 9-4. Registration starts at 8:00. Free parking is available on site.

To register please click here: https://fs6.formsite.com/ferguson/form19/index.html

&nbsp;

Presentations will include:

&nbsp;

- Idea Lab – Jeremy Goldstein and Maisam Nouh (Minuteman Library Network and The Ferguson Library)
- Innovative Corporate Update and In-Market Product Roadmap – Michael Monroy, Director, Global Sales Support, iii and Bill Easton Account Executive
- Innovative Inspire – Tom Jacobson, VP, Executive Library Advocate & Strategist, iii
- My (Auto Renewal) Diary – Thomas O'Connell, Mid-Hudson Library System, iii
- Bringing SQL reports to your users – Jeremy Goldstein and Sam Cook (Minuteman Library Network and Library Connection, Inc)
- API – Brendan Lawlor (CLAMS)

&nbsp;

This is a free event! Breakfast and lunch are included.

&nbsp;

Thanks,

&nbsp;

Maisam Nouh Information Technology Supervisor

The Ferguson Library

One Public Library Plaza

Stamford, CT 06904

Phone: 203-351-8414

asked 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_902cb53f1e19400998e50b73872b32ee.jpg"/> gjelinek@faylib.org

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [New England IUG is going virtual](https://iii.rightanswers.com/portal/controller/view/discussion/838?)
- [New England IUG is going virtual](https://iii.rightanswers.com/portal/controller/view/discussion/802?)
- [June IUG Forum - I'm a New Polaris ILS Administrator](https://iii.rightanswers.com/portal/controller/view/discussion/1195?)
- [Idea lab: IUG announcements and new ideas](https://iii.rightanswers.com/portal/controller/view/discussion/1168?)
- [Vote for IUG Steering Committee!](https://iii.rightanswers.com/portal/controller/view/discussion/400?)

### Related Solutions

- [IUG 2018: New Services Offerings](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180517164131867)
- [IUG 2015: A New Direction for ERM](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=150914104719432)
- [IUG 2017: What's New and Ahead for MyLibrary!](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170417151933261)
- [IUG 2018: What's New and Ahead for Sierra](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180515144804263)
- [IUG 2018: What's New and Ahead for Leap](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180517160321548)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)