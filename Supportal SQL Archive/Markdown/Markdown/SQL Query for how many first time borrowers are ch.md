SQL Query for how many first time borrowers are checking out holds? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL Query for how many first time borrowers are checking out holds?

0

63 views

Hello All!

Does anyone have any thoughts on an SQL query that would show how many first time borrowers are checking out held items?

Thanks!

Michael Gregory

CCPL -KY

asked 2 years ago  by <img width="18" height="18" src="../../_resources/bdf52233df724e7982e2664f96b419af.jpg"/>  mgregory@cc-pl.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_590"></a>[](#)

0

<a id="downVoteAns_590"></a>[](#)

Hi Michael

Here's a query that MIGHT make a good start.

It uses ItemRecordHistory and PatronRegistration to find patrons who registered in the previous week who checked out an item that was in Held status. The  MIN narrows down the results to the first checkout of a held item.

I say that this is a start, because a patron may place a hold and have to wait for some time for the hold to be available. If they don' t check anything out before the hold comes in, they won't get caught by the query.

If you need to include these situations, you may be able to set up the inner select to pull the first x number of checkouts for each patron registered, say in the last month, using Row Number and Partition. Here's a StackOverFlow page that may help:

https://stackoverflow.com/questions/176964/select-top-10-records-for-each-category

Also, please note that this is not a guarantee that the patron's absolute first checkout is a held item, just that they checked out a held item pretty soon after they were registered. You could just get the first checkout in the inner query and let the outer query find only patrons whose first checkout was a held item. This would omit patrons who found a book on the shelf when they came to pick up their hold and the on-shelf book was the first one checked out.

This may or may not give you what you need, but it could be a start.

Hope this helps.

JT

--query starts here--

SELECT
t1.PatronID
,t1.OrganizationID
,MIN(t1.TransactionDate) as CheckoutDate

 FROM
(
SELECT irh.ItemRecordHistoryID
      ,irh.ItemRecordID
      ,irh.TransactionDate
      ,irh.ActionTakenID
      ,irh.OldItemStatusID
      ,irh.NewItemStatusID
      ,irh.AssignedBranchID
      ,irh.InTransitRecvdBranchID
      ,irh.PatronID
      ,irh.OrganizationID
      ,irh.PolarisUserID
      ,irh.WorkstationID
  FROM Polaris.Polaris.ItemRecordHistory irh
  JOIN Polaris.Polaris.PatronRegistration pr
  ON (irh.PatronID = pr.PatronID and pr.RegistrationDate > DATEADD(dd,-7,GETDATE()))
    WHERE irh.ActionTakenID in (13,75,77,78,79,90,91)
  ) t1

  WHERE t1.OldItemStatusID = 4
  GROUP BY t1.PatronID, t1.OrganizationID

answered 2 years ago  by <img width="18" height="18" src="../../_resources/bdf52233df724e7982e2664f96b419af.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_590"></a>[Accept as answer](#)

<a id="commentAns_590"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)
- [Identifying a count of patrons, by registered library, whose only activity over a period of time is OverDrive checkout through Polaris integration?](https://iii.rightanswers.com/portal/controller/view/discussion/1044?)
- [Is there an SQL to change item level holds to bib level holds?](https://iii.rightanswers.com/portal/controller/view/discussion/407?)
- [Can we stop patrons from placing holds on items they already have checked out or being Held?](https://iii.rightanswers.com/portal/controller/view/discussion/958?)
- [I need a SQL for locating hold requests where the hold has been denied by a library](https://iii.rightanswers.com/portal/controller/view/discussion/843?)

### Related Solutions

- [Query to find unfillable holds due to volume data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930242167096)
- [ExpressCheck: Held items can be checked out by the wrong patrons](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930574330183)
- [How does Borrow By Mail work?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170825100128299)
- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)
- [Label/Paper stock for BBM Mailer](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=171201090449943)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)