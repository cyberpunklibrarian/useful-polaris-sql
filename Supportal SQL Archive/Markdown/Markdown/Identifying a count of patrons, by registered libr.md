[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Identifying a count of patrons, by registered library, whose only activity over a period of time is OverDrive checkout through Polaris integration?

0

61 views

Hello,

One of the member libraries here would like to know, if possible, how many of their patrons have been active - but only by checking out OverDrive content.

This consortium has the OverDrive integration, and it looks as though that checkout takes place via OrgID 40? (a fake "Digital Collections" branch)

I am just checking if anyone else has gathered statistics with a similar goal and has SQL or other report suggestions to share?

Thanks, much!

Alison

Monarch Library System

asked 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_793770e474ea4e958431ecba16071101.jpg"/> ahoffman@monarchlibraries.org

[overdrive](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=30#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 3 years ago

<a id="upVoteAns_1152"></a>[](#)

0

<a id="downVoteAns_1152"></a>[](#)

I believe you can pull this report from OverDrive Marketplace using the Checkouts report. Change the "Borrowed From" criteria to API. This will give you a count of checkouts. If you export to a worksheet, you should get User ID data, which could give you the patron usage data you are looking for. 

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_793770e474ea4e958431ecba16071101.jpg"/> eberg@siouxfalls.org

- <a id="acceptAnswer_1152"></a>[Accept as answer](#)

<a id="commentAns_1152"></a>[Add a Comment](#)

<a id="upVoteAns_1151"></a>[](#)

0

<a id="downVoteAns_1151"></a>[](#)

There's not a good way to do this by relying only on data from Polaris, because there are a half-dozen wonky things about how Polaris handles the OverDrive integration which make it thoroughly unreliable for anything to do with OverDrive.

The best I could think of would be to pull a list of active patrons from OverDrive Marketplace, then a list of active patrons from Polaris *excluding* anything downloadable, then to see which barcodes from OverDrive aren't also in the Polaris results.

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_793770e474ea4e958431ecba16071101.jpg"/> jjack@aclib.us

- <a id="acceptAnswer_1151"></a>[Accept as answer](#)

<a id="commentAns_1151"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Besides Quipu, does any know of any other products that integrate with Polaris, allows patrons to self-register for a library card online, and verifies and standardizes the address information entered as well as provides name and residency verification?](https://iii.rightanswers.com/portal/controller/view/discussion/253?)
- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)
- [Overdrive API integration and notices](https://iii.rightanswers.com/portal/controller/view/discussion/67?)
- [Is there a way to change checkout period settings for class visits for Bibliotheca self-checkout Kiosks in Polaris 5.5?](https://iii.rightanswers.com/portal/controller/view/discussion/165?)
- [SQL Query for how many first time borrowers are checking out holds?](https://iii.rightanswers.com/portal/controller/view/discussion/504?)

### Related Solutions

- [New patrons identified as a duplicate / new patrons not able to register on the PAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930681961607)
- [Statistics for registered patrons](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930383515616)
- [Error: The Postal Code is not valid. Please try another or see a staff member.](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170616164552265)
- [Special loan period of zero (0) days in check out](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930733069676)
- [ExpressCheck: Held items can be checked out by the wrong patrons](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930574330183)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)