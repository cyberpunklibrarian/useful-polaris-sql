[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Error message: Unable to view another user's record set.

0

59 views

I'm seeing a record set attached to an item record I'm looking at, but the creator is a former employee and I can't actually get the record set: getting the error message "Unable to view another user's record set".

I'm clearly missing some permission or other, but I have the permissions to view, modify, delete, create cataloguing record sets for any organization in our system down to branch level. What am I missing?

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_53898664e07f483e8ba45915264f9534.jpg"/> akrahn@nlls.ab.ca

[administration](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=491#t=commResults) [cataloging](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=4#t=commResults) [cataloging record sets](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=518#t=commResults) [permissions](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=56#t=commResults) [record sets](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=73#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_1353"></a>[](#)

1

<a id="downVoteAns_1353"></a>[](#)

If the record set is owned by the staff user, then no one can view it but them.  Not even administrators with all permissions possible in Polaris can override this.  The owner of the record set will hvae to be changed via SQL if the login is no longer usable.  Your site manager should be able to help with that. 

&nbsp;

You might also want to head over to the IUG site and their discussion forums for questions like this in the future!  We'd love to have you there.

&nbsp;

https://www.innovativeusers.org/

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_53898664e07f483e8ba45915264f9534.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_1353"></a>[Accept as answer](#)

<a id="commentAns_1353"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [For the libraries that pay for zmarc, what setting do you select for Perform Authority Control in the Bibliographic Record tab in your import profiles? And why?](https://iii.rightanswers.com/portal/controller/view/discussion/475?)
- [Authority Records](https://iii.rightanswers.com/portal/controller/view/discussion/580?)
- [Polaris Label Manager set up for Demco labels](https://iii.rightanswers.com/portal/controller/view/discussion/944?)
- [Bib record modifiers](https://iii.rightanswers.com/portal/controller/view/discussion/1380?)
- [Deleting item records with holds](https://iii.rightanswers.com/portal/controller/view/discussion/32?)

### Related Solutions

- [Copying macros from one user to another](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930678014743)
- [Catalog Extract Error Message](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930674953830)
- [MARC record display in PAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930394778428)
- [Do the home and owning branch need to match for floating item records?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930466429189)
- [How can I move a record from one record set to another?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930246836875)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)