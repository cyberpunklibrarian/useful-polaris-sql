SQL for MARC tag search - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for MARC tag search

0

241 views

Does anyone have an SQL query to find all records that contain data in a particular MARC tag? We have old records with series info in 440 and we'd like to create a record set of all those so that we can correct them. So basically we'd like to do an SQL query to find all records with anything in 440.

asked 3 years ago  by <img width="18" height="18" src="../../_resources/5422fb7f35ab4d8084a0e4feae06f35e.jpg"/>  musack@bcls.lib.nj.us

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 3 years ago

<a id="upVoteAns_156"></a>[](#)

0

<a id="downVoteAns_156"></a>[](#)

Thank you so much!! I'll give it a try

answered 3 years ago  by <img width="18" height="18" src="../../_resources/5422fb7f35ab4d8084a0e4feae06f35e.jpg"/>  musack@bcls.lib.nj.us

- <a id="acceptAnswer_156"></a>[Accept as answer](#)

<a id="commentAns_156"></a>[Add a Comment](#)

<a id="upVoteAns_155"></a>[](#)

0

<a id="downVoteAns_155"></a>[](#)

Hi,

We have this query set up in the Bib Search Find Tool SQL search to find records with any data in 300 $e:

select distinct bt.BibliographicRecordID as recordid from Polaris.BibliographicTags bt
with (NOLOCK)
inner join BibliographicRecords br WITH (NOLOCK)
ON br.BibliographicRecordID = bt.BibliographicRecordID
inner join CircItemRecords cir WITH (NOLOCK)
ON bt.BibliographicRecordID = cir.AssociatedBibRecordID
join
Polaris.BibliographicSubfields bs on (bt.BibliographicTagID = bs.BibliographicTagID)
where
TagNumber = 300 and Subfield = 'e' and Data <> '0'

I think the presence of the innerly joined CircItemRecords table limits to those bibs with attached items.  

For your query, the last line could be:  TagNumber = 440 and Subfield = 'a' and Data <> '0'

--Kara

answered 3 years ago  by <img width="18" height="18" src="../../_resources/5422fb7f35ab4d8084a0e4feae06f35e.jpg"/>  kara.steiniger@smgov.net

- <a id="acceptAnswer_155"></a>[Accept as answer](#)

<a id="commentAns_155"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL to edit saved searches](https://iii.rightanswers.com/portal/controller/view/discussion/874?)
- [Does anyone have an SQL report for a specific tag in Authority records?](https://iii.rightanswers.com/portal/controller/view/discussion/625?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [Is there a way via SQL to include the contents of 508 and 511 MARC fields in addition to genre subject headings?](https://iii.rightanswers.com/portal/controller/view/discussion/907?)
- [Searching for Items that have not circ'd in 2 years ex. DVD's](https://iii.rightanswers.com/portal/controller/view/discussion/1053?)

### Related Solutions

- [Searching for Accelerated Reader Tag (526) Data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930649873277)
- [Do I need to re-index during an upgrade?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930458783291)
- [007 LDR Cataloging Question](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930329896653)
- [Bibliographic and Authority Record tag order](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930557664312)
- [1XX, 6XX, 7XX or 8XX tags not linked to Authority Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930898871020)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)