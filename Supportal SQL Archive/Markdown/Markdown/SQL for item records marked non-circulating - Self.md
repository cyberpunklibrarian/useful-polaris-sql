[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for item records marked non-circulating

0

102 views

Can one of you SQL pros provide me with a SQL search to find item records marked non-circulating? TIA!![laughing](../_resources/smiley-laughing_335780eb281443a2983dcd24e8f21559.gif)

asked 5 years ago by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg) sbills@lpld.lib.in.us

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 5 years ago

<a id="upVoteAns_520"></a>[](#)

0

<a id="downVoteAns_520"></a>[](#)

Answer Accepted

From the MSSMS you can use:

SELECT \*  
FROM CircItemRecords  
WHERE NonCirculating = 1  
AND RecordStatusID <> 4

&nbsp;

or from the Item Find Tool SQL Search

SELECT ItemRecordID  
FROM CircItemRecords  
WHERE NonCirculating = 1  
AND RecordStatusID <> 4  

&nbsp;

Rex Helwig  
Finger Lakes Library System  
rhelwig@flls.org  

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_b103c6134b044f8d8c2340931590d99c.jpg"/> rhelwig@flls.org

Thank you, Rex! I appreciate it! ![smile](../_resources/smiley-smile_ce6ef82325194c908832f6b941fadfa4.gif)

— sbills@lpld.lib.in.us 5 years ago

<a id="commentAns_520"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for Item records from PO/FY/Branch](https://iii.rightanswers.com/portal/controller/view/discussion/326?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [SQL for changes to a patron record](https://iii.rightanswers.com/portal/controller/view/discussion/840?)
- [SQL query to export specific record set: Polaris 4.1R2](https://iii.rightanswers.com/portal/controller/view/discussion/93?)
- [Last modifier of patron record](https://iii.rightanswers.com/portal/controller/view/discussion/1048?)

### Related Solutions

- [How do I define an item as non-circulating?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930906100414)
- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [Should non-circulating items be Pending for holds?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180409103151965)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)