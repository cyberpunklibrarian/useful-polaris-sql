[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Limiting New Patron's Check-Out Limit

0

142 views

Originally posted by nshults@schertz.com on Monday, April 17th 15:07:01 EDT 2017  
<br/>We are interested in limiting our first time users to a limited check-out amount, but we do not want to have to manually input this into each patron record via patron code. Do any of you do this automatically right now through SQL? If you do so, do you mind sharing how?

asked 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_67b0468a02654834bc6fce5fd043ffbe.jpg"/> support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 7 years ago

<a id="upVoteAns_38"></a>[](#)

0

<a id="downVoteAns_38"></a>[](#)

Limiting New Patron's check-out limit

Response by cmaxey@websterpl.org on June 21st, 2017 at 2:55 pm

We use Polaris Administration, System, Policy Tables, Patron / Material Type Loan Limit Blocks for this.  
Also, the new ability to group material types (Administration, System, Parameters, Patron Services, Material Type Groups) helps. We've set group for DVD/Blu-ray and limit the total number for our Probationary (New) patron codes to 2 maximum.  
Changing the patron code automatically changes the limits.

answered 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_67b0468a02654834bc6fce5fd043ffbe.jpg"/> support1@iii.com

- <a id="acceptAnswer_38"></a>[Accept as answer](#)

<a id="commentAns_38"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Any ideas how we might restrict checkout of collections by patron age?](https://iii.rightanswers.com/portal/controller/view/discussion/1373?)
- [Are cancelled holds considered holds in holds item count?](https://iii.rightanswers.com/portal/controller/view/discussion/414?)
- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)
- [Does anyone have a SQL report that will show renewal vs new cards for a particular patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/1377?)
- [Can we stop patrons from placing holds on items they already have checked out or being Held?](https://iii.rightanswers.com/portal/controller/view/discussion/958?)

### Related Solutions

- [Patron / Material Type Loan Limit Blocks policy table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930651711850)
- [Patron Codes should have prevented checkouts based on Patron/Material Type Loan Limit Blocks table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930949436345)
- [How do you block patron codes from checking out certain material types?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930356168511)
- [What to check after setting up a new code in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170228154215576)
- [New Patron Code limits and blocks](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930616802374)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)