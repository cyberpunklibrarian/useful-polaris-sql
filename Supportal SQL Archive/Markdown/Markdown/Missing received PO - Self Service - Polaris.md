[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Acquisitions](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=10)

# Missing received PO

0

32 views

Why would a bib with multiple holdings have no purchase orders linked to it?

asked 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_d04b863b49d748308b58b0b41a6080be.jpg"/> ecrumblehulme@sasklibraries.ca

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 6 years ago

<a id="upVoteAns_183"></a>[](#)

0

<a id="downVoteAns_183"></a>[](#)

Hi.

There are a number of reasons why a bib may not have a purchase order linked to it:

\-The holdings existed before your library transitioned to Polaris.

\-The purchase order linked to the bib has been purged due to its age.

\-There was an issue when the title was cataloged. (A new bib and items were created that are not linked to the PO. There may be legitimate reasons why this happened.)

\-The items were all gifts and a PO was never created.

...And probably others, but these are what I can think of at the moment.

If you have a title you know was ordered via a purchase order but there is no PO linked to your bib, you may be able to find a deleted bib (if your library retains them) that was originally linked to the PO via an SQL query. This, along with information about the existing bib and items (who created the record(s), and when) may help you figure out how the link was lost.

&nbsp;

Hope this helps,

JT

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_d04b863b49d748308b58b0b41a6080be.jpg"/> jwhitfield@aclib.us

- <a id="acceptAnswer_183"></a>[Accept as answer](#)

<a id="commentAns_183"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Barcode Missing in Item Record](https://iii.rightanswers.com/portal/controller/view/discussion/216?)
- [Is there a way in the Acquistions module to receive and barcode multipart items or combo packages that will need to be split and attached to two sepaerate bibliographic records but paid and received as one item?](https://iii.rightanswers.com/portal/controller/view/discussion/175?)
- [Is it possible to "uncancel" a canceled PO line?](https://iii.rightanswers.com/portal/controller/view/discussion/824?)
- [Can someone tell me why I can't delete or cancel this PO?](https://iii.rightanswers.com/portal/controller/view/discussion/1321?)
- [Are there any unintended consquences for purging POs and invoices that are, say, 2 years or older?](https://iii.rightanswers.com/portal/controller/view/discussion/472?)

### Related Solutions

- [POs with status of Partly Received, but all line items are Received](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930265371398)
- [How do I correct EDI invoice lines that do not link to the PO line?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930240796789)
- [Default Price Overriding Price from 970 $p during Bulk Add to PO](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930605884624)
- [How does automatic PO closure work?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180608160501210)
- [Credit an individual Invoice Line Item that has been paid for](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930299529481)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)