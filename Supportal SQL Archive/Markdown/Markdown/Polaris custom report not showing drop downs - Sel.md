[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Polaris custom report not showing drop downs

0

151 views

Hi all,

Hopefully someone can help me with this. I've built a custom report that has three parameters and when I load that report into the SQL server reporting services site they work just fine and the drop downs are there. However, when I open the report in Polaris' reports & notices utility the drop downs don't appear they're just text boxes that you can manually enter the values into.

I've tried adding the GIS_ and GIS_MS_ tags to the front of my parameter names but that doesn't seem to do anything. I've also had my parameters set as filters on the table and as parameters solely on the dataset.

Any help would be greatly appreciated.

Thank you,

Max

asked 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_3b88fa2beae144978e7b225015eeb846.jpg"/> mcohen@krl.org

[reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=32#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 6 years ago

<a id="upVoteAns_372"></a>[](#)

1

<a id="downVoteAns_372"></a>[](#)

Answer Accepted

I took a look at one I built a while back and here's how I've got things set up for the branch selection dropdown:

Parameter Name: GIS_MS_OrganizationList

Prompt: Branch

Data type: Text

Get values from a query: The query is in the dataset and calls a sproc. The value is **exec Polaris.Rpt_GetOrganizations 3**

The parameter is set to use OrganizationID as the Value Field and the Label Field is set to Name.

Also, make sure you hit the little button on the Reports and Notices window to refresh the reports and pull the latest versions of whatever you've saved. I've had that help me out a time or two.

Maybe that'll get you some ideas to get up and running!

&nbsp;

answered 6 years ago by ![danielmesser@mcldaz.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_danielmesser@mcldaz.org20180523162210.jpg) danielmesser@mcldaz.org

Thank you! Looks like Polaris doesn't like manually added values for drop downs. As soon as I used exec Polaris.Rpt_GetOrganizations 3 as a dataset for the values it all started working!

I also used GIS_ instead of GIS_MS_ for the non multiple selections which also seems to do the trick.

&nbsp;

Thanks again!

— mcohen@krl.org 6 years ago

Yay! Glad it got up and working! That sproc thing threw me for a loop, so I wound up looking at some other report that *did* work and finding that. I shrugged, hit CTRL C and CTRL V and that made it happen. :)

&nbsp;

Have a great weekend!

&nbsp;

— danielmesser@mcldaz.org 6 years ago

<a id="commentAns_372"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Report for custom Hold Requests to Fill list](https://iii.rightanswers.com/portal/controller/view/discussion/91?)
- [Creating a dropdown menu for a parameter so that it works in Polaris client reports](https://iii.rightanswers.com/portal/controller/view/discussion/769?)
- [Renewals reports](https://iii.rightanswers.com/portal/controller/view/discussion/83?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Custom report parameters not allowing multiple selections in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930424447003)
- [Question about a saved SimplyReports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930457380859)
- [Question about published SimplyReports reports](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989986518)
- [Publishing Custom Reports](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930507535402)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)