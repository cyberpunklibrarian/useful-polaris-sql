SimplyReports or SQL to get circ count by a UDF? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SimplyReports or SQL to get circ count by a UDF?

0

53 views

Originally posted by aross@esls.lib.wi.us on Friday, January 13th 08:42:12 EST 2017
Hello. I am trying to figure out how to get a circulation count by one of our locally defined UDF's. We defined one of the UDFs as "Aldermanic District," and one of the new libraries would like a total count of circulation by Aldermanic Disctrict (by month, I think). I could not figure out an option via Simply Reports. Would this require SQL? If yes, does anyone have help they could offer in that area? I know the basics of SQL but not enough to connect to the transaction database tables, which I think are needed here? (I always end up with WAY too many results, so I know I am missing a tool in my toolbox :-) Thanks, much, for any assistance you can provide. Alison Monarch Library System

asked 4 years ago  by <img width="18" height="18" src="../../_resources/eec2df800cdb4dcdb355d67284286baa.jpg"/>  support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 4 years ago

<a id="upVoteAns_67"></a>[](#)

0

<a id="downVoteAns_67"></a>[](#)

SQL query

Response by ggosselin@richlandlibrary.com on January 18th, 2017 at 9:44 am

Hi Alison!
Here is a SQL query that you could use:
select count (distinct th.transactionid) AS \[NumofCheckouts\],pr.User1
from PolarisTransactions.Polaris.TransactionHeaders th (nolock)
join PolarisTransactions.Polaris.TransactionDetails td (nolock)
on th.TransactionID = td.TransactionID
join Polaris.Polaris.PatronRegistration pr (nolock)
on td.numValue = pr.PatronID
where th.TransactionTypeID = 6001
and td.TransactionSubTypeID = 6
and th.TranClientDate between '2017-01-01' and GETDATE()
group by pr.User1
You'd need to update the pr.User1 to be the UDF field that you're actually interested in and change the TranClientDate line to be the date range you want. I also didn't make any delineation between checkouts and renewals, so they're all getting caught up in this count.
Also in this query I'm joining the transactions and Polaris databases together since the UDF info is not stored in transactions. That means if a patron's UDF value changes after their checkout it will always show in the results with their current value, not the value they had when they actually checked out the item.
I couldn't see a way to do this in SimplyReports, but I might have missed something!

answered 4 years ago  by <img width="18" height="18" src="../../_resources/eec2df800cdb4dcdb355d67284286baa.jpg"/>  support1@iii.com

- <a id="acceptAnswer_67"></a>[Accept as answer](#)

<a id="commentAns_67"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SimplyReports or SQL to get circ count by a UDF?](https://iii.rightanswers.com/portal/controller/view/discussion/82?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)
- [SQL top bib checkouts between 2 dates](https://iii.rightanswers.com/portal/controller/view/discussion/650?)
- [Has anyone developed a SimplyReport/SQL Report to answer PLA's Public Library Data Service circulation questions?](https://iii.rightanswers.com/portal/controller/view/discussion/439?)
- [SQL help for for circ info on 'new' stuff](https://iii.rightanswers.com/portal/controller/view/discussion/698?)

### Related Solutions

- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [How do I schedule the Year End Circ Count Rollover job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930582726474)
- [How do I find item counts by material type?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930417498777)
- [Year End Circ Count Rollover](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160907153854067)
- [Adding a new UDF to patron registration](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930740605846)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)