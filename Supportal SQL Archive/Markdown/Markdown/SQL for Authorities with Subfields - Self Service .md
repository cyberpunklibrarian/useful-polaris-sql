SQL for Authorities with Subfields - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# SQL for Authorities with Subfields

0

84 views

Is there a way to find a specific authority tag paired with a specific subfield? For instance, a 100 tag with a subfield t. I can find bib record tags with specfic subfields (and text in the subfields) with a SQL statement, but I'm not having any luck on the authority side.

Thanks for your help,

Tara

asked 2 years ago  by <img width="18" height="18" src="../../_resources/9e24c98cef0145dbbe283fbfa0ea90eb.jpg"/>  tarar@wacotx.gov

[authority records](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=44#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_707"></a>[](#)

0

<a id="downVoteAns_707"></a>[](#)

Answer Accepted

Hi Tara.

You should be able to search for authority records just like bibliographic records. The tables will be AuthorityRecords, AuthorityTags and AuthoritySubfields. If you use the Authority Find Tool, you can do something like this:

SELECT aut.AuthorityRecordID
FROM Polaris.Polaris.AuthorityTags aut WITH (NOLOCK)
JOIN Polaris.Polaris.AuthoritySubfields aus (NOLOCK)
ON (aut.AuthorityTagID = aus.AuthorityTagID)
WHERE aut.tagnumber = 100
AND aus.Subfield like 't'

If you are working on a query for a report, you will probably need to start with AuthorityRecords and join the tags and subfields table like this:

SELECT aur.AuthorityRecordID, aur.BrowseHeading
FROM Polaris.Polaris.AuthorityRecords aur WITH (NOLOCK)
JOIN Polaris.Polaris.AuthorityTags aut (NOLOCK)
ON (aur.AuthorityRecordID = aut.AuthorityRecordID)
JOIN Polaris.Polaris.AuthoritySubfields aus (NOLOCK)
ON (aut.AuthorityTagID = aus.AuthorityTagID)
WHERE aut.tagnumber = 100
AND aus.Subfield like 't'
AND aus.Data like '%adventures%'

If you are using the find tool, make sure that you are using the tool for the sort of record you want. (I've made that mistake before. ) You \_can\_ search for bibliographicrecords based on data in CircItemRecords, but you need to select the AssociatedBibRecordID instead of the ItemRecordID. If the ID you retrieve is not the right type of ID, you will either get no results, or very bad ones. ![smile](../../_resources/3288f5ce8dd04f27addf6f2392e017a5.gif)

Hope this helps.

JT

answered 2 years ago  by <img width="18" height="18" src="../../_resources/9e24c98cef0145dbbe283fbfa0ea90eb.jpg"/>  jwhitfield@aclib.us

Thanks, JT! I was actually pretty close but couldn't quite figure out where I was going wrong. I have been burned by being in the wrong place in the search tool before, so I was on the lookout for that!

Tara

— tarar@wacotx.gov 2 years ago

<a id="commentAns_707"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Authority Records](https://iii.rightanswers.com/portal/controller/view/discussion/580?)
- [Authority Control Vendors](https://iii.rightanswers.com/portal/controller/view/discussion/739?)
- [alternate title/author fields](https://iii.rightanswers.com/portal/controller/view/discussion/304?)
- [Authority Changes Made By Overdrive Integration](https://iii.rightanswers.com/portal/controller/view/discussion/567?)
- [Changing offensive LOC subject terms to local subject terms](https://iii.rightanswers.com/portal/controller/view/discussion/879?)

### Related Solutions

- [Database tables for Keyword and Browse indexing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930397363904)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [See Also Note Text in Browse Searches](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161115095911100)
- [See Also and See Also From References in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161114152724147)
- [Creating Authority Records for See Also From References](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161115093238839)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)