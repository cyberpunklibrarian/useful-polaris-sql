SQL for circulation by call number prefix and date range - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL for circulation by call number prefix and date range

0

28 views

Hi,

Does anyone have a SQL for circulation based on call number prefix during a specific period of time? We have VOX audiobooks, and VOX is in the prefix of all the items, but the prefix doesn't necessarily ***start*** with VOX (e.g, SPA VOX, SPANISH/ENGLISH VOX, VOX, etc). So what I'd like is a query that can show circulation numbers for 2019 vs 2020, for all VOX items (i.e., items with a prefix of \*VOX, or whatever the wildcard symbol is before VOX). Additionally (because our cataloging is not consistent), sometimes the prefix includes something after the VOX (like VOX EF) and sometimes it doesn't (e.g., the EF is in the Class), so it would be something like \*VOX* with wildcards before and after, if necessary. Any help? Thanks!

asked 8 months ago  by <img width="18" height="18" src="../../_resources/4c3eb36df5ad4f63b2c3755cc5529afe.jpg"/>  musack@bcls.lib.nj.us

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 8 months ago

<a id="upVoteAns_1048"></a>[](#)

0

<a id="downVoteAns_1048"></a>[](#)

Thank you so much!! That looks like what I need. I got stuck because I didn't know the exact name for Call Number Prefix and wasn't sure what to use as a wildcard. This will also help me in future SQL requests. Thanks again :)

answered 8 months ago  by <img width="18" height="18" src="../../_resources/4c3eb36df5ad4f63b2c3755cc5529afe.jpg"/>  musack@bcls.lib.nj.us

- <a id="acceptAnswer_1048"></a>[Accept as answer](#)

<a id="commentAns_1048"></a>[Add a Comment](#)

<a id="upVoteAns_1047"></a>[](#)

0

<a id="downVoteAns_1047"></a>[](#)

This should give you a count of checkouts:

SELECT COUNT (DISTINCT th.TransactionID) AS 'Circ Count'
FROM TransactionHeaders th (nolock)
JOIN TransactionDetails td (nolock)
ON th.TransactionID = td.TransactionID
JOIN ItemRecords ir (nolock)
ON td.numValue = ir.ItemRecordID
WHERE th.TransactionTypeID = 6001
AND td.TransactionSubTypeID = 38
AND th.TranClientDate BETWEEN '1/1/2020' AND '12/31/2020'
AND ir.CallNumberPrefix LIKE '%VOX%'

answered 8 months ago  by ![pgunderson@sandiego.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_pgunderson@sandiego.gov20210505173747.jpg)  pgunderson@sandiego.gov

- <a id="acceptAnswer_1047"></a>[Accept as answer](#)

<a id="commentAns_1047"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL query for circulation by call number range and date range](https://iii.rightanswers.com/portal/controller/view/discussion/190?)
- [I need help creating a SQL for circulation data that includes a lot of details.](https://iii.rightanswers.com/portal/controller/view/discussion/550?)
- [SQL for patron circ at a particular workstation](https://iii.rightanswers.com/portal/controller/view/discussion/918?)
- [How to circulate Museum Passes?](https://iii.rightanswers.com/portal/controller/view/discussion/423?)
- [COVID-19 Due Date Extensions, Fine Amnesty, Etc](https://iii.rightanswers.com/portal/controller/view/discussion/715?)

### Related Solutions

- [You Saved Message - What is the date range for YTD savings?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930616286891)
- [How do I reset the year-to-date circulation counts?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930831368342)
- [Bulk change item due dates](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930588598835)
- [What is the patron age update step in the patron processing job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930331250831)
- [Circulation statistics and deleted records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930736904760)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)