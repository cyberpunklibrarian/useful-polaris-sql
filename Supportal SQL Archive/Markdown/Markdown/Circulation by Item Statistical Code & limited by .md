Circulation by Item Statistical Code & limited by assigned collection? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Circulation by Item Statistical Code & limited by assigned collection?

0

39 views

Can anyone help me figure out a report I can run that will let me look at circulation over a time period, that looks at items from an assigned collection and pulls the circulation by item statistical code? I'm having no luck figuring this out in Simply Reports.

asked 1 year ago  by <img width="18" height="18" src="../../_resources/421e575febd947eb9655b1025302b1d7.jpg"/>  dreynolds@cityofcamas.us

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_958"></a>[](#)

0

<a id="downVoteAns_958"></a>[](#)

This might give you a starting point:

select
OG.name AS "Library Name", mt.description AS "Material Type", col.name AS "Collection Desc.",
ST.description AS "Statistical Code", count(distinct TD.transactionid)AS Circs
from polaristransactions.polaris.transactiondetails TD
join polaris.polaris.circitemrecords CI on CI.itemrecordid = TD.numvalue
join polaristransactions.polaris.transactionheaders TH on TH.transactionid = TD.transactionid
join polaris.polaris.statisticalcodes ST on ST.statisticalcodeid = CI.statisticalcodeid
Join polaris.polaris.organizations OG On og.organizationid = ci.assignedbranchid
Join polaris.polaris.collections COL On col.collectionid = ci.assignedcollectionid
Join polaris.polaris.materialtypes MT On mt.materialtypeid = ci.materialtypeid
where og.organizationid = 9
and th.TranClientDate between '2020-02-01 00:00:00.001'and '2020-03-01 00:00:00.001'
and TH.transactiontypeid = 6001
and TD.transactionsubtypeid = 38
group by OG.name, col.name, ST.description, mt.description
order by OG.name, col.name, ST.description, mt.description

Rex

answered 1 year ago  by <img width="18" height="18" src="../../_resources/421e575febd947eb9655b1025302b1d7.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_958"></a>[Accept as answer](#)

I wanted to add a quick comment to Rex's (excellent ![laughing](../../_resources/12fce8ad73b24fea90bb532fc1d4cc0e.gif)) query above. Since each organization can have its own statistical codes, sometimes a query will count each transaction once for each organization, so if you have 2 transactions and 10 branches in your StatisticalCodes table you may get a count of 20 instead of 2. The "distinct" in the count should take care of this. I'm not sure if it adds significantly to the query time (transactions queries can take a while) but if you try to remove the "distinct" to speed things up, you may run into the overcounting situation. Another approach may be to add a condition to the join to the StatisticalCodes table "and OrganizationID = 9"--or any other branch that has the same number of stat codes.

If you have branches with different stat code descriptions (one branch's Fantasy is another branch's Fiction) you may need to add the organization to the join in order to get the desired stat code descriptions.

You may be able to get similar information a bit quicker using the ItemRecordHistory table, but that depends on how long you retain the history. This table is also updated to add a day's history overnight, so you would have to look in ItemRecordHistoryDaily to get this morning's checkouts.

JT

— jwhitfield@aclib.us 1 year ago

I hope my emoticon didn't give the impression I was being sarcastic. I do think it's an excellent query and I'm very grateful for Rex's assistance with things SQL over the years. My apologies if it came across that way. JT

— jwhitfield@aclib.us 1 year ago

In the email notification the emoticon came through broken but no offense taken.  Luckely for me all our libraries have the identical Statistical codes so grouping by Description eliminates the problem you describe.

— rhelwig@flls.org 1 year ago

<a id="commentAns_958"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Circulation count of deleted items from a collection](https://iii.rightanswers.com/portal/controller/view/discussion/92?)
- [Shelf location circulation statistics](https://iii.rightanswers.com/portal/controller/view/discussion/441?)
- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [SQL for items returned late by collection](https://iii.rightanswers.com/portal/controller/view/discussion/718?)

### Related Solutions

- [Collection workform "branches" checkboxes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930545936496)
- [What to check after setting up a new code in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170228154215576)
- [Item statistical class code changes automatically](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181025152019128)
- [Item branch bulk change error](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930329171579)
- [EDI items coming in with the wrong collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930695455841)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)