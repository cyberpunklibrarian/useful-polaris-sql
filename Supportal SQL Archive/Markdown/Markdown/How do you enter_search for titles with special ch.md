[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# How do you enter/search for titles with special characters?

0

52 views

There is a new Rap CD out with the title of ? (Performer: XXXTentacion)  
In OCLC the record (#<a id="OBJ_PREFIX_DWT4362_com_zimbra_phone"></a>[1035016378](callto:1035016378)) has a bracketed title in the 245 \[Question mark\] and a 246 of ?  
<br/>However, if you try to search by ? even with an exact match (explicitly truncated) search, you cannot bring up the record. Is there a workaround for this kind of situation?  
<br/>Kathy Schmidt  
CCS  
<br/><a id="OBJ_PREFIX_DWT4363_ZmEmailObjectHandler"></a>kschmidt@ccslib.org

asked 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_a99d444adf594b0bbabddbb2c66c7672.jpg"/> kschmidt@ccslib.org

[cataloging](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=4#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 6 years ago

<a id="upVoteAns_351"></a>[](#)

0

<a id="downVoteAns_351"></a>[](#)

Hi Kathy.

I don't think there's a way to "escape" a question mark to search it as a literal in a regular or power search. Polaris help mentions a way to do a power search for a number/pound/hashtag (#) but I'm not sure if it would work with other symbols.

A browse might work, however. I tried browsing by "?" as a title and retrieved the beginning of the title browse list, which contains punctuation marks and symbols such as "#" and "$".  You would need to scroll up or down to see if "?" is in the list (we do not own it--yet).

You can also escape out wildcard characters in a SQL search, but it may be more complicated than you want to deal with.

&nbsp;

Hope this helps,

JT

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_a99d444adf594b0bbabddbb2c66c7672.jpg"/> jwhitfield@aclib.us

- <a id="acceptAnswer_351"></a>[Accept as answer](#)

<a id="commentAns_351"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [alternate title/author fields](https://iii.rightanswers.com/portal/controller/view/discussion/304?)
- [Missing Cover Art](https://iii.rightanswers.com/portal/controller/view/discussion/1191?)
- [TOM for audio-enabled books](https://iii.rightanswers.com/portal/controller/view/discussion/655?)
- [Deleting item records with holds](https://iii.rightanswers.com/portal/controller/view/discussion/32?)
- [How to you correct a terminated import job?](https://iii.rightanswers.com/portal/controller/view/discussion/131?)

### Related Solutions

- [007 LDR Cataloging Question](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930329896653)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Selecting a bibliographic style when printing a bibliographic record set](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930306605978)
- [Why are some titles not displaying correctly when doing a browse search?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930659750468)
- [How are item records created from embedded holdings tags?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930494756808)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)