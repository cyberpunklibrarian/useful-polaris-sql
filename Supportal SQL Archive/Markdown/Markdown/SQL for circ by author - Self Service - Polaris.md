SQL for circ by author - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for circ by author

0

46 views

Is there a SQL for circulation by author? One of my selectors is reviewing our AY authors and wants to see how each author circulates. TIA!

asked 6 months ago  by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg)  sbills@lpld.lib.in.us

[sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 6 months ago

<a id="upVoteAns_1078"></a>[](#)

0

<a id="downVoteAns_1078"></a>[](#)

Hi! I borrowed this query a long time ago so I'm not sure who to give credit to. The caveats below are original, but I did add comments to the query to make it clearer for myself in the beginning. If you want a report of ALL authors, just remove the 'top 10' from the select statement:

You will probably need to change your collection id, statistical code id, and dates, and maybe your assigned branch id. There are a couple of caveats with this query.
1) The 100 tag in all of your MARC records must match exactly, including punctuation and spaces. We had to do quite a bit of bib record and authority record cleanup before we got a report we could trust.
2) If there are authors with the same number of circulations at the tail end of the report, they will order randomly each time you run the report. Actually, I’m sure it’s not random but I haven’t invested the time to see what the order is. That wasn’t a big deal for us, but if you want to make sure there are no ties being excluded, you could change the first part of the statement to something like “SELECT top 20.”
select top 10 br.browseauthor as Author,
count(th.Transactionid) as Circs
from PolarisTransactions.Polaris.TransactionHeaders TH (nolock)
JOIN PolarisTransactions.Polaris.TransactionDetails TD (nolock)
on th.Transactionid = td.transactionid
JOIN Polaris.CircItemRecords CIR (nolock)
on CIR.ItemRecordID=TD.numvalue
JOIN Polaris.bibliographicrecords br (nolock)
on CIR.AssociatedBibRecordID=BR.BibliographicRecordID
where th.transactiontypeid=6001
and td.transactionsubtypeid=38
and th.TranClientDate > '1-1-2019' -- date range
and th.TranClientDate < '12-31-2021'
and cir.assignedbranchid IN (3) --branch location
and cir.assignedcollectionid IN (47,48,49,50,51,52,53,54,55)--collection code
--and cir.StatisticalCodeID IN () --stat code, if needed
and br.browseauthor is not null
group by br.browseauthor
order by count(th.Transactionid) DESC

answered 6 months ago  by <img width="18" height="18" src="../../_resources/08627c79a7144da0b1f61f0d4195d844.jpg"/>  abrookins@cvlga.org

- <a id="acceptAnswer_1078"></a>[Accept as answer](#)

<a id="commentAns_1078"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL help for for circ info on 'new' stuff](https://iii.rightanswers.com/portal/controller/view/discussion/698?)
- [SimplyReports or SQL to get circ count by a UDF?](https://iii.rightanswers.com/portal/controller/view/discussion/82?)
- [SQL or report for circ stats by Patron Statistical Class?](https://iii.rightanswers.com/portal/controller/view/discussion/488?)
- [SimplyReports or SQL to get circ count by a UDF?](https://iii.rightanswers.com/portal/controller/view/discussion/88?)
- [Does anyone have an SQL report for a specific tag in Authority records?](https://iii.rightanswers.com/portal/controller/view/discussion/625?)

### Related Solutions

- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [How do I schedule the Year End Circ Count Rollover job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930582726474)
- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)
- [How do I verify headings in SkyRiver?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=190814171235639)
- [See Also and See Also From References in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161114152724147)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)