Add location to an SQL statement - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Add location to an SQL statement

0

56 views

The following SQL statement pulls items without a price using the F12 search tool:

Select ItemRecordID as RecordID
From ItemRecordDetails (nolock)
Where Price is NULL

Is there a way to add location(s)?

Thanks in advance,

Anne Krulik

asked 1 year ago  by <img width="18" height="18" src="../../_resources/a7a2be3b03464665af88c38f256532d3.jpg"/>  akrulik@bcls.lib.nj.us

[item record](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=268#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 1 year ago

<a id="upVoteAns_757"></a>[](#)

0

<a id="downVoteAns_757"></a>[](#)

Answer Accepted

Hi Anne.

Locations such as branch and shelf location live in CircItemRecords, but there is a view called itemrecords that combines CircItemRecords and ItemRecordDetails.

If you want to search for items at branches 3, 4, and 9, you can do the following

Select ItemRecordID as RecordID
From ItemRecords (nolock)
Where Price is NULL
And AssignedBranchID In (3,4,9)

If you want to include most branches, you can use ...AssignedBranchID Not In (5,12,15)

If you are concerned about a shelf location, you can use ShelfLocationID = 2 or ShelfLocationID in (2,4).

For a temporary shelf location (free text) you would need to use something like TemporaryShelfLocation Like 'ChildrensHolidayDisplay' (in single quotes). You can truncate this by using a percent sign % before, after, or even in the middle of the word(s).

Hope this is helpful (and not over-helpful ![smile](../../_resources/afb5441ae18c43a99fc9f56440360fcc.gif)).

JT

answered 1 year ago  by <img width="18" height="18" src="../../_resources/a7a2be3b03464665af88c38f256532d3.jpg"/>  jwhitfield@aclib.us

Thanks JT and Trevor for responding.  Adding either 'And AssignedBranchID In (3,4,9)' or 'AssignedBranchID Not In (5,12,15)' using our branch numbers works.

— akrulik@bcls.lib.nj.us 1 year ago

<a id="commentAns_757"></a>[Add a Comment](#)

<a id="upVoteAns_758"></a>[](#)

0

<a id="downVoteAns_758"></a>[](#)

What I mean by location is the item's assigned branch in the item record.

answered 1 year ago  by <img width="18" height="18" src="../../_resources/a7a2be3b03464665af88c38f256532d3.jpg"/>  akrulik@bcls.lib.nj.us

- <a id="acceptAnswer_758"></a>[Accept as answer](#)

<a id="commentAns_758"></a>[Add a Comment](#)

<a id="upVoteAns_756"></a>[](#)

0

<a id="downVoteAns_756"></a>[](#)

What do you mean by location?

answered 1 year ago  by <img width="18" height="18" src="../../_resources/a7a2be3b03464665af88c38f256532d3.jpg"/>  trevor.diamond@mainlib.org

- <a id="acceptAnswer_756"></a>[Accept as answer](#)

<a id="commentAns_756"></a>[Add a Comment](#)

## Your Reply

&lt;p&gt;&nbsp;&lt;br&gt;&lt;/p&gt;

Attach file...

### Similar Questions

- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [Shelf location circulation statistics](https://iii.rightanswers.com/portal/controller/view/discussion/441?)
- [Report for patrons with items out. If possible, sort by location?](https://iii.rightanswers.com/portal/controller/view/discussion/777?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)
- [SQL for changes to a patron record](https://iii.rightanswers.com/portal/controller/view/discussion/840?)

### Related Solutions

- [Unable to delete two holds, error message about ILL$Messages appears](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930410469399)
- [Add a new Payment Method for our "Food for Fines" drive](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161026092701843)
- [A New Branch](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161228114330859)
- [Add Shelf Locations - System Level](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930254225543)
- [How can I locate bibs that were generated during a Terminated import without using SQL Server Management Studio?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930570115337)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)