[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [PAC](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=14)

# Newreleases.aspx has been showing the same items for a while despite new aquisitions.

0

28 views

Hi Everyone,

&nbsp;

On our page https://suffolk.polarislibrary.com/polaris/Search/newreleases.aspx?ListingTypeID=28 , it's showing a list that doesn't include new acquisitions.  I don't know what to check.

Thank you

asked 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_27d3221bd4c6439bb6d3dcf9d19d0859.jpg"/> bgoldberg@suffolkva.us

[dashboard new titles](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=404#t=commResults) [new releases](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=405#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 3 years ago

<a id="upVoteAns_1051"></a>[](#)

0

<a id="downVoteAns_1051"></a>[](#)

I'd have your site manager check and make sure that the Dashboard New Titles SQL job is running properly.

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_27d3221bd4c6439bb6d3dcf9d19d0859.jpg"/> wosborn@clcohio.org

- <a id="acceptAnswer_1051"></a>[Accept as answer](#)

<a id="commentAns_1051"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [What causes the Reading History Title to not show?](https://iii.rightanswers.com/portal/controller/view/discussion/53?)
- [Does anyone have a PAC instructional handout for new patrons?](https://iii.rightanswers.com/portal/controller/view/discussion/498?)
- [Change external PAC header links to open in new window?](https://iii.rightanswers.com/portal/controller/view/discussion/964?)
- [Is the new responsive pac ADA compliant?](https://iii.rightanswers.com/portal/controller/view/discussion/247?)
- [Finding In-Processing items in the PAC](https://iii.rightanswers.com/portal/controller/view/discussion/493?)

### Related Solutions

- [Request did not appear on Pending list despite "In" item](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930397510485)
- [Claimed items still showing on email Overdue notices even when set not to show](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930396938713)
- [Polaris Vega: Creating a New Materials Showcase based on item data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=231005164230383)
- [Creating a new collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930912772907)
- [When does an item show up on the "Holds to Action" picklist?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240417134311717)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)