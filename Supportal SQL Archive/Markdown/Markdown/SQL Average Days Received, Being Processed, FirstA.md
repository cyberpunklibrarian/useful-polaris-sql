[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL Average Days Received, Being Processed, FirstAvailableDate

0

76 views

I wrote a SQL query for finding the average days between when an item is received, to the last day it is Being Processed before the First Available Date.  It looks like it is working great when I run it in SSMS, but would like a second opinion just to make sure I'm not missing something.  \*I did it this way so I could see what  it was pullingand what it was doing. Will find the total Averages later.

&nbsp;

SELECT  
cir.AssignedBranchID,  
cir.MaterialTypeID,  
cir.barcode,  
MIN(irh1.TransactionDate) as Received,  
MAX(irh2.TransactionDate) as BeingProcessed,  
cir.FirstAvailableDate,  
DATEDIFF (Day, MIN(irh1.TransactionDate), MAX(irh2.TransactionDate)) AS NumberOfDaysFromReceivedToLastDayBeingProcessed,  
DATEDIFF (Day, MAX(irh2.TransactionDate),(Cir.FirstAvailableDate)) AS NumberOfDaysFromLastDayBeingProcessedToFirstAvailableDate,  
DATEDIFF (Day, MIN(irh1.TransactionDate),(Cir.FirstAvailableDate)) AS NumberOfDaysFromReceivedToFirstAvailableDate

FROM  
Polaris.ItemRecordHistory irh1 with (nolock)  
INNER JOIN Polaris.ItemRecordHistory irh2 with (nolock)  
ON irh1.ItemRecordID = irh2.ItemRecordID  
INNER JOIN POLARIS.CircItemRecords cir with (nolock)  
ON cir.ItemRecordID = irh1.ItemRecordID

WHERE  
irh1.OldItemStatusID = 13  
and irh1.NewItemStatusID = 15  
and irh2.NewItemStatusID = 15  
and irh1.TransactionDate between '2022-12-01' and '2022-12-31 23:59:59' -- date item Received  
and irh2.TransactionDate between'2022-12-01' and '2023-03-01 23:59:59' -- Last date item Being Processed  
and cir.FirstAvailableDate between '2022-12-01' and '2023-03-01 23:59:59'--- First Available Date  
and cir.AssignedBranchID = 10  
GROUP BY  
cir.AssignedBranchID, cir.barcode, cir.FirstAvailableDate, irh1.TransactionDate, cir.MaterialTypeID  
ORDER BY  
irh1.TransactionDate asc

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_d5cec190adc747a28e625c44aedc2531.jpg"/> scole@slcolibrary.org

<a id="comment"></a>[Add a Comment](#)

## 6 Replies   last reply 1 year ago

<a id="upVoteAns_1362"></a>[](#)

0

<a id="downVoteAns_1362"></a>[](#)

Answer Accepted

I'll keep the answer out of the text in case you want to try and figure it out yourself first, but I think the following SQL should fix it:

&nbsp;

```javascript
select cir.AssignedBranchID
	  ,cir.MaterialTypeID
	  ,cir.barcode
	  ,min(irh1.TransactionDate)										   as Received
	  ,max(irh2.TransactionDate)										   as BeingProcessed
	  ,cir.FirstAvailableDate
	  ,datediff(day, min(irh1.TransactionDate), max(irh2.TransactionDate)) as NumberOfDaysFromReceivedToLastDayBeingProcessed
	  ,datediff(day, max(irh2.TransactionDate), (Cir.FirstAvailableDate))  as NumberOfDaysFromLastDayBeingProcessedToFirstAvailableDate
	  ,datediff(day, min(irh1.TransactionDate), (Cir.FirstAvailableDate))  as NumberOfDaysFromReceivedToFirstAvailableDate

from Polaris.ItemRecordHistory irh1 with (nolock)
inner join Polaris.ItemRecordHistory irh2 with (nolock)
	on irh1.ItemRecordID = irh2.ItemRecordID
inner join POLARIS.CircItemRecords cir with (nolock)
	on cir.ItemRecordID = irh1.ItemRecordID

where irh1.OldItemStatusID = 13
	and irh1.NewItemStatusID = 15
	and irh2.NewItemStatusID = 15
	and irh1.TransactionDate between '2022-12-01' and '2022-12-31 23:59:59'  -- date item Received
	and irh2.TransactionDate between '2022-12-01' and cir.FirstAvailableDate -- Last date item Being Processed
	and cir.FirstAvailableDate between '2022-12-01' and '2023-03-01 23:59:59'-- First Available Date
	and cir.AssignedBranchID = 10
group by cir.AssignedBranchID, cir.barcode, cir.FirstAvailableDate, irh1.TransactionDate, cir.MaterialTypeID
order by irh1.TransactionDate asc
```

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_d5cec190adc747a28e625c44aedc2531.jpg"/> mfields@clcohio.org

<a id="commentAns_1362"></a>[Add a Comment](#)

<a id="upVoteAns_1363"></a>[](#)

0

<a id="downVoteAns_1363"></a>[](#)

AHHH I was so close! First Available Date!  That's what I thought I was doing in the Select statement. Thank you so much! I've already spent a lot of time on it. 

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_d5cec190adc747a28e625c44aedc2531.jpg"/> scole@slcolibrary.org

- <a id="acceptAnswer_1363"></a>[Accept as answer](#)

<a id="commentAns_1363"></a>[Add a Comment](#)

<a id="upVoteAns_1361"></a>[](#)

0

<a id="downVoteAns_1361"></a>[](#)

Ah, bummer. I looked through my results and did find some negative numbers (thought I had searched with the find tool before but must not have done it throughly). I will keep working on it. Thank you for your help! 

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_d5cec190adc747a28e625c44aedc2531.jpg"/> scole@slcolibrary.org

- <a id="acceptAnswer_1361"></a>[Accept as answer](#)

<a id="commentAns_1361"></a>[Add a Comment](#)

<a id="upVoteAns_1360"></a>[](#)

0

<a id="downVoteAns_1360"></a>[](#)

Hm, I really wish I hadn't closed the first instance I had looked at cause I must have inadvertently goofed something up after using the formatter to make it more readable that made the problem look significantly worse. You're correct, as far as I know, that the first available date doesn't generally update after being set. Though because of that I think that even if the end dates are the same you can still potentially run into issues where an item becomes available early in the range and is then processed again later in the range, giving it a negative NumberOfDaysFromLastDayBeingProcessedToFirstAvailableDate. This seems like it should be fairly rare though as even in our quite large system we only have 4 instances out of ~43,500 items processed across all locations during your default time period.

&nbsp;

Here's what the output looks like for those four items if it's helpful:

&nbsp;

```markup
AssignedBranchID MaterialTypeID barcode              Received                BeingProcessed          FirstAvailableDate      NumberOfDaysFromReceivedToLastDayBeingProcessed NumberOfDaysFromLastDayBeingProcessedToFirstAvailableDate NumberOfDaysFromReceivedToFirstAvailableDate
---------------- -------------- -------------------- ----------------------- ----------------------- ----------------------- ----------------------------------------------- --------------------------------------------------------- --------------------------------------------
62               8              1339379939           2022-12-02 04:06:48.420 2023-02-16 15:27:10.473 2022-12-08 11:08:13.787 76                                              -70                                                       6
44               8              1339379940           2022-12-02 04:06:48.483 2023-02-16 15:27:10.490 2022-12-09 07:39:56.870 76                                              -69                                                       7
43               8              1339379941           2022-12-06 04:07:57.437 2023-02-16 15:29:13.633 2022-12-16 13:14:20.563 72                                              -62                                                       10
7                8              31870009316968       2022-12-15 16:32:17.883 2023-01-13 09:49:40.523 2022-12-27 09:46:08.730 29                                              -17                                                       12
```

&nbsp;

Edit: Ugh, that code block isn't nearly as useful in the actual post as it was in the preview. Oh well.

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_d5cec190adc747a28e625c44aedc2531.jpg"/> mfields@clcohio.org

- <a id="acceptAnswer_1360"></a>[Accept as answer](#)

<a id="commentAns_1360"></a>[Add a Comment](#)

<a id="upVoteAns_1359"></a>[](#)

0

<a id="downVoteAns_1359"></a>[](#)

Hi,

Thanks for replying! I was running into that problem, but I think as long as you make sure to set the  irh2s betweens maximum date the same as the First Available date it will choose the Being Processed max date before the First Available Date (which I believe never changes even if the item goes back to being processed or some other status?). 

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_d5cec190adc747a28e625c44aedc2531.jpg"/> scole@slcolibrary.org

- <a id="acceptAnswer_1359"></a>[Accept as answer](#)

<a id="commentAns_1359"></a>[Add a Comment](#)

<a id="upVoteAns_1358"></a>[](#)

0

<a id="downVoteAns_1358"></a>[](#)

I think you're on the right track. The only thing that stands out to me is that you may want to add another condition to limit the irh2 table to only transactions that occurred before the item's FirstAvailableDate. Not sure if you'll run into the same issue, but I see a bunch of items in our system that were processed again after becoming available which was giving some really odd numbers.

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_d5cec190adc747a28e625c44aedc2531.jpg"/> mfields@clcohio.org

- <a id="acceptAnswer_1358"></a>[Accept as answer](#)

<a id="commentAns_1358"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Average hold wait times](https://iii.rightanswers.com/portal/controller/view/discussion/845?)
- [Average time things are lost before coming back?](https://iii.rightanswers.com/portal/controller/view/discussion/436?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)
- [SQL - 650 subject](https://iii.rightanswers.com/portal/controller/view/discussion/1228?)
- [Help with automatically exporting SQL query to CSV file](https://iii.rightanswers.com/portal/controller/view/discussion/1399?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Hold Processing SQL jobs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930920183263)
- [Saved Searches emails are not being received](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930389681864)
- [Not receiving summary e-mails for the Hold Notice Processing job](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930706028849)
- [Is it safe to run the EDI SQL job more than once a day](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=190124092946263)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)