SQL for "Exclude from notices and reminders" - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL for "Exclude from notices and reminders"

0

38 views

Is there a SQL for patron accounts that have "Exclude from notices and reminders" check boxes checked?

asked 1 year ago  by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg)  sbills@lpld.lib.in.us

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_937"></a>[](#)

0

<a id="downVoteAns_937"></a>[](#)

Answer Accepted

Well, there are six of them in that group, but if you want just any of them:

SELECT PatronID
FROM PatronRegistration with (NOLOCK)
WHERE ExcludeFromOverdues = 1
OR ExcludeFromHolds = 1
OR ExcludeFromBills = 1
OR ExcludeFromAlmostOverdueAutoRenew = 1
OR ExcludeFromPatronRecExpiration = 1
OR ExcludeFromInactivePatron = 1

answered 1 year ago  by <img width="18" height="18" src="../../_resources/eb84f946a83a477b8b5aac338f39ad2b.jpg"/>  jjack@aclib.us

Yes, I wanted all. Thank you!!

— sbills@lpld.lib.in.us 1 year ago

You're welcome!  It's only just occurred to me that that could be refined further, e.g. to only pull up non-expired accounts.  If it's including results you'd rather *exclude* and can't figure out how to exclude them, please let me know and I can take another look.

— jjack@aclib.us 1 year ago

We don't want any of those boxes checked except for our mobile patrons, so I just wanted to see how big of a clean up I had on my hands. ;-) I really appreciate your willingness to help!

— sbills@lpld.lib.in.us 1 year ago

<a id="commentAns_937"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)
- [SQL for Patron Name Titles](https://iii.rightanswers.com/portal/controller/view/discussion/836?)
- [SQL for Claim Returned items](https://iii.rightanswers.com/portal/controller/view/discussion/855?)
- [SQL for patron circ at a particular workstation](https://iii.rightanswers.com/portal/controller/view/discussion/918?)
- [Does anyone have an SQL for a request ratio?](https://iii.rightanswers.com/portal/controller/view/discussion/394?)

### Related Solutions

- [Query to find unfillable holds due to volume data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930242167096)
- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)
- [Hold Processing SQL jobs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930920183263)
- [Add a new Payment Method for our "Food for Fines" drive](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161026092701843)
- [Error: GetPatronPreCirc() failed](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170922112726384)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)