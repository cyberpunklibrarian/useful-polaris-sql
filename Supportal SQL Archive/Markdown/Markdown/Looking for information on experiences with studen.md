[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Looking for information on experiences with student IDs as library cards

0

91 views

Before we start actively working our local school district we would like to know what other libraries experiences have been with adding students from local school districts into Polaris.  We are hoping to reach out to every student and get library access into their hot little hands!  What are your experiences with using the student ID as a library card, and for the younger student without ID what other ways have you used so that they can identfity themselves when checking out, or are you issuing every student a regular library card?  What material limits or check out limits do you have? Did you have to involve iii to import the records, did you manually add them or used an in-house SQL to add accounts?  Did you have parents sign an agreement or a memorandum of understanding with the school district?

We are looking for everything and anything you may have to share!

Thank you, be safe and stay well!

Trish Pelletier

tp@ajcity.net

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_1efb6decfdab49738c0701f7626b8c7f.jpg"/> tp@ajcity.net

[alternate library cards](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=375#t=commResults) [importing student records](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=376#t=commResults) [library cards](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=377#t=commResults) [student accounts](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=378#t=commResults) [student cards](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=134#t=commResults) [student library cards](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=379#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 4 years ago

<a id="upVoteAns_967"></a>[](#)

0

<a id="downVoteAns_967"></a>[](#)

Hi Trish,

We've been doing this for years and I'd be happy to chat!

Quick rundown:

Elementary students are given a library card with an instructional flyer on how to go to a website to activate their card.

The school district sends us an Excel file of the middle and high school students which I then import into Polaris using the bookmobile.

I had to make configuration changes to our Bibliotheca self-checks to get them to recognize the student IDs (as some are 7, 8, or 10 digits), and I believe our electronic resources librarian had to do some negotiating with our eContent vendors to allow the students access.

The school district distributes opt-out forms to parents in case they don't want their child involved in this program.  It's up to the district to not include the students who opt out in the export to us.

Students who already had library cards before getting ones linked to their student IDs can either choose to have the records merged or continue to have two separate accounts.

We have a separate patron code for the student records, but it is set to the same material type loan limits as our normal resident cards.  We're completely fine free so there aren't any changes to make there (it used to be that students were fine free, while residents were not).

Please email me if you'd like to set up some time for phone call to answer any more questions or to pick my brain about the process,

Gabrielle

ggosselin@richlandlibrary.com

answered 4 years ago by ![ggosselin@richlandlibrary.com](https://iii.rightanswers.com/portal/app/images/custom/avatar_ggosselin@richlandlibrary.com20170530111420.JPG) ggosselin@richlandlibrary.com

- <a id="acceptAnswer_967"></a>[Accept as answer](#)

<a id="commentAns_967"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Looking to gather Libraries' experience with Student Cards](https://iii.rightanswers.com/portal/controller/view/discussion/444?)
- [Looking for Info on Student Library Cards](https://iii.rightanswers.com/portal/controller/view/discussion/607?)
- [Experiences Using Student IDs and Bookmobile TRN](https://iii.rightanswers.com/portal/controller/view/discussion/1255?)
- [Student cards - allow checkout of items?](https://iii.rightanswers.com/portal/controller/view/discussion/248?)
- [Besides Quipu, does any know of any other products that integrate with Polaris, allows patrons to self-register for a library card online, and verifies and standardizes the address information entered as well as provides name and residency verification?](https://iii.rightanswers.com/portal/controller/view/discussion/253?)

### Related Solutions

- [When is the “Former barcode” field in Patron Registration updated?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170126165932698)
- [Library contact information on eReceipt](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930457677951)
- [Importing Student Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930247180454)
- [Patron Record Import TRN Format - Polaris 6.5](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200513113659590)
- [Self-Check credit card transaction do not appear in the Polaris Credit Card Manager](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930951221225)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)