SQL for Overdue fines - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for Overdue fines

0

124 views

Does anybody have an SQL to find current outstanding overdue fines by location? We'd like to exclude lost fees and debt collect fees.

Thanks!

Mel

asked 2 years ago  by <img width="18" height="18" src="../../_resources/a65e71346ea444c79d6978adb2a7a171.jpg"/>  musack@bcls.lib.nj.us

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 2 years ago

<a id="upVoteAns_651"></a>[](#)

0

<a id="downVoteAns_651"></a>[](#)

Thank you both so much! We'll check them out, but I think this should do it!

answered 2 years ago  by <img width="18" height="18" src="../../_resources/a65e71346ea444c79d6978adb2a7a171.jpg"/>  musack@bcls.lib.nj.us

- <a id="acceptAnswer_651"></a>[Accept as answer](#)

<a id="commentAns_651"></a>[Add a Comment](#)

<a id="upVoteAns_650"></a>[](#)

0

<a id="downVoteAns_650"></a>[](#)

I created the following SQL based on an 'out of the box' View, then created this SQL into a view for use with Power BI & Excel Power Query for conducting measures on the data, such as limiting by block amounts, accounts that only owe overdue fines, etc. You will need to determine if you have other fee codes that are tied to staff issued fines (not overdue) but as far as Polaris 'Out of the Box' Overdue Fines they are covered with Txn Code 1 = Charge &FeeReasonCode 0 = Overdue Item.

I am sure it could be better, but I was rushed to get the data together. Hope this helps.

Field Definitions

- **PatronID:** Unique Account ID, primary key for database relationship referencing.
- **Branch:** The name of the branch as displayed in the system settings.
- **Age Group:** A calculated column based on Birthdate to determine patron age using Julian year, then converted to a grouping of Adult, Juvenile, & Unknown.
- **Patron Code:** The description of the patron code. Patron codes define available services, loan periods, fines, and limits for specific classes of patrons.
- **Status****:** A calculated column based on Last Activity Date, Entry Date (when Last Activity date is unknown) and Expiration Date to determine if the patron is active within the last three (3) years.
- **Status Definitions**
    
    **Active account:** Accounts with a Last Activity Date within the last three (3) years.
    
    **Inactive Account**: Accounts with a Last Activity Date within the last three years.
    
    **Purge Account:** Accounts with Last Activity Date AND Expiration Date over three (3) years is age. In the event Last Activity Date is unavailable, Entry Date is used as the Last Activity Date.
    
- **ItemsOut:** A calculated column that counts the number of items currently checked out to the patron.
- **ItemsOverdue:** A calculated column that counts the number of items that are checked out and overdue.
- **Outstanding Fines:** A calculated column that sums the Outstanding Amount when a record is a Charge (TxnCodeID =1) and the Fee Reason is Overdue Item (FeeReasonCodeID = 0).
- **Outstanding Fees:** A calculated column that sums the Outstanding Amount when a record is a Charge (TxnCodeID =1) and the Fee Reason is NOT Overdue Item (FeeReasonCodeID not in (0)).
- **Outstanding Total:** A calculated column that sums the Outstanding Amount when a record is a Charge (TxnCodeID =1) regardless of Fee Reason.

* * *

SELECT PatronID
 ,\[Branch\] = BranchName
 ,\[Age Group\] = CASE WHEN Birthdate is null THEN 'Unknown' ELSE (CASE WHEN (DATEDIFF(hour,Birthdate,GETDATE())/8766+1) between 0 and 17 THEN 'Juvenile' ELSE 'Adult' END) END
 ,\[Patron Code\] =PCodeDescription
 ,\[Status\] = CASE WHEN DATEDIFF(day,getdate(),(COALESCE (\[LastActivityDate\],\[EntryDate\])))<-1095 -- Greater than 3 Years from today
      AND DATEDIFF(day,getdate(),\[ExpirationDate\])<-1095 -- Greater than 3 Years from today
     THEN 'Purge'
     ELSE (CASE WHEN DATEDIFF(day,getdate(),\[LastActivityDate\])>=-1095 -- Less than 3 Years from today
       THEN 'Active' ELSE 'Inactive' END)
     END
 ,\[ItemsOut\] = (SELECT Count(icko.ItemRecordID)
      FROM Polaris.Polaris.ItemCheckouts icko WITH (NOLOCK)
      WHERE icko.PatronID = pr.PatronID)
 ,\[ItemsOverdue\] = (SELECT Count(icko.ItemRecordID)
      FROM Polaris.Polaris.ItemCheckouts icko WITH (NOLOCK)
      WHERE icko.PatronID = pr.PatronID
       AND icko.DueDate < getdate()-1)
 , \[OutstandingFines\] = (SELECT SUM(OutstandingAmount)
        FROM Polaris.Polaris.PatronAccount AS PA WITH (NOLOCK)
        WHERE PA.TxnCodeID = 1   --1 Charge
         AND pa.FeeReasonCodeID = 0 --0 Overdue
         AND pr.PatronID = PA.PatronID)
 , \[OutstandingFees\] = (SELECT SUM(OutstandingAmount)
        FROM Polaris.Polaris.PatronAccount AS PA WITH (NOLOCK)
        WHERE PA.TxnCodeID = 1   --1 Charge
         AND (pa.FeeReasonCodeID not in (0) --0 Overdue
          OR pa.FeeReasonCodeID is null)
         AND pr.PatronID = PA.PatronID)
 , \[OutstandingTotal\] = (SELECT SUM(OutstandingAmount)
        FROM Polaris.Polaris.PatronAccount AS PA WITH (NOLOCK)
        WHERE PA.TxnCodeID = 1   --1 Charge
         AND pr.PatronID = PA.PatronID)
FROM Polaris.Polaris.ViewPatronRegistration pr (nolock)

* * *

answered 2 years ago  by ![eric.young@phoenix.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_eric.young@phoenix.gov20191029113427.jpg)  eric.young@phoenix.gov

- <a id="acceptAnswer_650"></a>[Accept as answer](#)

<a id="commentAns_650"></a>[Add a Comment](#)

<a id="upVoteAns_649"></a>[](#)

0

<a id="downVoteAns_649"></a>[](#)

I've added all kinds of date range criteria because we did a waiver of old debt (more than 3 years old), but this could do the trick for you! 

select pa.PatronID, o.Name, SUM(OutstandingAmount)
from Polaris.PatronAccount pa with (nolock)
join polaris.Patrons p (nolock)
on pa.PatronID = p.PatronID
join polaris.Organizations o (nolock)
on pa.PatronBranchID = o.OrganizationID
WHERE o.OrganizationID = 3 --insert desired organization ID
and pa.TxnID in (
select distinct TxnID from Polaris.PatronAccount with (nolock)
where TxnCodeID in (1)
AND FeeReasonCodeID = 0 --overdue fee reason code --OR --NOT IN (all excluded codes)
and OutstandingAmount > 0
--and TxnDate < DATEADD (yy, -3, getdate()) --age of transaction if needed
)
GROUP BY pa.PatronID, o.Name
--HAVING SUM(pa.OutstandingAmount) > 50 --enter amount if you're looking for certain account balances

answered 2 years ago  by <img width="18" height="18" src="../../_resources/a65e71346ea444c79d6978adb2a7a171.jpg"/>  abrookins@cvlga.org

- <a id="acceptAnswer_649"></a>[Accept as answer](#)

<a id="commentAns_649"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Anyone have a sql script that will total fines for a year and then break them down by patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/688?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [SQL - looking for a report on how many fines we have for a specific collection code and how many were waived.](https://iii.rightanswers.com/portal/controller/view/discussion/673?)
- [Does anyone know of a way to remove a library's telephone number on the return address portion of a printed overdue notice?](https://iii.rightanswers.com/portal/controller/view/discussion/1021?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)

### Related Solutions

- [Replacement fines were not automatically waived after overdue paid](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930589018439)
- [How are overdue fines calculated?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930310567045)
- [Where are the waive and charge free text notes stored in the database?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930334007926)
- [When are fine amounts included in notices?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930389144292)
- [Patron Status does not show a reminder that is in the NotificationHistory table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930782414146)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)