Creating a dropdown menu for a parameter so that it works in Polaris client reports - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Creating a dropdown menu for a parameter so that it works in Polaris client reports

0

50 views

Hello -

I'm working with SSRS and Report Builder.  What I’m trying to do is create a dropdown menu to set a parameter that gets used in the query as a call number prefix.  The labels in the parameter menu are things like ‘Juvenile’ or ‘CD Book’ and the values are ‘J ‘ or ‘CDB ‘ and so forth.  I've found some discussion online about quirks with the client reports, but nothing that addresses this directly.

 What I’ve tried so far:

- In Report Builder, in the parameter properties, don’t specify labels and values, instead have them come from a query
- Name the parameter beginning with GIS_ .

 What I’ve learned:

- The above tweaks aren’t enough – the dropdown menu works fine in Reporting Services on the web, but it doesn’t work in the client Reports and Notices.  There, it will show up as a menu, but the menu isn’t populated.
- I have other parameters in this report that are set up the same way, but the difference is that the possible values are integers rather than text.  For example, a menu for the user to choose a branch, where the labels are branch names but the values are OrganizationIDs.  Those dropdown menus function correctly in the client.  But I need to have the values in this “Prefix” parameter be text. 

 Questions:

- Is it just not possible to have the values be text instead of integers?  I hope not!
- What else is causing this?
    

Thanks very much for any insight or help!

Bill

asked 1 year ago  by <img width="18" height="18" src="../../_resources/7d3da5f01ae245639ac9b0841d99dc67.jpg"/>  wtaylor@washcolibrary.org

[dropdown menu](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=358#t=commResults) [report builder](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=151#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_943"></a>[](#)

0

<a id="downVoteAns_943"></a>[](#)

JT -

Thank you so much!  That does the trick.  I really appreciate your taking the time to do this.

Bill

answered 1 year ago  by <img width="18" height="18" src="../../_resources/7d3da5f01ae245639ac9b0841d99dc67.jpg"/>  wtaylor@washcolibrary.org

- <a id="acceptAnswer_943"></a>[Accept as answer](#)

<a id="commentAns_943"></a>[Add a Comment](#)

<a id="upVoteAns_939"></a>[](#)

0

<a id="downVoteAns_939"></a>[](#)

Hi Bill.

I managed to figure out a way that may work for you. I think you identified the problem in that Reports and Notices is not happy with text values in a parameter.

Here's what I did to get around this:

Create a report that has two datasets. One dataset, CallNumbers, uses a temporary table to assign an integer value to the call numbers. The query I used is below:

CREATE TABLE #CallPrefixes
(prid int
,prfix nvarchar(20)
)

INSERT INTO #CallPrefixes (prid, prfix)
VALUES
('1','CD JFICTION')
,('2','CD POP-ROCK')
,('3','CD JAZZ')
SELECT * FROM #CallPrefixes WITH (NOLOCK)

DROP TABLE #CallPrefixes

The second dataset, DataSet1, is the "main" dataset query that pulls data for the report. Here is its query:

DECLARE @prefstr nvarchar(20)

IF @pref = 1 SET @prefstr = 'CD JFICTION'
IF @pref = 2 SET @prefstr = 'CD POP-ROCK'
IF @pref = 2 SET @prefstr = 'CD JAZZ'

SELECT ItemRecordID, Barcode, CallNumber from Polaris.Polaris.ItemRecords WITH (NOLOCK)
WHERE RecordStatusID = 1
AND CallNumberPrefix LIKE @prefstr

The variable @pref is mapped to the parameter of the same name. This parameter uses the prid (integer) from the CallNumbers dataset as the value and the prfix (nvarchar) as the labels for the drop-down menu.

The second variable, @prefstr, is not used as a parameter. The IF statements assign a string to it depending on the integer value of @pref.

This setup will allow you to pass a single call number prefix via the parameter and variable. I'm not sure if you could configure it to do multiples, since we're using LIKE.

I am attaching a copy of the .rdl file. Since the forum doesn't like .rdl attachments, the extension is .txt. You shoud be able to rename the file and then open it in Report Builder.

There may be an easier way to do this, but this SEEMS to work.

Hope it helps.

JT

answered 1 year ago  by <img width="18" height="18" src="../../_resources/7d3da5f01ae245639ac9b0841d99dc67.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_939"></a>[Accept as answer](#)

- [Test Report with String variable.txt](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/769?fileName=769-939_Test+Report+with+String+variable.txt "Test Report with String variable.txt")

<a id="commentAns_939"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Renewals reports](https://iii.rightanswers.com/portal/controller/view/discussion/83?)
- [Simply Reports Discrepancies](https://iii.rightanswers.com/portal/controller/view/discussion/960?)
- [Fiscal year expenditures report](https://iii.rightanswers.com/portal/controller/view/discussion/98?)

### Related Solutions

- [Errors accessing reports in the client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930369939404)
- [Custom report parameters not allowing multiple selections in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930424447003)
- [Publishing Custom Reports](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930507535402)
- [Circulation statistics in Polaris toolbar reports](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965786679)
- [Accessing Report Builder to create a custom report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930999383964)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)