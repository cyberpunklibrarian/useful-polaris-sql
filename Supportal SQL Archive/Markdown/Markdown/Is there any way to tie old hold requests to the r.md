[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Is there any way to tie old hold requests to the relevant Bib records?

0

77 views

Patrons can request that we buy titles we don't have in the collection.  We wanted to try to figure out how the circs are generally doing on any titles which we bought at patron request, but we don't have a recordset of those titles.

If we buy something in physical format at a patron's suggestion, one of two staff members will place a hold on the title for the patron.  I thought that I might be able to use that workflow to find old purchase suggestions, but I can't find any way to tie an old (fulfilled) hold request to an accompanying bibliographic record.

Is it possible to do what I wanted, or would we need to start keeping a recordset instead, to get at circ figures?

asked 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_0a0068f3d23543b2ab74e325ca29c42f.jpg"/> jjack@aclib.us

[esoteric](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=150#t=commResults) [report builder](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=151#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 6 years ago

<a id="upVoteAns_389"></a>[](#)

0

<a id="downVoteAns_389"></a>[](#)

Answer Accepted

Quick question...did you try looking at the transaction database?  I think the create hold transaction does include bib information.

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_0a0068f3d23543b2ab74e325ca29c42f.jpg"/> trevor.diamond@mainlib.org

<a id="commentAns_389"></a>[Add a Comment](#)

<a id="upVoteAns_390"></a>[](#)

0

<a id="downVoteAns_390"></a>[](#)

Ah.  Yes, that should be doable--looks like it's TransactionType 6005, TransactionSubType 36, taking the numValue as BibRecordID, limiting by PolarisUserID, etc.

Thanks!

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_0a0068f3d23543b2ab74e325ca29c42f.jpg"/> jjack@aclib.us

- <a id="acceptAnswer_390"></a>[Accept as answer](#)

<a id="commentAns_390"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [I need to pull OCLC numbers from records with a deleted status to batch update our OCLC holdings. I'm able to get a report with: Bib record ID, Title, and Bib record status, but the column for MARC OCLC number is blank. Why doesn't it show up?](https://iii.rightanswers.com/portal/controller/view/discussion/829?)
- [Report for custom Hold Requests to Fill list](https://iii.rightanswers.com/portal/controller/view/discussion/91?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [SQL for bibs with holds and no owned items by branches](https://iii.rightanswers.com/portal/controller/view/discussion/821?)
- [SQL for Bibs w/no good items but have holds - including item and patron info](https://iii.rightanswers.com/portal/controller/view/discussion/594?)

### Related Solutions

- [How to create bulk holds from a record set](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161128141614846)
- [Linking holds on multiple bib records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930439017974)
- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)
- [Can we suspend the hold option for specific titles?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930255045537)
- [NCIP Lending & Borrowing with Polaris 4.1](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160503113118130)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)