[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Does anyone have a SQL to find out hold information on integrated e-content?

0

54 views

I'm trying to get some hold information on e-content without going to the vendors site.  I have looked through the SysHold and Vendor tables and nothing catches my eyes involving e-content and holds. My end goal is to find out what patrons have particular integrated e-books/e-audiobooks on hold.

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_73ed2cff26b6462f8f8020ca068f30d4.jpg"/> kleblanc@cmpl.org

[overdrive](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=30#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 4 years ago

<a id="upVoteAns_829"></a>[](#)

1

<a id="downVoteAns_829"></a>[](#)

I don't think that you will find any information on holds for integrated content, since that information usually resides with the vendor. Transactions tables may contain some information about checkout of OverDrive items, but it would be after the fact and may or may not be useful information.

Sorry this wasn't more helpful.

JT

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_73ed2cff26b6462f8f8020ca068f30d4.jpg"/> jwhitfield@aclib.us

- <a id="acceptAnswer_829"></a>[Accept as answer](#)

Yeah I had a feeling this was the case. Just wanted to see if someone else found something I couldnt. Thanks for responding. 

— kleblanc@cmpl.org 4 years ago

<a id="commentAns_829"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Does anyone have experience with EnvisionWare's 24-Hour Library?](https://iii.rightanswers.com/portal/controller/view/discussion/282?)
- [Does anyone have a SQL query to get circulation statistics for material with the subject heading Historical Fiction?](https://iii.rightanswers.com/portal/controller/view/discussion/1151?)
- [Information regarding library practices with retaining Patron ID](https://iii.rightanswers.com/portal/controller/view/discussion/137?)
- [Welcome emails for new patrons. Does anyone do this?](https://iii.rightanswers.com/portal/controller/view/discussion/307?)
- [SQL for waiving certain fines](https://iii.rightanswers.com/portal/controller/view/discussion/192?)

### Related Solutions

- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)
- [SysHoldRequest table missing Title information](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930733866097)
- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Link to download integrated content (Overdrive, Axis 360, 3M Cloud)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930286574983)
- [Expired Overdrive content explained and finding associated MARC records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930885172979)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)