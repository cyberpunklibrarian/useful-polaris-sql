[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# SQL help for finding previous, previous patron on an item

0

95 views

Originally posted by mwood@douglas.lib.nv.us on Thursday March 9th 14:26:39 EST 2017  
<br/>I used to have a SQL script that I found on the old Polaris Forums that would let me find the previous, previous patron (the one before the "Last Use Patron") on a specific item. I can't find the copy that I thought I had saved! Does someone else still have a copy of that script that you can share? I think it was one that we could run from the Polaris toolbar. We haven't needed it in quite a while, but could sure use it now. Thanks!

asked 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_fcf9f7ab620941dd8749c3ccaf0e1ab9.jpg"/> support1@iii.com

RE-asking this question, as it was never answered, and I also am looking for this particular piece of SQL code!

— bowmanb@cidlibrary.org 2 years ago

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_1230"></a>[](#)

0

<a id="downVoteAns_1230"></a>[](#)

Not the original code (I wasn't around back then), but the query below seems to work in the Polaris SQL Find Tool. Replace the ItemRecordID "00000" with your ItemRecordID in three places.

If the item is currently checked out, use SELECT TOP 3 in the third line; if not checked out, change to SELECT TOP 2.

SELECT DISTINCT p.PatronID  
FROM Polaris.Polaris.Patrons p  
WHERE p.PatronID IN (SELECT TOP 3 p.PatronID  
FROM Polaris.Polaris.Patrons p  
JOIN Polaris.Polaris.ItemRecordHistory irh  
ON p.PatronID = irh.PatronID  
WHERE irh.ItemRecordID = 00000  
AND irh.ActionTakenID IN (13,75)  
GROUP BY p.PatronID, irh.TransactionDate  
ORDER BY irh.TransactionDate DESC)  
AND p.PatronID NOT IN (SELECT ci.PatronID from ItemCheckouts ci WHERE ci.ItemRecordID = 00000)  
AND p.PatronID NOT IN (SELECT cri.LastUsePatronID from CircItemRecords cri WHERE cri.ItemRecordID = 00000)

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_fcf9f7ab620941dd8749c3ccaf0e1ab9.jpg"/> tvineberg@sandiego.gov

- <a id="acceptAnswer_1230"></a>[Accept as answer](#)

<a id="commentAns_1230"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL or Scoping for Items Withdrawn NOT linked to patron fee records](https://iii.rightanswers.com/portal/controller/view/discussion/525?)
- [Does anyone have a SQL to find out hold information on integrated e-content?](https://iii.rightanswers.com/portal/controller/view/discussion/684?)
- [PACREG Patrons - Virtual Patrons](https://iii.rightanswers.com/portal/controller/view/discussion/724?)
- [Finding out who has what permmission](https://iii.rightanswers.com/portal/controller/view/discussion/110?)
- [Item Stat codes](https://iii.rightanswers.com/portal/controller/view/discussion/1142?)

### Related Solutions

- [How to find the second to last borrower](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930492867728)
- [When is the “Former barcode” field in Patron Registration updated?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170126165932698)
- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)