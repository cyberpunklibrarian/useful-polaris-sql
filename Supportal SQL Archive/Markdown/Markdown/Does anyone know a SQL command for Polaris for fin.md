Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates?  (Not calendar year.) - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)

0

92 views

We ended up backing into this report in Simply Reports by finding the items which had circ, and subtracting from the total items in the collection by collection code (minus withdrawn, lost, etc.) But then we had to run a report for every collection code. Thanks . . .

asked 2 years ago  by <img width="18" height="18" src="../../_resources/2b9c18adcf5a4666a426c8167036e761.jpg"/>  crosbys@hillsboroughcounty.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_479"></a>[](#)

0

<a id="downVoteAns_479"></a>[](#)

I realized that my original query had a flaw that would create an overcount ![frown](../../_resources/be97031cb82b4ef38fd3b2e46afa3d0c.gif). I have revised the query and replaced the original one. My apologies for any inconvenience.

JT

answered 2 years ago  by <img width="18" height="18" src="../../_resources/2b9c18adcf5a4666a426c8167036e761.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_479"></a>[Accept as answer](#)

<a id="commentAns_479"></a>[Add a Comment](#)

<a id="upVoteAns_477"></a>[](#)

0

<a id="downVoteAns_477"></a>[](#)

EDIT: The original query had a flaw. A revised response is below.

I think that this version of the query will work better. My apologies if you tried the one I first posted here and it confused you.

It looks for items that were first available before the end of a timeframe that did not checkout or renew during the timeframe, that are not in a "bad" status from before the timeframe. If you want to count only checkouts, I will give the alternative ItemRecordHistoryActions after the end of the query.

SELECT col.Name as \[Collection\]
,COUNT(ir.ItemRecordID) as ZeroCircItems
FROM Polaris.Polaris.ItemRecords ir WITH (NOLOCK)
JOIN Polaris.Polaris.Collections col (NOLOCK)
ON (ir.AssignedCollectionID = col.CollectionID)
LEFT JOIN Polaris.Polaris.ItemRecordHistory irh (NOLOCK)
ON (ir.ItemRecordID = irh.ItemRecordID AND irh.ActionTakenID IN (13,28,75,76,77,78,79,80,81,82,89,90,91,93)
 AND irh.TransactionDate BETWEEN '07/01/2018' AND '11/30/2018 23:59:59.999')

WHERE
ir.FirstAvailableDate < '12/01/2018' --before the last date of the timeframe

AND irh.ItemRecordHistoryID IS NULL --Excludes items that checked out or were renewed during the timeframe

AND ir.ItemRecordID NOT IN
 (SELECT ItemRecordID FROM Polaris.Polaris.ItemRecords WITH (NOLOCK)
 WHERE ir.ItemStatusID IN (7,8,9,10,11)
 AND ir.ItemStatusDate < '7/1/2018') --Claimed, Lost, Missing, Withdrawn items from before the timeframe

GROUP BY col.Name
ORDER BY col.Name

--To exclude only items that checked out instead of renewals and checkouts, use this set of actions in the join: (13,75,77,78,79,89,90,91)

answered 2 years ago  by <img width="18" height="18" src="../../_resources/2b9c18adcf5a4666a426c8167036e761.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_477"></a>[Accept as answer](#)

<a id="commentAns_477"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Searching for Items that have not circ'd in 2 years ex. DVD's](https://iii.rightanswers.com/portal/controller/view/discussion/1053?)
- [Circulation count of deleted items from a collection](https://iii.rightanswers.com/portal/controller/view/discussion/92?)
- [Circulation by Item Statistical Code & limited by assigned collection?](https://iii.rightanswers.com/portal/controller/view/discussion/801?)
- [Report to find Items added by Material Type in a certain year?](https://iii.rightanswers.com/portal/controller/view/discussion/430?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)

### Related Solutions

- [How do I reset the year-to-date circulation counts?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930831368342)
- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [Circulation statistics and deleted records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930736904760)
- [Sort by year searching](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930814636875)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)