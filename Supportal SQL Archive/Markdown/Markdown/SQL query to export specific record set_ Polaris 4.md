SQL query to export specific record set: Polaris 4.1R2 - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL query to export specific record set: Polaris 4.1R2

0

86 views

Originally posted by lschimmer@ebsco.com on Tuesday, April 18th 18:11:42 EDT 2017
In the old supportal I came across a SQL query that would allow me to export records from a specific set into .mrc. I can't get this to run: SELECT a.BibliographicRecordID, 1 as \[ItemCount\] FROM BibRecordSets WHERE RecordSetID = \[55726\] But I'm wondering if there are further settings on the Export Criteria that I need to check as well? Any help would be much appreciated!

Lisa

asked 4 years ago  by <img width="18" height="18" src="../../_resources/51606a09d94741b4aad0b8698d74dc4f.jpg"/>  support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Does anyone have an SQL report for a specific tag in Authority records?](https://iii.rightanswers.com/portal/controller/view/discussion/625?)
- [Is there still a SQL repository for the Polaris forum?](https://iii.rightanswers.com/portal/controller/view/discussion/706?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)

### Related Solutions

- [How to export records in a record set to a MARC file using the Export Utility in the Polaris staff client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930873793203)
- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)
- [Find all permission groups that have the Secure patron record permissions](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930759206464)
- [How was the data gathered in SimplyReports?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930857021045)
- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)