[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL Query to find items that we own in eBook format but not in physical book format.

0

61 views

So I received this request from staff and it sounded so simple at first, but I just cannot seem to wrap my head around how I produce the results they are asking for.

Here is what I am working with, but due to the differences in Bibliographic BrowseTitles I can't get a reliable output.  I'm hoping there is another way to do the comparing/filtering other than the BrowseTitle to get more reliable results.  I am using MaterialTypeID=15 which is Electronic File, which encompases Ebooks, but wonder if maybe using MarcTypeofMaterial = 36 Ebook would help any.

Any help would be greatly appreciated.

&nbsp;

SELECT BrowseTitle  
FROM Polaris.Polaris.BibliographicRecords BR  
INNER JOIN Polaris.Polaris.CircItemRecords CIR  
ON BR.BibliographicRecordID = CIR.AssociatedBibRecordID AND MaterialTypeID = 15  
WHERE CIR.RecordStatusID = 1  
AND BrowseTitle NOT IN(  
SELECT BrowseTitle  
FROM Polaris.Polaris.BibliographicRecords BR1  
INNER JOIN Polaris.Polaris.CircItemRecords CIR1  
ON BR1.BibliographicRecordID = CIR1.AssociatedBibRecordID AND MaterialTypeID <> 15  
)  
ORDER BY BrowseTitle

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_468f1adcbc3845f3aa71bf5292c50950.jpg"/> Tim.Gross@lfpl.org

[reporting](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=82#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 6 Replies   last reply 1 year ago

<a id="upVoteAns_1384"></a>[](#)

0

<a id="downVoteAns_1384"></a>[](#)

Ah, ok, that actually helps us out quite a bit. This should get you pretty close:

&nbsp;

```javascript
select distinct
		br.BibliographicRecordID
		,bs245_a.Data
from polaris.polaris.BibliographicRecords br
join polaris.polaris.BibliographicTags bt_245
	on bt_245.BibliographicRecordID = br.BibliographicRecordID and bt_245.TagNumber = 245
join polaris.polaris.BibliographicSubfields bs245_a
	on bs245_a.BibliographicTagID = bt_245.BibliographicTagID and bs245_a.Subfield = 'a'
join polaris.polaris.CircItemRecords cir
	on cir.AssociatedBibRecordID = br.BibliographicRecordID
where br.RecordStatusID = 1
	and cir.ItemStatusID = 1
	and cir.MaterialTypeID = 15
	and not exists (
		select 1
		from polaris.polaris.BibliographicRecords br2
		join polaris.polaris.BibliographicTags bt_245_2
			on bt_245_2.BibliographicRecordID = br2.BibliographicRecordID and bt_245_2.TagNumber = 245
		join polaris.polaris.BibliographicSubfields bs245_a_2
			on bs245_a_2.BibliographicTagID = bt_245_2.BibliographicTagID and bs245_a_2.Subfield = 'a'
		join polaris.polaris.CircItemRecords cir2
			on cir2.AssociatedBibRecordID = br2.BibliographicRecordID
		where br2.RecordStatusID = 1
			and cir2.ItemStatusID = 1
			and cir2.MaterialTypeID != 15
			and bs245_a_2.Data = bs245_a.Data
			and br2.BibliographicRecordID != br.BibliographicRecordID
	)
```

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_468f1adcbc3845f3aa71bf5292c50950.jpg"/> mfields@clcohio.org

- <a id="acceptAnswer_1384"></a>[Accept as answer](#)

<a id="commentAns_1384"></a>[Add a Comment](#)

<a id="upVoteAns_1383"></a>[](#)

0

<a id="downVoteAns_1383"></a>[](#)

Book -  Our one common country : Abraham Lincoln and the Hampton Roads Peace Congerence of 1865

eBook - Our Once Common Country

&nbsp;

Field 245 sbufield - a is what I believe I need for compring, instead of the browsetitle, where as most of the titles that are showing as "different" are displaying or using subfield - a and subfield - b

Trying to find subfield 245 information in the Polaris database to hopefully narrow down the results.

&nbsp;

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_468f1adcbc3845f3aa71bf5292c50950.jpg"/> Tim.Gross@lfpl.org

- <a id="acceptAnswer_1383"></a>[Accept as answer](#)

<a id="commentAns_1383"></a>[Add a Comment](#)

<a id="upVoteAns_1382"></a>[](#)

0

<a id="downVoteAns_1382"></a>[](#)

Book -  Our one common country : Abraham Lincoln and the Hampton Roads Peace Congerence of 1865

eBook - Our Once Common Country

&nbsp;

Field 245 sbufield - a is what I believe I need for compring, instead of the browsetitle, where as most of the titles that are showing as "different" are displaying or using subfield - a and subfield - b

Trying to find subfield 245 information in the Polaris database to hopefully narrow down the results.

&nbsp;

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_468f1adcbc3845f3aa71bf5292c50950.jpg"/> Tim.Gross@lfpl.org

- <a id="acceptAnswer_1382"></a>[Accept as answer](#)

<a id="commentAns_1382"></a>[Add a Comment](#)

<a id="upVoteAns_1381"></a>[](#)

0

<a id="downVoteAns_1381"></a>[](#)

Hm, that certainly makes it a bit trickier. What kind of differences in title do the ebook/physical version have?

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_468f1adcbc3845f3aa71bf5292c50950.jpg"/> mfields@clcohio.org

- <a id="acceptAnswer_1381"></a>[Accept as answer](#)

<a id="commentAns_1381"></a>[Add a Comment](#)

<a id="upVoteAns_1380"></a>[](#)

0

<a id="downVoteAns_1380"></a>[](#)

[mfields@clcohio.org](mailto:mfields@clcohio.org) -- I like where you were going with that, but unfortunately the ISBN numbers are different.  I'm at a loss to find a field that has content that matches between the two so that I can do the comparison.  I've reached out to our content management department for some insight, but don't have much hope.

&nbsp;

Thanks for responding.

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_468f1adcbc3845f3aa71bf5292c50950.jpg"/> Tim.Gross@lfpl.org

- <a id="acceptAnswer_1380"></a>[Accept as answer](#)

<a id="commentAns_1380"></a>[Add a Comment](#)

<a id="upVoteAns_1379"></a>[](#)

0

<a id="downVoteAns_1379"></a>[](#)

Assuming the ISBNs are the same something like this might do the trick:

&nbsp;

```javascript
select distinct 
		 br.BibliographicRecordID
		,br.BrowseTitle
from polaris.polaris.BibliographicRecords br
join polaris.polaris.BibliographicISBNIndex bii
	on bii.BibliographicRecordID = br.BibliographicRecordID
join polaris.polaris.CircItemRecords cir
	on cir.AssociatedBibRecordID = br.BibliographicRecordID
where br.RecordStatusID = 1
	and cir.RecordStatusID = 1
	and cir.MaterialTypeID = 15
	and not exists ( 
		select 1
		from polaris.polaris.BibliographicRecords br2
		join polaris.polaris.BibliographicISBNIndex bii2
			on bii2.BibliographicRecordID = br2.BibliographicRecordID
		join polaris.polaris.CircItemRecords cir2
			on cir2.AssociatedBibRecordID = bii2.BibliographicRecordID
		where br2.RecordStatusID = 1
			and cir2.RecordStatusID = 1
			and bii2.ISBNString = bii.ISBNString
			and cir2.MaterialTypeID != 15
	)
```

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_468f1adcbc3845f3aa71bf5292c50950.jpg"/> mfields@clcohio.org

- <a id="acceptAnswer_1379"></a>[Accept as answer](#)

<a id="commentAns_1379"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [Simply Reports - formatting when comments fields are included.](https://iii.rightanswers.com/portal/controller/view/discussion/128?)
- [Help with automatically exporting SQL query to CSV file](https://iii.rightanswers.com/portal/controller/view/discussion/1399?)
- [Is there still a SQL repository for the Polaris forum?](https://iii.rightanswers.com/portal/controller/view/discussion/706?)
- [SQL query to export specific record set: Polaris 4.1R2](https://iii.rightanswers.com/portal/controller/view/discussion/93?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [Do Cloud library ebooks formaly 3m ebooks count towards a patron's total item limit?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930691690855)
- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)
- [SQL query for a count of items by material type](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930506564972)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)