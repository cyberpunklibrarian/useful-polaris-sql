SQL to check for Leap activity? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=13)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Leap](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=13)

# SQL to check for Leap activity?

0

160 views

Hello, all!
This is a half Leap, half sys admin thing. I'm looking for a way to see if a given user has done anything in Leap. We're doing some training here and I was curious if there might be a SQL query that pulls activity for a user, and specifically limits that activity to whatever has happened in Leap. I've pulled activity for users before, but as I try and fold in Leap related codes from the TransactionSubtypeCodes table, I'm just not quite getting there. I've been running activity on my own Polaris ID, and I thought I had it until I noticed that there was stuff in there that didn't match up with what I'd been doing. So I'm going wrong someplace.

Any ideas would be incredibly helpful. Thank you!

asked 3 years ago  by ![danielmesser@mcldaz.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_danielmesser@mcldaz.org20180523162210.jpg)  danielmesser@mcldaz.org

[leap](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=99#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 3 years ago

<a id="upVoteAns_341"></a>[](#)

1

<a id="downVoteAns_341"></a>[](#)

Answer Accepted

Hi again, Daniel.

Here's your query, with a couple of tweaks.

SELECT td.TransactionID,
td.TransActionSubTypeID,

tst.TransactionSubTypeDescription

,tsc.TransactionSubtypeCodeDesc

FROM PolarisTransactions.Polaris.TransactionDetails td (nolock)

JOIN PolarisTransactions.Polaris.TransactionSubTypes tst (nolock) ON td.TransactionSubTypeID = tst.TransactionSubTypeID

left outer JOIN PolarisTransactions.Polaris.TransactionSubTypeCodes tsc (nolock) ON td.numValue = tsc.TransactionSubTypeCode and td.TransactionSubTypeID = 145

WHERE td.TransactionID = 395738862

The results will give you a bunch of NULLS, but should include the type of checkout for the 145 row.

For checkins, you will need to join to TransactionSubtypeCodes with the ID = 128 --and so on for other LEAP activity.

Hope this helps.

JT

answered 3 years ago  by <img width="18" height="18" src="../../_resources/8b97afe8a287476aaa73d64d8153d350.jpg"/>  jwhitfield@aclib.us

That works *beautifully!* Thank you so much!

— danielmesser@mcldaz.org 3 years ago

<a id="commentAns_341"></a>[Add a Comment](#)

<a id="upVoteAns_340"></a>[](#)

1

<a id="downVoteAns_340"></a>[](#)

I'm pretty sure that the problem is being partially contributed by an inconsistent application of the Leap SubTypeCode.  I remember a while back (before May 2017) where I was trying to figure out if something took place in Leap and it turned out that the subtypecode hadn't been implemeneted correctly and transactions were recording improperly (the transaction was there, but the leap indicator was missing).

Since you posted a little, it looks like there's inconsistent labeling of Leap vs. Desktop activity.  Maybe Innovative can address if there's some kind of logic behind this?  I'm definitely putting it on the agenda for the next PIAC meeting.

answered 3 years ago  by <img width="18" height="18" src="../../_resources/8b97afe8a287476aaa73d64d8153d350.jpg"/>  trevor.diamond@mainlib.org

- <a id="acceptAnswer_340"></a>[Accept as answer](#)

I had a feeling that *might* be the case, Trevor! Looking around the database (of which I'm totally not an expert), I couldn't really find consisten implementaion of those SubTypeCodes. So I'd love to know if there's some kinda thing I should know about that.
Also, JT's query worked *perfectly!* And I think that'll get me what I need to know for now!

— danielmesser@mcldaz.org 3 years ago

<a id="commentAns_340"></a>[Add a Comment](#)

<a id="upVoteAns_339"></a>[](#)

0

<a id="downVoteAns_339"></a>[](#)

Oh jeez, I didn't save the exact thing I was working on, since it didn't work, but I'll walk through what I was trying to do!

So starting with the PolarisTransactions database, I know I began with something really basic like:

`SELECT th.TransactionID,`
`th.PolarisUserID,`
`th.TranClientDate,`
`th.TransactionTypeID`
`FROM PolarisTransactions.Polaris.TransactionHeaders th (nolock)`
`WHERE th.PolarisUserID = 437`
`AND th.TranClientDate BETWEEN '2018-08-01' AND '2018-08-02 23:59:59.000'`

I'm PolarisUserID 437, so this is looking specifically at me. That way I kinda know what to expect in the results. I get back this result set:

**TransactionID    PolarisUserID    TransactionDate    TransactionTypeID**
395738825    437    2018-08-02 13:29:08.103    7200
395738862    437    2018-08-02 13:29:28.500    6001
395660748    437    2018-08-01 14:53:30.193    7201
395624846    437    2018-08-01 10:48:01.820    7200
395626872    437    2018-08-01 10:57:50.337    7200
395748090    437    2018-08-02 14:46:21.390    2003
395748683    437    2018-08-02 14:53:55.460    7201
395704591    437    2018-08-02 07:17:51.690    7200

I sometimes rememeber what the TransactionTypeID codes refer to, but I'm lazy enough to just add the table into the mix. Because if I make this a report, I'm going to need that stuff anyway. I threw in something to order things too, so I wound up with something like:

`SELECT th.TransactionID,`
`th.PolarisUserID,`
`th.TranClientDate,`
`th.TransactionTypeID,`
`tt.TransactionTypeDescription`
`FROM PolarisTransactions.Polaris.TransactionHeaders th (nolock)`
`JOIN PolarisTransactions.Polaris.TransactionTypes tt (nolock) ON th.TransactionTypeID = tt.TransactionTypeID`
`WHERE th.PolarisUserID = 437`
`AND th.TranClientDate BETWEEN '2018-08-01' AND '2018-08-02 23:59:59.000'`
`ORDER BY th.TranClientDate`

And I get this:

**TransactionID    PolarisUserID    TransactionDate    TransactionTypeID    TransactionTypeDescription**
395624846    437    2018-08-01 10:48:01.820    7200    System Logon
395626872    437    2018-08-01 10:57:50.337    7200    System Logon
395660748    437    2018-08-01 14:53:30.193    7201    System Logoff
395704591    437    2018-08-02 07:17:51.690    7200    System Logon
395738825    437    2018-08-02 13:29:08.103    7200    System Logon
395738862    437    2018-08-02 13:29:28.500    6001    Check out
395748090    437    2018-08-02 14:46:21.390    2003    Patron registration modified
395748683    437    2018-08-02 14:53:55.460    7201    System Logoff

Okay, good. That matches up to what I've done. I'm interested in a couple of things here, the check out at 13:29 and the patron reg modifcation at 14:46. I know exactly what these are, so yeah. Now, here's where I'm sure I'm going wrong, but I don't know how to get right. So let's pop over to the TransactionDetails and have a look at the patron registration that I modified:

`SELECT td.TransactionID,`
`td.TransActionSubTypeID,`
`tst.TransactionSubTypeDescription`
`FROM PolarisTransactions.Polaris.TransactionDetails td (nolock)`
`JOIN PolarisTransactions.Polaris.TransactionSubTypes tst (nolock) ON td.TransactionSubTypeID = tst.TransactionSubTypeID`
`WHERE td.TransactionID = 395748090`

And I get:

TransactionID    TransActionSubTypeID    TransactionSubTypeDescription
395748090    6    PatronID
395748090    123    Patrons Assigned Branch ID
395748090    315    **Modified via LEAP**

I added the emphasis. So I'm thinking "Dandy! That's what I'm after!" Ah, but let's look at the checkout transaction:

`SELECT td.TransactionID,`
`td.TransActionSubTypeID,`
`tst.TransactionSubTypeDescription`
`FROM PolarisTransactions.Polaris.TransactionDetails td (nolock)`
`JOIN PolarisTransactions.Polaris.TransactionSubTypes tst (nolock) ON td.TransactionSubTypeID = tst.TransactionSubTypeID`
`WHERE td.TransactionID = 395738862`

And I get:

**TransactionID    TransActionSubTypeID    TransactionSubTypeDescription**
395738862    4    MaterialType
395738862    6    PatronID
395738862    7    PatronCode
395738862    33    PatronStatClassCode
395738862    38    ItemRecordID
395738862    60    Item Statistical Code
395738862    61    Assigned Collection Code
395738862    123    Patrons Assigned Branch ID
395738862    125    Items Assigned Branch ID
395738862    145    Checkout Type
395738862    186    Checkout Date
395738862    220    CourseReserveID
395738862    224    TermID
395738862    233    HoldRequestID
395738862    264    NextHoldRequestID
395738862    268    Item Home BranchID
395738862    296    Shelf location
395738862    300    Vendor Account ID
395738862    302    Patron Postal Code ID

Nothing about Leap. ![frown](../../_resources/2ebb370e2f654648bae91413c2168e1a.gif)

So I go digging around and find that in the TransactionSubTypeCodes there are several that are called out for Leap:

**TransactionSubTypeID    TransactionSubTypeCode    TransactionSubTypeCodeDesc**
128    50    Leap Check in
128    53    Checkin Leap Quick-circ
128    54    Checkin Leap Bulk Quick-circ
128    55    Checkin Leap Bulk
128    56    Checkin Leap In House
128    57    Checkin Leap Inventory
145    51    Leap Checkout and Renewal
145    52    Checkout Leap Quick-circ
188    22    Leap Registration
235    38    LEAP

And I tried several ways to figure out how those worked. I couldn't seem to match them with anything, nor could I find anything in my documentation. So I have a feeling I'm missing something obvious, but when I try to find out where those codes come into play, I'm coming up empty.

Like I said, by the time I was done trying, I had a query much longer than what you're looking at up there and a quarter of it was commented out as I tried different things. But the above queries are the building blocks for where I was getting started.

Let me know if I need to clarify anything!

Thanks!

answered 3 years ago  by ![danielmesser@mcldaz.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_danielmesser@mcldaz.org20180523162210.jpg)  danielmesser@mcldaz.org

- <a id="acceptAnswer_339"></a>[Accept as answer](#)

<a id="commentAns_339"></a>[Add a Comment](#)

<a id="upVoteAns_338"></a>[](#)

0

<a id="downVoteAns_338"></a>[](#)

Hi Daniel.

Do you have a sample query you could show us?

JT

answered 3 years ago  by <img width="18" height="18" src="../../_resources/8b97afe8a287476aaa73d64d8153d350.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_338"></a>[Accept as answer](#)

<a id="commentAns_338"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Running an SQL in Leap](https://iii.rightanswers.com/portal/controller/view/discussion/47?)
- [Is there any way to suppress the different claims options for items checked out to customers in LEAP?](https://iii.rightanswers.com/portal/controller/view/discussion/882?)
- [LEAP for Dummies or LEAP 101](https://iii.rightanswers.com/portal/controller/view/discussion/911?)
- [Leap Offline Procedures](https://iii.rightanswers.com/portal/controller/view/discussion/635?)
- [Payments in Leap](https://iii.rightanswers.com/portal/controller/view/discussion/50?)

### Related Solutions

- [No Leap enabled box for a workstation record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180612120107333)
- [Leap offline overview and FAQ](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=190807164520143)
- [RFID not working when using Leap with Internet Explorer](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161107085147303)
- [Leap receipts are consistently long regardless the number of items being checked out](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930992303843)
- [Adding Leap permissions en mass](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930306603411)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)