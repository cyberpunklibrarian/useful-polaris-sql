[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SimplyReports or SQL to get circ count by a UDF?

0

72 views

Originally posted by aross@esls.lib.wi.us on Friday, January 13th 08:42:16 EST 2017  
<br/>Hello. I am trying to figure out how to get a circulation count by one of our locally defined UDF's. We defined one of the UDFs as "Aldermanic District," and one of the new libraries would like a total count of circulation by Aldermanic Disctrict (by month, I think). I could not figure out an option via Simply Reports. Would this require SQL? If yes, does anyone have help they could offer in that area? I know the basics of SQL but not enough to connect to the transaction database tables, which I think are needed here? (I always end up with WAY too many results, so I know I am missing a tool in my toolbox :-) Thanks, much, for any assistance you can provide. Alison Monarch Library System

asked 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_1b3d38260c954841a05c7ba2a16c5c5c.jpg"/> support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SimplyReports or SQL to get circ count by a UDF?](https://iii.rightanswers.com/portal/controller/view/discussion/88?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)
- [SQL top bib checkouts between 2 dates](https://iii.rightanswers.com/portal/controller/view/discussion/650?)
- [Has anyone developed a SimplyReport/SQL Report to answer PLA's Public Library Data Service circulation questions?](https://iii.rightanswers.com/portal/controller/view/discussion/439?)
- [Does anyone have a sql script for a monthly in-house check-in count?](https://iii.rightanswers.com/portal/controller/view/discussion/615?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [Year End Circ Count Rollover](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160907153854067)
- [How do I find item counts by material type?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930417498777)
- [How do I schedule the Year End Circ Count Rollover job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930582726474)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)