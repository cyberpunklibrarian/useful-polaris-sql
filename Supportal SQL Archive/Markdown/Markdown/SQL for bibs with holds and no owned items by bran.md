SQL for bibs with holds and no owned items by branches - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for bibs with holds and no owned items by branches

0

36 views

We are a consortia that previously shared items between member jurisdictions.  While we still share bibs amongst all consortia members, jurisdictions are only accepting holds and circulating among their own branches.  Since items are not able to fill holds outside their jurisdictions, we are concerned about existing holds that are not getting filled because they are attached to bib records with no  items belonging to specific locations.  We would like to have a list so that librarians could look and see if ordering should happen, or ZIP books, or some other contact and resolution.  The list should also include some basic Bib information (control Num, Browse author, browse title, sort title, pub date, and format) and patron info (patron barcode, pickup branch, hold status, email address, phone).

It is possible to produce such a list but it takes pulling Simply Reports data separately from Holds (pickup/hold status/patron/bib info), from Bibs (bibs with active holds filter) and from Items (assigned branch) including downloading item counts, and it takes hours to do.

One note about the canned report “Holds alert report by Library”:  it is not useful for jurisdiction information.  The column ‘localitemcount’ reflects only the items owned by the pickup branches.  We need it to include all branches within a jurisdiction.  It looks like it is just aggregating the “item hold/item count” report.  In the following examples there are other jurisdictionsal copies, just not at the pickup branches.

![](../../_resources/8bf65819b6e14b94b4eb042ea9003bb6.png)

 I was hoping that some location with a SQL guru had also thought of this issue and had addressed it in a manner requiring a report similar to this and was willing to share their creation.

Many thanks for your time and efforts!

Kimberly Hunter

Black Gold Libraries

San Luis Obispo, CA

805-543-6082

khunter@blackgold.org

asked 1 year ago  by <img width="18" height="18" src="../../_resources/fab42bf9bfb14243975cb6a6a0da1350.jpg"/>  khunter@blackgold.org

[holds](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=96#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_972"></a>[](#)

0

<a id="downVoteAns_972"></a>[](#)

Hi,

We have a custom report for that. Below is the SQL from the stored procedure...obviously you will have to modify it a bit, but it should be a good starting point. In the report, we built in parameter options for which library's patrons placed the holds, whether or not there are any system items or just no local items, and the minimum number of holds per title.

Hope this helps!

Eleanor

ALTER Procedure \[Polaris\].\[SILS\_Rpt\_HoldsAlertLibrary\_no\_items_v2\]
@OrganizationList varchar(MAX),
@nNumberOfHolds int
AS
BEGIN
SET NOCOUNT ON
CREATE TABLE #Branches (OrganizationID INT);
--IF LEN(@OrganizationList) = 1 AND @OrganizationList= '0' -- ALL SELECTED
--BEGIN;
\-\- INSERT INTO #Branches (OrganizationID)
\-\- SELECT OrganizationID FROM Polaris.Organizations WITH (NOLOCK) WHERE OrganizationCodeID in (3);
--END;
--ELSE
BEGIN;
EXEC ('INSERT INTO #Branches SELECT OrganizationID FROM Polaris.Organizations WITH (NOLOCK) WHERE ParentOrganizationID IN (' + @OrganizationList + ')');
END;
;with LocalHoldTabPickupTab (BibliographicRecordID, ParentOrganizationID, HoldCount)
AS
(SELECT shr.BibliographicRecordID, ppu.ParentOrganizationID, count(distinct SHR.SysHoldRequestID) as HoldCount
FROM Polaris.Polaris.SysHoldRequests SHR WITH (NOLOCK)
inner join Polaris.patrons p (nolock)
on shr.PatronID = p.PatronID and p.PatronCodeID != 8
inner join #Branches b
on b.OrganizationID = p.OrganizationID
inner join Polaris.Organizations ppu (nolock)
on p.OrganizationID = ppu.OrganizationID
WHERE SysHoldStatusID IN (1,3) AND BibliographicRecordID <> 0 AND BibliographicRecordID is Not null
GROUP BY shr.BibliographicRecordID,ppu.ParentOrganizationID),
AllHoldTabPickupTab (BibliographicRecordID, HoldCount)
AS
(SELECT shr.BibliographicRecordID, count(distinct SHR.SysHoldRequestID) as HoldCount
FROM Polaris.Polaris.SysHoldRequests SHR WITH (NOLOCK)
WHERE SysHoldStatusID IN (1,3) AND BibliographicRecordID <> 0 AND BibliographicRecordID is Not null
GROUP BY shr.BibliographicRecordID),
LocalItems (BibliographicRecordID,ItemCount)
AS
(SELECT shr.BibliographicRecordID,count(distinct cir.ItemRecordID) as ItemCount
FROM Polaris.Polaris.SysHoldRequests SHR (nolock)
inner join Polaris.patrons p (nolock)
on shr.PatronID = p.PatronID and p.PatronCodeID != 8
inner join #Branches b
on b.OrganizationID = p.OrganizationID
LEFT JOIN Polaris.CircItemRecords cir (nolock)
ON cir.AssociatedBibRecordID = shr.BibliographicRecordID
INNER JOIN Polaris.Organizations pup (nolock)
ON p.OrganizationID = pup.OrganizationID
INNER JOIN Polaris.Organizations ib (nolock)
ON cir.AssignedBranchID = ib.OrganizationID
WHERE CIR.ItemStatusID NOT IN (10,7,11,8,9)
AND CIR.RecordStatusID = 1
AND cir.MaterialTypeID not in (31,34)
AND pup.ParentOrganizationID = ib.ParentOrganizationID
GROUP BY shr.BibliographicRecordID),
AllItems (BibliographicRecordID,ItemCount)
AS
(SELECT shr.BibliographicRecordID,count(distinct cir.ItemRecordID) as ItemCount
FROM Polaris.Polaris.SysHoldRequests SHR (nolock)
inner join Polaris.patrons p (nolock)
on shr.PatronID = p.PatronID and p.PatronCodeID != 8
inner join #Branches b
on b.OrganizationID = p.OrganizationID
LEFT JOIN Polaris.CircItemRecords cir (nolock)
ON cir.AssociatedBibRecordID = shr.BibliographicRecordID
WHERE CIR.ItemStatusID NOT IN (10,7,11,8,9)
AND CIR.RecordStatusID = 1
AND cir.MaterialTypeID not in (31,34)
GROUP BY shr.BibliographicRecordID)
select par.Name,
br.BibliographicRecordID,
br.BrowseTitle as Title,
br.BrowseAuthor as Author,
br.MARCPubDateOne as PubDate,
tom.Description as MARCTypeOfMaterial,
ta.Description as TargetAudience,
br.MARCMedium as GMD,
COALESCE(shr.ISBN,u.UPCNumber,'') as ISBN,
lh.HoldCount as AgencyHolds,
isnull(li.ItemCount,0) as AgencyItems,
ah.HoldCount as SystemHolds,
isnull(ai.ItemCount,0) as SystemItems
from Polaris.BibliographicRecords br
inner join Polaris.SysHoldRequests shr (nolock)
on br.BibliographicRecordID = shr.BibliographicRecordID
left join
(select BibliographicRecordID,min(UPCNumber) as UPCNumber
from Polaris.BibliographicUPCIndex (nolock)
group by BibliographicRecordID) u
on u.BibliographicRecordID = br.BibliographicRecordID
inner join LocalHoldTabPickupTab lh
on lh.BibliographicRecordID = br.BibliographicRecordID
inner join AllHoldTabPickupTab ah
on ah.BibliographicRecordID = br.BibliographicRecordID
left join LocalItems li
on li.BibliographicRecordID = br.BibliographicRecordID
left join AllItems ai
on ai.BibliographicRecordID = br.BibliographicRecordID
left join Polaris.TargetAudiences ta
on ta.Code = br.MARCTargetAudience
inner join polaris.MARCTypeOfMaterial tom (nolock)
on tom.MARCTypeOfMaterialID = br.PrimaryMARCTOMID
inner join Polaris.Organizations par (nolock)
on lh.ParentOrganizationID = par.OrganizationID
where lh.HoldCount >= @nNumberOfHolds and (li.ItemCount is null or li.ItemCount = 0)
GROUP BY
par.Name,
br.BibliographicRecordID,
br.BrowseTitle,
br.BrowseAuthor,
br.MARCPubDateOne,
tom.Description,
ta.Description,
br.MARCMedium,
COALESCE(shr.ISBN,u.UPCNumber,''),
lh.HoldCount,
isnull(li.ItemCount,0),
ah.HoldCount,
isnull(ai.ItemCount,0)
END

answered 1 year ago  by <img width="18" height="18" src="../../_resources/fab42bf9bfb14243975cb6a6a0da1350.jpg"/>  ecrumblehulme@sasklibraries.ca

- <a id="acceptAnswer_972"></a>[Accept as answer](#)

<a id="commentAns_972"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for Bibs w/no good items but have holds - including item and patron info](https://iii.rightanswers.com/portal/controller/view/discussion/594?)
- [SQL for Item records from PO/FY/Branch](https://iii.rightanswers.com/portal/controller/view/discussion/326?)
- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [SQL top bib checkouts between 2 dates](https://iii.rightanswers.com/portal/controller/view/discussion/650?)

### Related Solutions

- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Count of items owned on a date in the past](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930671934108)
- [Do the home and owning branch need to match for floating item records?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930466429189)
- [Why can't I link to the holds queue from an item record?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989583652)
- [Re-routing holds during brief branch closure](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930587478425)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)