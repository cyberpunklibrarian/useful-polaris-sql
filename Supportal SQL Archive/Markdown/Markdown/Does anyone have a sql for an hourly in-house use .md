[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Does anyone have a sql for an hourly in-house use report? Thanks!

0

44 views

We have a report for lifetime and monthly, but I have one library wanting hourly if possible.

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_767cdf4d30b143a6896c45da78eed869.jpg"/> dporter@illinoisheartland.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_1347"></a>[](#)

0

<a id="downVoteAns_1347"></a>[](#)

Sure, see the attached.  It's originally a circ by hour report, but I changed it to check-ins with in-house use.  You may need to see that everything else aligns (I may have missed a setting indicating circ).

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_767cdf4d30b143a6896c45da78eed869.jpg"/> jtenter@sasklibraries.ca

- <a id="acceptAnswer_1347"></a>[Accept as answer](#)

- [In-House Use by Hour.zip](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/1312?fileName=1312-1347_In-House+Use+by+Hour.zip "In-House Use by Hour.zip")

<a id="commentAns_1347"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Does anyone use the collection agency reporting type of 'other'?](https://iii.rightanswers.com/portal/controller/view/discussion/345?)
- [Does anyone have a SQL report that will show renewal vs new cards for a particular patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/1377?)
- [Does anyone use Polaris on a Virtual Workstation?](https://iii.rightanswers.com/portal/controller/view/discussion/273?)
- [Does anyone have an SQL for a request ratio?](https://iii.rightanswers.com/portal/controller/view/discussion/394?)
- [Does anyone have a damaged item template report you are willing to share?](https://iii.rightanswers.com/portal/controller/view/discussion/956?)

### Related Solutions

- [Why is in-house check in not counting as circulation?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930877939075)
- [Patron Status does not show a reminder that is in the NotificationHistory table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930782414146)
- [New Hours of Operation for Branch](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930508226832)
- [Setting the default view for the Check In workform](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930805135071)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)