SQL to identify holds on bib records with no copies owned at pickup location (+limited to specific bib level qualifiers)? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=20)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# SQL to identify holds on bib records with no copies owned at pickup location (+limited to specific bib level qualifiers)?

0

73 views

Hello. I work for a consortium of 31 libraries. One of the libraries would like a report that lists titles on which holds for local pickup have been placed but to which they have no items attached. They are looking at this as a collection development tool and would like to start with nonfiction books. I am still relatively new to SQL. I put together the following which I think limits to holds for nonfiction book formats for pickup at their location, but I am not sure how to include a component that either highlights to which bibs they have no items attached - or which limits to only bibs for which they do not own a copy. Is that possible?

They are aware of the Holds Alert by Branch canned report, but that does not provide the granularity to limit to just Nonfiction books. (though they will use it for the time being :-)

Thanks, much, in advance for any help you can provide. :-)

Alison

SELECT o.Name AS PickupLibrary,shr.BibliographicRecordID,mt.Description AS MaterialType,br.BrowseTitle AS Title,
"Audience" =
CASE
WHEN br.MARCTargetAudience = 'j' THEN 'Juv'
WHEN br.MARCTargetAudience = 'g' THEN 'Adult'
ELSE 'Other'
END
FROM Polaris.Polaris.SysHoldRequests SHR WITH (NOLOCK)
INNER JOIN Polaris.Polaris.Organizations o
on (SHR.PickupBranchID = o.OrganizationID)    
INNER JOIN Polaris.Polaris.BibliographicRecords BR with (nolock)
ON (shr.BibliographicRecordID = br.BibliographicRecordID)
INNER JOIN Polaris.Polaris.MARCTypeOfMaterial mt
ON (br.PrimaryMARCTOMID = mt.MARCTypeOfMaterialID)
WHERE shr.PickupBranchID IN (8)
AND shr.SysHoldStatusID NOT IN (16,17)
AND shr.BibliographicRecordID <> 0
AND shr.BibliographicRecordID is Not null
AND br.PrimaryMARCTOMID IN (1)
AND br.LiteraryForm = '0'

asked 3 years ago  by <img width="18" height="18" src="../../_resources/c2e7315b99b84e20b3424c5b81b84a96.jpg"/>  ahoffman@monarchlibraries.org

[sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 3 years ago

<a id="upVoteAns_370"></a>[](#)

0

<a id="downVoteAns_370"></a>[](#)

Answer Accepted

Here's an addition to your original search criteria that may work for you. It uses another select statement to omit any hold requests on bibs that have an item assigned to the desired branch.

There may be other ways to do this that are more efficient, but when I tested it on our database it took less than a second to run.

Hope this helps.

JT

\-\-\-\-\-\-\-

SELECT o.Name AS PickupLibrary,shr.BibliographicRecordID,mt.Description AS MaterialType,br.BrowseTitle AS Title,
"Audience" =
CASE
WHEN br.MARCTargetAudience = 'j' THEN 'Juv'
WHEN br.MARCTargetAudience = 'g' THEN 'Adult'
ELSE 'Other'
END

FROM Polaris.Polaris.SysHoldRequests SHR WITH (NOLOCK)

INNER JOIN Polaris.Polaris.Organizations o
on (SHR.PickupBranchID = o.OrganizationID)   

INNER JOIN Polaris.Polaris.BibliographicRecords BR with (nolock)
ON (shr.BibliographicRecordID = br.BibliographicRecordID)

INNER JOIN Polaris.Polaris.MARCTypeOfMaterial mt
ON (br.PrimaryMARCTOMID = mt.MARCTypeOfMaterialID)

WHERE shr.PickupBranchID IN (8)
AND shr.SysHoldStatusID NOT IN (16,17)
AND shr.BibliographicRecordID <> 0
AND shr.BibliographicRecordID is Not null
AND br.PrimaryMARCTOMID IN (1)
AND br.LiteraryForm = '0'
AND shr.BibliographicRecordID NOT IN
 (SELECT DISTINCT AssociatedBibRecordID from Polaris.Polaris.ItemRecords WITH (NOLOCK)
 WHERE AssignedBranchID IN (8)
 AND RecordStatusID = 1 --Final
 AND ItemStatusID IN (1,2,3,4,5,6) --In, Out, Out-Ill, Held, Transferred, In-Transit
 )

answered 3 years ago  by <img width="18" height="18" src="../../_resources/c2e7315b99b84e20b3424c5b81b84a96.jpg"/>  jwhitfield@aclib.us

<a id="commentAns_370"></a>[Add a Comment](#)

<a id="upVoteAns_369"></a>[](#)

0

<a id="downVoteAns_369"></a>[](#)

The target audience field is actually the relatively reliable. I update that field within, at most, a week to be either g, j,  or d. Above all, I make sure it is never left blank. We have hundreds of collection codes, and use of those codes varies greatly among libraries. However, all collection codes are preceded by either "Adult", "Young Adult," or "Juvenile," so I am able to use those as the bases for batch updates to the target audience field in the 008 field. In a tiny percentage of cases, the collection codes on a single bib overlap between adult, YA, or JUV codes, but the vast majority are consistent. It is not perfect, but it is better than the large percentage of blanks usually found and has also been helpful in making the audience limit in the PAC more useful. :-)

However, if you have something based on collection codes, I may still be able to make that work here, especially if I focus the query on one library at a time.

In either case, thank you for taking the time to respond and try to help. :-)

answered 3 years ago  by <img width="18" height="18" src="../../_resources/c2e7315b99b84e20b3424c5b81b84a96.jpg"/>  ahoffman@monarchlibraries.org

- <a id="acceptAnswer_369"></a>[Accept as answer](#)

<a id="commentAns_369"></a>[Add a Comment](#)

<a id="upVoteAns_368"></a>[](#)

0

<a id="downVoteAns_368"></a>[](#)

Hi Alison.

Depending on your database, using data from the 008 field of the MARC record may not give you the results you want. For example, the TargetAudience character may be blank.

It may be easier to approach this by item collection, if your library system uses the same collections for all locations.  (This would use the assigned collection of an item that is not assigned to the particular library.)

If a bib has items from different collections linked to it (say Adult Nonfiction and Children's Nonfiction) you may get a few "false positives" or otherwise stray results.

We may have something along this line here. If this sounds like something that may work for you I could see if I can find it, or if there's something similar that I could tweak.

JT

answered 3 years ago  by <img width="18" height="18" src="../../_resources/c2e7315b99b84e20b3424c5b81b84a96.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_368"></a>[Accept as answer](#)

<a id="commentAns_368"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [In a consortium of 8 libraries, Is it possible to limit either a collection or material type by both the patron's home library and their hold pickup to branches within that library?](https://iii.rightanswers.com/portal/controller/view/discussion/262?)
- [adding info to bibs via SQL script?](https://iii.rightanswers.com/portal/controller/view/discussion/107?)
- [Is there any harm in deleting old item/bib record templates?](https://iii.rightanswers.com/portal/controller/view/discussion/591?)
- [Creating a new branch for holds pickup](https://iii.rightanswers.com/portal/controller/view/discussion/119?)
- [Does anyone have a SQL to find out hold information on integrated e-content?](https://iii.rightanswers.com/portal/controller/view/discussion/684?)

### Related Solutions

- [How to create bulk holds from a record set](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161128141614846)
- [No one is able to request a particular title that looks like it is available](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=171109103434625)
- [Overdrive integration creating non-integrated items and no resource entity](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170503103227830)
- [How can I locate bibs that were generated during a Terminated import without using SQL Server Management Studio?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930570115337)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)