SQL for items withdrawn by a particular staff member - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# SQL for items withdrawn by a particular staff member

0

29 views

I'm not sure if this is possible, but I would like a list of all items in our catalog that currenlty have a circ status of Withdrawn and were withdrawn by a particular staff member. I've been playing around with some SQL queries but haven't been able to build one that gets me what I need. 

asked 1 year ago  by <img width="18" height="18" src="../../_resources/dd63f3e393c44f9c8ba4c5597b7eb3b4.jpg"/>  slewis@nolalibrary.org

[sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_961"></a>[](#)

0

<a id="downVoteAns_961"></a>[](#)

Hi.

Here is a Find Tool query that may give you what you want. If you need a list report, you can select additional columns from the existing tables, or join to another table (such as BibliographicRecords) and select from that. This query looks for withdrawn items that were modified (by cataloging or bulk change) by a user after a date when the item status before the change was not Withdrawn, but the item status after the change was Withdrawn.

Comments with double hyphens cause Find Tool queries to fail, so I will add a couple of notes below the query.

SELECT irh.ItemRecordID FROM Polaris.Polaris.ItemRecordHistory irh WITH (NOLOCK)

JOIN Polaris.Polaris.ItemRecords ir (NOLOCK)

ON (irh.ItemRecordID = ir.ItemRecordID)

JOIN Polaris.Polaris.PolarisUsers polu (NOLOCK)

ON (irh.PolarisUserID = polu.PolarisUserID)

WHERE

ir.RecordStatusID = 1

AND ir.ItemStatusID = 11

AND irh.ActionTakenID IN (8,9)

AND irh.OldItemStatusID <> 11

AND irh.NewItemStatusID = 11

AND irh.TransactionDate >= '07/01/2020'

AND polu.Name like 'jsmith'

Notes: If you need information about items withdrawn today, you will need to replace ItemRecordHistory with the ItemRecordHistoryDaily table (or wait until tomorrow).

There is a way that this query may return a false positive: If User A withdraws an item, the item is later un-withdrawn and User B withdraws it again, the item would show up in a query for User A or User B. (The item record's history display would show which of the users modified it last.)

Depending on how long your library retains ItemRecordHistory entries, you may be a limit on how far back you can search.

If you don't want to enter a PolarisUser Name, you can omit the join to that table and replace the last line with "AND irh.PolarisUserID = 123".

Since the query joins onto the ItemRecords view, you can limit your query by the item's collection, stat code, material type, assigned branch, etc.

Hope this helps,

JT

answered 1 year ago  by <img width="18" height="18" src="../../_resources/dd63f3e393c44f9c8ba4c5597b7eb3b4.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_961"></a>[Accept as answer](#)

<a id="commentAns_961"></a>[Add a Comment](#)

<a id="upVoteAns_960"></a>[](#)

0

<a id="downVoteAns_960"></a>[](#)

This should work, unless you have a lot of items which have been unwithdrawn (in which case it would catch items which the staff member withdrew, which were unwithdrawn, and which got withdrawn again, possibly by someone else).

That said, it will only go back as far as your ItemRecordHistory (ours is trimmed on a schedule; I'm not sure if that's mandatory or typical).

You'll need to change the "AND pu.Name" line to have the username you're looking for between the single quotes.

If this won't work for you, please let me know.

SELECT DISTINCT cir.ItemRecordID
FROM itemrecordhistory irh with (NOLOCK)
JOIN PolarisUsers pu with (NOLOCK)
ON irh.PolarisUserID = pu.PolarisUserID
JOIN CircItemRecords cir with (NOLOCK)
ON irh.ItemRecordID = cir.ItemRecordID
WHERE cir.ItemStatusID = 11
AND irh.NewItemStatusID = 11
AND irh.OldItemStatusID <> 11
AND pu.Name = 'jjack'

answered 1 year ago  by <img width="18" height="18" src="../../_resources/dd63f3e393c44f9c8ba4c5597b7eb3b4.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_960"></a>[Accept as answer](#)

<a id="commentAns_960"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [How do you purge withdrawn items](https://iii.rightanswers.com/portal/controller/view/discussion/186?)
- [Is the an SQL statement that finds items in processing that have holds?](https://iii.rightanswers.com/portal/controller/view/discussion/476?)
- [Deleting item records with holds](https://iii.rightanswers.com/portal/controller/view/discussion/32?)
- [e-content with no item records](https://iii.rightanswers.com/portal/controller/view/discussion/251?)
- [Delete a collection, stat code, or Shelf code from drop down list on item records](https://iii.rightanswers.com/portal/controller/view/discussion/402?)

### Related Solutions

- [Default cataloging templates](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930883676906)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [How do I find item counts by material type?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930417498777)
- [What font is used in the Polaris staff client?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930334895408)
- [How can I locate bibs that were generated during a Terminated import without using SQL Server Management Studio?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930570115337)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)