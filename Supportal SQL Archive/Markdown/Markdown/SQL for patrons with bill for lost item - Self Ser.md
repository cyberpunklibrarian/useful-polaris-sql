[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for patrons with bill for lost item

0

211 views

Any of you brilliant SQL whizzes able to create a query that will find patrons who have been billed for items that have now been deleted from our system?

I have the titles of the items and the last 3 digits of their barcodes. 

Thanks!

asked 6 years ago by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG) rdye@krl.org

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 6 years ago

<a id="upVoteAns_176"></a>[](#)

0

<a id="downVoteAns_176"></a>[](#)

Answer Accepted

No worries!  I'm glad it helped.

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_0637f181fbae4b77b507d8bd612c1060.jpg"/> jjack@aclib.us

<a id="commentAns_176"></a>[Add a Comment](#)

<a id="upVoteAns_175"></a>[](#)

0

<a id="downVoteAns_175"></a>[](#)

Thank you!  This gets me close enough.  For some reason, I never saw this until now - sorry for the delay in thanking you.

answered 6 years ago by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG) rdye@krl.org

- <a id="acceptAnswer_175"></a>[Accept as answer](#)

<a id="commentAns_175"></a>[Add a Comment](#)

<a id="upVoteAns_132"></a>[](#)

0

<a id="downVoteAns_132"></a>[](#)

Were you looking for cases where the bill had been paid (so you could refund them) or where the bill was outstanding (so you could waive it)?  Or something else?

Anyway, this gets you part of the way there but you'll want to double-check the patron's record to verify what happened with their account.  I tried to limit it to cases where the bill at least hadn't been waived automatically because the item was turned back in, but apparently my brain just isn't up to it today; I couldn't make it happen.

I've attached the SQL in a text file; III doesn't like the percent sign and doesn't seem to let you put in raw HTML either.

The second-to-last line lets you put in just part of the title; the final line lets you put in just the end of the item barcode.

If this isn't what you need, please let me know.

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_0637f181fbae4b77b507d8bd612c1060.jpg"/> jjack@aclib.us

- <a id="acceptAnswer_132"></a>[Accept as answer](#)

- [patrons with bills on deleted items.txt](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/164?fileName=164-132_patrons+with+bills+on+deleted+items.txt "patrons with bills on deleted items.txt")

<a id="commentAns_132"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for Lost and Paid Items](https://iii.rightanswers.com/portal/controller/view/discussion/662?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [SQL for Bibs w/no good items but have holds - including item and patron info](https://iii.rightanswers.com/portal/controller/view/discussion/594?)
- [Report of Lost and Paid Items](https://iii.rightanswers.com/portal/controller/view/discussion/331?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [4.0 Patron Services - Lost Item Charge Options](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160908155555187)
- [Items converting to Lost status](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930282614657)
- [Which lost item recovery settings are used for partially paid replacement costs?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170523113239517)
- [Paid lost item still displaying as unpaid](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930877186258)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)