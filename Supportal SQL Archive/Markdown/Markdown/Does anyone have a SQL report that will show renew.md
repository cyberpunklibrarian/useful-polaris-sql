[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Does anyone have a SQL report that will show renewal vs new cards for a particular patron code?

0

35 views

We need to pull a report that shows card renewal vs new cards for our Board.  We have looked and tried multiple ways in Simply Reports but can't get the information needed.  Does anyone have a good way using SQL to do this?

asked 5 months ago by <img width="18" height="18" src="../_resources/default-avatar_36f90ca19f9c4eadbc71767f94d46125.jpg"/> robin.walden@brentwoodtn.gov

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 5 months ago

<a id="upVoteAns_1415"></a>[](#)

0

<a id="downVoteAns_1415"></a>[](#)

@ Robin - replace the # with the Patron Code integer(s) and set your time frame. Hope this helps.

**\--New Card Registrations**

SELECT DISTINCT pr.PatronID

FROM Polaris.Polaris.PatronRegistration pr (NOLOCK)

JOIN Polaris.Polaris.Patrons p (NOLOCK)

ON p.PatronID \= pr.PatronID

WHERE

p.PatronCodeID IN (#) AND

pr.RegistrationDate BETWEEN '2024-01-01 00:00:00' AND '2024-01-31 23:59:59'

&nbsp;

**\--Updated Cards**

SELECT DISTINCT pr.PatronID

FROM Polaris.Polaris.PatronRegistration pr (NOLOCK)

JOIN Polaris.Polaris.Patrons p (NOLOCK)

ON p.PatronID \= pr.PatronID

WHERE

p.PatronCodeID IN (#) AND

pr.UpdateDate BETWEEN '2024-01-01 00:00:00' AND '2024-01-31 23:59:59'

answered 5 months ago by ![delaneyp@hcfl.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_delaneyp@hcfl.gov20240311130530.jpg) delaneyp@hcfl.gov

- <a id="acceptAnswer_1415"></a>[Accept as answer](#)

<a id="commentAns_1415"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Does anyone have a sql for an hourly in-house use report? Thanks!](https://iii.rightanswers.com/portal/controller/view/discussion/1312?)
- [Does anyone have a damaged item template report you are willing to share?](https://iii.rightanswers.com/portal/controller/view/discussion/956?)
- [Floating Reports](https://iii.rightanswers.com/portal/controller/view/discussion/363?)
- [Does anyone use the collection agency reporting type of 'other'?](https://iii.rightanswers.com/portal/controller/view/discussion/345?)
- [Report for patrons with items out. If possible, sort by location?](https://iii.rightanswers.com/portal/controller/view/discussion/779?)

### Related Solutions

- [Where to find Patron Circ Count in Patron record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930283871188)
- [Credit Card reports at Self-Check](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930668080115)
- [Sending Collection Agency Reports Manually](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930959368316)
- [Why is in-house check in not counting as circulation?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930877939075)
- [Statistics for Holds and ILLs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930628713826)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)