[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [PAC](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=14)

# Tracking usage of links in PAC

0

113 views

Does anyone know of a way to pull stats on link usage on the PAC? We have a number of links on our sidebar (e.g., staff picks, new books, NY Times best sellers, library calendar), and we're trying to determine which links are used the most, and which don't get used. Is there a way to track this using Google Analytics, SQL, or some other means?

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_2ac656818b2e4a2395b615f249c62618.jpg"/> musack@bcls.lib.nj.us

[clickable link](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=209#t=commResults) [pac](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=58#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 4 years ago

<a id="upVoteAns_831"></a>[](#)

0

<a id="downVoteAns_831"></a>[](#)

Thanks so much for your response! We were able to find the information we were looking for, but I'll keep this in mind if it turns out that we need more detailed stats.

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_2ac656818b2e4a2395b615f249c62618.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_831"></a>[Accept as answer](#)

<a id="commentAns_831"></a>[Add a Comment](#)

<a id="upVoteAns_830"></a>[](#)

0

<a id="downVoteAns_830"></a>[](#)

It's possible, but you'd want to set up a separate property for it in Google Analytics, and then you'd have to use Tag Manager to set up triggers to track interaction with various elements in the PAC.  Ideally you'd use custom IDs and/or classes rather than a convoluted CSS inheritance to pinpoint which elements you want to track.  Please let me know if you have any questions.

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_2ac656818b2e4a2395b615f249c62618.jpg"/> jjack@aclib.us

- <a id="acceptAnswer_830"></a>[Accept as answer](#)

<a id="commentAns_830"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Change external PAC header links to open in new window?](https://iii.rightanswers.com/portal/controller/view/discussion/964?)
- [Setting up PAC Registration](https://iii.rightanswers.com/portal/controller/view/discussion/1183?)
- [v6+ PAC Customization?](https://iii.rightanswers.com/portal/controller/view/discussion/341?)
- [Google Analytics outbound links](https://iii.rightanswers.com/portal/controller/view/discussion/503?)
- [Niche Academy or JavaScript in PAC?](https://iii.rightanswers.com/portal/controller/view/discussion/229?)

### Related Solutions

- [Create Inn-Reach Deep Links](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=211118171449190)
- [Deep links](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930313811356)
- [How can I track how often the Mobile PAC is used to access the catalog?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930666328366)
- [PowerPAC links to Google](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930860463838)
- [Links in saved search e-mail not working](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930268629338)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)