[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Experiences Using Student IDs and Bookmobile TRN

0

55 views

We currently issue over 20K cards each year that we refer to as ‘ecards’ that are distributed to local school districts to give out to students with the goal being to give each student access to some of our online database resources for research and homework regardless of whether they currently have a regular library card. We have at least two school districts within our service area with each district having multiple schools in their system.

&nbsp;We use the Bookmobile patron TRN file process to get them into Polaris. The ecards have a separate patron code from our regular library cards, they have no checkout privileges, they expire after one year, and have a separate barcode prefix that includes the last two digits for the end of the upcoming school year. Each TRN record is a ‘5’ Record Code for new patron and everything about each record is identical except for the barcode. The barcode prefix allows easy placement into a record set so a SQL statement can remove the system block that the import process places on the records. After the new school year starts, we delete the old cards from Polaris.

&nbsp;We are considering seeing if the school districts will be willing to send us some basic student information in addition to the studentID (name, phone, and birthdate at a minimum) and then using the student ID in the Barcode field of the TRN record. The goal being to no longer issue barcodes for student ecards and students to be able to sign in to the online databases with their already familiar student ID rather than another long barcode number to remember.

&nbsp;Is anyone already doing this kind of thing using student IDs that would be willing to share your experiences?  What challenges does this introduce? How do you handle updates throughout the year as student turnover occurs? Do you have one file with a mix of both ‘5’ and ‘7’ record codes? If you have multiple school districts, do you combine them in to one TRN file?

&nbsp;One thing I am unsure of is what would happen going forward once the initial student ID ‘5’ record is imported and updates are required, especially in instances where the student also has a regular library card as well.

&nbsp;During the initial import of the ‘5’ ecard record the duplicate detection process will find a match on the patron name because the patron also has a regular library card and issue a warning to that effect. That is fine initially, as we do not want to merge the two records due to different checkout privileges for the respective patron codes. Later in the year, or perhaps the next school year, we get a file from the district with a ‘7’ (Update patron record code) that has a phone number update and the UDF fields of the TRN file are blank. We use the UDF fields in our regular cards, but not the ecards.

&nbsp;If I understand the Polaris Importing Student Records documentation correctly, the incoming ‘7’ record completely overwrites existing records with the new values, even if the incoming fields are blank and any existing cards contain data in those fields. This time when the import runs I suspect it will detect two duplicate records: the regular library card for the patron and the existing student ID card from the initial load.

&nbsp;What happens in this instance when the update record processes? Does the incoming ‘7’ record update both the regular card (which already has values in the UDF that we want to keep) and the inital ecard with the new incoming blank UDF information?

Another concern is that we are in a consortium with another area library. If a match is found between our new ecard and a regular card from the consortium library, we certainly don't want to be overwriting any of their information.

Are there any other things we should be aware of?

Feel free to reply here or email me at [sthorpe@myepl.org](mailto:sthorpe@myepl.org).

&nbsp;

Thank you in advance

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_d281c7c55bcc43cd9936b29ec39c1f34.jpg"/> sthorpe@elkhart.lib.in.us

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_1294"></a>[](#)

0

<a id="downVoteAns_1294"></a>[](#)

We have a colledge that we partner with. They export their student file into a TRN. They use the student number as the barcode number. It is like the first letter of their first and last name followed by a number.

Our selfchecks are set to allow a manual entry since the ID is not in a barcode form. There are wasy to get such an ID into a barcode form.

answered 1 year ago by ![carl.ratz@phoenix.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_carl.ratz@phoenix.gov20170808200600.png) carl.ratz@phoenix.gov

- <a id="acceptAnswer_1294"></a>[Accept as answer](#)

<a id="commentAns_1294"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Looking for information on experiences with student IDs as library cards](https://iii.rightanswers.com/portal/controller/view/discussion/814?)
- [Looking to gather Libraries' experience with Student Cards](https://iii.rightanswers.com/portal/controller/view/discussion/444?)
- [Bookmobile set-up?](https://iii.rightanswers.com/portal/controller/view/discussion/1192?)
- [Best practices for importing student records](https://iii.rightanswers.com/portal/controller/view/discussion/725?)
- [Student cards - allow checkout of items?](https://iii.rightanswers.com/portal/controller/view/discussion/248?)

### Related Solutions

- [Anatomy of a TRN file](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180822133010554)
- [Patron Record Import TRN Format - Polaris 6.5](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200513113659590)
- [Unable to access offline transaction files through the Polaris Virtual Private Cloud](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930751966866)
- [Importing Student Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930247180454)
- [Imported Patrons with Password of 4321](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930727358148)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)