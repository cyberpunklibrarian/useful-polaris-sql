[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Is there a way to report the daily number of patrons checking out items within a particular month?

0

73 views

I've used the Item Statistical Report to generate the number of check-outs per hour, per day, but that's an imperfect proxy. I was hoping there was a way to show daily patron traffic (or, ideally, hourly traffic) to get a sense of any temporal patterns. Is this possible within SimplyReports? (Note that our library has a hosted server, so I can't run any SQL reports).

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_d7e2345512a44133901d1dc5252d83d7.jpg"/> carends@josephinelibrary.org

[checkouts](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=313#t=commResults) [patron](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=315#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_1200"></a>[](#)

1

<a id="downVoteAns_1200"></a>[](#)

Answer Accepted

Have your tried running the Hourly Circulation report in the Utilities > Reports and Notices via the Polaris Client?   

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_d7e2345512a44133901d1dc5252d83d7.jpg"/> lori@esrl.org

Unfortunately, that just shows me circ numbers, not actual number of patrons.

— carends@josephinelibrary.org 2 years ago

SR will not outright tell you how many patrons checked out within a hour, but you can estimate using the Hourly Circulation report and then running a SR Item Lists report. From that you can examine the Item record history and the item history date.  You'll need to throw your results into excel and then filter and sort it. This is about as close as you'll get without having access to SQL.

I use the following... Item barcode, Title, Check Out Branch Name, Patron Barcode, Action Taken and History Transaction Date.  Sometimes I add Patron ID.  Then in the Item Date Filters,  I select the Item record history transaction date and input my dates.

Using Excel..... Filter: Check out branches, Action Taken.  Sort by date/time. Remove dupliate patrons. This should give you an estimate of how many patrons within a certain time frame.

&nbsp;

&nbsp;

&nbsp;

&nbsp;

— lori@esrl.org 2 years ago

Hi Lori, thanks for the suggestion! I'll give it a shot.

— carends@josephinelibrary.org 2 years ago

So I ran the SR report per your recommendations, but for some reason I don't have a patron barcode nor ID for every checked-out item. I'll have one barcode for a row, then several rows with no patron barcode. I've filtered Action Taken for "Checked out" only and still have missing entries. Not sure why?

— carends@josephinelibrary.org 2 years ago

Because they are no longer the last patron to check out the item in the record history. You'll need to manually go into each item record and find the last patron and add it to your spreadsheet. The report you ran was for the month and so data will be overwritten.  

If you want a better count for the future.... create the report and then schedule it to run every night. Instead of using the Item Date filter use the Item Relative date filters (between two dates) to run 2 days ago and the report run date.  For example, I need the patron count for January 4th and since the Item Record history transactions do not include data for the current day **(day 1)**, I need to change it to **(day 2)**. This will include all the patron barcodes and the items that were checked out. This is the only way to ensure that you are getting all the data you need.  You may occassionaly get an item that will have no patron data on the off chance that a patron checked out an item in the morning and then returned it in time for another patron to check it out the very same day.   

You'll still need to use excel to filter and sort the data.

Good luck. 

&nbsp;

&nbsp;

— lori@esrl.org 2 years ago

Hi Lori, thanks for that very thorough explanation! I really appreciate it. I'll follow your advice and set up a recurring daily report. Cheers and happy new year!

— carends@josephinelibrary.org 2 years ago

<a id="commentAns_1200"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Reports for Floating Items](https://iii.rightanswers.com/portal/controller/view/discussion/62?)
- [I need a report of the total number of items checked out by user ID where the total number out is greater than 35](https://iii.rightanswers.com/portal/controller/view/discussion/961?)
- [Does anyone have a SQL report that will show renewal vs new cards for a particular patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/1377?)
- [Report for patrons with items out. If possible, sort by location?](https://iii.rightanswers.com/portal/controller/view/discussion/779?)
- [Does anyone have a damaged item template report you are willing to share?](https://iii.rightanswers.com/portal/controller/view/discussion/956?)

### Related Solutions

- [Why is in-house check in not counting as circulation?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930877939075)
- [Where to find Patron Circ Count in Patron record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930283871188)
- [Credit Card reports at Self-Check](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930668080115)
- [Sending Collection Agency Reports Manually](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930959368316)
- [Reports for On-the-Fly (OTF) items](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930655195362)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)