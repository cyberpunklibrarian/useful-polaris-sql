[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [General Announcements](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=55)

# Idea Lab: Polaris - Analyze This! Challenge

0

15 views

Hello Polaris Users,  
<br/>The new challenge is now open!  
<br/>[Polaris - Analyze This!](https://idealab.iii.com/analyzethis/Page/Overview) is for ideas that will improve functionality and introduce new features to support reporting for your library. Submit ideas about SimplyReports, improvements/additions to canned reports, data elements that would help with reporting or anything else that would enable you to gather data for your library.  
<br/>Have you been dreaming of better Simply Reports functionality? A new column in a transactions table? A new canned SSRS report? Idea submission is open today through Oct. 2. Remember, the earlier you submit your ideas, the more time they have to garner views, votes, and comments.  
<br/>New to [Idea Lab](https://idealab.iii.com/)? Idea Lab is designed to allow you to submit, discuss, and vote on ideas to improve Sierra and Polaris. Anyone at any Innovative library may participate in Idea Lab. Find information on how to create an Idea Lab account on our [FAQ page](https://www.innovativeusers.org/enhancement/idea-lab-faq).  
<br/>Eleanor Crumblehulme and Sarah Frieldsmith  
IUG Enhancements Co-Coordinators  
enhancements@innovativeusers.org

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_fc980d6a0e8d47f2b44ea39a7ec9251a.jpg"/> ecrumblehulme@sasklibraries.ca

[idea lab](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=436#t=commResults) [reporting](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=82#t=commResults) [simply reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=11#t=commResults) [sql reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=441#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Idea Lab Challenge](https://iii.rightanswers.com/portal/controller/view/discussion/921?)
- [Idea Lab Challenge](https://iii.rightanswers.com/portal/controller/view/discussion/927?)
- [Polaris Idea Lab Challenge Update](https://iii.rightanswers.com/portal/controller/view/discussion/499?)
- [Idea Lab: Polaris Potluck Challenge Extended](https://iii.rightanswers.com/portal/controller/view/discussion/453?)
- [Idea Lab: Challenge winners!](https://iii.rightanswers.com/portal/controller/view/discussion/1262?)

### Related Solutions

- [Using Idea Exchange to Submit an Enhancement](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=230403205834033)
- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Ingram SFTP Transmission](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240517101145090)
- [How To Request An Upgrade for Solus / Innovative Mobile App](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240528085937760)
- [IUG 2023: If I Ran the Zoo or: How an Idea Becomes a Product](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=230610201432497)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)