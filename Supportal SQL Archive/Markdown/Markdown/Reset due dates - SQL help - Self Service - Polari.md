[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Reset due dates - SQL help

0

52 views

Hi! I'm looking for some help ... I would like an SQL to find items that have had their due dates reset. I'm noticing a disturbing trend of some staff resetting due dates mulitples times (for months) on items with large holds queues. I couldn't find a way to get all the info I'm looking for in Simply Reports. Here's what I'd like in a report:

Item barcode; Item title; Patron barcode; Patron name; Patron registered branch; Dates the due date was reset (history action); Staff resetting the date

Any help?!?

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_0e1408c31d6b4730ab3caad9adbab878.jpg"/> musack@bcls.lib.nj.us

[due date reset](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=519#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 5 Replies   last reply 1 year ago

<a id="upVoteAns_1371"></a>[](#)

0

<a id="downVoteAns_1371"></a>[](#)

Thank you so much, Jason and Rex! You both rock!!

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_0e1408c31d6b4730ab3caad9adbab878.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_1371"></a>[Accept as answer](#)

<a id="commentAns_1371"></a>[Add a Comment](#)

<a id="upVoteAns_1370"></a>[](#)

0

<a id="downVoteAns_1370"></a>[](#)

I deleted my previous post because it was repeating the transactions so I updated it to eliminate that.

  
select distinct irh.ItemRecordHistoryID,cir.Barcode, b.SortTitle, vpr.Barcode, vpr.PatronFullName, vpr.BranchName, irh.TransactionDate,pu.\[Name\]  
from PolarisTransactions.Polaris.TransactionHeaders th (NOLOCK)  
join PolarisTransactions.Polaris.TransactionDetails td (NOLOCK)  
on td.TransactionID=th.TransactionID  
join PolarisTransactions.Polaris.TransactionDetails td2 (NOLOCK)  
on td2.TransactionID=th.TransactionID  
join Polaris.Polaris.ItemCheckouts ic (NOLOCK)  
on ic.ItemRecordID=td.numValue  
join Polaris.Polaris.ItemRecordHistory irh (NOLOCK)  
on irh.ItemRecordID=td.numValue  
join Polaris.Polaris.PolarisUsers pu (NOLOCK)  
on pu.PolarisUserID=irh.PolarisUserID  
join Polaris.Polaris.ViewPatronRegistration vpr (NOLOCK)  
on vpr.PatronID=td2.numValue  
join Polaris.Polaris.CircItemRecords cir (NOLOCK)  
on cir.ItemRecordID=ic.ItemRecordID  
join Polaris.Polaris.BibliographicRecords b (NOLOCK)  
on b.BibliographicRecordID=cir.AssociatedBibRecordID  
where th.TransactionTypeID=6003  
and th.TranClientDate < GETDATE()  
and td.TransactionSubTypeID=38  
and td2.TransactionSubTypeID=6  
and irh.ActionTakenID=65  
order by vpr.BranchName,vpr.Barcode,b.SortTitle, irh.TransactionDate

Rex Helwig  
Finger Lakes Library System

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_0e1408c31d6b4730ab3caad9adbab878.jpg"/> rhelwig@flls.org

- <a id="acceptAnswer_1370"></a>[Accept as answer](#)

<a id="commentAns_1370"></a>[Add a Comment](#)

<a id="upVoteAns_1369"></a>[](#)

0

<a id="downVoteAns_1369"></a>[](#)

That is EXACTLY what I'm looking for! Thank you ... you're a hero

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_0e1408c31d6b4730ab3caad9adbab878.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_1369"></a>[Accept as answer](#)

<a id="commentAns_1369"></a>[Add a Comment](#)

<a id="upVoteAns_1367"></a>[](#)

0

<a id="downVoteAns_1367"></a>[](#)

Thank you so much!! I'll give it a try. Thanks :)

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_0e1408c31d6b4730ab3caad9adbab878.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_1367"></a>[Accept as answer](#)

<a id="commentAns_1367"></a>[Add a Comment](#)

<a id="upVoteAns_1366"></a>[](#)

0

<a id="downVoteAns_1366"></a>[](#)

Hi there,

See the attached, it should give what you need.  It's been a while since I've looked at it closely, though, so you may want to look through it to be sure the logic makes sense.  Let me know if any questions come up...

\-Jason

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_0e1408c31d6b4730ab3caad9adbab878.jpg"/> jtenter@sasklibraries.ca

- <a id="acceptAnswer_1366"></a>[Accept as answer](#)

- [Due Date Reset Query (2023-04-18).txt](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/1324?fileName=1324-1366_Due+Date+Reset+Query+%282023-04-18%29.txt "Due Date Reset Query (2023-04-18).txt")

<a id="commentAns_1366"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Can anyone help me with a SQL for how many items are checked in daily?](https://iii.rightanswers.com/portal/controller/view/discussion/1185?)
- [I need help creating a SQL for circulation data that includes a lot of details.](https://iii.rightanswers.com/portal/controller/view/discussion/550?)
- [SQL for circulation by call number prefix and date range](https://iii.rightanswers.com/portal/controller/view/discussion/913?)
- [SQL query for circulation by call number range and date range](https://iii.rightanswers.com/portal/controller/view/discussion/190?)
- [COVID-19 Due Date Extensions, Fine Amnesty, Etc](https://iii.rightanswers.com/portal/controller/view/discussion/715?)

### Related Solutions

- [How do I reset the year-to-date circulation counts?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930831368342)
- [Can I change the due date of items that are already checked out?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930682806374)
- [What is the patron age update step in the patron processing job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930331250831)
- [Bulk change item due dates](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930588598835)
- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)