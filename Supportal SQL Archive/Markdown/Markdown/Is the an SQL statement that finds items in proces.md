Is the an SQL statement that finds items in processing that have holds? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Is the an SQL statement that finds items in processing that have holds?

0

109 views

I need to find the new books in processing that have holds on them. I need to catalog those first.

asked 2 years ago  by <img width="18" height="18" src="../../_resources/54abc2acb44341299a659fb8a42e91a0.jpg"/>  rhalterman@coralville.org

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 2 years ago

<a id="upVoteAns_701"></a>[](#)

0

<a id="downVoteAns_701"></a>[](#)

Another more general option might be a holds report on items in a (bib) record set. It gives some flexibility - anything you can put in a bib record set, you can check for holds on. 

select br.BrowseAuthor,br.BrowseTitle,ddv.NumberActiveHolds from polaris.Polaris.BibliographicRecords br with (nolock) inner join RWRITER_BibDerivedDataView ddv with (nolock) on (br.BibliographicRecordID = ddv.BibliographicRecordID) where br.RecordOwnerID in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21) and br.BibliographicRecordID in (select BibliographicRecordID from Polaris.BibRecordSets with (nolock) where RecordSetID = @RecordSetNumber)
order by NumberActiveHolds DESC

answered 2 years ago  by <img width="18" height="18" src="../../_resources/54abc2acb44341299a659fb8a42e91a0.jpg"/>  rpatterson@aclib.us

- <a id="acceptAnswer_701"></a>[Accept as answer](#)

<a id="commentAns_701"></a>[Add a Comment](#)

<a id="upVoteAns_560"></a>[](#)

0

<a id="downVoteAns_560"></a>[](#)

Thank  you for your help. I do have another question. I ran the report and it gave me Item Record IDs. Is there a way to get titles instead?

Thanks.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/54abc2acb44341299a659fb8a42e91a0.jpg"/>  rhalterman@coralville.org

- <a id="acceptAnswer_560"></a>[Accept as answer](#)

It's a bit slow, but this should work:

SELECT Holds.ItemRecordID, br.BrowseTitle, br.BrowseAuthor
FROM
(
SELECT ir.ItemRecordID, COUNT(shr.BibliographicRecordID) AS NumOfHolds FROM Polaris.ItemRecords ir
JOIN Polaris.SysHoldRequests shr
ON ir.AssociatedBibRecordID = shr.BibliographicRecordID
WHERE (
shr.BibliographicRecordID NOT IN
(
SELECT shr.BibliographicRecordID
FROM Polaris.ItemRecords subir
WHERE shr.BibliographicRecordID = subir.AssociatedBibRecordID
AND (COALESCE(RTRIM(LTRIM(shr.VolumeNumber)),'SMRLASeedVol') like COALESCE(RTRIM(LTRIM(subir.VolumeNumber)),'SMRLASeedVol') OR shr.VolumeNumber is null )
AND subir.ItemStatusID IN ('1','2','3','4','5','6','13','16','17','18','19')
)
OR
shr.SysHoldRequestID IN
(
SELECT shr.SysHoldRequestID FROM Polaris.ItemRecords subir
WHERE subir.ItemStatusID NOT IN ('1','2','3','4','5','6','13','16','17','18','19')
AND subir.ItemRecordID = shr.ItemLevelHoldItemRecordID
)
)
AND shr.SysHoldStatusID IN ('1','3')
AND ir.ItemStatusID IN ('15')
AND ir.AssociatedBibRecordID is not null
GROUP BY ir.ItemRecordID
HAVING COUNT(shr.BibliographicRecordID) >= 1
) AS Holds
JOIN CircItemRecords cir with (NOLOCK)
ON holds.itemRecordID = cir.ItemRecordID
JOIN BibliographicRecords br with (NOLOCK)
ON br.BibliographicRecordID = cir.AssociatedBibRecordID
ORDER BY br.SortTitle, br.BrowseAuthor

— jjack@aclib.us 2 years ago

Yes. It worked. Thank you!

— rhalterman@coralville.org 2 years ago

<a id="commentAns_560"></a>[Add a Comment](#)

<a id="upVoteAns_543"></a>[](#)

0

<a id="downVoteAns_543"></a>[](#)

Yes, we have two. One is for bib records with only "being cataloged" item records and the second is for bib records with item records that are being cataloged and with other statuses.

Hopefully they will work for you.

**ONLY Being Cataloged**

SELECT Holds.ItemRecordID

FROM

(

SELECT ir.ItemRecordID, COUNT(shr.BibliographicRecordID) AS NumOfHolds FROM Polaris.ItemRecords ir

JOIN Polaris.SysHoldRequests shr

ON ir.AssociatedBibRecordID = shr.BibliographicRecordID

WHERE (

shr.BibliographicRecordID NOT IN

(

SELECT shr.BibliographicRecordID

FROM Polaris.ItemRecords subir

WHERE shr.BibliographicRecordID = subir.AssociatedBibRecordID

AND (COALESCE(RTRIM(LTRIM(shr.VolumeNumber)),'SMRLASeedVol') like COALESCE(RTRIM(LTRIM(subir.VolumeNumber)),'SMRLASeedVol') OR shr.VolumeNumber is null )

AND subir.ItemStatusID IN ('1','2','3','4','5','6','13','16','17','18','19')

)

OR

shr.SysHoldRequestID IN

(

SELECT shr.SysHoldRequestID FROM Polaris.ItemRecords subir

WHERE subir.ItemStatusID NOT IN ('1','2','3','4','5','6','13','16','17','18','19')

AND subir.ItemRecordID = shr.ItemLevelHoldItemRecordID

)

)

AND shr.SysHoldStatusID IN ('1','3')

AND ir.ItemStatusID IN ('15')

AND ir.AssociatedBibRecordID is not null

GROUP BY ir.ItemRecordID

HAVING COUNT(shr.BibliographicRecordID) >= 1

) AS Holds

Holds on Being Cataloged but there are other items attached:

SELECT Holds.ItemRecordID

FROM

(

SELECT ir.ItemRecordID, COUNT(shr.SysHoldRequestID) AS NumOfHolds

FROM Polaris.ItemRecords ir

join polaris.SysHoldRequests shr

on shr.BibliographicRecordID = ir.AssociatedBibRecordID

WHERE ir.ItemStatusID IN ('15')

GROUP BY ir.ItemRecordID

HAVING COUNT(shr.SysHoldRequestID) >= 5

) AS Holds

answered 2 years ago  by <img width="18" height="18" src="../../_resources/54abc2acb44341299a659fb8a42e91a0.jpg"/>  sgrant@somd.lib.md.us

- <a id="acceptAnswer_543"></a>[Accept as answer](#)

<a id="commentAns_543"></a>[Add a Comment](#)

<a id="upVoteAns_542"></a>[](#)

0

<a id="downVoteAns_542"></a>[](#)

I would love to have an answer to this as well.  Currently I have to run two reports (bibs with holds and items in process) and combine them manually, which takes a while.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/54abc2acb44341299a659fb8a42e91a0.jpg"/>  kmanion@urbandale.org

- <a id="acceptAnswer_542"></a>[Accept as answer](#)

<a id="commentAns_542"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Deleting item records with holds](https://iii.rightanswers.com/portal/controller/view/discussion/32?)
- [Finding new and replaced bibs in Polaris between certain dates](https://iii.rightanswers.com/portal/controller/view/discussion/871?)
- [SQL for items withdrawn by a particular staff member](https://iii.rightanswers.com/portal/controller/view/discussion/809?)
- [Polaris OCLC Holdings](https://iii.rightanswers.com/portal/controller/view/discussion/410?)
- [Finding duplicate 035s, 050 etc.](https://iii.rightanswers.com/portal/controller/view/discussion/500?)

### Related Solutions

- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [How do I find item counts by material type?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930417498777)
- [How do I remove item records with the status of Deleted?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930423208345)
- [How are item records created from embedded holdings tags?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930494756808)
- [Finding unlinked bibs or authority records for database cleanup](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930305889326)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)