[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# SQL search for missing subfield

0

58 views

I'm trying to figure out a SQL search that will give me records that are missing a specific subfield in a specific MARC tag, for example, 300 fields missing the subfield a.

I've been working on tweaking SQL queries that are similar, but nothing is quite getting me there.

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_dc151a60bc15440b83b8f532ca517c5a.jpg"/> nbennyhoff@stchlibrary.org

[marc records](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=463#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults) [sql bibliographic](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=393#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_1403"></a>[](#)

1

<a id="downVoteAns_1403"></a>[](#)

Answer Accepted

I run this kind of query (that someone on a Polaris forum provided - THANK YOU, POLARIS FORUMS!) in the find tool SQL search for finding missing subfields.

Select DISTINCT BR.BibliographicRecordID  
From Polaris.BibliographicRecords BR  
JOIN Polaris.BibliographicTags BT  
ON BR.BibliographicRecordID = BT.BibliographicRecordID  
WHERE BR.RecordStatusID = 1  
AND BT.TagNumber = 300  
AND BT.BibliographicTagID NOT IN  
(  
Select BT.BibliographicTagID  
From Polaris.BibliographicRecords BR  
JOIN Polaris.BibliographicTags BT  
On BR.BibliographicRecordID = BT.BibliographicRecordID  
JOIN Polaris.bibliographicSubfields BS  
ON BT.BibliographicTagID = BS.BibliographicTagID  
WHERE BT.TagNumber = 300  
AND BS.Subfield = 'a'  
) 

&nbsp;

Hope it helps.

Erin

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_dc151a60bc15440b83b8f532ca517c5a.jpg"/> eshield@nols.org

<a id="commentAns_1403"></a>[Add a Comment](#)

<a id="upVoteAns_1404"></a>[](#)

0

<a id="downVoteAns_1404"></a>[](#)

Thank you so much!  
<br/>This seems to be doing exactly what we need.

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_dc151a60bc15440b83b8f532ca517c5a.jpg"/> nbennyhoff@stchlibrary.org

- <a id="acceptAnswer_1404"></a>[Accept as answer](#)

<a id="commentAns_1404"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Authority subfield mixup](https://iii.rightanswers.com/portal/controller/view/discussion/1253?)
- [Missing Cover Art](https://iii.rightanswers.com/portal/controller/view/discussion/1191?)
- [How should I be entering call number info in the 092 to make the subfields correctly correspond with item record call number fields?](https://iii.rightanswers.com/portal/controller/view/discussion/313?)
- [How do you enter/search for titles with special characters?](https://iii.rightanswers.com/portal/controller/view/discussion/320?)
- [Is anyone doing batch searching to the OCLC WorldCat Metadata API?](https://iii.rightanswers.com/portal/controller/view/discussion/1252?)

### Related Solutions

- [Database tables for Keyword and Browse indexing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930397363904)
- [856 tag display in the PowerPAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930661861606)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Item Record Shelving Scheme updated even though 852 tag did not include a Shelving Scheme subfield](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170227150057532)
- [Remove subfield e from 100 tag for RDA records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930517311975)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)