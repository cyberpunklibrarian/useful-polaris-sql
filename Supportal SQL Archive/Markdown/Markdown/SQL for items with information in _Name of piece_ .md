[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# SQL for items with information in "Name of piece"

0

40 views

We just realized that somebody has been putting the Title and other information in the "Name of piece" field in the Cataloging view of the item record. This is not a field we use, so we'd like to find a list of items that have anything in that field. I can't search on that field in Polaris or Simply Reports. Does anyone have an SQL to search for data in that field? I don't even know what that field would be called in the database tables. Thanks in advance!

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_93dcf09af6ea4dadb0aba20132759418.jpg"/> musack@bcls.lib.nj.us

[cataloging](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=4#t=commResults) [item records](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=190#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_1401"></a>[](#)

0

<a id="downVoteAns_1401"></a>[](#)

That worked ... thank you!!

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_93dcf09af6ea4dadb0aba20132759418.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_1401"></a>[Accept as answer](#)

<a id="commentAns_1401"></a>[Add a Comment](#)

<a id="upVoteAns_1400"></a>[](#)

0

<a id="downVoteAns_1400"></a>[](#)

Try

SELECT ird.NameOfPiece  
FROM \[Polaris\].\[Polaris\].\[ItemRecordDetails\]ird with (NOLOCK)  
WHERE ird.NameOfPiece is not null

&nbsp;

or if you want a Find tool search:

SELECT ird.ItemRecordID  
FROM \[Polaris\].\[Polaris\].\[ItemRecordDetails\]ird with (NOLOCK)  
WHERE ird.NameOfPiece is not null

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_93dcf09af6ea4dadb0aba20132759418.jpg"/> crosenthal@pwcgov.org

- <a id="acceptAnswer_1400"></a>[Accept as answer](#)

<a id="commentAns_1400"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for items withdrawn by a particular staff member](https://iii.rightanswers.com/portal/controller/view/discussion/809?)
- [Is the an SQL statement that finds items in processing that have holds?](https://iii.rightanswers.com/portal/controller/view/discussion/476?)
- [Formatting date information in volume field to match requests](https://iii.rightanswers.com/portal/controller/view/discussion/36?)
- [How do you purge withdrawn items](https://iii.rightanswers.com/portal/controller/view/discussion/186?)
- [Deleting item records with holds](https://iii.rightanswers.com/portal/controller/view/discussion/32?)

### Related Solutions

- [What holdings information is sent when my Polaris database is a remote target?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930755803285)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [How do I find item counts by material type?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930417498777)
- [How can Authority Control records be merged?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930673534728)
- [Find System Generated Authority Headings (Find Tool SQL)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=221207162751470)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)