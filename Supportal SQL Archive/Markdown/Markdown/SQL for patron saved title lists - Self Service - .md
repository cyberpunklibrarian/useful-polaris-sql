SQL for patron saved title lists - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for patron saved title lists

0

24 views

Does any one have a query that will tell me how many patrons how saved title lists, how many lists each patron has, and how many titles are on the lists?  I can't seem to find a table(s) that has this data.  Thanks!

asked 9 months ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

[title lists](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=403#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 8 months ago

<a id="upVoteAns_1041"></a>[](#)

0

<a id="downVoteAns_1041"></a>[](#)

Answer Accepted

Give this a try.  Hopefully it will give you a start on where to find what you're looking for.

SELECT PatronID, COUNT(PatronID) as "Saved Searches"
FROM SDIHeader (NOLOCK)
GROUP BY PatronID
ORDER BY PatronID

Rex Helwig
Finger Lakes Library System

answered 8 months ago  by <img width="18" height="18" src="../../_resources/70138d69fd644ec3a6b2ad5dd75d0142.jpg"/>  rhelwig@flls.org

<a id="commentAns_1041"></a>[Add a Comment](#)

<a id="upVoteAns_1044"></a>[](#)

0

<a id="downVoteAns_1044"></a>[](#)

Bummer, I misread it.  I cannot find where the Patron Lists data is stored either.

answered 8 months ago  by <img width="18" height="18" src="../../_resources/70138d69fd644ec3a6b2ad5dd75d0142.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_1044"></a>[Accept as answer](#)

<a id="commentAns_1044"></a>[Add a Comment](#)

<a id="upVoteAns_1043"></a>[](#)

0

<a id="downVoteAns_1043"></a>[](#)

Oops.  Spoke too quickly.  I'm looking to know how many saved title lists, not saved searches.  

\- Robin

answered 8 months ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

- <a id="acceptAnswer_1043"></a>[Accept as answer](#)

<a id="commentAns_1043"></a>[Add a Comment](#)

<a id="upVoteAns_1042"></a>[](#)

0

<a id="downVoteAns_1042"></a>[](#)

Thanks Rex!  Seems so simple now.  ![undecided](../../_resources/44ac3ba0ded942e09de4cf3e6f1afc58.gif)

\- Robin

answered 8 months ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

- <a id="acceptAnswer_1042"></a>[Accept as answer](#)

<a id="commentAns_1042"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [SQL to edit saved searches](https://iii.rightanswers.com/portal/controller/view/discussion/874?)
- [SQL to Find all bibs with same title.](https://iii.rightanswers.com/portal/controller/view/discussion/675?)
- [SQL for changes to a patron record](https://iii.rightanswers.com/portal/controller/view/discussion/840?)
- [Last modifier of patron record](https://iii.rightanswers.com/portal/controller/view/discussion/1048?)

### Related Solutions

- [Will Saved Title Lists be retained when two patrons are merged?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930896743928)
- [Saved List won't display? Easy solution](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181231155526984)
- [Saved title lists and withdrawn items](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930665093628)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Saved Searches emails are not being received](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930389681864)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)