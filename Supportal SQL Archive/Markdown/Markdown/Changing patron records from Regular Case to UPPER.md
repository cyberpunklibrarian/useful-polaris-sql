[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Changing patron records from Regular Case to UPPERCASE

0

49 views

Hello hello! We've recently changed our registration procedure to capitalizing patron names and addresses. Is there an easier way to change patron records created prior to this from regular case to uppercase than going one by one and retyping?? Thanks in advance! :) 

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_79eb6e8f744249ad9cd63bcc354cb6c8.jpg"/> elinacre@altoona-iowa.com

<a id="comment"></a>[Add a Comment](#)

## 5 Replies   last reply 1 year ago

<a id="upVoteAns_1340"></a>[](#)

0

<a id="downVoteAns_1340"></a>[](#)

If you are Hosted then you probably don't have direct access to your Polaris Database using Microsoft SQL Server Management Studio (MSSMS) so your only option may be manually touching patron record.  Maybe Polaris Support would run this for you.

The "BEGIN TRAN" should start every UPDATE statement you manually run so you can either Commit the changes or Rollback the changes.  Last I knew, if you're hosted you don't have access.

Do you print your own notices from Polaris Reports Manager?  When I print mine, the Name and Address are all automatically printed in Uppercase regardless of how they are entered in the Polaris Registration record.

"Also, how the heck do you KNOW this stuff?" - LOL.  I've been the Sys Admin for our Polaris install since day one in 2005.  I attended a Polaris SQL Boot Camp which was three days of intense head in the vise sessions that ran into well the evening but was some of the best training I've ever had.  From there, Internet searches are my friend and the Polaris Community.  there are much smarter people out there that i've relied over the years.

Rex Helwig  
Finger Lakes Library System  
rhelwig@flls.org

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_79eb6e8f744249ad9cd63bcc354cb6c8.jpg"/> rhelwig@flls.org

- <a id="acceptAnswer_1340"></a>[Accept as answer](#)

<a id="commentAns_1340"></a>[Add a Comment](#)

<a id="upVoteAns_1339"></a>[](#)

0

<a id="downVoteAns_1339"></a>[](#)

If you are Hosted then you probably don't have direct access to your Polaris Database using Microsoft SQL Server Management Studio (MSSMS) so your only option may be manually touching patron record.  Maybe Polaris Support would run this for you.

The "BEGIN TRAN" should start every UPDATE statement you manually run so you can either Commit the changes or Rollback the changes.  Last I knew, if you're hosted you don't have access.

Do you print your own notices from Polaris Reports Manager?  When I print mine, the Name and Address are all automatically printed in Uppercase regardless of how they are entered in the Polaris Registration record.

"Also, how the heck do you KNOW this stuff?" - LOL.  I've been the Sys Admin for our Polaris install since day one in 2005.  I attended a Polaris SQL Boot Camp which was three days of intense head in the vise sessions that ran into well the evening but was some of the best training I've ever had.  From there, Internet searches are my friend and the Polaris Community.  there are much smarter people out there that i've relied over the years.

Rex Helwig  
Finger Lakes Library System  
rhelwig@flls.org

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_79eb6e8f744249ad9cd63bcc354cb6c8.jpg"/> rhelwig@flls.org

- <a id="acceptAnswer_1339"></a>[Accept as answer](#)

<a id="commentAns_1339"></a>[Add a Comment](#)

<a id="upVoteAns_1338"></a>[](#)

0

<a id="downVoteAns_1338"></a>[](#)

If I could bother y'all for a bit more assistance (because obviously I'm a noob)--would I paste these into the Patron Record search under SQL Mode, or do I need access to something more in depth? (We're also hosted, if that makes a difference...? Or that may mean nothing, haha.)

Some kind souls have helped me with SQL before, but those were just to search for certain records which I added to a record set to bulk change, not actually updating them automatically with SQL, if that makes sense.

Brad--can each of those "UPDATE" chunks be done individually? I'm afraid I'm going to jack up our entire patron database, so I was going to just mess with the legal middle name fields or something, but I get an error--"The search could not be processed by the remote database: PR." (...because we're hosted? IDK.)

Rex--apparently we had some back and forth drama with the local Postmaster General who declared that the preferred format is all caps, and our director decided to MAKE IT SO. :) If you have a few, can you tell me more about this COMMIT and ROLLBACK? This is where I'm thinking maybe I don't have access to actually do this stuff, because those sound like "Go ahead" and an "Oops please undo" and I had no idea these were possibilities!

Also, how the heck do you KNOW this stuff?! I can only imagine how much tedious work we clueless folks do because we don't know any better!

Thank you SO MUCH for your time and patience! I really appreciate it!

&nbsp;

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_79eb6e8f744249ad9cd63bcc354cb6c8.jpg"/> elinacre@altoona-iowa.com

- <a id="acceptAnswer_1338"></a>[Accept as answer](#)

<a id="commentAns_1338"></a>[Add a Comment](#)

<a id="upVoteAns_1336"></a>[](#)

0

<a id="downVoteAns_1336"></a>[](#)

I'll forgo the question of why.

The following SQL uses a WHERE based on the first letter of the last name but can be deleted if you want.  May depend on how many patrons are in your database.  You may need to make adjustments for the Leagal Names if you're using them.

SELECT NameFirst, NameMiddle, NameLast, NameSuffix FROM PatronRegistration WHERE NameLast LIKE 'A%'  --SQL to view names. Delete Where if not needed.

&nbsp;

BEGIN TRAN  
UPDATE PatronRegistration  
SET NameFirst=UPPER(NameFirst), NameMiddle=UPPER(NameMiddle), NameLast=UPPER(NameLast), NameSuffix=UPPER(NameSuffix)  
WHERE NameLast LIKE 'A%'

\-- COMMIT   -- Once the Update stement is run and you're happy with the results after running the SELECT again, Run COMMIT   
\-- ROLLBACK   -- If you're not happy with the results,  Run ROLLBACK to restore.

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_79eb6e8f744249ad9cd63bcc354cb6c8.jpg"/> rhelwig@flls.org

- <a id="acceptAnswer_1336"></a>[Accept as answer](#)

<a id="commentAns_1336"></a>[Add a Comment](#)

<a id="upVoteAns_1335"></a>[](#)

0

<a id="downVoteAns_1335"></a>[](#)

Here's what we run nightly as a job:

&nbsp;

UPDATE polaris.PatronRegistration  
SET NameFirst = UPPER(NameFirst)  
WHERE NameFirst != UPPER(NameFirst)  
COLLATE Latin1_General_CS_AS

  
UPDATE polaris.PatronRegistration  
SET NameLast = UPPER(NameLast)  
WHERE NameLast != UPPER(NameLast)  
COLLATE Latin1_General_CS_AS

UPDATE polaris.PatronRegistration  
SET NameMiddle = UPPER(NameMiddle)  
WHERE NameMiddle != UPPER(NameMiddle)  
COLLATE Latin1_General_CS_AS

UPDATE polaris.Addresses  
SET StreetOne = UPPER(StreetOne)  
WHERE StreetOne != UPPER(StreetOne)  
COLLATE Latin1_General_CS_AS

UPDATE polaris.Addresses  
SET StreetTwo = UPPER(StreetTwo)  
WHERE StreetTwo != UPPER(StreetTwo)  
COLLATE Latin1_General_CS_AS

UPDATE polaris.PatronRegistration  
SET LegalNameFirst = UPPER(LegalNameFirst)  
WHERE LegalNameFirst != UPPER(LegalNameFirst)  
COLLATE Latin1_General_CS_AS  
and LegalNameFirst is not NULL

  
UPDATE polaris.PatronRegistration  
SET LegalNameLast = UPPER(LegalNameLast)  
WHERE LegalNameLast != UPPER(LegalNameLast)  
COLLATE Latin1_General_CS_AS  
and LegalNameLast is not NULL

UPDATE polaris.PatronRegistration  
SET LegalNameMiddle = UPPER(LegalNameMiddle)  
WHERE LegalNameMiddle != UPPER(LegalNameMiddle)  
COLLATE Latin1_General_CS_AS  
and LegalNameMiddle is not NULL

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_79eb6e8f744249ad9cd63bcc354cb6c8.jpg"/> bcsmith@cityofboise.org

- <a id="acceptAnswer_1335"></a>[Accept as answer](#)

<a id="commentAns_1335"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Is there a way to set Polaris to change expiration dates and address checks for new cards based on the patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/1213?)
- [SQL or Scoping for Items Withdrawn NOT linked to patron fee records](https://iii.rightanswers.com/portal/controller/view/discussion/525?)
- [Making Changes to PolarisTransactions Databae](https://iii.rightanswers.com/portal/controller/view/discussion/104?)
- [PACREG Patrons - Virtual Patrons](https://iii.rightanswers.com/portal/controller/view/discussion/724?)
- [Updating Telephony outbound human voice recording](https://iii.rightanswers.com/portal/controller/view/discussion/780?)

### Related Solutions

- [How do I change the text of the You Saved receipts?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170119085231340)
- [Staff notification of patron account changes via the PAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930694354968)
- [Bulk Change Circulation Statuses](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930606607662)
- [Cannot bulk change library assigned blocks](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930641484135)
- [Importing Student Records as Patron Records - Polaris 5.6](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170927144406767)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)