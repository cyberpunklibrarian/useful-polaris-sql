[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Creating Jobs in SQL Agent for an SSIS package

0

60 views

Has anyone experience scheduling an SSIS package that lives on the SQL server and the import file (excel) resides on a local pc?

Creating the SSIS package was the easy part. I works manually. I would like to schedule to run periodically.

The configuration part is baffling. Have read some documentation online.

The server is 64-bit, the Package was set up using 32-bit.

I am importing a list of BibliographicRecordIDs

Please email privately.

Thank you.

Jon Lellelid

Sno-Isle Libraries

jlellelid@sno-isle.org

&nbsp;

asked 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_51bee3023cca41d8b682a2537fc6d997.jpg"/> jlellelid@sno-isle.org

- [SnoIsle SSIS Package.docx](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/403?fileName=403-SnoIsle+SSIS+Package.docx "SnoIsle SSIS Package.docx")

[sql job agent](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=191#t=commResults) [ssis](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=192#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 5 years ago

<a id="upVoteAns_525"></a>[](#)

0

<a id="downVoteAns_525"></a>[](#)

Hi Jon,

&nbsp;

I have set up an SSIS package that is scheduled on the Server. 

&nbsp;

Thank you.

Femi Banjo

Prince George Memorial Library system

femi.banjo@pgcmls.info

&nbsp;

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_51bee3023cca41d8b682a2537fc6d997.jpg"/> femi.banjo@pgcmls.info

- <a id="acceptAnswer_525"></a>[Accept as answer](#)

<a id="commentAns_525"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Creating a new branch for holds pickup](https://iii.rightanswers.com/portal/controller/view/discussion/119?)
- [Advice on creating our own table in Polaris database](https://iii.rightanswers.com/portal/controller/view/discussion/1300?)
- [SQL for waiving certain fines](https://iii.rightanswers.com/portal/controller/view/discussion/192?)
- [SQL for Range of Closed Dates](https://iii.rightanswers.com/portal/controller/view/discussion/716?)
- [Deleting Material Types using SQL](https://iii.rightanswers.com/portal/controller/view/discussion/323?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Configure SQL Server Agent Mail to Use Database Mail](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930507425889)
- [Saved Searches emails are not being received](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930389681864)
- [How can SQL jobs be accessed?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930442864574)
- [CJ Chained Job Launcher SQL job](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930553523996)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)