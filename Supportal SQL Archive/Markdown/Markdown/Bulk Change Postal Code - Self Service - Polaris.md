[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Bulk Change Postal Code

0

28 views

We use a couple of "custom" postal codes. For example, we created 99998 for one of our school virtual cards. If we add these into a record set, is there any way of bulk changing the postal code (maybe via SQL)? 

The reason I ask is I am finding some mistakes and would like to tackle them in bulk rather than one by one , if possible.

Thanks,

Chad

asked 11 months ago by ![chad.eller@gastongov.com](https://iii.rightanswers.com/portal/app/images/custom/avatar_chad.eller@gastongov.com20210201162116.jpg) chad.eller@gastongov.com

[bulk change](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=77#t=commResults) [postal code](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=524#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [change loan period on material by patron code](https://iii.rightanswers.com/portal/controller/view/discussion/163?)
- [Patron Code "Staff "override](https://iii.rightanswers.com/portal/controller/view/discussion/704?)
- [Patron passwords have been mysteriously changing](https://iii.rightanswers.com/portal/controller/view/discussion/558?)
- [Adding a QR code to receipts](https://iii.rightanswers.com/portal/controller/view/discussion/1332?)
- [Does anyone know how to batch update the patron code field?](https://iii.rightanswers.com/portal/controller/view/discussion/886?)

### Related Solutions

- [Wrong default postal code](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180316120042002)
- [Polaris won't let me enter a Canadian postal code](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930803432388)
- [Incorrect postal codes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930359078195)
- [What to check after setting up a new code in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170228154215576)
- [What happens when we delete a postal code that's in use?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930308320846)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)