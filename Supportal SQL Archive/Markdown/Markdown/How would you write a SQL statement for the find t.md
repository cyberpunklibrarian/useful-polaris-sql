How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give! - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!

0

69 views

SELECT DISTINCT BR.BibliographicRecordID FROM BibliographicRecords BR WITH (NOLOCK)
JOIN CircItemRecords CI (nolock) ON (CI.AssociatedBibRecordID = BR.BibliographicRecordID)
JOIN RWRITER_BibDerivedDataView RW (NOLOCK) ON (BR.BibliographicRecordID = RW.BibliographicRecordID)
WHERE CI.AssignedCollectionID IN (2,6,9) AND (RW.NumberofItems>=10) AND BR.RecordStatusID = 1

This works, but doesn't filter for lost, missing, etc. I think I did something wrong at the end, but asking for circ item status of one doesn't work at all. 

Thank you!

asked 2 years ago  by <img width="18" height="18" src="../../_resources/cbc30976df9647ed8e3ba478635bd916.jpg"/>  crosbys@hillsboroughcounty.org

[sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_573"></a>[](#)

0

<a id="downVoteAns_573"></a>[](#)

Wow that was quick. This seems exactly what we are looking for. Thanks so much!

answered 2 years ago  by <img width="18" height="18" src="../../_resources/cbc30976df9647ed8e3ba478635bd916.jpg"/>  crosbys@hillsboroughcounty.org

- <a id="acceptAnswer_573"></a>[Accept as answer](#)

You're welcome!  I'm glad to help.

— jjack@aclib.us 2 years ago

<a id="commentAns_573"></a>[Add a Comment](#)

<a id="upVoteAns_572"></a>[](#)

0

<a id="downVoteAns_572"></a>[](#)

BR.RecordStatusID is the status of the Bib record (e.g. final, deleted, etc.)

You probably want something like this: 

SELECT DISTINCT BR.BibliographicRecordID FROM BibliographicRecords BR WITH (NOLOCK)
JOIN CircItemRecords CI (nolock) ON (CI.AssociatedBibRecordID = BR.BibliographicRecordID)
JOIN RWRITER_BibDerivedDataView RW (NOLOCK) ON (BR.BibliographicRecordID = RW.BibliographicRecordID)
WHERE CI.AssignedCollectionID IN (2,6,9) AND RW.NumberofItems - RW.NumberLostItems - RW.NumberClaimRetItems - RW.NumberWithdrawnItems - RW.NumberMissingItems - RW.NumberSHRCopies >= 10

You may or may not have a column "RW.NumberSHRCopies" -- I'm not clear on if that is standard or if it was set up specifically for us.

If this isn't working for you and/or isn't what you're looking for, please let me know.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/cbc30976df9647ed8e3ba478635bd916.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_572"></a>[Accept as answer](#)

<a id="commentAns_572"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL - looking for a report on how many fines we have for a specific collection code and how many were waived.](https://iii.rightanswers.com/portal/controller/view/discussion/673?)
- [Circulation by Item Statistical Code & limited by assigned collection?](https://iii.rightanswers.com/portal/controller/view/discussion/801?)
- [SQL query to export specific record set: Polaris 4.1R2](https://iii.rightanswers.com/portal/controller/view/discussion/93?)
- [SQL for Lost and Paid Items](https://iii.rightanswers.com/portal/controller/view/discussion/662?)
- [SQL for items returned late by collection](https://iii.rightanswers.com/portal/controller/view/discussion/718?)

### Related Solutions

- [Items converting to Lost status](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930282614657)
- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Overdrive integration creating non-integrated items and no resource entity](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170503103227830)
- [EDI items coming in with the wrong collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930695455841)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)