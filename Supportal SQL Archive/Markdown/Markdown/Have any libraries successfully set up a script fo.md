[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Have any libraries successfully set up a script for transferring CollectionHQ extracts automatically?

0

32 views

Have any libraries successfully set up a script for transferring CollectionHQ extracts automatically? 

Thanks,

Tomiko Kutyna

Alachua County Library District

asked 2 months ago by <img width="18" height="18" src="../_resources/default-avatar_17214c3d20ec4c1b8f61800c5d148c91.jpg"/> tkutyna@aclib.us

[collection hq](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=550#t=commResults) [collectionhq](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=551#t=commResults) [extract](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=552#t=commResults) [script](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=553#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 month ago

<a id="upVoteAns_1444"></a>[](#)

0

<a id="downVoteAns_1444"></a>[](#)

Answer Accepted

Here is what we do: https://forum.innovativeusers.org/t/automatically-sending-collectionhq-marc-files/1927

answered 1 month ago by <img width="18" height="18" src="../_resources/default-avatar_17214c3d20ec4c1b8f61800c5d148c91.jpg"/> wosborn@clcohio.org

<a id="commentAns_1444"></a>[Add a Comment](#)

<a id="upVoteAns_1442"></a>[](#)

0

<a id="downVoteAns_1442"></a>[](#)

Following...

answered 1 month ago by <img width="18" height="18" src="../_resources/default-avatar_17214c3d20ec4c1b8f61800c5d148c91.jpg"/> jplpolaris@coj.net

- <a id="acceptAnswer_1442"></a>[Accept as answer](#)

<a id="commentAns_1442"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [CollectionHQ Transaction and Holds Data Extracts](https://iii.rightanswers.com/portal/controller/view/discussion/358?)
- [Does anyone have a sql script for a monthly in-house check-in count?](https://iii.rightanswers.com/portal/controller/view/discussion/615?)
- [Holds alert by library report for multi-branch libraries](https://iii.rightanswers.com/portal/controller/view/discussion/1310?)
- [Anyone have a sql script that will total fines for a year and then break them down by patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/688?)
- [Help with automatically exporting SQL query to CSV file](https://iii.rightanswers.com/portal/controller/view/discussion/1399?)

### Related Solutions

- [How to update Collection HQ from a monthly to a weekly extract](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930966073176)
- [How do I sign up for emails about press releases and other Innovative news?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170426103239987)
- [Requirements for CollectionHQ Exports](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930727440796)
- [IUG 2021: Customer Experience - What's New and Ahead for Library Services](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210413101230203)
- [IUG 2012: Success Stories: Innovative Services Helping Libraries](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160421115427485)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)