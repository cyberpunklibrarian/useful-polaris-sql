Need an sql for circulation by workstation between two dates - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Need an sql for circulation by workstation between two dates

0

118 views

I am in need of a sql query for circulation by workstation for a time of between two dates.

asked 3 years ago  by ![debmcl@carr.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_debmcl@carr.org20191231094326.JPG)  debmcl@carr.org

<a id="comment"></a>[Add a Comment](#)

## 6 Replies   last reply 3 years ago

<a id="upVoteAns_115"></a>[](#)

0

<a id="downVoteAns_115"></a>[](#)

Thank you. That works.

answered 3 years ago  by ![debmcl@carr.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_debmcl@carr.org20191231094326.JPG)  debmcl@carr.org

- <a id="acceptAnswer_115"></a>[Accept as answer](#)

<a id="commentAns_115"></a>[Add a Comment](#)

<a id="upVoteAns_113"></a>[](#)

0

<a id="downVoteAns_113"></a>[](#)

This is pretty basic, but should work assuming the SIP Workstation was left with the default name.

SELECT COUNT(th.TransactionID)
FROM PolarisTransactions.Polaris.TransactionHeaders AS \[th\] WITH (NOLOCK)
INNER JOIN Polaris.Polaris.Workstations AS \[w\] WITH (NOLOCK)
ON th.WorkstationID = w.WorkstationID
WHERE th.TransactionTypeID = '6001'
AND w.ComputerName LIKE 'SIP Workstation'
AND th.TranClientDate BETWEEN '20170101 00:00:00' AND '20170131 23:59:59'

answered 3 years ago  by <img width="18" height="18" src="../../_resources/954a0beb56134482979a1f3bb8b3e35e.jpg"/>  trevor.diamond@mainlib.org

- <a id="acceptAnswer_113"></a>[Accept as answer](#)

<a id="commentAns_113"></a>[Add a Comment](#)

<a id="upVoteAns_112"></a>[](#)

0

<a id="downVoteAns_112"></a>[](#)

I know of that canned report, I need the sql to plug into a third party statistics site.

answered 3 years ago  by ![debmcl@carr.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_debmcl@carr.org20191231094326.JPG)  debmcl@carr.org

- <a id="acceptAnswer_112"></a>[Accept as answer](#)

<a id="commentAns_112"></a>[Add a Comment](#)

<a id="upVoteAns_110"></a>[](#)

0

<a id="downVoteAns_110"></a>[](#)

What are you looking for that is different from the Circulation by Workstation report in the Circulation folder of the reports area?

answered 3 years ago  by <img width="18" height="18" src="../../_resources/954a0beb56134482979a1f3bb8b3e35e.jpg"/>  trevor.diamond@mainlib.org

- <a id="acceptAnswer_110"></a>[Accept as answer](#)

<a id="commentAns_110"></a>[Add a Comment](#)

<a id="upVoteAns_109"></a>[](#)

0

<a id="downVoteAns_109"></a>[](#)

I am intersted in circulation counts for my self-check workstations only for a timeframe, either by month or by year.

answered 3 years ago  by ![debmcl@carr.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_debmcl@carr.org20191231094326.JPG)  debmcl@carr.org

- <a id="acceptAnswer_109"></a>[Accept as answer](#)

<a id="commentAns_109"></a>[Add a Comment](#)

<a id="upVoteAns_108"></a>[](#)

0

<a id="downVoteAns_108"></a>[](#)

Can you explain a bit more about what you're looking for?  Do you need the circ figures for a particular workstation, or for all workstations?  Are you looking for the number of circs per hour? per day?  Are you interested only in checkouts, also checkins, or just certain statuses you'd want to specify yourself?  Are the circs recent (so almost certainly still in ItemRecordHistory) or could they include deleted items (so you'd need to go into the transaction tables)?

answered 3 years ago  by <img width="18" height="18" src="../../_resources/954a0beb56134482979a1f3bb8b3e35e.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_108"></a>[Accept as answer](#)

<a id="commentAns_108"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)
- [SQL for circulation numbers](https://iii.rightanswers.com/portal/controller/view/discussion/94?)
- [SQL top bib checkouts between 2 dates](https://iii.rightanswers.com/portal/controller/view/discussion/650?)
- [Need help with query to find checked out titles with holds](https://iii.rightanswers.com/portal/controller/view/discussion/1063?)
- [Has anyone developed a SimplyReport/SQL Report to answer PLA's Public Library Data Service circulation questions?](https://iii.rightanswers.com/portal/controller/view/discussion/439?)

### Related Solutions

- [How do I reset the year-to-date circulation counts?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930831368342)
- [Polaris Operating System Requirements (May 2021)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930342582785)
- [CopyOfflineFiles Job is not updating the offline files on workstations](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930838055588)
- [Bulk change item due dates](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930588598835)
- [What is the patron age update step in the patron processing job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930331250831)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)