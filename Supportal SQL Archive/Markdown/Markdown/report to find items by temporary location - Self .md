[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# report to find items by temporary location

0

78 views

Can anyone provide me with a SQL query I can run in the Item Find Tool to locate items with text in the Temporary Shelf Location?

asked 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_b4b8fa0266ca43c0a0c53900d12b434f.jpg"/> esouth@flpl.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 5 years ago

<a id="upVoteAns_652"></a>[](#)

0

<a id="downVoteAns_652"></a>[](#)

Answer Accepted

I think that this will do what you want. It looks for Final items with anything in the Temporary Shelf Location.

SELECT ItemRecordID FROM ItemRecords WITH (NOLOCK)  
WHERE RecordStatusID = 1  
AND TemporaryShelfLocation IS NOT NULL

If you want to find Deleted or Provisional items, you can change 1 to 4 for Deleted or 2 for Provisional.

Hope this helps,

JT

&nbsp;

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_b4b8fa0266ca43c0a0c53900d12b434f.jpg"/> jwhitfield@aclib.us

Perfect!  Does just what I need.  Thank you so much!

— esouth@flpl.org 5 years ago

<a id="commentAns_652"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Report for patrons with items out. If possible, sort by location?](https://iii.rightanswers.com/portal/controller/view/discussion/777?)
- [Report of Lost and Paid Items](https://iii.rightanswers.com/portal/controller/view/discussion/331?)
- [Adding number of "live" items to a report](https://iii.rightanswers.com/portal/controller/view/discussion/666?)

### Related Solutions

- [How do I set a default location for the bulk change report?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930976945995)
- [Item Circulation Statistics Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930404036798)
- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [Item Circulation by Statistical Code](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930592909894)
- [Claimed item does not appear in the Claimed Items report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930283717537)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)