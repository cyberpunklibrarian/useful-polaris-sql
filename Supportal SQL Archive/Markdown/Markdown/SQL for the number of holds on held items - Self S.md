SQL for the number of holds on held items - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for the number of holds on held items

0

78 views

Does anyone have a query that can list the items that are on the hold shelf (held) at a given branch, along with the number of other holds on that title?  I'd really appreciate it!

\- Robin

asked 1 year ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

[holds; held items](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=333#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_845"></a>[](#)

0

<a id="downVoteAns_845"></a>[](#)

Answer Accepted

Hi Robin.

I think this will work for you. It looks for holds that are in Held status at a particular pickup branch. I included the option for NewPickupBranchID in case there is a difference from the original. This may result in a line showing up where the original pickup branch is the desired branch, but the hold has been moved to another branch after it became Held.  This should be pretty rare. If you think it would be so rare that it's not worth including, you can change that line to "WHERE PickupBranchID = 4" (whichever is your desired branch). If you do include it, make sure to change the branch ID in both locations on this line.

This excludes titles (bibs) that have no other active holds (Active, Pending, Shipped or Located statuses) but you can comment out the HAVING line if you want to include those titles.

It includes item-level holds in the total holds count. (The number of active holds in RW_BibDerivedDataView does not include them, which is why I had to create the subquery t1.)

Hope this helps.

JT

SELECT

shr.BibliographicRecordID
,br.BrowseTitle
,CONCAT (ir.Callnumber,' ', ir.VolumeNumber) AS ItemCallNummber
,ir.Barcode
,COUNT(t1.BibliographicRecordID) as OtherActiveHolds

FROM
Polaris.Polaris.SysHoldRequests shr WITH (NOLOCK)
JOIN Polaris.Polaris.BibliographicRecords br (NOLOCK)
ON (shr.BibliographicRecordID = br.BibliographicRecordID)
JOIN Polaris.Polaris.ItemRecords ir (NOLOCK)
ON (shr.TrappingItemRecordID = ir.ItemRecordID)

LEFT JOIN
(SELECT shr1.BibliographicRecordID
FROM Polaris.Polaris.SysHoldRequests shr1 WITH (NOLOCK)
WHERE shr1.SysHoldStatusID IN (3,4,5,18) --Active, Pending, Shipped, Located
) t1
ON (shr.BibliographicRecordID = t1.BibliographicRecordID)

WHERE (shr.PickupBranchID = 4 OR shr.NewPickupBranchID = 4)
AND shr.SysHoldStatusID = 6

GROUP BY
shr.BibliographicRecordID
,br.BrowseTitle
,ir.Barcode
,CONCAT(ir.Callnumber,' ', ir.VolumeNumber)

HAVING (COUNT(t1.BibliographicRecordID) > 0 ) --Comment out this line to include items with no other active holds

ORDER BY
CONCAT(ir.Callnumber,' ', ir.VolumeNumber)
,br.BrowseTitle

answered 1 year ago  by <img width="18" height="18" src="../../_resources/266d7fff34ad47babda1d684947bad37.jpg"/>  jwhitfield@aclib.us

<a id="commentAns_845"></a>[Add a Comment](#)

<a id="upVoteAns_846"></a>[](#)

0

<a id="downVoteAns_846"></a>[](#)

Super!  Just what I need.  Thanks JT!

\- Robin

answered 1 year ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

- <a id="acceptAnswer_846"></a>[Accept as answer](#)

<a id="commentAns_846"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [SQL for bibs with holds and no owned items by branches](https://iii.rightanswers.com/portal/controller/view/discussion/821?)
- [SQL for Bibs w/no good items but have holds - including item and patron info](https://iii.rightanswers.com/portal/controller/view/discussion/594?)
- [Need help with query to find checked out titles with holds](https://iii.rightanswers.com/portal/controller/view/discussion/1063?)
- [I'm looking for a SQL report that will tell how long items are in held status.](https://iii.rightanswers.com/portal/controller/view/discussion/495?)

### Related Solutions

- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)
- [Item Held Longer Than Expected](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930398757933)
- [SysHoldRequest table missing Title information](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930733866097)
- [Date an item is held until in the telephony voice message](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930375783756)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)