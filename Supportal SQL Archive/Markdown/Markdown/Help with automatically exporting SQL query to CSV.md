[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Help with automatically exporting SQL query to CSV file

0

43 views

Hi all.  
<br/>We've recently had our ILS admin move on from her position, and our IT department is trying to fill her shoes!  
<br/>We're looking to do a bi-weekly export to get all of our new patron registrations out to our Communications team for welcome emails.  
<br/>I wrote a SQL query that gathers this info... but I can't seem to find a way to schedule it as an automatic CSV export (or other file type)  
<br/>I see how to create it as a scheduled job, it's just the export piece that I'm missing. Thanks!

  
<br/>Here's the Query so far:  
<br/>select RegistrationDate, NameFirst, NameLast, EmailAddress, POR.Name  
<br/>from Polaris.Polaris.PatronRegistration PR with nolock  
<br/>inner join Polaris.Polaris.Patrons PAT with nolock  
<br/>on PR.Patr PAT.PatronID  
<br/>inner join Polaris.polaris.organizations POR with nolock  
<br/>on PAT.OrganizationID POR.OrganizationID  
<br/>

where RegistrationDate >= DATEADD(DAY, -14, GETDATE())

order by RegistrationDate

asked 1 month ago by <img width="18" height="18" src="../_resources/default-avatar_0913aad3d92a4677818e426e8a0f9ff8.jpg"/> ntamburro@krl.org

[sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults) [sql server management studio](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=305#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 month ago

<a id="upVoteAns_1443"></a>[](#)

1

<a id="downVoteAns_1443"></a>[](#)

&nbsp;

- **Create and Deploy the Report**:
    
    - Open Report Builder and create a new report using your SQL query to gather new patron registrations.
    - Design the report layout using tables or lists to display the data.
    - Save and deploy the report to the SSRS server.
- **Access Report Manager**:
    
    - Navigate to the SSRS Report Manager URL, typically `https://<YourServerName>/reports`.
    - Locate the deployed report in the report server folder structure.
- **Create a Subscription**:
    
    - Click on the report to open it.
    - In the toolbar, click on the `Subscriptions` tab.
    - Click `New Subscription` to create a new email subscription.
- **Configure Subscription Details**:
    
    - **Subscription Type**: Choose `E-Mail Subscription`.
    - **To**: Enter the email addresses of the Communications team.
    - **Subject**: Set the email subject line.
    - **Render Format**: Select `CSV` as the output format.
    - **File Name**: Specify a name for the CSV file.
    - **Comment**: Optionally, add a message for the email body.
    - **Include Report**: Ensure the option to include the report is selected.
- **Set Up the Schedule**:
    
    - In the subscription settings, locate the `Schedule` section.
    - Click `Configure schedule` to set up the delivery frequency.
    - Choose `Bi-weekly` and specify the exact days and times for the report to be sent.
- **Save and Test**:
    
    - Save the subscription configuration.
    - Run the subscription manually to test the setup and ensure the Communications team receives the email with the CSV attachment.

&nbsp;

&nbsp;

answered 1 month ago by ![eric.young@phoenix.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_eric.young@phoenix.gov20191029113427.jpg) eric.young@phoenix.gov

- <a id="acceptAnswer_1443"></a>[Accept as answer](#)

<a id="commentAns_1443"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL query to export specific record set: Polaris 4.1R2](https://iii.rightanswers.com/portal/controller/view/discussion/93?)
- [Need help with query to find checked out titles with holds](https://iii.rightanswers.com/portal/controller/view/discussion/1063?)
- [SQL help for for circ info on 'new' stuff](https://iii.rightanswers.com/portal/controller/view/discussion/698?)
- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [Is there still a SQL repository for the Polaris forum?](https://iii.rightanswers.com/portal/controller/view/discussion/706?)

### Related Solutions

- [Export from old Supportal of forum posts with SQL queries](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161214213310321)
- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Automatically open SQL Server query window](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930908117973)
- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)
- [Polaris help files favorites](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930314453696)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)