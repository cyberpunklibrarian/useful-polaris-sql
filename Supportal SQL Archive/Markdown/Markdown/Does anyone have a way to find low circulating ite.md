Does anyone have a way to find low circulating items via SR or SQL? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Does anyone have a way to find low circulating items via SR or SQL?

0

38 views

Hello,
Does anyone have a good way of reporting on low circulating materials?  My library would like to be able to see items that have gone out under 3 times in the past 5 years since they suspect they’re only keeping these items because 1 person randomly checked it out recently.
-Kevin
Kevin French
kfrench@gmilcs.org
System Administrator
GMILCS, Inc.

asked 3 months ago  by <img width="18" height="18" src="../../_resources/9b72651d2af24ecc94cfe1be51d129b7.jpg"/>  kfrench@gmilcs.org

[circulation](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=185#t=commResults) [simply reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=11#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 3 months ago

<a id="upVoteAns_1112"></a>[](#)

0

<a id="downVoteAns_1112"></a>[](#)

Answer Accepted

Hey Kevin!

This is pretty basic, so you may want to add some conditions.  Basically you'll find everything that has circed more then 3 times in the last five years and then find all the items that are not in that list.  I excluded renewals (so only "real" checkouts are counted) and did add the condition that the item must have a first available date of at least 5 years ago.

SELECT *
FROM Polaris.Polaris.CircItemRecords AS \[cir\] WITH (NOLOCK)
WHERE cir.FirstAvailableDate < DATEADD(year,-5,GETDATE())
AND cir.ItemRecordID NOT IN (
SELECT td.numValue
FROM PolarisTransactions.Polaris.TransactionHeaders AS \[th\] WITH (NOLOCK)
INNER JOIN PolarisTransactions.Polaris.TransactionDetails AS \[td\] WITH (NOLOCK)
ON th.TransactionID = td.TransactionID AND td.TransactionSubTypeID = '38'
LEFT OUTER JOIN PolarisTransactions.Polaris.TransactionDetails AS \[renew\] WITH (NOLOCK)
ON th.TransactionID = renew.TransactionID AND renew.TransactionSubTypeID = '124'
WHERE th.TransactionTypeID = '6001'
AND renew.numValue IS NULL
AND th.TranClientDate > DATEADD(year,-5,GETDATE())
GROUP BY td.numValue
HAVING COUNT(td.numValue) > '3')

answered 3 months ago  by <img width="18" height="18" src="../../_resources/9b72651d2af24ecc94cfe1be51d129b7.jpg"/>  trevor.diamond@mainlib.org

<a id="commentAns_1112"></a>[Add a Comment](#)

<a id="upVoteAns_1113"></a>[](#)

0

<a id="downVoteAns_1113"></a>[](#)

Thank you Trevor!

This will work nicely. 

I really appreciate the assistance.

answered 3 months ago  by <img width="18" height="18" src="../../_resources/9b72651d2af24ecc94cfe1be51d129b7.jpg"/>  kfrench@gmilcs.org

- <a id="acceptAnswer_1113"></a>[Accept as answer](#)

<a id="commentAns_1113"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Searching for Items that have not circ'd in 2 years ex. DVD's](https://iii.rightanswers.com/portal/controller/view/discussion/1053?)
- [Simply Reports Discrepancies](https://iii.rightanswers.com/portal/controller/view/discussion/960?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)
- [Can you get a list of exact dates an item was placed on hold?](https://iii.rightanswers.com/portal/controller/view/discussion/1031?)

### Related Solutions

- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [Simply Reports session is locked](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930921070501)
- [Simply Reports Overview](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160908151835082)
- [Finding bibs with no items attached and putting them into a record set](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161021140649266)
- [How was the data gathered in SimplyReports?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930857021045)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)