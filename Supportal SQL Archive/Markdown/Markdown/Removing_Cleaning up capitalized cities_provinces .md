[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Removing/Cleaning up capitalized cities/provinces

0

58 views

Hello,

&nbsp;

When staff are filling in a new patron record, and they type in a Postal Code, it presents them with a list of City/Province/County that has been previsouly entered for that postal code.

&nbsp;

We have patrons with "Guelph, ON", or "GUELPH, On" or "Guelph, Ontario" etc. We'd like to have them all the same as "Guelph, ON".

Can't seem to change that through a bulk change record set, wondering where I might change this through SQL, if possible?

&nbsp;

And, after accounts have been updated to say "Guelph, ON", is there a way to remove the other "options" listed above? So staff will only be presented with the one we want them to use, rather than a list of odd options?

&nbsp;

Thanks everyone, hope you're all having a good Holiday season!

&nbsp;

Tom

&nbsp;

asked 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_d75e044262d64083b00c87b864993203.jpg"/> tbaxter@guelphpl.ca

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 3 years ago

<a id="upVoteAns_1040"></a>[](#)

0

<a id="downVoteAns_1040"></a>[](#)

Answer Accepted

You should be able to remove and update the invalid Postal Codes/Cities by going into Administration>System>Database Tables>Postal Codes.  From there you will need to select your Country, and Province and Apply Filter.  From here you can identify and either edit the incorrect record or delete it.  When you attempt to delete it you will be prompted to select a Postal Code that you want all the Patron Records to be changed to if there are any patron records attached.

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_d75e044262d64083b00c87b864993203.jpg"/> rhelwig@flls.org

<a id="commentAns_1040"></a>[Add a Comment](#)

<a id="upVoteAns_1038"></a>[](#)

0

<a id="downVoteAns_1038"></a>[](#)

Hi Tom,

Not sure about how to batch fix the incorrect entries via SQL, but you'll want to make sure circulation staff do *not* have the Circulation permission "Patron Registration: Modify postal address fields." I would make this change right away so that more errors are not introduced to the postal codes table while you are working on cleaning it up. 

Phil

&nbsp;

answered 3 years ago by ![pgunderson@sandiego.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_pgunderson@sandiego.gov20210505173747.jpg) pgunderson@sandiego.gov

- <a id="acceptAnswer_1038"></a>[Accept as answer](#)

<a id="commentAns_1038"></a>[Add a Comment](#)

<a id="upVoteAns_1037"></a>[](#)

0

<a id="downVoteAns_1037"></a>[](#)

A visual!

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_d75e044262d64083b00c87b864993203.jpg"/> tbaxter@guelphpl.ca

- <a id="acceptAnswer_1037"></a>[Accept as answer](#)

- [pol.JPG](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/904?fileName=904-1037_pol.JPG "pol.JPG")

<a id="commentAns_1037"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [T-Mobile patrons not receiving text notifications](https://iii.rightanswers.com/portal/controller/view/discussion/982?)

### Related Solutions

- [Search patron reading history within the staff client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930708077324)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)