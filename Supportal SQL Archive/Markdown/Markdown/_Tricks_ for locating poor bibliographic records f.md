"Tricks" for locating poor bibliographic records for merging? Cataloging cleanup question - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# "Tricks" for locating poor bibliographic records for merging? Cataloging cleanup question

0

46 views

I've recently been tasked with a large scale cataloging cleanup job. My library district is a loose consortium of 20 public libraries that share Polaris, and share a common catalog. As things go there, the catalog needs some cleanup and I am looking for community support and ideas on how to locate low-quality bibliographic records that need merged or updated.

I am starting to brainstorm some definitions as to what qualifies as a "bad bibliographic record," such as an empty 100 field or something similar. I am not an experienced cataloger so please forgive me if this is a bad definition, but that's what I'm here for. A good working definition of a low-quality bib record would go a long way.

Secondly, how do I go about locating these records? Out of the millions of records we have, I am thinking SQL and record sets will play a large role here. 

Thanks for your help, we have a great community here.

asked 3 months ago  by <img width="18" height="18" src="../../_resources/8d2d79d1c5854141a295dee18035359b.jpg"/>  bret@washlibs.org

[catalog cleanup](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=424#t=commResults) [cataloging](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=4#t=commResults) [cleanup](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=45#t=commResults) [database cleanup](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=425#t=commResults) [sql bibliographic](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=393#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 3 months ago

<a id="upVoteAns_1116"></a>[](#)

1

<a id="downVoteAns_1116"></a>[](#)

### I'll be very interested to see other replies to this thread as we are always looking for catalog cleanup ideas.

### We've done a few global cleanups and have a few ongoing. We are also in a consortium. 

### Knowing that our record quality standards have changed over time, we looked for the oldest records in our database and had staff at our member libraries help review which ones needed to be replaced with updated records from OCLC. Depending on your database, that could be accomplished doing a basic find tool search, or SimplyReports if you have that. 

### Another project focused on looking for low encoding level records and finding more complete replacements, which we did with SQL:

### SELECT BibliographicRecordID AS recordid FROM BibliographicRecords (nolock) WHERE MARCBibEncodingLevel LIKE '3' AND RecordStatusID = 1

### My co-worker is our resident SQL genius and she just drafted this SQL search to help us find DVD records lacking 700 fields (since our patrons like to search for movies by actor names). We haven't test driven this one yet. 

select distinct br.BibliographicRecordID

from Polaris.polaris.BibliographicRecords br

where br.RecordStatusID = 1

and br.MARCBibEncodingLevel != '5'

and br.PrimaryMARCTOMID in (33, 40, 45, 49)

       and NOT exists (

              select 1

              from polaris.polaris.BibliographicTags bt

              join polaris.polaris.BibliographicSubfields bs

                     on bs.BibliographicTagID = bt.BibliographicTagID

              where bt.BibliographicRecordID = br.BibliographicRecordID

                     and (bt.TagNumber = 700

                     OR bt.TagNumber = 710

                     OR bt.TagNumber = 730

                     OR bt.TagNumber = 740)

)

       and NOT exists (

              select 1

              from polaris.polaris.BibliographicTags bt

              join polaris.polaris.BibliographicSubfields bs

                     on bs.BibliographicTagID = bt.BibliographicTagID

              where bt.BibliographicRecordID = br.BibliographicRecordID

                     and bt.TagNumber = 245

                     and bs.Subfield = 'a'

                     and (bs.Data LIKE '%ILL%'

                     OR bs.Data LIKE '%binge box%')

)

### GROUP BY BR.BibliographicRecordID  HAVING COUNT(BR.BibliographicRecordID) < 2

### Good luck! Hope some of this is helpful!

**Amy Mihelich**

Cataloging Librarian, Washington County Cooperative Library Services ([WCCLS](https://www.wccls.org/))

111 NE Lincoln Street, MS 58, Suite 230 | Hillsboro, Oregon 97124

answered 3 months ago  by <img width="18" height="18" src="../../_resources/8d2d79d1c5854141a295dee18035359b.jpg"/>  amihelich@wccls.org

- <a id="acceptAnswer_1116"></a>[Accept as answer](#)

<a id="commentAns_1116"></a>[Add a Comment](#)

<a id="upVoteAns_1117"></a>[](#)

0

<a id="downVoteAns_1117"></a>[](#)

That is very helpful, thank you Amy! I was able to locate a large number of records modifying these scripts.

I'm leaving this open to hopefully continue the discussion about cleanup and cleanup ideas :)

If you have anything to share about your catalog and database cleanup process please let us know!

answered 3 months ago  by <img width="18" height="18" src="../../_resources/8d2d79d1c5854141a295dee18035359b.jpg"/>  bret@washlibs.org

- <a id="acceptAnswer_1117"></a>[Accept as answer](#)

<a id="commentAns_1117"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)