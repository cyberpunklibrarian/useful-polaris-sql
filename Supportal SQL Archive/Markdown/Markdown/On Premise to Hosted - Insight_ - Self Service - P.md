[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Additional Products (e.g. Community Profiles, ExpressCheck, Fusion)](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=54)

# On Premise to Hosted - Insight?

0

108 views

Sno-Isle Libraries is interested in moving to a III hosted environment. We are looking for other library systems that have gone from an on premise to hosted environment in the past in order to get lessons learned information. What went well? What did you wish you knew before you started?

Thanks for any insight you can provide!

asked 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_927c88a2fc024d5c84c0642e51e218d9.jpg"/> cgabehart@sno-isle.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 6 years ago

<a id="upVoteAns_347"></a>[](#)

0

<a id="downVoteAns_347"></a>[](#)

We have NOT made the switch, but since you haven't gotten any responses, here are the types of questions, I'd be asking. No doubt many of them have already crossed your mind, but just in case:

1.  Check the latency/jitter, etc. between your various networks and where your actual hosting location would be. Ideally check throughout the day. Compare this with your current values, see if there is some way to "slow down" your current processes (using a cellular modem?) to match what you'd be getting through the hosting enviornment to see how it works?
2.  Check your receipt printers with the hosting enviornment. Printing is typically ugly anyhow and I can't imagine moving to hosting makes it much better.
3.  Are you already using RDP / terminal server bases sessions? If not, you may want to test that process and see what staff training might be required.
4.  Ask for details about how backups are handled and how frequently test restores are performed.
5.  Depending on your volume of transasactions, check for any long running single threaded jobs, like notice sending. Depending on the hosted hardware, the timing of some of those single threaded jobs might be slowed down versus what you're running on prem. Check for details about the single threaded clock speed in the hosted enviornment and compare that to your current hardware.
6.  Make a detailed list of any customization you've done outside SA and check to make sure that it will still work.
7.  Are you used to being able to access SQL directly? If so, you'll want to make sure that is still possible with your hosting plan.
8.  Do you have any custom SQL jobs or tables? Check to see if you'll be able to continue using those.
9.  What will your upgrade process be like? How frequently can you request an upgrade? A few months ago I was investigating a Polaris issue and I was stunned by how many hosted customers (PAC URL included Polaris library) were several version behind the currently version?
10. Do any of your vendors use IP addresses for authentication services like 3M SIP? Make sure you'll know what your new 3M SIP ip addresses will be an have your list of vendors that need updated information ready.
11. Are you able to keep all your public facing services like the PAC under your own domain name? Or do they require you to be a subdomain under their domain?
12. Do they include TLS certificates from Let's Encrypt? If you had an EV cert that you wanted to apply to your services would you be able to do that?
13. How are usernames and passwords handled? Do they use your local AD domain for that? Do they use SSO or SAML? Or do you have to double enter usernames and passwords? If you have to double enter, what type of password policies do they have in place?
14. Do you use telephony? If so, ask how the calls will be made and what phone numbers patrons will see.
15. What is III's standpoint on keeping up with security vulerablities? For instances are they actively working to disable TLS 1.0? Do they run the reporting services server and SimplyReports across TLS/HTTPS connections?
16. If you're sharing the hosted hardware with another library or AWS customer, what preventive measures are in place for side channel attacks like Meltdown and Spectre?
17. Do you use exportexpress in SimplyReports? If so do you build scripts to FTP those files somewhere? You might need to see if you'll still be able to do this.

Those are a few things that come to mind off the top of my head, hopefully others will be able to chime in with "real-world" examples.

Here is the perspective from a Sierra library: https://www.ohiug.org/uploads/6/1/3/5/61351715/2017_hosted.pdf Not everything would apply to Polaris, but there still might be some helpful information.

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_927c88a2fc024d5c84c0642e51e218d9.jpg"/> wosborn@clcohio.org

- <a id="acceptAnswer_347"></a>[Accept as answer](#)

Thank you, Wes! Some of these questions I already have answers to, but others I do not and we can bring them up to III and the references we are speaking with. Any other information folks have would be greatly appreciated!

— cgabehart@sno-isle.org 6 years ago

<a id="commentAns_347"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Related Solutions

- [Staff receiving "Your workstation is not registered" message when using the Polaris Export Utility](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181025085708870)
- [Disconnecting from Polaris terminal server](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930905890354)
- [How to configure the remote desktop connection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930561459679)
- [Polaris Integration with EContent Guide](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170419085906750)
- [Unable to access file from OCLC from the Polaris Virtual Cloud](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930986996441)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)