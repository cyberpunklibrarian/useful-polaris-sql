[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL for patrons with no Zip Code

0

42 views

Does anyone have a SQL to find patrons with no Zip/Postal code?

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_c7ad55ffaee04cb29f1a721e0ab60ab4.jpg"/> musack@bcls.lib.nj.us

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_1387"></a>[](#)

0

<a id="downVoteAns_1387"></a>[](#)

Thank you!! I'll give it a try :)

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_c7ad55ffaee04cb29f1a721e0ab60ab4.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_1387"></a>[Accept as answer](#)

<a id="commentAns_1387"></a>[Add a Comment](#)

<a id="upVoteAns_1386"></a>[](#)

0

<a id="downVoteAns_1386"></a>[](#)

SELECT DISTINCT p.Barcode,pr.PatronFullName

FROM Polaris.Polaris.PatronRegistration pr (nolock)

LEFT JOIN Polaris.Polaris.Patrons p (nolock) ON pr.PatronID=p.PatronID

LEFT JOIN Polaris.Polaris.PatronAddresses pa (nolock) ON pr.PatronID=pa.PatronID

LEFT JOIN Polaris.Polaris.Addresses a (nolock) ON pa.AddressID=a.AddressID

LEFT JOIN Polaris.Polaris.PostalCodes pc (Nolock) ON a.PostalCodeID=pc.PostalCodeID

WHERE  
pc.PostalCode IS NULL

&nbsp;

Try this. 

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_c7ad55ffaee04cb29f1a721e0ab60ab4.jpg"/> lmartinez@cityofirving.org

- <a id="acceptAnswer_1386"></a>[Accept as answer](#)

<a id="commentAns_1386"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Patron Code "Staff "override](https://iii.rightanswers.com/portal/controller/view/discussion/704?)
- [Does anyone have a SQL report that will show renewal vs new cards for a particular patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/1377?)
- [Does anyone know how to batch update the patron code field?](https://iii.rightanswers.com/portal/controller/view/discussion/886?)
- [change loan period on material by patron code](https://iii.rightanswers.com/portal/controller/view/discussion/163?)
- [SQL for Patron Name Titles](https://iii.rightanswers.com/portal/controller/view/discussion/836?)

### Related Solutions

- [Incorrect postal codes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930359078195)
- [What happens when we delete a postal code that's in use?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930308320846)
- [Patron Code for Computer / Internet Use](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930652657234)
- [Can I delete a patron code from the Patron Codes Policy Table?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930483335271)
- [Wrong default postal code](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180316120042002)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)