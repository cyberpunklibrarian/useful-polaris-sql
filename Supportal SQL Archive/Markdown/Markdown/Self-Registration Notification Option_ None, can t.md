[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [PAC](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=14)

# Self-Registration Notification Option: None, can this be hidden?

0

54 views

None is not an option in the Polaris Sys Admin Parameters that we can choose to enable/disable, but it appears on the self-registration form. I'd like to hide this option because I have not found a way to find these patrons & ultimately we need to be able to notify them of holds/bills etc. As for finding these patrons in the system, does anyone know what an SQL search would be to find patrons with NULL or "None" as their notification setting? 

asked 11 months ago by <img width="18" height="18" src="../_resources/default-avatar_cc39df7604034dc2ba729ed61df4b337.jpg"/> dreynolds@cityofcamas.us

[notification setting](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=302#t=commResults) [pac registration](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=488#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 11 months ago

<a id="upVoteAns_1405"></a>[](#)

1

<a id="downVoteAns_1405"></a>[](#)

Answer Accepted

```
We run the following SQL in the staff client (F7) to get a list of accounts with the notification option set to none:

select patronid from patronregistration pr (nolock)
where DeliveryOptionID is NULL

Hope that helps!
```

answered 11 months ago by <img width="18" height="18" src="../_resources/default-avatar_cc39df7604034dc2ba729ed61df4b337.jpg"/> musack@bcls.lib.nj.us

<a id="commentAns_1405"></a>[Add a Comment](#)

<a id="upVoteAns_1407"></a>[](#)

0

<a id="downVoteAns_1407"></a>[](#)

If you want to remove the option from the self-registration form, you could ask your Innovative support person to add custom css code in the attached image.  For some reason, I couldn't add it as code in this field.

answered 11 months ago by <img width="18" height="18" src="../_resources/default-avatar_cc39df7604034dc2ba729ed61df4b337.jpg"/> blashbrook@dcplibrary.org

- <a id="acceptAnswer_1407"></a>[Accept as answer](#)

- [2023-09-22_10-28-33.png](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/1351?fileName=1351-1407_2023-09-22_10-28-33.png "2023-09-22_10-28-33.png")

<a id="commentAns_1407"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Customizing PAC self-registration?](https://iii.rightanswers.com/portal/controller/view/discussion/1140?)
- [Does anyone know how to set up automatic "welcome emails" when someone registers for a new library card via the PAC?](https://iii.rightanswers.com/portal/controller/view/discussion/1157?)
- [Book Jackets in Patron Account](https://iii.rightanswers.com/portal/controller/view/discussion/643?)
- [\[How to fix\] 5.5+ PAC no longer returns to previous position in search results after placing hold](https://iii.rightanswers.com/portal/controller/view/discussion/161?)
- [Random search](https://iii.rightanswers.com/portal/controller/view/discussion/1233?)

### Related Solutions

- [Removing notification options in the PAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930952432121)
- [What happens if a patron has fax for their notification option?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930749899053)
- [Setting up phone notification with additional TXT messages](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930542970896)
- [Would like a way to gather all patrons with no notification option selected](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930285364780)
- [Online Registration Defaults](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930782646108)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)