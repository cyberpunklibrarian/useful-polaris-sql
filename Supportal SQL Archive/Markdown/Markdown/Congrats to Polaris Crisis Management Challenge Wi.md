[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [General Announcements](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=55)

# Congrats to Polaris Crisis Management Challenge Winners

0

35 views

Hello Innovative Users!  
<br/>Pairwise has concluded for the Polaris Crisis Management challenge and we have our winners!  
<br/>Designed to focus specifically on ways the system can improve handling of large-scale, long-term closures, this challenge garnered 24 ideas and received 611 votes!  
<br/>**The Winners**  
<br/>**Submission**: [Ability to bulk change due dates](https://idealab.iii.com/polaris3acrisismanag/Page/ViewIdea?ideaid=15544)  
<br/>**Inspired by**: Idea submitted by Debra Wischmeyer  
<br/>**Proposed for**: Polaris 6.8, H1 2020  
<br/><br/>**Submission**: [Bulk add to dates closed policy table](https://idealab.iii.com/polaris3acrisismanag/Page/ViewIdea?ideaid=15469)  
<br/>**Inspired by**: Idea submitted by Cecilia Smiley  
<br/>**Proposed for**: Polaris 6.8, H1 2020  
<br/>“We’ve received a lot of requests recently for bulk changing dates,” said Samantha Quell, Polaris Product Owner. “I am very glad these ideas ranked so highly in the pairwise challenge. With the implementation of these features, we can make it easier for staff to make these bulk date changes without requiring the use of SQL scripts. “  
Remember, submissions live on in Idea Lab and our Always Open Space never closes. If you submitted an enhancement that is still important to you or you’ve just discovered a development opportunity, the Always Open space provides a place to post that idea for any Polaris product.  
<br/>Visit the Innovative Idea Lab: [](https://idealab.iii.com/)[](https://idealab.iii.com/)[](https://idealab.iii.com/)[](https://idealab.iii.com/)https://idealab.iii.com/  
<br/>**Thanks again to our wonderful Idea Lab Leadership team**  
Jeremy Goldstein, Minuteman Library Network  
Cecilia Smiley, Lee County Library System  
Maisam Nouh, The Ferguson Library  
Kathy Setter, IFLS Library System  
<br/>**And special thanks to your moderators and expert reviewers:**  
Jamie King  
Debra Wischmeyer  
Marian McCollum  
Erin Shield  
Tracey Thompson  
<br/>**What is Idea Lab?**  
<br/>Idea Lab is the website hub for our customer-driven innovation process that informs Innovative product roadmaps. To participate, simply go to [Idea Lab](https://idealab.iii.com/main/User/Login#/login), and enter in your username and password. If you don’t have an account yet, it’s not too late – simply follow the instructions on the [Idea Lab FAQ](https://www.innovativeusers.org/enhancement/idea-lab-faq) and join in!  
<br/>Please spread the word to colleagues at your own institutions and beyond about joining Idea Lab. At Innovative, we are committed to user-driven innovation that will benefit all our library partners, and we look forward to the next Challenge with you.  
<br/>**We are Recruiting for Idea Lab**  
<br/>Don’t forget that the Idea Lab team is recruiting for the 2020-2022 term. So if you love the site and would like to contribute to it in a greater capacity, consider joining the team. If you would like to join the Idea Lab Team for 2020-2022, please complete the survey at: https://forms.gle/rdZgaN2HfbAiQCVi7  
<br/>The 2020-21 Enhancements co-coordinators will be reviewing applications and contacting team members in late May. **Deadline to submit an application is June 5, 2020**. Those selected for the team will be contacted shortly thereafter.  
<br/>jgoldstein@minlib.net

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_7739fcbebb86424d81edf129c2e471de.jpg"/> csmiley@leegov.com

[challenge](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=337#t=commResults) [crisis management](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=338#t=commResults) [idealab](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=52#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris - Crisis Management](https://iii.rightanswers.com/portal/controller/view/discussion/731?)
- [Polaris Crisis Management Challenge](https://iii.rightanswers.com/portal/controller/view/discussion/740?)
- [Crisis Management Challenge](https://iii.rightanswers.com/portal/controller/view/discussion/734?)
- [Crisis Management Challenge](https://iii.rightanswers.com/portal/controller/view/discussion/737?)
- [Idea Lab Undo challenge winner and new Polaris LEAP challenge!](https://iii.rightanswers.com/portal/controller/view/discussion/367?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Polaris Inventory Manager Guide 4.1R2](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160504101532134)
- [Wireless Access Manager (WAM) Guide](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160504111853160)
- [Polaris Inventory Manager Installation 4.1R2](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160504101956110)
- [Ingram SFTP Transmission](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240517101145090)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)