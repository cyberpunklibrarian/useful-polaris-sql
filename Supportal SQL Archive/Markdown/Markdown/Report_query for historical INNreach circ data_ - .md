Report/query for historical INNreach circ data? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Report/query for historical INNreach circ data?

0

10 views

Hi all,

I am hoping someone has had a similar request and come up with a way to find this data. We are looking to get historical INNreach circ activity for certain collections so that we can compare it against non-INNreach circ activity. Simply Reports can give me a list of titles/items that were checked out via INNreach, and items that are currently out via INNreach, but I see no way to get a report together that will break out lifetime INNreach vs non-INNreach circ activity.

Does anyone have any ideas?

Thanks!

asked 21 days ago  by <img width="18" height="18" src="../../_resources/63abb168fea6417eb20190abfa88e9d0.jpg"/>  josh.rouan@baldwinlib.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 day ago

<a id="upVoteAns_1169"></a>[](#)

0

<a id="downVoteAns_1169"></a>[](#)

I think this should give you a decent starting point, assuming there aren't quirks to the INNReach process that I'm not aware of. You'll need to change the INNReach patron code id variable from 20 to whatever yours is and adjust the dates for whatever time period you want.

declare @beginDate date = '1/1/2021'
declare @endDate date = '2/1/2021'
declare @innreachPatronCodeId int = 20

select o.Name \[Branch\]
,isnull(c.Name, 'None') \[Collection\]
,count(case when td_pc.numValue = @innreachPatronCodeId then 1 end) \[INNReach Circs\]
,count(case when td_pc.numValue != @innreachPatronCodeId then 1 end) \[Local Circs\]
,count(th.TransactionID) \[Total Circs\]
from PolarisTransactions.Polaris.TransactionHeaders th
join PolarisTransactions.polaris.TransactionDetails td_pc
on td\_pc.TransactionID = th.TransactionID and td\_pc.TransactionSubTypeID = 7
left join PolarisTransactions.Polaris.TransactionDetails td_r
on td\_r.TransactionID = th.TransactionID and td\_r.TransactionSubTypeID = 124
left join PolarisTransactions.Polaris.TransactionDetails td_c
on td\_c.TransactionID = th.TransactionID and td\_c.TransactionSubTypeID = 61
left join Polaris.Polaris.Collections c
on c.CollectionID = td_c.numValue
join Polaris.Polaris.Organizations o
on o.OrganizationID = th.OrganizationID
where th.TransactionTypeID = 6001
and th.TranClientDate between @beginDate and @endDate
and td_r.TransactionID is null
group by o.Name, isnull(c.Name, 'None')
order by o.Name, \[Collection\]

answered 1 day ago  by <img width="18" height="18" src="../../_resources/63abb168fea6417eb20190abfa88e9d0.jpg"/>  mfields@clcohio.org

- <a id="acceptAnswer_1169"></a>[Accept as answer](#)

<a id="commentAns_1169"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [CollectionHQ Transaction and Holds Data Extracts](https://iii.rightanswers.com/portal/controller/view/discussion/358?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)
- [Circ by material type and time frame](https://iii.rightanswers.com/portal/controller/view/discussion/85?)
- [Has anyone developed a SimplyReport/SQL Report to answer PLA's Public Library Data Service circulation questions?](https://iii.rightanswers.com/portal/controller/view/discussion/439?)
- [SQL or report for circ stats by Patron Statistical Class?](https://iii.rightanswers.com/portal/controller/view/discussion/488?)

### Related Solutions

- [InnReach 3.0 Release Notes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160630151112681)
- [Resource Sharing 3.1 Release Notes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180507101815447)
- [Resource Sharing 3.2 Release Notes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=190422153038084)
- [Resource Sharing 3.3 Release Notes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200210113218915)
- [Release Announcements and Notes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160629163705283)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)