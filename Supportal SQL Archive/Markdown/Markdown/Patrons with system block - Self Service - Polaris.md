Patrons with system block - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Patrons with system block

0

223 views

I just imported student records into Polaris, and some of them are showing a system block of "Verify patron data - offline registration". Not all of them have that block, however, and I'm not sure how some are blocked and others aren't. Is there a way to bulk change and removed that particular system block from patrons in a record set? Or is there an SQL I can run to do so? I'd rather not have to go into 500+ records individually to remove the block :-)

asked 3 years ago  by <img width="18" height="18" src="../../_resources/a51ef68bdcc84939b4b25c5a5fd7f679.jpg"/>  musack@bcls.lib.nj.us

[bulk change](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=77#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults) [system blocks](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=145#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 3 years ago

<a id="upVoteAns_381"></a>[](#)

0

<a id="downVoteAns_381"></a>[](#)

Thank you so much!! I'll give it a try

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a51ef68bdcc84939b4b25c5a5fd7f679.jpg"/>  musack@bcls.lib.nj.us

- <a id="acceptAnswer_381"></a>[Accept as answer](#)

<a id="commentAns_381"></a>[Add a Comment](#)

<a id="upVoteAns_380"></a>[](#)

0

<a id="downVoteAns_380"></a>[](#)

Hi,

I gather the newly imported students into a record set using the creation date and branch.  Then I ask my site manager to remove the block.  As I understand it, the SQL used is...

select SystemBlocks from Patrons (nolock) where PatronID in (select PatronID from PatronRecordSets where RecordSetID = XXXXX) –update with your record set ID

Kevin French
Systems Librarian
GMILCS, Inc.
31 Mount Saint Mary's Way
Hooksett, NH 03106-4400

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a51ef68bdcc84939b4b25c5a5fd7f679.jpg"/>  kfrench@gmilcs.org

- <a id="acceptAnswer_380"></a>[Accept as answer](#)

<a id="commentAns_380"></a>[Add a Comment](#)

<a id="upVoteAns_379"></a>[](#)

0

<a id="downVoteAns_379"></a>[](#)

You shoudl be able to talk to your site manager (after putting the records in a record set) and have them remove the block.  I think I remember doing this when I imported patrons many years ago.

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a51ef68bdcc84939b4b25c5a5fd7f679.jpg"/>  trevor.diamond@mainlib.org

- <a id="acceptAnswer_379"></a>[Accept as answer](#)

<a id="commentAns_379"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Is there a way to put a birth date block on all patron cards?](https://iii.rightanswers.com/portal/controller/view/discussion/370?)
- [Is there a way to block patrons from putting hold on a material type if they have that material type currently checked out?](https://iii.rightanswers.com/portal/controller/view/discussion/535?)
- [We are rolling out hotspots for our patrons. We have created the records for the Hotspots. How can we restrict checkouts... we want no renewals... and you have to wait 48 hours prior to checking out the device again. Does anyone have any advice ?](https://iii.rightanswers.com/portal/controller/view/discussion/596?)
- [Patron expiration date update](https://iii.rightanswers.com/portal/controller/view/discussion/727?)
- [Patron Identification removal](https://iii.rightanswers.com/portal/controller/view/discussion/180?)

### Related Solutions

- [Expired Patron Blocks Not Appearing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930808100621)
- [Does blocking in SIP follow the "patron initiated circulation" settings?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930374822804)
- [System-assigned blocks in the Patrons table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930929206259)
- [Old patron notes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181105154206729)
- [Blocks for expired registration and address checks](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930502649491)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)