[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Leap](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=13)

# Running an SQL in Leap

0

82 views

Originally posted by carl.ratz@phoenix.gov on Friday, June 16th 15:23:21 EDT 2017

Will we still be able to run SQL queries in Leap like we can in the find tool of the Client?

asked 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_984405f3b64f4b72a974821f3b835e55.jpg"/> support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 6 years ago

<a id="upVoteAns_346"></a>[](#)

1

<a id="downVoteAns_346"></a>[](#)

The files attached to this post are what the interface looks like for the Find Tool SQL portion.

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_984405f3b64f4b72a974821f3b835e55.jpg"/> wosborn@clcohio.org

- <a id="acceptAnswer_346"></a>[Accept as answer](#)

- [2018-08-10 10_37_12-Polaris Leap - Circulation.png](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/47?fileName=47-346_2018-08-10+10_37_12-Polaris+Leap+-+Circulation.png "2018-08-10 10_37_12-Polaris Leap - Circulation.png")
- [2018-08-10 10_37_29-Polaris Leap - Circulation.png](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/47?fileName=47-346_2018-08-10+10_37_29-Polaris+Leap+-+Circulation.png "2018-08-10 10_37_29-Polaris Leap - Circulation.png")

<a id="commentAns_346"></a>[Add a Comment](#)

<a id="upVoteAns_342"></a>[](#)

0

<a id="downVoteAns_342"></a>[](#)

Hey Carl-

&nbsp;

Yes!  You can run SQL queries in the Find Tool in Leap.

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_984405f3b64f4b72a974821f3b835e55.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_342"></a>[Accept as answer](#)

<a id="commentAns_342"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [LEAP for Dummies or LEAP 101](https://iii.rightanswers.com/portal/controller/view/discussion/911?)
- [Leap Offline Procedures](https://iii.rightanswers.com/portal/controller/view/discussion/635?)
- [Leap Training](https://iii.rightanswers.com/portal/controller/view/discussion/264?)
- [Leap and RFID](https://iii.rightanswers.com/portal/controller/view/discussion/427?)
- [Payments in Leap](https://iii.rightanswers.com/portal/controller/view/discussion/50?)

### Related Solutions

- [Adding Leap permissions en mass](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930306603411)
- [What is required for Leap?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930646331533)
- [Leap resource guide](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210217105146847)
- [LEAP and Multi-Factor Authentication](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=231205134723017)
- [Minimum screen resolution for use with Leap](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=190319120402566)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)