Pre-box Cataloging - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Pre-box Cataloging

0

37 views

We are looking at ways to do more cataloging remotely.  As we have recently completed a Lean project one of the things we are looking at is switching from predicting call numbers at the point order to pre-box cataloging titles before they arrive.  We utlize EDI Invoices and ASN recvieding so I'm a little stuck on how we figure out what the vendors have shipped us in a reliable fashion.  The Advanced Shipment Containers Not Yet Recieved is one report that could be helpful as is the Electronic Invoice report, but both are a little clunky.  Has anyone conquered this and be willing to share your process? 

Thanks,

Heather

asked 1 year ago  by <img width="18" height="18" src="../../_resources/70f7a544a09342309cb4c50b1a406e9b.jpg"/>  hkaufman@piercecountylibrary.org

[cataloging](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=4#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_942"></a>[](#)

0

<a id="downVoteAns_942"></a>[](#)

Answer Accepted

Hi Again, Heather.

I realized that the query I just posted can tell you how many items to expect, but not what they are. >sheepish grin<

Here's a tweak to the query that will give you the BibliographicRecordID, Title and number of copies coming soon. The same variation for a material type criterion should work, but make sure to include the new group by criteria. The order by can be rearranged as you like.

Hope this is more helpful.

JT

SELECT
sup.SupplierName
, br.BibliographicRecordID
, br.BrowseTitle
, COUNT(it.ItemRecordID) as NumberOfItems

FROM Polaris.Polaris.ItemRecords it WITH (NOLOCK)
JOIN Polaris.Polaris.Collections col
ON (it.AssignedCollectionID = col.CollectionID)
JOIN Polaris.Polaris.BibliographicRecords br (NOLOCK)
ON (it.AssociatedBibRecordID = br.BibliographicRecordID)
JOIN Polaris.Polaris.RwriterLineItemToItemsView l2i (NOLOCK)
ON (it.ItemRecordID = l2i.ItemRecordID)
JOIN Polaris.Polaris.POLines pol (NOLOCK)
ON (l2i.POLineItemID = pol.POLineItemID)
JOIN Polaris.Polaris.PurchaseOrders po (NOLOCK)
ON (pol.PurchaseOrderID = po.PurchaseOrderID)
JOIN Polaris.Polaris.Suppliers sup (NOLOCK)
ON (po.SupplierID = sup.SupplierID)

WHERE it.itemstatusid = 15
AND CAST(it.ItemStatusDate as Date) = CAST(GetDate() as Date)
AND (sup.SupplierName like 'Baker %' OR sup.SupplierName like 'Ingram%')

GROUP BY sup.SupplierName, br.BibliographicRecordID, br.BrowseTitle
ORDER BY sup.SupplierName, br.BibliographicRecordID, br.BrowseTitle

answered 1 year ago  by <img width="18" height="18" src="../../_resources/70f7a544a09342309cb4c50b1a406e9b.jpg"/>  jwhitfield@aclib.us

Hi JT,

Thanks for the search.   I realized that I didn't specify we wanted to be able to catalog these titles before they arrive in the building but after they have been shipped.  So they would still be in On-Order status when I need to identify them.  Have  you had any luck finding titles likes this?

Thanks,

Heather

— hkaufman@piercecountylibrary.org 1 year ago

Hi Heather. At our library, the status of the items changes when we receive an EDI invoice. I think I recall that this is an option.

Here is a tweak to the query that uses the invoice date instead of the item status:

IMPORTANT: Supportal comments do not accept percent signs. Please replace {pct} below with the appropriate symbol.

SELECT
sup.SupplierName
, br.BibliographicRecordID
, br.BrowseTitle
, COUNT(it.ItemRecordID) as NumberOfItems

FROM Polaris.Polaris.ItemRecords it WITH (NOLOCK)
JOIN Polaris.Polaris.Collections col
ON (it.AssignedCollectionID = col.CollectionID)
JOIN Polaris.Polaris.BibliographicRecords br (NOLOCK)
ON (it.AssociatedBibRecordID = br.BibliographicRecordID)
JOIN Polaris.Polaris.RwriterLineItemToItemsView l2i (NOLOCK)
ON (it.ItemRecordID = l2i.ItemRecordID)
JOIN Polaris.Polaris.InvLines inl (NOLOCK)
ON (l2i.POLineItemID = inl.InvLineItemID)
JOIN Polaris.Polaris.Invoices inv (NOLOCK)
ON (inl.InvoiceID = inv.InvoiceID)
JOIN Polaris.Polaris.Suppliers sup (NOLOCK)
ON (inv.SupplierID = sup.SupplierID)

WHERE CAST(inv.InvDate as Date) = CAST(GetDate() as Date)
AND (sup.SupplierName like 'Baker{pct}' OR sup.SupplierName like 'Ingram{pct}')

GROUP BY sup.SupplierName, br.BibliographicRecordID, br.BrowseTitle
ORDER BY sup.SupplierName, br.BibliographicRecordID, br.BrowseTitle

Hope this helps.

JT

— jwhitfield@aclib.us 1 year ago

<a id="commentAns_942"></a>[Add a Comment](#)

<a id="upVoteAns_941"></a>[](#)

0

<a id="downVoteAns_941"></a>[](#)

Hi Heather.

I created a report (query below) that looks for items that have gone from On-Order to In Processing (what happens on our end when an EDI invoice is received/processe by Polaris). It is scheduled to run each morning from Mon-Sat at around 8:30 AM. It also includes an estimate of how many boxes to expect so that the people who receive shipments can have an idea how busy they will be in a couple of days.

We use this for our primary book vendors, but you can modify the SupplierName conditions to be whatever you want. You could also change the criteria to be by item material type. I'll include a snippet below the query with the WHERE syntax for that option. The date criterion allows comparision of the date only, without the time part of the ItemStatusDate.

SELECT
sup.SupplierName
, COUNT(it.ItemRecordID) as NumberOfItems
, (COUNT(it.ItemRecordID) / 20.0) as NumberOfBoxes

FROM Polaris.Polaris.ItemRecords it WITH (NOLOCK)
JOIN Polaris.Polaris.Collections col
ON (it.AssignedCollectionID = col.CollectionID)
JOIN Polaris.Polaris.RwriterLineItemToItemsView l2i (NOLOCK)
ON (it.ItemRecordID = l2i.ItemRecordID)
JOIN Polaris.Polaris.POLines pol (NOLOCK)
ON (l2i.POLineItemID = pol.POLineItemID)
JOIN Polaris.Polaris.PurchaseOrders po (NOLOCK)
ON (pol.PurchaseOrderID = po.PurchaseOrderID)
JOIN Polaris.Polaris.Suppliers sup (NOLOCK)
ON (po.SupplierID = sup.SupplierID)

WHERE it.itemstatusid = 15
AND CAST(it.ItemStatusDate as Date) = CAST(GetDate() as Date)
AND (sup.SupplierName like 'Baker %' OR sup.SupplierName like 'Ingram%')

GROUP BY sup.SupplierName
ORDER BY sup.SupplierName

-------For limiting by material type:

WHERE it.itemstatusid = 15
AND CAST(it.ItemStatusDate as Date) = CAST(GetDate() as Date)
AND it.MaterialTypeID IN (4,25) --Whatever your material types are

GROUP BY sup.SupplierName
ORDER BY sup.SupplierName

Hope this helps,

JT

answered 1 year ago  by <img width="18" height="18" src="../../_resources/70f7a544a09342309cb4c50b1a406e9b.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_941"></a>[Accept as answer](#)

<a id="commentAns_941"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Accession Numbers](https://iii.rightanswers.com/portal/controller/view/discussion/1029?)
- ["Tricks" for locating poor bibliographic records for merging? Cataloging cleanup question](https://iii.rightanswers.com/portal/controller/view/discussion/1005?)
- [How do you enter/search for titles with special characters?](https://iii.rightanswers.com/portal/controller/view/discussion/320?)
- [Un-integrating OverDrive records?](https://iii.rightanswers.com/portal/controller/view/discussion/992?)
- [Looking for another Polaris library that has added Spanish initial articles to the Initial Articles Table in SA.](https://iii.rightanswers.com/portal/controller/view/discussion/906?)

### Related Solutions

- [Catalog Extract Error Message](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930674953830)
- [How do I remove item records with the status of Deleted?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930423208345)
- [Label Manager settings not saving](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930916508464)
- [OCLC Z39.50 Targets](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181128103954612)
- [Item record partially deleted](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930251623954)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)