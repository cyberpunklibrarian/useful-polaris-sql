[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# SQL search for item records with Loan Period of 0 days--Help!

0

52 views

Can anyone whip up a quick SQL search for item records with Loan Period of 0 days? I'd be ever so grateful!

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_9e934781625e462481266404870d5585.jpg"/> elinacre@altoona-iowa.com

[item records](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=190#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_1199"></a>[](#)

0

<a id="downVoteAns_1199"></a>[](#)

Answer Accepted

We don't have any loan period of 0 days (though there is one called "Non-circulating"), so I suspect that the codes and descriptions must vary by organizations.

For us, it'd be 

SELECT \*  
FROM CircItemRecords with (NOLOCK)  
WHERE LoanPeriodCodeID = 4 

for "Non-circulating" items, but that's an extremely basic search and it'd probably be different for you.  You'd have to find what the "0 days" LoanPeriodCodeID is and change the 4 to match.  (Also, if you're doing this in the client or LEAP, you'd need to change it to be an item search and the SQL to be

SELECT ItemRecordID   
FROM CircItemRecords with (NOLOCK)  
WHERE LoanPeriodCodeID = 4

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_9e934781625e462481266404870d5585.jpg"/> jjack@aclib.us

Aahh, thanks a million! You were right; I scrounged around in the Admin Explorer to find our loan period codes, and that seemed to work! Found a few oopsies, at least!

— elinacre@altoona-iowa.com 2 years ago

<a id="commentAns_1199"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for items with information in "Name of piece"](https://iii.rightanswers.com/portal/controller/view/discussion/1345?)
- [Deleting item records with holds](https://iii.rightanswers.com/portal/controller/view/discussion/32?)
- [Bib record modifiers](https://iii.rightanswers.com/portal/controller/view/discussion/1380?)
- [Delete a collection, stat code, or Shelf code from drop down list on item records](https://iii.rightanswers.com/portal/controller/view/discussion/402?)
- [Update item record data using item control number as a match point?](https://iii.rightanswers.com/portal/controller/view/discussion/1396?)

### Related Solutions

- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [What holdings information is sent when my Polaris database is a remote target?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930755803285)
- [Finding item records by PO received date](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180712134112976)
- [Item record is missing item history](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930266377481)
- [How to link multiple Authority Records to a Bibliographic Record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930318385316)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)