[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Third Party](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=56)

# Anyone using LibraryIQ?

0

19 views

Hello all,

I was curious to see if anyone was using LibraryIQ and if so, what method of data export do you use? Specifically, how did you go about creating the CSV files IF you do not use the SQL data pull.

Thanks!

Chad

asked 2 years ago by ![chad.eller@gastongov.com](https://iii.rightanswers.com/portal/app/images/custom/avatar_chad.eller@gastongov.com20210201162116.jpg) chad.eller@gastongov.com

[libraryiq](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=450#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Does your library use Kanopy?](https://iii.rightanswers.com/portal/controller/view/discussion/256?)
- [What Camera Do You Use For Patron Photos?](https://iii.rightanswers.com/portal/controller/view/discussion/122?)
- [Anyone currently (or previous experience) using Polaris with WorldCat Discovery? We're considering trying it...](https://iii.rightanswers.com/portal/controller/view/discussion/560?)
- [Is anyone using two (or more! ) different RFID vendors within a single consortium?](https://iii.rightanswers.com/portal/controller/view/discussion/1034?)
- [Anyone using the self checkout feature of the BiblioCommons app?](https://iii.rightanswers.com/portal/controller/view/discussion/1389?)

### Related Solutions

- [Does Polaris still use "stop words"?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930936911386)
- [Using Office365 to Send Polaris Email Notifications](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210821172116663)
- [What Information Should I Use for Library of Congress?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930355289656)
- [Minimum screen resolution for use with Leap](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=190319120402566)
- [Patron Code for Computer / Internet Use](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930652657234)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)