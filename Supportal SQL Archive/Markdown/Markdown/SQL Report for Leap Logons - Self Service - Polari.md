SQL Report for Leap Logons - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=13)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Leap](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=13)

# SQL Report for Leap Logons

0

117 views

Does anyone have an SQL that would show  who has logged into Leap and includes the user, date, time, etc?  I posed this question to our site manager and received the following response:

"There is a transaction for "System Logon" that can be tracked in your database. If you navigate to Administration> Explorer> System> expand "System" on left menu> Database Tables> "Transaction Logging" and scroll down to Transaction 7200 "System Logon", you can see if your database is currently logging this transaction. If "Log Transaction" is set to Yes, then this is information that can be retrieved, but there are no toolbar reports or SimplyReports functions that will show you this data, so it'd be something we'd have to get via SQL. 
If this inquiry represents a one-time data need with specific parameters, I can go ahead and pull the data for you myself and send it over in a spreadsheet. However, if it is a report you'd like to be able to run regularly or as-needed, it would be considered a custom report (which involves a fee). Essentially a custom report is something our staff would build for you, make available in the Reports and Notices Utility for ease of access, and would provide maintenance and Support to ensure it continues to function throughout future upgrades of the Polaris software. 
What do you think? Is this something you'll need into the future, or just a one-time data pull? If you would like a quote for the custom report, go ahead and return to me the attached form with your desired details and I can request the quote be sent for your consideration. Otherwise, let me know what parameters should be used and I can grab the data for you."

Our Transaction 7200 is set to yes.  We're not requesting a quote for a custom report at this time.  I wonder if this is something for the Idea Lab.

Thanks in advance,

Anne Krulik

Burlington County Library System

asked 2 years ago  by <img width="18" height="18" src="../../_resources/4d7ac898675746a888d4a72cf3083f91.jpg"/>  akrulik@bcls.lib.nj.us

[leap](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=99#t=commResults) [logons](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=215#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 2 years ago

<a id="upVoteAns_504"></a>[](#)

0

<a id="downVoteAns_504"></a>[](#)

Answer Accepted

You can try this and modify it or sort it however fits for you.

DECLARE @BeginDate datetime SET @BeginDate = '2019-01-01'
DECLARE @EndDate datetime SET @EndDate = '2019-03-06'
SELECT O.Abbreviation, PU.Name, W.ComputerName, CASE WHEN TH.TransactionTypeID=7200 THEN 'LogOn' WHEN TH.TransactionTypeID=7201 THEN 'LogOff' END AS 'Action',
CASE WHEN TD.numValue=38 THEN 'LEAP' ELSE 'Polaris' END As 'ClientType', TH.TranClientDate
from PolarisTransactions.Polaris.TransactionHeaders TH
JOIN PolarisTransactions.Polaris.TransactionDetails TD
ON TD.TransactionID = TH.TransactionID
JOIN Polaris.Polaris.Workstations W
ON W.WorkstationID = TH.WorkstationID
JOIN Polaris.Polaris.PolarisUsers PU
ON PU.PolarisUserID = TH.PolarisUserID
JOIN Polaris.Polaris.Organizations O
ON O.OrganizationID = TH.OrganizationID
WHERE TH.TransactionTypeID IN (7200,7201)
AND TD.TransactionSubTypeID = 235
AND TH.TranClientDate BETWEEN @BeginDate AND @EndDate
ORDER BY W.ComputerName, TH.TranClientDate,PU.PolarisUserID,  O.Abbreviation

Rex Helwig
Finger Lakes Library System
[rhelwig@flls.org](mailto:rhelwig@flls.org)
607-319-5615

answered 2 years ago  by <img width="18" height="18" src="../../_resources/4d7ac898675746a888d4a72cf3083f91.jpg"/>  rhelwig@flls.org

Thanks.  We will test this tomorrow and let you know how it goes.

Anne

— akrulik@bcls.lib.nj.us 2 years ago

Thanks again.

— akrulik@bcls.lib.nj.us 2 years ago

Thank you, Rex!
Mary

— mary@fortbend.lib.tx.us 9 months ago

<a id="commentAns_504"></a>[Add a Comment](#)

<a id="upVoteAns_502"></a>[](#)

0

<a id="downVoteAns_502"></a>[](#)

Thanks, Gwyneth.  I posted in the Idea Lab.  In the meantime maybe someone has an SQL.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/4d7ac898675746a888d4a72cf3083f91.jpg"/>  akrulik@bcls.lib.nj.us

- <a id="acceptAnswer_502"></a>[Accept as answer](#)

<a id="commentAns_502"></a>[Add a Comment](#)

<a id="upVoteAns_501"></a>[](#)

0

<a id="downVoteAns_501"></a>[](#)

Hi Anne,

I can't (unfortunately) give advice on the SQL query, but I can tell you that it is always a good idea to submit via Idea Lab.  There may be a ton of libraries out there who are dying for this report.  If you submit this idea via Always Open on Idea Lab, and illustrate its need to be a standard report instead of custom, then others can get on your bandwagon.  I currently serve to assist the IUG Enhancements Co-Coordinators to represent the Polaris side.  I am happy to help if you ever need assistance with Idea Lab.

I hope you receive your SQL answer and best of luck with the Idea Lab!

Sincerely,

Gwyneth Jelinek

Fayetteville Public Library

gjelinek@faylib.org

answered 2 years ago  by <img width="18" height="18" src="../../_resources/4d7ac898675746a888d4a72cf3083f91.jpg"/>  gjelinek@faylib.org

- <a id="acceptAnswer_501"></a>[Accept as answer](#)

<a id="commentAns_501"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Running an SQL in Leap](https://iii.rightanswers.com/portal/controller/view/discussion/47?)
- [LEAP for Dummies or LEAP 101](https://iii.rightanswers.com/portal/controller/view/discussion/911?)
- [Leap Offline Procedures](https://iii.rightanswers.com/portal/controller/view/discussion/635?)
- [Leap and RFID](https://iii.rightanswers.com/portal/controller/view/discussion/427?)
- [Payments in Leap](https://iii.rightanswers.com/portal/controller/view/discussion/50?)

### Related Solutions

- [Leap login error "You do not have the permission, Access Leap: Allow"](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930322636227)
- [Innovative's HTTPS Everywhere Plan](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180920175535460)
- [Adding Leap permissions en mass](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930306603411)
- [What is required for Leap?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930646331533)
- [Leap resource guide](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210217105146847)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)