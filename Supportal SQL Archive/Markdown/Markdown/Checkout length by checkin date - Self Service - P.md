Checkout length by checkin date - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Checkout length by checkin date

0

54 views

Found this excellent query (by Brian Wohlers)  to find Average length of checkout by material type. but I'd like to switch it around to look at checkin date as opposed to checkout date (so for items checked in in September, they were checked out for x days). Can anyone help?

select mt.Description \[MaterialType\]

, AVG(CONVERT(float,ChkAvgLen.CheckoutLength)) \[Average Checkout Length in Days\]

, MIN(ChkAvgLen.CheckoutLength) \[Min Checkout Length\]

, MAX(ChkAvgLen.CheckoutLength) \[Max Checkout Length\]

, COUNT(ChkAvgLen.ItemRecordID) \[Number of items checked out\]

from (

select ChkLen.ItemRecordID, ChkLen.CheckoutTransactionID, ChkLen.CheckoutDate, ChkLen.CheckinDate, (CONVERT(int,ChkLen.CheckinDate) - CONVERT(int,ChkLen.CheckoutDate)) \[CheckoutLength\]

from

(

select ChkRecent.ItemRecordID, ChkRecent.CheckoutTransactionID, ChkRecent.CheckoutDate, MIN(ChkRecent.CheckinDate) \[CheckinDate\]

from (

select firsttdItm.numValue \[ItemRecordID\], chkout.TransactionID \[CheckoutTransactionID\], chkout.TranClientDate \[CheckoutDate\], chkin.TransactionID \[CheckinTransactionID\], chkin.TranClientDate \[CheckinDate\]

from PolarisTransactions.Polaris.TransactionHeaders chkout (nolock)

join PolarisTransactions.Polaris.TransactionDetails firsttdItm (nolock)

on (firsttdItm.TransactionID = chkout.TransactionID and firsttdItm.TransactionSubTypeID = 38)

left outer join PolarisTransactions.Polaris.TransactionDetails firsttdRnw (nolock)

on (firsttdRnw.TransactionID = chkout.TransactionID and firsttdRnw.TransactionSubTypeID = 124)

left outer join PolarisTransactions.Polaris.TransactionDetails firsttdChkT (nolock)

on (firsttdChkT.TransactionID = chkout.TransactionID and firsttdChkT.TransactionSubTypeID = 145)

left outer join (select subtd.numValue, subtd.TransactionID, subth.TranClientDate from PolarisTransactions.Polaris.TransactionHeaders subth join PolarisTransactions.Polaris.TransactionDetails subtd on (subth.TransactionID = subtd.TransactionID and subtd.TransactionSubTypeID = 38) where subth.TransactionTypeID = 6002 ) chkin

on (chkin.numValue = firsttdItm.numValue and chkin.TransactionID > chkout.TransactionID and firsttdItm.numValue = chkin.numValue)

where chkout.TransactionTypeID = 6001 and firsttdRnw.numValue is null and firsttdChkT.numValue in (15,20,22,42,43,44,51)

and chkout.TranClientDate between '2020-9-1' and '2020-9-30 23:59:59.998'

) as ChkRecent

group by ChkRecent.ItemRecordID, ChkRecent.CheckoutDate, ChkRecent.CheckoutTransactionID

) as ChkLen

) as ChkAvgLen

left outer join PolarisTransactions.Polaris.TransactionDetails tdMat (nolock)

on (tdMat.TransactionID = ChkAvgLen.CheckoutTransactionID and tdMat.TransactionSubTypeID = 4)

join Polaris.Polaris.MaterialTypes mt (nolock)

on tdMat.numValue = mt.MaterialTypeID

group by mt.Description

asked 10 months ago  by <img width="18" height="18" src="../../_resources/12f2a22dc2a64d91aeeac69d3dec9a5a.jpg"/>  crosenthal@pwcgov.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 6 months ago

<a id="upVoteAns_1072"></a>[](#)

0

<a id="downVoteAns_1072"></a>[](#)

I didn't fully fix the formatting of the original query to see exactly what it was doing, but based off its final 'select' and your description I think this should just about do the trick. I filter out some extremely high/low loan length values, which will technically skew the number of items, though most of those are unlikely to be "real" circs anyway.

declare @startDate date = '1/1/2021'
declare @endDate date = '2/1/2021'

select d.MaterialType
,avg(d.LoanDays) \[AvgLoanDays\]
,min(d.LoanDays) \[MinLoanDays\]
,max(d.LoanDays) \[MaxLoandays\]
,count(*) \[NumItemsNotEntirelyAccurate\]
from (
select mt.Description \[MaterialType\]
,td_ll.numValue / 60.0 / 24.0 \[LoanDays\]
from PolarisTransactions.Polaris.TransactionHeaders th
join PolarisTransactions.Polaris.TransactionDetails td_ll
on td\_ll.TransactionID = th.TransactionID and td\_ll.TransactionSubTypeID = 323
join PolarisTransactions.Polaris.TransactionDetails td_item
on td\_item.TransactionID = th.TransactionID and td\_item.TransactionSubTypeID = 38
join Polaris.polaris.CircItemRecords cir
on cir.ItemRecordID = td_item.numValue
join Polaris.Polaris.MaterialTypes mt
on mt.MaterialTypeID = cir.MaterialTypeID
where th.TransactionTypeID = 6002
and isnull(td_ll.numValue,0) between 1000 and 1000000 -- adjust these numbers to eliminate outliers but also further skew number of items downwards
and th.TranClientDate between @startDate and @endDate
) d
group by d.MaterialType

answered 6 months ago  by <img width="18" height="18" src="../../_resources/12f2a22dc2a64d91aeeac69d3dec9a5a.jpg"/>  mfields@clcohio.org

- <a id="acceptAnswer_1072"></a>[Accept as answer](#)

<a id="commentAns_1072"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL top bib checkouts between 2 dates](https://iii.rightanswers.com/portal/controller/view/discussion/650?)
- [Does anyone have a sql script for a monthly in-house check-in count?](https://iii.rightanswers.com/portal/controller/view/discussion/615?)
- [Need help with query to find checked out titles with holds](https://iii.rightanswers.com/portal/controller/view/discussion/1063?)
- [Mass change Hold Pick Up date?](https://iii.rightanswers.com/portal/controller/view/discussion/95?)
- [Need an sql for circulation by workstation between two dates](https://iii.rightanswers.com/portal/controller/view/discussion/159?)

### Related Solutions

- [How are due dates calculated?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930778190992)
- [How to take a collection inventory using Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930994460225)
- [Setting the default view for the Check In workform](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930805135071)
- [Block checkouts in SIP when item is out](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930597906901)
- [Label/Paper stock for BBM Mailer](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=171201090449943)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)