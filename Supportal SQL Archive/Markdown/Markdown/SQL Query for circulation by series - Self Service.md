SQL Query for circulation by series - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL Query for circulation by series

0

61 views

Hi everyone,

Thank you all for your help in advance, I'm still somewhat new to SQL querying. I'm trying to find circulation stats on certain items in out collection by series information (I'd like to see if a whole series checks out or if interest fails after the first book or two) and I came up with the following query:

SELECT ir.callnumber, br.browseauthor, br.browsetitle, ci.lastcheckoutrenewdate, ci.itemstatusid, ci.firstavailabledate, ci.lifetimecirccount, bs.data

FROM circitemrecords ci

inner join itemrecorddetails ir on (ci.itemrecordid = ir.itemrecordid)

inner join bibliographicrecords br on (ci.associatedbibrecordid = br.bibliographicrecordid)

inner join bibliographictags bt on (br.bibliographicrecordid = bt.bibliographicrecordid)

inner join bibliographicsubfields bs on (bt.bibliographictagid = bs.bibliographictagid)

WHERE ci.assignedbranchid = 26 and ci.shelflocationid = 14 and (bt.tagnumber = 800 or bt.tagnumber = 830)

The query almost works. The problem is that the results will give me separate rows for each subfield in the 800, so a single bib record with the following 800:

$aPatterson, James,$d1947-$tWomen's murder club ;$vbk. 1. 

shows up four times in my results, each row showing a different subfield. Does anyone know of a way that would combine all of the rows so that each item only shows up once with all of the 800/830 data in the same row?

Thank you!

asked 1 year ago  by <img width="18" height="18" src="../../_resources/692d6f1ecce049d28419fdbba9779081.jpg"/>  josh.barnes@portneuflibrary.org

[polaris](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=7#t=commResults) [reporting](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=82#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults) [sql server](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=256#t=commResults) [sql server management studio](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=305#t=commResults) [ssms](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=306#t=commResults) [statistics](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=307#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_768"></a>[](#)

1

<a id="downVoteAns_768"></a>[](#)

Answer Accepted

Hi Josh.

I came up with something a while ago that concatenates the subfields in 245, 440, 490, 800 and 830. It looks a bit "ugly" to me, but it seemed to work for my purposes.Perhaps you can modify it for yours.

It is based on a record set, but you may be able to replace the subquery conditions with the first part of your WHERE statement.

Hope this helps,

JT

--Begin query below

SELECT

br.MARCPubDateOne as PubYR
, br.BrowseCallNo
, br.SortTitle
,br.BibliographicRecordID AS BibID

,REPLACE (
 REPLACE(
  REPLACE(
   REPLACE(
    REPLACE(
     REPLACE(
      REPLACE(CONCAT(bt.tagnumber, ' ', bt.indicatorone, bt.indicatortwo,'$a', bsa.Data
      ,'$c', bsc.Data,'$q', bsq.Data, '$d' + bsd.Data, '$t', bst.Data, '$s', bss.Data
      , '$v', bsv.Data, '$9'),'$v$', '$'),
      '$s$', '$'),
     '$t$', '$'),
    '$q$','$'),
   '$c$', '$'),
  '$a$', '$'),
 '$9', '')

as \[SubFielddata\]

from Polaris.Polaris.BibliographicRecords br with (nolock)
LEFT OUTER JOIN Polaris.Polaris.BibliographicTags bt with (nolock)
ON (br.BibliographicRecordID= bt.BibliographicRecordID and bt.TagNumber in (245,440,490,800, 830))
LEFT OUTER JOIN Polaris.Polaris.BibliographicSubfields bsa (nolock)
on (bt.BibliographicTagID = bsa.BibliographicTagID and bt.TagNumber in (245,440,490,800,830) and bsa.subfield like 'a')
LEFT OUTER JOIN Polaris.Polaris.BibliographicSubfields bsc (nolock)
on (bt.BibliographicTagID = bsc.BibliographicTagID and bt.TagNumber in (440,490,800) and bsc.subfield like 'c')
LEFT OUTER JOIN Polaris.Polaris.BibliographicSubfields bsq (nolock)
on (bt.BibliographicTagID = bsq.BibliographicTagID and bt.TagNumber in (440,490,800) and bsq.subfield like 'q')
LEFT OUTER JOIN Polaris.Polaris.BibliographicSubfields bsd (nolock)
on (bt.BibliographicTagID = bsd.BibliographicTagID and bt.TagNumber in (440,490,800) and bsd.subfield like 'd')
LEFT OUTER JOIN Polaris.Polaris.BibliographicSubfields bst (nolock)
on (bt.BibliographicTagID = bst.BibliographicTagID and bt.TagNumber in (440,490,800) and bst.subfield like 't')
LEFT OUTER JOIN Polaris.Polaris.BibliographicSubfields bss (nolock)
on (bt.BibliographicTagID = bss.BibliographicTagID and bt.TagNumber in (440,490,800) and bss.subfield like 's')
LEFT OUTER JOIN Polaris.Polaris.BibliographicSubfields bsv (nolock)
on (bt.BibliographicTagID = bsv.BibliographicTagID and bt.TagNumber in (440,490,800,830) and bsv.subfield like 'v')

WHERE br.RecordStatusID = 1
AND br.BibliographicRecordID IN
 (SELECT BibliographicRecordID from Polaris.Polaris.BibRecordSets with (nolock)
 WHERE RecordSetID = 72696)

ORDER BY br.SortTitle, br.MARCPubDateOne, br.BrowseCallNo ,br.BibliographicRecordID, bt.TagNumber

answered 1 year ago  by <img width="18" height="18" src="../../_resources/692d6f1ecce049d28419fdbba9779081.jpg"/>  jwhitfield@aclib.us

Thank you! I was able to modify your query to match what I needed.

— josh.barnes@portneuflibrary.org 1 year ago

<a id="commentAns_768"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL query for circulation by call number range and date range](https://iii.rightanswers.com/portal/controller/view/discussion/190?)
- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)
- [I need help creating a SQL for circulation data that includes a lot of details.](https://iii.rightanswers.com/portal/controller/view/discussion/550?)
- [SQL for patron circ at a particular workstation](https://iii.rightanswers.com/portal/controller/view/discussion/918?)
- [SQL for circulation by call number prefix and date range](https://iii.rightanswers.com/portal/controller/view/discussion/913?)

### Related Solutions

- [Query to find unfillable holds due to volume data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930242167096)
- [Find all permission groups that have the Secure patron record permissions](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930759206464)
- [How do I reset the year-to-date circulation counts?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930831368342)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [Where are the waive and charge free text notes stored in the database?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930334007926)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)