I need help creating a SQL for circulation data that includes a lot of details.  - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# I need help creating a SQL for circulation data that includes a lot of details.

0

120 views

I'm new to writing SQL and I'm having trouble writing a circulation script with that includes a lot of details.  I don't know if I need to use the Transaction database or CircItem.

My details need to have....

Barcode, Bib Record ID, Transaction ID, Item Record ID, Transcating Branch, Browse Author, Browse Call Number, Browse Title, Checkout / Renewal (transcation type), Collection Name, Material Type, Pulication Year, Transcation Date

I appreciate any help I can get. I'm so confused as to how to create this SQL. 

asked 2 years ago  by <img width="18" height="18" src="../../_resources/81dd3230a92b4edb8fc4158e36d8022f.jpg"/>  lori@esrl.org

[circulation](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=185#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_662"></a>[](#)

0

<a id="downVoteAns_662"></a>[](#)

Hi Lori.

Here is a query based on the transactions tables. I have tried to include comments where they may be helpful.

This query does not have an ORDER BY statement, but you can add one to meet your needs.

You can also limit the results by omitting branches, collections, etc. by adding to the WHERE statement.

This query (for the month of February took about 1 minute, 18 seconds to run on our training server. Your "milage" may vary, but if you are trying to get transactions for a long range of dates, it may take a while to run.

Hope this is helpful.

JT

PS: If you can get them, lists of transaction types, transaction subtypes and transaction subtype codes can be very helpful when figuring out what means what in TransactionDetails. Also, sometimes something that looks like what you want is actually not what Polaris calls it. You can get a list of all the transaction details for a particular transaction to see what is actually being recorded in that transaction. I put a brief query to do this after the main query below.

SELECT
ir.Barcode
,br.BibliographicRecordID
,th.TransactionID
,org.Name as TransactingBranch
,br.BrowseAuthor
,br.BrowseCallNo
,br.BrowseTitle
,tsc.TransactionSubTypeCodeDesc as TransactionType
,col.Name as \[Collection\]
,mt.Description as MaterialType
,br.PublicationYear
,th.TranClientDate --Can use TranClientDate to get the date/time of offline transactions, otherwise TransactionDate is OK

FROM PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK)
JOIN PolarisTransactions.Polaris.TransactionDetails tdi (NOLOCK)
ON (th.TransactionID = tdi.TransactionID AND tdi.TransactionSubTypeID = 38) --ItemrecordID. Needed to link to Bib record
JOIN Polaris.Polaris.ItemRecords ir (NOLOCK)
ON (tdi.numValue = ir.ItemRecordID)
JOIN Polaris.Polaris.BibliographicRecords br (NOLOCK)
ON (ir.AssociatedBibRecordID = br.BibliographicRecordID) --Data from bibliographic record
JOIN PolarisTransactions.Polaris.TransactionDetails tdct (NOLOCK)
ON (th.TransactionID = tdct.TransactionID AND tdct.TransactionSubTypeID = 145) --Checkout type
JOIN PolarisTransactions.Polaris.TransactionSubTypeCodes tsc (NOLOCK)
ON (tdct.numValue = tsc.TransactionSubTypeCode) --Checkout type description
JOIN PolarisTransactions.Polaris.TransactionDetails tdc
ON (th.TransactionID = tdc.TransactionID AND tdc.TransactionSubTypeID = 61) --Collection
LEFT OUTER JOIN Polaris.Polaris.Collections col (NOLOCK)
ON (tdc.numValue = col.CollectionID) --Collection name. Left join in case the item had no collection
JOIN PolarisTransactions.Polaris.TransactionDetails tdmt (NOLOCK)
ON (th.TransactionID = tdmt.TransactionID and tdmt.TransactionSubTypeID = 4) --Material type
JOIN Polaris.Polaris.MaterialTypes mt (NOLOCK)
ON (tdmt.numValue = mt.MaterialTypeID) --Material type description
JOIN Polaris.Polaris.Organizations org (NOLOCK)
ON (th.OrganizationID = org.OrganizationID)

WHERE

th.TransactionTypeID = 6001
AND th.TranClientDate >= '02/01/2019'
AND th.TranClientDate < '02/16/2019'--gets everything through the end of the previous day

--Query to get transaction details stored for a type of transaction--

SELECT TOP 100 * from
PolarisTransactions.Polaris.TransactionDetails td (NOLOCK)
JOIN PolarisTransactions.Polaris.TransactionHeaders th
ON (th.TransactionID = td.TransactionID AND th.TransactionTypeID = 6001)
ORDER BY th.TransactionID DESC

answered 2 years ago  by <img width="18" height="18" src="../../_resources/81dd3230a92b4edb8fc4158e36d8022f.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_662"></a>[Accept as answer](#)

Hi Lori. I Just realized that I goofed on the first line of the SELECT statement. It should be "ir.Barcode". >sigh< I edited the query above to reflect this.

— jwhitfield@aclib.us 2 years ago

Thanks JT. I'm going to try it and see if it works for us. 

— lori@esrl.org 2 years ago

It worked.  You're a genius!!!!!  Thank you so much.  I would have been pulling my hair out with this query. 

— lori@esrl.org 2 years ago

<a id="commentAns_662"></a>[Add a Comment](#)

<a id="upVoteAns_656"></a>[](#)

0

<a id="downVoteAns_656"></a>[](#)

Hi Lori.

If you need the "official" TransactionID and TransactionDate, you will need to join the transactions database to the "main" Polaris database. The transactions tables include much of the information you want, but is coded in TransactionDetails under various subtype IDs  (ItemRecordID, AssignedCollectionID, etc.). Elements from the bibliographic record will need to come from the main Polaris database, along with any descriptions (names of Collections, Branches, etc.).

If you want to stay within one database, the ItemRecordHistory table in the main Polaris database could be a substitute for transaction data. Queries on ItemRecordHistory often run quicker than ones involving transactions.

It is important to note that you would not have an "official" TransactionID, but you would have an ItemRecordHistoryID. Also, the transactions tables store the status of the item (collection, material type, stat code, etc.) at the time of the transaction. Using ItemRecordHistory and linking to item records will give you, for example, the current collection, which may be different from the collection at the time of the checkout/renewal. (This may be a good thing, or not, depending on how much cleanup or reassignment you may have done.)

One last thing, ItemRecordHistory may not go back as far as the transactions tables will, so if you are looking for transactions a long time ago you may be out of luck with IRH.

Let us know which direction you would like to go and I'm sure someone will be able to help out.

JT

answered 2 years ago  by <img width="18" height="18" src="../../_resources/81dd3230a92b4edb8fc4158e36d8022f.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_656"></a>[Accept as answer](#)

<a id="commentAns_656"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [I need a SQL for phatom lost item count attached to a patron's record.](https://iii.rightanswers.com/portal/controller/view/discussion/839?)
- [SQL for patron circ at a particular workstation](https://iii.rightanswers.com/portal/controller/view/discussion/918?)
- [SQL for circulation by call number prefix and date range](https://iii.rightanswers.com/portal/controller/view/discussion/913?)
- [SQL query for circulation by call number range and date range](https://iii.rightanswers.com/portal/controller/view/discussion/190?)
- [I need a SQL for locating hold requests where the hold has been denied by a library](https://iii.rightanswers.com/portal/controller/view/discussion/843?)

### Related Solutions

- [Query to find unfillable holds due to volume data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930242167096)
- [How do I reset the year-to-date circulation counts?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930831368342)
- [Adding a new UDF to patron registration](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930740605846)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [Where are the waive and charge free text notes stored in the database?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930334007926)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)