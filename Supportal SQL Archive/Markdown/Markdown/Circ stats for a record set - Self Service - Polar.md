Circ stats for a record set - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Circ stats for a record set

0

161 views

Does anyone have a way (either via Simply Reports or SQL query) to get circulation statistics for a group of items (in a record set) for a particular time period? We'd like to get circ stats for particular group of items for a three week period and compare it to stats for the same period last year. We have the items in a record set. Thanks!

asked 3 years ago  by <img width="18" height="18" src="../../_resources/6799e5c56a2f4860ab9c0ceb4212d12a.jpg"/>  musack@bcls.lib.nj.us

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 3 years ago

<a id="upVoteAns_136"></a>[](#)

1

<a id="downVoteAns_136"></a>[](#)

Answer Accepted

This should get the job done.  Update the dates and recordsetid as necessary.

SELECT CASE td2.numValue WHEN '1' THEN 'Renewal' ELSE 'Check Out' END, COUNT(*)
FROM PolarisTransactions.Polaris.TransactionHeaders AS \[th\] WITH (NOLOCK)
INNER JOIN PolarisTransactions.Polaris.TransactionDetails AS \[td1\] WITH (NOLOCK)
ON th.TransactionID = td1.TransactionID AND td1.TransactionSubTypeID = '38'
INNER JOIN Polaris.Polaris.ItemRecordSets AS \[irs\] WITH (NOLOCK)
ON td1.numValue = irs.ItemRecordID
LEFT OUTER JOIN PolarisTransactions.Polaris.TransactionDetails AS \[td2\] WITH (NOLOCK)
ON th.TransactionID = td2.TransactionID AND td2.TransactionSubTypeID = '124'
WHERE th.TransactionTypeID = '6001'
--circ dates

AND th.TranClientDate BETWEEN '20170101' AND '20170131 23:59:59'
--RecordSetID

AND irs.RecordSetID = '41066'
GROUP BY CASE td2.numValue WHEN '1' THEN 'Renewal' ELSE 'Check Out' END

Trevor D
--
Trevor Diamond
Systems/UX Librarian
MAIN (Morris Automated Information Network), Morristown NJ
www.mainlib.org

answered 3 years ago  by <img width="18" height="18" src="../../_resources/6799e5c56a2f4860ab9c0ceb4212d12a.jpg"/>  trevor.diamond@mainlib.org

Thank you for this, Trevor! So useful.

Sarah Gillis

Technical Services Supervisor

Bernardsville Public Library, Bernardsville, NJ

— sgillis@bernardsvillelibrary.org 2 years ago

<a id="commentAns_136"></a>[Add a Comment](#)

<a id="upVoteAns_137"></a>[](#)

0

<a id="downVoteAns_137"></a>[](#)

Thank you so much, Trevor! We'll give that a shot. I really appreciate your help!

answered 3 years ago  by <img width="18" height="18" src="../../_resources/6799e5c56a2f4860ab9c0ceb4212d12a.jpg"/>  musack@bcls.lib.nj.us

- <a id="acceptAnswer_137"></a>[Accept as answer](#)

<a id="commentAns_137"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL or report for circ stats by Patron Statistical Class?](https://iii.rightanswers.com/portal/controller/view/discussion/488?)
- [SQL query to export specific record set: Polaris 4.1R2](https://iii.rightanswers.com/portal/controller/view/discussion/93?)
- [Is it possible to save a record set when running a saved report in Simply Reports?](https://iii.rightanswers.com/portal/controller/view/discussion/979?)
- [Simply Reports Discrepancies](https://iii.rightanswers.com/portal/controller/view/discussion/960?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)

### Related Solutions

- [Billed items be set to Lost and Lost Item Recovery table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930478370792)
- [Item statistical class code changes automatically](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181025152019128)
- [Bulk Change Circulation Statuses](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930606607662)
- [Record sets are not displaying in SimplyReports](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170306172536417)
- [Polaris 7.0 – Breaking PAPI Change in Record Set Endpoints](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210510134358007)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)