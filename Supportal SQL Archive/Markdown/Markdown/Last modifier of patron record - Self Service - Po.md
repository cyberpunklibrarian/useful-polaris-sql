Last modifier of patron record - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Last modifier of patron record

0

34 views

Hi.

I am trying to get a list of patron records both created and or updated by our staff members for a contest on how many cards they add or update in the system per week/month. I have noticed some similar SQL statements, but the CreatorID is the original creator, which works for the new cards, but doesnt seem to work for the updated or renewed cards? I see there is a modifier naem in the patron record, but I am not sure what tabel to pull that name from?

asked 28 days ago  by <img width="18" height="18" src="../../_resources/8acba9ce7caa44fc84a6a91794bc9a67.jpg"/>  brandon.williams@mesaaz.gov

[sql patron](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=387#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 28 days ago

<a id="upVoteAns_1160"></a>[](#)

0

<a id="downVoteAns_1160"></a>[](#)

You can also look at the transaction logs (if you're tracking patron creation and modification).  THe SQL below would provide counts per user for patrons created or modified in a given time frame.

SELECT pu.Name, tt.TransactionTypeDescription, COUNT(*) AS \[Patrons\]
FROM PolarisTransactions.Polaris.TransactionHeaders AS \[th\] WITH (NOLOCK)
INNER JOIN Polaris.Polaris.PolarisUsers AS \[pu\] WITH (NOLOCK)
ON th.PolarisUserID = pu.PolarisUserID
INNER JOIN PolarisTransactions.Polaris.TransactionTypes AS \[tt\] WITH (NOLOCK)
ON th.TransactionTypeID = tt.TransactionTypeID
AND th.TransactionTypeID IN ('2001','2003')
AND th.TranClientDate BETWEEN '20210801' AND '20210831 23:59:59'
GROUP BY pu.Name, tt.TransactionTypeDescription

answered 28 days ago  by <img width="18" height="18" src="../../_resources/8acba9ce7caa44fc84a6a91794bc9a67.jpg"/>  trevor.diamond@mainlib.org

- <a id="acceptAnswer_1160"></a>[Accept as answer](#)

<a id="commentAns_1160"></a>[Add a Comment](#)

<a id="upVoteAns_1159"></a>[](#)

0

<a id="downVoteAns_1159"></a>[](#)

You could use the ViewPatronRegistration table which probably has everthing you would need and join it to the PolarisUsers table on the on PolarisUserID=ModifierID

Rex

answered 28 days ago  by <img width="18" height="18" src="../../_resources/8acba9ce7caa44fc84a6a91794bc9a67.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_1159"></a>[Accept as answer](#)

<a id="commentAns_1159"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for changes to a patron record](https://iii.rightanswers.com/portal/controller/view/discussion/840?)
- [I'm looking for a report that will tell me how many bib records a staff member has created and/or modified in a month.](https://iii.rightanswers.com/portal/controller/view/discussion/942?)
- [Circulation report by patrons age](https://iii.rightanswers.com/portal/controller/view/discussion/97?)
- [Simply reports patron services](https://iii.rightanswers.com/portal/controller/view/discussion/355?)
- [SQL query to export specific record set: Polaris 4.1R2](https://iii.rightanswers.com/portal/controller/view/discussion/93?)

### Related Solutions

- [Importing Student Records as Patron Records - Polaris 5.6](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170927144406767)
- [Patron purge and record set](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930655028324)
- [Importing Student Records 5.1 SP1](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161021110533329)
- [Importing Student Records 6.7](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210104115104170)
- [Importing Student Records 5.0](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160503132529489)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)