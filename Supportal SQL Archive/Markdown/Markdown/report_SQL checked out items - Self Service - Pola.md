[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# report/SQL checked out items

0

313 views

Hello, 

I'm looking for a report/SQL that gives us the current items that are checked out? I'm drawing a blank the best way to go about to get numbers/items. 

Thank in advance!!!

Teresa R

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_6e9974a5ea894157ae4308c57c182362.jpg"/> treel@mypccl.org

[checked out items sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=343#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 4 years ago

<a id="upVoteAns_917"></a>[](#)

0

<a id="downVoteAns_917"></a>[](#)

Answer Accepted

If you are a part of a consortium you may want to know just what was checked out at your library rather than what you own that is checked out.  You can use the SQL in the Item Find Tool to get a list of items checked out at/by your library.  You just need to know your OrganizationID number.

SELECT ItemRecordID  
FROM ItemCheckouts  
WHERE OrganizationID = {OrgID Here}

&nbsp;

Rex Helwig

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_6e9974a5ea894157ae4308c57c182362.jpg"/> rhelwig@flls.org

<a id="commentAns_917"></a>[Add a Comment](#)

<a id="upVoteAns_919"></a>[](#)

0

<a id="downVoteAns_919"></a>[](#)

Both suggestions worked. We are not in a consortium, so I removed the WHERE. 

Thank you!!!

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_6e9974a5ea894157ae4308c57c182362.jpg"/> treel@mypccl.org

- <a id="acceptAnswer_919"></a>[Accept as answer](#)

<a id="commentAns_919"></a>[Add a Comment](#)

<a id="upVoteAns_916"></a>[](#)

0

<a id="downVoteAns_916"></a>[](#)

Hmm, that's too bad. You may be able to do an item search, using circ status "out" ... see attached image. Depending on how many items are out, you may need to go to the settings tab and increase the retrieval limit.

&nbsp;

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_6e9974a5ea894157ae4308c57c182362.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_916"></a>[Accept as answer](#)

- [Item search.PNG](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/749?fileName=749-916_Item+search.PNG "Item search.PNG")

<a id="commentAns_916"></a>[Add a Comment](#)

<a id="upVoteAns_915"></a>[](#)

0

<a id="downVoteAns_915"></a>[](#)

You could use Simply Reports to get an item list report of all items with current circ status of "out"

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_6e9974a5ea894157ae4308c57c182362.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_915"></a>[Accept as answer](#)

Unfortunately we do not have simply reports 

— treel@mypccl.org 4 years ago

<a id="commentAns_915"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [Searching for Items that have not circ'd in 2 years ex. DVD's](https://iii.rightanswers.com/portal/controller/view/discussion/1053?)
- [SQL Query to find items that we own in eBook format but not in physical book format.](https://iii.rightanswers.com/portal/controller/view/discussion/1330?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)

### Related Solutions

- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [SQL query for a count of items by material type](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930506564972)
- [New Community Records report is blank](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930406592025)
- [Automatically open SQL Server query window](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930908117973)
- [SysHoldRequest table missing Title information](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930733866097)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)