SQL for a history of holds or requests for a specific patron? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=20)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# SQL for a history of holds or requests for a specific patron?

0

193 views

Hello, all!

I'm at the limit of my abilities to pull some data via SQL, so I'm hoping someone can help me.

I've got a patron who says that holds are disappearing off their account. While I can pull a list of active holds, and I can pull somewhat limited data from the SysHoldHistory table, I'm not figuring out how (or if) I can pull an entire history of requests for a given patron, by PatronID.

Any ideas would be seriously appreciated!

asked 2 years ago  by ![danielmesser@mcldaz.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_danielmesser@mcldaz.org20180523162210.jpg)  danielmesser@mcldaz.org

[history](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=180#t=commResults) [holds](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=96#t=commResults) [requests](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=181#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 2 years ago

<a id="upVoteAns_440"></a>[](#)

2

<a id="downVoteAns_440"></a>[](#)

Answer Accepted

Hey Daniel-

SysHoldHistory would only contain history actions for current holds.  For historical information, you have to go to the Transaction logs.  Below is the query I use, which might be helpful to you. Near the end is a place to put in the patron barcode.

Due to the restrictions of posting on this forum, please note that the 'hold' clause should actually have a percent sign before and after the word, but I can't leave them in and still post.

--Patron Hold History
SELECT w.ComputerName, th.TranClientDate AS \[Activity Date\], th.TransactionTypeID, tt.TransactionTypeDescription AS \[Activity Type\], th.TransactionID, holdid.numValue AS \[Hold ID\], cir.Barcode AS \[Item Barcode\], br.BrowseTitle AS \[Item Title\], br2.BrowseTitle AS \[Hold Title\], br2.BibliographicRecordID AS \[Hold Title ID\]
FROM PolarisTransactions.Polaris.TransactionHeaders AS \[th\] WITH (NOLOCK)
INNER JOIN PolarisTransactions.Polaris.TransactionTypes AS \[tt\] WITH (NOLOCK)
ON th.TransactionTypeID = tt.TransactionTypeID
INNER JOIN PolarisTransactions.Polaris.TransactionDetails AS \[td\] WITH (NOLOCK)
ON th.TransactionID = td.TransactionID AND td.TransactionSubTypeID = '6'
LEFT OUTER JOIN PolarisTransactions.Polaris.TransactionDetails AS \[td1\] WITH (NOLOCK)
ON th.TransactionID = td1.TransactionID AND td1.TransactionSubTypeID = '38'
LEFT OUTER JOIN Polaris.Polaris.CircItemRecords AS \[cir\] WITH (NOLOCK)
ON td1.numValue = cir.ItemRecordID
LEFT OUTER JOIN Polaris.Polaris.BibliographicRecords AS \[br\] WITH (NOLOCK)
ON cir.AssociatedBibRecordID = br.BibliographicRecordID
INNER JOIN Polaris.Polaris.Workstations AS \[w\] WITH (NOLOCK)
ON th.WorkstationID = w.WorkstationID
LEFT OUTER JOIN PolarisTransactions.Polaris.TransactionDetails AS \[holdid\] WITH (NOLOCK)
ON th.TransactionID = holdid.TransactionID AND holdid.TransactionSubTypeID ='233'
LEFT OUTER JOIN PolarisTransactions.Polaris.TransactionDetails AS \[title\] WITH (NOLOCK)
ON th.TransactionID = title.TransactionID AND title.TransactionSubTypeID = '36'
LEFT OUTER JOIN Polaris.Polaris.BibliographicRecords AS \[br2\] WITH (NOLOCK)
ON title.numValue = br2.BibliographicRecordID
WHERE td.numValue = ( SELECT p.PatronID FROM Polaris.Polaris.Patrons AS \[p\] WITH (NOLOCK) WHERE p.Barcode =
'1101500131417')
AND tt.TransactionTypeDescription LIKE 'hold'
ORDER BY th.TranClientDate

answered 2 years ago  by <img width="18" height="18" src="../../_resources/2c757c8c8e164c13b9a304856d0ea3ff.jpg"/>  trevor.diamond@mainlib.org

<a id="commentAns_440"></a>[Add a Comment](#)

<a id="upVoteAns_658"></a>[](#)

0

<a id="downVoteAns_658"></a>[](#)

Daniel,

Can this query be run in the client? If so, where? 

Thanks! 

answered 2 years ago  by ![chad.eller@gastongov.com](https://iii.rightanswers.com/portal/app/images/custom/avatar_chad.eller@gastongov.com20210201162116.jpg)  chad.eller@gastongov.com

- <a id="acceptAnswer_658"></a>[Accept as answer](#)

No, it can't, I'm sorry. 

— trevor.diamond@mainlib.org 2 years ago

<a id="commentAns_658"></a>[Add a Comment](#)

<a id="upVoteAns_441"></a>[](#)

0

<a id="downVoteAns_441"></a>[](#)

Freaking BRILLIANT, Trevor! I knew I'd have to tie in the Transactions database but I was flummoxed as to *how*. Thank you so very much! I seriously appreciate your help!

answered 2 years ago  by ![danielmesser@mcldaz.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_danielmesser@mcldaz.org20180523162210.jpg)  danielmesser@mcldaz.org

- <a id="acceptAnswer_441"></a>[Accept as answer](#)

<a id="commentAns_441"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Block requests on expired patrons?](https://iii.rightanswers.com/portal/controller/view/discussion/416?)
- [Does anyone have a SQL to find out hold information on integrated e-content?](https://iii.rightanswers.com/portal/controller/view/discussion/684?)
- [In a consortium of 8 libraries, Is it possible to limit either a collection or material type by both the patron's home library and their hold pickup to branches within that library?](https://iii.rightanswers.com/portal/controller/view/discussion/262?)
- [SQL help for finding previous, previous patron on an item](https://iii.rightanswers.com/portal/controller/view/discussion/105?)
- [SQL or Scoping for Items Withdrawn NOT linked to patron fee records](https://iii.rightanswers.com/portal/controller/view/discussion/525?)

### Related Solutions

- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [Hold Processing SQL jobs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930920183263)
- [Can we suspend the hold option for specific titles?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930255045537)
- [Search patron reading history within the staff client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930708077324)
- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)