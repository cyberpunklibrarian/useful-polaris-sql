We would like to identify Bib records that have the same ISBN numbers across multiple Bibs. - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# We would like to identify Bib records that have the same ISBN numbers across multiple Bibs.

0

123 views

I'm looking for a SQL Query that would output a list of BibliographicRecordID's where there is a common ISBN in multiple Bib's.  We are having issues with Bib imports where there is more than one possible Bib and polaris not being able to choose which one it should attach a 970 tag for orders.

asked 3 years ago  by <img width="18" height="18" src="../../_resources/7e439084695f484e919ec6608d0919b1.jpg"/>  rhelwig@flls.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 3 years ago

<a id="upVoteAns_172"></a>[](#)

0

<a id="downVoteAns_172"></a>[](#)

Answer Accepted

Hi Rex.

This query uses the normalized ISBN string from the BibliographicISBNIndex table. I think it will do what you want. (I included the author and title as a way of double-checking the query, but you can remove those elements if you don't need them.) "Distinct" is in there two times because leaving either one out results in duplicate rows if a bib has the same ISBN two or more times.

Hope this helps,

JT

Select distinct br.BibliographicRecordID, br.BrowseAuthor, br.BrowseTitle, isbn.ISBNString from polaris.polaris.bibliographicrecords br with (nolock)
join Polaris.Polaris.BibliographicISBNIndex isbn (nolock)
on (br.BibliographicRecordID = isbn.BibliographicRecordID)
join
(Select isi.ISBNString, count(distinct isi.BibliographicRecordID) as Bibcount from Polaris.Polaris.BibliographicISBNIndex isi with (nolock)
group by isi.ISBNString) t1
on (isbn.ISBNString = t1.ISBNString)
where t1.Bibcount > 1
order by isbn.ISBNString

answered 3 years ago  by <img width="18" height="18" src="../../_resources/7e439084695f484e919ec6608d0919b1.jpg"/>  jwhitfield@aclib.us

<a id="commentAns_172"></a>[Add a Comment](#)

<a id="upVoteAns_174"></a>[](#)

0

<a id="downVoteAns_174"></a>[](#)

Thank you.  This does exactly what I am looking for.

answered 3 years ago  by <img width="18" height="18" src="../../_resources/7e439084695f484e919ec6608d0919b1.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_174"></a>[Accept as answer](#)

<a id="commentAns_174"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Use Item Record Call Numbers to Bulk Update Bib MARC Tag](https://iii.rightanswers.com/portal/controller/view/discussion/235?)
- [Can you change a tag in a bib record in Leap?](https://iii.rightanswers.com/portal/controller/view/discussion/1055?)
- [SQL for Bibs lacking local call #](https://iii.rightanswers.com/portal/controller/view/discussion/605?)
- [Finding new and replaced bibs in Polaris between certain dates](https://iii.rightanswers.com/portal/controller/view/discussion/871?)
- [How should I be entering call number info in the 092 to make the subfields correctly correspond with item record call number fields?](https://iii.rightanswers.com/portal/controller/view/discussion/313?)

### Related Solutions

- [Bib Call Number is not automatically copying to Item Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930769128110)
- [How does "Use template values instead of these (if available)" work?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170524120618032)
- [Copying bib call number to item record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930712730749)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Default Bib Records to MARC 21 Editor](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930603446551)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)