[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# How are libraries clearing overdue eContent, when the system skips them in the nightly process?

0

74 views

Frequently, the over night process that checks in ebooks from Overdrive, will miss some ebooks  and they then go overdue.

They are never checked in after other runs of the nightly.  We only know about them when the customer notices on a overdue notice.

When we find them, we can't check them in. Have any of you dealt with this problem? Are you able to check your's in?

&nbsp;

asked 3 years ago by ![carl.ratz@phoenix.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_carl.ratz@phoenix.gov20170808200600.png) carl.ratz@phoenix.gov

[checkin](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=251#t=commResults) [ebook](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=431#t=commResults) [econtent](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=410#t=commResults) [late](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=432#t=commResults) [overdrive](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=30#t=commResults) [overdue](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=253#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 3 years ago

<a id="upVoteAns_1127"></a>[](#)

0

<a id="downVoteAns_1127"></a>[](#)

Hi Carl- 

This happens to us occasionally too, so I've started running a quick search for overdue econtent items once a month. Hopefully, that way I can catch any before they become long overdues.

When an overdue checkout does show up, I'll delete the row in the ItemCheckouts table through SSMS.  If you don't have access or aren't comfortable doing it via SQL, I'm sure you're Site Manager would be happy to help.

&nbsp;

Let me know if you have any other questions.

&nbsp;

\-Matt

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_3fd350db8fb44cb58575badb825cd72c.jpg"/> mhammermeister@pinnaclelibraries.org

- <a id="acceptAnswer_1127"></a>[Accept as answer](#)

<a id="commentAns_1127"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Moving items to lost without billing](https://iii.rightanswers.com/portal/controller/view/discussion/1384?)
- [What causes patron's reading history to clear unexpectedly?](https://iii.rightanswers.com/portal/controller/view/discussion/249?)
- [How is everyone handling bills and overdues once you've restarted your notices?](https://iii.rightanswers.com/portal/controller/view/discussion/765?)
- [I would like help calculating 'Overdue Duration', that is, the number of days an item was overdue for each overdue occurrence.](https://iii.rightanswers.com/portal/controller/view/discussion/1340?)
- [Can you auto wave fines at check in?](https://iii.rightanswers.com/portal/controller/view/discussion/487?)

### Related Solutions

- [How Do Almost Overdue Reminder Notices and Autorenew Interact with Library Closed Dates?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181218172047064)
- [Replacement fines were not automatically waived after overdue paid](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930589018439)
- [How are overdue fines calculated?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930310567045)
- [Tip for checking the due date on returned overdue materials](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930463638326)
- [No holds going pending at new branch](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930674074347)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)