Is there a way via SQL to include the contents of 508 and 511 MARC fields in addition to genre subject headings? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Is there a way via SQL to include the contents of 508 and 511 MARC fields in addition to genre subject headings?

0

26 views

Hello,

Is there a way via SQL to include the contents of 508 and 511 MARC fields, in addition to genre subject headings, in a report's output? One of the member libraries would like a report that includes cast, producer, and directors in addition to genre subjects and quite a bit of other item information for their feature film DVD collection, so I thought being able to display the 508 and 511 fields (if present) might work better than displaying 700 fields. However, if that is doable, I am not quite sure how to create the SQL query. Thanks, much, for any advice or examples.

Alison Hoffman

Monarch Library System

asked 8 months ago  by <img width="18" height="18" src="../../_resources/fe706e51d8a242338619d1694f8dea60.jpg"/>  ahoffman@monarchlibraries.org

[sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 6 months ago

<a id="upVoteAns_1070"></a>[](#)

0

<a id="downVoteAns_1070"></a>[](#)

 This will probably need some tweaking but should help get you on the right path:

`select br.BibliographicRecordID`
`,bs_genre.Data [Genre]`
`,bs_508.SubfieldSequence [508sequence]`
`,bs_508.Subfield [508subfield]`
`,bs_508.Data [508value]`
`,bs_511.SubfieldSequence [511sequence]`
`,bs_511.Subfield [511subfield]`
`,bs_511.Data [511data]`
`from Polaris.Polaris.BibliographicRecords br`
`join Polaris.Polaris.BibliographicTags bt_genre`
`on bt_genre.BibliographicRecordID = br.BibliographicRecordID and bt_genre.TagNumber = 650 -- genre tag number`
`join polaris.polaris.BibliographicSubfields bs_genre`
`on bs_genre.BibliographicTagID = bt_genre.BibliographicTagID and bs_genre.Subfield = 'a' and bs_genre.SubfieldSequence = 1 -- genre subfield`
`left join Polaris.polaris.BibliographicTags bt_508`
`on bt_508.BibliographicRecordID = br.BibliographicRecordID and bt_508.TagNumber = 508`
`left join Polaris.Polaris.BibliographicSubfields bs_508`
`on bs_508.BibliographicTagID = bt_508.BibliographicTagID`
`left join Polaris.Polaris.BibliographicTags bt_511`
`on bt_511.BibliographicRecordID = br.BibliographicRecordID and bt_511.TagNumber = 511`
`left join Polaris.Polaris.BibliographicSubfields bs_511`
`on bs_511.BibliographicTagID = bt_511.BibliographicTagID`
`where (bs_508.Data is not null or bs_511.Data is not null)`

answered 6 months ago  by <img width="18" height="18" src="../../_resources/fe706e51d8a242338619d1694f8dea60.jpg"/>  mfields@clcohio.org

- <a id="acceptAnswer_1070"></a>[Accept as answer](#)

<a id="commentAns_1070"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Do reporting by subject headings?](https://iii.rightanswers.com/portal/controller/view/discussion/1059?)
- [Simply Reports - formatting when comments fields are included.](https://iii.rightanswers.com/portal/controller/view/discussion/128?)
- [SQL for MARC tag search](https://iii.rightanswers.com/portal/controller/view/discussion/194?)
- [SQL for Bibs w/no good items but have holds - including item and patron info](https://iii.rightanswers.com/portal/controller/view/discussion/594?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)

### Related Solutions

- [What authority files are included in SkyRiver?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=190814155145933)
- [Outreach picklist generation](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930564049678)
- [What is the difference between "See" and "See Also"?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930297807799)
- [Adding a new UDF to patron registration](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930740605846)
- [Link to download integrated content (Overdrive, Axis 360, 3M Cloud)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930286574983)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)