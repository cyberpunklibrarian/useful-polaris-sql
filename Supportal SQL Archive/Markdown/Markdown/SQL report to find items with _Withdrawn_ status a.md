SQL report to find items with "Withdrawn" status and NOT in a record set - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL report to find items with "Withdrawn" status and NOT in a record set

0

78 views

I'm wondering if there is a SQL to find items that have a circ status of Withdrawn but are not in a record set?

asked 3 years ago  by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg)  sbills@lpld.lib.in.us

[sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 3 years ago

<a id="upVoteAns_382"></a>[](#)

1

<a id="downVoteAns_382"></a>[](#)

Answer Accepted

If you meant an item record set, this is fairly easy:

SELECT ItemRecordID
FROM CircItemRecords with (NOLOCK)
WHERE ItemStatusID = 11
AND ItemRecordID NOT IN (
SELECT DISTINCT ItemRecordID
FROM ItemRecordSets with (NOLOCK)
)

If you meant that the attached bib must not be in a recordset, please let me know--that's slightly more complicated, but still doable.

answered 3 years ago  by <img width="18" height="18" src="../../_resources/af24db1a2e9746b780bc42a70af3e969.jpg"/>  jjack@aclib.us

I did mean item record sets, thank you!! This worked perfectly. :-) Thank you, John!

— sbills@lpld.lib.in.us 3 years ago

You're welcome!

— jjack@aclib.us 3 years ago

<a id="commentAns_382"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Searching for Items that have not circ'd in 2 years ex. DVD's](https://iii.rightanswers.com/portal/controller/view/discussion/1053?)
- [Report of Lost and Paid Items](https://iii.rightanswers.com/portal/controller/view/discussion/331?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)

### Related Solutions

- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [Item Circulation Statistics Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930404036798)
- [Item Circulation by Statistical Code](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930592909894)
- [Unable to Use Report Builder (in SQL Server Reporting Services)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930942443296)
- [Claimed item does not appear in the Claimed Items report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930283717537)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)