"you saved" year end report - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# "you saved" year end report

0

146 views

 We would like to get a total "You Saved" $$ for the library system for the current fiscal year OR the average "You Saved" $$ per patron for the fiscal year.  Any thoughts?

asked 3 years ago  by <img width="18" height="18" src="../../_resources/fe65200f97b048c58a332e1ff4c9137f.jpg"/>  cmaxey@websterpl.org

["you saved"](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=31#t=commResults) [reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=32#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 3 years ago

<a id="upVoteAns_139"></a>[](#)

0

<a id="downVoteAns_139"></a>[](#)

Answer Accepted

How familiar are you with SQL?  I have something that works for our system, but your system's situation is likely different in one or more significant ways (in CollectionID if nothing else).  
caveats on this SQL below:

1.  If you didn't know already, you'll want to exclude activity on any OverDrive items because Polaris adds and removes those items willy-nilly; therefore any numbers dealing with them are unreliable
2.  It's possible that content from Axis360, 3M, and OverClick faces similar issues; they're not integrated into our catalog so I couldn't say.
3.  This SQL can undercount (undersum?) depending on your item retention policies--information disappears from the itemRecordHistory table once items are deleted, so that "you saved" value might be lost
4.  The "Through date" is the last date of checkout activity you want included
5.  Don't miss the comments below; you'll likely have to change parts of this to suit your own system's situation

SELECT DATEPART(yy,irh.TRansactionDate) AS 'Year', mat.Description AS 'Material type', sum(ird.Price) AS 'Value of Circs'
FROM polaris.polaris.ItemRecordHistory irh (NOLOCK)
JOINpolaris.polaris.CircItemRecords cir (NOLOCK)
ON cir.ItemRecordID = irh.ItemRecordID
JOINpolaris.polaris.ItemRecordDetails ird (NOLOCK)
ON cir.ItemRecordID = ird.ItemRecordID
JOINpolaris.polaris.MaterialTypes mat (NOLOCK)
ON mat.MaterialTypeID = cir.MaterialTypeID
WHERE irh.ActionTakenID IN (13,44,45,75,77,89,90,91)
*--you'll want to double-check that you're including what you want and not what you don't
--For us this ^ includes checkouts, excludes everything else including renewals*
AND mat.MaterialTypeID NOT IN (5,6,7,9,10)
*--you'll want to double-check that you're not excluding things you want to include*
AND cir.AssignedCollectionID NOT IN (22)
*--you'll want to double-check that you're not excluding things you want to include*
*--for us, this ^ is an easy way to exclude all downloadable content*
AND irh.TransactionDate BETWEEN @From AND @Through + ' 11:59:59 PM'
GROUP BY DATEPART(yy,irh.TRansactionDate), mat.Description
ORDER BY DATEPART(yy,irh.TRansactionDate), mat.Description

That SQL above gives sums; for an average per patron I'd be inclined to sum up the material types you want to include then divide by either total # patrons or total # active patrons (whichever number matches what you're looking for).

If you have any questions, please let me know.

answered 3 years ago  by <img width="18" height="18" src="../../_resources/fe65200f97b048c58a332e1ff4c9137f.jpg"/>  jjack@aclib.us

<a id="commentAns_139"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)