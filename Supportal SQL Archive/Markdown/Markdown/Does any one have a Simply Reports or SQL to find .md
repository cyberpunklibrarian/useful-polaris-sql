[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Does any one have a Simply Reports or SQL to find duplicate driver's license numbers (user defined field 1)?

0

32 views

I am looking for a report or a way to run a report to get a list of patrons with duplicate driver's license numbers.  In our patron registration, it is user defined field 1.  Any help would be appreciated!  (I am new to this and not a trained IT person, so I am learning as I go.)

asked 3 months ago by <img width="18" height="18" src="../_resources/default-avatar_f62c5d0a78ce4ff88f4bb5995bd0ec4d.jpg"/> hpriestley@oplib.org

[polaris](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=7#t=commResults) [sql patron](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=387#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 3 months ago

<a id="upVoteAns_1427"></a>[](#)

0

<a id="downVoteAns_1427"></a>[](#)

Thanks for your reply!  This didn't work for me using the Find Tool in the client and Leap.  I have put in a ticket requesting access through MS SSMS.  Maybe it will work that way?

answered 3 months ago by <img width="18" height="18" src="../_resources/default-avatar_f62c5d0a78ce4ff88f4bb5995bd0ec4d.jpg"/> hpriestley@oplib.org

- <a id="acceptAnswer_1427"></a>[Accept as answer](#)

<a id="commentAns_1427"></a>[Add a Comment](#)

<a id="upVoteAns_1426"></a>[](#)

0

<a id="downVoteAns_1426"></a>[](#)

You could try something like this:

```markup
select pr.patronid,pr2.patronid,pr.user1 from patronregistration pr with (nolock)
left join patronregistration pr2 with (nolock) on (pr.user1=pr2.user1 and pr.patronid!=pr2.patronid)
where pr.user1 is not null and pr2.user1 is not null
```

When I ran this I got a bunch of placeholders (like just the text "nys dl") so you would likely need to play around with adding conditions to the where clause (like *pr.user1 != 'nys dl'* or *pr.user1 not in ('nys dl','student id')*) to get a useful list.

If you want to run this in the find tool (for instance to create a record set), just limit the columns to patronid, like this:

```markup
select pr.patronid from patronregistration pr with (nolock)
left join patronregistration pr2 with (nolock) on (pr.user1=pr2.user1 and pr.patronid!=pr2.patronid)
where pr.user1 is not null and pr2.user1 is not null
```

answered 3 months ago by <img width="18" height="18" src="../_resources/default-avatar_f62c5d0a78ce4ff88f4bb5995bd0ec4d.jpg"/> ebrondermajor@onlib.org

- <a id="acceptAnswer_1426"></a>[Accept as answer](#)

<a id="commentAns_1426"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Simply Reports - formatting when comments fields are included.](https://iii.rightanswers.com/portal/controller/view/discussion/128?)
- [SQL or Simply Report Active Patrons](https://iii.rightanswers.com/portal/controller/view/discussion/1164?)
- [Do reporting by subject headings?](https://iii.rightanswers.com/portal/controller/view/discussion/1059?)
- [Simply Reports Discrepancies](https://iii.rightanswers.com/portal/controller/view/discussion/960?)

### Related Solutions

- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [Saved report does not include all the data / rows expected](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930269832140)
- [Run SimplyReports Scheduled Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930277327637)
- [Question about a saved SimplyReports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930457380859)
- [Question about published SimplyReports reports](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989986518)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)