SQL for Lost and Paid Items - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for Lost and Paid Items

0

63 views

I've been using the following SQL for over a year now.  It would return items that had been lost and paid.  The last couple times I've run it I'm getting not only lost items that have been paid but items on the patron status view -- account page that have a replacement cost that has not been paid.  Unfortunately, I'll get 237 items and need to go through each one to find the paid items.  Did something go wrong with the SQL statement I've been copying and pasting?

SELECT distinct cir.itemrecordid

from polaris.polaris.CircItemRecords cir with(nolock)

inner join polaris.polaris.BibliographicRecords bib with(nolock)

on cir.AssociatedBibRecordID = bib.BibliographicRecordID

WHERE ItemStatusID = 7

and ItemRecordID not in (select ItemRecordID AS RecordID from polaris.polaris.PatronLostItems)

and cir.ItemStatusDate < '20200109'

We did upgrade to 6.3.2292 in November 2019. 

Thanks in advance,

Anne Krulik

asked 1 year ago  by <img width="18" height="18" src="../../_resources/ae79f2aa6c244c1fac17e7072174fdb3.jpg"/>  akrulik@bcls.lib.nj.us

[lost and paid](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=321#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 1 year ago

<a id="upVoteAns_816"></a>[](#)

0

<a id="downVoteAns_816"></a>[](#)

Hi Anne.

We have approached this from the other direction, looking for paid bills and getting a list of linked items.

Here is the query (with a minor tweak or two) we use for our Items from Paid Bills report. We do not accept partial payments, so the transaction amount should match the item price. You could add a column for the item price if you want to compare.

We use a separate report to populate an item record set with the linked items, so we don't have to search for them. If you want to use this query in the Find Tool, you should be able to change the select statement to return only ir.ItemRecordID and remove the order by statement.

Hope this helps.

JT

select frc.FeeDescription as Reason,cast(ac.TxnAmount as decimal(20,2)) as TxnAmount,
ac.TxnDate,ir.Barcode as ItemBarcode,
mt.Description as MaterialTypeDescription,
ist.Description as ItemStatusDescr,
br.BrowseTitle,
p.Barcode as PatronBarcode
from Polaris.Polaris.PatronAccount ac with (nolock)
inner join Polaris.Polaris.Patrons p with (nolock) on (p.PatronID = ac.PatronID)
inner join Polaris.Polaris.PatronRegistration pr with (nolock) on (p.PatronID = pr.PatronID)
inner join Polaris.Polaris.Organizations gov with (nolock) on (ac.OrganizationID = gov.OrganizationID)
join Polaris.Polaris.FeeReasonCodes frc with (nolock) on (ac.FeeReasonCodeID = frc.FeeReasonCodeID)
join Polaris.Polaris.ItemRecords ir with (nolock) on (ac.ItemRecordID = ir.ItemRecordID and ir.itemstatusid = 7)
--This join gets Lost items only. Delete the part after "and" to get all items.
join Polaris.Polaris.BibliographicRecords br with (nolock) on (ir.AssociatedBibRecordID = br.BibliographicRecordID)
join Polaris.Polaris.MaterialTypes mt with (nolock) on (ir.MaterialTypeID = mt.MaterialTypeID)
join Polaris.Polaris.ItemStatuses ist with (nolock) on (ir.ItemStatusID = ist.ItemStatusID)

where ac.TxnID in
(select distinct TxnID from Polaris.Polaris.PatronAccount with (nolock)
where TxnCodeID not in (1,4,8,9) and txnamount > 0 ) 
and ac.FeeReasonCodeID in (9,8,-1,7)  and ac.PaymentMethodID in (11,14,12,4,5,13,3,1,2,17,6,15) 
and ac.TxnDate between dateadd(dd,-7,getdate()) and getdate() 

order by TxnDate desc

answered 1 year ago  by <img width="18" height="18" src="../../_resources/ae79f2aa6c244c1fac17e7072174fdb3.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_816"></a>[Accept as answer](#)

<a id="commentAns_816"></a>[Add a Comment](#)

<a id="upVoteAns_806"></a>[](#)

0

<a id="downVoteAns_806"></a>[](#)

I would love to see how this works! I've been trying to find a reliable query that pulls back just Lost items that have been paid so we can remove them but my results are always inaccurate.

answered 1 year ago  by <img width="18" height="18" src="../../_resources/ae79f2aa6c244c1fac17e7072174fdb3.jpg"/>  abrookins@cvlga.org

- <a id="acceptAnswer_806"></a>[Accept as answer](#)

<a id="commentAns_806"></a>[Add a Comment](#)

<a id="upVoteAns_817"></a>[](#)

-1

<a id="downVoteAns_817"></a>[](#)

Thanks, JT.  I ran the following SQL in the item record Find Tool and it pulled paid lost items.  It also pulled lost items that were only partially paid :(  We are not supposed to accept partial payments either.

select ir.ItemRecordID
from Polaris.Polaris.PatronAccount ac with (nolock)
inner join Polaris.Polaris.Patrons p with (nolock) on (p.PatronID = ac.PatronID)
inner join Polaris.Polaris.PatronRegistration pr with (nolock) on (p.PatronID = pr.PatronID)
inner join Polaris.Polaris.Organizations gov with (nolock) on (ac.OrganizationID = gov.OrganizationID)
join Polaris.Polaris.FeeReasonCodes frc with (nolock) on (ac.FeeReasonCodeID = frc.FeeReasonCodeID)
join Polaris.Polaris.ItemRecords ir with (nolock) on (ac.ItemRecordID = ir.ItemRecordID and ir.itemstatusid = 7)
join Polaris.Polaris.BibliographicRecords br with (nolock) on (ir.AssociatedBibRecordID = br.BibliographicRecordID)
join Polaris.Polaris.MaterialTypes mt with (nolock) on (ir.MaterialTypeID = mt.MaterialTypeID)
join Polaris.Polaris.ItemStatuses ist with (nolock) on (ir.ItemStatusID = ist.ItemStatusID)

where ac.TxnID in
(select distinct TxnID from Polaris.Polaris.PatronAccount with (nolock)
where TxnCodeID not in (1,4,8,9) and txnamount > 0 ) 
and ac.FeeReasonCodeID in (9,8,-1,7)  and ac.PaymentMethodID in (11,14,12,4,5,13,3,1,2,17,6,15) 

answered 1 year ago  by <img width="18" height="18" src="../../_resources/ae79f2aa6c244c1fac17e7072174fdb3.jpg"/>  akrulik@bcls.lib.nj.us

- <a id="acceptAnswer_817"></a>[Accept as answer](#)

Hi again, Anne.

Here is a tweak that should pull item records when the transaction amount equals the item price. You can go the other way (finding under or overpayments) by changing the "=" in "pa.TxnAmount = it.Price" to "&lt;>",  or "<", or "&gt;" (in the second to last line of the query).

Hope this helps.

JT

select ir.ItemRecordID
from Polaris.Polaris.PatronAccount ac with (nolock)
inner join Polaris.Polaris.Patrons p with (nolock) on (p.PatronID = ac.PatronID)
inner join Polaris.Polaris.PatronRegistration pr with (nolock) on (p.PatronID = pr.PatronID)
inner join Polaris.Polaris.Organizations gov with (nolock) on (ac.OrganizationID = gov.OrganizationID)
join Polaris.Polaris.FeeReasonCodes frc with (nolock) on (ac.FeeReasonCodeID = frc.FeeReasonCodeID)
join Polaris.Polaris.ItemRecords ir with (nolock) on (ac.ItemRecordID = ir.ItemRecordID and ir.itemstatusid = 7)
join Polaris.Polaris.BibliographicRecords br with (nolock) on (ir.AssociatedBibRecordID = br.BibliographicRecordID)
join Polaris.Polaris.MaterialTypes mt with (nolock) on (ir.MaterialTypeID = mt.MaterialTypeID)
join Polaris.Polaris.ItemStatuses ist with (nolock) on (ir.ItemStatusID = ist.ItemStatusID)
where ac.TxnID in
(select distinct TxnID from Polaris.Polaris.PatronAccount pa with (nolock)
join Polaris.Polaris.ItemRecords it (nolock)
on (pa.ItemRecordID = it.ItemRecordID)
where pa.TxnCodeID not in (1,4,8,9) and pa.txnamount > 0
and pa.TxnAmount = it.Price) 
and ac.FeeReasonCodeID in (9,8,-1,7)  and ac.PaymentMethodID in (11,14,12,4,5,13,3,1,2,17,6,15) 

— jwhitfield@aclib.us 1 year ago

This worked great, JT!!  Thank you.

— akrulik@bcls.lib.nj.us 1 year ago

<a id="commentAns_817"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Report of Lost and Paid Items](https://iii.rightanswers.com/portal/controller/view/discussion/331?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [SQL for items returned late by collection](https://iii.rightanswers.com/portal/controller/view/discussion/718?)
- [SQL for Item records from PO/FY/Branch](https://iii.rightanswers.com/portal/controller/view/discussion/326?)
- [SQL for Bibs w/no good items but have holds - including item and patron info](https://iii.rightanswers.com/portal/controller/view/discussion/594?)

### Related Solutions

- [Which lost item recovery settings are used for partially paid replacement costs?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170523113239517)
- [Paid lost item still displaying as unpaid](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930877186258)
- [Items converting to Lost status](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930282614657)
- [Default pricing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930543557097)
- [Items not moving to Lost status](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930564239028)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)