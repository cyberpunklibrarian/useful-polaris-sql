[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# SQL for waiving certain fines

0

278 views

Hi,

We're exploring options for eliminating overdue fines for our young patrons, and are considering waiving all existing fines.  Has anyone done such a thing using SQL?  I see there's an enhancement request for this as a built-in option, but hopefully there's another way.

In our previous ILS, one could perform a transaction in the client and then go to the logs, find that transaction and set up a file with mimicked transactions. Is there a similar option in Polaris?  That would make my week!

Thanks,

\--Kara

Santa Monica Public Library

kara.steiniger@smgov.net

&nbsp;

&nbsp;

&nbsp;

&nbsp;

asked 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_c822ae7f09744a0997e5ffb82fc9b3a7.jpg"/> kara.steiniger@smgov.net

[sql waive fines](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=37#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 6 years ago

<a id="upVoteAns_181"></a>[](#)

0

<a id="downVoteAns_181"></a>[](#)

I have contacted Polaris to get a quote for them to create a stored procedure that would waive all fines incurred before a specified date.  Are there any other libraries that would like to split the cost on that with us?

Bill Taylor

Western Md. Regional Library

wtaylor@washcolibrary.org

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_c822ae7f09744a0997e5ffb82fc9b3a7.jpg"/> wtaylor@washcolibrary.org

- <a id="acceptAnswer_181"></a>[Accept as answer](#)

<a id="commentAns_181"></a>[Add a Comment](#)

<a id="upVoteAns_171"></a>[](#)

0

<a id="downVoteAns_171"></a>[](#)

I also checked with Polaris staff who did not recommend trying to replicate a waive transaction en masse via SQL, in the interest of database integrity.  In looking at the bright side, we've determined that there are some benefits to waiving fines by individual.  Prior to the switch to fine-free, we'll be offering youth options to read-away fines, and/or donate food in exchange.  Waiving in-person is more likely to be appreciated and generate good-will than a mass-transaction.  We haven't worked out all the details yet, and at some point may create record sets for staff to go through and waive fines, so fines won't be an obstacle to coming back to the Library.   It turns out that the manual waives are preferable to our Director than the bulk transaction.

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_c822ae7f09744a0997e5ffb82fc9b3a7.jpg"/> kara.steiniger@smgov.net

- <a id="acceptAnswer_171"></a>[Accept as answer](#)

<a id="commentAns_171"></a>[Add a Comment](#)

<a id="upVoteAns_170"></a>[](#)

0

<a id="downVoteAns_170"></a>[](#)

Same here!  We'd like to waive all fines older than 2 years.  I don't mind doing a little updating of tables via SQL, but just basic stuff - I wouldn't feel comfortable with something this complex - there are a lot of tables involved with patron accounts!   I asked Gail Maitland at Polaris support, who confirmed that they don't recommend making changes to patron accounts via SQL.  She said III staff could probably do it as a project for a fee.  Any suggestions would be welcome!

Bill Taylor, Western Md. Regional Library

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_c822ae7f09744a0997e5ffb82fc9b3a7.jpg"/> wtaylor@washcolibrary.org

- <a id="acceptAnswer_170"></a>[Accept as answer](#)

<a id="commentAns_170"></a>[Add a Comment](#)

<a id="upVoteAns_162"></a>[](#)

0

<a id="downVoteAns_162"></a>[](#)

Our library is investigating this very thing as well and we would be very grateful to hear from anyone who has experience with making this bulk change in the Polaris database.

&nbsp;

Thanks!

&nbsp;

Taylor Buchheit @ Eugene Public Library

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_c822ae7f09744a0997e5ffb82fc9b3a7.jpg"/> taylor.j.buchheit@ci.eugene.or.us

- <a id="acceptAnswer_162"></a>[Accept as answer](#)

<a id="commentAns_162"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Fees waived by username](https://iii.rightanswers.com/portal/controller/view/discussion/1123?)
- [Deleting fines/fees](https://iii.rightanswers.com/portal/controller/view/discussion/1077?)
- [Do you use fine codes for items not related to a material type?](https://iii.rightanswers.com/portal/controller/view/discussion/359?)
- [SQL for Range of Closed Dates](https://iii.rightanswers.com/portal/controller/view/discussion/716?)
- [Deleting Material Types using SQL](https://iii.rightanswers.com/portal/controller/view/discussion/323?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Where are the waive and charge free text notes stored in the database?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930334007926)
- [Replacement fines were not automatically waived after overdue paid](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930589018439)
- [Find patrons with a certain amount of existing fines/fees](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181016133058447)
- [Searching for notes on patron record from Waived Fines report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930960439867)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)