[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Does anyone have an SQL that will find the top 50 Adult Fiction titles requested (placed on hold) by patrons registered at certain branch in the past year?

0

69 views

Find the 50 most requested titles that were placed on hold by patrons registered at certain branch in the past year? With results showing branch patron registered at, collection,  author, title, and number of requests.

asked 9 months ago by <img width="18" height="18" src="../_resources/default-avatar_c35c61bf3b134ed78992e8e8ff1efc32.jpg"/> scole@slcolibrary.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 9 months ago

<a id="upVoteAns_1409"></a>[](#)

0

<a id="downVoteAns_1409"></a>[](#)

Answer Accepted

The attached script will give you what you need.

"Collection" can be weird to include, since it's applied individually on items, so there can be multiple collection values under the same title/bib.  For instance when I run the script I see 5-6 collections for each title across our consortium!  I set the script up so it will identify the top 50 titles before adding the collections in, so duplicate collection rows won't affect the number of title results, but I also noted the lines you can comment out if you need to remove the collections.

answered 9 months ago by <img width="18" height="18" src="../_resources/default-avatar_c35c61bf3b134ed78992e8e8ff1efc32.jpg"/> jtenter@sasklibraries.ca

- [Top 50 Holds by Patron Branch (2023-11-09).txt](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/1359?fileName=1359-1409_Top+50+Holds+by+Patron+Branch+%282023-11-09%29.txt "Top 50 Holds by Patron Branch (2023-11-09).txt")

<a id="commentAns_1409"></a>[Add a Comment](#)

<a id="upVoteAns_1410"></a>[](#)

0

<a id="downVoteAns_1410"></a>[](#)

This is fantastic!! I really appreciate it. Thank you!

answered 9 months ago by <img width="18" height="18" src="../_resources/default-avatar_c35c61bf3b134ed78992e8e8ff1efc32.jpg"/> scole@slcolibrary.org

- <a id="acceptAnswer_1410"></a>[Accept as answer](#)

<a id="commentAns_1410"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)
- [Is there a way to set staff placed holds for patrons to default to patron branch instead of branch location when hold is placed?](https://iii.rightanswers.com/portal/controller/view/discussion/799?)
- [Does anyone have an SQL for a request ratio?](https://iii.rightanswers.com/portal/controller/view/discussion/394?)
- [Identifying a count of patrons, by registered library, whose only activity over a period of time is OverDrive checkout through Polaris integration?](https://iii.rightanswers.com/portal/controller/view/discussion/1044?)
- [Besides Quipu, does any know of any other products that integrate with Polaris, allows patrons to self-register for a library card online, and verifies and standardizes the address information entered as well as provides name and residency verification?](https://iii.rightanswers.com/portal/controller/view/discussion/253?)

### Related Solutions

- [Statistics for registered patrons](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930383515616)
- [New patrons identified as a duplicate / new patrons not able to register on the PAC](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930681961607)
- [Cannot place holds on materials at the new branch](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930383002391)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Re-routing holds during brief branch closure](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930587478425)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)