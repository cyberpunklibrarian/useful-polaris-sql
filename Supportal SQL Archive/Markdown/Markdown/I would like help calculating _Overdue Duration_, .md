[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# I would like help calculating 'Overdue Duration', that is, the number of days an item was overdue for each overdue occurrence.

0

47 views

&nbsp;

I had planned to subtract the due date from actual check-in date to return number of days the item was overdue, for each overdue occurrence. If  an item is not yet returned,use current date.  I can not find DueDate or an obvious way to derive DueDate. Any suggestions? 

For context, this is part of an analysis of frequency and duration of overdue items. The goal is to count overdue frequency and duration, aggregating duration statistics, to provide 'typical' length of overdue periods for the library

&nbsp;

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_9258dcc3f56f49828125549c9369410a.jpg"/> rbeverungen@wmrl.info

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_1391"></a>[](#)

0

<a id="downVoteAns_1391"></a>[](#)

I believe something about the loan length was added in one of the Polaris upgrades to the Transaction database. One of the sql gurus on the IUG forums might be able to help if you want to post over there: https://forum.innovativeusers.org/c/polaris/8

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_9258dcc3f56f49828125549c9369410a.jpg"/> wosborn@clcohio.org

- <a id="acceptAnswer_1391"></a>[Accept as answer](#)

<a id="commentAns_1391"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Moving items to lost without billing](https://iii.rightanswers.com/portal/controller/view/discussion/1384?)
- [How is everyone handling bills and overdues once you've restarted your notices?](https://iii.rightanswers.com/portal/controller/view/discussion/765?)
- [How are libraries clearing overdue eContent, when the system skips them in the nightly process?](https://iii.rightanswers.com/portal/controller/view/discussion/1019?)
- [Can you auto wave fines at check in?](https://iii.rightanswers.com/portal/controller/view/discussion/487?)
- [Can anyone help me with a SQL for how many items are checked in daily?](https://iii.rightanswers.com/portal/controller/view/discussion/1185?)

### Related Solutions

- [How are overdue fines calculated?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930310567045)
- [What will happen if you give an item a special loan due date for the current date?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930416353137)
- [Long Overdue Item report shows thousands of items](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930725698901)
- [How Do Almost Overdue Reminder Notices and Autorenew Interact with Library Closed Dates?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181218172047064)
- [Replacement fines were not automatically waived after overdue paid](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930589018439)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)