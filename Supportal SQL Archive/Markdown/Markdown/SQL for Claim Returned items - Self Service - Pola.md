SQL for Claim Returned items - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL for Claim Returned items

0

46 views

Does anyone have a SQL for patrons (barcode and name) from a particular branch who have items Claim Returned or Claim Never Had, along with the item name/barcode, regardless of assigned branch. Basically, Branch A wants a list of all Branch A patrons with Claims (returned or never had), and what items they Claimed, whether the item belonged to Branch A or Branches B through N. When I try to run an Item List report in Simply Reports, I can get Patron Barcode for Claimed Item and Patron Full Name for Claimed Item, but it won't give me the Patron Branch (the branch column comes up empty).

asked 11 months ago  by <img width="18" height="18" src="../../_resources/6e983ef2bc394d48953612741c9aa842.jpg"/>  musack@bcls.lib.nj.us

That was exactly what I was looking for ... thank you!!

— musack@bcls.lib.nj.us 11 months ago

You're welcome!  Glad to help.

— jjack@aclib.us 11 months ago

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 11 months ago

<a id="upVoteAns_1003"></a>[](#)

0

<a id="downVoteAns_1003"></a>[](#)

What do I put in the second to last line where it has (@PatronBranch)? Branch ID?

answered 11 months ago  by <img width="18" height="18" src="../../_resources/6e983ef2bc394d48953612741c9aa842.jpg"/>  musack@bcls.lib.nj.us

- <a id="acceptAnswer_1003"></a>[Accept as answer](#)

Sorry, yes.  Whatever the branch ID is that you're looking for, e.g. *and p.OrganizationID in (3) *

— jjack@aclib.us 11 months ago

<a id="commentAns_1003"></a>[Add a Comment](#)

<a id="upVoteAns_1002"></a>[](#)

0

<a id="downVoteAns_1002"></a>[](#)

The SQL on that report is trying to go through ItemCheckouts to get at patron info, but it looks like items get moved out of ItemCheckouts into PatronClaims once they have been claimed.

This should work.  It uses "registered at" to determine what a branch's patrons are:

select ird.CallNumber,br.BrowseTitle,cir.Barcode as ItemBarcode,p.Barcode as PatronBarcode,pr.PatronFullName 
from Polaris.CircItemRecords cir with (nolock)
inner join Polaris.ItemRecordDetails ird with (nolock)
on (cir.ItemRecordID = ird.ItemRecordID)
inner join Polaris.BibliographicRecords br with (nolock)
on (cir.AssociatedBibRecordID = br.BibliographicRecordID)
left join Polaris.PatronClaims pc with (nolock)
on (cir.ItemRecordID = pc.ItemRecordID)
left join Polaris.Patrons p with (nolock)
on (pc.PatronID = p.PatronID)
left join Polaris.PatronRegistration pr with (nolock)
on (pr.PatronID = pc.PatronID)
where cir.ItemStatusID in (8,9)
and p.OrganizationID in (@PatronBranch) 
order by ird.CallNumber asc,br.SortTitle asc

If that's not what you're looking for, please let me know.

answered 11 months ago  by <img width="18" height="18" src="../../_resources/6e983ef2bc394d48953612741c9aa842.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_1002"></a>[Accept as answer](#)

<a id="commentAns_1002"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [I need a SQL for phatom lost item count attached to a patron's record.](https://iii.rightanswers.com/portal/controller/view/discussion/839?)
- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)
- [Is there an SQL to change item level holds to bib level holds?](https://iii.rightanswers.com/portal/controller/view/discussion/407?)
- [Return Deposit on patron account in Leap](https://iii.rightanswers.com/portal/controller/view/discussion/645?)
- [Reports for Floating Items](https://iii.rightanswers.com/portal/controller/view/discussion/62?)

### Related Solutions

- [Item displaying as Lost/Accruing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930784833630)
- [What happens when old Lost items are returned?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180907110226360)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [INN-Reach: Items that are transferred back from the branches to Main are not auto-returning when checked in](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930314735486)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)