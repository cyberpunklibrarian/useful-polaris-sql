[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL - 650 subject

0

84 views

Hello.

I am looking to clean up some of our 650 subject headings and would like to run a report pulling from only our JE and JF juv collections, listing title, auth, pub date, call number, 650 subject, and ISBN.

Is there also a way to save this in Polaris as a record set or imprt the Bibs?

I can't seem to pull the tables together and thoguht I wouls ask the experts!

I appreciate any help in advance.

&nbsp;

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_5403ad13600c4115914feb5325e1d29b.jpg"/> brandon.williams@mesaaz.gov

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_1276"></a>[](#)

0

<a id="downVoteAns_1276"></a>[](#)

Here's something to start, I guess.  It returns everything you asked for, but a record can have multiople ISBNs and multiple subject headings.  This means the same title might show up several times (once for each isbn/subject combo). If the ISBN is only to be used as a record identifier (I didn't see you list Bib Control number), then I think you'd be better served by eliminating the ISBN.  This same query (limited to Juve Non Fic in my system) returned 1,255,989 lines, but using BibRecordID instead of ISBN reduced it to 479,500 results.

&nbsp;

\--With ISBN Info

SELECT br.BrowseTitle, br.BrowseAuthor, br.PublicationYear, bs1.Data AS \[ISBN\], bs2.Data AS \[Subject Heading\]  
FROM Polaris.Polaris.CircItemRecords AS \[cir\] WITH (NOLOCK)  
INNER JOIN Polaris.Polaris.BibliographicRecords AS \[br\] WITH (NOLOCK)  
ON cir.AssociatedBibRecordID = br.BibliographicRecordID  
INNER JOIN Polaris.Polaris.BibliographicTags AS \[bt1\] WITH (NOLOCK)  
ON br.BibliographicRecordID = bt1.BibliographicRecordID  
INNER JOIN Polaris.Polaris.BibliographicSubfields AS \[bs1\] WITH (NOLOCK)  
ON bt1.BibliographicTagID = bs1.BibliographicTagID AND bt1.TagNumber = '020'  
INNER JOIN Polaris.Polaris.BibliographicTags AS \[bt2\] WITH (NOLOCK)  
ON br.BibliographicRecordID = bt2.BibliographicRecordID  
INNER JOIN Polaris.Polaris.BibliographicSubfields AS \[bs2\] WITH (NOLOCK)  
ON bt2.BibliographicTagID = bs2.BibliographicTagID AND bt2.TagNumber = '650'  
INNER JOIN Polaris.Polaris.Collections AS \[c\] WITH (NOLOCK)  
ON cir.AssignedCollectionID = c.CollectionID  
WHERE c.Abbreviation = 'JNF'  
GROUP BY br.BrowseTitle, br.BrowseAuthor, br.PublicationYear, bs1.Data, bs2.Data

\--Without ISBN

SELECT br.BrowseTitle, br.BrowseAuthor, br.PublicationYear, br.BibliographicRecordID, bs2.Data AS \[Subject Heading\]  
FROM Polaris.Polaris.CircItemRecords AS \[cir\] WITH (NOLOCK)  
INNER JOIN Polaris.Polaris.BibliographicRecords AS \[br\] WITH (NOLOCK)  
ON cir.AssociatedBibRecordID = br.BibliographicRecordID  
INNER JOIN Polaris.Polaris.BibliographicTags AS \[bt2\] WITH (NOLOCK)  
ON br.BibliographicRecordID = bt2.BibliographicRecordID  
INNER JOIN Polaris.Polaris.BibliographicSubfields AS \[bs2\] WITH (NOLOCK)  
ON bt2.BibliographicTagID = bs2.BibliographicTagID AND bt2.TagNumber = '650'  
INNER JOIN Polaris.Polaris.Collections AS \[c\] WITH (NOLOCK)  
ON cir.AssignedCollectionID = c.CollectionID  
WHERE c.Abbreviation = 'JNF'  
GROUP BY br.BrowseTitle, br.BrowseAuthor, br.PublicationYear, br.BibliographicRecordID, bs2.Data

&nbsp;

From you're post, it sounds like you'd just be making a record set of all the bibs with a certain collection, so that's already something you can do with the find tool, so I didn't go anywhere with that via SQL.  THere's still a lot of possible confounding variables (example: multiple subfields in a signel 650), but I'm not sure what exactly your process is to be, so I don't know how best to address anything like that.

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_5403ad13600c4115914feb5325e1d29b.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_1276"></a>[Accept as answer](#)

<a id="commentAns_1276"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL top bib checkouts between 2 dates](https://iii.rightanswers.com/portal/controller/view/discussion/650?)
- [Is there a way via SQL to include the contents of 508 and 511 MARC fields in addition to genre subject headings?](https://iii.rightanswers.com/portal/controller/view/discussion/907?)
- [Do reporting by subject headings?](https://iii.rightanswers.com/portal/controller/view/discussion/1059?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)
- [SQL for changes to a patron record](https://iii.rightanswers.com/portal/controller/view/discussion/840?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Why when adding a 650 tag to a bib does the tag come up as invalid.](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181228145044629)
- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)
- [How can SQL jobs be accessed?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930442864574)
- [Editing the Advanced Searches in Vital](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=150911340571323)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)