[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Is there a way to find out when someone changed their PIN/Password?

0

160 views

Hello, all!

I've got a case where a customer is concerned that someone changed their PIN/Password at some point. I did some digging around in the Transactions database and I'm not really finding much, but my knowledge of the Transactions database is spotty compared to the main Polaris database. Does anyone know if there's a way to see if a PIN was changed and/or *when* a PIN might have been changed?

&nbsp;

Thanks so much!

asked 5 years ago by ![danielmesser@mcldaz.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_danielmesser@mcldaz.org20180523162210.jpg) danielmesser@mcldaz.org

[password](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=156#t=commResults) [patron account](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=85#t=commResults) [pin](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=157#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 5 years ago

<a id="upVoteAns_402"></a>[](#)

1

<a id="downVoteAns_402"></a>[](#)

Answer Accepted

You can only see that a patron record was modified, not which specific part was changed. I guess if you REALLY wanted to find out, you could (if you have backups) compare the password hash from past back ups and get an approximate time frame for when the change occurred.

&nbsp;

Trevor D  
\--  
Trevor Diamond  
Systems/UX Librarian  
MAIN (Morris Automated Information Network), Morristown NJ  
www.mainlib.org

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_baea0eafa7a940aa85982a868a6980ce.jpg"/> trevor.diamond@mainlib.org

<a id="commentAns_402"></a>[Add a Comment](#)

<a id="upVoteAns_403"></a>[](#)

0

<a id="downVoteAns_403"></a>[](#)

That's exactly what I thought. I knew that, at best, I could see *when* a change might have been made, but as to *what* was changed, I figured it probably wasn't called out in any meaningful way.

&nbsp;

Thanks, Trevor!

answered 5 years ago by ![danielmesser@mcldaz.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_danielmesser@mcldaz.org20180523162210.jpg) danielmesser@mcldaz.org

- <a id="acceptAnswer_403"></a>[Accept as answer](#)

<a id="commentAns_403"></a>[Add a Comment](#)

<a id="upVoteAns_401"></a>[](#)

0

<a id="downVoteAns_401"></a>[](#)

You can only see that a patron record was modified, not which specific part was changed. I guess if you REALLY wanted to find out, you could (if you have backups) compare the password hash from past back ups and get an approximate time frame for when the change occurred.

&nbsp;

Trevor D  
\--  
Trevor Diamond  
Systems/UX Librarian  
MAIN (Morris Automated Information Network), Morristown NJ  
www.mainlib.org

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_baea0eafa7a940aa85982a868a6980ce.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_401"></a>[Accept as answer](#)

<a id="commentAns_401"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Is there a way to set Polaris to change expiration dates and address checks for new cards based on the patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/1213?)
- [Making Changes to PolarisTransactions Databae](https://iii.rightanswers.com/portal/controller/view/discussion/104?)
- [Changing patron records from Regular Case to UPPERCASE](https://iii.rightanswers.com/portal/controller/view/discussion/1303?)
- [Has anyone who houses their own servers come up with a way fo have multifuction authenication for Polaris?](https://iii.rightanswers.com/portal/controller/view/discussion/981?)
- [Our library is extending our loan periods. Advice? Checklist?](https://iii.rightanswers.com/portal/controller/view/discussion/590?)

### Related Solutions

- [ExpressCheck patron password for login](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930688910047)
- [Patron password functionality in 5.1 SP1 and newer](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170505145937003)
- [Is there a way to change a certain Material Type from nonrenewable to renewable?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930422392480)
- [IUG 2016: It Doesn't Have to Be That Way Anymore - Clean up system codes to match the way your library runs today](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160406151714446)
- [System Changes Made by Innovative Staff](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160125092907532)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)