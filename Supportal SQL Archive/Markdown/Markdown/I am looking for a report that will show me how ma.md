I am looking for a report that will show me how many times we checked in items during a certain period.  - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# I am looking for a report that will show me how many times we checked in items during a certain period.

0

110 views

We are attempting to figure out how many items are turned in on time vs. being late with charges. I tried Simply reports, but could only find the ability to look up last checked-in time, but that only will count an item once during the period, vs total check-ins. 

Any help would be appreciated

Thank You

Cody Wassenberg

asked 2 years ago  by <img width="18" height="18" src="../../_resources/89ffb7cecd6c4c399510a3bc1a3976ab.jpg"/>  cwassen@manhattan.lib.ks.us

[polaris 6.0](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=72#t=commResults) [report](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=91#t=commResults) [simply reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=11#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_476"></a>[](#)

0

<a id="downVoteAns_476"></a>[](#)

Answer Accepted

Maybe the following SQL will help you with your query.  I use the following query to identify the 3 DVD material Types we have that were returned and how many of those were returned late.

--Total Check-ins during period

DECLARE @BeginDate datetime SET @BeginDate = '2019-01-01'
DECLARE @EndDate datetime SET @EndDate = '2019-01-22'
SELECT COUNT(TH.TransactionID) AS "Total Check-ins"
FROM PolarisTransactions.Polaris.TransactionHeaders TH
JOIN PolarisTransactions.Polaris.TransactionDetails TD
ON TD.TransactionID = TH.TransactionID
JOIN Polaris.Polaris.MaterialTypes MT
ON MT.MaterialTypeID = TD.numValue
WHERE TH.TransactionTypeID = 6002
AND TH.OrganizationID = 43  --OrgID comment out if not needed
AND TH.TranClientDate BETWEEN @BeginDate AND @EndDate
AND TD.TransactionSubTypeID = 4  --Used to Identify specific MaterialTypes list in following line
AND TD.numValue IN (12,13,14)  --MaterialTypeID's

--Total Overdue Check-ins during same period
SELECT COUNT(TH.TransactionID) AS "Total Late Check-ins"
FROM PolarisTransactions.Polaris.TransactionHeaders TH
JOIN PolarisTransactions.Polaris.TransactionDetails TD1
ON TD1.TransactionID = TH.TransactionID
JOIN PolarisTransactions.Polaris.TransactionDetails TD2
ON TD2.TransactionID = TH.TransactionID
JOIN Polaris.Polaris.MaterialTypes MT
ON MT.MaterialTypeID = TD1.numValue
WHERE TH.TransactionTypeID = 6002
AND TH.OrganizationID = 43  --OrgID comment out if not needed
AND TH.TranClientDate BETWEEN @BeginDate AND @EndDate
AND TD1.TransactionSubTypeID = 4  --Used to Identify specific MaterialTypes list in following line
AND TD1.numValue IN (12,13,14)  --MaterialTypeID's
AND TD2.TransactionSubTypeID = 127  --Identifies overdue check-ins

answered 2 years ago  by <img width="18" height="18" src="../../_resources/89ffb7cecd6c4c399510a3bc1a3976ab.jpg"/>  rhelwig@flls.org

<a id="commentAns_476"></a>[Add a Comment](#)

<a id="upVoteAns_482"></a>[](#)

0

<a id="downVoteAns_482"></a>[](#)

This is a great start! 

I really appreciate it. 

answered 2 years ago  by <img width="18" height="18" src="../../_resources/89ffb7cecd6c4c399510a3bc1a3976ab.jpg"/>  cwassen@manhattan.lib.ks.us

- <a id="acceptAnswer_482"></a>[Accept as answer](#)

<a id="commentAns_482"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Report to find Items added by Material Type in a certain year?](https://iii.rightanswers.com/portal/controller/view/discussion/430?)
- [Report of Lost and Paid Items](https://iii.rightanswers.com/portal/controller/view/discussion/331?)
- [I'm looking for a SQL report that will tell how long items are in held status.](https://iii.rightanswers.com/portal/controller/view/discussion/495?)

### Related Solutions

- [Item Circulation Statistics Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930404036798)
- [Are in-house check-ins included in canned circulation reports?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930566498391)
- [Item Circulation by Statistical Code](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930592909894)
- [Why is in-house check in not counting as circulation?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930877939075)
- [Claimed item does not appear in the Claimed Items report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930283717537)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)