[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Circulation for a collection by branch and stat code

0

107 views

I'm trying to determine how often Juvenile DVDs are circulating at all of our branches. So I'm looking for a report that will give a count of the number of checkouts per branch for a Collection (in this case, Movies and TV, collection code 21) and a stat class (Juvenile, statistical class code 2) during the previous year. I'm horrible at SQL ... any help??

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_fc82f4326267408493658dfc87b95a14.jpg"/> musack@bcls.lib.nj.us

[circulation stats](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=412#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 5 Replies   last reply 1 year ago

<a id="upVoteAns_1315"></a>[](#)

0

<a id="downVoteAns_1315"></a>[](#)

Answer Accepted

You can try the following SQL Query to see if it works for you:

SELECT o.Abbreviation,c.Abbreviation,s.Description,COUNT(th.TransactionID) AS "CheckOuts"  
FROM PolarisTransactions.Polaris.TransactionHeaders th (NOLOCK)  
JOIN PolarisTransactions.Polaris.TransactionDetails td (NOLOCK)  
ON td.TransactionID=th.TransactionID  
JOIN PolarisTransactions.Polaris.TransactionDetails td1 (NOLOCK)  
ON td1.TransactionID=th.TransactionID  
JOIN Polaris.Polaris.Organizations o (NOLOCK)  
ON o.OrganizationID=th.OrganizationID  
JOIN Polaris.Polaris.Collections c (NOLOCK)  
ON c.CollectionID=td.numValue  
JOIN Polaris.Polaris.StatisticalCodes s (NOLOCK)  
ON s.StatisticalCodeID=td1.numValue  
WHERE th.TransactionTypeID=6001  
AND td.TransactionSubTypeID=61  
AND td1.TransactionSubTypeID=60  
AND th.TranClientDate BETWEEN '2022-12-01' AND '2022-12-02' --Enter your date range here  
AND c.CollectionID IN (58,59,255) --Enter your Collection Code ID numbers here  
AND s.Description='Audio/Visual' --Enter your Statisical Code description here or comment it out to see all the Statistical Code Descriptions being used with the Collections above  
GROUP BY o.Abbreviation,c.Abbreviation,s.Description  
ORDER BY o.Abbreviation

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_fc82f4326267408493658dfc87b95a14.jpg"/> rhelwig@flls.org

<a id="commentAns_1315"></a>[Add a Comment](#)

<a id="upVoteAns_1317"></a>[](#)

0

<a id="downVoteAns_1317"></a>[](#)

This was perfect! Exactly what I needed .... thanks!

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_fc82f4326267408493658dfc87b95a14.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_1317"></a>[Accept as answer](#)

<a id="commentAns_1317"></a>[Add a Comment](#)

<a id="upVoteAns_1316"></a>[](#)

0

<a id="downVoteAns_1316"></a>[](#)

Thank you ... I'll give that a try! I really appreciate the help :)

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_fc82f4326267408493658dfc87b95a14.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_1316"></a>[Accept as answer](#)

<a id="commentAns_1316"></a>[Add a Comment](#)

<a id="upVoteAns_1314"></a>[](#)

0

<a id="downVoteAns_1314"></a>[](#)

Hmmm ... that gives circ for each item individually, correct? I'm looking for an overall number for circ of Juvenile DVDs -- total circ per branch of all the Juvenile DVDs together. Thanks, though! That looks like a pretty intense SQL!

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_fc82f4326267408493658dfc87b95a14.jpg"/> musack@bcls.lib.nj.us

- <a id="acceptAnswer_1314"></a>[Accept as answer](#)

<a id="commentAns_1314"></a>[Add a Comment](#)

<a id="upVoteAns_1313"></a>[](#)

0

<a id="downVoteAns_1313"></a>[](#)

This might help, but you will need to add a filter for stat class. [Circ Count for Every I... | Useful Polaris SQL (cyberpunklibrarian.nohost.me)](https://cyberpunklibrarian.nohost.me/usefulpolarissql/books/circulation/page/circ-count-for-every-item-in-a-libarys-collection)

answered 1 year ago by ![eric.young@phoenix.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_eric.young@phoenix.gov20191029113427.jpg) eric.young@phoenix.gov

- <a id="acceptAnswer_1313"></a>[Accept as answer](#)

<a id="commentAns_1313"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Fines paid per year by Branch, Branch/Collection, and Branch/Patron Code](https://iii.rightanswers.com/portal/controller/view/discussion/1079?)
- [SQL for circ of items in record set, to include checkout info](https://iii.rightanswers.com/portal/controller/view/discussion/1122?)
- [Any ideas how we might restrict checkout of collections by patron age?](https://iii.rightanswers.com/portal/controller/view/discussion/1373?)
- [Feedback on Floating Collections](https://iii.rightanswers.com/portal/controller/view/discussion/984?)
- [Floating Collections for Dummies](https://iii.rightanswers.com/portal/controller/view/discussion/73?)

### Related Solutions

- [What to check after setting up a new code in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170228154215576)
- [Can patron codes available for selection by a branch be filtered?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930382166305)
- [Collection Agency and Minimum Balance](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181231110228981)
- [Collection Agency block does not clear even after the patron pays their fines](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930289283379)
- [How to set up Collection Agency](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930618943301)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)