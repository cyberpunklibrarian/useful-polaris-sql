SQL for holds when we re-enable RTF - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for holds when we re-enable RTF

0

74 views

RTF is disabled for our libraries, and we have dismantled the secondary routing sequence, but we have been otherwise been allowing holds. I've now had an inquiry about how many titles may show up on the picklist once we reinstate the RTF. I revised an old SQL script and although a bit messy, is pretty close. I'm sure one of my joins is wrong since I'm not getting "IN" results for only the branch that is the pickup location. Any suggestions? (Since I've increased the time in the primary routing sequence to 60 days, I'll probably just enable RTF processing and see what goes to pending, but it would be nice to have a report ahead of time.)

select distinct shr.PickupBranchID, br.BrowseTitle, tom.Description AS TypeOfMaterial, shs.Description AS HoldStatus, cis.Description AS Status, cir.AssignedBranchID, shr.ActivationDate, pat.Barcode, ptr.PatronFullName
from Polaris.SysHoldRequests shr with (nolock)
inner join Polaris.SysHoldStatuses shs with (nolock)
on (shs.SysHoldStatusID = shr.SysHoldStatusID)
inner join Polaris.MARCTypeOfMaterial tom with (nolock)
on (tom.MARCTypeOfMaterialID = shr.PrimaryMARCTOMID)
inner join Polaris.CircItemRecords cir with (nolock)
on (shr.BibliographicRecordID = cir.AssociatedBibRecordID)
inner join bibliographicrecords br with (nolock)
on (br.BibliographicRecordID = shr.BibliographicRecordID)
inner join Polaris.ItemStatuses cis with (nolock)
on (cis.ItemStatusID = cir.ItemStatusID)
inner join Polaris.Patrons pat with (nolock)
on (pat.PatronID = shr.PatronID)
inner join Polaris.PatronRegistration ptr with (nolock)
on (ptr.PatronID = pat.PatronID)
where cir.ItemStatusID <> 13
where shs.SysHoldStatusID = 3
and shr.PickupBranchID = 5
and cis.ItemStatusID = 1
and cir.AssignedBranchID = 5
and pat.Barcode like '24539%'
order by cis.Description, br.BrowseTitle, ptr.PatronFullName

Many thanks!

Marilyn

asked 1 year ago  by <img width="18" height="18" src="../../_resources/e381ce3c67f04340bb6778d6b807192c.jpg"/>  mborg@gmilcs.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_914"></a>[](#)

0

<a id="downVoteAns_914"></a>[](#)

Answer Accepted

Hi Marilyn.

I found a couple of hiccups in your query. You have 2 where statements. Also one of your conditions refers to ItemStatuses instead of CircItemRecords.

With those tweaks, I got a list. This list, however has one row for each active hold, no matter how many items there are. If that's OK for your purposes, the query below should work for you.

select distinct shr.PickupBranchID, br.BrowseTitle, tom.Description AS TypeOfMaterial, shs.Description AS HoldStatus, cis.Description AS Status, cir.AssignedBranchID, shr.ActivationDate, pat.Barcode, ptr.PatronFullName
from Polaris.Polaris.SysHoldRequests shr with (nolock)
inner join Polaris.Polaris.SysHoldStatuses shs with (nolock)
on (shs.SysHoldStatusID = shr.SysHoldStatusID)
inner join Polaris.Polaris.MARCTypeOfMaterial tom with (nolock)
on (tom.MARCTypeOfMaterialID = shr.PrimaryMARCTOMID)
inner join Polaris.Polaris.CircItemRecords cir with (nolock)
on (shr.BibliographicRecordID = cir.AssociatedBibRecordID)
inner join Polaris.Polaris.bibliographicrecords br with (nolock)
on (br.BibliographicRecordID = shr.BibliographicRecordID)
inner join Polaris.Polaris.ItemStatuses cis with (nolock)
on (cis.ItemStatusID = cir.ItemStatusID)
inner join Polaris.Polaris.Patrons pat with (nolock)
on (pat.PatronID = shr.PatronID)
inner join Polaris.Polaris.PatronRegistration ptr with (nolock)
on (ptr.PatronID = pat.PatronID)
where shs.SysHoldStatusID = 3
and shr.PickupBranchID = 5
and cir.ItemStatusID = 1
and cir.AssignedBranchID = 5
and pat.Barcode like '24539%'
order by cis.Description, br.BrowseTitle, ptr.PatronFullName

If just a list of titles would work better for you, here is a query that omits the patron info (the reason for multiple rows above)

select shr.PickupBranchID, br.BrowseTitle, tom.Description AS TypeOfMaterial,
 shs.Description AS HoldStatus, cis.Description AS Status, cir.AssignedBranchID,
 MIN(shr.ActivationDate)
from Polaris.Polaris.SysHoldRequests shr with (nolock)
inner join Polaris.Polaris.SysHoldStatuses shs with (nolock)
on (shs.SysHoldStatusID = shr.SysHoldStatusID)
inner join Polaris.Polaris.MARCTypeOfMaterial tom with (nolock)
on (tom.MARCTypeOfMaterialID = shr.PrimaryMARCTOMID)
inner join Polaris.Polaris.CircItemRecords cir with (nolock)
on (shr.BibliographicRecordID = cir.AssociatedBibRecordID)
inner join Polaris.Polaris.bibliographicrecords br with (nolock)
on (br.BibliographicRecordID = shr.BibliographicRecordID)
inner join Polaris.Polaris.ItemStatuses cis with (nolock)
on (cis.ItemStatusID = cir.ItemStatusID)
inner join Polaris.Polaris.Patrons pat with (nolock)
on (pat.PatronID = shr.PatronID)
inner join Polaris.Polaris.PatronRegistration ptr with (nolock)
on (ptr.PatronID = pat.PatronID)
--where cir.ItemStatusID <> 13
where shs.SysHoldStatusID = 3
and shr.PickupBranchID = 5
and cir.ItemStatusID = 1
and cir.AssignedBranchID = 5
and pat.Barcode like '24539%'
group by shr.PickupBranchID, br.BrowseTitle, tom.Description,
 shs.Description, cis.Description, cir.AssignedBranchID
order by cis.Description, br.BrowseTitle

These queries do not tell you how many items are In, however. There is probably a way to get to this, but I have a feeling it will get complicated.

Hope one or both of these are helpful.

JT

answered 1 year ago  by <img width="18" height="18" src="../../_resources/e381ce3c67f04340bb6778d6b807192c.jpg"/>  jwhitfield@aclib.us

<a id="commentAns_914"></a>[Add a Comment](#)

<a id="upVoteAns_918"></a>[](#)

0

<a id="downVoteAns_918"></a>[](#)

Thanks JT! The extra where is because I played around with it commenting out one of those lines. I think your fixes have given me what I need.

Stay well!

Marilyn

answered 1 year ago  by <img width="18" height="18" src="../../_resources/e381ce3c67f04340bb6778d6b807192c.jpg"/>  mborg@gmilcs.org

- <a id="acceptAnswer_918"></a>[Accept as answer](#)

<a id="commentAns_918"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for bibs with holds and no owned items by branches](https://iii.rightanswers.com/portal/controller/view/discussion/821?)
- [Need help with query to find checked out titles with holds](https://iii.rightanswers.com/portal/controller/view/discussion/1063?)
- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [SQL for Bibs w/no good items but have holds - including item and patron info](https://iii.rightanswers.com/portal/controller/view/discussion/594?)
- [Average hold wait times](https://iii.rightanswers.com/portal/controller/view/discussion/845?)

### Related Solutions

- [Hold Request RTF Routing Nightly SQL job is failing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=201211165310293)
- [No holds going pending at new branch](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930674074347)
- [Trapping Preferences - What are they and how do they work?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930956439738)
- [Why did Hold Requests move to Active while RTF process was stopped?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200731095955030)
- [Hold Request to Fill Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161031124829450)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)