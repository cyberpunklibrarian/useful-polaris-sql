[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# COVID-19 Due Date Extensions, Fine Amnesty, Etc

0

271 views

Hi All!  
<br/>Is anyone blanket waiving fines or extending due dates since many people are limiting their trips out if they're in a higher risk demographic? We have an older than average population in our community (Manatee County, Florida), and we already have a handful of confirmed cases here, so our citizens are being proactive.  
<br/><br/>Without spending hours fussing with Polaris, I wonder:  
<br/><br/>Whats the easiest way to extend due dates for currently checked items and for future items? We currently have 3 auto-renewals on most items, which has helped.  
<br/><br/>What's the easiest way to go fine free for a short period of time? Increase the number of free days?

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_7312a6d9516448b6903dc3728ec02876.jpg"/> stephanie.katz@mymanatee.org

<a id="comment"></a>[Add a Comment](#)

## 11 Replies   last reply 4 years ago

<a id="upVoteAns_890"></a>[](#)

0

<a id="downVoteAns_890"></a>[](#)

&nbsp;Anyone turning off their notices or are you just letting them continue?  If you are, are you turning them off at the system level?

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_7312a6d9516448b6903dc3728ec02876.jpg"/> lori@esrl.org

- <a id="acceptAnswer_890"></a>[Accept as answer](#)

Yes, we have adjusted the Notices Processing SQL job, per a suggestion from our site manager. We want sone processing to take place -- particularly auto-renewals -- without sending the email and text notices for two reasons:   
1\. Patrons will be disappointed that they are unable to pick up their requested items.  
2\. Emailing and texting overdues moves items through the billing process, which we wish to avoid.

The steps for your Notices Processing job in MS SQL Server Management Studio may differ, but here are the instructions we were given.

To modify Notices Processing so that email and text notices are not sent:  
. Log in to SSMS using Windows Authentication.  
. In the Object Explorer, expand SQL Server Agent then double click on Job Activity Monitor.  
. Locate Notices Processing, right click, and choose Properties.  
. In the left side bar, click on Steps.  
. Double click on step 11 (the one right before the Execute Polaris Email Manager).  
. Click on the Advanced tab in the left side bar.  
. Change the "on success action" to "quit the job reporting success" \[from “Go to the next step”\] and the "on failure action" to "quit the job reporting failure" \[from “Go to the next step”\].   
. Click OK to close the job step properties and OK again to close the job properties. You'll get a warning informing you that step 12 cannot be reached - click Yes.

You can simply disable the Hold Notice Processing job.

If you wish to halt Polaris phone notification:

. Log on to your Telephony server.  
. Open up Services and locate Polaris Phone Notification.  
. Right click and choose Properties.  
. On the General tab, change the Startup type to be Manual \[from Automatic\]. This will prevent the service from restarting if the server reboots.  
. Stop the service.

— ruth.vargas@hclibrary.org 4 years ago

<a id="commentAns_890"></a>[Add a Comment](#)

<a id="upVoteAns_889"></a>[](#)

0

<a id="downVoteAns_889"></a>[](#)

&nbsp;Anyone turning off their notices or are you just letting them continue?  If you are, are you turning them off at the system level?

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_7312a6d9516448b6903dc3728ec02876.jpg"/> lori@esrl.org

- <a id="acceptAnswer_889"></a>[Accept as answer](#)

<a id="commentAns_889"></a>[Add a Comment](#)

<a id="upVoteAns_888"></a>[](#)

0

<a id="downVoteAns_888"></a>[](#)

All great advice. Thanks Guys.

&nbsp;

We're now closed until 3/30 (or TBD but not telling the public that yet).

I've also set us all libraries to closed, and am currently working on moving all accounts that have expired the last month, will expire this month and may expire next month up past our new open date or adding 30 days to the one that have yet to expire.

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_7312a6d9516448b6903dc3728ec02876.jpg"/> aal-shabibi@champaign.org

- <a id="acceptAnswer_888"></a>[Accept as answer](#)

<a id="commentAns_888"></a>[Add a Comment](#)

<a id="upVoteAns_887"></a>[](#)

0

<a id="downVoteAns_887"></a>[](#)

The "hold till" date is calculated based on the number of open days, so it automatically skips the weekend if your closed during that.  When I mark a library closed from March 16-27 (12 days), I the go and add 12 days to the unclaimed dates of any currently held items. This moves their dates until past the closure and spreads them out so the patrons are blasting down your down the day you re-open to the public.

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_7312a6d9516448b6903dc3728ec02876.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_887"></a>[Accept as answer](#)

<a id="commentAns_887"></a>[Add a Comment](#)

<a id="upVoteAns_886"></a>[](#)

0

<a id="downVoteAns_886"></a>[](#)

So far we went to Parameters>Patron Services and:

\>Free days bulk and normal- changed to 31

\>Days not fineable- we checked all the days

\>Renewal block if there are holds- we changed to No

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_7312a6d9516448b6903dc3728ec02876.jpg"/> stephanie.katz@mymanatee.org

- <a id="acceptAnswer_886"></a>[Accept as answer](#)

<a id="commentAns_886"></a>[Add a Comment](#)

<a id="upVoteAns_881"></a>[](#)

0

<a id="downVoteAns_881"></a>[](#)

You might also want to consider stopping your Notice process jobs so any 2nd, 3rd Overdue notices or patron account expiration reminders don't go out while you're closed.

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_7312a6d9516448b6903dc3728ec02876.jpg"/> wosborn@clcohio.org

- <a id="acceptAnswer_881"></a>[Accept as answer](#)

<a id="commentAns_881"></a>[Add a Comment](#)

<a id="upVoteAns_880"></a>[](#)

0

<a id="downVoteAns_880"></a>[](#)

We just went through this. Our libraries decided we want nothing due before May 1 to help ease worries about having overdue things. 

As such Polaris helped us batch update all items out with new due dates between May 1-7.

I updated the # of units on our loan period codes, basically adding 50 to each. so 7 day checkout is now 57, 14 day checkout is now 64.

We followed this up with an email blast to all accounts.

I'll have to edit these records once we feel it's time, but other than that it went well and customers are happy to have one less worry.

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_7312a6d9516448b6903dc3728ec02876.jpg"/> aal-shabibi@champaign.org

- <a id="acceptAnswer_880"></a>[Accept as answer](#)

<a id="commentAns_880"></a>[Add a Comment](#)

<a id="upVoteAns_877"></a>[](#)

0

<a id="downVoteAns_877"></a>[](#)

We're adding closed dates (post to come about that SQL).  moving due date, hold dates, and considering going fine free (by bulk changing items to have the "no fine" code and have record sets to change it all back when the time comes).

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_7312a6d9516448b6903dc3728ec02876.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_877"></a>[Accept as answer](#)

We're hosted so our site manager can bump our due dates, but the SQL would be wonderful if you have it for others. Thank you! I think every sysadmin should change items to no fine then refuse to change them back!

— stephanie.katz@mymanatee.org 4 years ago

Hi, Trevor. Could you share more about what you are doing with hold dates? Thank you in advance!

— ahoffman@monarchlibraries.org 4 years ago

<a id="commentAns_877"></a>[Add a Comment](#)

<a id="upVoteAns_876"></a>[](#)

0

<a id="downVoteAns_876"></a>[](#)

How about changing your renewals from 3 to 5. That would extend the due dates for anything that doesn't have a hold on it. 

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_7312a6d9516448b6903dc3728ec02876.jpg"/> lori@esrl.org

- <a id="acceptAnswer_876"></a>[Accept as answer](#)

We may do that.

— stephanie.katz@mymanatee.org 4 years ago

<a id="commentAns_876"></a>[Add a Comment](#)

<a id="upVoteAns_875"></a>[](#)

0

<a id="downVoteAns_875"></a>[](#)

Last year when the government shut down, we did not assess fines for about 6 weeks. We set Polaris to "closed" for all those days, which wrecks havoc with "On shelf" status, but worked ok. 

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_7312a6d9516448b6903dc3728ec02876.jpg"/> ruth.vargas@hclibrary.org

- <a id="acceptAnswer_875"></a>[Accept as answer](#)

Oh good reminder if we have to close!

— stephanie.katz@mymanatee.org 4 years ago

With the government shutdown our branches were actually open to the public, but that seemed the best way not to assess fines or send patrons to collections. Being physically closed seems a little easier.

— ruth.vargas@hclibrary.org 4 years ago

Gotcha. That'a a nice little Polaris hack!

— stephanie.katz@mymanatee.org 4 years ago

<a id="commentAns_875"></a>[Add a Comment](#)

<a id="upVoteAns_874"></a>[](#)

0

<a id="downVoteAns_874"></a>[](#)

When we do a fine-free month, we just increase the free days to 99 and the most difficult thing about that is remembering to change it back.

Marilyn

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_7312a6d9516448b6903dc3728ec02876.jpg"/> mborg@gmilcs.org

- <a id="acceptAnswer_874"></a>[Accept as answer](#)

Thank you!

— stephanie.katz@mymanatee.org 4 years ago

<a id="commentAns_874"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Can you auto wave fines at check in?](https://iii.rightanswers.com/portal/controller/view/discussion/487?)
- [Reset due dates - SQL help](https://iii.rightanswers.com/portal/controller/view/discussion/1324?)
- [Patron expiration date update](https://iii.rightanswers.com/portal/controller/view/discussion/727?)
- [All library cards expire on the same date](https://iii.rightanswers.com/portal/controller/view/discussion/670?)
- [Questions about automatic waiving of lost fees](https://iii.rightanswers.com/portal/controller/view/discussion/826?)

### Related Solutions

- [Retention period of Dates Closed policy table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930512608486)
- [Polaris SIP not returning detailed fine information](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930827108312)
- [Fee reason display order in Leap](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930612692592)
- [Estimated fines and integrated eBooks](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930274536879)
- [Estimated fines](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930719003718)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)