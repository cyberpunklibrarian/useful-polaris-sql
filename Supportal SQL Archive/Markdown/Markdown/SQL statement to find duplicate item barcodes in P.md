SQL statement to find duplicate item barcodes in Polaris. - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL statement to find duplicate item barcodes in Polaris.

0

89 views

Does anyone have an SQL statement to find duplicate item barcodes in Polaris?

Thanks in advance,

Anne Krulik

Burlington County Library System

asked 2 years ago  by <img width="18" height="18" src="../../_resources/1f445cded7804d5aa1c36750c3d5aa8a.jpg"/>  akrulik@bcls.lib.nj.us

[barcodes](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=208#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_494"></a>[](#)

0

<a id="downVoteAns_494"></a>[](#)

Answer Accepted

This should work in the client:
SELECT ItemRecordID
FROM CircItemRecords cir with (NOLOCK)
WHERE cir.Barcode IN (
SELECT cir.Barcode
FROM CircItemRecords cir with (NOLOCK)
GROUP BY cir.Barcode
HAVING count(cir.Barcode) > 1
)

answered 2 years ago  by <img width="18" height="18" src="../../_resources/1f445cded7804d5aa1c36750c3d5aa8a.jpg"/>  jjack@aclib.us

<a id="commentAns_494"></a>[Add a Comment](#)

<a id="upVoteAns_495"></a>[](#)

0

<a id="downVoteAns_495"></a>[](#)

Thanks!  It worked.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/1f445cded7804d5aa1c36750c3d5aa8a.jpg"/>  akrulik@bcls.lib.nj.us

- <a id="acceptAnswer_495"></a>[Accept as answer](#)

<a id="commentAns_495"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [SQL to Find all bibs with same title.](https://iii.rightanswers.com/portal/controller/view/discussion/675?)
- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)
- [Is there still a SQL repository for the Polaris forum?](https://iii.rightanswers.com/portal/controller/view/discussion/706?)
- [SQL for items returned late by collection](https://iii.rightanswers.com/portal/controller/view/discussion/718?)

### Related Solutions

- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [Unable to delete two holds, error message about ILL$Messages appears](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930410469399)
- [Polaris not recognizing ILL item barcode](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930615916421)
- [Printing multiple spine labels for the same call number](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930444030856)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)