[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Curbside Delivery?

0

85 views

Good evening

We're thinking of starting Holds curbside delivery.

Does anyone else out there in the Polaris universe provide a similar service? Can you share your procedures and anything you wish you had done differently?

&nbsp;

Thanks,

&nbsp;

Amy

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_40e49eada3274f27bb4872921ac567a0.jpg"/> aal-shabibi@champaign.org

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 4 years ago

<a id="upVoteAns_922"></a>[](#)

0

<a id="downVoteAns_922"></a>[](#)

Hi Gabrielle, 

We just started curbside during our library closure and I've been avoiding creating a new location for just curbside. Now that we're planning on continuing the service post-Covid - if such a thing exists - I'm thinking I will need to add locations after all. My concern has been that patrons might put something on a curbside hold and then decide that they actually want to pick it up while physically inside the library. In that case, staff will have to figure out why the patron's item isn't on the regular holds shelf and then run to the back to check the curbside shelf.   I may be way overthinking this, but have you experienced any issues with this?

&nbsp;

My other concern is that having a curbside option on the pickup dropdown ends up giving us a slightly confusing array of pickup locations from the patron perspective. Might not be an issue, but I wonder...

Thanks!

Julia

answered 4 years ago by ![julia.cagle@arlingtontx.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_julia.cagle@arlingtontx.gov20200423151509.jpg) julia.cagle@arlingtontx.gov

- <a id="acceptAnswer_922"></a>[Accept as answer](#)

Hi Julia,

I didn't hear feedback from staff about customers deciding to come in and staff not knowing where the hold was.  I suspect that's because one of the benefits of creating that drive-thru branch is it shows in the patron record where the hold is held, so the staff could look it up.  Our branches have these two hold areas far away from each other, so I expect that staff defaulted to looking at the patron record instead of running to check both shelves.

One adjustment I did have to make was on our pickup slips and custom holds reports, adding the pickup branch abbreviation so that staff could see which shelf the hold was intended for, or needed to be pulled from (unclaimed). Before the pandemic closed things down I was working with one location to try to make it more obvious on the hold pickup slips since the abbreviations are pretty close: SH vs SHP for example.

With the drop down... it might be confusing.  We had to switch ours from displaying according to organizationID to alphabetically so that the regular location was listed with the pick-up window location instead of all the new pickup locations being at the bottom.  It might help to use WebAdmin to add a custom language string to remind patrons to pick the curbside option.  I'm guessing you'll suppress the regular branches if curbside is the only option.

I completely understand why you wouldn't want to create new locations for a temporary workflow! Deleting branches is not a fun task.

Gabrielle

— ggosselin@richlandlibrary.com 4 years ago

<a id="commentAns_922"></a>[Add a Comment](#)

<a id="upVoteAns_795"></a>[](#)

0

<a id="downVoteAns_795"></a>[](#)

Thanks Gabrielle!

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_40e49eada3274f27bb4872921ac567a0.jpg"/> aal-shabibi@champaign.org

- <a id="acceptAnswer_795"></a>[Accept as answer](#)

<a id="commentAns_795"></a>[Add a Comment](#)

<a id="upVoteAns_778"></a>[](#)

0

<a id="downVoteAns_778"></a>[](#)

Hi Amy!

Not exactly curb-side pickup but we have a few branches that were renovated with drive thru hold pickup windows. 

I created a branch in Polaris for each of the windows so patrons could choose them as their hold pick-up location since the windows are far from where the "regular" holds are kept.

Most branch staff had never needed to use branch switching, so one of the staff training issues was getting the workflow ingrained that the pick-up window holds had to be checked in at the correct branch in order to go held and send out a notice to the patron.

We also didn't want any patrons registered at the pick-up window branches, but staff kept doing that to try to set the default hold pickup branch to be the drive thru window.  I ended up creating a SQL job that runs nightly to find those patrons, set the hold pickup branch default to the drive thru window, and then update the registered branch back to the non-drive thru branch. 

I hope this helps!

Gabrielle

answered 4 years ago by ![ggosselin@richlandlibrary.com](https://iii.rightanswers.com/portal/app/images/custom/avatar_ggosselin@richlandlibrary.com20170530111420.JPG) ggosselin@richlandlibrary.com

- <a id="acceptAnswer_778"></a>[Accept as answer](#)

<a id="commentAns_778"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [How are you using Outreach Services at your library?](https://iii.rightanswers.com/portal/controller/view/discussion/1371?)
- [SQL for patron circ at a particular workstation](https://iii.rightanswers.com/portal/controller/view/discussion/918?)
- [Bookmobile set-up?](https://iii.rightanswers.com/portal/controller/view/discussion/1192?)
- [Scanning an item record to determine destination (especially for holds) without clicking further?](https://iii.rightanswers.com/portal/controller/view/discussion/1309?)
- [Patron Registration during a Pandemic](https://iii.rightanswers.com/portal/controller/view/discussion/925?)

### Related Solutions

- [Polaris Considerations for Opening Physical Locations](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200421171849439)
- [Unable to delete Outreach route](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180820155708472)
- [Patron Record Import TRN Format - Polaris 6.5](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200513113659590)
- [Sending or resending hold notices after a long closure](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200423150522824)
- [How can hold request be handled when the library is quarantining items?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200424121313289)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)