SQL help for for circ info on 'new' stuff - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL help for for circ info on 'new' stuff

0

47 views

I've got a librarian who is looking for the top 35 circing items for the last calendar year in specific collections. She wants only newly published things added within that time frame. The 'top circing items by collection' canned report in the client doesn't help since that includes older titles that have been in our collection for ages (like Harry Potter and the Sorcerers Stone, our top circulating juvenile fiction book within the last year, but not purchased in that time). I'm not sure if there's a quick or easy way to do that in Simply Reports and I'm not SQL savvy enough to compose one myself (other than making tweaks to existing SQL queries) and let me know if I can help clarify anything in the explanation above. Thanks!

asked 1 year ago  by <img width="18" height="18" src="../../_resources/e2de448e215f4b4d96020aaebbcd6cc4.jpg"/>  adornink@amespl.org

[sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_841"></a>[](#)

0

<a id="downVoteAns_841"></a>[](#)

This is quick work, but should do the trick.  I spot-checked a half-dozen bibs and they all matched.

If you don't have Report Builder, and must do this in the client or LEAP (make sure you do this from Bib Search; else the results will be garbage):

`SELECT pyic.BibliographicRecordID FROM (`

`SELECT top 25 br.BibliographicRecordID`
`FROM BibliographicRecords br with (NOLOCK)`
`JOIN CircItemRecords cir with (NOLOCK)`
`ON br.BibliographicRecordID = cir.AssociatedBibRecordID`
`JOIN PrevYearItemsCirc pyic`
`ON pyic.ItemRecordID = cir.itemRecordID`
`WHERE cir.AssignedCollectionID IN (14)`
`AND br.FirstAvailableDate BETWEEN '10/1/2018' AND '9/30/2019 11:59:59 PM'`
`AND cir.AssignedBranchID in (4,5,16,17,6,7,3,8,9,10,11,12,13,14)`
`GROUP BY br.BibliographicRecordID`
`ORDER BY SUM(pyic.YTDCircCount) DESC`

`) pyic`

If you do have Report Builder:

`SELECT top 25 br.BibliographicRecordID, br.BrowseTitle, br.BrowseAuthor, SUM(pyic.YTDCircCount) AS 'CircsLastYear'`
`FROM BibliographicRecords br with (NOLOCK)`
`JOIN CircItemRecords cir with (NOLOCK)`
`ON br.BibliographicRecordID = cir.AssociatedBibRecordID`
`JOIN PrevYearItemsCirc pyic`
`ON pyic.ItemRecordID = cir.itemRecordID`
`WHERE cir.AssignedCollectionID IN (14)`
`AND br.FirstAvailableDate BETWEEN '10/1/2018' AND '9/30/2019 11:59:59 PM'`
`AND cir.AssignedBranchID in (4,5,16,17,6,7,3,8,9,10,11,12,13,14)`
`GROUP BY br.BibliographicRecordID, br.BrowseTitle, br.BrowseAuthor`
`ORDER BY SUM(pyic.YTDCircCount) DESC`

Caveats:

- You can change "`SELECT top 25`" to be whatever number you're interested in.
- You'll need to change the line "`WHERE cir.AssignedCollectionID IN (14)`" to match whatever collection numbers you're interested in.  You *could* query multiple collections but they'd be mixed in the results and you might get, say, 20 from one collection and 5 from the other.  It's probably easier just to run the report again for each individual collection.
- Depending on your fiscal year, you may need to change the dates in the line "`AND br.FirstAvailableDate BETWEEN '10/1/2018' AND '9/30/2019 11:59:59 PM'`"  This date is the FirstAvailableDate for the bib.
- I have the line "`AND cir.AssignedBranchID in (4,5,16,17,6,7,3,8,9,10,11,12,13,14)`" to calculate circs of items assigned only to certain branches.  You can change the numbers to match your own system, or delete the line altogether if you want to count all circs.
- It's calculating previous year circs from the PrevYearItemsCirc table.  I suspect that info attached to any particular item is deleted from there (as it is in the rest of the database) when the corresponding item is deleted, so these numbers might drift a bit depending on your item record retention policies.  If you don't keep your item records for the current fiscal year plus all of the previous fiscal year, you might need different SQL than what I've provided.

If you have any questions, please let me know.

Edit to add: additional caveat: you might get the occasional odd-looking result, esp. if items attached to a bib are not all in the same collection.  e.g. we could get some Guinness Book titles because we have them split between Juvenile Non-Fiction and Adult Non-Fiction, and this can appear to be a mistake even if it's not.  It's possible to rewrite the query to avoid this, but easier just to know it's a potential issue and to delete the records manually if/when they show up unwanted.

Edit to add: that bit I first wrote about cir.AssignedBranchID was clumsy and imprecise.  That line was a quick bit added to *exclude* any OverDrive titles, which are all assigned to the same branch.  This will not address the issue if you wanted to count, say, checkouts only from branch A even if the item has since floated to branch B or C.  And I can't remember at this point why I thought that CollectionID wouldn't be sufficient to exclude OverDrive items....

answered 1 year ago  by <img width="18" height="18" src="../../_resources/e2de448e215f4b4d96020aaebbcd6cc4.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_841"></a>[Accept as answer](#)

<a id="commentAns_841"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)
- [SQL for Bibs w/no good items but have holds - including item and patron info](https://iii.rightanswers.com/portal/controller/view/discussion/594?)
- [Need help with query to find checked out titles with holds](https://iii.rightanswers.com/portal/controller/view/discussion/1063?)
- [SimplyReports or SQL to get circ count by a UDF?](https://iii.rightanswers.com/portal/controller/view/discussion/88?)
- [SimplyReports or SQL to get circ count by a UDF?](https://iii.rightanswers.com/portal/controller/view/discussion/82?)

### Related Solutions

- [Is it possible to add a new circulation status?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930604642359)
- [Guide to Ticket Product Selection Fields](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=190809095548053)
- [Add a new Payment Method for our "Food for Fines" drive](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161026092701843)
- [Automatically open SQL Server query window](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930908117973)
- [How do I schedule the Year End Circ Count Rollover job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930582726474)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)