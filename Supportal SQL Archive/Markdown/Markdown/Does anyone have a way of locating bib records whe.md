[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Does anyone have a way of locating bib records where the summary includes strings of symbols (&#x2019;) in place of apostrophes and hyphens?

0

57 views

Other examples include: &#x2026; &#x2013; &#x2014; &#xA0; 

I would love to be able to find them all so I can clean them up. It seems to happen most often with records coming in from Baker & Taylor or Hoopla, where the summary includes any of these symbols (; ' -).

asked 5 months ago by <img width="18" height="18" src="../_resources/default-avatar_6d4eaed1f6624eefaa0864439bfd42cd.jpg"/> courtney.desear@mymanatee.org

- [symbolex.PNG](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/1379?fileName=1379-symbolex.PNG "symbolex.PNG")

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 months ago

<a id="upVoteAns_1417"></a>[](#)

1

<a id="downVoteAns_1417"></a>[](#)

Answer Accepted

Thanks for posting, I'm adding this to my cleanup project list! I just ran an SQL and found several hundred bibs. Interestingly, the vast majority in  our results are books. The following bib record SQL search looks for 520 $a's with the text string **&#x**, within bibs that are in final status and not acq (ELvl 5). Hope this helps!

SELECT DISTINCT br.bibliographicrecordid  
FROM polaris.polaris.bibliographicrecords br  
WHERE br.recordstatusid = 1  
AND br.marcbibencodinglevel != '5'  
AND EXISTS (SELECT 1  
FROM polaris.polaris.bibliographictags bt  
JOIN polaris.polaris.bibliographicsubfields bs  
ON bs.bibliographictagid = bt.bibliographictagid  
WHERE bt.bibliographicrecordid = br.bibliographicrecordid  
AND bt.tagnumber = 520  
AND bs.subfield = 'a'  
AND bs.data LIKE '%&#x%')

answered 5 months ago by ![marikok@wccls.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_marikok@wccls.org20240119162535.jpg) marikok@wccls.org

<a id="commentAns_1417"></a>[Add a Comment](#)

<a id="upVoteAns_1428"></a>[](#)

0

<a id="downVoteAns_1428"></a>[](#)

Thanks so much for posting the SQL search for finding symbols in the 520s. It works great and was exactly what I needed, too!

answered 2 months ago by <img width="18" height="18" src="../_resources/default-avatar_6d4eaed1f6624eefaa0864439bfd42cd.jpg"/> eshield@nols.org

- <a id="acceptAnswer_1428"></a>[Accept as answer](#)

<a id="commentAns_1428"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Bib record modifiers](https://iii.rightanswers.com/portal/controller/view/discussion/1380?)
- [Can you change a tag in a bib record in Leap?](https://iii.rightanswers.com/portal/controller/view/discussion/1055?)
- ["Tricks" for locating poor bibliographic records for merging? Cataloging cleanup question](https://iii.rightanswers.com/portal/controller/view/discussion/1005?)
- [Wondering if anyone's come up with a good way to manage items that are in a temp. location/shelf loc.? I want items to keep temp/shelf loc. after checkin & have an exp. date that will make them show up on pull list so they can be returned to perm. loc.](https://iii.rightanswers.com/portal/controller/view/discussion/682?)
- [Is there a way to import the records back into the system and only import a tag that then goes over onto the existing record?](https://iii.rightanswers.com/portal/controller/view/discussion/1150?)

### Related Solutions

- [Locate Bibs without attached authority records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930243668853)
- [How does "Use template values instead of these (if available)" work?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170524120618032)
- [How can I locate bibs that were generated during a Terminated import without using SQL Server Management Studio?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930570115337)
- [Bib IDs and Vega; Linking to works in Vega and using bibIDs found in Vega to locate bib records in Polaris or Sierra](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210224114837770)
- [Default Bib Records to MARC 21 Editor](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930603446551)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)