[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Searching for Items that have not circ'd in 2 years ex. DVD's

0

57 views

Hello, 

Could someone give me a SQL or SImply Report to run for Items that have not circ'd in the past 2 years?

ex. Item: DVD'd Haven't circed since 2018. Or Items with YTD less than 2 from

2000 -2018

I tried to run on Simply Reports, but I must not have selected the correct filters.  Weeding project our managers want to complete within the year.

Please & Thank you!

All help is appreciated. 

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_e77842638dc541c59d7bd6c2fb2fe152.jpg"/> treel@mypccl.org

[build report](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=200#t=commResults) [circulation](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=185#t=commResults) [material type](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=166#t=commResults) [simply reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=11#t=commResults) [sql reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=441#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_1163"></a>[](#)

0

<a id="downVoteAns_1163"></a>[](#)

You didn't say what filters you selected, so it's hard to know what might have gone wrong.  But here's the steps I give to our libraries for a weekding report from SimplyReports.  I didn't specify filters by collection or material type, but you can certainly add those.  The date filters can be tricky so it's important to careful read what you are selecting.  SimplyReports is terrible at "or" logic so you'll need to run a report for duty books (aka no circs in 3 years) separate from a report for items created since 2000 with two or fewer circs this calendar year (YTD). 

&nbsp;

1.  When the SimplyReports screen appears, select the **Items tab** and then select **Item list reports**.

&nbsp;

1.  In **Report output column** on left side, choose data items to send to **Columns selected for output** on the right (suggestions below):

&nbsp;

- Item assigned collection abbreviation
- Item call number
- Item barcode
- Marc author
- Marc title
- Item lifetime circ count
- Item last circ activity date
- Item first available date

&nbsp;

&nbsp;

1.  Under **Item general filters**:

&nbsp;

Click the check box next to **Assigned Branch** and then choose your **Library** from the list.

&nbsp;Still further down the page, under **Item relative date filters( prior to a specified date)** select:

&nbsp;

Click the check box next to **First available date more than  \____ before the report date**:   For example, 3 years 

&nbsp;

Click the check box next to **Last Activity Date more than \___\__ before the report date**:

For example, 3 years. Also click the check box to **Include Null Values.**

&nbsp;

&nbsp;Click **Submit** to run the report.

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_e77842638dc541c59d7bd6c2fb2fe152.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_1163"></a>[Accept as answer](#)

<a id="commentAns_1163"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Items in system 2+ years with 2+ copies](https://iii.rightanswers.com/portal/controller/view/discussion/86?)
- [Report to find Items added by Material Type in a certain year?](https://iii.rightanswers.com/portal/controller/view/discussion/430?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)
- [Does anyone have an SQL that can get the circulation counts from either funds or invoice line items in a certain fiscal year that will count deleted items circulation?](https://iii.rightanswers.com/portal/controller/view/discussion/1397?)

### Related Solutions

- [Sort by year searching](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930814636875)
- [Acquisitions Fiscal Year Rollover Instruction](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930945428671)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Tip for Year End Activities](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930785949303)
- [Fiscal Year Processing Options](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160908151051784)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)