[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Holds alert by library report for multi-branch libraries

0

45 views

Our libraries are using the report “Holds Alerts by library” to figure out holds ratios and it does not seem to work correctly for our multibranch libraries. Our multibranch go into Polaris and count the number of copies owned by the main library and Branches, and the number of holds by the library patrons and figure it out. This adds a lot of extra work for the libraries with branches to figure out there holds ratios.

Just curious if any other systems with multibranch libraries have found a better way to use this report or get the data the libraries are looking for.

asked 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_e90f8533f5b44a5ca56186a7a2ed0792.jpg"/> jason@sals.edu

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_1346"></a>[](#)

0

<a id="downVoteAns_1346"></a>[](#)

I'll add, I just stumbled across a note in the client help articles that said customs reports are available for a fee.  So you may be limited today in going a custom route :/

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_e90f8533f5b44a5ca56186a7a2ed0792.jpg"/> jtenter@sasklibraries.ca

- <a id="acceptAnswer_1346"></a>[Accept as answer](#)

<a id="commentAns_1346"></a>[Add a Comment](#)

<a id="upVoteAns_1345"></a>[](#)

0

<a id="downVoteAns_1345"></a>[](#)

Hi Jason,

We created a custom version of this report for our consortium of several libraries (with a number of branches in each).  I wasn't here when it was created, so I'm not sure about the extent it differs from the Polaris version, but I've attached it in case it may be helpful for you.  Screenshot also attached for reference, and I'll mention (since the column headers are a bit unclear) that the record ID is for the bib.

Basically it allows you to filter on the library (or "Agency" in our report), and see details for them.  You should be able to create it locally by running the 2 stored procedures in SQL (you'll probably want to get rid of the "SILS" - our org name, applied to our custom database objects for easy reference).  Then you'll want to add the .rdl (SSRS report file) with your other report files, however this should be in a custom folder so it's not overwritten during upgrades; your Polaris support person should be able to give guidance on this.

Hope this may be helpful!

\-Jason

&nbsp;

&nbsp;

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_e90f8533f5b44a5ca56186a7a2ed0792.jpg"/> jtenter@sasklibraries.ca

- <a id="acceptAnswer_1345"></a>[Accept as answer](#)

- [Agency Hold Alert.png](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/1310?fileName=1310-1345_Agency+Hold+Alert.png "Agency Hold Alert.png")
- [Agency Holds Alert (2023-03-07).zip](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/1310?fileName=1310-1345_Agency+Holds+Alert+%282023-03-07%29.zip "Agency Holds Alert (2023-03-07).zip")

<a id="commentAns_1345"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Has anyone developed a SimplyReport/SQL Report to answer PLA's Public Library Data Service circulation questions?](https://iii.rightanswers.com/portal/controller/view/discussion/439?)
- [Report for custom Hold Requests to Fill list](https://iii.rightanswers.com/portal/controller/view/discussion/91?)
- [Renewals reports](https://iii.rightanswers.com/portal/controller/view/discussion/83?)

### Related Solutions

- [Statistics for Holds and ILLs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930628713826)
- [IUG 2016: Microsoft Report Builder for Polaris Libraries](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160406153612400)
- [Errors accessing reports in the client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930369939404)
- [Custom report parameters not allowing multiple selections in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930424447003)
- [Same report results](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930545379375)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)