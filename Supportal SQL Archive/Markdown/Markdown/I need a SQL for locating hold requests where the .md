I need a SQL for locating hold requests where the hold has been denied by a library - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# I need a SQL for locating hold requests where the hold has been denied by a library

0

40 views

I need a SQL that will find any hold request that was denied by any of the libraries in my consortium. I've tried writting one but I can't figure out how to link transaction type ID 2096 to the system hold table.

asked 1 year ago  by <img width="18" height="18" src="../../_resources/c67dae40395745a5adc14520d35b5017.jpg"/>  lori@esrl.org

[deny](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=389#t=commResults) [hold](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=70#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 1 year ago

<a id="upVoteAns_988"></a>[](#)

0

<a id="downVoteAns_988"></a>[](#)

th is the alias I use for TransactionHeaders, I didn't think and just assumed you already were looking at that table (since you knew to limit by TransactionTypeID = '2096').

I also made 2 typos (I was just typing it out and not checking in SSMS, sorry!)

SELECT *
FROM PolarisTransactions.Polaris.TransactionHeaders AS \[th\] WITH (NOLOCK)
INNER JOIN PolarisTransactions.Polaris.TransactionDetails AS \[holdid\] WITH (NOLOCK)
ON th.TransactionID = holdid.TransactionID AND holdid.TransactionSubTypeID = '233'
INNER JOIN Polaris.Polaris.SysHoldRequests AS \[shr\] WITH (NOLOCK)
ON holdid.numValue = shr.SysHoldRequestID
WHERE th.TransactionTypeID = '2096'

answered 1 year ago  by <img width="18" height="18" src="../../_resources/c67dae40395745a5adc14520d35b5017.jpg"/>  trevor.diamond@mainlib.org

- <a id="acceptAnswer_988"></a>[Accept as answer](#)

<a id="commentAns_988"></a>[Add a Comment](#)

<a id="upVoteAns_987"></a>[](#)

0

<a id="downVoteAns_987"></a>[](#)

Thanks, Trevor!

answered 1 year ago  by <img width="18" height="18" src="../../_resources/c67dae40395745a5adc14520d35b5017.jpg"/>  lori@esrl.org

- <a id="acceptAnswer_987"></a>[Accept as answer](#)

<a id="commentAns_987"></a>[Add a Comment](#)

<a id="upVoteAns_986"></a>[](#)

0

<a id="downVoteAns_986"></a>[](#)

Hey Lori!

TransactionSubTypeID = '233'  is the SysHoldRequestID.  So you should be able to ...

INNER JOIN PolarisTransactions.Polaris.TranactionDetails AS \[holdid\] WITH (NOLOCK)

ON th.TransactionID = holdid.TransactionID AND holdid.TransactionSubTypeID = '233'

INNER JOIN Polars.Polaris.SysHoldRequests AS \[shr\] WITH (NOLOCK)

ON holdid.numValue = shr.SysHoldRequestID

answered 1 year ago  by <img width="18" height="18" src="../../_resources/c67dae40395745a5adc14520d35b5017.jpg"/>  trevor.diamond@mainlib.org

- <a id="acceptAnswer_986"></a>[Accept as answer](#)

<a id="commentAns_986"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Does anyone have an SQL for a request ratio?](https://iii.rightanswers.com/portal/controller/view/discussion/394?)
- [Purchase or Inter-Library Loan Request Form/Database?](https://iii.rightanswers.com/portal/controller/view/discussion/856?)
- [SQL for patron checkouts at a specific location](https://iii.rightanswers.com/portal/controller/view/discussion/269?)
- [Phone notices based on pickup location](https://iii.rightanswers.com/portal/controller/view/discussion/854?)
- [I need a SQL for phatom lost item count attached to a patron's record.](https://iii.rightanswers.com/portal/controller/view/discussion/839?)

### Related Solutions

- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)
- [Hold Processing SQL jobs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930920183263)
- [ILL hold limits](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930523230486)
- [Can we suspend the hold option for specific titles?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930255045537)
- [Cannot place holds on materials at the new branch](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930383002391)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)