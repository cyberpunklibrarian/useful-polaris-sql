SQL for Bibs lacking local call # - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# SQL for Bibs lacking local call #

0

46 views

We have some MARC records in our catalog that have an 082 tag, but no local call number tags (092 or 099), which means that we have some records showing a call # of 813.54 rather than (for example) F SMITH. Does anyone have an SQL to find bib records without and 092 or 099 tag?

asked 1 year ago  by <img width="18" height="18" src="../../_resources/5afc22d6fced4bdfad7f333a61cebc1c.jpg"/>  musack@bcls.lib.nj.us

<a id="comment"></a>[Add a Comment](#)

## 5 Replies   last reply 1 year ago

<a id="upVoteAns_736"></a>[](#)

0

<a id="downVoteAns_736"></a>[](#)

Thank you! That should get me going :)

answered 1 year ago  by <img width="18" height="18" src="../../_resources/5afc22d6fced4bdfad7f333a61cebc1c.jpg"/>  musack@bcls.lib.nj.us

- <a id="acceptAnswer_736"></a>[Accept as answer](#)

<a id="commentAns_736"></a>[Add a Comment](#)

<a id="upVoteAns_735"></a>[](#)

0

<a id="downVoteAns_735"></a>[](#)

The original search was for bibs that didn't have an 082, 050, or 092 and I stripped out those other tags so here the original search below. Try it with the AND and then try it with an OR and see what happens!

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

select distinct br.BibliographicRecordID from Polaris.BibliographicRecords br (nolock)

where

(

br.BibliographicRecordID NOT IN

(select bt050.BibliographicRecordID from Polaris.BibliographicTags bt050 where bt050.EffectiveTagNumber = 50)

AND

br.BibliographicRecordID NOT IN

(select bt082.BibliographicRecordID from Polaris.BibliographicTags bt082 where bt082.EffectiveTagNumber = 82)

AND

br.BibliographicRecordID NOT IN

(select bt092.BibliographicRecordID from Polaris.BibliographicTags bt092 where bt092.EffectiveTagNumber = 92)

)

AND br.ILLFlag = 0

AND br.CreationDate < CONVERT(date,GETDATE() - 180)

AND BR.RecordStatusID = 1

answered 1 year ago  by <img width="18" height="18" src="../../_resources/5afc22d6fced4bdfad7f333a61cebc1c.jpg"/>  sgrant@somd.lib.md.us

- <a id="acceptAnswer_735"></a>[Accept as answer](#)

<a id="commentAns_735"></a>[Add a Comment](#)

<a id="upVoteAns_734"></a>[](#)

0

<a id="downVoteAns_734"></a>[](#)

If I wanted to find records that have neither an 092 nor an 099, could I modify by putting an OR statement? Or how could I do that?

answered 1 year ago  by <img width="18" height="18" src="../../_resources/5afc22d6fced4bdfad7f333a61cebc1c.jpg"/>  musack@bcls.lib.nj.us

- <a id="acceptAnswer_734"></a>[Accept as answer](#)

<a id="commentAns_734"></a>[Add a Comment](#)

<a id="upVoteAns_733"></a>[](#)

0

<a id="downVoteAns_733"></a>[](#)

Thanks! I'll give it a try

answered 1 year ago  by <img width="18" height="18" src="../../_resources/5afc22d6fced4bdfad7f333a61cebc1c.jpg"/>  musack@bcls.lib.nj.us

- <a id="acceptAnswer_733"></a>[Accept as answer](#)

<a id="commentAns_733"></a>[Add a Comment](#)

<a id="upVoteAns_732"></a>[](#)

0

<a id="downVoteAns_732"></a>[](#)

Here's one for the 092 tag. You can adjust for the 099 tag, I think:

select distinct br.BibliographicRecordID from Polaris.BibliographicRecords br (nolock)

where

(

               br.BibliographicRecordID NOT IN

                              (select bt092.BibliographicRecordID from Polaris.BibliographicTags bt092 where bt092.EffectiveTagNumber = 92)

)

AND br.ILLFlag = 0

AND br.CreationDate < CONVERT(date,GETDATE() - 180)

AND BR.RecordStatusID = 1

answered 1 year ago  by <img width="18" height="18" src="../../_resources/5afc22d6fced4bdfad7f333a61cebc1c.jpg"/>  sgrant@somd.lib.md.us

- <a id="acceptAnswer_732"></a>[Accept as answer](#)

<a id="commentAns_732"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Finding new and replaced bibs in Polaris between certain dates](https://iii.rightanswers.com/portal/controller/view/discussion/871?)
- [Changing offensive LOC subject terms to local subject terms](https://iii.rightanswers.com/portal/controller/view/discussion/879?)
- [Can you change a tag in a bib record in Leap?](https://iii.rightanswers.com/portal/controller/view/discussion/1055?)
- [Use Item Record Call Numbers to Bulk Update Bib MARC Tag](https://iii.rightanswers.com/portal/controller/view/discussion/235?)
- [SQL for items withdrawn by a particular staff member](https://iii.rightanswers.com/portal/controller/view/discussion/809?)

### Related Solutions

- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [How can I locate bibs that were generated during a Terminated import without using SQL Server Management Studio?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930570115337)
- [MARC 049 tag validation](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930325894633)
- [How do I schedule the Year End Circ Count Rollover job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930582726474)
- [Database tables for Keyword and Browse indexing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930397363904)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)