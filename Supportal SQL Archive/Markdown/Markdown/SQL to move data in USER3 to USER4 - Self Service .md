SQL to move data in USER3 to USER4 - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL to move data in USER3 to USER4

0

25 views

Does anyone have a SQL script that can move what is entered in UDF3 to UDF4 for records in a record set?

Thanks!

asked 4 months ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 4 months ago

<a id="upVoteAns_1101"></a>[](#)

1

<a id="downVoteAns_1101"></a>[](#)

Answer Accepted

begin tran
UPDATE PatronRegistration
SET User4=User3
WHERE PatronID IN (
SELECT PatronID
FROM PatronRecordSets
WHERE RecordSetID = ?????
)

--commit

begin tran
UPDATE PatronRegistration
SET User3=NULL
WHERE PatronID IN (
SELECT PatronID
FROM PatronRecordSets
WHERE RecordSetID = ?????

\-\- commit

answered 4 months ago  by <img width="18" height="18" src="../../_resources/a158e42a660f4e4b8a4908d52b83628e.jpg"/>  rhelwig@flls.org

<a id="commentAns_1101"></a>[Add a Comment](#)

<a id="upVoteAns_1104"></a>[](#)

0

<a id="downVoteAns_1104"></a>[](#)

Thanks Rex!  Just tried it out and it works perfectly!

\- Robin

answered 4 months ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

- <a id="acceptAnswer_1104"></a>[Accept as answer](#)

<a id="commentAns_1104"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Has anyone developed a SimplyReport/SQL Report to answer PLA's Public Library Data Service circulation questions?](https://iii.rightanswers.com/portal/controller/view/discussion/439?)
- [CollectionHQ Transaction and Holds Data Extracts](https://iii.rightanswers.com/portal/controller/view/discussion/358?)
- [Report/query for historical INNreach circ data?](https://iii.rightanswers.com/portal/controller/view/discussion/1052?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)
- [SQL for changes to a patron record](https://iii.rightanswers.com/portal/controller/view/discussion/840?)

### Related Solutions

- [Query to find unfillable holds due to volume data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930242167096)
- [Adding a new UDF to patron registration](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930740605846)
- [How was the data gathered in SimplyReports?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930857021045)
- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)
- [How can SQL jobs be accessed?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930442864574)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)