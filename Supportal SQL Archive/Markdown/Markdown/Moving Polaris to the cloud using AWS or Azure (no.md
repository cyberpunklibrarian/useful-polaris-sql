[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Moving Polaris to the cloud using AWS or Azure (not III hosted)

0

87 views

Due to some hurricane fun last year, we're looking into moving our Polaris environment to the cloud ourselves (AWS or Azure). 

I've heard there are some other Polaris libraries out there that have done this and I'd love to hear any wisdom you can share!

asked 5 years ago by ![ggosselin@richlandlibrary.com](https://iii.rightanswers.com/portal/app/images/custom/avatar_ggosselin@richlandlibrary.com20170530111420.JPG) ggosselin@richlandlibrary.com

[amazon web services](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=204#t=commResults) [aws](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=205#t=commResults) [azure](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=206#t=commResults) [cloud](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=207#t=commResults) [hosting](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=104#t=commResults) [polaris](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=7#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 5 years ago

<a id="upVoteAns_489"></a>[](#)

0

<a id="downVoteAns_489"></a>[](#)

In addition to AWS and Azure, you may want to check with any local co-location providers. They may actually be able to offer you a solution that allows your equipment to be mirrored to another one of their datacenters which could provide a better price performance ratio than AWS or Azure. That also gives you the opportunity to have lower latency services during most instances and you'd only need to "switch" to the remote DR site if there was a local weather event. Check for providers in your area, but: 

https://www.immedion.com/products-services/disaster-recovery-solutions/colocation looks like they might have some options.

The traditional cloud providers don't always do so well with "always-on" loads like Polaris. Polaris also loves single threaded CPU performance and RAM, which tend to be some of the most expensive cloud VMs to operate. I think the best bet would be if you were able to break out the SQL database so you could run that natively on Azure. Rather than just having to maintain a full Windows stack in the clould that needs all the traditional management AND then the extra complexity of managing the cloud provider elements on those VMs. Probably won't be possible to have an Azure SQL instance though due to some of the "old" tech that Polaris uses, like the CLR stuff it loads, AKA "Unsafe assembly 'ermsplugin, version=0.0.0.0, culture=neutral, publickeytoken=null, processorarchitecture=amd64' loaded into appdomain 2 (Polaris.dbo\[runtime\].1)."

Just putting a VM on azure or AWS doesn't ensure backup or disaster recovery either. You'd still need to build out that infrastruture and orchestration within the cloud provider. And you need to take into consideration that your cloud provider may reboot your VM to patch the underlying host system. Again, some botique local co-location shops can ease this process.

I'd probably look to engage an Azure or AWS consultant for a project like this. But I would really enjoy hearing more about your journey with exploring this option. It is something I've wanted to do for years, but due to our size and the way Polaris is designed, it would cost us at least 4 times more versus running our own hardware. And that doesn't consider the extra staffing needed to manage the more complex enviornment.

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_bbc58f80c2274de3a6bfa227a8212fad.jpg"/> wosborn@clcohio.org

- <a id="acceptAnswer_489"></a>[Accept as answer](#)

<a id="commentAns_489"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Anyone using PeopleSoft with ordering in Polaris?](https://iii.rightanswers.com/portal/controller/view/discussion/114?)
- [Setting up a DNS ALIAS for Testing a server migration](https://iii.rightanswers.com/portal/controller/view/discussion/690?)
- [Anyone suddenly receiving this pop up error "Authentication Failure: Polaris workstation not enabled" on a PC that is used everyday??](https://iii.rightanswers.com/portal/controller/view/discussion/978?)
- [Polaris Database Backups](https://iii.rightanswers.com/portal/controller/view/discussion/413?)
- [Deleting Material Types using SQL](https://iii.rightanswers.com/portal/controller/view/discussion/323?)

### Related Solutions

- [Staff receiving "Your workstation is not registered" message when using the Polaris Export Utility](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181025085708870)
- [Disconnecting from Polaris terminal server](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930905890354)
- [How to configure the remote desktop connection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930561459679)
- [Polaris Integration with EContent Guide](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170419085906750)
- [Unable to access file from OCLC from the Polaris Virtual Cloud](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930986996441)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)