Renewals reports - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Renewals reports

0

175 views

Originally posted by inorris@lcplin.org on Wednesday, January 18th 18:00:04 EST 2017
I am trying to determine how long patrons keep their material.. so the difference between checkout date and return date on average for various collection types. Note: We have automatic renewals and I am wondering if most people return their material before they get their full number or renewals or if people return the items only when the item will no longer allow a renewal. Thanks for any help

Ingrid Norris

asked 4 years ago  by <img width="18" height="18" src="../../_resources/3c3fd3aa3532455985d17d1972789ad8.jpg"/>  support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 3 years ago

<a id="upVoteAns_138"></a>[](#)

0

<a id="downVoteAns_138"></a>[](#)

We created a report for this, but found the results were too skewed by late returns to make them usable.

edit: JT has now modified our report to exlude any returns after 90 days. We make sure to run the report at least 90 days prior. That corrected the later return and recent checkout skewing.

answered 3 years ago  by <img width="18" height="18" src="../../_resources/3c3fd3aa3532455985d17d1972789ad8.jpg"/>  rpatterson@aclib.us

- <a id="acceptAnswer_138"></a>[Accept as answer](#)

<a id="commentAns_138"></a>[Add a Comment](#)

<a id="upVoteAns_226"></a>[](#)

0

<a id="downVoteAns_226"></a>[](#)

Actually Polaris does store this because it has CKOdate and CKIdate easily available in ItemRecordHistory. Subtracting the one from the other, however, requires some fancy dancing, using grammar and functions I wasnt familiar with (and avail only in >= SQL 2012).

This gives you average days out per collection for a single branch:

; WITH CircActivityPaired
AS
(SELECT col.abbreviation as collection, ActionTakenID,
CKOdate= CAST(TransactionDate As DATE),
CKOitem = IRH.ItemRecordID,
CKIitem = Lead(IRH.ItemRecordID, 1) OVER(ORDER BY IRH.ItemRecordID, TransactionDate),
CKOtime = TransactionDate,
CKItime = Lead(TransactionDate, 1) OVER(ORDER BY IRH.ItemRecordID, TransactionDate)
FROM Polaris.ItemRecordHistory IRH
join Polaris.CIrcItemRecords CIR (nolock) on IRH.ItemRecordID=CIR.ItemRecordID
join Polaris.Collections col (nolock) on CIR.AssignedCollectionID=col.CollectionID
where ActionTakenID in (11,13) /\*cko, cki\*/
and CIR.AssignedBranchID=13
)
 
SELECT collection,
avg(DATEDIFF(day, CKOtime, CKItime)) as AvgDaysOut
FROM CircActivityPaired
WHERE ActionTakenID = 13 /\*cko\*/
  AND CKItime Is Not Null
  AND CKOitem = CKIitem
group by collection
order by collection

More verbosely, this gives it at the item level, so you can do some sanity checking and join it with fields other than collection.

; WITH CircActivityPaired
AS
(SELECT ActionTakenID,
CKOdate= CAST(TransactionDate As DATE),
CKOitem = IRH.ItemRecordID,
CKIitem = Lead(IRH.ItemRecordID, 1) OVER(ORDER BY IRH.ItemRecordID, TransactionDate),
CKOtime = TransactionDate,
CKItime = Lead(TransactionDate, 1) OVER(ORDER BY IRH.ItemRecordID, TransactionDate)
FROM Polaris.ItemRecordHistory IRH
join Polaris.CIrcItemRecords CIR (nolock) on IRH.ItemRecordID=CIR.ItemRecordID
where ActionTakenID in (11,13)
and CIR.AssignedBranchID=13
)
 
SELECT CKOitem as ItemID,
CKOdate,
CKOtime,
CKItime,
DATEDIFF(day, CKOtime, CKItime) as DaysOut
FROM CircActivityPaired
WHERE ActionTakenID = 13 /\*cko\*/
  AND CKItime Is Not Null
  AND CKOitem = CKIitem
  order by ItemID, CKOdate

answered 3 years ago  by <img width="18" height="18" src="../../_resources/3c3fd3aa3532455985d17d1972789ad8.jpg"/>  mike.najarian@reading.lib.pa.us

- <a id="acceptAnswer_226"></a>[Accept as answer](#)

<a id="commentAns_226"></a>[Add a Comment](#)

<a id="upVoteAns_57"></a>[](#)

0

<a id="downVoteAns_57"></a>[](#)

SQL to show number of renewals

Response by wosborn@clcohio.org on April 18th, 2017 at 2:57 pm

This SQL will show the average number of all renewal types and then average number of auto-renewals broken down by lend LIBRARY (not branch). It uses the item history table, which gives us data for the last 365 days, but if you purge that data more frequently, it may not be of much help.
select data.Name \[Item Owner\],
cast(avg(cast(data.Renewals as float)) as decimal(10,2)) \[AverageRenewals\],
cast(avg(cast(data.AutoRenews as float)) as decimal(10,2)) \[AverageAutoRenews\],
round(avg(cast(data.RenewalLimit as float)), 0) \[AverageRenewalLimit\]
from (
select o2.Name,
count(*) \[Renewals\],
count(case when irh.ActionTakenID = 93 then 1 end) \[AutoRenews\],
avg(cir.RenewalLimit) \[RenewalLimit\]
from polaris.polaris.ItemRecordHistory irh
join polaris.polaris.Organizations o ON
irh.AssignedBranchID = o.OrganizationID
join polaris.polaris.organizations o2 on
o.ParentOrganizationID = o2.OrganizationID
left join polaris.polaris.CircItemRecords cir ON
cir.ItemRecordID = irh.ItemRecordID
where irh.ActionTakenID in (28,73,74,76,80,82,93)
group by irh.ItemRecordID, irh.PatronID, o2.Name
) data
group by data.name
order by data.Name

answered 4 years ago  by <img width="18" height="18" src="../../_resources/3c3fd3aa3532455985d17d1972789ad8.jpg"/>  support1@iii.com

- <a id="acceptAnswer_57"></a>[Accept as answer](#)

<a id="commentAns_57"></a>[Add a Comment](#)

<a id="upVoteAns_56"></a>[](#)

0

<a id="downVoteAns_56"></a>[](#)

Vote for enhancement to record this information

Response by wosborn@clcohio.org on April 25th, 2017 at 11:39 am
I wasn't clear earlier, but incredibly Polaris does not store historic information on how long things that have been returned were checked out for. So there is no way to get that historical information if you've made a policy change.
I've opened an enhancement for this, please consider opening your own enhancement and request being added to ours which is number: 540611

answered 4 years ago  by <img width="18" height="18" src="../../_resources/3c3fd3aa3532455985d17d1972789ad8.jpg"/>  support1@iii.com

- <a id="acceptAnswer_56"></a>[Accept as answer](#)

<a id="commentAns_56"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Simply Reports Discrepancies](https://iii.rightanswers.com/portal/controller/view/discussion/960?)
- [Fiscal year expenditures report](https://iii.rightanswers.com/portal/controller/view/discussion/98?)
- [Do reporting by subject headings?](https://iii.rightanswers.com/portal/controller/view/discussion/1059?)

### Related Solutions

- [Errors accessing reports in the client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930369939404)
- [Custom report parameters not allowing multiple selections in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930424447003)
- [Same report results](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930545379375)
- [Reports returning a 503 error](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930713278993)
- [Move reporting services subscriptions to another user](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930313777401)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)