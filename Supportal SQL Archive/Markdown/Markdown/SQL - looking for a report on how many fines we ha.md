[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL - looking for a report on how many fines we have for a specific collection code and how many were waived.

0

35 views

We added a new collection that cannot be renewed and staff are mentioning they are waiving a lot of fines. Trying to get a report that will show the number of fines per a collection code.

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_be771ac1d5f24790b5eb04c09e657525.jpg"/> brandon.williams@mesaaz.gov

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Anyone have an SQL code to create a monthly report of all waived transactions by all staff members? Or know how to generate it in Simply Reports](https://iii.rightanswers.com/portal/controller/view/discussion/1193?)
- [Does anyone have an SQL report for a specific tag in Authority records?](https://iii.rightanswers.com/portal/controller/view/discussion/625?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [What are the definitions of the Waive transactions listed in the Financial Transactions Summary by Fee Reason report?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930964890520)
- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [Where are UMS reports stored?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930596768478)
- [Item Circulation by Statistical Code](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930592909894)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)