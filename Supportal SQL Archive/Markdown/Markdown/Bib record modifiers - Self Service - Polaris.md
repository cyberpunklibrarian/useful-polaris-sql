[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Bib record modifiers

0

45 views

I am looking for a way to find all users who modified a specific bib record, not just the last modifier. Any suggestions?

asked 5 months ago by <img width="18" height="18" src="../_resources/default-avatar_7f0aa3e0e5af4416b7291262d2b94eed.jpg"/> mlacoste@cityofirving.org

[cataloging](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=4#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 5 months ago

<a id="upVoteAns_1419"></a>[](#)

0

<a id="downVoteAns_1419"></a>[](#)

Possibly, but it depends on if your system is set up to log Bib Record Modified transactions.  In Polaris, head to SA -- System -- Database Tables -- Transaction Logging.  In the list of transaction types, find "Bibliographic record modified."  If it's set to "yes," we'll be able to look for it in the transaction database:

In SQL Server Management Studio, run this query, swapping out bib's control number in the last line:

Select TH.TranClientDate, PU.Name  
From PolarisTransactions.Polaris.TransactionHeaders TH  
JOIN Polaristransactions.Polaris.TransactionDetails TD  
ON TH.TransactionID = TD.TransactionID  
AND TD.TransactionSubTypeID = 36  
JOIN POlaris.PolarisUsers PU  
ON PU.PolarisUserID = TH.PolarisUserID  
WHERE TH.TransactionTypeID = 3003  
AND TD.numValue = 99999 -- Bib ID

&nbsp;

&nbsp;

&nbsp;

&nbsp;

answered 5 months ago by <img width="18" height="18" src="../_resources/default-avatar_7f0aa3e0e5af4416b7291262d2b94eed.jpg"/> mhammermeister@pinnaclelibraries.org

- <a id="acceptAnswer_1419"></a>[Accept as answer](#)

<a id="commentAns_1419"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Why isn't 'modify the headings (or references) to match the new authority heading' working?](https://iii.rightanswers.com/portal/controller/view/discussion/381?)
- [Can you change a tag in a bib record in Leap?](https://iii.rightanswers.com/portal/controller/view/discussion/1055?)
- [Use Item Record Call Numbers to Bulk Update Bib MARC Tag](https://iii.rightanswers.com/portal/controller/view/discussion/235?)
- [SQL for Bibs lacking local call #](https://iii.rightanswers.com/portal/controller/view/discussion/605?)
- [Authority Records](https://iii.rightanswers.com/portal/controller/view/discussion/580?)

### Related Solutions

- [Default Bib Records to MARC 21 Editor](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930603446551)
- [Why can't I see the "Do Not Overlay" checkbox?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=171003145914463)
- [marcgt value for 380$2](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181011195525699)
- [Bibliographic and Authority Record tag order](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930557664312)
- [Locate Bibs without attached authority records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930243668853)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)