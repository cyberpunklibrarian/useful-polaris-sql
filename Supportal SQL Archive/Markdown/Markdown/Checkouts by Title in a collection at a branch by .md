Checkouts by Title in a collection at a branch by year - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Checkouts by Title in a collection at a branch by year

0

25 views

We want to see magazine circ at the title level over the last few years at a certain branch.

While the below gets me total magazine circ by year for a branch, I'd like to add a Bib Title column to see how individual magazines have ebbed and flowed over the years: Consumer Reports, People, Us Weekly etc. 

select torg.abbreviation as TransactionBranchAbbr,polcol.abbreviation as ItemCollectionAbbr,year(th.TranClientDate) as 'Year',
count(distinct th.transactionid) as Total from PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK)
left outer join PolarisTransactions.Polaris.TransactionDetails td (nolock) on (th.TransactionID = td.TransactionID)
inner join Polaris.Polaris.Organizations torg (nolock) on (th.OrganizationID = torg.OrganizationID)
left join Polaris.Polaris.Collections polcol with (nolock) on (td.numvalue = polcol.CollectionID)
where th.TransactionTypeID = 6001 and td.TransactionSubTypeID = 61 and th.TranClientDate between '1/1/2018' and '12/31/2020 23:59:59'
and th.OrganizationID in (6)
and td.numvalue in (96)
Group by torg.abbreviation,polcol.abbreviation,year(th.TranClientDate)
Order by torg.abbreviation,polcol.abbreviation,year(th.TranClientDate)

Thanks!

Charlie Rosenthal

crosenthal@pwcgov.org

asked 7 months ago  by <img width="18" height="18" src="../../_resources/9676a43866e545de9dc737c504f5a747.jpg"/>  crosenthal@pwcgov.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 6 months ago

<a id="upVoteAns_1069"></a>[](#)

0

<a id="downVoteAns_1069"></a>[](#)

I think this should give you what you're looking for, though it won't include any deleted items:

`select br.BrowseTitle [Title]`
`,o.abbreviation as TransactionBranchAbbr`
`,c.abbreviation as ItemCollectionAbbr`
`,year(th.TranClientDate) as 'Year'`
`,count(distinct th.transactionid) as Total`
`from PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK)`
`left outer join PolarisTransactions.Polaris.TransactionDetails td_c (nolock)`
`on td_c.TransactionID = th.TransactionID and td_c.TransactionSubTypeID = 61`
`inner join Polaris.Polaris.Organizations o (nolock)`
`on th.OrganizationID = o.OrganizationID`
`left join Polaris.Polaris.Collections c with (nolock)`
`on td_c.numvalue = c.CollectionID`
`join PolarisTransactions.polaris.TransactionDetails td_item`
`on td_item.TransactionID = th.TransactionID and td_item.TransactionSubTypeID = 38`
`join Polaris.Polaris.CircItemRecords cir`
`on cir.ItemRecordID = td_item.numValue`
`join Polaris.polaris.BibliographicRecords br`
`on br.BibliographicRecordID = cir.AssociatedBibRecordID`
`where th.TransactionTypeID = 6001`
`and th.TranClientDate between '1/1/2021' and '1/1/2021 23:59:59'`
`and th.OrganizationID in (6)`
`and td_c.numvalue in (96)`
`Group by o.abbreviation,c.abbreviation,year(th.TranClientDate), br.BrowseTitle`
`Order by br.BrowseTitle,o.abbreviation,c.abbreviation,year(th.TranClientDate)`

answered 6 months ago  by <img width="18" height="18" src="../../_resources/9676a43866e545de9dc737c504f5a747.jpg"/>  mfields@clcohio.org

- <a id="acceptAnswer_1069"></a>[Accept as answer](#)

<a id="commentAns_1069"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)
- [Fiscal year expenditures report](https://iii.rightanswers.com/portal/controller/view/discussion/98?)
- [Checkout length by checkin date](https://iii.rightanswers.com/portal/controller/view/discussion/877?)
- [SQL to Find all bibs with same title.](https://iii.rightanswers.com/portal/controller/view/discussion/675?)
- [SQL top bib checkouts between 2 dates](https://iii.rightanswers.com/portal/controller/view/discussion/650?)

### Related Solutions

- [Collection workform "branches" checkboxes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930545936496)
- [Opening Day Collection (ODC) budget question](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930893811503)
- [Opening Day Collections](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160503113356129)
- [Change text on checkout receipts](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930905424507)
- [Which line of the Loan Periods table is used?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170413110944913)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)