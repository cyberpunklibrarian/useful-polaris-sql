[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Re-importing MARCIVE records after clean-up

0

67 views

We're planning to have MARCIVE do some record clean-up and deduplication.  I'm realizing that the re-import process will be somewhat tricky, especially with the deduplicated records.  Has anyone had experience with this?

&nbsp;

Thanks!

&nbsp;

Bill Taylor

Western Md. Regional Library

[wtaylor@washcolibrary.org](mailto:wtaylor@washcolibrary.org)

&nbsp;

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_5f7ee4eedd5a4de3af4f1f818d36aabd.jpg"/> wtaylor@washcolibrary.org

[import](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=324#t=commResults) [marcive](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=183#t=commResults) [polaris](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=7#t=commResults) [reimport](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=325#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 4 years ago

<a id="upVoteAns_820"></a>[](#)

0

<a id="downVoteAns_820"></a>[](#)

Hi Bill, 

I don't have experience with deduplication, but we have done authority record clean up several times with LTI (who went out of business last year). This involved importing a large number of bibs to overlay what was already in there. We started by exporting all of our bib records and sending them off for updates. When I got the new records back, I used a tool called MarcEdit (free) to copy the 001 to the 035 field. I also used this tool to split the file of bibs into smaller batches. I then uploaded the batches into polaris during off hours, about 9000 at a time. We always call the period when we send off records to when we load them back the 'gap' period. During that time, we are sure not to purge or modify any bibs. If you do, then any changes will be overwritten (or bibs added back in) when you reload the bibs. Of course, you probably know that when you import, it will essecially delete the old record and create a new record (as opposed to overlaying it), so the control number does change. I feel like I heard this might change in a future upgrade though, but I'm not totally sure. 

&nbsp;

Hope this helps! If you are interested in my documentation, let me know. 

Thanks, Desiree

ddavey@daytonmetrolibrary.org

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_5f7ee4eedd5a4de3af4f1f818d36aabd.jpg"/> ddavey@daytonmetrolibrary.org

- <a id="acceptAnswer_820"></a>[Accept as answer](#)

6.3 (June 2019) did change the way bib overlays work. The bib control number is now retained. So basically, the exisiting bib data is all removed and then replaced by the incoming bib data.

— wosborn@clcohio.org 4 years ago

Calling it the gap period is a great idea! And it is an important part of the process.

— wosborn@clcohio.org 4 years ago

<a id="commentAns_820"></a>[Add a Comment](#)

<a id="upVoteAns_819"></a>[](#)

0

<a id="downVoteAns_819"></a>[](#)

Hi Bill. We have not done this specific project, but I have done some other cleanup projects involving exporting, modifying and importing records in the past.

Updating records with a 1-to-1 match (using 001) should be fairly straightforward, but deduplication may be tricker.

If you try to update records based on a standard number, Polaris will save the record as provisional if it matches more than one record. If you start with two duplicate records, each with its own ISBN and load a deduped record with both ISBNS, you will probably end up with a third, provisional record.

At that point, you could manually save each provisional record and replace both of the duplicate records. Depending on how many duplicates you have, this may be easy or onerous.

III may be able to do a custom dedupe, via SQL but there are many places where a table links to a BibliographicRecordID (Control Number) so it's not something you would want to do on your own.

You may want to ask MARCIVE to send separate files for updates and deduplication. That way you could handle each part separately.

Also, if you have holdings set in OCLC, you will want to make sure you have the OCLC numbers that go away as part of the deduplication process. MARCIVE may be able to provide this information, but if they cannot you can: (1) Load the dedupe file(s) set to match on the 001 field. (2) Save each record (if not too onerous) and replace the "other" duplicate(s). (3) Pull OCLC numbers from the deleted duplicate records to batch unset OCLC holdings.

Hope this helps, some.

JT

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_5f7ee4eedd5a4de3af4f1f818d36aabd.jpg"/> jwhitfield@aclib.us

- <a id="acceptAnswer_819"></a>[Accept as answer](#)

<a id="commentAns_819"></a>[Add a Comment](#)

<a id="upVoteAns_818"></a>[](#)

0

<a id="downVoteAns_818"></a>[](#)

When we've done it before it was a custom data service to do the reload with Polaris/Innovative. Maybe it is possible to do it yourself if you only have a small number of records, but I would think it would be very difficult. 

Also when we did it before we didn't have the ability to de-dup based on the Polaris control number. It might be more feasible now. 

How many records did you have to process?

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_5f7ee4eedd5a4de3af4f1f818d36aabd.jpg"/> wosborn@clcohio.org

- <a id="acceptAnswer_818"></a>[Accept as answer](#)

<a id="commentAns_818"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Is anyone using MARCIVE for authority control now? I know there were some problems in the past. Curious to know if any one is using their service with success...](https://iii.rightanswers.com/portal/controller/view/discussion/392?)
- [Does anyone use Proficio for archives management and export its MARC records to import into Polaris?](https://iii.rightanswers.com/portal/controller/view/discussion/1098?)
- [Is there a way to import the records back into the system and only import a tag that then goes over onto the existing record?](https://iii.rightanswers.com/portal/controller/view/discussion/1150?)
- [For the libraries that pay for zmarc, what setting do you select for Perform Authority Control in the Bibliographic Record tab in your import profiles? And why?](https://iii.rightanswers.com/portal/controller/view/discussion/475?)
- [How to you correct a terminated import job?](https://iii.rightanswers.com/portal/controller/view/discussion/131?)

### Related Solutions

- [Imports running, but not importing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930268116922)
- [Import Profiles and Item Record Creation](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930457734892)
- [How to import deleted records / bulk delete with an import](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200501085203736)
- [Importing Weekly Authority Update records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930310842441)
- [Why can't I see the "Do Not Overlay" checkbox?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=171003145914463)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)