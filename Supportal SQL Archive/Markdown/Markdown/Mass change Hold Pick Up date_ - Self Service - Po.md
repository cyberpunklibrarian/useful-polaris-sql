[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Mass change Hold Pick Up date?

0

134 views

Originally posted by tbaxter@guelphpl.ca on Wednesday, June 14th 10:13:45 EDT 2017  
<br/>Hello, We recently upgraded to 5.2, but had some issues with our email notifications. So a lot of patrons did not receive their hold pickup emails. Does anyone know how I can extend the date on these? It is a few days worth.

Thanks,

Tom

asked 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_2303ff81ca934ee1910b600c6c6a70cd.jpg"/> support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 7 years ago

<a id="upVoteAns_94"></a>[](#)

0

<a id="downVoteAns_94"></a>[](#)

Hello:

Once these have been triggered it is next to impossible to chnage these, unless you want to go into each individual patron and chnage them.  Polaris doe not recommend changing this via SQL.

Vincent Kruggel

vkruggel@orl.bc.ca

answered 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_2303ff81ca934ee1910b600c6c6a70cd.jpg"/> vkruggel@orl.bc.ca

- <a id="acceptAnswer_94"></a>[Accept as answer](#)

<a id="commentAns_94"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Can you get a list of exact dates an item was placed on hold?](https://iii.rightanswers.com/portal/controller/view/discussion/1031?)
- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [Checkout length by checkin date](https://iii.rightanswers.com/portal/controller/view/discussion/877?)
- [SQL for changes to a patron record](https://iii.rightanswers.com/portal/controller/view/discussion/840?)
- [How to store date that report was last run?](https://iii.rightanswers.com/portal/controller/view/discussion/1085?)

### Related Solutions

- [Combined In-Transit and Hold Slip](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161005093304601)
- [Hold Pick-Up Slip - Tag](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161005084041146)
- [Hold Pick-Up Slip (Horizontal)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161013115756742)
- [Hold Pick-Up Slip (Vertical 2)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161025092049109)
- [Item Held Longer Than Expected](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930398757933)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)