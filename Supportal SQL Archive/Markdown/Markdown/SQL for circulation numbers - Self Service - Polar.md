[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for circulation numbers

0

561 views

Originally posted by musack@bcls.lib.nj.us on Wednesday, June 7th 09:50:26 EDT 2017  
<br/>Hi, We're being asked to provide circulation number for specific Dewey call numbers (e.g., 001-006, 600-629) for specific time periods (4/1/17 - 5/26/17 and 4/1/16 - 5/26/16). I've looked in Simply Reports (we don't have Item History Reports) and Reports and Notices and have not been able to find anything. Does anybody have any suggestions as to how to accomplish this? Perhaps a SQL query we could use?

asked 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_f5bd8e397aed4f5195a922225574c158.jpg"/> support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 5 years ago

<a id="upVoteAns_473"></a>[](#)

0

<a id="downVoteAns_473"></a>[](#)

I'm in need of similar information, and would love to have a SQL query for it if anyone has one.

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_f5bd8e397aed4f5195a922225574c158.jpg"/> mgregory@cc-pl.org

- <a id="acceptAnswer_473"></a>[Accept as answer](#)

<a id="commentAns_473"></a>[Add a Comment](#)

<a id="upVoteAns_95"></a>[](#)

0

<a id="downVoteAns_95"></a>[](#)

Hello:

If you want information for just the last year then the Polaris.ItemRecordHistory table will be the easiest place to start a query.  If you need something durable that will be able to scan beyond the last year then the Transacation database is where you need to start from.

If you want we can talk and I can give you some pointers.  There is quite a bit involved if you have never been to this great place.

Vincent Kruggel

vkruggel@orl.bc.ca

&nbsp;

answered 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_f5bd8e397aed4f5195a922225574c158.jpg"/> vkruggel@orl.bc.ca

- <a id="acceptAnswer_95"></a>[Accept as answer](#)

<a id="commentAns_95"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Need an sql for circulation by workstation between two dates](https://iii.rightanswers.com/portal/controller/view/discussion/159?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [Does anyone have an SQL that can get the circulation counts from either funds or invoice line items in a certain fiscal year that will count deleted items circulation?](https://iii.rightanswers.com/portal/controller/view/discussion/1397?)
- [Has anyone developed a SimplyReport/SQL Report to answer PLA's Public Library Data Service circulation questions?](https://iii.rightanswers.com/portal/controller/view/discussion/439?)
- [Does any one have a Simply Reports or SQL to find duplicate driver's license numbers (user defined field 1)?](https://iii.rightanswers.com/portal/controller/view/discussion/1391?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Patron registration numbers jumped in value](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989626569)
- [How do I reset the year-to-date circulation counts?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930831368342)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [Where are the waive and charge free text notes stored in the database?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930334007926)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)