SQL for patron checkouts at a specific location - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SQL for patron checkouts at a specific location

0

99 views

We're trying to figure out a way to determine how many unique patrons are using our Mail service. Items are never assigned to the "Mail" location, so I can't use Simply Reports to pull all transactions using Item Assigned Location, and patrons from all of our branches have the option of using our Mail service, so patrons are not assigned to a "Mail" branch. In Simply Reports, I cannot pull by check-out location, only check-in location. I can pull a list of all items that were checked out in a specific time period and pull out those that were checked out from the Mail location (using Item check out branch), but that will not give me patron information. I can get a total number of items checked out via Mail using various reports in Simply Reports or Reports and Notices, but it doens't tell me which patron checked the item out. Does anybody have any ideas or an SQL query I can use to get this information?

asked 3 years ago  by <img width="18" height="18" src="../../_resources/3733dfeacc4e47d29b2a47f37b80bb87.jpg"/>  musack@bcls.lib.nj.us

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 3 years ago

<a id="upVoteAns_268"></a>[](#)

0

<a id="downVoteAns_268"></a>[](#)

Wow! Thank you so much :-) We'll definitely give this a try. I really appreciate your help and your quick response.

answered 3 years ago  by <img width="18" height="18" src="../../_resources/3733dfeacc4e47d29b2a47f37b80bb87.jpg"/>  musack@bcls.lib.nj.us

- <a id="acceptAnswer_268"></a>[Accept as answer](#)

<a id="commentAns_268"></a>[Add a Comment](#)

<a id="upVoteAns_267"></a>[](#)

0

<a id="downVoteAns_267"></a>[](#)

Here is a query that should work:

You will need to modify the OrganizationID to your Mail branch and the dates to cover the date range (first day you want and the first day after the period you want).

There are some notes below the query (same font, bold, green) describing changes to make to account for different types of checkouts.  (Do not include them in the query.)

Below that is a query to count distinct patrons. To verify that this is accurate, you probably want to run both queries. I believe you can remove duplicate patron barcodes in Excel and see how many are left.

Hope this helps,

JT

select irh.TransactionDate
, pat.Barcode as PatronBarcode
, it.Barcode as ItemBarcode
, org.Name
from polaris.polaris.ItemRecordHistory irh with (nolock)
join Polaris.Polaris.Patrons pat (nolock)
on (irh.PatronID = pat.PatronID)
join Polaris.Polaris.Organizations org (nolock)
on (irh.OrganizationID = org.OrganizationID)
join Polaris.Polaris.ItemRecords it (nolock)
on (irh.ItemRecordID = it.ItemRecordID)
where irh.ActionTakenID = 13
and irh.OrganizationID = 7
and irh.TransactionDate >= '05/01/2018'
and irh.TransactionDate < '06/01/2018'

**The ActionTakenID (13) is for checkouts. Renewals are a separate action, so a renewal of a mailed item should not be counted. If you need those, the ID is 28.
If you use Bulk Check out via Outreach Services, the ID is 44.
The ID for Bulk Check out via Borrow by Mail is 45.
The ID for Checked out by associated patron is 91.
If you do offline checkouts "at" the Mail branch, the ID for regular checkout is 77 and for a checkout by an associated patron is 90.
To make sure which types of checkouts you have, you can run the query for each, or change the line with "where ActionTakenDescription = 13" to "where ActionTakenDescription in (13,44,45,90,91)" to get all types of checkouts except renewals and "...13,28,44,45,90,91)" to include renewals.**

---Below is the query for the count.

select count(distinct irh.PatronID) as PatronCount
from polaris.polaris.ItemRecordHistory irh with (nolock)
where irh.ActionTakenID = 13
and irh.OrganizationID = 7
and irh.TransactionDate >= '05/01/2018'
and irh.TransactionDate < '06/01/2018'

answered 3 years ago  by <img width="18" height="18" src="../../_resources/3733dfeacc4e47d29b2a47f37b80bb87.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_267"></a>[Accept as answer](#)

<a id="commentAns_267"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Identifying a count of patrons, by registered library, whose only activity over a period of time is OverDrive checkout through Polaris integration?](https://iii.rightanswers.com/portal/controller/view/discussion/1044?)
- [You saved feature on patron checkout receipts](https://iii.rightanswers.com/portal/controller/view/discussion/509?)
- [Report for patrons with items out. If possible, sort by location?](https://iii.rightanswers.com/portal/controller/view/discussion/779?)
- [SQL for Patron Name Titles](https://iii.rightanswers.com/portal/controller/view/discussion/836?)
- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)

### Related Solutions

- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [Locating patrons using a specific address type](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180628133452068)
- [Place a block on a patron account](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930900161876)
- [How do I find a deleted patron in SQL?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930727145951)
- [Error: GetPatronPreCirc() failed](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170922112726384)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)