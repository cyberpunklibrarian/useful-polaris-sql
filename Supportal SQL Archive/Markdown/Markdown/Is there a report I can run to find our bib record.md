Is there a report I can run to find our bib records that contain an 852 field? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Is there a report I can run to find our bib records that contain an 852 field?

0

82 views

Is there a canned report or simply report I can run to create a record set of bibs containg an 852 line?

TIA!

asked 3 years ago  by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg)  sbills@lpld.lib.in.us

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 3 years ago

<a id="upVoteAns_355"></a>[](#)

0

<a id="downVoteAns_355"></a>[](#)

Answer Accepted

I don't think there's a canned report like this, nor do I know of a way to do it in Simply. You'd have to have access to your database, or at the very least, SQL access within the Staff Client Find Tool. I tried this query in SQL Studio and the Find Tool and it worked both places, so I *think* it might be close to what you're after? And, of course, if you run it in Find Tool you can export those results into a bib record set.

edit: Forgot to add the NOLOCK to the table. Updated.

`SELECT DISTINCT`

`BibliographicRecordID`

`FROM`

`Polaris.BibliographicTagsAndSubfields_View (NOLOCK)
`

`WHERE TagNumber = 852`

`AND DATALENGTH(Data) != 0`

answered 3 years ago  by ![danielmesser@mcldaz.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_danielmesser@mcldaz.org20180523162210.jpg)  danielmesser@mcldaz.org

<a id="commentAns_355"></a>[Add a Comment](#)

<a id="upVoteAns_354"></a>[](#)

1

<a id="downVoteAns_354"></a>[](#)

We use this to find the 856 tag with subfield 'u':

SELECT DISTINCT BibliographicRecordID AS RecordID
  from bibliographictags BT (NOLOCK)
  JOIN BibliographicSubfields BS (NOLOCK) ON
                 BT.BibliographicTagID = BS.BibliographicTagID
  WHERE TagNumber = 856
  AND Subfield = 'u'

**You could change the tag number to 852 and edit or delete the subfield.**

answered 3 years ago  by <img width="18" height="18" src="../../_resources/643bb04f7ac84b889402371ceaa60150.jpg"/>  akrulik@bcls.lib.nj.us

- <a id="acceptAnswer_354"></a>[Accept as answer](#)

<a id="commentAns_354"></a>[Add a Comment](#)

<a id="upVoteAns_356"></a>[](#)

0

<a id="downVoteAns_356"></a>[](#)

Thank you both, so much! :-)

answered 3 years ago  by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg)  sbills@lpld.lib.in.us

- <a id="acceptAnswer_356"></a>[Accept as answer](#)

<a id="commentAns_356"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Is it possible to save a record set when running a saved report in Simply Reports?](https://iii.rightanswers.com/portal/controller/view/discussion/979?)
- [Simply Reports - formatting when comments fields are included.](https://iii.rightanswers.com/portal/controller/view/discussion/128?)
- [I'm looking for a report that will tell me how many bib records a staff member has created and/or modified in a month.](https://iii.rightanswers.com/portal/controller/view/discussion/942?)

### Related Solutions

- [Run SimplyReports Scheduled Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930277327637)
- [Reports and Notices not running](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930295540365)
- [Statistical Summary Report Bin Record Totals](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930459959777)
- [Errors accessing reports in the client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930369939404)
- [Where to find Patron Circ Count in Patron record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930283871188)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)