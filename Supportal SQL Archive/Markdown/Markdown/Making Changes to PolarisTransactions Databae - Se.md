[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Making Changes to PolarisTransactions Databae

0

86 views

Originally posted by jlellelid@sno-isle.org on Wednesday, March 8th 10:03:02 EST 2017  
<br/>I would like to know if anyone has made changes to their PolarisTransactions database. What are the risks? Were you successful and willing to share any code that you developed or point me to documentation? We sent out a workstation to be used as a Polaris ExpressCheck machine at a larger branch. It had been in use as an ExpressCheck at a smaller branch. The Workstation Name and Branch had been changed, but the machine was not rebooted to reflect the change in branch before it was put into use. Transactions at the larger branch were attributed to the smaller branch. Our data team wants the transactions updated to reflect that they took place at the larger branch so that statistics will not be skewed/incorrect? What are the risks? Were you successful and willing to share any code that you developed. Polaris/III’ has indicated that they “cannot support or assist with any modification to the Transactions database” as it is an historical record of what occurred during the course of business. Some thoughts I have had: Use the Copy Database Wizard in SSMS (we are on SQL Server 2014 Only copy the data from 2017. Use the test database to practice.

Thank you.

Jon Lellelid jlellelid@sno-isle.org

asked 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_56d1ce65941b48b480039a176f7340de.jpg"/> support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 7 years ago

<a id="upVoteAns_85"></a>[](#)

0

<a id="downVoteAns_85"></a>[](#)

Hello:

I have querried the database and isolated all transaction for staff, specific holds etc.  The database does not have any triggers so I see no reason why an update statement could not be used to change the OrganizationID within the Polaris.TransactionHeaders table for all transactions involving the units specific Workstation ID from the date in question.

Try the changes on your training database and see how it pans out.

I suspect that you have solved this problem already, but if you have not then you have something more to work with.

If you ever need anything let me know.

Vincent Kruggel

Systems Analyst

Okanagan Regional Library

vkruggel@orl.bc.ca

answered 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_56d1ce65941b48b480039a176f7340de.jpg"/> vkruggel@orl.bc.ca

- <a id="acceptAnswer_85"></a>[Accept as answer](#)

<a id="commentAns_85"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Changing patron records from Regular Case to UPPERCASE](https://iii.rightanswers.com/portal/controller/view/discussion/1303?)
- [Is there a way to set Polaris to change expiration dates and address checks for new cards based on the patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/1213?)
- [Outreach: Included Branches](https://iii.rightanswers.com/portal/controller/view/discussion/116?)
- [SQL for waiving certain fines](https://iii.rightanswers.com/portal/controller/view/discussion/192?)
- [Fees waived by username](https://iii.rightanswers.com/portal/controller/view/discussion/1123?)

### Related Solutions

- [How can I find all the transaction types and subtypes used in Polaris?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930580777417)
- [We need to update a metadata datastream for all objects in Vital, can we match and overlay?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170921174742021)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [How to make a Supportal contact inactive](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160321060009377)
- [Making Program Data folder Visible in Terminal Server](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930865139706)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)