[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Investigating fine free -- need some help with reports director is asking for

0

74 views

Below I have listed the information my director is wanting. I ran reports in SimplyReports and canned reports, but I'm questioning whether I truly got JUST the late fines, and not the billed fees. If you could tell me how you would run each report, I would REALLY appreciate it. I want to make sure I am providing the most accurate information. TIA

Number of cards with outstanding late fees (not billed items).  

Number of cards with outstanding late fees blocking use of card. 

The number from above broken down by kids/adults. 

Total amount outstanding from late fees. 

Average amount of late fees per card. 

Total amount received in late fee collections during the first quarter of this year.

How many of these accounts are expired?

&nbsp;

asked 1 year ago by ![syoung@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_syoung@lpld.lib.in.us20231219095540.jpg) syoung@lpld.lib.in.us

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_1357"></a>[](#)

0

<a id="downVoteAns_1357"></a>[](#)

Answer Accepted

You may be able to coax the info out of SimplyReports as well, but I think this SQL should also do the trick:

```javascript
select	 count(distinct case when datediff(hour, pr.BirthDate, getdate()) / 8766 >= 18 then pa.PatronID end) [AdultsWithOverdues]
		,count(distinct case when datediff(hour, pr.BirthDate, getdate()) / 8766 < 18 then pa.PatronID end) [JuvenilesWithOverdues]
		,count(distinct case when pr.Birthdate is null then pa.PatronID end) [NoBirthDateWithOverdues]
		,count(distinct pa.PatronID) [TotalPatronsWithOverdues]
		,sum(pa.OutstandingAmount) [OutstandingOverdues]
		,cast(sum(pa.OutstandingAmount) / count(distinct pa.PatronID) as decimal(18,2)) [AverageOverduesPerPatron]
		,count(distinct case when pr.ExpirationDate < getdate() then pa.PatronID end) [ExpiredPatronsWithOverdues]
from polaris.polaris.PatronAccount pa (nolock)
join polaris.polaris.PatronRegistration pr (nolock)
	on pr.PatronID = pa.PatronID
where pa.TxnCodeID = 1
	and pa.FeeReasonCodeID = 0
	and pa.OutstandingAmount > 0

select sum(pa.TxnAmount) [FinePaymentsReceivedLast4Months]
from polaris.polaris.PatronAccount pa (nolock)
where pa.TxnCodeID = 2
	and pa.FeeReasonCodeID = 0
	and pa.TxnDate > dateadd(month, -4, getdate())


select count(distinct d.PatronID) [NumPatronsBlockedByFines]
from (
	select p.PatronID
	from polaris.polaris.PatronAccount pa (nolock)
	join polaris.polaris.patrons p (nolock)
		on p.PatronID = pa.PatronID
	join polaris.polaris.PatronLoanLimits pll (nolock)
		on pll.OrganizationID = p.OrganizationID and pll.PatronCodeID = p.PatronCodeID
	where pa.TxnCodeID = 1
		and pa.OutstandingAmount > 0
		and pa.FeeReasonCodeID in (0)
	group by p.PatronID
	having sum(pa.OutstandingAmount) >= max(pll.MaxFine)
) d
```

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_c71e01850d2846c3bd19df05a107d653.jpg"/> mfields@clcohio.org

<a id="commentAns_1357"></a>[Add a Comment](#)

<a id="upVoteAns_1355"></a>[](#)

0

<a id="downVoteAns_1355"></a>[](#)

For most of this, I would just use SQL but in the absence of that ability, I think you should be able to run some patron account list reports in SimplyReports and use pivot tables in Excel to get the nubmers you want.

&nbsp;

You might also want to head over to the IUG site and their discussion forums for questions like this in the future!  We'd love to have you there.

&nbsp;

https://www.innovativeusers.org/

answered 1 year ago by <img width="18" height="18" src="../_resources/default-avatar_c71e01850d2846c3bd19df05a107d653.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_1355"></a>[Accept as answer](#)

<a id="commentAns_1355"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Fiscal year expenditures report](https://iii.rightanswers.com/portal/controller/view/discussion/98?)
- [Circulation report by patrons age](https://iii.rightanswers.com/portal/controller/view/discussion/97?)
- [Where can I find the rdl for receipts (Leap and the client)](https://iii.rightanswers.com/portal/controller/view/discussion/136?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)

### Related Solutions

- [My library needs usage statistics for our Vega Discover catalog](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=230227144428060)
- [Supportal roles](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160202200228655)
- [Innovative's HTTPS Everywhere Plan](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180920175535460)
- [Innovative's incident response to CVE-2021-44228 Log4Shell: RCE 0-day Exploit](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=211211170729667)
- [Auto-Renew basics and FAQ](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930279224783)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)