SQL for total "You Saved" amount since quarantine began - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=13)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Leap](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=13)

# SQL for total "You Saved" amount since quarantine began

0

69 views

I would like to provide my Administration (and possibly the community) with the total "you saved" by patrons since our library closed for COVID-19.  I think it might be beneficial for demonstrating that the library is still providing a huge service even if we aren't hosting people on site like we used to.  

asked 1 year ago  by <img width="18" height="18" src="../../_resources/b31b284b2b234edcb496153c22f7ac99.jpg"/>  slewis@nolalibrary.org

["you saved"](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=31#t=commResults) [hosted](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=290#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_955"></a>[](#)

0

<a id="downVoteAns_955"></a>[](#)

It seems "You Saved" info is only stored in the database as year-to-date amounts, so there's not a simple way to get the amount by date range.

That said, some testing shows that "YTD You Saved" amounts are updated immediately on checkout, so you could get an approximate total using ItemRecordDetails and ItemRecordHistory, with some caveats: 1) "You saved" amounts would disappear as items are purged from the database, since the item record history would get purged as well, and 2) the query would only work for physical, barcoded items (not ephemeral items, and nothing downloadable due to the squirrelly way Polaris and OverDrive work together).

Checking YTD amounts from 10/1/2019 through to today using the query below, then comparing that total to a sum of the YTD amounts from Patron tables, I got a difference of 0.05%.  That seems reasonable, given that item record history disappears from the database as the item records themselves get purged.

This should work.  I'd probably go with a "You saved more than" phrasing since it would be underestimating based on item deletions:

SELECT sum(ird.Price) as "YouSaved"
FROM ItemRecordHistory irh WITH (NOLOCK)
JOIN CircItemRecords cir WITH (NOLOCK)
ON irh.ItemRecordID = cir.ItemRecordID
JOIN Collections col with (NOLOCK)
ON col.CollectionID = cir.AssignedCollectionID
JOIN ItemRecordDetails ird with (NOLOCK)
ON irh.ItemRecordID = ird.ItemRecordID
WHERE irh.ActionTakenID IN (13,44,45,75,77,81,89,90,91)
AND cir.AssignedCollectionID IN (@Collection)
AND irh.TransactionDate BETWEEN @From AND @Through + ' 11:59:59 PM'

Instead of @Collection, you could simply list all the collection numbers you're interested in, if you have them (e.g. "(13,14,15,18)" etc.)  Be sure to exclude whatever collection you're using for digital/downloadable items; Polaris can't be trusted for those numbers.

If this won't work for you, or if you have any other questions, please let me know and I'll be happy to help.

answered 1 year ago  by <img width="18" height="18" src="../../_resources/b31b284b2b234edcb496153c22f7ac99.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_955"></a>[Accept as answer](#)

<a id="commentAns_955"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Running an SQL in Leap](https://iii.rightanswers.com/portal/controller/view/discussion/47?)
- [Newbie question - Leap and scanning](https://iii.rightanswers.com/portal/controller/view/discussion/922?)
- [Leap Offline Procedures](https://iii.rightanswers.com/portal/controller/view/discussion/635?)
- [Welcome to the Polaris Forum!](https://iii.rightanswers.com/portal/controller/view/discussion/44?)
- [Bluetooth scanners & LEAP](https://iii.rightanswers.com/portal/controller/view/discussion/42?)

### Related Solutions

- [What is required for Leap?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930646331533)
- [Leap offline overview and FAQ](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=190807164520143)
- [Polaris Resources to Close Physical Locations - Webinar Slides](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200325142658499)
- [Polaris Resources to Reopen Physical Locations - Webinar Slides](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200512173750650)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)