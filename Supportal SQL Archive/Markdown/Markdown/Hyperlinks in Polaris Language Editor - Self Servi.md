[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Hyperlinks in Polaris Language Editor

0

96 views

Originally posted by jennifer.pelton@iii.com on Friday, February 10th 15:39:39 EST 2017  
<br/>Can hyperlinks be applied to Polaris Language Editor strings? Example "Please contact the library or visit http://www.sno-isle.org/?ID=1171. " could be represented as " Please contact the library or click here (where "here" would be underlined/embedded with the link above."

Thank you.

Jon Lellelid Sno-Isle Libraries jlellelid@sno-isle.org

asked 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_46dd721c002047e59ef251054df15db5.jpg"/> support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 7 years ago

<a id="upVoteAns_70"></a>[](#)

0

<a id="downVoteAns_70"></a>[](#)

RE: Here is an example of hyper text in the PowerPAC section of the Language editor

Response by ggosselin@richlandlibrary.com on February 2nd, 2017 at 9:52 am

Hi Carl,  
<br/>It looks like the Supportal stripped out all of your greater than/lesser than symbols! I ran into that issue with a SQL query I was trying to share

answered 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_46dd721c002047e59ef251054df15db5.jpg"/> support1@iii.com

- <a id="acceptAnswer_70"></a>[Accept as answer](#)

<a id="commentAns_70"></a>[Add a Comment](#)

<a id="upVoteAns_69"></a>[](#)

0

<a id="downVoteAns_69"></a>[](#)

Here is an example of hyper text in the PowerPAC section of the Language editor

Response by carl.ratz@phoenix.gov on February 1st, 2017 at 5:19 pm

Your digital audiobook has been successfully checked out.brbrYou may download it now using yourbrAxis 360 app or a href="http://lib.axis360.baker-taylor.com/AppZone/ReaderList" target="\_blank"Click here/a to get the Axis 360 app for your device.

answered 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_46dd721c002047e59ef251054df15db5.jpg"/> support1@iii.com

- <a id="acceptAnswer_69"></a>[Accept as answer](#)

<a id="commentAns_69"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris Database Backups](https://iii.rightanswers.com/portal/controller/view/discussion/413?)
- [Server Busy](https://iii.rightanswers.com/portal/controller/view/discussion/278?)
- [Polaris 6.5 and Windows 7](https://iii.rightanswers.com/portal/controller/view/discussion/746?)
- [Welcome to the Polaris Forum!](https://iii.rightanswers.com/portal/controller/view/discussion/112?)
- [Is anyone experienced latency issues when running the Polaris client?](https://iii.rightanswers.com/portal/controller/view/discussion/974?)

### Related Solutions

- [Where can documentation of customizable language strings be found?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930659606814)
- [Language menu strings for multi-language telephony](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161221424154166)
- [System Administration Overview--Z39.50, PAC Customization and Language Editor](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160908152921635)
- [Training - 5.0 SP3 Language Editor.pptx](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160505142220723)
- [Language string for PAC request block](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930713096546)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)