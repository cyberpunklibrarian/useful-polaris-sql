[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Is there still a SQL repository for the Polaris forum?

0

116 views

I am just trying to dig through SQL queries others have created to get a starting point... Lots of great code out there.

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_f89d537e025b4f60ac675a982692a9d1.jpg"/> brandon.williams@mesaaz.gov

[sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Welcome to the Polaris Forum!](https://iii.rightanswers.com/portal/controller/view/discussion/89?)
- [SQL query to export specific record set: Polaris 4.1R2](https://iii.rightanswers.com/portal/controller/view/discussion/93?)
- [Does any one have a Simply Reports or SQL to find duplicate driver's license numbers (user defined field 1)?](https://iii.rightanswers.com/portal/controller/view/discussion/1391?)
- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)
- [No \*\*IUG Forum\*\* post left behind in Q1 of 2023](https://iii.rightanswers.com/portal/controller/view/discussion/1292?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Polaris Forum Webinar](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180711151717136)
- [IUG 2019: Leap Feedback Forum](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=190521102343479)
- [Polaris Database Repository (Version 5.6)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180313130034560)
- [Polaris Database Repository (Version 5.5)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=171013130008229)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)