[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# SMS Carrier Cricket Gateway

0

84 views

Originally posted by carl.ratz@phoenix.gov on Friday, February 17th 12:14:03 EST 2017  
<br/>Just chated with Cricket phone support about the @sms.mycricket.com and or .net address for sending TXT messages. I was told that one is no longer any good. (anyone else confirm that?) So these MMS (SMS) addresses are used by Cricket phone. @mms.cricketwireless.net and the newest one @mms.aiowireless.net Question for anyone. Did you update the carrier table and change the mycricket to aio? or did you add a new carrier for aio and in sql update each account with that one and removed old one from the table? I think the mycricket needs to be removed. I see I can hide it, but how does it affect the existing customer?

asked 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_ce0991b4cf0643fa9b97bb20cb2e0237.jpg"/> support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 7 years ago

<a id="upVoteAns_41"></a>[](#)

1

<a id="downVoteAns_41"></a>[](#)

RE: Need Clarification

Response by carl.ratz@phoenix.gov on May 17th, 2017 at 1:51 pm

After contacting Cricket support as well as talking to individual Cricket people, I found the company and representatives most un-helpful. They could not give a definitive answer as to the status of their own SMS gateways. I was however able to get enough evidence to support the research that \*.\*MyCricket\*.\* is indeed a bad address. I was able to get one of my staff who belonged to Cricket to test with me. The fail logs have almost completely gone away after changing MyCricket to @mms.cricketwireless.net. I also still have mms.aiowireless.net in place as suggested. Our records show less then a handful of customers using that address. I have not had any complaints with that one. So it may be good.

answered 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_ce0991b4cf0643fa9b97bb20cb2e0237.jpg"/> support1@iii.com

- <a id="acceptAnswer_41"></a>[Accept as answer](#)

<a id="commentAns_41"></a>[Add a Comment](#)

<a id="upVoteAns_40"></a>[](#)

0

<a id="downVoteAns_40"></a>[](#)

Need Clarification

Response by jlellelid@sno-isle.org on May 17th, 2017 at 1:19 pm

Hi Carl,  
<br/>You comment: "@sms.mycricket.com and or .net address for sending TXT messages. I was told that one is no longer any good. (anyone else confirm that?)"  
<br/>to which of these addresses are you referring when you indicate that "one is no longer any good?"  
<br/>Jon Lellelid  
Sno-Isle Libraries

answered 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_ce0991b4cf0643fa9b97bb20cb2e0237.jpg"/> support1@iii.com

- <a id="acceptAnswer_40"></a>[Accept as answer](#)

<a id="commentAns_40"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Our patrons are not receiving text messages, as we are being advised that some carriers are no longer converting Polaris messages into "text" format. Has this been an issue for any other library recently?](https://iii.rightanswers.com/portal/controller/view/discussion/1139?)
- [Deleting Mobile Carriers](https://iii.rightanswers.com/portal/controller/view/discussion/631?)
- [I have more and more patrons that are using contract cell phone services, but they want to get text messages from us. I have not found a way to update the cell phone provider list. Does anyone know how to do this?](https://iii.rightanswers.com/portal/controller/view/discussion/261?)
- [T-Mobile patrons not receiving text notifications](https://iii.rightanswers.com/portal/controller/view/discussion/982?)
- [Has anyone run into patrons with Project Fi for phone service?](https://iii.rightanswers.com/portal/controller/view/discussion/138?)

### Related Solutions

- [Setting up phone notification with additional TXT messages](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930542970896)
- [How do I enter government subsidized phone numbers?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930413229938)
- [Long Distance Phone Numbers and Text Messages](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930379236869)
- [How should phone numbers be formatted in patron registration?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930411345829)
- [Remove held-till date on text notices for one branch's holds](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930458066960)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)