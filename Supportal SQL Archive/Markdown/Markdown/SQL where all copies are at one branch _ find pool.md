SQL where all copies are at one branch / find pooled titles - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL where all copies are at one branch / find pooled titles

0

72 views

I'm trying to get a list where copies of a book are at one branch but not at any other branch.

So if the system ordered six copies of a title in a given collection, and they somehow all ended up at branch 5, with no copies anywhere else, I'd like a list of those titles.

my attempt (below) is not working

select cir.AssociatedBibRecordID from Polaris.CircItemRecords cir with (nolock) inner join Polaris.ItemRecordDetails ird with (nolock) on (cir.ItemRecordID = ird.ItemRecordID) where cir.assignedbranchid in (5) and cir.AssignedBranchID not in (1,2,3,4,6,7,8,9,10,11,12,13,14,15,16) and cir.AssignedCollectionID in (42,73,124,112,113,122,123,125,129,150)

Thanks!

asked 1 year ago  by <img width="18" height="18" src="../../_resources/90f7d649bfee44ca96416f52865b64e8.jpg"/>  crosenthal@pwcgov.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_750"></a>[](#)

0

<a id="downVoteAns_750"></a>[](#)

Answer Accepted

This may work for you. It joins to the ItemRecords view twice. The first time looks for the desired branch and the second, left join looks for any items that aren't at that branch. The last line excludes any bibs that have items at branch 5 and at least one other location. You can add additional selection criteria, such as the RecordStatusID (Final, Deleted, etc.) or the ItemStatusID (In, Out, Lost, Withdrawn, etc.) but it may be best to add them to the join criteria, rather than after the WHERE.

SELECT DISTINCT
br.bibliographicRecordID
FROM
Polaris.Polaris.BibliographicRecords br WITH (NOLOCK)
INNER JOIN Polaris.Polaris.ItemRecords ir (NOLOCK)
ON (br.BibliographicRecordID = ir.AssociatedBibRecordID AND ir.AssignedBranchID = 5)
LEFT JOIN Polaris.Polaris.ItemRecords ir2 (NOLOCK)
ON (br.BibliographicRecordID = ir2.AssociatedBibRecordID AND ir2.AssignedBranchID <> 5)
WHERE ir.AssignedCollectionID in (42,73,124,112,113,122,123,125,129,150)
AND ir2.ItemRecordID IS NULL

Hope this helps,

JT

answered 1 year ago  by <img width="18" height="18" src="../../_resources/90f7d649bfee44ca96416f52865b64e8.jpg"/>  jwhitfield@aclib.us

Works great! As always, you are the best JT!

— crosenthal@pwcgov.org 1 year ago

<a id="commentAns_750"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL to Find all bibs with same title.](https://iii.rightanswers.com/portal/controller/view/discussion/675?)
- [Checkouts by Title in a collection at a branch by year](https://iii.rightanswers.com/portal/controller/view/discussion/932?)
- [SQL for Item records from PO/FY/Branch](https://iii.rightanswers.com/portal/controller/view/discussion/326?)
- [SQL for bibs with holds and no owned items by branches](https://iii.rightanswers.com/portal/controller/view/discussion/821?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)

### Related Solutions

- [A New Branch](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161228114330859)
- [CopyOfflineFiles Job is not updating the offline files on workstations](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930838055588)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Updating the Copy Offline Files Job after an upgrade](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930749142343)
- [SysHoldRequest table missing Title information](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930733866097)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)