[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# SQL for LDR encoding levels

0

58 views

We have a number of old bibliographic records with a lower case 'o' for the encoding level. Is there a way to find these? Is the LDR considered a tag?

I would like to find these and change to a valid encoding level.

Thank you,

Loretta Staal  
Harford County Public Library

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_bc2e11d8f15a40faa5e95abdda9c4f79.jpg"/> staal@hcplonline.org

[ldr](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=470#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_1192"></a>[](#)

0

<a id="downVoteAns_1192"></a>[](#)

Answer Accepted

Hi Loretta,

We use this SQL to find acquisitions records. Replacing the 5 with your code of choice should do the trick:

SELECT BibliographicRecordID AS recordid FROM BibliographicRecords (nolock)  
WHERE MARCBibEncodingLevel = '5'

&nbsp;

Warmly,

Mariko Kershaw

Washington County Cooperative Library Services

answered 2 years ago by ![marikok@wccls.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_marikok@wccls.org20240119162535.jpg) marikok@wccls.org

<a id="commentAns_1192"></a>[Add a Comment](#)

<a id="upVoteAns_1193"></a>[](#)

0

<a id="downVoteAns_1193"></a>[](#)

Hi Mariko,

&nbsp;

Works perfect. Thank you so much!

&nbsp;

Loretta  
Harford County Public Library

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_bc2e11d8f15a40faa5e95abdda9c4f79.jpg"/> staal@hcplonline.org

- <a id="acceptAnswer_1193"></a>[Accept as answer](#)

<a id="commentAns_1193"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Bib record modifiers](https://iii.rightanswers.com/portal/controller/view/discussion/1380?)
- [SQL for Bibs lacking local call #](https://iii.rightanswers.com/portal/controller/view/discussion/605?)
- [SQL for items withdrawn by a particular staff member](https://iii.rightanswers.com/portal/controller/view/discussion/809?)
- [Finding new and replaced bibs in Polaris between certain dates](https://iii.rightanswers.com/portal/controller/view/discussion/871?)
- [SQL for items with information in "Name of piece"](https://iii.rightanswers.com/portal/controller/view/discussion/1345?)

### Related Solutions

- [007 LDR Cataloging Question](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930329896653)
- [How to import deleted records / bulk delete with an import](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200501085203736)
- [Is it possible to add options to the LDR, 007, or 008 fields of a bibliographic record?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930679897930)
- [TOMS have changed after upgrading](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170313101817515)
- [Where can I find the order of precedence for bibliographic Types of Materials (TOMs)?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930865918926)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)