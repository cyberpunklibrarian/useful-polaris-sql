Is there any way to write a SQL script to find bib records that do not have a 040 field ending in $dUtOrBLW? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Is there any way to write a SQL script to find bib records that do not have a 040 field ending in $dUtOrBLW?

0

63 views

Hello.

Is there any way to write a SQL script to find bib records that do **not** have "UtOrBLW" entered in the **last** $d in the 040 field?

We will be upgrading to 6.3 this month from 6.0, and I am running into problems with the changes that now preserve the original bib control number and original creation date for bibliographic records when overlaid. The problem is that the "Overlay/import" date will not be updated for manual overlays, such as those done with new bib records brought in via our Z39.50 connection to OCLC. Up until now, when gathering new OCLC records for things like sending to Backstage for processing, I was able to rely on the creation date + the presence of "(OCoLC)" at the beginning of an 035 field.

With the creation date now staying as the date the first bib was originally added, and the "Overlay/import" date only updating for imports made through the Import Manager (which is a very small portion of the overlaying work we do), I am trying to figure out new ways to locate new OCLC overlays that have **not** yet been sent to Backstage ...

We were kindly given the work-around of saving the new OCLC record brought in via Z39.50 and then saving it again and at that point replacing the existing bib (which will treat it as a new bib with a new creator and new creation date), and I am thankful for that option. But I am trying to find a way to work within the parameters of the new funtionality that retains the original bib number ...

In any case, than you in advance, for any SQL suggestions you can offer. :-)

--

Alison Hoffman

Monarch Library System

asked 2 years ago  by <img width="18" height="18" src="../../_resources/76f688bf29b14d03814c6e24e5d7810f.jpg"/>  ahoffman@monarchlibraries.org

[sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 2 years ago

<a id="upVoteAns_644"></a>[](#)

0

<a id="downVoteAns_644"></a>[](#)

Answer Accepted

Hi!

This is the one we use! I definitely borrowed someone's query and modified it for this purpose so it may not be the cleanest. But it works!

SELECT DISTINCT BR.BibliographicRecordID
FROM polaris.Bibliographicrecords BR (nolock)
JOIN polaris.BibrecordSets BRS (NOLOCK)
ON BRS.BibliographicRecordID = BR.BibliographicrecordID
JOIN polaris.BibliographicTags BT (NOLOCK)
ON BT.BibliographicrecordID = BR.BibliographicrecordID
JOIN polaris.BibliographicSubfields BS (NOLOCK)
ON BS.BibliographictagID = BT.BibliographicTagID
WHERE br.RecordStatusID = 1

AND BR.BibliographicRecordID NOT IN (
SELECT DISTINCT(BR.BibliographicRecordID) AS recordID
FROM polaris.Bibliographicrecords BR
JOIN polaris.BibrecordSets BRS (NOLOCK)
ON BRS.BibliographicRecordID = BR.BibliographicrecordID
JOIN polaris.BibliographicTags BT (NOLOCK)
ON BT.BibliographicrecordID = BR.BibliographicrecordID
JOIN polaris.BibliographicSubfields BS (NOLOCK)
ON BS.BibliographictagID = BT.BibliographicTagID
WHERE TagNumber =040
AND BS.Subfield LIKE 'd' AND Data LIKE 'UtOrBLW')

answered 2 years ago  by <img width="18" height="18" src="../../_resources/76f688bf29b14d03814c6e24e5d7810f.jpg"/>  abrookins@cvlga.org

<a id="commentAns_644"></a>[Add a Comment](#)

<a id="upVoteAns_666"></a>[](#)

0

<a id="downVoteAns_666"></a>[](#)

Sure thing - though the changes I made may be specific to our setup. :-)

We need to track a bit by date added, and we only send in OCLC records (not brief vendor records or baby bib records awaiting fuller cataloging), so I added in a creation date parameter and an OCLC record limit.

I have attached it as a .txt document since I apparently cannot paste in a script that includes a percent sign. %-) :-)

answered 2 years ago  by <img width="18" height="18" src="../../_resources/76f688bf29b14d03814c6e24e5d7810f.jpg"/>  ahoffman@monarchlibraries.org

- <a id="acceptAnswer_666"></a>[Accept as answer](#)

- [OCLC records not yet sent to Backstage.txt](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/540?fileName=540-666_OCLC+records+not+yet+sent+to+Backstage.txt "OCLC records not yet sent to Backstage.txt")

<a id="commentAns_666"></a>[Add a Comment](#)

<a id="upVoteAns_646"></a>[](#)

0

<a id="downVoteAns_646"></a>[](#)

Oh I think this is what I needed - I was able to edit it to add a few additional qualifiers, and it seems to be working as needed. Thank you!

Alison :-)

answered 2 years ago  by <img width="18" height="18" src="../../_resources/76f688bf29b14d03814c6e24e5d7810f.jpg"/>  ahoffman@monarchlibraries.org

- <a id="acceptAnswer_646"></a>[Accept as answer](#)

Hi Alison, 

Would you be willing to share the SQL script you wrote as well? We also just upgraded to 6.3 (in addition to adding a library to our consortium!) and I am going to have to send a batch of records to Backstage in the next week or so. Thanks so much for the help!

— esloan@cityofboise.org 2 years ago

<a id="commentAns_646"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [Is there a way via SQL to include the contents of 508 and 511 MARC fields in addition to genre subject headings?](https://iii.rightanswers.com/portal/controller/view/discussion/907?)
- [SQL top bib checkouts between 2 dates](https://iii.rightanswers.com/portal/controller/view/discussion/650?)
- [Does anyone have a sql script for a monthly in-house check-in count?](https://iii.rightanswers.com/portal/controller/view/discussion/615?)

### Related Solutions

- [Bib Call Number is not automatically copying to Item Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930769128110)
- [How can I locate bibs that were generated during a Terminated import without using SQL Server Management Studio?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930570115337)
- [Vital start and stop scripts](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=151113130835651)
- [How do I schedule the Year End Circ Count Rollover job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930582726474)
- [Adding a new UDF to patron registration](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930740605846)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)