[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Welcome to the Polaris Forum!

0

13 views

Originally posted by david.pisciarino@iii.com on Wednesday, January 4th 15:03:38 EST 2017  
<br/>Product forums are available to our customers to provide a platform for knowledge sharing and open discussion about products, features, and workflows. These forums are not monitored by Innovative staff. Please see a copy of our legal disclaimer [here.](https://iii.rightanswers.com/portal/controller/view/discussion/../../../app/portlets/results/viewsolution.jsp?solutionid=151029112319039)

asked 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_2235463817b044e7b9c4760c0a331ca9.jpg"/> support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 7 years ago

<a id="upVoteAns_54"></a>[](#)

0

<a id="downVoteAns_54"></a>[](#)

RE: Old Content?

Response by ggosselin@richlandlibrary.com on January 30th, 2017 at 9:18 am

The posts from the old forums couldn't be migrated, but the posts that had SQL queries in them were exported into an Excel spreadsheet which was posted in the reporting forum in this new Supportal.

answered 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_2235463817b044e7b9c4760c0a331ca9.jpg"/> support1@iii.com

- <a id="acceptAnswer_54"></a>[Accept as answer](#)

<a id="commentAns_54"></a>[Add a Comment](#)

<a id="upVoteAns_53"></a>[](#)

0

<a id="downVoteAns_53"></a>[](#)

Old Content?

Response by rdye@krl.org on January 26th, 2017 at 7:07 pm

Hi, I do like the new "Supportal", but I'm wondering if the content from the old forums will be migrated over? There was a lot of valuable information there that I used to refer to quite a bit.  
\- Robin

answered 7 years ago by <img width="18" height="18" src="../_resources/default-avatar_2235463817b044e7b9c4760c0a331ca9.jpg"/> support1@iii.com

- <a id="acceptAnswer_53"></a>[Accept as answer](#)

<a id="commentAns_53"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [ILL in Polaris](https://iii.rightanswers.com/portal/controller/view/discussion/64?)
- [RFID Integration with Polaris](https://iii.rightanswers.com/portal/controller/view/discussion/66?)
- [MessageBee and Hosted Polaris](https://iii.rightanswers.com/portal/controller/view/discussion/1116?)
- [Polaris ILL, INN-Reach and Illiad](https://iii.rightanswers.com/portal/controller/view/discussion/691?)
- [General Staff guides for Polaris 6.3](https://iii.rightanswers.com/portal/controller/view/discussion/609?)

### Related Solutions

- [Deleting item records with fines](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930356108467)
- [Polaris not recognizing ILL item barcode](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930615916421)
- [How does Polaris determine requests to be duplicates?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930617910193)
- [Patron Record Import TRN Format - Polaris 6.5](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200513113659590)
- [Credit card vendor options](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180612111730348)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)