Duplicate ISBN in a MARC - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Duplicate ISBN in a MARC

0

59 views

We get MARC records coming in that have the 020 - ISBN duplicated many times in the same MARC.

Has anyone encountered this? Do you know what causes it and how to stop it? Seems like it's coming from the vendor this way but I can't pinpoint which one and if there is a way to avoid it happening.

Some of them have the same ISBN or two ISBNs displaying five or more times. It's pretty annoying and time consuming to have to individually edit each record after an import. And since we're short staffed we really don't have the time to do this. Am I missing something obvious?

Thanks.

asked 1 year ago  by <img width="18" height="18" src="../../_resources/e4e32a28c2de4ab488e2c103e08347b8.jpg"/>  csmiley@leegov.com

[isbn](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=383#t=commResults) [marc 020](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=384#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_970"></a>[](#)

0

<a id="downVoteAns_970"></a>[](#)

Yes, I've seen it (and hunted it down, and terminated it with extreme prejudice, at least in "my" collections.  Some of the Captain Underpants books had the same ISBN listed in the PAC literally 15 times).

It has something to do with EDI when you order adds.  Beyond that, I couldn't say.

If you're looking to get a scope of the problem, you can use this with the "count only" option.  You can change the *HAVING count(ISBNDisplayData) > 1* line below to other numbers, to get e.g. books with the same ISBN more than 3 times.

SELECT DISTINCT a.BibliographicRecordID
FROM (
SELECT BibliographicRecordID
FROM BibliographicISBNIndex with (NOLOCK)
GROUP BY CASE WHEN CHARINDEX(' ',SUBSTRING(ISBNDisplayData,0, 200)) > 0
THEN SUBSTRING(ISBNDisplayData, 0, CHARINDEX(' ',SUBSTRING(ISBNDisplayData,0, 200)))
ELSE ISBNDisplayData
END, BibliographicRecordID
HAVING count(ISBNDisplayData) > 1
) a
JOIN BibliographicRecords br with (NOLOCK)
ON br.BibliographicRecordID = a.BibliographicRecordID
WHERE DisplayInPAC = 1

 \[edit: "same bib" -> "same ISBN"\]

answered 1 year ago  by <img width="18" height="18" src="../../_resources/e4e32a28c2de4ab488e2c103e08347b8.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_970"></a>[Accept as answer](#)

Thanks for the info. My results on that search are scary big. I narrowed to 3 or more and that was better but still...

Can I ask - what do you mean you terminated it? You mean you stopped it from happening? I was suspecting the EDI orders but don't know why or what to do. I'll check with my site manager though.

Thanks!

— csmiley@leegov.com 1 year ago

Ah, nothing so fancy.  I just tracked down the ones in kids' or teens' books and deleted the duplicated ISBNs.  It was a slow project, something I worked on for months when I had a spare moment.

— jjack@aclib.us 1 year ago

Sounds like a good Work From Home project for staff!![smile](../../_resources/2ac2d44c12d04713bb7b62d107edde9d.gif)

— csmiley@leegov.com 1 year ago

<a id="commentAns_970"></a>[Add a Comment](#)

<a id="upVoteAns_971"></a>[](#)

0

<a id="downVoteAns_971"></a>[](#)

Do you import records into the system for acquisitions? We've gotten many duplicate ISBNs added to records due to the imcoming acquisitions brief record adding its ISBN to the existing bib record. There is a property to change in Polaris to fix this (I don't recall where that is--yiou can ask Polaris perhaps). The disadvantage of turning off that off is that you will need to be more aware of checking the alerts on the PO for problems with multiple ISBNS on a bib record. 

answered 1 year ago  by <img width="18" height="18" src="../../_resources/e4e32a28c2de4ab488e2c103e08347b8.jpg"/>  sgrant@somd.lib.md.us

- <a id="acceptAnswer_971"></a>[Accept as answer](#)

We do use EDI importing in acquisitions and I wondered if there was an issue there. I'll ask our site manager about the fix.

Thanks for the information.

— csmiley@leegov.com 1 year ago

If your import profile for adding 970 fields for additional copies/items is set to insert the 020 from the incoming record, you may end up with duplcates. Theoretically, when a purchase order is created from 970 fields, Polaris uses the most recently added 020 field as the desired ISBN. We have had some issues in the past with ordering the incorrect binding because the most recent ISBN was the wrong one (we may not have been inserting a new 020). I'm not sure if there is an easy way to get around this without manual cleanup since bulk changes cannot be made (last I checked) on matching fields (010, 020, 024, 035, etc.).

Now that Polaris is replacing data rather than deleting a bib record and creating a new on on import, it MAY be worth considering locating records with duplicated ISBNS, exporting them, using a utility like MARC Edit to strip out the duplicate fields and then re-importing them. (You can match the existing 001 with the incoming 001.)

— jwhitfield@aclib.us 1 year ago

<a id="commentAns_971"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Finding duplicate 035s, 050 etc.](https://iii.rightanswers.com/portal/controller/view/discussion/500?)
- [TOMs and MARC coding](https://iii.rightanswers.com/portal/controller/view/discussion/31?)
- [Accession Numbers](https://iii.rightanswers.com/portal/controller/view/discussion/1029?)
- [Use Item Record Call Numbers to Bulk Update Bib MARC Tag](https://iii.rightanswers.com/portal/controller/view/discussion/235?)
- [Does anyone work with Baker & Taylor as a vendor, and now receive FTP files for MARC records?](https://iii.rightanswers.com/portal/controller/view/discussion/999?)

### Related Solutions

- [Does the de-duplication process de-dupe on all values in repeatable tags (example - ISBN), or just the first tag?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930402321885)
- [ISBN Normalization - What is it and how is it done?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930433096327)
- [Catalog Extract Error Message](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930674953830)
- [How can Authority Control records be merged?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930673534728)
- [Searching for Accelerated Reader Tag (526) Data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930649873277)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)