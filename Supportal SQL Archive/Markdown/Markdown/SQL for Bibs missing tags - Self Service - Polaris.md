SQL for Bibs missing tags - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for Bibs missing tags

0

64 views

Hi,

As part of a catalog clean up project I would like to find all of our Bib records that are still in AACR2. Does anyone have a SQL search that would help with this? 

Thanks!

asked 1 year ago  by <img width="18" height="18" src="../../_resources/0763d6c7174c47da95d668979447f71b.jpg"/>  hkaufman@piercecountylibrary.org

[sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_907"></a>[](#)

1

<a id="downVoteAns_907"></a>[](#)

Answer Accepted

Hi.

There is a field in the BibliographicRecords table that should give you the information you want. I don't have access to my library's database at the moment, but this should work. It is set to work in the Find Tool, but you can add fields to the select statement as needed if you want a list report. The last line will limit to final records only.

Here's a query that \_should\_ work.

SELECT BibliographicRecordID

FROM Polaris.Polaris.BibliographicRecords WITH (NOLOCK)

WHERE MARCDescCatalogingForm like 'a'

AND RecordStatusID = 1

Hope this helps.

JT

answered 1 year ago  by <img width="18" height="18" src="../../_resources/0763d6c7174c47da95d668979447f71b.jpg"/>  jwhitfield@aclib.us

<a id="commentAns_907"></a>[Add a Comment](#)

<a id="upVoteAns_911"></a>[](#)

0

<a id="downVoteAns_911"></a>[](#)

Thanks!  I was able to get this to work for me.

Heather

answered 1 year ago  by <img width="18" height="18" src="../../_resources/0763d6c7174c47da95d668979447f71b.jpg"/>  hkaufman@piercecountylibrary.org

- <a id="acceptAnswer_911"></a>[Accept as answer](#)

<a id="commentAns_911"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [SQL for MARC tag search](https://iii.rightanswers.com/portal/controller/view/discussion/194?)
- [SQL top bib checkouts between 2 dates](https://iii.rightanswers.com/portal/controller/view/discussion/650?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [Does anyone have an SQL report for a specific tag in Authority records?](https://iii.rightanswers.com/portal/controller/view/discussion/625?)

### Related Solutions

- [Bibliographic and Authority Record tag order](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930557664312)
- [What tag does Syndetics use to retrieve book cover images?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930693836586)
- [SysHoldRequest table missing Title information](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930733866097)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Overdrive integration creating non-integrated items and no resource entity](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170503103227830)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)