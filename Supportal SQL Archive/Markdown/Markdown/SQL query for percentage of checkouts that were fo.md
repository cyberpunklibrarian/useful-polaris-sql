[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL query for percentage of checkouts that were for held items

0

107 views

Does anyone have a SQL query to get circulation data that would show me what percentage of checkouts (no renewals) were for held items during a given time period by branch? 

I have done some queries using TransactionTypeID 6001 with TransactionSubType 233 and a numValue of NULL vs. TransactionTypeID 6039 but those two totaled together are not matching up to my typical base queries for checkouts and I am not sure why.

Any sample queries from the community would be much appreciated :)

asked 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_b232a226c34f4cf086d09db074d01ae0.jpg"/> averba@sno-isle.org

[checkouts](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=313#t=commResults) [hold](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=70#t=commResults) [query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=90#t=commResults) [report](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=91#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 3 years ago

<a id="upVoteAns_1071"></a>[](#)

0

<a id="downVoteAns_1071"></a>[](#)

This number doesn't exactly match my count of 6039 transactions but is within ~2.5%:

&nbsp;

`select o.Name [Branch]`  
`,count(case when td_shr.numValue is null then 1 end) [NonHoldCkos]`  
`,count(case when td_shr.numValue is not null then 1 end) [HoldCkos]`  
`,count(*) [TotalCkos]`  
`from PolarisTransactions.Polaris.TransactionHeaders th`  
`join Polaris.Polaris.Organizations o`  
`on o.OrganizationID = th.OrganizationID`  
`left join PolarisTransactions.Polaris.TransactionDetails td_shr`  
`on td_shr.TransactionID = th.TransactionID and td_shr.TransactionSubTypeID = 233`  
`where th.TransactionTypeID = 6001`  
`and th.TranClientDate between '1/1/2021' and '2/1/2021'`  
`group by o.Name`  
`order by o.Name`

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_b232a226c34f4cf086d09db074d01ae0.jpg"/> mfields@clcohio.org

- <a id="acceptAnswer_1071"></a>[Accept as answer](#)

<a id="commentAns_1071"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Checkout percentage from self checkouts per location](https://iii.rightanswers.com/portal/controller/view/discussion/1260?)
- [Need help with query to find checked out titles with holds](https://iii.rightanswers.com/portal/controller/view/discussion/1063?)
- [SQL for bibs with holds and no owned items by branches](https://iii.rightanswers.com/portal/controller/view/discussion/821?)
- [SQL Query to find items that we own in eBook format but not in physical book format.](https://iii.rightanswers.com/portal/controller/view/discussion/1330?)
- [I'm looking for a SQL report that will tell how long items are in held status.](https://iii.rightanswers.com/portal/controller/view/discussion/495?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)
- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)
- [Query to find unfillable holds due to volume data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930242167096)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)