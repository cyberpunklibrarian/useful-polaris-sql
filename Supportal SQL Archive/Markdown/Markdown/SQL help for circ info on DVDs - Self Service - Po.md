SQL help for circ info on DVDs - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL help for circ info on DVDs

0

99 views

I've got a librarian who wants a list of all the juvenile DVDs that have checked out seven times or fewer in the last year (outside of the YTD circ info). I'm blanking on how to do that in Simply Reports and I'm not SQL savvy enough to compose one myself (other than making tweaks to existing SQL queries). Thanks!

asked 2 years ago  by <img width="18" height="18" src="../../_resources/8076928147a8439d98b40dd57b993619.jpg"/>  adornink@amespl.org

[sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_699"></a>[](#)

0

<a id="downVoteAns_699"></a>[](#)

Answer Accepted

Here is a query that I used for another purpose with a few tweaks:

There are criteria for AssignedBranch or the transacting branch (the line with the hyphens). You can choose either option, or delete both lines as needed. This query does not include renewals. If you want renewals as well as checkouts, add 28,73,74,76,80,93 to the ActionTakenID (in both locations).

SELECT it.ItemRecordID, it.CallNumber, br.BrowseTitle, it.Barcode, count(irh.ItemRecordHistoryID) as CkOutCount
FROM Polaris.Polaris.Itemrecords it with (nolock)
JOIN Polaris.Polaris.ItemRecordHistory irh (nolock)
ON (it.ItemRecordID = irh.ItemRecordID)
JOIN Polaris.Polaris.BibliographicRecords br (NOLOCK)
ON (it.AssociatedBibRecordID = br.BibliographicRecordID)
WHERE
it.RecordStatusID = 1
AND it.AssignedBranchID = 7
\-\- AND irh.OrganizationID = 7
AND it.AssignedCollectionID in (20,38)
AND it.MaterialTypeID in (4)
AND irh.ActionTakenID in  (12,13,75,77,78,79,81,89,90,91)
AND irh.TransactionDate between '01/01/2017' and '12/31/2017 23:59:59.999'
GROUP BY it.ItemRecordID, it.CallNumber, br.BrowseTitle, it.Barcode
HAVING COUNT(CASE WHEN irh.ActionTakenID in  (12,13,75,77,78,79,81,89,90,91) and irh.TransactionDate between '01/01/2017' and '12/31/2017 23:59:59.999'
 THEN irh.ItemRecordHistoryID END) <= 7

Hope this helps,

JT

answered 2 years ago  by <img width="18" height="18" src="../../_resources/8076928147a8439d98b40dd57b993619.jpg"/>  jwhitfield@aclib.us

Also, don't forget to modify dates, CollectionID(s) and MaterialTypeID(s) as needed. JT

— jwhitfield@aclib.us 2 years ago

<a id="commentAns_699"></a>[Add a Comment](#)

<a id="upVoteAns_700"></a>[](#)

0

<a id="downVoteAns_700"></a>[](#)

Awesome- thanks, that does it!

answered 2 years ago  by <img width="18" height="18" src="../../_resources/8076928147a8439d98b40dd57b993619.jpg"/>  adornink@amespl.org

- <a id="acceptAnswer_700"></a>[Accept as answer](#)

<a id="commentAns_700"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL help for for circ info on 'new' stuff](https://iii.rightanswers.com/portal/controller/view/discussion/698?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)
- [SQL for Bibs w/no good items but have holds - including item and patron info](https://iii.rightanswers.com/portal/controller/view/discussion/594?)
- [Need help with query to find checked out titles with holds](https://iii.rightanswers.com/portal/controller/view/discussion/1063?)
- [SimplyReports or SQL to get circ count by a UDF?](https://iii.rightanswers.com/portal/controller/view/discussion/88?)

### Related Solutions

- [How do I schedule the Year End Circ Count Rollover job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930582726474)
- [Polaris Help Documentation is now on the Documentation Portal](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210909094727613)
- [Polaris Database Repository (Version 5.5)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=171013130008229)
- [Polaris Database Repository (Version 6.0)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180503133747174)
- [Polaris Database Repository (Version 5.6)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180313130034560)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)