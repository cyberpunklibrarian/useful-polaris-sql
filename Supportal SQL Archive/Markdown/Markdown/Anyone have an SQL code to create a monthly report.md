[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Anyone have an SQL code to create a monthly report of all waived transactions by all staff members? Or know how to generate it in Simply Reports

0

77 views

Need a monthly report of all waived items by all staff members.  Either SQL or in Simply Reports.

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_5013ed1ad6604ba5a269368764fa43eb.jpg"/> pamela.johnson@brentwoodtn.gov

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_1253"></a>[](#)

0

<a id="downVoteAns_1253"></a>[](#)

Attached is a version of the script I shared previously that can be run directly as a query (the previous had Stored Prcoedure formatting) - just update the variables at the top.

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_5013ed1ad6604ba5a269368764fa43eb.jpg"/> jtenter@sasklibraries.ca

- <a id="acceptAnswer_1253"></a>[Accept as answer](#)

- [Rpt_FinesPaidWaived v2.txt](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/1193?fileName=1193-1253_Rpt_FinesPaidWaived+v2.txt "Rpt_FinesPaidWaived v2.txt")

<a id="commentAns_1253"></a>[Add a Comment](#)

<a id="upVoteAns_1251"></a>[](#)

0

<a id="downVoteAns_1251"></a>[](#)

Here's the query we use for a report of paid and waived transaction lines that you can filter by date and branch.  It doesn't look like anything in it is specific to our system, although I'll mention that we don't include the patron name in our report, and will be removing the barcodes (due to sensitivity of patron data; we'll use PatronID instead).

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_5013ed1ad6604ba5a269368764fa43eb.jpg"/> jtenter@sasklibraries.ca

- <a id="acceptAnswer_1251"></a>[Accept as answer](#)

- [Rpt_FinesPaidWaived_Trans.txt](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/1193?fileName=1193-1251_Rpt_FinesPaidWaived_Trans.txt "Rpt_FinesPaidWaived_Trans.txt")

Thank you!  I'll give it a whirl!

— pamela.johnson@brentwoodtn.gov 2 years ago

I noticed this SQL points to a stored procedure that I dont seem to see in our system.

— brandon.williams@mesaaz.gov 2 years ago

Are you referring to the "ALTER PROCEDURE..." section?  That script is a stored procedure, and that's the line that allows you to alter it in the DB.  Someone creating their own version of a report SProc would want to rename it, and make it "CREATE PROCEDURE..." to save it.

In a separate comment I've attached a version that can be run directly as a query, just update the variables at the top.

— jtenter@sasklibraries.ca 2 years ago

<a id="commentAns_1251"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [SQL or Simply Report Active Patrons](https://iii.rightanswers.com/portal/controller/view/discussion/1164?)
- [Simply Reports Discrepancies](https://iii.rightanswers.com/portal/controller/view/discussion/960?)
- [Simply reports patron services](https://iii.rightanswers.com/portal/controller/view/discussion/355?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)

### Related Solutions

- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [Can a report created in SimplyReports be made available to staff without access to SimplyReports?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930241316369)
- [Instructions for creating Edelweiss reports in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104858811)
- [Run SimplyReports Scheduled Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930277327637)
- [Can I create a report in SimplyReports that can be run with a specific date range?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930371790305)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)