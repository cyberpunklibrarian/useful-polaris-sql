[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Acquisitions](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=10)

# Can I invoice a POLI with no linked final bib record?

0

83 views

Instead of overwriting an on-order bib, the items were created on an existing bib and the on-order bib was deleted. ![undecided](../_resources/smiley-undecided_064edbe3c467419ab1a2622823016ebc.gif)  Now, as we try to clean up our funds, we've come across an item that cannot be invoiced because "Line item 8 must have a linked bibliographic record with a status of Final." Is there something I can do so that we can get this item invoiced and paid? TIA!

&nbsp;

Edited to add: Some of these are from 2012 with no linked bibs or items.. would it hurt to just un-receive and "cancel" them so that the funds will be released?

asked 5 years ago by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg) sbills@lpld.lib.in.us

[acquisitions](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=53#t=commResults) [invoice](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=196#t=commResults) [poli](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=197#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 5 years ago

<a id="upVoteAns_472"></a>[](#)

0

<a id="downVoteAns_472"></a>[](#)

Your site manager may be able to re-associate the POLI to the bib so that you can invoice the line through SQL. At least that worked for us in the past!

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_94550aba89be428b8b40c6d918e7c46c.jpg"/> amye@wccls.org

- <a id="acceptAnswer_472"></a>[Accept as answer](#)

I've been in touch with her, but haven't heard back. Thanks!

— sbills@lpld.lib.in.us 5 years ago

<a id="commentAns_472"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Are there any unintended consquences for purging POs and invoices that are, say, 2 years or older?](https://iii.rightanswers.com/portal/controller/view/discussion/472?)
- [Barcode Missing in Item Record](https://iii.rightanswers.com/portal/controller/view/discussion/216?)
- [Overdrive MARC record with no audience code or all coded adult in 006 and 008 of Marc record.](https://iii.rightanswers.com/portal/controller/view/discussion/176?)
- [Is there a way in the Acquistions module to receive and barcode multipart items or combo packages that will need to be split and attached to two sepaerate bibliographic records but paid and received as one item?](https://iii.rightanswers.com/portal/controller/view/discussion/175?)
- [Fiscal year rollover by replication](https://iii.rightanswers.com/portal/controller/view/discussion/324?)

### Related Solutions

- [How do I apply a credit memo to an invoice?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930321951911)
- [Invoice Lines view is blank](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=220923110002050)
- [Why is the EDI Agent job continuously running?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930649540631)
- [How do I correct EDI invoice lines that do not link to the PO line?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930240796789)
- [Credit an individual Invoice Line Item that has been paid for](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930299529481)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)