[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [General Announcements](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=55)

# Idea Lab: Expiring Ideas

0

17 views

Hi Polaris users,  
<br/>Did you know that there are now 9 ideas from the Always Open Space that are being developed for the next Polaris release? You can see the full list on [the IUG website](https://www.innovativeusers.org/enhancement/selected-ideas) or within the Idea Lab by filtering ideas to "selected".  
<br/>As always you have all been hard at work submitting new ideas to the site, and a few of those ideas need just a bit more support to graduate:  
<br/>[](http://ability%20to%20add%20a%20new%20bib%20record%20in%20leap/)[](https://idealab.iii.com/alwaysopen-polaris/Page/ViewIdea?ideaid=8452)[](https://idealab.iii.com/alwaysopen-polaris/Page/ViewIdea?ideaid=8452)[](https://idealab.iii.com/alwaysopen-polaris/Page/ViewIdea?ideaid=8452)[](https://idealab.iii.com/alwaysopen-polaris/Page/ViewIdea?ideaid=8452)[](https://idealab.iii.com/alwaysopen-polaris/Page/ViewIdea?ideaid=8452)[](https://idealab.iii.com/alwaysopen-polaris/Page/ViewIdea?ideaid=8452)[](https://idealab.iii.com/alwaysopen-polaris/Page/ViewIdea?ideaid=8452)https://idealab.iii.com/alwaysopen-polaris/Page/ViewIdea?ideaid=8452  
[Ability to change the size of the type on receipts](https://idealab.iii.com/alwaysopen-polaris/Page/ViewIdea?ideaid=8398)  
[Bibliographic Find Tool Search Points](https://idealab.iii.com/alwaysopen-polaris/Page/ViewIdea?ideaid=8446)  
[Find Tool SQL search criteria box](https://idealab.iii.com/alwaysopen-polaris/Page/ViewIdea?ideaid=8430)  
[Send notices using Alt Email Address even when the primary Email Address field is blank](https://idealab.iii.com/alwaysopen-polaris/Page/ViewIdea?ideaid=8442)  
[Would you like a receipt n/a Yes No](https://idealab.iii.com/alwaysopen-polaris/Page/ViewIdea?ideaid=8322)  
<br/>New to Idea Lab? [Idea Lab](https://idealab.iii.com/) is designed to allow you to submit, discuss, and vote on ideas to improve Sierra and Polaris. Anyone at any Innovative library may participate in Idea Lab. See in the information on how to create an Idea Lab account on our FAQ page: [](https://www.innovativeusers.org/enhancement/idea-lab-faq)https://www.innovativeusers.org/enhancement/idea-lab-faq  
<br/>Jeremy Goldstein and Maisam Nouh  
IUG Enhancements Co-Coordinators  
Gwyneth Jelinek, Polaris Team Lead  
enhancements@innovativeusers.org

asked 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_89fb2378fbdd4571af6420fba816f194.jpg"/> gjelinek@faylib.org

[enhancements](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=51#t=commResults) [idealab](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=52#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Idea Lab: Expiring Ideas](https://iii.rightanswers.com/portal/controller/view/discussion/1047?)
- [Idea Lab: Expiring Ideas](https://iii.rightanswers.com/portal/controller/view/discussion/1221?)
- [Idea Lab: Expiring Ideas](https://iii.rightanswers.com/portal/controller/view/discussion/1065?)
- [Idea Lab: Expiring Ideas](https://iii.rightanswers.com/portal/controller/view/discussion/1087?)
- [Idea Lab: Expiring Ideas](https://iii.rightanswers.com/portal/controller/view/discussion/1243?)

### Related Solutions

- [Using Idea Exchange to Submit an Enhancement](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=230403205834033)
- [IUG 2023: If I Ran the Zoo or: How an Idea Becomes a Product](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=230610201432497)
- [Set all current patron expiration dates to the future to remove the expired patron block](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930981683916)
- [What will happen to holds if I change the default Holds Expiration Date?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930309710990)
- [IUG 2015: The "Sierra Lab": What Innovative is developing now](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160418155717844)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)