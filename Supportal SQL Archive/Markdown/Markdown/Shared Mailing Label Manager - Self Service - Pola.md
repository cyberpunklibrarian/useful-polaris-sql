[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Shared Mailing Label Manager

0

41 views

Is it possible to have a shared mailing label list? My staff would like to be able to add patron addresses to a list that can then be printed by one person in its entirety each day. In the past, we had to write a script to do this, but I want to see if this is now a feature. I don't see anything about this in the Mialing Label Manager help files or the forum.

&nbsp;

Thanks for any suggestions you have,

&nbsp;

Taylor Buchheit  
Eugene Public Library

asked 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_211af5bbb58840a6b40096415718c929.jpg"/> tbuchheit@eugene-or.gov

[mailing label](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=178#t=commResults) [polaris 5.6](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=179#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 5 years ago

<a id="upVoteAns_450"></a>[](#)

0

<a id="downVoteAns_450"></a>[](#)

To clarify for anyone who is also interested in this, iii informed me that this is not currently a feature in Polaris.

&nbsp;

My alternative approach is to write a SQL report that will aggregate all addresses from patron registrations that day. My library also sends mail to patrons who update their addresses which means I need to include those in the mailing label report. Does anyone know if the database holds any kind of information about when an address specifically is changed in patron pegistration? When I look in the transaction database, I only see a TransactionType for "Patron registration modified" which seems to be a blanket transaction type for any change in patron registration. There is a TransactionSubType called "Primary Address", but it doesn't seem like our database is utilizing it or it rarely does.

&nbsp;

Thanks for any help you can provide.

&nbsp;

Taylor

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_211af5bbb58840a6b40096415718c929.jpg"/> tbuchheit@eugene-or.gov

- <a id="acceptAnswer_450"></a>[Accept as answer](#)

<a id="commentAns_450"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Borrow by Mail in LEAP](https://iii.rightanswers.com/portal/controller/view/discussion/1088?)
- [Does Borrow by Mail work in Leap?](https://iii.rightanswers.com/portal/controller/view/discussion/758?)
- [Inventory Manager permissions](https://iii.rightanswers.com/portal/controller/view/discussion/70?)
- [Is anyone that has multiple locations using Borrow by Mail?](https://iii.rightanswers.com/portal/controller/view/discussion/552?)
- [MessageBee notices](https://iii.rightanswers.com/portal/controller/view/discussion/1134?)

### Related Solutions

- [Label/Paper stock for BBM Mailer](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=171201090449943)
- [How does Borrow By Mail work?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170825100128299)
- [Ordering barcode labels, spine labels, spine label printers, barcode scanners, receipt printers, and receipt printer paper](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930942324958)
- [Library not receiving e-mails for online registrations](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930660833764)
- [Reminder notices option is e-mail. Do you want to continue? - What does this pop-up mean?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930539888351)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)