Report for all outstanding charges on patron accounts - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Report for all outstanding charges on patron accounts

0

66 views

My director is considering an amnesty event or going fine free. I ran a report a while back totalling our outstanding fines from just our youth. She wants more information that just a total number of patrons and total number of money owed. She doesn't know what answers she's looking for just yet![undecided](../../_resources/5d3ccb11b20242829ea38623e8497147.gif), she just wants all the info I can muster I suppose. I've been playing around in Simply Reports but can't seem to get everything I'd like to include. And then, when I think I do, a lot of the information is duplicated. It will show Patron JANE DOE total $400 5 or 6 times for each transaction on her account.I'm giving up right now...

So, does anyone know of a good report to run (in Utilites, Simply Reports, SQL?) that can give me the following information, or more!?

Total Balance Patron Account Charges (-- just once, not for each transaction!)
Fee Reason
Transaction Date
Patron last activity date?
(More patron info such as patron code, statistical class, lifetime circ count, postal code, full name, barcode)

I hope this all makes sense.. HELP!

asked 1 year ago  by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg)  sbills@lpld.lib.in.us

It looks like the reason you are seeing repeating lines in your output may be that you are requesting individual transaction information which requires a line for each.  You may need to split this into two reports, one as a Summary and the other as the supporting detail.  i have included a couple of SQL Queries that you can build off to hopefully satisfy what you need to extract.  You will need to join other tables to get PostalCode etc.

Summary Output:

SELECT P.PatronID, P.PatronCodeID, P.OrganizationID, P.Barcode, PR.PatronFullName, P.LastActivityDate, P.LifetimeCircCount, P.ChargesAmount, P.CreditsAmount,
SUM(P.ChargesAmount - P.CreditsAmount) AS "TotalOutstanding"
FROM Patrons P (NOLOCK)
JOIN PatronRegistration PR (NOLOCK)
ON PR.PatronID = P.PatronID
WHERE P.ChargesAmount > '0.00'
GROUP BY P.PatronID, P.PatronCodeID, P.OrganizationID, P.Barcode, PR.PatronFullName, P.LastActivityDate, P.LifetimeCircCount, P.ChargesAmount, P.CreditsAmount
ORDER BY P.PatronID

Detail Output:

SELECT PA.PatronID, PR.PatronFullName, PTC.\[Description\], PA.TxnDate, PA.TxnCodeID, FR.FeeDescription, PA.OutstandingAmount,
(P.ChargesAmount - P.CreditsAmount) AS "TotalOutstanding", P.LastActivityDate
FROM PatronAccount PA (NOLOCK)
JOIN PatronRegistration PR (NOLOCK)
ON PR.PatronID = PA.PatronID
JOIN Patrons P (NOLOCK)
ON P.PatronID = PR.PatronID
JOIN PatronAccTxnCodes PTC (NOLOCK)
ON PTC.TxnCodeID = PA.TxnCodeID
JOIN FeeReasonCodes FR (NOLOCK)
ON FR.FeeReasonCodeID = PA.FeeReasonCodeID
WHERE PA.OutstandingAmount > '0.00'
AND PA.TxnCodeID IN (1,4,8)
GROUP BY PA.PatronID, PR.PatronFullName, PTC.\[Description\], FR.FeeDescription, PA.TxnDate, PA.TxnCodeID, PA.TxnAmount, PA.OutstandingAmount,
P.ChargesAmount, P.CreditsAmount, P.LastActivityDate
ORDER by PA.PatronID

Rex Helwig
Finger Lakes Library System

— rhelwig@flls.org 1 year ago

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_723"></a>[](#)

0

<a id="downVoteAns_723"></a>[](#)

I used the ViewPatronRegistration view and then tapped into other tables for some data. In the 'Activity' field, we looked at last three years (365*3=1095 and "-" is in past). It is not perfect but it was easier...

Hope this helps :)

`SELECT PatronID
 ,[Branch] = BranchName
 ,[Age Group] = CASE WHEN Birthdate is null THEN 'Unknown' ELSE (CASE WHEN (DATEDIFF(hour,Birthdate,GETDATE())/8766+1) between 0 and 17 THEN 'Juvenile' ELSE 'Adult' END) END
 ,[Patron Code] =PCodeDescription
 ,[Activity] = CASE WHEN DATEDIFF(day,getdate(),[LastActivityDate])>=-1095 THEN 'Active' ELSE 'Inactive' END
 ,[Items Out] = (SELECT Count(icko.ItemRecordID)
      FROM Polaris.Polaris.ItemCheckouts icko WITH (NOLOCK)
      WHERE icko.PatronID = pr.PatronID)
 ,[Items Overdue] = (SELECT Count(icko.ItemRecordID)
      FROM Polaris.Polaris.ItemCheckouts icko WITH (NOLOCK)
      WHERE icko.PatronID = pr.PatronID
       AND icko.DueDate < getdate()-1)
 , [Outstanding Fines] = (SELECT SUM(OutstandingAmount)
        FROM Polaris.Polaris.PatronAccount AS PA WITH (NOLOCK)
        WHERE PA.TxnCodeID = 1   --1 Charge
         AND pa.FeeReasonCodeID = 0 --0 Overdue
         AND pr.PatronID = PA.PatronID)
 , [Outstanding Fees & NULL Reasons] = (SELECT SUM(OutstandingAmount)
        FROM Polaris.Polaris.PatronAccount AS PA WITH (NOLOCK)
        WHERE PA.TxnCodeID = 1   --1 Charge
         AND (pa.FeeReasonCodeID not in (0) --0 Overdue
          OR pa.FeeReasonCodeID is null)
         AND pr.PatronID = PA.PatronID)
 , [Outstanding Total] = (SELECT SUM(OutstandingAmount)
        FROM Polaris.Polaris.PatronAccount AS PA WITH (NOLOCK)
        WHERE PA.TxnCodeID = 1   --1 Charge
         AND pr.PatronID = PA.PatronID)
FROM Polaris.Polaris.ViewPatronRegistration pr (nolock)`

answered 1 year ago  by ![eric.young@phoenix.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_eric.young@phoenix.gov20191029113427.jpg)  eric.young@phoenix.gov

- <a id="acceptAnswer_723"></a>[Accept as answer](#)

<a id="commentAns_723"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [Simply reports patron services](https://iii.rightanswers.com/portal/controller/view/discussion/355?)
- [Circulation report by patrons age](https://iii.rightanswers.com/portal/controller/view/discussion/97?)

### Related Solutions

- [What do the "Outstanding balance options" and "History options" under Patron Account Count Reports in SimplyReports include?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930691502831)
- [Determining the number of registered patrons](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930339388822)
- [Patron registration counts differ](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930359958800)
- [Question about Circulation by Postal Code report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930374734874)
- [Errors accessing reports in the client](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930369939404)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)