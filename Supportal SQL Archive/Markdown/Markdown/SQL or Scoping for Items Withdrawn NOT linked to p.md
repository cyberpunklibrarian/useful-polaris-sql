SQL or Scoping for Items Withdrawn NOT linked to patron fee records - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=20)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# SQL or Scoping for Items Withdrawn NOT linked to patron fee records

0

42 views

Hello, I am able to pull up Item Records with a Circulation Status of Withdrawn. Is it possible, using a SQL search or Scoping, to find Withdrawn items that do not have a link to any patron fee records? Thank you for your help.

asked 2 years ago  by <img width="18" height="18" src="../../_resources/6a2f83474fda45a9bc3eefd4068209e1.jpg"/>  tamara.zavinski@oxnard.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_645"></a>[](#)

0

<a id="downVoteAns_645"></a>[](#)

Thanks JT. I will give it a shot.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/6a2f83474fda45a9bc3eefd4068209e1.jpg"/>  tamara.zavinski@oxnard.org

- <a id="acceptAnswer_645"></a>[Accept as answer](#)

<a id="commentAns_645"></a>[Add a Comment](#)

<a id="upVoteAns_622"></a>[](#)

0

<a id="downVoteAns_622"></a>[](#)

Hi Tamara.

Here is a query that may work for you, or at least be a start.

You can join the ItemRecords view to the PatronAccount table, but this table is more or less a history table. This means that if a Withdrawn item is linked to PatronAccount, it may be linked to two (or maybe more) entries: One for the original charge, and another for the payment/waiver of that charge. To get around that. This query does a left outer join to a subquery that looks for PatronAccount entries where the outstanding amount is 0.00, indicating that the charge to this patron, for this item has been taken care of. The line with IS NULL basically weeds out any item/patron combinations where a charge has been paid/waived.

This query only retrieves item records that are still in the database. For items that have been purged, I am pretty sure the PatronAcctDeletedItemRecords table can be joined to PatronAccount instead of the ItemRecords view in the query.

Hope this helps. If you have any questions, you can reply to the thread or email me.

JT

SELECT
ir.ItemRecordID
,ir.Barcode
,pa.PatronID
,pab.StatusDesc as BillingStatus
,pax.Description as TransactionType
,frc.FeeDescription
,pa.OutstandingAmount
FROM Polaris.Polaris.ItemRecords ir WITH (NOLOCK)
JOIN Polaris.Polaris.PatronAccount pa (NOLOCK)
ON (ir.ItemRecordID = pa.ItemRecordID)
JOIN Polaris.Polaris.FeeReasonCodes frc (NOLOCK)
ON (pa.FeeReasonCodeID = frc.FeeReasonCodeID)
JOIN Polaris.Polaris.PatronAcctBillingStatus pab (NOLOCK)
ON (pa.BillingStatusID = pab.StatusID)
JOIN Polaris.Polaris.PatronAccTxnCodes pax (NOLOCK)
ON (pa.TxnCodeID = pax.TxnCodeID)
LEFT OUTER JOIN
 (SELECT PatronID, ItemRecordID
 FROM Polaris.Polaris.PatronAccount WITH (NOLOCK)
 WHERE OutstandingAmount = 0.00) NoneOwed
ON (pa.PatronID = NoneOwed.PatronID and ir.ItemRecordID = NoneOwed.ItemRecordID)
WHERE ir.ItemStatusID = 11
AND NoneOwed.PatronID IS NULL
ORDER BY pa.PatronID, ir.Barcode

answered 2 years ago  by <img width="18" height="18" src="../../_resources/6a2f83474fda45a9bc3eefd4068209e1.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_622"></a>[Accept as answer](#)

<a id="commentAns_622"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL help for finding previous, previous patron on an item](https://iii.rightanswers.com/portal/controller/view/discussion/105?)
- [Query Syntax for a Linked Server](https://iii.rightanswers.com/portal/controller/view/discussion/491?)
- [Is there any harm in deleting old item/bib record templates?](https://iii.rightanswers.com/portal/controller/view/discussion/591?)
- [SQL for waiving certain fines](https://iii.rightanswers.com/portal/controller/view/discussion/192?)
- [Do you use fine codes for items not related to a material type?](https://iii.rightanswers.com/portal/controller/view/discussion/359?)

### Related Solutions

- [Polaris SIP not returning detailed fine information](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930827108312)
- [Notice history: item and account notices time threshold](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930958906927)
- [Which branch's Replacement Fee Defaults setting is used?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181112111440588)
- [Patron / Material Type Loan Limit Blocks policy table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930651711850)
- [Fee reason display order in Leap](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930612692592)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)