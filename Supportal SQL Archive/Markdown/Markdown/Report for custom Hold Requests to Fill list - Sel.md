Report for custom Hold Requests to Fill list - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Report for custom Hold Requests to Fill list

0

86 views

Originally posted by rpatterson@aclib.us on Monday, April 24th 14:33:26 EDT 2017
Our staff weren't fond of printing the holds 'pick list' from Request Manager (font too small), or from Reports and Notices (too much wasted space) and requested a report with a more readable output. Amy at Richland shared a custom holds report they'd created, and we modified it to fit our needs. I'm sharing our resulting report here in case anyone is interested in a similar option and wants to edit it for their library. We set ours so the GN column only appears if there is a graphic novel, and the barcode column only appears if there is an item level hold - why we have the NULL in the query. DataSet1 Select ird.CallNumber as Call#, br.BrowseTitle as Title, 'Author' = case when br.BrowseAuthor is NULL then ' ' else br.BrowseAuthor end, 'Item Specific' = CASE when hr.ItemLevelHold = 1 then hr.itembarcode else Null end, 'GN' = CASE when sc.StatisticalCodeID = 43 then 'GN' else Null end, hr.LastStatusTransitionDate AS 'Pending date', hpu.abbreviation as Pickup from Polaris.SysHoldRequests hr with (nolock) inner join Polaris.Organizations hpu with (nolock) on (hr.PickupBranchID = hpu.OrganizationID) inner join Polaris.BibliographicRecords br with (nolock) on (hr.BibliographicRecordID = br.BibliographicRecordID) left join Polaris.CircItemRecords cir with (nolock) on (case when hr.TrappingItemRecordID IS Not null then HR.TrappingItemRecordID else HR.ItemLevelHoldItemRecordID end = cir.ItemRecordID) INNER JOIN StatisticalCodes sc with (NOLOCK) ON (cir.StatisticalCodeID = sc.StatisticalCodeID AND sc.OrganizationID = hr.PickupBranchID) left join Polaris.ItemRecordDetails ird with (nolock) on (cir.ItemRecordID = ird.ItemRecordID) where hr.PickupBranchID in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17) and cir.AssignedBranchID in (@branch) and hr.SysHoldStatusID in (4) order by CallNumber asc,br.BrowseAuthor DataSet2 (just adding a parameter for Branch location) select organizationid, abbreviation from polaris.polaris.organizations with (nolock) where organizationid in (3,4,5,6,7,8,9,10,11,12,13,14,16,17)

asked 4 years ago  by <img width="18" height="18" src="../../_resources/42ceeda475fc4a16a9fd0477ec0f27b5.jpg"/>  support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Can you get a list of exact dates an item was placed on hold?](https://iii.rightanswers.com/portal/controller/view/discussion/1031?)
- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [Renewals reports](https://iii.rightanswers.com/portal/controller/view/discussion/83?)

### Related Solutions

- [Statistics for Holds and ILLs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930628713826)
- [Custom report parameters not allowing multiple selections in Polaris](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930424447003)
- [Hold Request to Fill Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161031124829450)
- [Question about a saved SimplyReports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930457380859)
- [Question about published SimplyReports reports](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989986518)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)