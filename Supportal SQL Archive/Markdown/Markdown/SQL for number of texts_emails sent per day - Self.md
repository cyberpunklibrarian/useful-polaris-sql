SQL for number of texts/emails sent per day - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL for number of texts/emails sent per day

0

108 views

We're trying to get an idea of the percent of text sent that fail/bounce back as undeliverable.  Anyone have a SQL query for that?  

Thanks,

Robin Dye

asked 2 years ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_486"></a>[](#)

0

<a id="downVoteAns_486"></a>[](#)

Answer Accepted

Hi All,

I got an answer from Sarah Goff at North Olympic Library System from the same question I posed in the IUG Forum.  Thanks Sarah!

Hey Robin! Would this work for you? It counts patrons that receive a text per day, so it misses when a patron receives more than one, but it might be helpful regardless.
SELECT Convert(date, NotificationDateTime) as Date
,COUNT(PatronID) AS 'Text Count'
FROM \[PolarisTransactions\].\[Polaris\].\[NotificationLog\]
WHERE DeliveryOptionID = 8
GROUP BY Convert(date, NotificationDateTime)
ORDER BY Date DESC
Sarah Goff
sgoff@nols.org

answered 2 years ago  by ![rdye@krl.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_rdye@krl.org20190514151552.JPG)  rdye@krl.org

<a id="commentAns_486"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for circulation numbers](https://iii.rightanswers.com/portal/controller/view/discussion/94?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [How to Find Definitions of ActionTakenID Numbers](https://iii.rightanswers.com/portal/controller/view/discussion/96?)
- [Adding number of "live" items to a report](https://iii.rightanswers.com/portal/controller/view/discussion/666?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)

### Related Solutions

- [Patron registration numbers jumped in value](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989626569)
- [Is it safe to run the EDI SQL job more than once a day](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=190124092946263)
- [Opening Day Collections](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160503113356129)
- [SQL Queries for the Find Tool - PUG 2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161212104426898)
- [How can SQL jobs be accessed?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930442864574)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)