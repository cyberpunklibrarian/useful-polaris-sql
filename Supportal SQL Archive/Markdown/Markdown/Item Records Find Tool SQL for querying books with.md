Item Records Find Tool SQL for querying books with specific subject headings - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Item Records Find Tool SQL for querying books with specific subject headings

0

21 views

I'm wondering if the SQL experts here can help me with this. I am helping to create record sets for our member libraries to assist in weeding projects, and one of the specifics requested are record sets of "no brainer" items to weed, such as income tax preparation books published prior to the new tax law in 2018. I have to create item record sets here because we have multiple libraries sharing the system and thus, sharing bib records. Limiting down to item-specific records will help our member libraries.

My initial thought was to search for specific 650 tags with words like "taxation" or "income tax" somewhere in the field. I started working on a SQL script with multiple joins to pull in data from MARC tags and subfields. Once I get a script that works and queries subject headings from the bib record I can adjust my subject heading query to look for the specific books I want, but I'm having trouble querying the subject heading tags. 

Any advice here? I'll copy my rough draft script here for reference, but this doesn't actually work. Polaris will complete the search, but my results are basically returning every record in the system. In other words, my attempt to limit by subject headings has failed. Thanks!
SELECT CR.ItemRecordID FROM CircItemRecords CR with (NOLOCK)
JOIN BibliographicRecords BR
ON CR.AssociatedBibRecordID = BR.BibliographicRecordID
left outer join BibliographicTags BT650
on BT650.BibliographicRecordID = BR.BibliographicRecordID and BT650.TagNumber = 650 -- subject tag number
left join BibliographicSubfields BS
on BS.BibliographicTagID = BT650.BibliographicTagID and BS.Subfield = 'a' -- primary subject subfield
WHERE RTRIM(BS.Data) LIKE '%tax%'

asked 27 days ago  by <img width="18" height="18" src="../../_resources/7029f8ea76c14141b4aa9c4c27b528e3.jpg"/>  bret@washlibs.org

[find tool](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=440#t=commResults) [record sets](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=73#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 27 days ago

<a id="upVoteAns_1161"></a>[](#)

1

<a id="downVoteAns_1161"></a>[](#)

Answer Accepted

It looks like your code is set up a little strange.  You're doing a left outer join that, when executed as written, pulls ALL the tags regardless of matching an existing bibrecordID.  I think this is why you end up pulling everything instead of just the tax stuff.  I'm also not sure why you're doing an RTRIM on the bs.data field when you already have truncation in your LIKE statement?  I updated the code to use inner joins and to search all subfields of all 6XX tags.

SELECT DISTINCT(cir.ItemRecordID)
FROM Polaris.Polaris.CircItemRecords AS \[cir\] WITH (NOLOCK)
INNER JOIN Polaris.Polaris.BibliographicRecords AS \[br\] WITH (NOLOCK)
ON cir.AssociatedBibRecordID = br.BibliographicRecordID
INNER JOIN Polaris.Polaris.BibliographicTags AS \[bt\] WITH (NOLOCK)
ON br.BibliographicRecordID = bt.BibliographicRecordID AND bt.TagNumber LIKE '6%'
INNER JOIN Polaris.Polaris.BibliographicSubfields AS \[bs\] WITH (NOLOCK)
ON bs.BibliographicTagID = bt.BibliographicTagID
WHERE BS.Data LIKE '%tax%'

answered 27 days ago  by <img width="18" height="18" src="../../_resources/7029f8ea76c14141b4aa9c4c27b528e3.jpg"/>  trevor.diamond@mainlib.org

<a id="commentAns_1161"></a>[Add a Comment](#)

<a id="upVoteAns_1162"></a>[](#)

0

<a id="downVoteAns_1162"></a>[](#)

Perfect, I'm still inexperienced with joins and this fixes it. Thank you!

answered 27 days ago  by <img width="18" height="18" src="../../_resources/7029f8ea76c14141b4aa9c4c27b528e3.jpg"/>  bret@washlibs.org

- <a id="acceptAnswer_1162"></a>[Accept as answer](#)

<a id="commentAns_1162"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Changing offensive LOC subject terms to local subject terms](https://iii.rightanswers.com/portal/controller/view/discussion/879?)
- [SQL for items withdrawn by a particular staff member](https://iii.rightanswers.com/portal/controller/view/discussion/809?)
- [Why isn't 'modify the headings (or references) to match the new authority heading' working?](https://iii.rightanswers.com/portal/controller/view/discussion/381?)
- [Deleting item records with holds](https://iii.rightanswers.com/portal/controller/view/discussion/32?)
- [Is there a job/report that we can run to relink bibliographic headings to their appropriate authority records (without having to do it manually for each heading)?](https://iii.rightanswers.com/portal/controller/view/discussion/239?)

### Related Solutions

- [How do I find item counts by material type?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930417498777)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [What is the difference between "See" and "See Also"?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930297807799)
- [Remove subfield e from 100 tag for RDA records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930517311975)
- [marcgt value for 380$2](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181011195525699)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)