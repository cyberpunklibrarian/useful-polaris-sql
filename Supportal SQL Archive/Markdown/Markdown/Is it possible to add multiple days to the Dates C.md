[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Is it possible to add multiple days to the Dates Closed table without adding them individually?

0

75 views

Is it possible to add multiple days to the Dates Closed table without adding each day individually?  I need to add two months worth of dates to the closed table and I don't want to add them one by one.  We're hosted so our SQL server is read only.

&nbsp;

&nbsp;

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_efbb8d2236a1425ebdc07f66678236ca.jpg"/> lori@esrl.org

[dates closed](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=352#t=commResults) [sa](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=353#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 4 years ago

<a id="upVoteAns_927"></a>[](#)

0

<a id="downVoteAns_927"></a>[](#)

Thanks, Trevor.  I figured that was the case.

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_efbb8d2236a1425ebdc07f66678236ca.jpg"/> lori@esrl.org

- <a id="acceptAnswer_927"></a>[Accept as answer](#)

<a id="commentAns_927"></a>[Add a Comment](#)

<a id="upVoteAns_925"></a>[](#)

0

<a id="downVoteAns_925"></a>[](#)

The only way to do this is via SQL.  If it is a large range of dates, you can try getting in touch with your site manager to help with this (I'm sure they had a lot of practice in March).

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_efbb8d2236a1425ebdc07f66678236ca.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_925"></a>[Accept as answer](#)

<a id="commentAns_925"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for Range of Closed Dates](https://iii.rightanswers.com/portal/controller/view/discussion/716?)
- [INSERTing urls into BibliographicSubfields Table](https://iii.rightanswers.com/portal/controller/view/discussion/118?)
- [Individual staff log-ins -- can it work for us?](https://iii.rightanswers.com/portal/controller/view/discussion/266?)
- [Advice on creating our own table in Polaris database](https://iii.rightanswers.com/portal/controller/view/discussion/1300?)
- [adding info to bibs via SQL script?](https://iii.rightanswers.com/portal/controller/view/discussion/107?)

### Related Solutions

- [How do Dates Closed factor in to grace periods?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930368145777)
- [Bulk change item due dates](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930588598835)
- [Which library's settings are used for dates closed and days not fineable?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930253968387)
- [Is it safe to run the EDI SQL job more than once a day](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=190124092946263)
- [Leap Year Birthdays](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930393212957)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)