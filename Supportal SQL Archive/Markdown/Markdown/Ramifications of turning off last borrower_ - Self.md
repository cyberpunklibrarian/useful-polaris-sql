Ramifications of turning off last borrower? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Ramifications of turning off last borrower?

0

38 views

We are considering turning last borrower off. Our reason ties in with patron privacy and staff behavior. 

I know this does not delete that data but rather "hides" it? 

My question is, if there was a real reason to retrieve that data, is there a simple SQL query that could run in the staff client?

For example, in our snag procedure we are not supposed to check the snag in, meaning we have the information we need to alert the borrower. If last borrower is turned off and a snag gets checked in accidentally we do not have that information. Would a simple SQL query be able to retrieve the needed info? And if so, does someone have that query to share as:

1 - I am SQL useless and 

2 - Our only access to SQL queries is through the staff client

Thanks -

Chad

asked 1 year ago  by ![chad.eller@gastongov.com](https://iii.rightanswers.com/portal/app/images/custom/avatar_chad.eller@gastongov.com20210201162116.jpg)  chad.eller@gastongov.com

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_860"></a>[](#)

0

<a id="downVoteAns_860"></a>[](#)

Answer Accepted

Hi Chad.

Here are a couple of queries that may do what you want. If possible, I suggest trying them on a variety of items with various lengths of time since the last checkin while you have the last borrower option turned on. That way, you can make sure that it works, and possibly to identify any situations where it doesn't give the correct patron info.

Note that there are two tables you will need to search. If the item was last checked in that day, you need to check ItemRecordHistoryDaily. If it was yesterday or earlier, you need to check ItemRecordHistory. There is probably a way to query both tables and get the most recent checkin, but this would be more complicated. (If you want to try that, please reply.)

The TOP 1 and ORDER BY parts of the statement should get the most recent checkin or checkin when an item was Lost.

*Query for today's checkin:*

SELECT TOP 1
irhd.PatronID
FROM Polaris.Polaris.ItemRecordHistoryDaily irhd WITH (NOLOCK)
JOIN Polaris.Polaris.ItemRecords ir (NOLOCK)
ON (irhd.ItemRecordID = ir.ItemRecordID)
WHERE
irhd.ActionTakenID in (11,12)
AND ir.Barcode like '31234555554443'
ORDER BY irhd.TransactionDate DESC

(Change the barcode number as needed. Make sure the single quotes are on each side.)

*Query to run only if the first query brings no results:*

SELECT TOP 1
irh.PatronID
FROM Polaris.Polaris.ItemRecordHistoryDaily irh WITH (NOLOCK)
JOIN Polaris.Polaris.ItemRecords ir (NOLOCK)
ON (irh.ItemRecordID = ir.ItemRecordID)
WHERE
irh.ActionTakenID in (11,12)
AND ir.Barcode like '31234555554443'
ORDER BY irh.TransactionDate DESC

answered 1 year ago  by <img width="18" height="18" src="../../_resources/6511e3a7fd0e43d08d2b5fdec85b7caf.jpg"/>  jwhitfield@aclib.us

<a id="commentAns_860"></a>[Add a Comment](#)

<a id="upVoteAns_861"></a>[](#)

0

<a id="downVoteAns_861"></a>[](#)

Thank you! 

answered 1 year ago  by ![chad.eller@gastongov.com](https://iii.rightanswers.com/portal/app/images/custom/avatar_chad.eller@gastongov.com20210201162116.jpg)  chad.eller@gastongov.com

- <a id="acceptAnswer_861"></a>[Accept as answer](#)

<a id="commentAns_861"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Does Borrow by Mail work in Leap?](https://iii.rightanswers.com/portal/controller/view/discussion/758?)
- [Is anyone that has multiple locations using Borrow by Mail?](https://iii.rightanswers.com/portal/controller/view/discussion/552?)
- [SQL Query for how many first time borrowers are checking out holds?](https://iii.rightanswers.com/portal/controller/view/discussion/504?)
- [Advice on load balancing for floating collections](https://iii.rightanswers.com/portal/controller/view/discussion/1003?)
- [Does anyone use the collection agency reporting type of 'other'?](https://iii.rightanswers.com/portal/controller/view/discussion/345?)

### Related Solutions

- [Label/Paper stock for BBM Mailer](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=171201090449943)
- [How to find the second to last borrower](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930492867728)
- [Change text on checkout receipts](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930905424507)
- [Workstation is only printing check-out receipts](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930700197420)
- [How are due dates calculated?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930778190992)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)