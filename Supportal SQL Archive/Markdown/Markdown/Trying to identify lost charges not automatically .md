Trying to identify lost charges not automatically waived via Lost Item Recovery settings due to known bug - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Trying to identify lost charges not automatically waived via Lost Item Recovery settings due to known bug

0

50 views

Hello, all,

We have encountered a known bug that, sporadically, results in replacement cost charges not being waived automatically (per a library's Lost Item Recovery settings) when a patron returns a lost item within the 6 month window allowed by the library. I am trying to help the library track down those charges so they can waive them instead of having to wait for a patron to notice, question a charge, or run into blocks that should not actually apply.

I cannot now remember which Simply Report query I used as the basis for the following script, but it seems to be generating the results we need (though, being based on the item record history, limited to 365 days, we can only track back for a year). In any case, my question: is there any way to include within this script the qualifier that the date the lost item was checked in be within 180 days of the date the replacement cost fee was assigned? (or is there a much better way to approach this period? :-)

I am still a SQL newbie, so apologies in advance if the following is clumsy (or completely off-base):

SELECT        FRC.FeeDescription,
        o.OrganizationID as TransactingLibID,
        o.Name AS TransactingLibName,
        OO.OrganizationID AS OwningLibID,
        OO.Name as OwningLibrary,
        PATC.\[Description\] AS TransactionType,
        PA.TxnAmount as ChargeAmt,
        PA.TxnDate AS ChargeDate,
        irh.TransactionDate AS ItemCKIPrevLost,
        case when P.Barcode = '' then PTD.PatronBarcode else P.Barcode end AS PatronBarcode,
        CIR.Barcode as ItemBarcode,
        BR.BrowseTitle AS Title,
        MT.Description as MaterialType,
        PA.ItemRecordID    
    
    FROM
        Polaris.PatronAccount PA (NOLOCK)
    
        INNER JOIN Polaris.Organizations O (NOLOCK)
            ON PA.OrganizationID = O.OrganizationID
        INNER JOIN Polaris.Patrons P (NOLOCK)
            ON (PA.PatronID = P.PatronID)
        INNER JOIN Polaris.PatronAccTxnCodes PATC (NOLOCK)
            ON (PA.TxnCodeID = PATC.TxnCodeID)
        LEFT OUTER JOIN Polaris.PolarisUsers PU (NOLOCK)
            ON PA.CreatorID = PU.PolarisUserID
        LEFT OUTER JOIN Polaris.Workstations W (NOLOCK)
            ON PA.WorkStationID = W.WorkstationID
        LEFT OUTER JOIN Polaris.PatronsToDelete PTD (NOLOCK)
            ON PTD.PatronID = P.PatronID
        INNER JOIN Polaris.Polaris.FeeReasonCodes FRC (nolock)
            ON FRC.FeeReasonCodeID = PA.FeeReasonCodeID
        INNER JOIN Polaris.Polaris.CircItemRecords CIR (nolock)
            ON CIR.ItemRecordID = PA.ItemRecordID
        INNER JOIN Polaris.Polaris.Organizations OO (nolock)
            ON OO.OrganizationID = CIR.AssignedBranchID
        INNER JOIN Polaris.Polaris.BibliographicRecords BR (nolock)
            ON BR.BibliographicRecordID = CIR.AssociatedBibRecordID
        INNER JOIN Polaris.Polaris.MaterialTypes MT (nolock)
            ON MT.MaterialTypeID = CIR.MaterialTypeID
        INNER JOIN Polaris.Polaris.ItemRecordHistory irh with (nolock)
            ON cir.ItemRecordID = irh.ItemRecordID
    WHERE
        o.OrganizationID IN (35)
           AND PA.TxnCodeID = 1 --charge (not payment)
        AND OutstandingAmount > 0
        AND PA.FeeReasonCodeID IN (-1) --replacement cost
        AND IRH.ActionTakenID IN (12) -- checked in - item was previously lost by patron
        AND irh.PatronID = pa.PatronID
        AND PA.TxnDate < irh.TransactionDate
    ORDER BY PA.TxnDate

Thanks, much, in advance for any insight or suggestions you might be able to offer - or for simply confirming what I am asking is not doable via SQL.

Alison Hoffman

Monarch Library System

asked 1 year ago  by <img width="18" height="18" src="../../_resources/71438077ff9a4f4fb1d091e35c735706.jpg"/>  ahoffman@monarchlibraries.org

[lost and found](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=266#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_859"></a>[](#)

0

<a id="downVoteAns_859"></a>[](#)

Answer Accepted

Hi Alison.

The following tweaks may help your query:

(1) Change the join to ItemRecordHistory to join to PatronAccount.

INNER JOIN Polaris.Polaris.ItemRecordHistory irh with (nolock)
            ON PA.ItemRecordID = irh.ItemRecordID

(This may not have any impact, but there is a slight chance it may make the query run a bit quicker.)

(2) Change the last line to:

AND DATEDIFF(DAY,PA.TxnDate,irh.TransactionDate) < 180

I'm not sure if a charge created at 5 PM on day 1 and waived at 6 PM on the 180th day will show up. If you need to, you could probably be more precise using "HOUR" or "MINUTE" and the appropriate hours or minutes in 180 days.

Fortunately for us, we don't seem to have this particular issue so I can't replicate what you need exactly. (I did find a couple of transactions where a bill was created manually just prior to the item being checked in to clear the Lost bill.) Hopefully this will be helpful, or at least be a start.

JT

answered 1 year ago  by <img width="18" height="18" src="../../_resources/71438077ff9a4f4fb1d091e35c735706.jpg"/>  jwhitfield@aclib.us

JT Thank you! It looks as though that DateDiff addition is doing just what I need it to do! I am filing that away for future use. I love adding new tools to the toolkit. Thank you, again! :-)

— ahoffman@monarchlibraries.org 1 year ago

Hi Again, Allison.

I noticed that I had something odd happen when I tried some different timeframes. I had some "false positives" where the ItemCKIPrevLost date was before the ChargeDate. I think that it was finding ChargDates that were within N days either side of the ItemCKIPrevLost date.

Anyway, i was able to come up with another tweak that may resolve this. I added a condition to the join to ItemRecordHistory that restricted it to irh transations with a date later than the PA date. The revised join is

INNER JOIN Polaris.Polaris.ItemRecordHistory irh with (nolock)
            ON (PA.ItemRecordID = irh.ItemRecordID AND irh.TransactionDate > PA.TxnDate)

Hope this helps a bit more than the original. ![embarassed](../../_resources/4fc4a7715587425d8a250be13b05b1f7.gif)

JT

— jwhitfield@aclib.us 1 year ago

<a id="commentAns_859"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Report of Lost and Paid Items](https://iii.rightanswers.com/portal/controller/view/discussion/331?)
- [SQL for Lost and Paid Items](https://iii.rightanswers.com/portal/controller/view/discussion/662?)
- [Average time things are lost before coming back?](https://iii.rightanswers.com/portal/controller/view/discussion/436?)
- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [Does anyone know a SQL command for Polaris for finding totals by collection of items with zero circulation between two set dates? (Not calendar year.)](https://iii.rightanswers.com/portal/controller/view/discussion/415?)

### Related Solutions

- [Which lost item recovery settings are used for partially paid replacement costs?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170523113239517)
- [Items converting to Lost status](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930282614657)
- [Billed items be set to Lost and Lost Item Recovery table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930478370792)
- [What happens when old Lost items are returned?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180907110226360)
- [4.0 Patron Services - Lost Item Charge Options](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160908155555187)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)