[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# Deleting Material Types using SQL

0

166 views

I am looking to delete some old Material Types. It is my understanding that this can't be done in SA, but I am a total beginner with SQL. Can someone walk me through the steps to delete Material Types using SQL? Polaris Help says "You cannot delete a material type from the table in Polaris Administration. The system administrator can change the Material Types table using the Query Analyzer in SQL Server." I'm really not sure if we have the Query Analyzer or how to access it. ![embarassed](../_resources/smiley-embarassed_74dfa1ef318b43b1984bcc456c0746d6.gif)

We currently have Polaris 5.1 and we are on SQLServer2008 R2.

&nbsp;Also, is it the same process to delete old stat codes? It seems I can't delete them from SA either.

TIA for your help and advice!

asked 6 years ago by ![sbills@lpld.lib.in.us](https://iii.rightanswers.com/portal/app/images/custom/avatar_sbills@lpld.lib.in.us20201021105928.jpg) sbills@lpld.lib.in.us

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 6 years ago

<a id="upVoteAns_357"></a>[](#)

3

<a id="downVoteAns_357"></a>[](#)

Hmm, I don't recommend deleting old codes unless it is an absolute requirement. The reason is that this may throw off your historic reporting information. You might instead want to prefix the codes with zzDoNotUseOldCodeName; so you'll still be able to perform historic reporting on these codes.

Keeping old codes would be somewhat less important if you've turned off all transaction logging in your Polaris instance, but I'd still probably recommend keeping the codes around until the end of your reporting "year".

Finally deleting (or updating) anything through SQL is not for the faint of heart. Though folks may send you some tips on how to do this via later posts, you may also want to ask about Innovative doing it for you as a billable service. That way in case anything goes wrong, it will be on their hands to fix it and get your system going again.

Good luck!

PS Here is a list of all the tables that will need checked/updated when removing the codes. For example, you'll need to make sure that your item templates, po lines, etc. are no longer using these codes and will need moved to new codes.

CircItemRecords, EphemeralItemRecords, InnReachFilterMaterialTypes, InvLines, ItemTemplates, LoanLimits, MaterialLoanLimits, NCIPMediumTypes, POLines, SA_BorrowByMail_MaterialTypesPermitted, SA_ChargeForCheckOut_MaterialTypes, SA_FloatingMatrixMaterialLimits, SA_FloatingMatrixMaterialTypes, SA_MaterialTypeGroups_Definitions, SA_MediaDispenser_MaterialTypes, SA_NCIPMediumTypes_Incoming, SA_NCIPMediumTypes_Outgoing, SA_ShelvingStatusDurations, SelfCheckMaterialTypes, SelListLines, SHRCopies

answered 6 years ago by <img width="18" height="18" src="../_resources/default-avatar_48dc518b546f47d1a028f6fefec36be3.jpg"/> wosborn@clcohio.org

- <a id="acceptAnswer_357"></a>[Accept as answer](#)

Wow, thank you for that information, Wes! I'm new to SA and I didn't realize the effect it could have. I'll definitely just leave it be, but I'll save the list you sent for future reference if I get comfortable enough to dive into it.

— sbills@lpld.lib.in.us 6 years ago

<a id="commentAns_357"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Do you use fine codes for items not related to a material type?](https://iii.rightanswers.com/portal/controller/view/discussion/359?)
- [In a consortium of 8 libraries, Is it possible to limit either a collection or material type by both the patron's home library and their hold pickup to branches within that library?](https://iii.rightanswers.com/portal/controller/view/discussion/262?)
- [Does anyone have a SQL query to get circulation statistics for material with the subject heading Historical Fiction?](https://iii.rightanswers.com/portal/controller/view/discussion/1151?)
- [Deleting fines/fees](https://iii.rightanswers.com/portal/controller/view/discussion/1077?)
- [How to delete a location?](https://iii.rightanswers.com/portal/controller/view/discussion/1367?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [SQL query for a count of items by material type](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930506564972)
- [Checklist: Adding a New Material Type](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181212124422933)
- [Locating patrons using a specific address type](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180628133452068)
- [Floating guide](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930254601444)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)