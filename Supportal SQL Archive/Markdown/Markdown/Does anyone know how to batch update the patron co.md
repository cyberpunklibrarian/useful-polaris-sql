[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Does anyone know how to batch update the patron code field?

0

59 views

Hi, we currently use the Patron Code to determine if an account is a "juvenile" -- under 14 years old -- or an "adult" -- over 14 years old.  I noticed recently that there are still many accounts with a "juvenile" patron code that should actually be an "adult" patron code.  I don't want to hand change all of these accounts, nor do I know SQL to write a script to change them all.  Is there a way to use the batch update feature to change the patron code from juvenile to adult based on a birthday?  Thanks for any insight you are able to provde me. Have a great Thanksgiving Holiday!

&nbsp;

Sincerely,

Holly Sealine

Ankeny Kirkendall Public Libary

asked 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_9c806954096f4d84a5f6b944e6e4690e.jpg"/> hsealine@ankenyiowa.gov

[batch update](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=397#t=commResults) [patron code](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=330#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 3 years ago

<a id="upVoteAns_1032"></a>[](#)

0

<a id="downVoteAns_1032"></a>[](#)

Hey Holly-

&nbsp;

You can search for patrons based on birthday (limit by patron code), then put them in a record set.  You can do a bulk change to the record set to set the patron code to adult.

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_9c806954096f4d84a5f6b944e6e4690e.jpg"/> trevor.diamond@mainlib.org

- <a id="acceptAnswer_1032"></a>[Accept as answer](#)

<a id="commentAns_1032"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [I have more and more patrons that are using contract cell phone services, but they want to get text messages from us. I have not found a way to update the cell phone provider list. Does anyone know how to do this?](https://iii.rightanswers.com/portal/controller/view/discussion/261?)
- [Does anyone have a SQL report that will show renewal vs new cards for a particular patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/1377?)
- [Does anyone know how to put a patron's contact preference on their holds slip?](https://iii.rightanswers.com/portal/controller/view/discussion/652?)
- [Patron Code "Staff "override](https://iii.rightanswers.com/portal/controller/view/discussion/704?)
- [Besides Quipu, does any know of any other products that integrate with Polaris, allows patrons to self-register for a library card online, and verifies and standardizes the address information entered as well as provides name and residency verification?](https://iii.rightanswers.com/portal/controller/view/discussion/253?)

### Related Solutions

- [Patron Code for Computer / Internet Use](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930652657234)
- [Can I delete a patron code from the Patron Codes Policy Table?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930483335271)
- [Incorrect postal codes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930359078195)
- [New patron code is not selectable](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930960708226)
- [Adding a New Patron Statistical Class Code](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930391497478)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)