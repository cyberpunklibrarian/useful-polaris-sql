I am looking for a SQL that adds Previous Year to Date Circ to a query that runs Year to Date, Last Transaction Date and Lifetime Circ by collection.   - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# I am looking for a SQL that adds Previous Year to Date Circ to a query that runs Year to Date, Last Transaction Date and Lifetime Circ by collection.

0

43 views

Here is my current query:  

SELECT
        D.CallNumber,
        I.AssignedCollectionID,
        B.BrowseTitle AS 'Title',
        B.PublicationYear AS 'Date',
        I.Barcode,
        S.\[Description\] AS 'Status',
        I.YTDCircCount AS 'YTD circ',
        I.LastCircTransactionDate AS 'Last Trans.',
        I.LifetimeCircCount AS '# of circs'
FROM
        Polaris.polaris.CircItemRecords I
JOIN    Polaris.polaris.ItemRecordDetails D ON D.ItemRecordID = I.ItemRecordID
JOIN    Polaris.polaris.BibliographicRecords B ON B.BibliographicRecordID = I.AssociatedBibRecordID
JOIN    Polaris.polaris.ItemStatuses S ON S.ItemStatusID = I.ItemStatusID
WHERE   I.AssignedCollectionID = 7

I was able to add YTD circs but previous year circs don't seem to be part of the same table.

Any help would be appreciated.

asked 1 year ago  by <img width="18" height="18" src="../../_resources/cad9b7908bfe448f8f8ca5b54132e257.jpg"/>  rpool@deerparktx.org

[previous year circ counts](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=322#t=commResults) [sql query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=304#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_807"></a>[](#)

0

<a id="downVoteAns_807"></a>[](#)

Answer Accepted

 Try this:

 SELECT

        D.CallNumber,
        I.AssignedCollectionID,
        B.BrowseTitle AS 'Title',
        B.PublicationYear AS 'Date',
        I.Barcode,
        S.\[Description\] AS 'Status',
        I.YTDCircCount AS 'YTD circ',
        P.YTDCircCount AS 'PrvYR',
        I.LastCircTransactionDate AS 'Last Trans.',
        I.LifetimeCircCount AS '# of circs'
FROM Polaris.polaris.CircItemRecords I (NOLOCK)
JOIN    Polaris.polaris.ItemRecordDetails D (NOLOCK) ON D.ItemRecordID = I.ItemRecordID
JOIN    Polaris.polaris.BibliographicRecords B (NOLOCK) ON B.BibliographicRecordID = I.AssociatedBibRecordID
JOIN    Polaris.polaris.ItemStatuses S (NOLOCK) ON S.ItemStatusID = I.ItemStatusID
JOIN Polaris.Polaris.PrevYearItemsCirc P (NOLOCK) ON P.ItemRecordID = I.ItemRecordID --Previous Years Item Circ Counts
WHERE   I.AssignedCollectionID = 7

Rex Helwig

answered 1 year ago  by <img width="18" height="18" src="../../_resources/cad9b7908bfe448f8f8ca5b54132e257.jpg"/>  rhelwig@flls.org

<a id="commentAns_807"></a>[Add a Comment](#)

<a id="upVoteAns_809"></a>[](#)

0

<a id="downVoteAns_809"></a>[](#)

That works beautifully.  I couldn't get that final join right.  Thank you so much!!

answered 1 year ago  by <img width="18" height="18" src="../../_resources/cad9b7908bfe448f8f8ca5b54132e257.jpg"/>  rpool@deerparktx.org

- <a id="acceptAnswer_809"></a>[Accept as answer](#)

<a id="commentAns_809"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL query for circulation by call number range and date range](https://iii.rightanswers.com/portal/controller/view/discussion/190?)
- [SQL for circulation by call number prefix and date range](https://iii.rightanswers.com/portal/controller/view/discussion/913?)
- [COVID-19 Due Date Extensions, Fine Amnesty, Etc](https://iii.rightanswers.com/portal/controller/view/discussion/715?)
- [Patron expiration date update](https://iii.rightanswers.com/portal/controller/view/discussion/727?)
- [All library cards expire on the same date](https://iii.rightanswers.com/portal/controller/view/discussion/670?)

### Related Solutions

- [YTD circulation and Previous YTD circulation do not add up to Lifetime circulation](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930953785925)
- [Circulation statistics and deleted records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930736904760)
- [How do I reset the year-to-date circulation counts?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930831368342)
- [Leap Year Birthdays](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930393212957)
- [How do I schedule the Year End Circ Count Rollover job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930582726474)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)