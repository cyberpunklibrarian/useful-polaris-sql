[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# SQL for waiving - OverDues

0

169 views

We are heading towards going fine free and I would like to see if anyone has come up with a script to delete all overdue fines within SQL? I read a post from 2 years ago stating Polaris did not recommend this, but I know that many libraries have went fine free recently. Did anyone delete in a batch? My only other option is to have staff delete as they come up or possibly make record sets for staff to start digging in and delete as a project.

Please let me know.

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_60e9a0df89904b4f8f1fc333b23df431.jpg"/> brandon.williams@mesaaz.gov

[sql waive fines](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=37#t=commResults)

We paid III to waive all of our fines when we went Fine Free.  We didn't want to delete them since we wanted to be able to run reports/statistics off of what was waived.  Wes is right that III is doing this for free right now, I'd highly recommend going that route.

— ggosselin@richlandlibrary.com 4 years ago

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 4 years ago

<a id="upVoteAns_920"></a>[](#)

0

<a id="downVoteAns_920"></a>[](#)

Answer Accepted

I think that Innovative is offering this service for free now as part of the COVID-19 response. See this page for more information: https://www.iii.com/blog/resources-to-reopen-library-operations-at-your-physical-location/

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_60e9a0df89904b4f8f1fc333b23df431.jpg"/> wosborn@clcohio.org

<a id="commentAns_920"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for waiving certain fines](https://iii.rightanswers.com/portal/controller/view/discussion/192?)
- [Fees waived by username](https://iii.rightanswers.com/portal/controller/view/discussion/1123?)
- [SQL for Range of Closed Dates](https://iii.rightanswers.com/portal/controller/view/discussion/716?)
- [Deleting Material Types using SQL](https://iii.rightanswers.com/portal/controller/view/discussion/323?)
- [Creating Jobs in SQL Agent for an SSIS package](https://iii.rightanswers.com/portal/controller/view/discussion/403?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Replacement fines were not automatically waived after overdue paid](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930589018439)
- [Where are the waive and charge free text notes stored in the database?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930334007926)
- [Patron Status does not show a reminder that is in the NotificationHistory table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930782414146)
- [SIP payment types](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930295306398)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)