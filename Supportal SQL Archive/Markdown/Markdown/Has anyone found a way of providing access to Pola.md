[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Leap](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=13)

# Has anyone found a way of providing access to Polaris Reports & Notices for Leap users?

0

163 views

We are looking for a way to provide access to Reports & Notices to our Leap only users. I would love to hear from other users how you are addressing this need.

Marian McCollum

IT Systems Administrator

Library Systems & Services

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_62b8e44f53dc4546be27532ab5cdc7eb.jpg"/> marian.mccollum@lsslibraries.com

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 4 years ago

<a id="upVoteAns_828"></a>[](#)

1

<a id="downVoteAns_828"></a>[](#)

Answer Accepted

You have to buy a SQL Server License in order to run Polaris. This includes access to SQL Server Reporting Services (they actually use this to provide reports in the desktop client).  You can use SSRS to access reports on the web, too!  Same reports, but via browser.  Ask your site manager (if you are hosted, they will have to contact cloudops to help get it set up for you).

&nbsp;

It does still require a login, but it's the same login the staff use to access reports (you also have more granular control over access to folders and reports, if that's something you would like).

&nbsp;

Trevor Diamond

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_62b8e44f53dc4546be27532ab5cdc7eb.jpg"/> trevor.diamond@mainlib.org

You can do everything in SSRS EXCEPT for posting notices. Posting notices still requires using the client.

— wosborn@clcohio.org 4 years ago

<a id="commentAns_828"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris Leap use on Ipads](https://iii.rightanswers.com/portal/controller/view/discussion/52?)
- [Has anyone attempted to set up access to Leap over an SSL VPN? Was it successful?](https://iii.rightanswers.com/portal/controller/view/discussion/155?)
- [Is there any way to suppress the different claims options for items checked out to customers in LEAP?](https://iii.rightanswers.com/portal/controller/view/discussion/882?)
- [Is there a way to stop Leap from opening the cash drawer when printing out checkout receipts?](https://iii.rightanswers.com/portal/controller/view/discussion/1086?)
- [Permissions for Volunteers and Picklist?](https://iii.rightanswers.com/portal/controller/view/discussion/486?)

### Related Solutions

- [Adding Leap permissions en mass](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930306603411)
- [Error Logging into LEAP](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930455513823)
- [Innovative's HTTPS Everywhere Plan](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180920175535460)
- [Leap login error "You do not have the permission, Access Leap: Allow"](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930322636227)
- [Leap auto-logins without offering the chance to choose a branch](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930686721840)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)