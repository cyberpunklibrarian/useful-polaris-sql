SQL to Find all bibs with same title. - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL to Find all bibs with same title.

0

67 views

I have a record set of Bib titles for which we have multiple records due to edition, format,  etc. Does anyone have any SQL that would find all bibs with the same title so that I can put them in a record set and run a report against it? I only have access to SQL in the client so I am not sure if its possible.

Thanks in advance.

Andrew

asked 1 year ago  by <img width="18" height="18" src="../../_resources/e097fdefc7f44f5cbccdb81b2952d973.jpg"/>  andrew.wright1@dallaslibrary.org

[duplicate titles](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=326#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_826"></a>[](#)

0

<a id="downVoteAns_826"></a>[](#)

Hi Again Andrew.

I tried something on a whim and came up with the Find Tool Query below.

A couple of notes:

1.  It brought up a HUGE number of matches, so I added a line to limit by the first letter of the BrowseTitle. (I tried to limit by the SortTitle, but it didn't like that for some reason.) This may cause lots of extra matches for "A," "The" etc. but you can add a condition to exclude those if desired. ("AND br.BrowseTitle NOT LIKE 'the %').
2.   Joining on the SortAuthor should narrow down the results to material with the same title and author, but may omit bibs that do not have an author. You may be able to find them by removing the join on SortAuthor and add the condition "AND br.BrowseAuthor IS NULL" to the end.
3.   If you have a single result, There may still be a match. A separate search for that title should bring up any matches.

Hope this helps, or at least is a start.

JT

SELECT br.BibliographicRecordID from Polaris.Polaris.Bibliographicrecords br WITH (NOLOCK)
JOIN Polaris.Polaris.Bibliographicrecords br1 (NOLOCK)
ON (br.SortTitle = br1.SortTitle) AND (br.BrowseAuthor = br1.BrowseAuthor) AND (br.Bibliographicrecordid <> br1.Bibliographicrecordid)
WHERE br.RecordStatusID = 1
AND br1.RecordStatusid = 1
AND br.BrowseTitle like 'z%'

answered 1 year ago  by <img width="18" height="18" src="../../_resources/e097fdefc7f44f5cbccdb81b2952d973.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_826"></a>[Accept as answer](#)

JT.

Thank you so much and appologies for my late thanks. Im afraid other projects tied me up. This looks good and I will start working with it to see how it goes. Thank you again

ANdrew

— andrew.wright1@dallaslibrary.org 1 year ago

<a id="commentAns_826"></a>[Add a Comment](#)

<a id="upVoteAns_824"></a>[](#)

0

<a id="downVoteAns_824"></a>[](#)

Hi Andrew.

I'm not sure if there's a good way via the Find Tool, since it doesn't support all SQL functions. I found a dandy query using GROUP BY and HAVING that seemed to work but when I tried it in the Find Tool it was not happy about it. >sigh< There is a MainTitleEntries table, but a "main title entry" in this case includes added titles, so it finds way more than just BrowseTitle duplicates. (Out of 400K or so bibs in our database, using the MainTitleEntries table resulted in about 145K matches.)

Sorry this wasn't more help. Maybe someone else can come up with something.

JT

answered 1 year ago  by <img width="18" height="18" src="../../_resources/e097fdefc7f44f5cbccdb81b2952d973.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_824"></a>[Accept as answer](#)

<a id="commentAns_824"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL top bib checkouts between 2 dates](https://iii.rightanswers.com/portal/controller/view/discussion/650?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [SQL for bibs with holds and no owned items by branches](https://iii.rightanswers.com/portal/controller/view/discussion/821?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [Need help with query to find checked out titles with holds](https://iii.rightanswers.com/portal/controller/view/discussion/1063?)

### Related Solutions

- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Sort order for content carousels](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930381469391)
- [No one is able to request a particular title that looks like it is available](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=171109103434625)
- [Polaris title holds not updating in INN-Reach central holdings](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181025120442989)
- [How can I locate bibs that were generated during a Terminated import without using SQL Server Management Studio?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930570115337)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)