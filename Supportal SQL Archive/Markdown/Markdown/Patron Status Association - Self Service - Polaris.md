[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Patron Status Association

0

81 views

In Simply Reports > Patrons Tab> Patron Status Association column. It returns a series of number ranging from "1" to "145". Is there a chart explaining what each code means? "74" means a child association I think, but I haven't been able to find what each means. There are at least 20 different codes.  
<br/>Thanks!  
Trevor

asked 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_f08e683f44e1473bb2c5b0de4d9fa3c6.jpg"/> thunter@desototexas.gov

[associated patron links](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=301#t=commResults) [simply reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=11#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 3 years ago

<a id="upVoteAns_1015"></a>[](#)

1

<a id="downVoteAns_1015"></a>[](#)

Answer Accepted

Hi Tevor-

Thanks for grabbing the SQL query. I'd agree that it looks like it's returning the PatronID of the user who created the account.

Let me know if you have any other questions about this.

&nbsp;

\-Matt

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_f08e683f44e1473bb2c5b0de4d9fa3c6.jpg"/> mhammermeister@pinnaclelibraries.org

<a id="commentAns_1015"></a>[Add a Comment](#)

<a id="upVoteAns_1010"></a>[](#)

1

<a id="downVoteAns_1010"></a>[](#)

Hi Trevor-

&nbsp;

I'm not seeing a "patron status association" column in my SimplyReports.  I'm thinking this could be one of the User Defined Fields or a renamed form of Patron Statistical Class.    The easiest way to check would be to grab the SQL query for us to look at:

- Build and run your report in SimplyReports as normal. Make sure that "Patron Status Association" is one of the columns on the report.
- When you get the results, view the underlying HTML (in most browsers, it's CTRL+U or right-click -- "View Page Source")  
    - The browser may ask to "confirm form resubmission" - click yes.
- Search (CTRL+F) for "SqlStatementHide"
- After that text, you'll see "value=" followed by the SQL query. I've attached a sample image

If you could copy and paste that query here, we'd be able to determine exactly what this column is and walk you through the rest of the way.

&nbsp;

Please let me know if you run into any trouble with this process.

&nbsp;

Thanks

&nbsp;

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_f08e683f44e1473bb2c5b0de4d9fa3c6.jpg"/> mhammermeister@pinnaclelibraries.org

- <a id="acceptAnswer_1010"></a>[Accept as answer](#)

- [simplyreports sql.jpg](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/867?fileName=867-1010_simplyreports+sql.jpg "simplyreports sql.jpg")

<a id="commentAns_1010"></a>[Add a Comment](#)

<a id="upVoteAns_1014"></a>[](#)

0

<a id="downVoteAns_1014"></a>[](#)

Hello!

  
Thank you for your help. I didn't know about looking at the SQL Query. I think I answered my question. It looks like it is Creator ID. I pasted the value below and then attached a text file as well in case the paste came out weird.

Please double check to make sure I haven't lost my mind.

Thanks!  
Trevor

  
&lt;input type="hidden" name="SQLStatementHide" id="SQLStatementHide" value="select p.Barcode as PatronBarcode,pc.Description as PatronCodeDescr,pr.PatronID,sc.Description as StatDescr,pr.StatisticalClassID,p.CreatorID as Patron_Status_Association from Polaris.PatronRegistration pr with (nolock) inner join Polaris.Patrons p with (nolock) on (pr.PatronID = p.PatronID) inner join Polaris.PatronCodes pc with (nolock) on (p.PatronCodeID = pc.PatronCodeID) left join Polaris.PatronStatClassCodes sc with (nolock) on (pr.StatisticalClassID = sc.StatisticalClassID) where p.OrganizationID in (6,3) " /&gt;  
&lt;input type="hidden" name="ReportMetaColumns" id="ReportMetaColumns" value="PatronBarcodePatronCodeDescrPatronIDStatDescrStatisticalClassIDPatron_Status_Association" /&gt;  
&lt;input type="hidden" name="RSSQL" id="RSSQL" value=" from Polaris.PatronRegistration pr with (nolock) inner join Polaris.Patrons p with (nolock) on (pr.PatronID = p.PatronID) inner join Polaris.PatronCodes pc with (nolock) on (p.PatronCodeID = pc.PatronCodeID) left join Polaris.PatronStatClassCodes sc with (nolock) on (pr.StatisticalClassID = sc.StatisticalClassID) where p.OrganizationID in (6,3) " /&gt;  
&lt;input type="hidden" name="ReportTypeID" id="ReportTypeID" value="1" /&gt;

answered 3 years ago by <img width="18" height="18" src="../_resources/default-avatar_f08e683f44e1473bb2c5b0de4d9fa3c6.jpg"/> thunter@desototexas.gov

- <a id="acceptAnswer_1014"></a>[Accept as answer](#)

- [Patronstatusassociation.txt](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/867?fileName=867-1014_Patronstatusassociation.txt "Patronstatusassociation.txt")

<a id="commentAns_1014"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [I'm looking for a SQL report that will tell how long items are in held status.](https://iii.rightanswers.com/portal/controller/view/discussion/495?)
- [SQL for changes to a patron record](https://iii.rightanswers.com/portal/controller/view/discussion/840?)
- [Last modifier of patron record](https://iii.rightanswers.com/portal/controller/view/discussion/1048?)
- [I need to pull OCLC numbers from records with a deleted status to batch update our OCLC holdings. I'm able to get a report with: Bib record ID, Title, and Bib record status, but the column for MARC OCLC number is blank. Why doesn't it show up?](https://iii.rightanswers.com/portal/controller/view/discussion/829?)
- [Simply reports patron services](https://iii.rightanswers.com/portal/controller/view/discussion/355?)

### Related Solutions

- [How do I create or edit patron associations](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930918254492)
- [4.1 Patron Services - Patron Associations](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160908163147796)
- [Unable to merge patrons due to collection block but there is no block on either patron record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930705901830)
- [Error: GetPatronPreCirc() failed](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170922112726384)
- [Patron information display in Patron Status and Checkout windows](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930618652354)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)