Finding new and replaced bibs in Polaris between certain dates - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Finding new and replaced bibs in Polaris between certain dates

0

28 views

I had a SQL to find newly cataloged and/or replaced bibliographic records to send to a vendor for batch processing. Now that Polaris retains the bib control number (which I love), my SQL does not retrieve the replaced records. This is what I use, but it's not picking up everything. Any suggestions? 

select br.BibliographicRecordID from polaris.bibliographicrecords br (nolock)
where br.creatorid in (316,324,400,488,518)
and br.ImportedDate between '1-1-20' and '2-1-20'
and br.BibliographicRecordID in
(select AssociatedBibRecordID
from polaris.CircItemRecords cir (nolock)
where cir.AssignedCollectionID not in (117, 118))
and br.BibliographicRecordID not in
(SELECT br1.bibliographicrecordid
from polaris.bibliographicRecords br1 (NOLOCK)
join polaris.bibliographicTags BT (NOLOCK)
ON BR.BibliographicrecordID = BT.BibliographicrecordID
join polaris.bibliographicSubfields BSu (NOLOCK)
ON (BT.bibliographicTagid = BSu.BibliographicTagID)
and BT.TagNumber = 996 and BSu.Subfield like '%a%'and BSu.Data like '%MARCV%')

asked 10 months ago  by <img width="18" height="18" src="../../_resources/44aad21740fa4b3ca3a7a68cac598f7c.jpg"/>  staal@hcplonline.org

[sql bibliographic](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=393#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL for Bibs lacking local call #](https://iii.rightanswers.com/portal/controller/view/discussion/605?)
- [Formatting date information in volume field to match requests](https://iii.rightanswers.com/portal/controller/view/discussion/36?)
- [Finding duplicate 035s, 050 etc.](https://iii.rightanswers.com/portal/controller/view/discussion/500?)
- [Polaris OCLC Holdings](https://iii.rightanswers.com/portal/controller/view/discussion/410?)
- [Can you change a tag in a bib record in Leap?](https://iii.rightanswers.com/portal/controller/view/discussion/1055?)

### Related Solutions

- [What is the difference between Material Type and Type of Material?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930618950203)
- [Sort by year searching](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930814636875)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Default pricing](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930543557097)
- [How can Authority Control records be merged?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930673534728)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)