Report for patrons with items out. If possible, sort by location?   - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Report for patrons with items out. If possible, sort by location?

0

26 views

Hello, 

Looking for a SQL search to find patrons with items checked out.  If possible sort by location. ( How do I find the Organization ID? ) 

Or is there another report to run.  other than an overdue report. 

Big Thank You in Advance!!

Treel

treel@mypccl.org

asked 1 year ago  by <img width="18" height="18" src="../../_resources/e419860d2d034570ab79f54ee1f34109.jpg"/>  treel@mypccl.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_940"></a>[](#)

0

<a id="downVoteAns_940"></a>[](#)

If all you want is a list of barcodes, names, and what branch they're with, this should work:

SELECT DISTINCT p.Barcode, pr.NameLast, pr.NameFirst, o.Abbreviation
FROM ItemCheckouts icko with (NOLOCK)
JOIN PatronRegistration pr with (NOLOCK)
ON icko.PatronID = pr.patronID
JOIN Patrons p with (NOLOCK)
ON p.PatronID = icko.patronID
JOIN Organizations o with (NOLOCK)
ON p.OrganizationID = o.OrganizationID
ORDER BY o.Abbreviation

But note that it includes only current checkouts of items in the system, and that those sometimes (but not always) include downloadables (other systems may or may not be integrated with Polaris, and even if they are there is some latency in picking up checkouts and/or returns, which can range anywhere from 15 minutes \[ideally\] to several days \[if something has gone wrong somewhere\]).

There's usually a way to exclude downloadables, but it's a bit more complicated.

answered 1 year ago  by <img width="18" height="18" src="../../_resources/e419860d2d034570ab79f54ee1f34109.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_940"></a>[Accept as answer](#)

<a id="commentAns_940"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [Report of Lost and Paid Items](https://iii.rightanswers.com/portal/controller/view/discussion/331?)
- [Simply reports patron services](https://iii.rightanswers.com/portal/controller/view/discussion/355?)

### Related Solutions

- [Item Circulation Statistics Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930404036798)
- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [How do I set a default location for the bulk change report?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930976945995)
- [Item Circulation by Statistical Code](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930592909894)
- [Patron registration counts differ](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930359958800)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)