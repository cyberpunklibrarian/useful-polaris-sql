[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# How to circulate Museum Passes?

0

70 views

Besides Quipu's ePASS product, is anyone using any other automated cloud based solution to circulate Museum Passes? Currently we have item records for all of our Museum Passes, but as this project is continuing to expand, adding more records to the database doesn't seem like the best solution.

&nbsp;

Thanks!!!

Amanda

&nbsp;

**Amanda Riley**  |  **Systems Librarian**  |  **Main Library**  

Phone: 504.596.2621  |  219 Loyola Avenue, New Orleans, LA 70112

ariley@nolalibrary.org

asked 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_e247c15a35e54a5cabe228884d344ec3.jpg"/> ariley@neworleanspubliclibrary.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 5 years ago

<a id="upVoteAns_481"></a>[](#)

0

<a id="downVoteAns_481"></a>[](#)

We also don't use a 'cloud based solution.' We strictly use Polaris. Each branch keeps a laminated, barcoded pass at the circ desk. The patron checks it out (7 day loan period), gets the receipt which we staple to a flyer and takes that to the institution for entry. Circ staff monitor the items, check them in on the due date and it's available for the next patron. 

Currently we only have one partner - a children's museum, but our new program coordinator has expanded and we're preparing to go live with 7 more - the symphony, a boat tour company, an arts center, and more museums. 

We're also trying to come up with something to display to the public. How does that work for you as far as putting in a display case? Can you give me specifics on that? I'm going to display a promotion on our Bibliotheca self check machines and marketing is working on an extensive marketing campaign, but a display case is something we've tossed around but not sure how to implement. 

Any info would be appreciated.

Thanks,

Cecilia Smiley

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_e247c15a35e54a5cabe228884d344ec3.jpg"/> csmiley@leegov.com

- <a id="acceptAnswer_481"></a>[Accept as answer](#)

<a id="commentAns_481"></a>[Add a Comment](#)

<a id="upVoteAns_480"></a>[](#)

0

<a id="downVoteAns_480"></a>[](#)

We check out the pass but keep it. The customer is given a receipt which has the expiration date on it. They show the receipt to the ticketing office to gain entry. At the end of the checkout period, staff manually check the pass back in. This way allows us to control how many passes per person can go out and how many uses allowed by the museum is under control.

After the pass is checked in, it is put back into the display case for reuse.

I have a SQL report to help our staff to use to track passes due back in.

answered 5 years ago by ![carl.ratz@phoenix.gov](https://iii.rightanswers.com/portal/app/images/custom/avatar_carl.ratz@phoenix.gov20170808200600.png) carl.ratz@phoenix.gov

- <a id="acceptAnswer_480"></a>[Accept as answer](#)

<a id="commentAns_480"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Automatically Check In Items](https://iii.rightanswers.com/portal/controller/view/discussion/1064?)
- [How can we keep patrons from abusing hotspot circulation?](https://iii.rightanswers.com/portal/controller/view/discussion/699?)
- [I need help creating a SQL for circulation data that includes a lot of details.](https://iii.rightanswers.com/portal/controller/view/discussion/550?)
- [SQL query for circulation by call number range and date range](https://iii.rightanswers.com/portal/controller/view/discussion/190?)
- [General Staff guides for Polaris 6.3](https://iii.rightanswers.com/portal/controller/view/discussion/609?)

### Related Solutions

- [Circulation statistics and deleted records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930736904760)
- [Adding a Loan Period](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930906442286)
- [Deleting item records with fines](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930356108467)
- [Library contact information on eReceipt](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930457677951)
- [How do I define an item as non-circulating?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930906100414)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)