Need an SQL for bib records with no holdable items - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Need an SQL for bib records with no holdable items

0

54 views

Hello All -

Long time reader, first time poster, novice SQL'er

I'm trying to create an SQL query that will get the BibRecordID of all bib records of certain material types (1,11,12,13) that have no holdable items associated with them. A holdable item is anything that is not lost, missing, withdrawn, or weeded (7,10,11,12). Once I have the bib#'s in a recordset I will toggle the "Display in PAC" flag to "off" via bulk change. Why show things in the PAC that patrons can't request right?

This is what I've come up with so far...

SELECT DISTINCT Bibs.BibliographicRecordID, Bibs.BrowseTitle, Bibs.BrowseAuthor, Bibs.DisplayInPAC
FROM Polaris.Polaris.BibliographicRecords Bibs (nolock)
WHERE Bibs.DisplayInPAC = 1 AND Bibs.BibliographicRecordID IN (
SELECT DISTINCT AssociatedBibRecordID
FROM Polaris.Polaris.CircItemRecords Items (nolock)
WHERE Items.ItemStatusID IN (7,10,11,12) AND
Items.MaterialTypeID IN (1,11,12,13)
)
ORDER BY BrowseAuthor, BrowseTitle ASC

This isn't perfect though. For example, we own 2 copies of the DVD "101 Dalmations". One is lost (item status = 7, material type = 13) but the second copy is in (item status = 1). I would want to exclude this record in my result because it can still appear in the PAC and has at least one holdable item.

I would appreciate a little advice or a pointer. Thanks in advance.

asked 1 year ago  by ![senig@chapinlibrary.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_senig@chapinlibrary.org20190710105855.png)  senig@chapinlibrary.org

[bibs](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=312#t=commResults) [pac](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=58#t=commResults) [reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=32#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_934"></a>[](#)

1

<a id="downVoteAns_934"></a>[](#)

Answer Accepted

Hi. I think you are on the right track. The easiest way I've found with this sort of situation is to add a sub select with a NOT IN condition.

Here's what your query would look like with this addition:

SELECT DISTINCT Bibs.BibliographicRecordID, Bibs.BrowseTitle, Bibs.BrowseAuthor, Bibs.DisplayInPAC
FROM Polaris.Polaris.BibliographicRecords Bibs (nolock)
WHERE Bibs.DisplayInPAC = 1 AND Bibs.BibliographicRecordID IN (
 SELECT DISTINCT AssociatedBibRecordID
 FROM Polaris.Polaris.CircItemRecords Items (nolock)
 WHERE Items.ItemStatusID IN (7,10,11,12) AND
 Items.MaterialTypeID IN (1,11,12,13)
 )
 AND Bibs.BibliographicRecordID NOT IN
 (
 SELECT DISTINCT AssociatedBibRecordID
 FROM Polaris.Polaris.CircItemRecords Items (nolock)
 WHERE Items.ItemStatusID NOT IN (7,10,11,12) AND
 Items.MaterialTypeID IN (1,11,12,13)
 )
ORDER BY BrowseAuthor, BrowseTitle ASC

If you don't want two sub selects, you can probably replace the first one with a join to CircItemRecords. Also, you may not need to have the MaterialTypeID condition in the second sub select, but that does cut down on the size of the result of that sub select. If you keep it in boith places and need to change the list of MaterialTypeIDs in one statement, make sure you make the same change in the other area.

Hope this helps,

JT

answered 1 year ago  by <img width="18" height="18" src="../../_resources/7a13d39f7f13426a9718b668b802493c.jpg"/>  jwhitfield@aclib.us

One other thing: Polaris has an overnight job that can be set up to automatically suppress bibs that meet certain conditions (has items, but no "good" or otherwise holdable ones) and unsuppress those that no longer meet them (like a Lost item comes back). It's not perfect, but it does a pretty good job. I double-check it weekly and usually don't have many bibs to adjust.

— jwhitfield@aclib.us 1 year ago

<a id="commentAns_934"></a>[Add a Comment](#)

<a id="upVoteAns_935"></a>[](#)

0

<a id="downVoteAns_935"></a>[](#)

Thanks for the quick reply JT.  I think I understand.  I'll test it out tomorrow...

![cool](../../_resources/040d7db5c77d469d834a21aa05777c91.gif)

answered 1 year ago  by ![senig@chapinlibrary.org](https://iii.rightanswers.com/portal/app/images/custom/avatar_senig@chapinlibrary.org20190710105855.png)  senig@chapinlibrary.org

- <a id="acceptAnswer_935"></a>[Accept as answer](#)

<a id="commentAns_935"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [How would you write a SQL statement for the find tool asking for bib records with 10+ available items (not withdrawn, lost, missing, etc.) for specific collection codes? Thanks for any help you can give!](https://iii.rightanswers.com/portal/controller/view/discussion/490?)
- [SQL for Bib Record Count (not e-materials) and number of bibs added per year](https://iii.rightanswers.com/portal/controller/view/discussion/712?)
- [SQL for Bibs w/no good items but have holds - including item and patron info](https://iii.rightanswers.com/portal/controller/view/discussion/594?)
- [SQL for bibs with holds and no owned items by branches](https://iii.rightanswers.com/portal/controller/view/discussion/821?)
- [SQL for Item records from PO/FY/Branch](https://iii.rightanswers.com/portal/controller/view/discussion/326?)

### Related Solutions

- [Finding bibs with no items attached and putting them into a record set](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161021140649266)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Patron unable to access account: Error on page an error was encountered.](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930923086474)
- [Why can't I link to the holds queue from an item record?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930989583652)
- [Bib Call Number is not automatically copying to Item Records](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930769128110)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)