Average checkout length for a collection and average time between circulations? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Average checkout length for a collection and average time between circulations?

0

45 views

I'm looking for a way to calculate average checkout length for a collection for a specific time period as well time between checkouts 

I.e. Patrons checked out DVDs for an average of 7 days in 2019. The average DVD sat on the shelf for 4 days before being checked out again

Thanks!

asked 1 year ago  by <img width="18" height="18" src="../../_resources/e07943936301486981f073e91fbd9b13.jpg"/>  crosenthal@pwcgov.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_981"></a>[](#)

0

<a id="downVoteAns_981"></a>[](#)

Answer Accepted

Hi Charlie.

In theory, both \_should\_ be possible.

I created a report that uses the ItemRecordHistory table to calculate the average length of checkout for a collection. I'm not sure if it is perfect (in fact, I'm pretty sure that it could be improved) but it may work for your puposes. I will paste the query below, but first I wanted to give some ideas on how you could convert this into a query for the average time on shelf.

This query looks for checkouts in the ItemRecordHistory and populates a temporary table. It then looks for the earliest checkin history action that follows the checkout. There is a line near the end of the query that excludes any checkout/checkin that are less than 180 minutes in length (to avoid checkouts that were mistakes, or where the patron realized that they didn't want the item shortly after checking it out). This time frame can be shortened or omitted.

There is a commented line that would restrict the query to a specific branch. It could also be restricted to a limited number of collections.

To modify this for average time on shelf, you may be able to reverse the ItemRecordHistoryActions, starting with a checkin (11) and then finding the earliest checkout (13) after that. I'm sure that there are other tweaks needed to refine it (such as considering the time frame limit).

A couple of things to consider: If items at your library go on shelf for the first time without being checked in, the query may not work for the first checkout (not sure). The time "on shelf" would include any time that an item was in Missing or Held status, which may or may not make a difference to you.

The results of the query below may extend out to many places past the deimal point. This can be rounded in the output.

I'm sorry that I don't have time to come up with a "solid" query for the time on shelf, but hopefully this will be a good start.

JT

BEGIN
 SET NOCOUNT ON

 
 DECLARE @StartDate date = '01/02/2018'
 DECLARE @EndDate date = '01/08/2018'
 DECLARE @Cutoff integer = 35
 DECLARE @Branch integer = 3
  
 CREATE TABLE #ChkoutLength
 ( ItemRecID int NOT NULL,
  OutHisID int NOT NULL,
  OutHisDate datetime,
  InHisID int NULL,
  CollName varchar (50) NOT NULL
 )

 INSERT INTO #ChkoutLength(ItemRecID, OutHisID, OutHisDate, CollName)
  (Select irh.ItemRecordID, irh.ItemRecordHistoryID as OutHisID,
  irh.TransactionDate as OutHisDate, col.Name as CollName
  from polaris.polaris.ItemRecordHistory irh with (nolock)
  join polaris.polaris.ItemRecords ir (nolock)
  on (irh.ItemRecordID = ir.ItemRecordID)
  join polaris.polaris.collections col (nolock)
  on (ir.AssignedCollectionID = col.CollectionID)
  where
  --irh.OrganizationID in (@branch) and
  irh.TransactionDate >= @StartDate
  and irh.TransactionDate < dateadd(dd,1,(@EndDate))
  and irh.ActionTakenID = 13)

 UPDATE #ChkoutLength
 SET InHisID = (
 Select min(inh.InHisID) from
  (Select irh2.ItemRecordID, irh2.ItemRecordHistoryID as InHisID
  from polaris.polaris.ItemRecordHistory irh2 with (nolock)
  where irh2.actiontakenid = 11
  and irh2.TransactionDate >= @StartDate
  and irh2.TransactionDate <= dateadd(dd,(@Cutoff + 1),@StartDate)
  )inh
 WHERE inh.itemrecordid = #Chkoutlength.ItemRecID
 and Inh.InHisID > #ChkoutLength.OutHisID)
 
 SELECT cko.CollName
 ,AVG((DATEDIFF(mi,cko.OutHisDate,irhi.TransactionDate)*1.0000)/1440) as AVGDaysOut
 from #ChkoutLength cko
 join polaris.polaris.ItemRecordHistory irhi (nolock)
 on (cko.InHisID = irhi.ItemRecordHistoryID)
 WHERE (DATEDIFF(mi,cko.OutHisDate,irhi.TransactionDate)*1.0000) > 180
 Group by cko.CollName
 order by cko.CollName

 DROP TABLE #ChkoutLength
END

answered 1 year ago  by <img width="18" height="18" src="../../_resources/e07943936301486981f073e91fbd9b13.jpg"/>  jwhitfield@aclib.us

This is great JT, thank you!

For my own knowledge, what's the purpose of Cutoff integer?

— crosenthal@pwcgov.org 1 year ago

Hi Charlie.

That line is constructed so that the user can enter a start date, for example '07/01/2020' and then have a paremeter for the ending date that is a number of days (30, 45, 60..or 14, 21...). The +1 sets the ending date to actually be the day after. If the user entered 15 for that parameter, the query would look for activity between midnight of July 1 up to midnight of July 16 (technically through 23:59:59 on July 15, plus 1 second). If I was writing it today, I would probably use "<" instead of "<=" to avoid accidentally catching an extra checkout that happened exactly at midnight.

The query could just as easily be set up to have an ending date that was an actual date. In this case, you would still probably want to use a dateadd and +1 and set it to be something like '' and irh2.TransactionDate < dateadd(dd,1,@EndDate)

Yet another example of when something makes perfect sense to the writer, but not to the reader because the reader didn't write it. ![smile](../../_resources/b0a1fa5b238d4744906cf34d2c10b25d.gif) Hope this helps clarify things. JT

— jwhitfield@aclib.us 1 year ago

<a id="commentAns_981"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Average hold wait times](https://iii.rightanswers.com/portal/controller/view/discussion/845?)
- [Average time things are lost before coming back?](https://iii.rightanswers.com/portal/controller/view/discussion/436?)
- [Checkout length by checkin date](https://iii.rightanswers.com/portal/controller/view/discussion/877?)
- [Circulation by Item Statistical Code & limited by assigned collection?](https://iii.rightanswers.com/portal/controller/view/discussion/801?)
- [Circulation count of deleted items from a collection](https://iii.rightanswers.com/portal/controller/view/discussion/92?)

### Related Solutions

- [Change text on checkout receipts](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930905424507)
- [How are due dates calculated?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930778190992)
- [Workstation is only printing check-out receipts](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930700197420)
- [Training - 5.0 SP3 Borrow by Mail.pptx](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160504131542391)
- [Floating guide](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930254601444)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)