is there a SQL that counts how long an items sits on the hold shelf?  - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=15)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# is there a SQL that counts how long an items sits on the hold shelf?

0

117 views

i'm looking specifically for the time between the transaction status 'Hold becomes held' and 'Hold Satisfied'.

asked 2 years ago  by <img width="18" height="18" src="../../_resources/a681afd42d72421ba5bb4a5e5d7a4958.jpg"/>  seddings@tscpl.org

[holds](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=96#t=commResults) [reports](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=32#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 2 years ago

<a id="upVoteAns_575"></a>[](#)

0

<a id="downVoteAns_575"></a>[](#)

Answer Accepted

it has come to pass that i need to break this report down by collections.  can anyone help with this again?

I want to narrow the scope to our large print collections that transfer to different branches, again measuirng the time between the  transaction status 'Hold becomes held' and 'Hold Satisfied'.

thanks in advance!

answered 2 years ago  by <img width="18" height="18" src="../../_resources/a681afd42d72421ba5bb4a5e5d7a4958.jpg"/>  seddings@tscpl.org

This should work:

SELECT td.numValue AS 'HoldRequestID', MAX(th.TranClientDate) AS 'Held', col.Name
INTO #tmpHeldDate
FROM PolarisTransactions..TransactionHeaders th (NOLOCK)
JOIN PolarisTransactions..TransactionDetails td (NOLOCK)
ON (td.TransactionID = th.TransactionID) AND (td.TransactionSubTypeID = '233') --HoldRequestID
JOIN PolarisTransactions..TransactionDetails tdColl (NOLOCK)
ON (tdColl.TransactionID = th.TransactionID) AND (tdColl.TransactionSubTypeID = '61') --CollectionID
JOIN Polaris.Polaris.Collections col with (NOLOCK)
ON tdColl.numValue = CollectionID
WHERE th.TransactionTypeID = '6006'
AND th.TranClientDate BETWEEN @From AND @Through + ' 11:59:59 pm'
GROUP BY td.numValue, col.Name

SELECT td.numValue AS 'HoldRequestID', th.TranClientDate AS 'Filled'
INTO #tmpFilledDate
FROM PolarisTransactions..TransactionHeaders th (NOLOCK)
JOIN PolarisTransactions..TransactionDetails td (NOLOCK)
ON (td.TransactionID = th.TransactionID) AND (td.TransactionSubTypeID = '233') --HoldRequestID
WHERE th.TransactionTypeID = '6039'
AND th.TranClientDate BETWEEN @From AND @Through + ' 11:59:59 pm'

SELECT datediff(day,thd.Held,tfd.Filled) AS 'Days held', count(datediff(day,thd.Held,tfd.Filled)) AS '# held', thd.Name AS 'Description'
FROM #tmpHeldDate thd with (NOLOCK)
JOIN #tmpFilledDate tfd with (NOLOCK)
ON thd.HoldRequestID = tfd.HoldRequestID
GROUP BY datediff(day,thd.Held,tfd.Filled), thd.Name
ORDER BY thd.Name, datediff(day,thd.Held,tfd.Filled)

DROP TABLE #tmpHeldDate
DROP TABLE #tmpFilledDate

You could also add "*AND col.CollectionID IN (@Collection)*" to the top query (just above "*GROUP BY td.numValue, col.Name*") if you wanted to limit it to particular collections.

Interestingly enough, this query showed me that we had Storyhour items being placed on hold, which isn't supposed to happen.  The Storyhour item I found placed on hold last month could be something which used to be Storyhour and was incompletely switched over to circulating, or it could be a staff override.  So now I'll put together a new subscribed report.  ^__^

— jjack@aclib.us 2 years ago

<a id="commentAns_575"></a>[Add a Comment](#)

<a id="upVoteAns_576"></a>[](#)

0

<a id="downVoteAns_576"></a>[](#)

this is perfect, thank you once again!

answered 2 years ago  by <img width="18" height="18" src="../../_resources/a681afd42d72421ba5bb4a5e5d7a4958.jpg"/>  seddings@tscpl.org

- <a id="acceptAnswer_576"></a>[Accept as answer](#)

<a id="commentAns_576"></a>[Add a Comment](#)

<a id="upVoteAns_475"></a>[](#)

0

<a id="downVoteAns_475"></a>[](#)

thank you so much!

At this point I only want to know the time on the shelf. 

answered 2 years ago  by <img width="18" height="18" src="../../_resources/a681afd42d72421ba5bb4a5e5d7a4958.jpg"/>  seddings@tscpl.org

- <a id="acceptAnswer_475"></a>[Accept as answer](#)

<a id="commentAns_475"></a>[Add a Comment](#)

<a id="upVoteAns_474"></a>[](#)

0

<a id="downVoteAns_474"></a>[](#)

This should work, unless you don't have permission to add and drop temporary tables. (You can pull info from the transaction tables without using temporary tables, but in my experience it has always been *astonishingly *slow.)

```
SELECT td.numValue AS 'HoldRequestID', MAX(th.TranClientDate) AS 'Held' 
INTO #tmpHeldDate 
FROM PolarisTransactions..TransactionHeaders th (NOLOCK) 
JOIN PolarisTransactions..TransactionDetails td (NOLOCK) 
ON (td.TransactionID = th.TransactionID) AND (td.TransactionSubTypeID = '233') --HoldRequestID
WHERE th.TransactionTypeID = '6006' 
AND th.TranClientDate BETWEEN @From AND @Through + ' 11:59:59 pm'
GROUP BY td.numValue
SELECT td.numValue AS 'HoldRequestID', th.TranClientDate AS 'Filled'
INTO #tmpFilledDate 
FROM PolarisTransactions..TransactionHeaders th (NOLOCK) 
JOIN PolarisTransactions..TransactionDetails td (NOLOCK) 
ON (td.TransactionID = th.TransactionID) AND (td.TransactionSubTypeID = '233') --HoldRequestID
WHERE th.TransactionTypeID = '6039' 
AND th.TranClientDate BETWEEN @From AND @Through + ' 11:59:59 pm'
SELECT datediff(day,thd.Held,tfd.Filled) AS 'Days held', count(datediff(day,thd.Held,tfd.Filled)) AS '# held'
FROM #tmpHeldDate thd with (NOLOCK) 
JOIN #tmpFilledDate tfd with (NOLOCK) 
ON thd.HoldRequestID = tfd.HoldRequestID
GROUP BY datediff(day,thd.Held,tfd.Filled)
ORDER BY datediff(day,thd.Held,tfd.Filled)
DROP TABLE #tmpHeldDate 
DROP TABLE #tmpFilledDate
```

We're using the max date in the first query to start the counter from the *last *time an item was held for a hold if it was held then shipped to another branch and then held again (perhaps several times) before being checked out.  If you're more interested in the total time something was unavailable to patrons because it was held, those numbers would be a bit different.

You didn't want any more info than this, though? It's possible to break it down by collection, stat code, etc.

\[edit: removing the bit giving it a padding on the "held" date, since it's most likely unnecessary and counterintuitive\]

answered 2 years ago  by <img width="18" height="18" src="../../_resources/a681afd42d72421ba5bb4a5e5d7a4958.jpg"/>  jjack@aclib.us

- <a id="acceptAnswer_474"></a>[Accept as answer](#)

<a id="commentAns_474"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Are cancelled holds considered holds in holds item count?](https://iii.rightanswers.com/portal/controller/view/discussion/414?)
- [I need a SQL for phatom lost item count attached to a patron's record.](https://iii.rightanswers.com/portal/controller/view/discussion/839?)
- [Is there a way to set a longer held period for one patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/983?)
- [Can we stop patrons from placing holds on items they already have checked out or being Held?](https://iii.rightanswers.com/portal/controller/view/discussion/958?)
- [Is there an SQL to change item level holds to bib level holds?](https://iii.rightanswers.com/portal/controller/view/discussion/407?)

### Related Solutions

- [Retrieving hold information for accidentally deleted hold requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930965728767)
- [Item Held Longer Than Expected](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930398757933)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [How do I reset the year-to-date circulation counts?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930831368342)
- [Long Overdue Item report shows thousands of items](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930725698901)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)