Circ by material type and time frame - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Circ by material type and time frame

0

126 views

Originally posted by candiceoliver@mlc-stl.org on Wednesday, December 21st 12:29:35 EST 2016
Hi, Does anyone have a report that will let me designate the material type and time frame and give me the total circ within that time frame? Circ by material type analysis gives me all the material types and I'm only interested in a few at this point. In SR I was only able to get lifetime or ytd circ sums. If I could have item assigned library on the output, that'd be great too. =) Thoughts? Thanks, Candice Oliver candiceoliver@mlc-stl.org

asked 4 years ago  by <img width="18" height="18" src="../../_resources/1e81acbce1744f2291da8abf22c3855a.jpg"/>  support1@iii.com

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 4 years ago

<a id="upVoteAns_97"></a>[](#)

0

<a id="downVoteAns_97"></a>[](#)

Sorry but my first answer was not correct.

My apologies as it was only built for one item.

Here is something that you can use:

CREATE PROCEDURE \[Polaris\].\[Rpt_JuinorItemCirculation\]
AS
DECLARE @startdate datetime = DATEADD(yy,-1,DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0))
DECLARE @enddate datetime = DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)
select mt.Description,
       br.BrowseTitle,
       br.BrowseAuthor,
       COUNT( Distinct th.transactionid) as TotalCirc,
      
from PolarisTransactions.Polaris.TransactionHeaders th (nolock)
    INNER JOIN PolarisTransactions.Polaris.TransactionDetails td (nolock)
       on th.TransactionID = td.TransactionID and td.TransactionSubTypeID = **4 -- mat type**
    INNER JOIN Polaris.Polaris.MaterialTypes mt (nolock)
        on td.numValue = mt.MaterialTypeID
    LEFT JOIN Polaris.Polaris.Organizations o (nolock)
        on o.OrganizationID = th.OrganizationID -- transacting branch
    INNER JOIN PolarisTransactions.Polaris.TransactionDetails td2 (nolock)
        on th.TransactionID = td2.TransactionID and td2.TransactionSubTypeID = 61 --collection
    LEFT JOIN Polaris.Polaris.collections c (nolock)
        on c.CollectionID = td2.numValue
    INNER JOIN PolarisTransactions.Polaris.TransactionDetails td3
        on th.TransactionID = td3.TransactionID and td3.TransactionSubTypeID = 38 -- item
    LEFT JOIN Polaris.Polaris.CircItemRecords cir (nolock)
        on cir.ItemRecordID = td3.numValue
    LEFT JOIN Polaris.Polaris.BibliographicRecords br (nolock) -- title and author
        on br.BibliographicRecordID = cir.AssociatedBibRecordID
where TranClientDate between @startdate and @enddate
      and TransactionTypeID = 6001
      ***and c.CollectionID = 76, and mt.MaterialTypeID = ? (What ever you need it to be)
***

      group by o.Name, c.Name, mt.Description, br.BrowseTitle, br.BrowseAuthor, th.TransactionTypeID
Order by Category asc, TotalCirc asc

I have placed one sections in bold.  You can chnage the collection ID, and as you will need to ad the Materila Type as it is already referenced in the above query.  Sorry but I am out of time or I would add in the MAterial type for you.

Either way generate the procedue, and then build a report. 

I hope this helps.

Have a good day.

Vincent Kruggel

vkruggel@orl.bc.ca

answered 4 years ago  by <img width="18" height="18" src="../../_resources/1e81acbce1744f2291da8abf22c3855a.jpg"/>  vkruggel@orl.bc.ca

- <a id="acceptAnswer_97"></a>[Accept as answer](#)

<a id="commentAns_97"></a>[Add a Comment](#)

<a id="upVoteAns_96"></a>[](#)

0

<a id="downVoteAns_96"></a>[](#)

The Statistical reports are taking inforamtion from the Transacation Database, though be carefule with running these duing prime time as they can bog down your server.

A simple query would be:

**SELECT     O.Name**
**        ,Count(O.Name) AS Checkouts**
**  FROM PolarisTransactions.Polaris.TransactionDetails AS TD**
**    Inner Join PolarisTransactions.Polaris.TransactionHeaders AS TH**
**        ON TD.TransactionID = TH.TransactionID**
**    Inner Join PolarisTransactions.Polaris.TransactionSubTypes AS TS**
**        ON TS.TransactionSubTypeID = TD.TransactionSubTypeID**
**    Inner Join PolarisTransactions.Polaris.TransactionTypes AS TT**
**        ON TT.TransactionTypeID = TH.TransactionTypeID**
**    Inner Join Polaris.Polaris.Organizations AS O**
**        ON TH.OrganizationID = O.OrganizationID**
**  WHERE th.TransactionTypeID = 6001 and TD.numValue in**
**    (SELECT CI.ItemRecordID**
**        FROM Polaris.Polaris.CircItemRecords AS CI**
**        WHERE AssociatedBibRecordID = 267909) and TransactionDate between @BeginDate and @EndDate**
**  GROUP BY O.Name**
**  ORDER BY O.Name ASC**

Dump the following into Report Builder or Visual Studio.  If you do not want to go that far then just enter the dates you want into the query and you are done.

Vincent Kruggel

vkruggel@orl.bc.ca

answered 4 years ago  by <img width="18" height="18" src="../../_resources/1e81acbce1744f2291da8abf22c3855a.jpg"/>  vkruggel@orl.bc.ca

- <a id="acceptAnswer_96"></a>[Accept as answer](#)

<a id="commentAns_96"></a>[Add a Comment](#)

<a id="upVoteAns_60"></a>[](#)

0

<a id="downVoteAns_60"></a>[](#)

I second that!

Response by rdye@krl.org on February 24th, 2017 at 3:11 pm

I'm looking for a similar report. I would like to be able to report on an item's yearly circ. Right now I can get YTD and Previous Year, but can't get circ for years before that.

answered 4 years ago  by <img width="18" height="18" src="../../_resources/1e81acbce1744f2291da8abf22c3855a.jpg"/>  support1@iii.com

- <a id="acceptAnswer_60"></a>[Accept as answer](#)

<a id="commentAns_60"></a>[Add a Comment](#)

<a id="upVoteAns_59"></a>[](#)

0

<a id="downVoteAns_59"></a>[](#)

SimplyReports subcategories

Response by wosborn@clcohio.org on March 1st, 2017 at 2:11 pm

You mentioned you tried SimplyReports. If you run a Item statistical report and use the Available subcategories "Item Material Type" that should allow you to use a date range and the Material Type filter in the Item statistical report filters section.

answered 4 years ago  by <img width="18" height="18" src="../../_resources/1e81acbce1744f2291da8abf22c3855a.jpg"/>  support1@iii.com

- <a id="acceptAnswer_59"></a>[Accept as answer](#)

<a id="commentAns_59"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Report to find Items added by Material Type in a certain year?](https://iii.rightanswers.com/portal/controller/view/discussion/430?)
- [Searching for Items that have not circ'd in 2 years ex. DVD's](https://iii.rightanswers.com/portal/controller/view/discussion/1053?)
- [Average hold wait times](https://iii.rightanswers.com/portal/controller/view/discussion/845?)
- [SQL for circ by author](https://iii.rightanswers.com/portal/controller/view/discussion/952?)
- [Average time things are lost before coming back?](https://iii.rightanswers.com/portal/controller/view/discussion/436?)

### Related Solutions

- [Items didn't float upon check-in at receiving branch](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930277940804)
- [How does the Shelving status work?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930389262465)
- [Floating guide](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930254601444)
- [Checklist: Adding a New Material Type](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181212124422933)
- [Type of Materials in Polaris 5.1](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930289501912)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)