[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [System Administration](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=20)

# SQL for Range of Closed Dates

0

114 views

Hey Guys!  
<br/>I had my first library closing today for a couple weeks. I came up with SQL to add a range of closed dates for a branch (ex. 03/13/2020-03/31/2020). If you have multiple branches, you will need to iterate for each branch (or modify the loop to account for that). There is the option to set the OrgID directly or via name search. Hope this helps someone!

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_a19a7e7c0f284ff39bcb43184f9ec853.jpg"/> trevor.diamond@mainlib.org

- [rangecloseddates.txt](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/716?fileName=716-rangecloseddates.txt "rangecloseddates.txt")

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 4 years ago

<a id="upVoteAns_879"></a>[](#)

0

<a id="downVoteAns_879"></a>[](#)

Thanks Trevor.  So very timely as i was about to try and figure out how to do this this afternoon.

Rex

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_a19a7e7c0f284ff39bcb43184f9ec853.jpg"/> rhelwig@flls.org

- <a id="acceptAnswer_879"></a>[Accept as answer](#)

<a id="commentAns_879"></a>[Add a Comment](#)

<a id="upVoteAns_878"></a>[](#)

0

<a id="downVoteAns_878"></a>[](#)

Thank you for sharing Trevor! We had our first five branches close today as well. 

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_a19a7e7c0f284ff39bcb43184f9ec853.jpg"/> mcamacho@denverlibrary.org

- <a id="acceptAnswer_878"></a>[Accept as answer](#)

<a id="commentAns_878"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Is it possible to add multiple days to the Dates Closed table without adding them individually?](https://iii.rightanswers.com/portal/controller/view/discussion/761?)
- [SQL for waiving certain fines](https://iii.rightanswers.com/portal/controller/view/discussion/192?)
- [Is there a way to set Polaris to change expiration dates and address checks for new cards based on the patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/1213?)
- [Deleting Material Types using SQL](https://iii.rightanswers.com/portal/controller/view/discussion/323?)
- [Creating Jobs in SQL Agent for an SSIS package](https://iii.rightanswers.com/portal/controller/view/discussion/403?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Bulk change item due dates](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930588598835)
- [You Saved Message - What is the date range for YTD savings?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930616286891)
- [Email summary report showing strange date ranges](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930301404239)
- [What is the patron age update step in the patron processing job?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930331250831)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)