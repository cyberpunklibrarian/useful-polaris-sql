[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Does anyone have an SQL report for a specific tag in Authority records?

0

72 views

I need a list of all Authority records that have a 680 tag.

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_aa4bdf955ba342218e08a6ab7658170f.jpg"/> eroberts@cityofterrell.org

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 4 years ago

<a id="upVoteAns_747"></a>[](#)

0

<a id="downVoteAns_747"></a>[](#)

Works like a charm. Thank you so much!

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_aa4bdf955ba342218e08a6ab7658170f.jpg"/> eroberts@cityofterrell.org

- <a id="acceptAnswer_747"></a>[Accept as answer](#)

<a id="commentAns_747"></a>[Add a Comment](#)

<a id="upVoteAns_746"></a>[](#)

0

<a id="downVoteAns_746"></a>[](#)

Here's a search you can use in an Authority Record Find Tool:

&nbsp;

Select distinct AR.AuthorityRecordID  
From Polaris.AuthorityRecords AR (nolock)  
JOIN Polaris.AuthorityTags AT (nolock)  
ON AR.AuthorityRecordID = AT.AuthorityRecordID  
WHERE AT.TagNumber = '680'

&nbsp;

&nbsp;

Let me know if you want to make any changes to it or need anything else.

&nbsp;

Thanks

&nbsp;

&nbsp;

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_aa4bdf955ba342218e08a6ab7658170f.jpg"/> mhammermeister@pinnaclelibraries.org

- <a id="acceptAnswer_746"></a>[Accept as answer](#)

<a id="commentAns_746"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [SQL or Simply Report Active Patrons](https://iii.rightanswers.com/portal/controller/view/discussion/1164?)
- [How to store date that report was last run?](https://iii.rightanswers.com/portal/controller/view/discussion/1085?)
- [SQL - looking for a report on how many fines we have for a specific collection code and how many were waived.](https://iii.rightanswers.com/portal/controller/view/discussion/673?)

### Related Solutions

- [Policy: Polaris Custom Report or Job Requests](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=240520101105277)
- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [IUG 2018: Best Practices in Using Headings Reports](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180517121307708)
- [Unable to Use Report Builder (in SQL Server Reporting Services)](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930942443296)
- [Question on statistical report terminology](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930553369163)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)