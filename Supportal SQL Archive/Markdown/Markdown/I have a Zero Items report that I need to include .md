I have a Zero Items report that I need to include items that we have as Zero Items - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# I have a Zero Items report that I need to include items that we have as Zero Items

0

62 views

Hello:

Let me explain.  I have a Zero Items query that I will be using to find all items that we have no actuall items but requests for.  The problem is though that I need to include items with Item status of 7, 11,10 and 16 as zero items also.  My report was simple for finding items that had Zero items for I just needed to find Bibrecords with no items that had requests.  Now though I need to include any record that has only these above status's as a zero item.  Therefore if I have two items with a 7 (Lost) then it would be considered a Zero item.  I have dumped the tables into temporary tables but have not found a way to add the results from one query to the other query.  It is something that I have never done.

Bellow is my query though very butchered.  Please do not laugh..  I have finally just drove back to the basics but need a clean set of eyes to look at this.  Right now the query just finds Zero Items.

SELECT BR.BrowseTitle,
       BR.BrowseAuthor,
       SHR.BibliographicRecordID,
       BS.data,
       CASE WHEN T.Description like 'unknown%' THEN ''
            ELSE T.Description
            END as TargetAudience,
       COUNT (distinct SHR.SysHoldRequestID) as Requests
       --COUNT (distinct CIR.itemrecordid) as LinkedItems
FROM Polaris.Polaris.SysHoldRequests SHR (NOLOCK)
    INNER JOIN Polaris.Polaris.BibliographicRecords BR (NOLOCK)
        ON BR.BibliographicRecordID = SHR.BibliographicRecordID
    LEFT JOIN Polaris.Polaris.CircItemRecords CIR (NOLOCK)
        ON CIR.AssociatedBibRecordID = SHR.BibliographicRecordID
    LEFT JOIN Polaris.Polaris.BibliographicTags BT (NOLOCK)
        ON BT.BibliographicRecordID = BR.BibliographicRecordID
            AND BT.TagNumber = 245
    LEFT JOIN Polaris.Polaris.BibliographicSubfields BS (NOLOCK)
        ON BS.BibliographicTagID = BT.BibliographicTagID
            AND BS.Subfield = 'h'
    LEFT JOIN Polaris.Polaris.TargetAudiences T (NOLOCK)
        ON T.code = BR.marctargetaudience
    --INNER JOIN Polaris.Polaris.ItemStatuses IT (NOLOCK)
        --ON IT.ItemStatusID = CIR.ItemStatusID
    --INNER JOIN Polaris.Polaris.RecordStatuses RST (NOLOCK)
        --ON RST.RecordStatusID = BR.RecordStatusID
WHERE SysHoldStatusID in (1,3,4)
    AND BR.PrimaryMARCTOMID not in (6,28,41)
    AND BR.RecordStatusID not in (4)
 GROUP BY BR.BrowseTitle,
         BR.BrowseAuthor,
         SHR.BibliographicRecordID,
         BS.Data,
         T.Description
HAVING COUNT (distinct cir.itemrecordid) between 0 and 0
ORDER BY T.Description,
         BS.Data,
         BR.BrowseTitle,
         BR.BrowseAuthor

asked 3 years ago  by <img width="18" height="18" src="../../_resources/a9292dba926340468eb5044b4d15a5e2.jpg"/>  vkruggel@orl.bc.ca

[query](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=90#t=commResults) [report](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=91#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults) [zero items](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=92#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 11 Replies   last reply 3 years ago

<a id="upVoteAns_235"></a>[](#)

0

<a id="downVoteAns_235"></a>[](#)

Answer Accepted

Hm I had added a bad item to my test bib to test with and then didn't do any more checking with a bib with no items. The following query should include both bibs with zero items and bibs with invalid items.

;with bibs as (
select distinct shr.BibliographicRecordID
from polaris.polaris.SysHoldRequests shr
left join polaris.polaris.CircItemRecords cir
on cir.AssociatedBibRecordID = shr.BibliographicRecordID
where shr.SysHoldStatusID in (1,3,4)
group by shr.SysHoldRequestID, shr.BibliographicRecordID
having count(case when coalesce(cir.ItemStatusID,7) in (7,10,11,16) then null else 1 end) = 0
)

select distinct br.BrowseTitle
,br.BrowseAuthor
,br.BibliographicRecordID
,bs.Data
,case when (left(ta.Description, 7) = 'unknown' or ta.Description is null) then '' else ta.Description end \[TargetAudience\]
,count(shr.SysHoldRequestID) over (partition by br.BibliographicRecordID) \[RequestCount\]
,(select count(*) from Polaris.polaris.CircItemRecords where AssociatedBibRecordID = br.BibliographicRecordID) \[ItemCount\]
from polaris.polaris.SysHoldRequests shr
join Polaris.polaris.BibliographicRecords br
on br.BibliographicRecordID = shr.BibliographicRecordID
left join polaris.polaris.BibliographicTags bt
on bt.BibliographicRecordID = shr.BibliographicRecordID and bt.TagNumber = 245
left join polaris.polaris.BibliographicSubfields bs
on bs.BibliographicTagID = bt.BibliographicTagID and bs.Subfield = 'h'
left join polaris.Polaris.TargetAudiences ta
on ta.Code = br.MARCTargetAudience
where br.BibliographicRecordID in ( select * from bibs ) and br.PrimaryMARCTOMID not in (6,28,41) and br.RecordStatusID not in (4) and shr.SysHoldStatusID in (1,3,4)
order by \[targetaudience\], bs.Data, br.BrowseTitle, br.BrowseAuthor

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a9292dba926340468eb5044b4d15a5e2.jpg"/>  mfields@clcohio.org

<a id="commentAns_235"></a>[Add a Comment](#)

<a id="upVoteAns_240"></a>[](#)

0

<a id="downVoteAns_240"></a>[](#)

Why do we need two queries for this total? I have a report based on this information, so if I'm doing wrong, I need to correct it!  By excluding bibs with any good items, you should be catching both 1) bibs with no items at all and 2) bibs with only "bad" items.  Here's the original version of Vincent's query, modified to use my exclusion principal (which negates the need for the HAVING clause).   Vincent, can you run it on your system and let me know if it returns drastically substandard results? We don't actually allow holds on bibs with no items, so while my logic seems sound to me, I can't readily test it. Since we can't have the percent sign, I replaced it with PERCENTSIGN in the query. I also attached it as a file for easy download/running if you like. 

SELECT br.BrowseTitle, br.BrowseAuthor, br.BibliographicRecordID, bs.Data,
CASE
WHEN ta.Description IS NULL THEN ''
WHEN ta.Description LIKE 'unknownPERCENTSIGN' THEN ''
ELSE ta.Description END AS \[TargetAudience\],
COUNT(DISTINCT(shr.SysHoldRequestID)) AS \[Requests\]
FROM Polaris.Polaris.BibliographicRecords AS \[br\] WITH (NOLOCK)
LEFT OUTER JOIN (SELECT DISTINCT(cir.AssociatedBibRecordID)
FROM Polaris.Polaris.CircItemRecords AS \[cir\] WITH (NOLOCK)
WHERE cir.ItemStatusID NOT IN ('7','11','10','16')) AS \[gooditems\]
ON br.BibliographicRecordID = gooditems.AssociatedBibRecordID
INNER JOIN Polaris.Polaris.SysHoldRequests AS \[shr\] WITH (NOLOCK)
ON br.BibliographicRecordID = shr.BibliographicRecordID
LEFT OUTER JOIN Polaris.Polaris.BibliographicTags AS \[bt\] WITH (NOLOCK)
on br.BibliographicRecordID = bt.BibliographicRecordID AND bt.TagNumber = '245'
LEFT OUTER JOIN Polaris.Polaris.BibliographicSubfields AS \[bs\] WITH (NOLOCK)
ON bt.BibliographicTagID = bs.BibliographicTagID AND bs.Subfield = 'h'
LEFT OUTER JOIN Polaris.Polaris.TargetAudiences AS \[ta\] WITH (NOLOCK)
ON br.MARCTargetAudience = ta.Code
WHERE gooditems.AssociatedBibRecordID IS NULL
AND shr.SysHoldStatusID IN ('1','3','4')
AND br.PrimaryMARCTOMID NOT IN ('6','28','41')
AND br.RecordStatusID <> '4'
GROUP BY br.BrowseTitle, br.BrowseAuthor, br.BibliographicRecordID, bs.Data,
CASE
WHEN ta.Description IS NULL THEN ''
WHEN ta.Description LIKE 'unknownPERCENTSIGN' THEN ''
ELSE ta.Description END
ORDER BY \[TargetAudience\], bs.Data, br.BrowseTitle, br.BrowseAuthor

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a9292dba926340468eb5044b4d15a5e2.jpg"/>  trevor.diamond@mainlib.org

- <a id="acceptAnswer_240"></a>[Accept as answer](#)

- [exclusionprincipaltest.txt](https://iii.rightanswers.com/portal/controller/viewAttachment/discussion/257?fileName=257-240_exclusionprincipaltest.txt "exclusionprincipaltest.txt")

<a id="commentAns_240"></a>[Add a Comment](#)

<a id="upVoteAns_239"></a>[](#)

0

<a id="downVoteAns_239"></a>[](#)

Just looked at the Coalesce statement in the query.  That is what I was missing.  Other things also, but that was a big one.  Makes for a faster and easier query to execute.

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a9292dba926340468eb5044b4d15a5e2.jpg"/>  vkruggel@orl.bc.ca

- <a id="acceptAnswer_239"></a>[Accept as answer](#)

<a id="commentAns_239"></a>[Add a Comment](#)

<a id="upVoteAns_238"></a>[](#)

0

<a id="downVoteAns_238"></a>[](#)

I had to test it.

Both queries come up withthe same results.  Yours just smokes mine as far as time to execute.

Thanks again.

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a9292dba926340468eb5044b4d15a5e2.jpg"/>  vkruggel@orl.bc.ca

- <a id="acceptAnswer_238"></a>[Accept as answer](#)

<a id="commentAns_238"></a>[Add a Comment](#)

<a id="upVoteAns_237"></a>[](#)

0

<a id="downVoteAns_237"></a>[](#)

Hello Mike:

I just saw your reply.  It is a bit more elegant than mine.

I will need to experiment with it next week.  Time to go home.

What are you still doing at work?

Thanks for everything.

Vincent

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a9292dba926340468eb5044b4d15a5e2.jpg"/>  vkruggel@orl.bc.ca

- <a id="acceptAnswer_237"></a>[Accept as answer](#)

<a id="commentAns_237"></a>[Add a Comment](#)

<a id="upVoteAns_236"></a>[](#)

0

<a id="downVoteAns_236"></a>[](#)

Hello:

So I used my brain for a minute and combined both queries with a union statement.  It may not be pretty right now but I think I have it together.  I need to optimize the result but it is a working result.

;with bibs as (
select distinct shr.BibliographicRecordID
from polaris.polaris.SysHoldRequests shr
left join polaris.polaris.CircItemRecords cir
on cir.AssociatedBibRecordID = shr.BibliographicRecordID
where shr.SysHoldStatusID in (1,3,4)
group by shr.SysHoldRequestID, shr.BibliographicRecordID
having count(case when cir.ItemStatusID in (7,10,11,16) then null else 1 end) = 0
)
select distinct br.BrowseTitle
,br.BrowseAuthor
,br.BibliographicRecordID
,bs.Data
,case when (left(ta.Description, 7) = 'unknown' or ta.Description is null) then '' else ta.Description end \[TargetAudience\]
,count(shr.SysHoldRequestID) over (partition by br.BibliographicRecordID) \[RequestCount\]
,(select count(*) from Polaris.polaris.CircItemRecords where AssociatedBibRecordID = br.BibliographicRecordID) \[ItemCount\]
from polaris.polaris.SysHoldRequests shr
join Polaris.polaris.BibliographicRecords br
on br.BibliographicRecordID = shr.BibliographicRecordID
left join polaris.polaris.BibliographicTags bt
on bt.BibliographicRecordID = shr.BibliographicRecordID and bt.TagNumber = 245
left join polaris.polaris.BibliographicSubfields bs
on bs.BibliographicTagID = bt.BibliographicTagID and bs.Subfield = 'h'
left join polaris.Polaris.TargetAudiences ta
on ta.Code = br.MARCTargetAudience
where br.BibliographicRecordID in ( select * from bibs ) and br.PrimaryMARCTOMID not in (6,28,41) and br.RecordStatusID not in (4)
--order by br.BibliographicRecordID,\[targetaudience\], bs.Data, br.BrowseTitle, br.BrowseAuthor
union
select distinct br.BrowseTitle
,br.BrowseAuthor
,br.BibliographicRecordID
,bs.Data
,case when (left(t.Description, 7) = 'unknown' or t.Description is null) then '' else t.Description end \[TargetAudience\]
,count(shr.SysHoldRequestID) over (partition by br.BibliographicRecordID) \[RequestCount\]
,(select count(*) from Polaris.polaris.CircItemRecords where AssociatedBibRecordID = br.BibliographicRecordID) \[ItemCount\]
FROM Polaris.Polaris.SysHoldRequests SHR (NOLOCK)
    INNER JOIN Polaris.Polaris.BibliographicRecords BR (NOLOCK)
        ON BR.BibliographicRecordID = SHR.BibliographicRecordID
    LEFT JOIN Polaris.Polaris.CircItemRecords CIR (NOLOCK)
        ON CIR.AssociatedBibRecordID = SHR.BibliographicRecordID
    LEFT JOIN Polaris.Polaris.BibliographicTags BT (NOLOCK)
        ON BT.BibliographicRecordID = BR.BibliographicRecordID
            AND BT.TagNumber = 245
    LEFT JOIN Polaris.Polaris.BibliographicSubfields BS (NOLOCK)
        ON BS.BibliographicTagID = BT.BibliographicTagID
            AND BS.Subfield = 'h'
    LEFT JOIN Polaris.Polaris.TargetAudiences T (NOLOCK)
        ON T.code = BR.marctargetaudience
    --INNER JOIN Polaris.Polaris.ItemStatuses IT (NOLOCK)
        --ON IT.ItemStatusID = CIR.ItemStatusID
    --INNER JOIN Polaris.Polaris.RecordStatuses RST (NOLOCK)
        --ON RST.RecordStatusID = BR.RecordStatusID
WHERE SysHoldStatusID in (1,3,4)
    AND BR.PrimaryMARCTOMID not in (6,28,41)
    AND BR.RecordStatusID not in (4)
    --AND br.BibliographicRecordID NOT IN (SELECT DISTINCT(cir.AssociatedBibRecordID)
--FROM Polaris.Polaris.CircItemRecords AS \[cir\] WITH (NOLOCK)
--WHERE cir.ItemStatusID NOT IN ('7','11','10','16'))
 GROUP BY BR.BrowseTitle,
         BR.BrowseAuthor,
         br.BibliographicRecordID,
         shr.SysHoldRequestID,
         BS.Data,
         T.Description
HAVING COUNT (distinct cir.itemrecordid) between 0 and 0
order by br.BibliographicRecordID,\[targetaudience\], bs.Data, br.BrowseTitle, br.BrowseAuthor

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a9292dba926340468eb5044b4d15a5e2.jpg"/>  vkruggel@orl.bc.ca

- <a id="acceptAnswer_236"></a>[Accept as answer](#)

<a id="commentAns_236"></a>[Add a Comment](#)

<a id="upVoteAns_234"></a>[](#)

0

<a id="downVoteAns_234"></a>[](#)

Hello Mike:

Your query caputes all of the BIBs that only have those status perfectly.  I constructed something similar but without much success a while back.  You out did me.  I am still missing all of the bibs though that have no actuall items and adding my original query to this query is what is causing me the most problems.  I have never had to mash two seprate queries together in such a way. 

If you have any ideas let me know, and thanks again for the help.

Vincent

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a9292dba926340468eb5044b4d15a5e2.jpg"/>  vkruggel@orl.bc.ca

- <a id="acceptAnswer_234"></a>[Accept as answer](#)

<a id="commentAns_234"></a>[Add a Comment](#)

<a id="upVoteAns_233"></a>[](#)

0

<a id="downVoteAns_233"></a>[](#)

I think this should give you what you're looking for. It won't let include a percent sign in my reply so my target audience comparison is a little wonky.

;with bibs as (
select distinct shr.BibliographicRecordID
from polaris.polaris.SysHoldRequests shr
left join polaris.polaris.CircItemRecords cir
on cir.AssociatedBibRecordID = shr.BibliographicRecordID
where shr.SysHoldStatusID in (1,3,4)
group by shr.SysHoldRequestID, shr.BibliographicRecordID
having count(case when cir.ItemStatusID in (7,10,11,16) then null else 1 end) = 0
)

select distinct br.BrowseTitle
,br.BrowseAuthor
,br.BibliographicRecordID
,bs.Data
,case when (left(ta.Description, 7) = 'unknown' or ta.Description is null) then '' else ta.Description end \[TargetAudience\]
,count(shr.SysHoldRequestID) over (partition by br.BibliographicRecordID) \[RequestCount\]
,(select count(*) from Polaris.polaris.CircItemRecords where AssociatedBibRecordID = br.BibliographicRecordID) \[ItemCount\]
from polaris.polaris.SysHoldRequests shr
join Polaris.polaris.BibliographicRecords br
on br.BibliographicRecordID = shr.BibliographicRecordID
left join polaris.polaris.BibliographicTags bt
on bt.BibliographicRecordID = shr.BibliographicRecordID and bt.TagNumber = 245
left join polaris.polaris.BibliographicSubfields bs
on bs.BibliographicTagID = bt.BibliographicTagID and bs.Subfield = 'h'
left join polaris.Polaris.TargetAudiences ta
on ta.Code = br.MARCTargetAudience
where br.BibliographicRecordID in ( select * from bibs ) and br.PrimaryMARCTOMID not in (6,28,41) and br.RecordStatusID not in (4)
order by \[targetaudience\], bs.Data, br.BrowseTitle, br.BrowseAuthor

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a9292dba926340468eb5044b4d15a5e2.jpg"/>  mfields@clcohio.org

- <a id="acceptAnswer_233"></a>[Accept as answer](#)

<a id="commentAns_233"></a>[Add a Comment](#)

<a id="upVoteAns_232"></a>[](#)

0

<a id="downVoteAns_232"></a>[](#)

Hello:

Unfortunatly it does not work.  My query has a Having Statement that only counts for zero items, and I need to count bibrecords that only have items with these statuses only as zero items and add them too the list.  You can run the sub query but it will return all bibrecods that match the criteria, but then be zeroed out.

I need a bibrecord that as two items that are both of the (7,11,19,16) status to be included in the report as a zero item.  If there is one item in the other statuses then it would not be a zero item. 

The report does a good job at getting all of the zero items, it is just adding the other bibrecords that mathc the criteria as pseudo zero items.  That is where I am missing the boat.

Vincent

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a9292dba926340468eb5044b4d15a5e2.jpg"/>  vkruggel@orl.bc.ca

- <a id="acceptAnswer_232"></a>[Accept as answer](#)

<a id="commentAns_232"></a>[Add a Comment](#)

<a id="upVoteAns_231"></a>[](#)

0

<a id="downVoteAns_231"></a>[](#)

Hello Trevor:

I did that beofore except without the Distict clause attached. It never occured to me to add Distinct.   I will add it in and give it a test.

Thanks for the help.

Vincent

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a9292dba926340468eb5044b4d15a5e2.jpg"/>  vkruggel@orl.bc.ca

- <a id="acceptAnswer_231"></a>[Accept as answer](#)

<a id="commentAns_231"></a>[Add a Comment](#)

<a id="upVoteAns_230"></a>[](#)

0

<a id="downVoteAns_230"></a>[](#)

Hey Vincent-

In order to acheive this result (requests with no good items), I used a nested query to exclude all the bibs with good items.

Step1 : It's easy to find all the "good" bibs.

SELECT DISTINCT(cir.AssociatedBibRecordID)
FROM Polaris.Polaris.CircItemRecords AS \[cir\] WITH (NOLOCK)
WHERE cir.ItemStatusID NOT IN ('7','11','10','16')

So we just take you're query and include a clause where all the bibs in your results are NOT in the "good bibs" list.

AND br.BibliographicRecordID NOT IN (SELECT DISTINCT(cir.AssociatedBibRecordID)
FROM Polaris.Polaris.CircItemRecords AS \[cir\] WITH (NOLOCK)
WHERE cir.ItemStatusID NOT IN ('7','11','10','16'))

Does that make sense? HTH

Trevor D
--
Trevor Diamond
Systems/UX Librarian
MAIN (Morris Automated Information Network), Morristown NJ
www.mainlib.org

answered 3 years ago  by <img width="18" height="18" src="../../_resources/a9292dba926340468eb5044b4d15a5e2.jpg"/>  trevor.diamond@mainlib.org

- <a id="acceptAnswer_230"></a>[Accept as answer](#)

<a id="commentAns_230"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Report of Lost and Paid Items](https://iii.rightanswers.com/portal/controller/view/discussion/331?)
- [Adding number of "live" items to a report](https://iii.rightanswers.com/portal/controller/view/discussion/666?)
- [Searching for Items that have not circ'd in 2 years ex. DVD's](https://iii.rightanswers.com/portal/controller/view/discussion/1053?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)

### Related Solutions

- [Item Circulation Statistics Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930404036798)
- [Too long reports not including central item status](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180825094507890)
- [Item Circulation by Statistical Code](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930592909894)
- [Claimed item does not appear in the Claimed Items report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930283717537)
- [Long Overdue Item report shows thousands of items](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930725698901)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)