Most popular titles by collection in a period of time SQL  - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# Most popular titles by collection in a period of time SQL

0

103 views

I know of the Top Circulating Titles by Collection canned report, but I'm looking for actual SQL I can edit (as opposed to just the stored procedure for the canned report).

I'd like to see top 10 circulating titles for each chosen collection.

Hoping it would look something like this:

0/1/01/2019-12/31/2019

Collection: Juv Fiction

1\. Diary of a Wimpy Kid | Kinney, Jeff | Circs: 975

2\. Captains Underpants | Pilkey, Dav| Circs 823 

3....

10

Collection: Biography

1\. Educated| Westover, Tara | Circs: 1007

2\. Becoming| Obama, Michelle| Circs 957

3....

10

asked 1 year ago  by <img width="18" height="18" src="../../_resources/98e8f698e583419f9dd63c6a513b9985.jpg"/>  crosenthal@pwcgov.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_796"></a>[](#)

0

<a id="downVoteAns_796"></a>[](#)

Answer Accepted

Here is a query adapted from one I came up with for finding the top circulating items by collection. There is probably a better/faster way of doing this by using PARTITION, but this does seem to work.

The query as written counts only "original" checkouts. There's a comment at the end with the coding to include renewals.

It also includes a line to exclude certain collections from the final output (the WHERE line above the line that drops the temporary table). This line can be removed if you want to include all collections.

Report Builder should be able to create a grouping by Collection that will organize your output similar to your example. I'm not sure how best to get the number before each title, however.

You can remove the part where you SET @StartDate and @EndDate from the query if you want to have date parameters available in the report.

Hopefully this will be a start:

JT

BEGIN
 SET NOCOUNT ON
 SET ansi_warnings OFF
 DECLARE @c int
 DECLARE @StartDate Date
 DECLARE @EndDate Date
 SET @c = 1
 SET @StartDate = '01/01/2019'
 SET @EndDate = '01/31/2019'
 
 CREATE TABLE #TopCircItems
 ( \[Collection\] varchar(75),
  \[CollID\] int,
  \[BibID\] int,
  \[Title\] varchar (256),
  \[Author\] varchar (100),
  \[Circs:\] int NULL
  )
WHILE @c <= (SELECT MAX(CollectionID) FROM Polaris.Polaris.Collections WITH (NOLOCK))

BEGIN

INSERT INTO #TopCircItems(\[Collection\], \[CollID\], \[BibID\], \[Title\], \[Author\], \[Circs:\])

(SELECT TOP 10
col.Name AS \[Collection\]
,t2.CollID
,t1.BibID
,t1.Title
,t1.Author
,t2.\[Circs:\]
FROM
(SELECT
BibliographicRecordID AS \[BibID\]
,br.BrowseTitle as \[Title\]
,br.BrowseAuthor as \[Author\]
FROM Polaris.Polaris.Bibliographicrecords br WITH (NOLOCK)
) t1

JOIN

(SELECT
br.BibliographicRecordID
,ir.AssignedCollectionID as CollID
,COUNT(irh.ItemRecordHistoryID) as \[Circs:\]
FROM Polaris.Polaris.Bibliographicrecords br WITH (NOLOCK)
JOIN Polaris.Polaris.ItemRecords ir (NOLOCK)
ON (br.BibliographicRecordID = ir.AssociatedBibRecordID)
JOIN Polaris.Polaris.ItemRecordHistory irh (NOLOCK)
ON (ir.ItemRecordID = irh.ItemRecordID and irh.ActionTakenID in (13,75,77,81,89,91))

WHERE irh.TransactionDate >= @StartDate
AND irh.TransactionDate < DATEADD(dd,1,@EndDate)
AND irh.OrganizationID = 11
AND ir.AssignedCollectionID = @c
GROUP BY
br.BibliographicRecordID
,ir.AssignedCollectionID
) t2

ON (t1.\[BibID\] = t2.BibliographicRecordID)

JOIN Polaris.Polaris.Collections col (NOLOCK)
ON (t2.CollID = col.CollectionID)
)

ORDER BY t2.\[Circs:\] DESC, t1.Author, t1.Title
 
SET @c = @c + 1

END

SELECT
\[Collection\]
,\[Title\]
,\[Author\]
,\[Circs:\]

FROM #TopCircItems
WHERE CollID NOT IN (2,3)

DROP TABLE #TopCircItems

SET ansi_warnings ON

END

-----If you want to include renewals, use this string in the join to ItemRecordHistory:
--ON (ir.ItemRecordID = irh.ItemRecordID and irh.ActionTakenID in (13,75,77,81,89,91,73,76,80,93))

answered 1 year ago  by <img width="18" height="18" src="../../_resources/98e8f698e583419f9dd63c6a513b9985.jpg"/>  jwhitfield@aclib.us

<a id="commentAns_796"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL to Find all bibs with same title.](https://iii.rightanswers.com/portal/controller/view/discussion/675?)
- [Checkouts by Title in a collection at a branch by year](https://iii.rightanswers.com/portal/controller/view/discussion/932?)
- [SQL for items returned late by collection](https://iii.rightanswers.com/portal/controller/view/discussion/718?)
- [Need help with query to find checked out titles with holds](https://iii.rightanswers.com/portal/controller/view/discussion/1063?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)

### Related Solutions

- [SQL query for item counts by collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930944635626)
- [What is the difference between relevance and popularity?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930891325370)
- [Polaris Collection Agency job after upgrading to SQL2014](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930771502651)
- [Collection Agency SQL Job Not Running](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930837487784)
- [How to stop floating items](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180122143949499)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)