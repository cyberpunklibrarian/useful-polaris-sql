[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# One building, two branches--intended and unintended consquences? Experiences?

0

51 views

One of our branches is about to move into a new building in about six months. The new building will have a drive up window for holds. We (the Polaris gurus) are thinking that we will need a new branch so that people can request that their holds to be picked up at the window (unfortunately, the window was not designed so that the regular  holds and the pickup window holds can be physically placed together--the pickup window is in the back of the building...)  
<br/>Has anyone here done this and what advice can you give me/us?

I'm thinking that they will need to have a computer that is dedicated to the window branch so that they will be able to transit the holds for the window branch.

&nbsp;

Anything else?

&nbsp;

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_827336125fb042d2bf05861bd12566a4.jpg"/> sgrant@somd.lib.md.us

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 4 years ago

<a id="upVoteAns_704"></a>[](#)

0

<a id="downVoteAns_704"></a>[](#)

Hi!

We have drive thru windows at three of our branches and I did create a new branch in Polaris for each one.  We have the same issue of the regular hold shelf being far away from the pick-up windows.  

The main issues I come across are 1) staff forgetting to check-in a drive-thru branch hold at the drive thru branch so it stays in transit and the patron never gets the hold.  And 2) staff tend to switch a patron's registered branch to the drive thru in order to "force" the hold pick-up preferred branch. 

We don't want our drive thru branches to "own" any patrons so I created an overnight SQL job that finds those patrons, updates the hold pickup branch to the drive thru, and switches the registered branch to the normal branch.

Each pick-up window does have a dedicated workstation, which I do think cuts down on the number of holds not making it out of transit.  An idea that I didn't use but might be helpful is to create a report that goes to that branch's manager with a list of in transit hold items that were incorrectly checked in, at least until the staff get into the habit of having two locations!

Gabrielle

answered 4 years ago by ![ggosselin@richlandlibrary.com](https://iii.rightanswers.com/portal/app/images/custom/avatar_ggosselin@richlandlibrary.com20170530111420.JPG) ggosselin@richlandlibrary.com

- <a id="acceptAnswer_704"></a>[Accept as answer](#)

Oh wow, I didn't even think about someone assigning a patron to that "branch"! That's a good consequence to be aware of--we wouldn't want that either.  
<br/>Thanks so much and I'll definitely keep your notes for when we meet to discuss the issue.

— sgrant@somd.lib.md.us 4 years ago

<a id="commentAns_704"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Experience with Reading History](https://iii.rightanswers.com/portal/controller/view/discussion/1091?)
- [Experiences with E'ware eCommerce in staff client](https://iii.rightanswers.com/portal/controller/view/discussion/677?)
- [Experiences Using Student IDs and Bookmobile TRN](https://iii.rightanswers.com/portal/controller/view/discussion/1255?)
- [Looking to gather Libraries' experience with Student Cards](https://iii.rightanswers.com/portal/controller/view/discussion/444?)
- [Looking for information on experiences with student IDs as library cards](https://iii.rightanswers.com/portal/controller/view/discussion/814?)

### Related Solutions

- [Emergency Planning Resources](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=210909100219740)
- [Where are UMS reports stored?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930596768478)
- [How do I change the text of the You Saved receipts?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170119085231340)
- [Polaris Considerations for Opening Physical Locations](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=200421171849439)
- [Can you go offline with the new client during an upgrade?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930892722856)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)