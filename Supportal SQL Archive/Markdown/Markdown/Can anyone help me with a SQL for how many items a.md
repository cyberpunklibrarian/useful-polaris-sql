[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Can anyone help me with a SQL for how many items are checked in daily?

0

68 views

&nbsp;I have ran multiple simply reports but keep getting drastically different numbers.  I tried Item list reports and Item statistical reports.  My parameters are very similar per search but the numbers vary greatly.  Any help you can give would be greatly appreciated.

asked 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_9047f865550748d09655eac3f66569e1.jpg"/> robin.walden@brentwoodtn.gov

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 2 years ago

<a id="upVoteAns_1247"></a>[](#)

0

<a id="downVoteAns_1247"></a>[](#)

Hi Rachel,

Thank you for helping.  I think I need a more complicated query.  I only want physical items that are being returned daily.  

&nbsp;

Kindly,

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_9047f865550748d09655eac3f66569e1.jpg"/> robin.walden@brentwoodtn.gov

- <a id="acceptAnswer_1247"></a>[Accept as answer](#)

Got it. How about this? In the "and cir.MaterialTypeID NOT IN (6, 8, 20, 21, 22, 23)" you would add the material types IDs you don't want included (ebooks and such).  

SELECT

CONVERT(NVARCHAR(11), th.TranClientDate) as 'Date',  
Count (DISTINCT th.TransactionID) as 'Check-ins'

  
FROM  
PolarisTransactions.Polaris.TransactionHeaders th  
LEFT JOIN PolarisTransactions.Polaris.TransactionDetails td on th.TransactionID = td.TransactionID  
LEFT JOIN Polaris.Polaris.CircItemRecords cir on td.numValue = cir.ItemRecordID

WHERE  
th.TransactionTypeID = 6002  
and td.TransactionSubTypeID = 38  
and cir.MaterialTypeID NOT IN (6, 8, 20, 21, 22, 23)  
and th.TransactionDate between dateadd(WEEK,-1,GETDATE()) and dateadd(DAY,-1,GETDATE())

GROUP BY  
CONVERT(NVARCHAR(11), th.TranClientDate)

ORDER By  
CONVERT(NVARCHAR(11), th.TranClientDate)

— rstein@somd.lib.md.us 2 years ago

<a id="commentAns_1247"></a>[Add a Comment](#)

<a id="upVoteAns_1246"></a>[](#)

0

<a id="downVoteAns_1246"></a>[](#)

Hi Robin, 

&nbsp;

If all you want to count is every time the transaction "check in" occurred, I think something like this should do it: 

&nbsp;

SELECT  
CONVERT(NVARCHAR(11), th.TranClientDate) as 'Date',  
COUNT(th.TransactionID) as 'Check Ins'

FROM  
PolarisTransactions.Polaris.TransactionHeaders th

WHERE  
th.TransactionTypeID = 6002  
and th.TransactionDate between dateadd(WEEK,-1,GETDATE()) and GETDATE()

GROUP BY  
CONVERT(NVARCHAR(11), th.TranClientDate)

ORDER BY  
CONVERT(NVARCHAR(11), th.TranClientDate)

&nbsp;

That will return the number of "check in" transactions by day, for the past week. If you want to differentiate further, that would be more complicated. 

&nbsp;

Does that help at all? 

&nbsp;

Rachael 

answered 2 years ago by <img width="18" height="18" src="../_resources/default-avatar_9047f865550748d09655eac3f66569e1.jpg"/> rstein@somd.lib.md.us

- <a id="acceptAnswer_1246"></a>[Accept as answer](#)

<a id="commentAns_1246"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [SQL Query For Patrons Registered at a Particular Branch Who Don't Use That Branch](https://iii.rightanswers.com/portal/controller/view/discussion/910?)
- [Reset due dates - SQL help](https://iii.rightanswers.com/portal/controller/view/discussion/1324?)
- [Automatically Check In Items](https://iii.rightanswers.com/portal/controller/view/discussion/1064?)
- [List all items with a Special Item Check In Note?](https://iii.rightanswers.com/portal/controller/view/discussion/548?)
- [I need help creating a SQL for circulation data that includes a lot of details.](https://iii.rightanswers.com/portal/controller/view/discussion/550?)

### Related Solutions

- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [Do the home and owning branch need to match for floating item records?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930466429189)
- [What happens when old Lost items are returned?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180907110226360)
- [ExpressCheck: Held items can be checked out by the wrong patrons](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930574330183)
- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)