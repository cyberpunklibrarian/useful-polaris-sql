SQL Search by Collection - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# SQL Search by Collection

0

71 views

I would like to search for items that have none in Collection or that was left empty.  Does anyone have a SQL search? 

Thank you in advance, 

Teresa 

asked 1 year ago  by <img width="18" height="18" src="../../_resources/bbbc844b42fc428c9fb86f4bf64c699a.jpg"/>  treel@mypccl.org

[none](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=340#t=commResults) [sql collection](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=341#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 2 Replies   last reply 1 year ago

<a id="upVoteAns_909"></a>[](#)

0

<a id="downVoteAns_909"></a>[](#)

Answer Accepted

Hi Teresa.

Here's a quicky and dirty Find Tool query:

SELECT ItemRecordID FROM Polaris.Polaris.ItemRecords WITH (NOLOCK)

WHERE RecordStatusID = 1

AND AssignedCollectionID IS NULL

This looks for Final records with no assigned collection code.

Hope this helps.

JT

answered 1 year ago  by <img width="18" height="18" src="../../_resources/bbbc844b42fc428c9fb86f4bf64c699a.jpg"/>  jwhitfield@aclib.us

<a id="commentAns_909"></a>[Add a Comment](#)

<a id="upVoteAns_910"></a>[](#)

0

<a id="downVoteAns_910"></a>[](#)

You can run this from the ItemRecord Find Tool.  It looks for items that aren't in a Deleted statis or On-order.  If you want this to be Branch specific you can add this line: AND AssignedBranchID = ?

SELECT ItemRecordID
FROM CircItemRecords
WHERE AssignedCollectionID IS NULL
AND RecordStatusID <> 4
AND ItemStatusID NOT IN (13)

Rex Helwig

answered 1 year ago  by <img width="18" height="18" src="../../_resources/bbbc844b42fc428c9fb86f4bf64c699a.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_910"></a>[Accept as answer](#)

<a id="commentAns_910"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [How do you enter/search for titles with special characters?](https://iii.rightanswers.com/portal/controller/view/discussion/320?)
- [Delete a collection, stat code, or Shelf code from drop down list on item records](https://iii.rightanswers.com/portal/controller/view/discussion/402?)
- [SQL for Bibs lacking local call #](https://iii.rightanswers.com/portal/controller/view/discussion/605?)
- [SQL for items withdrawn by a particular staff member](https://iii.rightanswers.com/portal/controller/view/discussion/809?)
- [Finding new and replaced bibs in Polaris between certain dates](https://iii.rightanswers.com/portal/controller/view/discussion/871?)

### Related Solutions

- [Using multiplication in a Find Tool SQL search](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=161220164029123)
- [Collection workform "branches" checkboxes](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930545936496)
- [CollectionHQ MARC Export Data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930742535665)
- [Requirements for CollectionHQ Exports](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930727440796)
- [Creating a new collection](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930912772907)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)