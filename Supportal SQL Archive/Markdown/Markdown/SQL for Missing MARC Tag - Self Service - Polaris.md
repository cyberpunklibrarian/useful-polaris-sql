SQL for Missing MARC Tag - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=11)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# SQL for Missing MARC Tag

0

92 views

I'm trying to find Bib records that are missing a particular MARC tag (092). I have modified some SQL searches found here that locate existing tags and subfields and those work well for existing tags, but when I try to locate records without a tag, I'm hitting a stumbling block. Does anyone have any ideas on how to accomplish this?

Thanks,

Stephanie

asked 1 year ago  by <img width="18" height="18" src="../../_resources/03e3269a132d419bb4da5d600a960b8b.jpg"/>  svanatta@avondale.org

[sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 1 year ago

<a id="upVoteAns_901"></a>[](#)

0

<a id="downVoteAns_901"></a>[](#)

Answer Accepted

I did some more searching on the forums and found a solultion from sgrant that I was able to adjust to my needs. Here's the SQL from sgrant in case anyone was interested:

select distinct br.BibliographicRecordID from Polaris.BibliographicRecords br (nolock)

where

(

br.BibliographicRecordID NOT IN

(select bt050.BibliographicRecordID from Polaris.BibliographicTags bt050 where bt050.EffectiveTagNumber = 50)

AND

br.BibliographicRecordID NOT IN

(select bt082.BibliographicRecordID from Polaris.BibliographicTags bt082 where bt082.EffectiveTagNumber = 82)

AND

br.BibliographicRecordID NOT IN

(select bt092.BibliographicRecordID from Polaris.BibliographicTags bt092 where bt092.EffectiveTagNumber = 92)

)

AND br.ILLFlag = 0

AND br.CreationDate < CONVERT(date,GETDATE() - 180)

AND BR.RecordStatusID = 1

answered 1 year ago  by <img width="18" height="18" src="../../_resources/03e3269a132d419bb4da5d600a960b8b.jpg"/>  svanatta@avondale.org

<a id="commentAns_901"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Use Item Record Call Numbers to Bulk Update Bib MARC Tag](https://iii.rightanswers.com/portal/controller/view/discussion/235?)
- [Duplicate ISBN in a MARC](https://iii.rightanswers.com/portal/controller/view/discussion/819?)
- [TOMs and MARC coding](https://iii.rightanswers.com/portal/controller/view/discussion/31?)
- [Accession Numbers](https://iii.rightanswers.com/portal/controller/view/discussion/1029?)
- [Can you change a tag in a bib record in Leap?](https://iii.rightanswers.com/portal/controller/view/discussion/1055?)

### Related Solutions

- [007 LDR Cataloging Question](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930329896653)
- [Bibliographic and Authority Record tag order](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930557664312)
- [MARC 049 tag validation](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930325894633)
- [How is RDA (Resource Description and Access) supported in Polaris products?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930454338721)
- [Searching for Accelerated Reader Tag (526) Data](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930649873277)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)