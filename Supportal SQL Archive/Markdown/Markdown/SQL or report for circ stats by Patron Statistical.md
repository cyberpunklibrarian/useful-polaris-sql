SQL or report for circ stats by Patron Statistical Class? - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# SQL or report for circ stats by Patron Statistical Class?

0

53 views

I am trying to get, every month, a count of total circulation arranged by Patron Statistical Class. I am sure this doable via SQL but being new to SQL am struggling a bit. I have the following query (given by another library) that groups by City, but I can't find the equivalent to "pc.City" to get this to return results Statistical Class. Can someone help?
    DECLARE @StartDate date
    
    DECLARE @EndDate date
       
    SET @StartDate = '20190401'    
    SET @EndDate = '20190430'
    
    SELECT pc.City, COUNT(*) AS \[Circs\]   
    FROM PolarisTransactions.Polaris.TransactionHeaders AS \[th\] WITH (NOLOCK)
          
    INNER JOIN PolarisTransactions.Polaris.TransactionDetails AS \[td1\] WITH (NOLOCK)
 
    ON th.TransactionID = td1.TransactionID AND td1.TransactionSubTypeID = '302'
    
    INNER JOIN Polaris.Polaris.PostalCodes AS \[pc\] WITH (NOLOCK)
   
    ON td1.numValue = pc.PostalCodeID
    LEFT OUTER JOIN PolarisTransactions.Polaris.TransactionDetails AS \[td2\] WITH (NOLOCK)
    
    ON th.TransactionID = td2.TransactionID AND td2.TransactionSubTypeID = '124'
    
    WHERE th.TransactionTypeID = '6001'
     
    AND th.TranClientDate BETWEEN @StartDate AND DATEADD(day,1,@EndDate)
    
    GROUP BY pc.City

asked 2 years ago  by <img width="18" height="18" src="../../_resources/88e05c9d8c4b4fc398f23a17e27e6464.jpg"/>  josh.rouan@baldwinlib.org

<a id="comment"></a>[Add a Comment](#)

## 1 Reply   last reply 2 years ago

<a id="upVoteAns_571"></a>[](#)

0

<a id="downVoteAns_571"></a>[](#)

Hi Josh.

Changing the SubTypeID  for td1 to 33 will pull the PatronStatClassCode.

The join to the PatronStatClassCodes table is tricky, because it is set up to have a separate list of stat codes for each organization.

I'm not sure if this will work, but I think you can do this:

INNER JOIN Polaris.Polaris.PatronStatClassCodes psc (NOLOCK)
ON (td1.numvalue = psc.StatisticalClassID and psc.OrganizationID = th.OrganizationID)

You will then need to go to the SELECT statement and change pc.City to psc.Description \[StatCode\] (or whatever you want the column label to be.

I haven't tried this, but I hope it helps.

JT

answered 2 years ago  by <img width="18" height="18" src="../../_resources/88e05c9d8c4b4fc398f23a17e27e6464.jpg"/>  jwhitfield@aclib.us

- <a id="acceptAnswer_571"></a>[Accept as answer](#)

<a id="commentAns_571"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [Simply Reports Discrepancies](https://iii.rightanswers.com/portal/controller/view/discussion/960?)
- [Finding a item's \[DELETED\] title when searching patron fines](https://iii.rightanswers.com/portal/controller/view/discussion/703?)
- [Simply reports patron services](https://iii.rightanswers.com/portal/controller/view/discussion/355?)

### Related Solutions

- [What do all the values in the Statistical Summary report mean?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170217134137722)
- [Where to find Patron Circ Count in Patron record](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930283871188)
- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [Statistics for Holds and ILLs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930628713826)
- [Item Circulation Statistics Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930404036798)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)