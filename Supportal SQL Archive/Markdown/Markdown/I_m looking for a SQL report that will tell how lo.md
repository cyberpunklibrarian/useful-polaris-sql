I'm looking for a SQL report that will tell how long items are in held status. - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# I'm looking for a SQL report that will tell how long items are in held status.

0

67 views

I'm trying to find out how long items on hold are waiting to be picked up. Our library is considering shortening how long we keep items on hold. I would like to see library name, material type, days on hold for the month of May 2019. Or if you can't do days on hold, could you do held date and check out date and I'll do the math? Is this possible? Thanks!

asked 2 years ago  by <img width="18" height="18" src="../../_resources/21e467532ec246b380d49f866349dfb3.jpg"/>  bsierra@davenportlibrary.com

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 2 years ago

<a id="upVoteAns_585"></a>[](#)

0

<a id="downVoteAns_585"></a>[](#)

Turns out I can't run this in the find tool! I've put in a request for acces to SQL Management Tool. I'll let you know how it turns out. Thanks again!

answered 2 years ago  by <img width="18" height="18" src="../../_resources/21e467532ec246b380d49f866349dfb3.jpg"/>  bsierra@davenportlibrary.com

- <a id="acceptAnswer_585"></a>[Accept as answer](#)

<a id="commentAns_585"></a>[Add a Comment](#)

<a id="upVoteAns_584"></a>[](#)

0

<a id="downVoteAns_584"></a>[](#)

If you are in MSSMS then make sure you are querying the Polaris Database and not the Master db.

Here is the query rewritten to accomodate that:

select irh1.ItemRecordHistoryID, irh2.ItemRecordHistoryID, irh1.ItemRecordID, Cast(irh1.TransactionDate as Date) as "Held Date",
Cast(irh2.TransactionDate as Date) as "Checked Out Date",
convert(VARCHAR, abs(datediff(day, irh1.TransactionDate, irh2.TransactionDate))) AS "DaysHeld"
from Polaris.Polaris.ItemRecordHistory irh1
join Polaris.Polaris.ItemRecordHistory irh2 on ((irh2.ItemRecordID = irh1.ItemRecordID) and (irh2.PatronID = irh1.PatronID))
where irh1.OrganizationID = 3 --enter OrganizationID here
and irh1.TransactionDate between '2019-05-01' and '2019-06-01'
and irh1.ActionTakenID = 66
and irh1.OldItemStatusID = 4
and irh2.ActionTakenID = 13
and irh2.NewItemStatusID = 2
and irh1.TransactionDate < irh2.TransactionDate
ORDER BY irh1.TransactionDate

answered 2 years ago  by <img width="18" height="18" src="../../_resources/21e467532ec246b380d49f866349dfb3.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_584"></a>[Accept as answer](#)

<a id="commentAns_584"></a>[Add a Comment](#)

<a id="upVoteAns_583"></a>[](#)

0

<a id="downVoteAns_583"></a>[](#)

Thank you. I'm getting an note that says cannot be processed by remote database. 

answered 2 years ago  by <img width="18" height="18" src="../../_resources/21e467532ec246b380d49f866349dfb3.jpg"/>  bsierra@davenportlibrary.com

- <a id="acceptAnswer_583"></a>[Accept as answer](#)

<a id="commentAns_583"></a>[Add a Comment](#)

<a id="upVoteAns_582"></a>[](#)

0

<a id="downVoteAns_582"></a>[](#)

Give this a try.

select irh1.ItemRecordHistoryID, irh2.ItemRecordHistoryID, irh1.ItemRecordID, Cast(irh1.TransactionDate as Date) as "Held Date",
Cast(irh2.TransactionDate as Date) as "Checked Out Date",
convert(VARCHAR, abs(datediff(day, irh1.TransactionDate, irh2.TransactionDate))) AS "DaysHeld"
from ItemRecordHistory irh1
join ItemRecordHistory irh2 on ((irh2.ItemRecordID = irh1.ItemRecordID) and (irh2.PatronID = irh1.PatronID))
where irh1.OrganizationID = 3 --enter OrganizationID here
and irh1.TransactionDate between '2019-05-01' and '2019-06-01'
and irh1.ActionTakenID = 66
and irh1.OldItemStatusID = 4
and irh2.ActionTakenID = 13
and irh2.NewItemStatusID = 2
and irh1.TransactionDate < irh2.TransactionDate
ORDER BY irh1.TransactionDate

Rex Helwig
Finger Lakes Library System
rhelwig@flls.org

answered 2 years ago  by <img width="18" height="18" src="../../_resources/21e467532ec246b380d49f866349dfb3.jpg"/>  rhelwig@flls.org

- <a id="acceptAnswer_582"></a>[Accept as answer](#)

<a id="commentAns_582"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Polaris tool bar reports refresh](https://iii.rightanswers.com/portal/controller/view/discussion/90?)
- [Is there anyway to pull the SQL from the canned reports and notices?](https://iii.rightanswers.com/portal/controller/view/discussion/536?)
- [SQL query for percentage of checkouts that were for held items](https://iii.rightanswers.com/portal/controller/view/discussion/905?)
- [I'm looking for a report that will tell me how many bib records a staff member has created and/or modified in a month.](https://iii.rightanswers.com/portal/controller/view/discussion/942?)
- [Searching for Items that have not circ'd in 2 years ex. DVD's](https://iii.rightanswers.com/portal/controller/view/discussion/1053?)

### Related Solutions

- [Too long reports not including central item status](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=180825094507890)
- [Locating a SQL query pulled from a Simply Reports report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930537417112)
- [Statistics for Holds and ILLs](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930628713826)
- [Item Circulation Statistics Report](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930404036798)
- [Item Circulation by Statistical Code](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930592909894)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)