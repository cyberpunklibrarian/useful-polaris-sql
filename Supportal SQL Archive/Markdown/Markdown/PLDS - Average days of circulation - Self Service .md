PLDS - Average days of circulation - Self Service - Polaris

[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20200406142341.png)Self Service](https://iii.rightanswers.com/portal/ss/)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)
- [Ask the Community](https://iii.rightanswers.com/portal/controller/post/discussion/?communityID=4&topicID=17)
- [Open a Ticket](https://iii.rightanswers.com/portal/app/customportlets/netsuite/openticket.jsp)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Reporting](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=17)

# PLDS - Average days of circulation

0

87 views

Does anyone have SQL for average days of circulation for PLDS?

Thanks,

Amy

asked 2 years ago  by <img width="18" height="18" src="../../_resources/2433fcfa20d94fc58378aac2965377c0.jpg"/>  aal-shabibi@champaign.org

<a id="comment"></a>[Add a Comment](#)

## 7 Replies   last reply 2 years ago

<a id="upVoteAns_545"></a>[](#)

0

<a id="downVoteAns_545"></a>[](#)

Answer Accepted

What version of Polaris are you on? I don't think support for this was added until 6.1 and it would only be for CKOs that happened after the upgrade that included support for it. Also there is a known bug for in-house items that I don't think is fixed until 6.3, so you'd probably need to filter those out of your calculations otherwise they'll greatly skew your results.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/2433fcfa20d94fc58378aac2965377c0.jpg"/>  wosborn@clcohio.org

<a id="commentAns_545"></a>[Add a Comment](#)

<a id="upVoteAns_593"></a>[](#)

0

<a id="downVoteAns_593"></a>[](#)

Thank you!

answered 2 years ago  by <img width="18" height="18" src="../../_resources/2433fcfa20d94fc58378aac2965377c0.jpg"/>  aal-shabibi@champaign.org

- <a id="acceptAnswer_593"></a>[Accept as answer](#)

<a id="commentAns_593"></a>[Add a Comment](#)

<a id="upVoteAns_592"></a>[](#)

0

<a id="downVoteAns_592"></a>[](#)

Kelly Sobrino gave me some starter code this week that I leveraged to create a version for comparing CKO duration by Collection. Here's two versions:

\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
SELECT th.TransactionID
    , th.TranClientDate
    , td1.TransactionSubTypeID
    , td1.numValue AS 'CollCode'
    , col.Name
    , col.Abbreviation
    , td2.TransactionSubTypeID
    , td2.numValue AS 'Minutes'
    , td2.numValue/1440 AS 'Days'
FROM  PolarisTransactions.Polaris.TransactionHeaders th WITH (nolock)
    INNER JOIN PolarisTransactions.Polaris.TransactionDetails td1 WITH (nolock)
        ON td1.TransactionID=th.TransactionID
            AND td1.TransactionSubTypeID = 61 -- Collection Code
    INNER JOIN PolarisTransactions.Polaris.TransactionDetails td2 WITH (nolock)
        ON td2.TransactionID = th.TransactionID
            AND td2.TransactionSubTypeID = 323 -- CKO duration in Minutes
    
    INNER JOIN Polaris.Polaris.Collections col WITH (nolock)
        ON td1.numValue = col.CollectionID
WHERE th.TransactionTypeID = 6002 -- CKI
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
--  Roll-up version
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
SELECT
     col.Name
    , col.Abbreviation
    , ROUND(AVG(CAST(td2.numValue AS FLOAT)/1440.0),2) AS 'AvgDays'
    --, td2.numValue/1440 AS 'Days'
    , COUNT(*) as 'CKI Count'
FROM  PolarisTransactions.Polaris.TransactionHeaders th WITH (nolock)
    INNER JOIN PolarisTransactions.Polaris.TransactionDetails td1 WITH (nolock)
        ON td1.TransactionID=th.TransactionID
            AND td1.TransactionSubTypeID = 61 -- Collection Code
    INNER JOIN PolarisTransactions.Polaris.TransactionDetails td2 WITH (nolock)
        ON td2.TransactionID = th.TransactionID
            AND td2.TransactionSubTypeID = 323 -- CKO duration in Minutes
    
    INNER JOIN Polaris.Polaris.Collections col WITH (nolock)
        ON td1.numValue = col.CollectionID
WHERE th.TransactionTypeID = 6002 -- CKI
GROUP BY col.Name
    , col.Abbreviation
ORDER BY col.Abbreviation

answered 2 years ago  by <img width="18" height="18" src="../../_resources/2433fcfa20d94fc58378aac2965377c0.jpg"/>  jlmcconnel@cob.org

- <a id="acceptAnswer_592"></a>[Accept as answer](#)

This should do the job, you'll just want to be careful of the bug Wes mentioned before. It's an easy enough bug to work around though, and I've included some SQL below to help do so.

To fix the first one I'd probably just filter the bad entries out entirely by adding a 'and td2.numValue < 4000000' to the WHERE clause.

The second, however, can be fixed a little more 'gracefully' by swapping the current 'AvgDays' field with: cast(avg(case when td2.numValue &lt; 4000000 then td2.numValue end/1440.0) as decimal(18,2)) as 'AvgDays'. The new one will filter out any values &gt; 4 million in the average calculation, replacing them with NULL and thereby excluding them. This will only affect the average day calculation and won't change the checkin count.

— mfields@clcohio.org 2 years ago

<a id="commentAns_592"></a>[Add a Comment](#)

<a id="upVoteAns_549"></a>[](#)

0

<a id="downVoteAns_549"></a>[](#)

Not sure, all I got was a bat signal from the person needing the info. I've asked for the form so I can see the question(s) in context. Let me see what I can ferret out.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/2433fcfa20d94fc58378aac2965377c0.jpg"/>  aal-shabibi@champaign.org

- <a id="acceptAnswer_549"></a>[Accept as answer](#)

<a id="commentAns_549"></a>[Add a Comment](#)

<a id="upVoteAns_548"></a>[](#)

0

<a id="downVoteAns_548"></a>[](#)

I think support for it in 6.1 is there although 6.1 also has the in-house bug. I don't have easy access to the PLDS requirements, how do they want it broken down, by material type, patron code?

answered 2 years ago  by <img width="18" height="18" src="../../_resources/2433fcfa20d94fc58378aac2965377c0.jpg"/>  wosborn@clcohio.org

- <a id="acceptAnswer_548"></a>[Accept as answer](#)

<a id="commentAns_548"></a>[Add a Comment](#)

<a id="upVoteAns_547"></a>[](#)

0

<a id="downVoteAns_547"></a>[](#)

transaction database, loan length in minutes?

answered 2 years ago  by <img width="18" height="18" src="../../_resources/2433fcfa20d94fc58378aac2965377c0.jpg"/>  aal-shabibi@champaign.org

- <a id="acceptAnswer_547"></a>[Accept as answer](#)

<a id="commentAns_547"></a>[Add a Comment](#)

<a id="upVoteAns_546"></a>[](#)

0

<a id="downVoteAns_546"></a>[](#)

We're on 6.1.

answered 2 years ago  by <img width="18" height="18" src="../../_resources/2433fcfa20d94fc58378aac2965377c0.jpg"/>  aal-shabibi@champaign.org

- <a id="acceptAnswer_546"></a>[Accept as answer](#)

<a id="commentAns_546"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Average hold wait times](https://iii.rightanswers.com/portal/controller/view/discussion/845?)
- [Circulation without Renewals](https://iii.rightanswers.com/portal/controller/view/discussion/87?)
- [Average time things are lost before coming back?](https://iii.rightanswers.com/portal/controller/view/discussion/436?)
- [SQL for circulation numbers](https://iii.rightanswers.com/portal/controller/view/discussion/94?)
- [Shelf location circulation statistics](https://iii.rightanswers.com/portal/controller/view/discussion/441?)

### Related Solutions

- [Set Free Days for Holidays proactively](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930494920674)
- [How do Dates Closed factor in to grace periods?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930368145777)
- [Which library's settings are used for dates closed and days not fineable?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930253968387)
- [Special loan period of zero (0) days in check out](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930733069676)
- [How do I add a grace day to the loan period?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930333927519)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2021R1.1
© 2021 Upland Software, Inc. All Rights Reserved

[](#)