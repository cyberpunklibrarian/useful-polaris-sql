[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Cataloging](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=11)

# Polaris OCLC Holdings

0

255 views

I would like to know how other libraries and consortia keep their OCLC holdings up-to-date. I’ve come up with the following possible options:

1.  We maintain our holdings as we catalog using OCLC Connexion.
2.  We run an SQL search and send OCLC an add file and a delete file based on that search (or searches).
3.  We compare our holdings from one OCLC sync to the next and send OCLC files of adds and deletes based on this comparison.
4.  We don’t worry about keeping OCLC up-to-date.
5.  Other

  
I would appreciate it if you could take a moment to respond to my little survey!

Libraries in Illinois have been using a company (TMQ) to do option 3. That company is no longer going to be doing this for us so we are looking for other solutions. We have found that option 2 is not entirely accurate due to items that are moved from one record to another not being counted as new. Let me know if you have any other suggestions!

&nbsp;

Thank you,

Kathy Schmidt

Member Services Librarian, Technical Services

847-483-8605 | www.ccslib.org

3355-J N Arlington Heights Rd, Arlington Heights, IL 60004

  
<br/>

asked 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_447fe7073eba436a943b7450eb642c4c.jpg"/> kschmidt@ccslib.org

[oclc](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=195#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 4 Replies   last reply 5 years ago

<a id="upVoteAns_518"></a>[](#)

0

<a id="downVoteAns_518"></a>[](#)

At present, we are only adding our holdings to OCLC for Books and only those in specific collections (i.e. collections from which we are willing to lend via ILL--for instance we will not lend board books via ILL so we don't add holdings to OCLC for board books).  We began adding our holdings a few years ago.  I pulled the bibs into a record set for the collections we wanted to add and overtime, I or a staff member added the holdings.  We were allowed an additional staff member in technical processing a bit over a year ago and I was sure this would keep her busy until the end of time.  She completed the work in less than 6 months.  It isn't interesting work but it can be done.  Note, if there is no OCLC number in the record than holdings were not added.  At present we are not worrying about these as many of these will attrition out of the system at some point.

Now at the beginning of the month I pull a report that shows all the bib records that were withdrawn from the collection--only the control number and OCLC number if present in the record are reported on this report.  It is usually about 15-20 pages long and the same employee that added all those holdings now deletes our holdings in OCLC.  It takes less than a morning.  The problem with this report is it does pick up all bib records removed from Polaris.  Thus if I am having to take out a large set of vendor records (rBdigital changes the URL on all records) I will come in on Saturday and delete those and then leave that date out of the report I run the first of the next month by running a report thru the Friday before and then a second report beginning the Monday after I have taken out the large set of records.  Since no one else is here to delete items then I know only the records I take out are the only records taken out that day. 

To add the holdings for new items, the first business day of the month, I pull a record set together of all bibs with a first available date within the previous month.  The employee than copies and pastes the OCLC number from the bib record into OCLC.  This is usually less than 500 records and typically the employee tasked to do this will have it done by the end of the day. 

This works for us because we are small to medium sized library.  If we were a much larger library system, something more automated would likely be a better option. 

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_447fe7073eba436a943b7450eb642c4c.jpg"/> janed@santarosa.fl.gov

- <a id="acceptAnswer_518"></a>[Accept as answer](#)

<a id="commentAns_518"></a>[Add a Comment](#)

<a id="upVoteAns_469"></a>[](#)

0

<a id="downVoteAns_469"></a>[](#)

Hi Kathy-

We're a cooperative and we do a variation of 1 & 2, using very similar workflows to the previous answers here.  When a last copy item is deleted, we keep the bibs in the system in deleted status.  Every couple weeks I export the deleted bibs using the Polaris export utility - limited to bibs deleted in a specific date range.  I run that exported bib file through MarcEdit to pull out the OCLC numbers (and separate out any records without OCLC numbers).  That list of OCLC numbers I convert into a text file, which I upload into OCLC Connexion's Batch Delete Holdings tool.  Records without OCLC numbers are checked manually.  I've tried using the OCLC Data Sync service to have the holdings updated but I prefer the instant gratification of running the files through Connexion.  It can handle a couple thousand records at a time, which is usually all I'm updating at once.  And it immediately reports if there were any errors to follow up on. 

As far as adding holdings -- Catalogers attach holdings for new titles in OCLC Connexion as they go.  Vendors who do contract cataloging add holdings in batches every couple days.  Recently I've been experimenting with following up and doing a batch update to add holdings, similar to how I delete them, on the same schedule.  So far it's been working pretty well. The majority of records already have holdings set, but the process does catch a couple dozen every time that don't.  For that, I create record sets by bib creation date, then use SimplyReports to export the record sets and pull the OCLC numbers.  After that, I use the batch update holdings tool in Connexion to update holdings. 

We don't maintain holdings for our ebooks. 

Hope this helps!

Amy Mihelich

Cataloging Librarian,  Washington County Cooperative Library Services  

[amihelich@wccls.org](mailto:amihelich@wccls.org) | [www.wccls.org](http://www.wccls.org/)

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_447fe7073eba436a943b7450eb642c4c.jpg"/> amihelich@wccls.org

- <a id="acceptAnswer_469"></a>[Accept as answer](#)

<a id="commentAns_469"></a>[Add a Comment](#)

<a id="upVoteAns_468"></a>[](#)

0

<a id="downVoteAns_468"></a>[](#)

Hi Kathy.

We do a combination of 1 and 2, with maybe a bit of 5. ![smile](../_resources/smiley-smile_863bf0d042644375afa28be664d466b4.gif)

Titles cataloged in-house have holdings updated in Connexion at the time of cataloging.

Titles received from some of our vendors (Baker & Taylor, Midwest Tape) have their holdings set by the vendor via a batch process on their end or via a data sync collection in OCLC.

Titles we receive from ebook vendors are set on a monthly basis. I have a custom report to pull OCLC numbers from e-resource bibs added the previous month and I use Connexion to batch-set holdings. I also pull OCLC numbers from "fully cataloged" e-resource bibs that were deleted in the previous month and batch-unset via Connexion.

At this time, we are not setting holdings where the bib record for an e-resource does not have an OCLC number. I have done a cleanup project to set holdings on these titles, but it is a long and complicated process. I may revisit it sometime in the future.

When preparing to delete non-e titles, I use a couple of reports to pull OCLC numbers from the bibs and use Connexion to batch-unset holdings. Any bibs without OCLC numbers or any that Connexion says were not held are manually checked by looking up the bib in Polaris, searching for the title in Connexion (ISBN, UPC or Title and Author) and manually unsetting holdings there.

Once that is done, the bibs are deleted.

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_447fe7073eba436a943b7450eb642c4c.jpg"/> jwhitfield@aclib.us

- <a id="acceptAnswer_468"></a>[Accept as answer](#)

<a id="commentAns_468"></a>[Add a Comment](#)

<a id="upVoteAns_467"></a>[](#)

0

<a id="downVoteAns_467"></a>[](#)

Hi Kathy,

I do option #1. We use the cataloging partnership services through OCLC which uploads most of our holdings at time of ordering. Otherwise, we update holdings when we download bibs. Additionally, I delete batches of bib records periodically and I created a report in Simply Reports to capture the OCLC number. I batch enter these into OCLC delete our holdings.

Erin Shield

North Olympic Library System

eshield@nols.org

answered 5 years ago by <img width="18" height="18" src="../_resources/default-avatar_447fe7073eba436a943b7450eb642c4c.jpg"/> eshield@nols.org

- <a id="acceptAnswer_467"></a>[Accept as answer](#)

Ditto!

— sbills@lpld.lib.in.us 5 years ago

<a id="commentAns_467"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [Has anyone used the OCLC Streamlined Holdings service? Are you going to?](https://iii.rightanswers.com/portal/controller/view/discussion/1176?)
- [Does anyone who departed OCLC for skyriver still update their holdings in OCLC for ILL via Worldcat, and if so how?](https://iii.rightanswers.com/portal/controller/view/discussion/868?)
- [Is anyone doing batch searching to the OCLC WorldCat Metadata API?](https://iii.rightanswers.com/portal/controller/view/discussion/1252?)
- [Finding duplicate 035s, 050 etc.](https://iii.rightanswers.com/portal/controller/view/discussion/500?)
- [Did upgrading to 6.3 bring a change to the way OCLC numbers are displayed in the 035?](https://iii.rightanswers.com/portal/controller/view/discussion/557?)

### Related Solutions

- [Export OCLC Control Numbers from Polaris to update WorldCat Holdings using Simply Reports – Record Set Method](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181128101912576)
- [Export OCLC Control Numbers from Polaris to update WorldCat Holdings using Simply Reports – Added Record Query Method](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=181128103351248)
- [Does Polaris integrate with OCLC Connexion Gateway?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=220622160839150)
- [OCLC number reaching 1 billion](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=170717105633952)
- [MARC 049 tag validation](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930325894633)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)