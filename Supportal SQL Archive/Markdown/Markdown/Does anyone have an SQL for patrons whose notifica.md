[Skip to content](#mainDiv)

[![logo](https://iii.rightanswers.com/portal/app/images/custom/8_Self%20Service%20-%20Polaris_20221120113730.png)Self Service](https://iii.rightanswers.com/portal/ss/)

 <a id="header-language-icon"></a>[](#)

- [Home](https://iii.rightanswers.com/portal/ss/)
- [Knowledgebase](https://iii.rightanswers.com/portal/ss/?tabname=knowledgebase&sl=&linw=)
- [Communities](https://iii.rightanswers.com/portal/ss/?tabname=communities&sl=&linw=)

![](https://iii.rightanswers.com/portal/app/images/custom/iii/PolarisForum-Icon_75px.png)

[Polaris Forum](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4) › [Patron Services and Circulation](https://iii.rightanswers.com/portal/ss/?tabname=community&communityID=4&topicID=15)

# Does anyone have an SQL for patrons whose notifications settings are listed as "none"?

0

122 views

I'm looking to find patrons where there notification settings in their registration is set to none.  Since SR makes me choose a notification option it's not exactly what I'm looking for? 

asked 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_30b2fe17e4aa4a4099ac2d6709fa5233.jpg"/> lori@esrl.org

[notification setting](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=302#t=commResults) [patron registration](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=238#t=commResults) [sql](https://iii.rightanswers.com/portal/ss/?tabid=2&tags=81#t=commResults)

<a id="comment"></a>[Add a Comment](#)

## 3 Replies   last reply 4 years ago

<a id="upVoteAns_745"></a>[](#)

0

<a id="downVoteAns_745"></a>[](#)

Perfect.  Thank you all for the help. 

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_30b2fe17e4aa4a4099ac2d6709fa5233.jpg"/> lori@esrl.org

- <a id="acceptAnswer_745"></a>[Accept as answer](#)

<a id="commentAns_745"></a>[Add a Comment](#)

<a id="upVoteAns_744"></a>[](#)

0

<a id="downVoteAns_744"></a>[](#)

<a id="tabEventDetails"></a>The following SQL can be run in the staff client (F7) for a list of accounts with the notification option set to none:  
<br/>select patronid from patronregistration pr (nolock)  
where DeliveryOptionID is NULL

Anne Krulik

Burlington County Library System

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_30b2fe17e4aa4a4099ac2d6709fa5233.jpg"/> akrulik@bcls.lib.nj.us

- <a id="acceptAnswer_744"></a>[Accept as answer](#)

<a id="commentAns_744"></a>[Add a Comment](#)

<a id="upVoteAns_743"></a>[](#)

0

<a id="downVoteAns_743"></a>[](#)

This should work in the client and/or LEAP, when searching PatronRegistration (or PatronStatus):

SELECT pr.patronID  
FROM PatronRegistration pr (NOLOCK)  
WHERE pr.DeliveryOptionID IS NULL  
AND pr.ExpirationDate > getdate()

If that's not what you're looking for, please let me know.

answered 4 years ago by <img width="18" height="18" src="../_resources/default-avatar_30b2fe17e4aa4a4099ac2d6709fa5233.jpg"/> jjack@aclib.us

- <a id="acceptAnswer_743"></a>[Accept as answer](#)

<a id="commentAns_743"></a>[Add a Comment](#)

## Your Reply

Attach file...

### Similar Questions

- [T-Mobile patrons not receiving text notifications](https://iii.rightanswers.com/portal/controller/view/discussion/982?)
- [Is there a way to search for patrons by what they have their notification setting as? i.e. is there a way to find any patrons that were accidentally set as fax instead of text?](https://iii.rightanswers.com/portal/controller/view/discussion/1198?)
- [Does anyone have a SQL report that will show renewal vs new cards for a particular patron code?](https://iii.rightanswers.com/portal/controller/view/discussion/1377?)
- [I have more and more patrons that are using contract cell phone services, but they want to get text messages from us. I have not found a way to update the cell phone provider list. Does anyone know how to do this?](https://iii.rightanswers.com/portal/controller/view/discussion/261?)
- [Does anyone know how to batch update the patron code field?](https://iii.rightanswers.com/portal/controller/view/discussion/886?)

### Related Solutions

- [Patron Status does not show a reminder that is in the NotificationHistory table](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930782414146)
- [Does blocking in SIP follow the "patron initiated circulation" settings?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930374822804)
- [Would like a way to gather all patrons with no notification option selected](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930285364780)
- [Finding patron specific information from the PolarisTransactions database](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930851383961)
- [How do I find a deleted patron in SQL?](https://iii.rightanswers.com/portal/app/portlets/results/viewsolution.jsp?solutionid=160930727145951)

### Email Notifications

Subscribe or unsubscribe to email notifications for this:

- Community
- Topic
- Discussion

[Manage Email Notifications](https://iii.rightanswers.com/portal/controller/route/mysubscriptions/view)

Upland RightAnswers | Self Service - 2022R1.2  
© 2024 Upland Software, Inc. All Rights Reserved

[](#)