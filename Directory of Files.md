# Directory of Files

## Folder - Admin

The queries in the Admin folder are useful for system administration tasks, updating and gathering information in ways that may be time consuming to do through traditional SA in the Staff Client.

* **Add Closed Dates** - Instead of inputting date after date after date into the Closed Dates dialogue in Polaris SA, this query allows you to put in an entire date range for a given organisation. Keep in mind, closing a system closes all libraries beneath it and closing a library closes all branches within it. But you can do branch by branch if needed.

* **Add Dashes to XXXXXXXXXX Formatted Phone Numbers** - We had a situation where a library had most of their phone numbers formatted as 4805551212 rather than the easier to read 480-555-1212. This query goes through your database and looks for numbers in the former format and reformats them to the latter format. It can be adjusted to suit a variety of similar issues.

* **Adjust Due Dates** - !!! CAUTION !!! This query updates the ItemCheckouts table en masse to adjust due dates. This cannot be undone. Use this query to adjust item due dates because of closures, emergencies, and so on.

* **Better Transaction Details** - When you pull data from the TransactionDetails table, you get a bunch of numeric codes that are FK relations to other tables in the Transactions and Polaris databases. Unless you've memorized all these numbers and what they mean, it's hard to know what you're looking at. This query allows you to throw a TransactionID into a variable, run that against TransactionDetails, and get human readable information on that transaction. In the case of a numValue that corresponds to a TransactionSubTypeCode, this query will also pull the SubTypeCode description.

* **Build Record Set for Additional Text Message Changes** - If you're having issues with a particular mobile carrier, you may want to temporarily switch patrons using that carrier over to email rather than relying on text messages. This query pulls a list of patrons using specified carriers, who are using the Additional Text message feature, and places the results in a given record set. You can offer that set to admin and execute a bulk change as needed to turn off additional text notifications.

* **Build Record Set for Text Message Changes** - If you're having issues with a particular mobile carrier, you may want to temporarily switch patrons using that carrier over to email rather than relying on text messages. This query pulls a list of patrons using specified carriers and places the results in a given record set. You can offer that set to admin and execute a bulk change as needed. 

* **Bulk Change Loan Limits** - I believe many of us already have our own versions of this code, but I was unable to successfully find a sample of it on the IUG Polaris SQL forum. I thought I'd share it here if it may make some bulk changes a bit easier for the newer SQL admins.

  The code attached allows you bulk change loan limits (max items out and max holds) for your organization without having to go line by line. *Our example will not run directly in your environment but it can act as a plug'n'play template.*

  *Contributed by: Derek Brown - Rochester Hills Public Library*

* **Check Telephony Server** - This is a two in one query. To run only one part, highlight the specific code and then execute the query. This query checks for activity on your telephony notifications. It's a quick way to make sure your telephony notifications are going out and that the server is working. The first query pulls the current notification queue to look for backups and the second one pulls the notification log to check for successful calls.

* **Check for Auto Renewal on Given Date** - This query checks to see if automatic renewals happened or not. Simply put, if you don't get any output, then chances are good that the auto-renewal database job didn't work properly.

* **Close the Library Every Sunday** - We have a library that wants to be closed every Sunday via the Dates Closed table. This query is useful when you need to close a library every Sunday (or some other day of the week) but you don't want to add each, date, by, hand. (Which is what you need to do in SA.) This query will pull a list of Sundays, throw those into a temporary table, then use that table to populate Polaris.Polaris.DatesClosed and Polaris.Polaris.NSDatesNotToCall.

* **Copy Collection Assignments from One Branch to Another** - When you create a new branch in Polaris, it doesn't assign it any collections at all. Going through all the individual collections can be time consuming, but you can save a lot of time by just copying over the assigned collections from one branch to your new branch. This query does exactly that, it pulls the collections assigned to a given branch and then assigns them to your new branch.

* **Count PAC Searches** - This query pulls the number of searches on your PowerPACs via the Transactions database through your TransactionDetails. It presents the counts, per month, along with the library/branch associated with the search.
  Thank you so much to: *Trevor Diamond, Morris Automated Information Network (MAIN)!*

* **Delete Import Jobs Older Than 90 Days** - This query looks for import jobs older than 90 days and then passes them on to the Cat_DeleteImportJob sproc for processing. In short, it deletes import jobs older than 90 days.  
  
  Contributed by: Kelly Hock - CLCOhio
  
* **Find Dormant Polaris Users** - This query looks for Polaris users who haven’t logged in for a given amount of time. It helps you find users who no longer work for the library, but their account still exists in Polaris.
  
* **Find Old Record Sets for Deletion** - This is a query specifically designed for the Polaris ILS Find Tool in the Staff Client and in Leap. Running this in the Record Set Find Tool will return a list of record sets that haven't been updated within a given amount of time. In other words, it'll help you find old record sets to weed out and delete. In the query, it's set to find record sets that were created three months ago and never modified or record sets that haven't been modified in the last three months. Change the -3 to another number to modify the timespan.
  
* **Get a List of Leap Logins** - This query pulls a list of logins to Polaris Leap and provides you with the username, the organization they logged into, the name of the computer they logged in on, and the datetime of the login.

* **Get a List and Schedule of SQL Server Jobs** - This query pulls a list of SQL jobs set up in SQL Server along with all the information connected to them like frequency, start time, end time, intervals and so on. It's particularly useful if you need to back up, clone, or reset a database system.

* **Get a List of SIP Ports by Branch** - A simple query that pulls a list of SIP ports (TCP/IP), and gives that to you with the branch name and organization ID.

* **Get FineIDs Based on Organization and Patron Codes** - This query is useful when adjust the Fines table through SQL as you'll be able to pull a list of FineIDs based upon FineCodeIDs, PatronCodeID, and OrganizationIDs. This gets you a list of *specific* FineIDs to target with your query.

* **Get Permissions for Staff Member** - This is nothing more than a modification of the sproc that powers the Staff Member Permissions report. The problem with that report is, if you need to get permissions for several staff members, you're going to be doing a lot of clicking and saving running the same report over and over again. With this query, you can set initial parameters and then simply change the PolarisUserIDs as needed to get your lists. It's much faster.

* **Item Availability Display Table Query** - This query produces a readable output of your system's Item Availability Display table.

* **Last Time Users Logged Into Polaris** - *Note: This query came from an IUG 2021 Annual Conference forum. My notes are incomplete so I'm not sure who contributed this to the group. If this is your code, feel free to contact me and I'll credit you properly!* This query pulls a  list of Polaris users and the last time they logged into the system. It's great for auditing your users list and making sure that users who are no longer with your library are cleaned out of the system.

* **List Collections Enabled at Branches and Libraries** - This query provides a simple output to show which Collections are enabled at all or given branches.

* **List Polaris Users on System** - Pull a list of Polaris suers on the system. This filters out deleted users. Results include Polaris User ID, User Name, Library/Branch, Enabled bit, and Last Login date.

* **Loan Periods - Get Full Info** - While you can get the list of Loan Periods from your Polaris system administration and management tools in the Staff Client, sometimes it's useful to get the whole list in one place, so you can export it to a CSV or Excel file. This query pulls all the relevant information for Loan Periods so you can easily see what's going on.

* **Move Items to Resting Branch for Quarantine** - This is a snip of our code that looks through existing material in the "wild" and moves it to a different branch location. We do this since our main library branch leverages an automated material returns machine and we wanted the material to come off of the patrons record when it was returned.

  This runs every hour...

  This moves material checked out to our patrons to a specific "resting" branch that the automation machine is also set to. When an item comes across the belt it checks the item "in" and lives in that "resting" branch for a set period of time. Items on hold or that do not fit the standard category are put into the exceptions bin and are handled manually by our staff.

  SEE Remove Items from Resting Branch in Quarantine.sql FOR FOLLOW UP

  *Contributed by: Derek Brown - Rochester Hills Public Library*

* **Notification Settings at System-Library-Branch Levels** - Jason Tenter (Library System Administrator) from Saskatchewan Information and Library Services shared with us a fantastic report on hunting down their notification settings. "... a report on notification settings at the system, library, and branch-level (if the branch's settings don't match the library). I think the code should be ready for any environment...

  It's missing details about some notice types we don't use, like fines and serial claims, but as a consortium dealing with re-openings it's been really handy to report on differences between library branches and their parent orgs..."

* **Remove Items from Resting Branch in Quarantine** - We then follow up with a removal of the quarantine once a day that will take the items out of the "resting" branch and move them back to the standard workflow for our library (based on our time allotted to quarantine).

  *Contributed by: Derek Brown - Rochester Hills Public Library*
  
* **Resetting Due Dates Based on Current Due Date** - This query will help you adjust due dates en masse, especially if a Reset Due Date operation has gone wrong. The query pulls in needed information and drops it into a temporary table that can be used to calculate the proper due date based off the most recent checkout date in Polaris.

* **SimplyReports Security Audit** - This is a quick script to check for Polaris users with elevated access to SimplyReports. Modify the the WHERE parameter as needed, but I typically don't give the standard users access to admin, patron and item history, and elevated metadata privileges.

* **Set Holiday Closures** - This query has a bunch of holidays set up for updating the Closed Dates and the Dates Not to Call tables. It's often quicker to add your holiday closures like this rather than one-by-one through the Staff Client. Set the dates as needed and remove those you don't need. (Or comment out the INSERTs you don't want.)

* **Update ItemAvailabilityDisplay for Multiple Organizations** - This query inserts a new branch into the ItemAvailabilityDisplay table and sets the Availability Level and the Sort Order for the branch within the table. Manual adjustment in Polaris SA may be necessary after running this query, but this query circumvents the tedious entry and set up of the new branch in all of the System, Library, and Branch Item Availability Display tables in Polaris SA.

  This query will need to be run twice: First with the @IALevelID set to 1 and again with the @IALevelID set to 2. Sort orders can be changed between queries as well.

* **Update Patron Phone Numbers for Long Distance Changes** - !! BE VERY CAREFUL WITH THIS ONE !! This query, developed with Gabrielle Gosselin (Richland Library: Columbia, South Carolina), is used to adjust patron phone numbers in preparation for changing settings on the telephony server. In this case, a library was in an area that switched to 10 digit calling and the system is such that it's better to put a 1 before *all* telephone numbers, including local ones. I was setting the telephony server to dial 9,1, before all calls, but there was a snag. Some patron phone numbers were already in Polaris with a 1 in front, which  meant the system would dial 9,1,1... and after that, the phone number wouldn't matter. With Gabrielle's help (by which I mean she did most of the work), we came up with a query to pull all of the phone numbers with a 1 already in the prefix, and then remove that 1 before writing the updated number back to the database. I recommend snapshotting your patron registration data before running this query, and try it out first on a training server. You may need to adjust things to suit your data.

  You will also need to run this query three times. Once for PhoneVoice1 and again for PhoneVoice2 and a third time for PhoneVoice3.

  Big thanks again to Gabrielle! 

## Folder - Bibliographic Records

These queries will help you gather information about your bibliographic and MARC records.

* **Locate Bibs with Tags and Specific Data** - This query is for the Find Tool. It locates bibs with tags with specific data. From there you can easily create a record set and use the native bulk bib change to update tags. I hope this helps somebody!

  *Contributed by: Heather Arnold - Municipal Library Consortium*

## Folder - Circulation

The queries in the Circulation folder will help you get circulation (check out, check in, etc) stats and information.

* **Aggregated Hourly Circ Counts by Day of the Week** - If you need to know your busiest circulation hours for the week, then this query will help. It pulls circulation data for a set timeperiod and then breaks that down for every hour of every day of the week, aggregated over the time period. So if you run this query for a two month time span, you'll find out  what your circ was like on given hours, on given days, over that timespan.

* **Average Loan Time by Collection** - This was a contribution from someone but damned if I can figure out who. If this is your code, let me know! It pulls the average loan time by collection - so you're going to get the average number of days items in that collection stay checked out.

* **Circ by Patrons Acting Outside Their Assigned Branch** - This query looks at the checkout activity for patrons acting outside their assigned branches. In other words a patron from Branch A goes and checks out from Branch B, that'd be counted in this query. We exclude the patron's branch as the transacting branch and then see where their other checkouts are happening.

* **Circ Count by Material Type or Collection** - This query pulls circulation (check outs) for a given library or set of libraries. You can limit the circ stats to a set of given Collections or a set of Material Types.

* **Circ Count By Patron Statistical Class** - This query produces a circ count for patron statistical classes along with the transacting branch and the patrons’ assigned branch. The original request (on the IUG Discord) called for UDF 4 to appear in the results but this can be modified/removed if needed.

* **Circ Count by Shelf Location** - This query will pull circ stats (check outs) for items with a given set of shelf location codes. Because this query pulls from the Transactions database, it's historical in nature. Even if the shelf location has changed, it'll come up in this query based on what that shelf location was at the time.

* **Circ Count for Every Item in a Library's Collection** - Sometimes, you actually need to know the circ stats for every single item in the collection. That's exactly what this query does. Note: This query can take a lot of time to run. It's not database intensive, but I recommend running it in the evening after the library has closed.

* **Circ Stats by Items in a Record Set** - This query will pull circulation stats, checkouts and renewals, for items contained in a given item record set. Useful for tracking circ stats on displays, special collections, etc.

* **Circ Stats - Distinct Patron Checkouts - By Shelf Location** - This query pulls stats for *distinct* patron check outs based on Shelf Location. In other words, it's checking for stats where a patron checked out items in a given collection, and only counts that patron once.

* **Get Basic Circulation Totals by Age Range** - This came up as a statistical question - How many items were checked out within a certain timeframe by people within certain age ranges? You can modify the TransactionTypeID to suit other, similar questions too. Or fiddle about with the age ranges to get what you need there.

* **ILL Activity for a Given Timespan** - A library requested a report providing the overall ILL activity for a given time, specifically what was happening, when, where, with whom, and the items involved. This query pulls ILL activity from the TransactionsDatabase based upon usage of the Polaris ILL system. If you're not using that, then this query won't help you too much!

* **List All Items Checked Out Between Two Dates** - This query pulls a list of items checked out between two given dates, including renewals. It returns the transacting library, the PatronID of whoever checked out the item, the ItemRecordID, the transaction date, the material type, the item's price, and the item's browse title.

* List of Items Checked In Through In-House Check In - This query will pull a list of items that have been checked in via in-house check in between two given dates. Along with branch, title, and author information, the subjects for each item are also included in a single column within the results.

* **PowerPAC Circ Count by eContent Vendor** - If you've integrated your Polaris system with an eContent vendor, then patrons will be able to check out eContent items via the PAC. We needed to get a circ count for items borrowed on the PAC and this was the result.

* **Self Check Circulation Counts** - If you need to get self-check circ stats, but you don't know the WorkstationID of your self-check machines, you can use this query to pull circ totals for each branch by specifically looking for the TransactionSubTypeCodes that indicate self-check activity.

* **Unique Patron Circulation Within Collections** - This query was requested on the IUG Discord and supplied by Matt Hammermeister at Pinnacle (IL). It produces a report of checkouts for unique patrons based on collection. In other words, how many distinct patrons account for circulation in Adult Fiction, or in Children's Non-Fiction?
  *Big thanks to Matt for this query!*

* **You Saved Amounts - Minus Renewals** - This query pulls the prices for items checked out at given branches in order to get You Saved data for those branches. However, this query excludes the renewals for those items to get only the initial You Saved data rather than bringing in prices for items renewed.

  Note: Since the pricing data is pulled CircItemDetails, if an item is deleted, you won’t get that pricing information.

## Folder - Holds

Get stats, information, and more on requests.

* **Active Holds Where Items are Checked In** - This query finds active holds that are sitting on shelf. Useful for finding items that fell of the RTF list. t gives you the owning and assigned branches to help you find the item in one place or another.

* **Average Days Held by Collection** - This query offers a look at the average number of days items within given collections are held.

* **Average Days Held by Material Type** - This was a contribution from another source but I'm not sure who. If this is your code, let me know! It delivers the average number of days items are held per material type.

* **Average Number of Patrons Waiting On Holds Per Week For a Given BibID** - This query takes a given BibliographicRecordID and produces an average number of holds created per week to let you know how many people, on average, are waiting in the request queue for that bib record.

  You're going to run into a couple of issues with this kind of query. The first is churn. Because a hold will be placed and another hold is filled and another hold is placed and so on. It's hard to know how many are on the list at any given time because, who knows? Maybe three people placed a hold that day and two got their holds filled. So at the start of the day, three people were waiting but by the end, only one person was. So I'm going to track only the number of holds created, but you could modify this to take other actions into account.

  Second, you run into a Scottish coastline problem as to how far down to you want to go for your average? I figured a week might be good? I dunno, seemed right. But this could be modified to suit for a month or something other.

  You get two things at the end of the query:

  A lifetime average of holds per week since the bib record was created.

  A week by week count of holds since the bib record was created.

* **Determine How Long Items Sit on Hold Pickup Shelves** - This query pulls a list of items that were placed on hold and subsequently became held. Then it pulls a list of those held items that were actually checked out. Finally, it compares the dates of those two events to get you a count of how long items stayed on the hold pickup shelves.

  Modified from a post on the Supportal - Original SQL by jjack@aclib.us

* **Find Active Holds on Lost, Claimed, and Missing Items** - This query will find active holds on Lost, Claimed, and Missing items.

  Contributed by: Derek Brown - Rochester Hills Public Library

* **Find Bib Records With Active Holds But No Items** - This query finds Bib Records with active holds that do not have items (excluding on order)

  Contributed by: Derek Brown - Rochester Hills Public Library

* **Find Hold Information from Given Dates** - Using this query, you can get detailed information about hold acvtivity for a given library/branch on a given range of dates. The codes for hold requests are noted at the bottom of the code.

* **Get Holds Stats for Collection Code and-or Material Type** -This query is useful to see which collections or material types are getting the most holds. You can modify as needed to search for only collections, only material types, both, or a combination. 

* **Get List of Branches Excluded for Hold Pickup** - This is a quick and dirty query that pulls a list of branches that have hold pickup exclusions assigned to them.

* **Get Number of Holds Created for Bibs in a Record Set** - If you need to get some holds statistics for bibliographic records in a bib record set, this query should help you out. You can adjust the TransactionTypeID to look for other holds/circ actviity as well.

* **Hindered Holds** - This query looks for holds with problems. Maybe the item isn't supplied, or it's a hold that's several months old, or there are no items to fill the hold, and so on. It pulls in the item and hold information so you can track down these issues and, hopefully, solve them.

  *Note: There's also an RDL file for this query in the repo. It can be opened* *in Microsoft Report Builder.*

  *Big thanks to: Trevor Diamond - MAIN (NJ)*

* **Hold Counts Patron Branch vs Pickup Branch** - This query pulls counts of holds placed at branches by patrons assigned to that branch and by patrons assigned to other branches. Useful for finding out how many patrons from other branches are using a given branch as a pickup branch.

* **Holds Data with Final Statuses** - This query pulls several facets of holds data, including when the  SysHoldRequestID, hold request was created, that patron involved, title, author, and item information, the pickup branch, and the final hold  status and date. *Note: There are two date parameters within this query and the date ranges should match in both of them.*

* **Hold History for Extant Holds** - This query pulls the hold activity histories for a given PatronID based upon the existing holds is the Polaris.Polaris.SysHoldRequests table. Hold activity isn’t well tracked in the PolarisTransactions database so this may be a better method of getting a report of hold activity so long as the hold hasn’t been filled  and/or removed from SysHoldRequests.

* **Hold Request History by Patron Barcode** - This generates a list of all the holds a patron has placed and what happened with them. NOTE: This query can take several minutes to run. It likely will not slow down the server but once you execute the query, give it some time to finish.

* **Pull Holds from Notifications Queue** - Your telephony server will fail at some point. And you might need to get the word out to your telephone notification patrons that they have holds. This query pulls  the patron and item information from your NotificationQueue, where all those calls are stacking up, and presents the data in a way that front line staff can use it to manually call the patrons.

* **Request and Hold History for Bib Record** - Maybe I missed it, but I couldn't find a great way to pull a complete history of hold activity for a given bibliographic record. So that's what this query does, feed it a bibliographic record ID and run it. It can take a few minutes to run depending on how many holds that record had over the course of time. You could always add specific TranClientDates and other limiters to speed things up as needed.

* **Unclaimed Holds With Patron Information** - This query pulls a list of holds that went unclaimed, along with the patron information attached to them.

## Folder - ILL

The queries in the ILL folder deal with Interlibrary Loans.

* **Last Month's ILL Report** - An ILL report requested by one of our libraries that I thought might be useful for other libraries. Pulls a bunch of relevant information about ILLs, the items, the patrons, and so on.

## Folder - Items

The queries in the Items folder deal with collecting or manipulating data on items within your Polaris catalogue.

* **Check Items in Record Set for Display in PAC** - This query checks the items in an item record set to see if they're set to Display in PAC. Additionally it checks the item's bib record to see if it's set to Display in PAC. It returns item record modification information as well as bib record modification info.

* **Circ Count for All Items Under Bib Record** - This query gets a circulation count for items under a given bibliographic record, and it makes sure to include items that may have been deleted. The first part of the query pulls all the item record IDs ever created under your given bib record, and then uses that to get a circ count for each item.

* **Collections Actually in Use at a Library** - Your libraries and branches might have access to dozens of collections, but they may not use all of them. This query will pull information about Collections actually in use at given branches and provide an item count for those collections.

* **Complete Item Circulation History** - This query produces a complete item circulation history based on an ItemRecordID. Basically, it pulls anything from theTransactions database with a TransactionTypeID between 6001 and 6999. It'll relay the item's browse title, the transaction type (check out, check in, charges, payments, etc), the patron name associated with the transaction, and the patron's barcode and PatronID.

* **Count of Items Created by User and Collection** - This query produces a count, by collection, of items created by the Polaris Users on your system. It's useful to see who's working in which collections the most often and tracking item creation statistics. The logic also takes into account the fact that some items created may be deleted by the time you run the query and it pulls those from the deleted items data.

* **Create Item Record Set and List for Deleting Withdrawn Items** - Typically, we delete withdrawn items after they've been withdrawn for a certain amount of time. Because I'm a paranoid kind of DBA, I want to keep a record of what was deleted and when... ya know, just in case. This query will pull withdrawn items based upon their last circ status change date and then dump those items into an item record set of your choosing. (Based upon a RecordSetID.) Then, after it does that, it'll present you with a result set with lots of columns that you can then save as a CSV to import into your own SQLite database to track what you deleted on the off chance that something comes up. I may be a little broken inside.

* **Deleted Items Count Between Two Dates** - This is just a quick query that'll get you a count of items deleted between two given dates.

* **Deleted Item Counts by Branch, Collection, and User** - So long as items are deleted through Polaris and not through a SQL query, this code will provide a count of items deleted by branch, collection, and user.

* **Examine Patron and Item Cross Borrowing** - This query is designed to pull counts of items circulated at given branches, specifically looking for items circulated at branches that are outside of the items' assigned branch. Used for various state reports, this query can get initial circ data and provide various information depending on what you need to pull from the temporary table #TempCircLendingData that's populated with item assigned branch, material type, transaction dates, transacting organization, TransactionTypeIDs, ItemRecordIDs, PatronIDs, and the specific TransactionIDs which can be used to hook into other data and result sets.

* **Find Items Denied Transit Without Branch Update** - *Submitted by Gabrielle Gosselin at Richland Library (Columbia, SC) - Thank you!* 
	The goal: find items where staff denied transit at CKI but didn't update the assigned branch to accurately reflect where the item physically was. The report the query will go in is intended for staff to run, sort by CKI Location column, find the ones listed for their branch, and go look on the shelves for them and if found, fix the item record.
	
* **Find Last Patrons That Checked Out a Now Deleted Item** - This query finds the last patrons to use an item that was deleted from the system. This is useful when you find yourself looking at an item that may remain on a patron's account as an active replacement charge.

* **Get Item History Based on ItemRecordID** - This query pulls a basic transaction history (from the Transactions database) for a given item based upon the ItemRecordID. While SimplyReports can produce a similar report, it does so based only upon a barcode. If the item has been deleted, then the barcode won't work. If you can get an ItemRecordID from the PatronAcctDeletedItemRecordstable, you can plug that into this query to get information about its history.

* **Get Item Record Set Data Based on Organization Own** - This query pulls item record information from item record sets based upon a given record set owner. So if you need to see the information in record sets owned by branch or library, this will get that for you.

* **Get Useful Information from Item Record History** - This query pulls the complete item history for a given item record ID. The data is pulled from the ItemRecordHistory table, so it may not be what you want for a full historical record as that table is often set to clear information after a given amount of time.

* **Item Overdue Durations** - This query pulls a list of items that were checked in overdue and reports each item's overdue duration in days. You’ll likely need to adjust this to suit your own needs and, as a big warning, this query is geared to work with items set to have loan periods measured in Days. If you’re going to look at items with loan periods measured in Minutes or Hours, you’ll definitely need to tweak the settings. The overdue duration is based upon a TransactionSubType within a 6002 TransactionType, which is check in. There’s a SubType in there that defines the loan period in minutes, so there’s a bit of maths going from  minutes to days. The check out date is derived from subtracting the number of minutes from the check in date. I dropped out the minutes and seconds, but you can always unCAST them to get those back.

* **Item Records in Smaller Collections** - This query looks for low number counts of items in your collections, the idea being that you might have items miscatalogued or mis-assigned to given collections that may not be used at a particular branch or location. It then produces a list of items in those collections so you can identify any problems.

* **Items Charged as Replacement With a Circulating Status** - If a patron has a lost item, and they've paid a portion of the replacement fee, but then they hand it in? Polaris will not automatically wave the fines and fees. The item can begin circulating as normal but the patron remains on the hook for the fines. This query looks for items that have been charged to a patron's account, yet they have a status that suggests they're circulating. Check the results and see if you need to waive some fines on a patron's account.

* **Items Checked Out and Declared Lost at Branch** - I had a request to create a report of items checked out at a given branch that were later declared lost by the same branch. This query pulls in a list of items checked out and then compares them to a list of items declared lost.

* **Items Replaced on Existing Bib Records** - This query looks for items added to existing bibliographic records in order to see if cataloguers are creating new bib records for newer items or if they’re adding the items to the extant bibs that are already in the system. This may need some tweaking as our Polaris ILS Admins typically handle the deletion of withdrawn items after a certain time, so the PolarisUsersIDs for our Admins are called out in the query. You’ll likely need to adjust those PolarisUserIDs to reflect whoever does item deletions within your own system.

  Three results are provided:

  1. A list of items added to extant bibs along with when the last time was deleted and when the most recent item was added.
  2. The total number of distinct bibs with new items added to them.
  3. The total number of distinct bibs with items deleted from them and the total number of item records deleted

  The results are collected across a given timespan.

* **Item Statistical Codes In Use at Libraries and Branches** - This simple query pulls a list of item statistical codes in use at given libraries or throughout the library system.

* **Items With Anomalous Collection Codes and Material Types** - This query looks for collections and material types with low item counts, which could indicate that the items have the wrong material type and/or collection designations in the item record. Using this query, you'll get an entire list, all at once, that can be handed to someone to take out to the stacks to verify reality and see if any items need a correction.

* **Low Item Counts by Collection and Material Type** - This query first pulls a table of low item counts based upon collection and material type. From that it'll build a list of actual items that fall within the low item count table. This query is useful for finding items that may have the wrong collection and/or material type and the item record may need adjustment.

* **Material Types Actually in Use at a Library** - A library might have dozens of material types, but that doesn't mean a branch or a library is using all of them. This query pulls a list of material types actually in use at a branch and presents them with an item count. 

* **Material Type Limits with Realistic Data** - Pull Material Type Loan Limit table and replicate the layout you see in Polaris SA. Output is limited to Patron Codes and Material Types actually used by the library.

## Folder - Patron Account

These queries will get you fines and fees information relating to patron accounts, along with statistical information for patrons and their relation to payments, charges, and so on.

* **Count of Expired Patrons with Given Amount of Fines** - A common question for fine-free transitions is "How many expired patrons are blocked for fines?" This comes up because many expired patrons are expired because their cards are blocked for fines and they never came back to the library. Perhaps they will after the library eliminated overdues.
* **Get Billing Notices Info From Previous Days** - Sometimes the printed billing notices get ran, forgotten about, and then ran the next day which overwrites the PDF from the previous day(s). If you need to get the print billing notices from a previous day, this query will pull all you need to send the patron a snail mail notification.
* **Get Outstanding Fines Including Deleted Item Titles** - This query pulls a list of all outstanding fines for all patrons of a given library or libraries. That's not all that big of a deal, except this query also pulls browse title information from the deleted items table if a patron has a charge for a deleted item.
* **Newest or Oldest Fines and Fees** - Pulls the oldest or newest fine or fee in the system. I've been asked for this more than once during fine-free transition work.
* **Overdue Counts and Total Charges by Collection** - This query pulls the total overdue charges and the number of overdue charges by Collecton for a given library. This allows you to see where your most charged collections are, and the number of charges in that collection. Useful stats to have when working on fine free transitions.
* **Patron Account Summary with Counts** - This query pulls transaction summary information for a branch or library along with a given set of fee reasons, number of transactons, number of patrons, and the total amount for those transactions. Particularly useful when working on fine free projects.
* **Patron Address Information Based on Fine Data** - Another useful query for fine free proposals. This pulls patron barcode, total outstanding charges, patron code, patron branch, patron full name, patron address information, and primary email. We've used it for demographic data on patrons and their addresses to discover concentrations of patrons owing fines in a geographic location and see if any information can be derived from that.
* **Patrons Blocked by Birthdate and ZIP Code** - This query looks for patrons who are blocked for excessive fines, within ZIP codes, and by birthdate. This one is good for fine free transitions as you can get information about children and teens with blocked accounts and cross reference that to ZIP codes and their economic statuses.
* **Patrons Blocked for Overdues - Total Outstanding Balances** - This query is useful for fine free proposals. It gets patron overdue information along with activity information. This specifically looks at overdue fines and nothing else, though the query can be easily modified to do so. In addition, it pulls the total number of overdue fines on patron accounts and provides their last activity date.
* **Patrons Blocked for Overdues Alone** - Another useful query for fine free proposals. This query pulls a list of patrons who owe overdue fines and nothing else. In other words, they don't owe replacement, processing, lost card, or any other charges. It also includes a parameter so you can set a blocking threshold, showing how many of these patrons are blocked from using their accounts because of the overdues.
* **Patron Circ Activity by City** - This query will pull patron check out activity at a given location and provide you with a count of patrons in cities, states, and countries. At most, this is really targeted at getting a count of patrons checking out items and grouping the count by city, but since there are lots of cities with the same names, I threw in states and countries to make sure you get unambiguous results. I marked this as a patron report rather than a circ report because, in the end, we're counting patrons, not circs.
* **Patron Demographic Data and Fines Information with Minimum Limit** - Another useful query for fine-free transitions. This pulls a list of patrons by PatronID, Barcode, Branch/Library, Birthdate, Street Address, Postal Code, Last Activity Date, and total charges for a selectable Fee Reason. You can use it to create demographic information about patrons who owe fines.
* **Payment History and Totals** - This pulls total payment history, by fee reason, for libraries within a system. Because it's pulling from the Transactions database, it's a query that will get you historical information.
* **Total Fines Owed by Patrons by Fee Reason** - This rather lengthy query is a modification of a SimplyReports query. This one provides the total balances, transactions, and number of patrons with fines and fees and breaks them down by Fee Reason. Useful for fine free projects.

## Folder - Patrons

* **Bulk Update Patron Addresses Based on Given Address** - This query will bulk update multiple patron addresses based upon a "master address" and will apply to all patrons within a given patron record set. There are a few steps to this process:

  1. For all the patrons you want to update, put them into a record set and use its RecordSetID in the query. (Line 31) Also, note down the number of records within that record set.

  2. Create or alter a *single* patron record that remains *outside* of your patron record set. Give this patron the address you want all of the other patrons in the record set to have. Once you've done that, make a note of that patron's PatronID. For this example, we'll say that PatronID is 161803.

  3. In ADS or SSMS, run the following query, replacing the PatronID of 161803 with the PatronID of the patron you set up in Step 2.

  SELECT AddressID FROM Polaris.Polaris.PatronAddresses WITH (NOLOCK)
  WHERE PatronID = 161803

  4. Note down that AddressID number and use it in the query below. (Line 34)

  5. Make the changes needed below. For your saftey, use the BEGIN TRAN along with the COMMIT and
  ROLLBACK commands. When you run your initial query with BEGIN TRAN, make sure that the number of
  rows affected matches the number of records in your patron record set. (Step 1)

  6. Run the query and COMMIT or ROLLBACK as needed.

* **Complete Patron Circulation History** - This query pulls a full patron circulation activity history between two given dates based on a Patron ID. Everything from check outs to check ins, fines and fees, and holds will show up in the results as the query pulls all TransactionIDs between 6001 and 6999. This is an upgraded version of the Patron Check Out History query.

* **Duplicate Patrons - Add to Exisiting Record Set** - This query will populate a given record set with patrons that appear to be duplicates. It's looking for duplicated by names and birthdates. You'll see plenty of false positives, but it'll find some duplicates that Polaris might miss.

* **Duplicate Patrons with Email - Add to Existing Records Set** - This query will populate a given record set with patrons that appear to be duplicates. It's looking for duplicated by names, birthdates, and emails. You'll see plenty of false positives, but it'll find some duplicates that Polaris might miss.

* **Expired Patron Count by Year** - A simple little query that reports the number of patrons with expired cards in a given year. Years are presented with the count of patrons who expired in that year.

* **Get Patron Saved Title Lists with Current Items** - This query pulls all of a patron's Saved Title lists from their PowerPAC account and displays the current titles contained in each list.

* **List of Patron Emails Based on Expiration and Check Out Dates** - This query pulls a list of patron email addresses for an email service service like Constant Contact or MailChimp. To filter out long inactive patrons (and possibly bad email addresses), this query looks at expiration dates as well as the last time a patron checked out an item from the library. Dates can be manipulated as needed to get the list you're after.

* **Notification Log for a Specific Patron** - This pulls all of the notifications sent to a given PatronID no matter what the delivery method might be.

* **Patron Check Out History** - This query was written to prove a point. No matter how much you hide your item record and patron record history, Polaris always retains check out histories in the PolarisTransactions database. This is a basic query to pull all items checked out by a patron during a given period of time.

* **Patron Claims History** - So far as I know, there's no good way to pull a claims history through Polaris without using SQL. This query addressed a librarian's request that we find out what a patron claimed, when, how often claims occurred and more. Given the nature of the query it can take a little time to run, depending on the patron's activity, so set it up, fire it off, and be patient.

* **Patron Codes Actually In Use at a Library** - This query simply pulls a list of patron codes actually in use at a library or branch along with a count of the number of patrons in those codes. It's not all that unusual that a system may have a lot of codes but only a few are in use at a given branch and, while adjusting the system, you only want to act upon those codes actually in use.

* **Patron Code Limits with Realistic Data** - This query pulls the Patron Loan Limit table and replicate the layout you see in Polaris SA. However, the output is limited to only the Patron Codes the library actually uses.

* **Patron Notification History** - This query pulls the notification history for a patron between two given dates, based upon PatronID. The query returns the PatronID, patron barcode, patron name, notification datetime, reporting library, ItemRecordID, item title, notification type, monetary amount (if any), the delivery type, and notification status.

* **Patron Reading History with NULL ItemRecordIDs** - This is a messy little query that might be useful anyway. If an item has been deleted from the Polaris database, the PatronReadingHistory table will add the title and author so the data will continue to display, even though the item is gone. If you need to tie an item back to a possible, and logical, bibliographic record then this can help. The catch is, if this script can't find a bibliographic record to match an item to, it won't display the item in the result list.

* **Patron Reading History with Working ItemRecordIDs** - This query can be used as a basis for dealing with data in the PatronReadingHistory table. For instance, it could be modified to search for the reading history of a specific patron, sets of patrons, or to look for titles, etc. The original use of this file was to provide data as a CSV for import into another ILS as a library migrated away from Polaris.

* **Patrons Served by Age** - This query pulls counts for current-ish patron accounts that are unexpired or expired within the last three years, with the output delivering age groups. While you'll likely need to define your age groups differently, this could serve as a template for a similar query, or as a basis for another query by age groups.

* **Patrons with Blocking Notes** - Pull a list of patrons with blocking notes. Gives you the patron ID, barcode, name, note text, and last updated date.

* **Patrons with Non Blocking Notes** - Get a list of patrons with non-blocking notes. Pulls the patron ID, barcode, name, note text, and last updated date.

* **Pull Email List for Patrons Based on Circ Activity** - These queries work together to get email addresses for patrons who’ve checked out certain numbers of items over a given period of time. In my case we used this for an email marketing campaign where patrons who’d frequently used their cards got a different message than patrons who only used their cards occasionally.

  There are two queries here, one which complies patron circulation data to a table in your Polaris database and another than queries that table. You can eventually drop the patron circulation table later on if you like. It’s just a lot faster to have it sitting in your database rather than using the same data in a temp table over and over again.

* **Pull Patrons Last Checkout Date** - This is a statistical query that pulls a list of patrons along with their last checkout date. This allows for checking on circ based patron activity rather than relying on the Last Activity Date. Because the Last Activity Date can get updated for any number of reasons that have nothing to do with a circulation based transaction. You can limit the date to a given timeframe if needed or you can change the TransactionTypeID to something else if you wish to track that.