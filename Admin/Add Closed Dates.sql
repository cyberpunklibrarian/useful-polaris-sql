/* Instead of inputting date after date after date into the Closed Dates dialogue in Polaris SA, this
query allows you to put in an entire date range for a given organization. Keep in mind, closing a
system closes all libraries beneath it and closing a library closes all branches within it. But you can
do branch by branch if needed. */


-- Declare some variables
DECLARE @StartDate DATETIME;
DECLARE @EndDate DATETIME;
DECLARE @OrgID INT;

-- Set some dates
SET @StartDate = '20200508'; -- Set your first closed date here
SET @EndDate = '20200615'; -- Set your last closed date here

-- Bang in an OrganizationID
SET @OrgID = ''; -- Set your OrganizationID

-- Do spomething useful
DECLARE @DateEntry DATETIME;
SET @DateEntry = @StartDate

WHILE @DateEntry <= @EndDate

BEGIN

INSERT INTO
    Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@DateEntry)

SET @DateEntry = DATEADD(day,1,@DateEntry)

END