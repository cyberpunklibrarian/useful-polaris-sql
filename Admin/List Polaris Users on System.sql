/* Pull a list of Polaris suers on the system. This filters out deleted users. Results include Polaris User ID,
User Name, Library/Branch, Enabled bit, and Last Login date. */

SELECT
    th.PolarisUserID AS "Polaris User ID",
    pu.Name AS "User Name",
    o.Name AS "Library/Branch",
    pu.Enabled AS "Enabled",
    MAX(th.TranClientDate) AS "Last Login"

FROM
    PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK)

JOIN
    Polaris.Polaris.PolarisUsers pu WITH (NOLOCK) ON pu.PolarisUserID = th.PolarisUserID
JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK) ON o.OrganizationID = pu.OrganizationID

WHERE
    th.TransactionTypeID = 7200 --Login Code
AND
    pu.Name NOT LIKE '%Deleted%'

GROUP BY
    th.PolarisUserID,
    pu.Name,
    o.Name,
    pu.Enabled

ORDER BY
    "Last Login"