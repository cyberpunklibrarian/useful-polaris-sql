/* This query is useful when adjust the Fines table through SQL as you'll be
able to pull a list of FineIDs based upon FineCodeIDs, PatronCodeID, and
OrganizationIDs. This gets you a list of *specific* FineIDs to target with
your query. */

SELECT
    f.FinesID AS [Fines ID],
    f.OrganizationID AS [Organization ID],
    o.Name AS [Library],
    f.PatronCodeID AS [Patron Code ID],
    pc.Description AS [Patron Code],
    f.FIneCodeID AS [Fine Code ID],
    fc.Description AS [Fine Code Description],
    f.Amount AS [Amount],
    f.MaximumFine AS [Max Fine],
    f.GraceUnits AS [Grace Units]

FROM
    Polaris.Polaris.Fines f WITH (NOLOCK)

INNER JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK) ON o.OrganizationID = f.OrganizationID
INNER JOIN
    Polaris.Polaris.PatronCodes pc WITH (NOLOCK) ON pc.PatronCodeID = f.PatronCodeID
INNER JOIN
    Polaris.Polaris.FineCodes fc WITH (NOLOCK) ON fc.FineCodeID = f.FineCodeID

WHERE
    f.PatronCodeID IN () -- Place your list of PatronCodeIDs here
AND
    f.OrganizationID IN () -- Place your OrganizationID here
AND
    f.FineCodeID IN () -- Place your FineCodeID here