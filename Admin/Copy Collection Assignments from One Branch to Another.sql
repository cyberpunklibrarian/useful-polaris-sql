/* When you create a new branch in Polaris, it doesn't assign it any collections at all. Going through
all the individual collections can be time consuming, but you can save a lot of time by just copying
over the assigned collections from one branch to your new branch. This query does exactly that, it
pulls the collections assigned to a given branch and then assigns them to your new branch. */

-- "Let's maintain some decorum, okay?" ~Lewis Black
BEGIN TRAN

-- Set the OrganizationID of the branch you're copying FROM
DECLARE @OriginalBranchID INT = 105;

-- Set the OrganizationID of the branch you're copying TO
DECLARE @NewBranchID INT = 118;

-- Build a table we can use to populate the collections at the new branch
CREATE TABLE #TempCollectionPull (
    CollectionID INT,
    OrganizationID INT
);

-- Populate that table
INSERT INTO #TempCollectionPull

SELECT
    DISTINCT AssignedCollectionID,
    @NewBranchID
FROM
    Polaris.Polaris.CircItemRecords WITH (NOLOCK)
WHERE
    AssignedBranchID = @OriginalBranchID
AND
    AssignedCollectionID IS NOT NULL;

-- Now use the temp table to insert date into the OrganizationsCollections table
INSERT INTO
    Polaris.Polaris.OrganizationsCollections
SELECT
    OrganizationID,
    CollectionID
FROM
    #TempCollectionPull;

-- Tidy up
DROP TABLE #TempCollectionPull;

-- COMMIT;

-- ROLLBACK;