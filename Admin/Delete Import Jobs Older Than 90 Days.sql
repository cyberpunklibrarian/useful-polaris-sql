/* This query looks for import jobs older than 90 days and then passes them on to the
Cat_DeleteImportJob sproc for processing. In short, it deletes import jobs older than
90 days.  

Contributed by: Kelly Hock - CLCOhio

*/

DECLARE @jobid int

declare cur CURSOR LOCAL for
    select ij.ImportJobID 
    from Polaris.polaris.ImportJobs ij
    where ij.ImportJobSubmittedDate < dateadd(day, -90, getdate())

open cur
fetch next from cur into @jobid
while @@FETCH_STATUS = 0 BEGIN
    exec Polaris.Polaris.Cat_DeleteImportJob @jobid
    fetch next from cur into @jobid
END

close cur
deallocate cur
select 'done' [status]