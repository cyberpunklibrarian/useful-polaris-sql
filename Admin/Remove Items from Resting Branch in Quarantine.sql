/* 
We then follow up with a removal of the quarantine once a day that will take the items out of the "resting"
branch and move them back to the standard workflow for our library (based on our time allotted to quarantine).

Contributed by: Derek Brown - Rochester Hills Public Library
*/

UPDATE
    Polaris.Polaris.CircItemRecords

-- Move Items from RESTING branch to MAIN
SET
    AssignedBranchID = '3',
    ShelfLocationID = sl2.ShelfLocationID,
    StatisticalCodeID = sc2.StatisticalCodeID

FROM
    Polaris.Polaris.CircItemRecords AS [cir] WITH (NOLOCK)
LEFT OUTER JOIN
    Polaris.Polaris.ShelfLocations AS [sl1] WITH (NOLOCK) ON cir.AssignedBranchID = sl1.OrganizationID AND cir.ShelfLocationID = sl1.ShelfLocationID
LEFT OUTER JOIN
    Polaris.Polaris.ShelfLocations AS [sl2] WITH (NOLOCK) ON sl1.Description = sl2.Description AND sl2.OrganizationID = '3'
LEFT OUTER JOIN
    Polaris.Polaris.StatisticalCodes AS [sc1] WITH (NOLOCK) ON cir.AssignedBranchID = sc1.OrganizationID AND cir.StatisticalCodeID = sc1.StatisticalCodeID
LEFT OUTER JOIN
    Polaris.Polaris.StatisticalCodes AS [sc2] WITH (NOLOCK) ON sc1.Description = sc2.Description AND sc2.OrganizationID = '3'

WHERE
    AssignedBranchID = '14'
AND
    Barcode NOT LIKE 'econtent%'
AND
    CheckInDate BETWEEN DATEADD(day,-3,GETDATE()) AND DATEADD(day,-2,GETDATE())