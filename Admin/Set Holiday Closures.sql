/*This query has a bunch of holidays set up for updating the Closed Dates and the Dates Not to Call tables. It's often quicker to add your holiday
closures like this rather than one-by-one through the Staff Client. Set the dates as needed and remove those you don't need. (Or comment out the 
INSERTs you don't want.) */

/* --- SET THE LIBRARY/BRANCH YOU'RE WORKING WITH --- */
DECLARE @OrgID int;
SET @OrgID = '';

/* --- SET UP THE LIST OF COMMON CLOSED DATES --- */

-- New Year's Day
DECLARE @NewYear datetime;
SET @NewYear = '20220101';

-- MLK Day
DECLARE @MLKDay datetime;
SET @MLKDay = '20220117';

-- President's Day
DECLARE @PresidentsDay datetime;
SET @PresidentsDay = '20220221';

-- Good Friday
DECLARE @GoodFriday datetime;
SET @GoodFriday = '20220415'

-- Easter
DECLARE @Easter datetime;
SET @Easter = '20220417';

-- Memorial Day
DECLARE @MemorialDay datetime;
SET @MemorialDay = '20220530';

-- Juneteenth
DECLARE @Juneteenth datetime;
SET @Juneteenth = '20220619';

-- Independence Day
DECLARE @IndependenceDay datetime;
SET @IndependenceDay = '20220704';

-- Labor Day
DECLARE @LaborDay datetime;
SET @LaborDay = '20220905';

-- Indigenous People's Day
DECLARE @IPD datetime;
SET @IPD = '20221010';

-- Veteran's Day
DECLARE @VeteransDay datetime;
SET @VeteransDay = '20221111';

-- Thanksgiving Day
DECLARE @Thanksgiving datetime;
SET @Thanksgiving = '20221124';

-- Day after Thanksgiving
DECLARE @DayAfterThanksgiving datetime;
SET @DayAfterThanksgiving = '20221125';

-- Day after day after Thanksgiving
DECLARE @DayAfterThanksgiving2 datetime;
SET @DayAfterThanksgiving2 = '20221126';

-- Day after day after day after Thanksgiving
DECLARE @DayAfterThanksgiving3 datetime;
SET @DayAfterThanksgiving3 = '20221127';

-- Christmas Eve Eve
DECLARE @XmasEveEve datetime;
SET @XmasEveEve = '20221223';

-- Christmas Eve
DECLARE @XMasEve datetime;
SET @XMasEve = '20221224';

-- Christmas Day
DECLARE @XMas datetime;
SET @XMas = '20221225';

-- Day after Christmas
DECLARE @DayAfterXMas datetime;
SET @DayAfterXMas = '20221226';

-- New Year's Eve (2023)
DECLARE @NewYearNextEve datetime;
SET @NewYearNextEve = '20221231'

-- New Year's Day (2023)
DECLARE @NewYearNext datetime;
SET @NewYearNext = '20230101';

-- Day After New Year's Day (2023)
Declare @DayAfterNewYear datetime;
SET @DayAfterNewYear = '20230102'

/* --- UPDATE CLOSED DATES --- */

--INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@NewYear);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@MLKDay);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@PresidentsDay);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@GoodFriday);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@Easter);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@MemorialDay);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@Juneteenth);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@IndependenceDay);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@LaborDay);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@IPD);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@VeteransDay);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@Thanksgiving);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@DayAfterThanksgiving);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@DayAfterThanksgiving2);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@DayAfterThanksgiving3);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@XmasEveEve);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@XMasEve);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@XMas);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@DayAfterXMas);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@NewYearNextEve);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@NewYearNext);
INSERT INTO Polaris.Polaris.DatesClosed (OrganizationID, DateClosed) VALUES (@OrgID,@DayAfterNewYear);

/* --- UPDATE DO NOT CALL DATES --- */

--INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@NewYear);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@MLKDay);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@PresidentsDay);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@GoodFriday);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@Easter);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@MemorialDay);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@Juneteenth);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@IndependenceDay);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@LaborDay);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@IPD);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@VeteransDay);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@Thanksgiving);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@DayAfterThanksgiving);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@DayAfterThanksgiving2);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@DayAfterThanksgiving3);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@XMasEveEve);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@XMasEve);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@XMas);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@DayAfterXMas);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@NewYearNextEve);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@NewYearNext);
INSERT INTO Polaris.Polaris.NSDatesNotToCall (OrganizationID, DateNotToCall) VALUES (@OrgID,@DayAfterNewYear);