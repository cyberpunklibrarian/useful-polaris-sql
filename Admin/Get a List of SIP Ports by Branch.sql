/* A simple query that pulls a list of SIP ports (TCP/IP), and gives that to you with the
branch name and organization ID */

SELECT
    ssp.OrganizationID,
    o.Name,
    ssp.TCPIPPortNumber

FROM
    Polaris.Polaris.SIPServicePorts ssp WITH (NOLOCK)

JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK) on o.OrganizationID = ssp.OrganizationID
    
ORDER BY
    ssp.OrganizationID