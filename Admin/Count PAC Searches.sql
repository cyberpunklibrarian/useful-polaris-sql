/* This query pulls the number of searches on your PowerPACs via the Transactions database through your
TransactionDetails. It presents the counts, per month, along with the library/branch associated with the
search.

Thank you so much to Trevor Diamond, Morris Automated Information Network (MAIN)!

*/

SELECT
    DATEPART(MONTH,th.TranClientDate) AS [Month Index],
    DATEPART(YEAR,th.TranClientDate) AS [Year],
    DATENAME(MONTH,th.TranClientDate) AS [Month],
    o.Name AS [Library],
    COUNT(*) AS [Searches]

FROM
    PolarisTransactions.Polaris.TransactionHeaders AS [th]

INNER JOIN
    PolarisTransactions.Polaris.TransactionTypes AS [tt] WITH (NOLOCK) ON th.TransactionTypeID = tt.TransactionTypeID
INNER JOIN
    Polaris.Polaris.Organizations AS [o] WITH (NOLOCK) ON o.OrganizationID = th.OrganizationID

WHERE
    tt.TransactionTypeDescription LIKE 'Search%'
AND
    th.TranClientDate > DATEADD(MONTH,-2,GETDATE())

GROUP BY
    DATEPART(MONTH,th.TranClientDate),
    DATEPART(YEAR,th.TranClientDate),
    DATENAME(MONTH,th.TranClientDate),
    o.Name
ORDER BY
    DATEPART(MONTH,th.TranClientDate) DESC