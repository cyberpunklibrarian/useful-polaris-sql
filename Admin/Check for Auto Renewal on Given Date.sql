/* This query checks to see if automatic renewals happened or not. Simply put, if you don't get any output, then chances
are good that the auto-renewal database job didn't work properly. */

SELECT
    br.BrowseTitle as "Title",
    pr.PatronFullName as "Patron",
    p.Barcode as "Barcode",
    irha.ActionTakenDesc as "Action Taken",
    o.Name as "Library",
    irhd.TransactionDate as "Date"

FROM
    Polaris.Polaris.ItemRecordHistory irhd WITH (NOLOCK)

JOIN
    Polaris.Polaris.ItemRecords ir WITH (NOLOCK) on ir.ItemRecordID = irhd.ItemRecordID
JOIN
    Polaris.Polaris.BibliographicRecords br WITH (NOLOCK) on ir.AssociatedBibRecordID = br.BibliographicRecordID
JOIN
    Polaris.Polaris.ItemRecordHistoryActions irha WITH (NOLOCK) on irha.ActionTakenID = irhd.ActionTakenID
JOIN
    Polaris.Polaris.PatronRegistration pr WITH (NOLOCK) on irhd.PatronID = pr.PatronID
JOIN
    Polaris.Polaris.Patrons p WITH (NOLOCK) on p.PatronID = irhd.PatronID
JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK) on o.OrganizationID = irhd.OrganizationID

WHERE
    irhd.ActionTakenID = 93 -- Automatically renwed
AND
    irhd.TransactionDate BETWEEN '2020-10-07 00:00:00.000' AND '2020-10-07 23:59:59.999' -- Adjust dates as needed

ORDER BY
    pr.PatronFullName