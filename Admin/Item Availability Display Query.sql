/* This query produces a readable output of your system's Item Availability Display table. */

SELECT
    iad.OrganizationID AS [OrganizationID],
    o.Name AS [Library/Branch],
    ial.Description AS [Level],
    iad.SortOrder AS [Sort Order],
    bl.Name AS [Branch Location],
    iad.BranchLocationOrgID AS [Branch Location ID]

FROM
    Polaris.Polaris.ItemAvailabilityDisplay iad WITH (NOLOCK)

INNER JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK)
    ON (o.OrganizationID = iad.OrganizationID)
INNER JOIN
    Polaris.Polaris.ItemAvailabilityLevels ial WITH (NOLOCK)
    ON (ial.ItemAvailabilityLevelID = iad.ItemAvailabilityLevelID)
INNER JOIN
    Polaris.Polaris.Organizations bl WITH (NOLOCK)
    ON (bl.OrganizationID = iad.BranchLocationOrgID)

ORDER BY
    o.Name,
    ial.Description,
    iad.SortOrder