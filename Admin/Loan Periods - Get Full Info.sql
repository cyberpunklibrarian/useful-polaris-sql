/* While you can get the list of Loan Periods from your Polaris system administration and management tools
in the Staff Client, sometimes it's useful to get the whole list in one place, so you can export it to a
CSV or Excel file. This query pulls all the relevant information for Loan Periods so you can easily see
what's going on. */

SELECT
    lp.LPID AS "Loan Period ID",
    lp.OrganizationID AS "Org ID",
    o.Name AS "Library",
    lp.PatronCodeID AS "Patron Code ID",
    pc.Description AS "Patron Code",
    lp.LoanPeriodCodeID AS "Loan Period Code ID",
    lpc.Description AS "Loan Period Description",
    lp.TimeUnit AS "Time Unit ID",
    tu.Description AS "Time Unit",
    lp.Units AS "Units"
FROM
    Polaris.Polaris.LoanPeriods lp WITH (NOLOCK)

JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK) ON o.OrganizationID = lp.OrganizationID
JOIN
    Polaris.Polaris.PatronCodes pc WITH (NOLOCK) ON pc.PatronCodeID = lp.PatronCodeID
JOIN
    Polaris.Polaris.LoanPeriodCodes lpc WITH (NOLOCK) ON lp.LoanPeriodCodeID = lpc.LoanPeriodCodeID
JOIN
    Polaris.Polaris.TimeUnits tu WITH (NOLOCK) ON tu.TimeUnitID = lp.TimeUnit

WHERE
    lp.OrganizationID IN (19,20,21) -- Limit by organization codes if needed
