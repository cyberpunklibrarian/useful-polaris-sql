/* This is a quick script to check for Polaris users with elevated access to SimplyReports.
Modify the the WHERE parameter as needed, but I typically don't give the standard users
access to admin, patron and item history, and elevated metadata privileges. */

SELECT
    pu.Name AS [User Name]
FROM
    Polaris.Polaris.RWRITER_UserSecurity simply WITH (NOLOCK)
INNER JOIN
    Polaris.Polaris.PolarisUsers pu WITH (NOLOCK) ON pu.PolarisUserID = simply.PolarisUserID
WHERE
    (AdminUsersAccess = 1
    OR PatronHistoryReports = 1
    OR ItemHistoryReports = 1
    OR EditMetadaAccess = 1 -- Yes this is spelled correctly.
    OR AddMetadaAccess = 1) -- ¯\_(ツ)_/¯
AND
    pu.Name NOT LIKE '%(Deleted)' -- Don't bother with users who aren't there.
