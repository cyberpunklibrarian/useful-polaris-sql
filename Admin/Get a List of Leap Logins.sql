/* This query pulls a list of logins to Polaris Leap and provides you with the username, the organization
they logged into, the name of the computer they logged in on, and the datetime of the login. */

SELECT
    pu.Name AS [Polaris User],
    o.Name AS [Branch / Library],
    w.DisplayName AS [Workstation],
    th.TranClientDate AS [Login Time]

FROM
    PolarisTransactions.Polaris.TransactionHeaders th WITH (NOLOCK)

INNER JOIN -- Looking for system logins
    PolarisTransactions.Polaris.TransactionDetails td WITH (NOLOCK)
    ON (th.TransactionID = td.TransactionID AND td.TransactionSubTypeID = 235)
INNER JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK)
    ON (o.OrganizationID = th.OrganizationID)
INNER JOIN
    Polaris.Polaris.Workstations w WITH (NOLOCK)
    ON (w.WorkstationID = th.WorkstationID)
INNER JOIN
    Polaris.Polaris.PolarisUsers pu WITH (NOLOCK)
    ON (pu.PolarisUserID = th.PolarisUserID)

WHERE -- System login
    th.TransactionTypeID = 7200
AND -- Leap login
    td.numValue = 38
AND -- Adjust dates as needed.
    th.TranClientDate BETWEEN '2023-04-25 00:00:00.000' AND '2023-04-26 23:59:59.999'

ORDER BY
    th.TranClientDate DESC
