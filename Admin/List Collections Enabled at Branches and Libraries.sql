/* This query provides a simple output to show which Collections are enabled at all or given branches. */


SELECT
    oc.OrganizationID AS [Organization ID],
    o.Name AS [Library/Branch],
    oc.CollectionID AS [Collection ID],
    c.Name AS [Collection],
    c.Abbreviation AS [Abbreviation]

FROM
    Polaris.Polaris.OrganizationsCollections oc WITH (NOLOCK)

INNER JOIN
    Polaris.Polaris.Organizations o WITH (NOLOCK)
    ON (o.OrganizationID = oc.OrganizationID)
INNER JOIN
    Polaris.Polaris.Collections c WITH (NOLOCK)
    ON (c.CollectionID = oc.CollectionID)

-- Uncomment and add OrganizationIDs to limit to given branches.
--WHERE oc.OrganizationID IN (1,2,3,4,5)

ORDER BY
    o.Name,
    c.Name
