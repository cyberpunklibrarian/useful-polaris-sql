/* When you pull data from the TransactionDetails table, you get a bunch of numeric codes that are FK relations
to other tables in the Transactions and Polaris databases. Unless you've memorized all these numbers and what
they mean, it's hard to know what you're looking at. This query allows you to throw a TransactionID into a
variable, run that against TransactionDetails, and get human readable information on that transaction. In the
case of a numValue that corresponds to a TransactionSubTypeCode, this query will also pull the SubTypeCode 
description. */

-- Declare a variable
DECLARE @TransactionID INT;

-- Put in your TransactionID
SET @TransactionID = 740657;

SELECT
    td.TransactionID,
    td.TransactionSubTypeID,
    tst.TransactionSubTypeDescription,
    td.numValue,
    tstc.TransactionSubTypeCodeDesc

FROM
    PolarisTransactions.Polaris.TransactionDetails td WITH (NOLOCK)

INNER JOIN
    PolarisTransactions.Polaris.TransactionSubTypes tst WITH (NOLOCK)
    ON (tst.TransactionSubTypeID = td.TransactionSubTypeID)

WHERE --Utilize your variable
    td.TransactionID = @TransactionID

ORDER BY
    TransactionSubTypeID ASC